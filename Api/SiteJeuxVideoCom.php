<?php

namespace IiMedias\VideoGamesBundle\Api;

use \DateTime;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatformQuery;
use \stdClass;

class SiteJeuxVideoCom extends SiteBasic
{
    protected $output;
    protected $months;

    public function __construct($output = null)
    {
        $config              = AppConfigSingleton::getInstance();
        $configGamesLastPage = $config->getValue('siteJeuxVideoComGamesLastPage', 1);
        $this->output = $output;
        $this->months = array(
            'Janvier'   => '01',
            'Février'   => '02',
            'Mars'      => '03',
            'Avril'     => '04',
            'Mai'       => '05',
            'Juin'      => '06',
            'Juillet'   => '07',
            'Août'      => '08',
            'Septembre' => '09',
            'Octobre'   => '10',
            'Novembre'  => '11',
            'Décembre'  => '12',
        );
    }

    protected function loadUrl($url, $parameters = array(), $headers = array(), $baseUri = 'http://www.jeuxvideo.com')
    {
        if (strstr($url, $baseUri) !== false) {
            $url = substr($url, strlen($baseUri));
        }
        if (!is_array($parameters)) {
            $parameters = array();
        }
        if (!is_array($headers)) {
            $headers = array();
        }

        $response = parent::loadUrl($url, $parameters, $headers, $baseUri . '/');
        return $response;
    }

    public function games($page = 'last')
    {
        $months = $this->months;
        $limit  = 20;
        $config = AppConfigSingleton::getInstance();

        switch ($page) {
            case 'all':
                $offset = 1;
                $loop   = true;
                break;
            case 'last':
                $config = AppConfigSingleton::getInstance();
                $offset = $config->getValue('siteJeuxVideoComGamesLastPage');
                $loop   = false;
                break;
            default:
                $offset = $page;
                $loop   = false;
        }

        for ($continue = true; $continue == true;) {
            $this->writeln('[JEUXVIDEO.COM] Chargement des jeux de la page ' . $offset);
            $continue = $loop;

            $response     = $this->loadUrl('/tous-les-jeux/', array('p' => $offset));
            $gameElements = $response->find('.liste-item-jaq article');
            $lastPage     = intval($response->find('.bloc-liste-num-page span', - 1)->innertext());

            foreach ($gameElements as $gameElement) {
                $name           = trim($gameElement->find('h2.titre-item a', 0)->innertext());
                $siteUrl        = trim($gameElement->find('h2.titre-item a', 0)->href);
                $platforms      = $gameElement->find('span.label-machine');
                $infosElements  = $gameElement->find('.line-info');
                $releasedAtText = null;
                $releasedAt     = null;
                foreach ($infosElements as $infosElement) {
                    if (trim($infosElement->find('.lib', 0)->innertext()) == 'Date de sortie :') {
                        $releasedAtText = trim($infosElement->find('.info', 0)->innertext());
                        if (preg_match('#^([0-9]{2}) ([A-Za-z&;éû]*) ([0-9]{4})$#', $releasedAtText, $dateMatches)) {
                            $releasedAt = new DateTime($dateMatches[3] . '-' . $months[$dateMatches[2]] . '-' . $dateMatches[1] . ' 00:00:00');
                        }
                    }
                }

                $id = null;
                if (preg_match('#/jeux/[a-z0-9-]*/([0-9]*)-[a-z0-9-]*.htm#', $siteUrl, $idMatches)) {
                    $id = intval($idMatches[1]);
                }
                elseif (preg_match('#/jeux/[a-z0-9-]*/jeu-([0-9]*)/#', $siteUrl, $idMatches)) {
                    $id = intval($idMatches[1]);
                }
                elseif (preg_match('#/jeux/jeu-([0-9]*)/#', $siteUrl, $idMatches)) {
                    $id = intval($idMatches[1]);
                }

                $siteJeuxVideoComGame = SiteJeuxVideoComGameQuery::create()
                    ->filterById($id)
                    ->findOneOrCreate();

                $platformsList = array();
                foreach ($platforms as $platform) {
                    $platformsList[] = trim($platform->innertext());
                }
                $this->writeln('[JEUXVIDEO.COM][JEU] ' . $name . ' {' . implode('}{', $platformsList) . '} [' . $siteJeuxVideoComGame->getId() . ']');

                $siteJeuxVideoComGame
                    ->setName($name)
                    ->setSiteDetailUrl($siteUrl)
                    ->setReleasedAt($releasedAt)
                    ->save();

                foreach ($platforms as $platform) {
                    $siteJeuxVideoComPlatform = SiteJeuxVideoComPlatformQuery::create()
                        ->filterByAbbr(trim($platform->innertext()))
                        ->findOneOrCreate();
                    $siteJeuxVideoComPlatform->save();

                    $siteJeuxVideoComRelease = SiteJeuxVideoComGameReleaseQuery::create()
                        ->filterBySiteJeuxVideoComGame($siteJeuxVideoComGame)
                        ->filterBySiteJeuxVideoComPlatform($siteJeuxVideoComPlatform)
                        ->findOneOrCreate();
                    $siteJeuxVideoComRelease->save();
                }
            }

            $offset += 1;
            if ($config->getValue('siteJeuxVideoComGamesLastPage') == ($offset - 1)) {
                $config->setValue('siteJeuxVideoComGamesLastPage', $offset);
            }
            if ($offset > $lastPage) {
                $continue = false;
                $config->setValue('siteJeuxVideoComGamesLastPage', 1);
            }
        }
    }

    public function game($game)
    {
        if (is_int($game)) {
            $game = SiteJeuxVideoComGameQuery::create()
                ->filterById($game)
                ->findOne()
            ;
        }

        $response     = $this->loadUrl($game->getSiteDetailUrl());
        dump($game);
        dump($response);
        exit();
    }
}
