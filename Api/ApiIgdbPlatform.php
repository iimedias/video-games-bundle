<?php

namespace IiMedias\VideoGamesBundle\Api;

use \DateTime;
use \Exception;
use IiMedias\VideoGamesBundle\Api\ApiBasicIgdb;
use IiMedias\VideoGamesBundle\Model\ApiCountQuery;
use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaform as ApiIgdbPlatformModel;
use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaformQuery;

class ApiIgdbPlatform extends ApiBasicIgdb
{
    protected $url;
    protected $fields;
    protected $limit;
    protected $offset;
    protected $order;

    public function __construct()
    {
        $this->url    = '/platforms/';
        $this->fields = '*';
        $this->limit  = 50;
        $this->offset = 0;
        $this->order  = 'platform.id:asc';
    }

    protected function constructAndLoadUrl()
    {
        $response = $this->loadUrl($this->url, array(
            'fields' => $this->fields,
            'limit'  => $this->limit,
            'offset' => $this->offset,
//            'order'  => $this->order,
        ));
        return $response;
    }

    public function refreshAll()
    {
        for ($canContinue = true, $this->offset = 0; $canContinue == true; ) {
            $apiPlatforms = $this->constructAndLoadUrl();
            foreach ($apiPlatforms->body as $apiPlatform) {
                $igdbPlatform = ApiIgdbPlaformQuery::create()
                    ->filterById($apiPlatform->id)
                    ->findOneOrCreate();
                foreach (get_object_vars($apiPlatform) as $ojbectPropertyName => $ojbectPropertyValue) {
                    if (!in_array($ojbectPropertyName, array('id', 'name', 'slug', 'url', 'website', 'alternative_name', 'generation', 'summary', 'games', 'created_at', 'updated_at'))) {
                        dump('propriété inconnue : ' . $ojbectPropertyName);
                        die();
                    }
                }
                $createdAt = new DateTime();
                $createdAt->setTimestamp(round($apiPlatform->created_at / 1000));
                $updatedAt = new DateTime();
                $updatedAt->setTimestamp(round($apiPlatform->updated_at / 1000));
                $igdbPlatform
                    ->setName($apiPlatform->name)
                    ->setSlug($apiPlatform->slug)
                    ->setUrl($apiPlatform->url)
                    ->setGeneration(property_exists($apiPlatform, 'generation') ? $apiPlatform->generation : null)
                    ->setWebSite(property_exists($apiPlatform, 'website') ? $apiPlatform->website : null)
                    ->setAlternativeName(property_exists($apiPlatform, 'alternative_name') ? $apiPlatform->alternative_name : null)
                    ->setSummary(property_exists($apiPlatform, 'summary') ? $apiPlatform->summary : null)
                    ->setGames(property_exists($apiPlatform, 'games') ? $apiPlatform->games : array())
                    ->setCreatedAt($createdAt)
                    ->setUpdatedAt($updatedAt)
                ;
                try {
                    $igdbPlatform->save();
                } catch (Exception $e) {
                    $igdbPlatform
                        ->setSummary(null)
                        ->save();
                }
            }
            if (count($apiPlatforms->body) < 50) {
                $canContinue = false;
            }
            $this->offset = $this->offset + 50;
        }
    }
}