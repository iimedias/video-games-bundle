<?php

namespace IiMedias\VideoGamesBundle\Api;

use \DateTime;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultPlatformQuery;
use Sunra\PhpSimple\HtmlDomParser;

class SiteGameKult extends SiteBasic
{
    protected $months;

    public function __construct($output)
    {
        $this->output = $output;
        $this->months = array(
            'jan' => '01', 'janv' => '01', 'Janvier' => '01',
            'fev' => '02', 'fevr' => '02', 'Février' => '02', 'F&eacute;vrier' => '02',
            'mar' => '03', 'mars' => '03', 'Mars' => '03',
            'avr' => '04', 'avri' => '04', 'Avril' => '04',
            'mai' => '05', 'Mai' => '05',
            'juin' => '06', 'Juin' => '06',
            'juil' => '07', 'Juillet' => '07',
            'aou' => '08', 'aout' => '07', 'Août' => '07', 'Ao&ucirc;t' => '07', 'ao&ucirc;t' => '07',
            'sep' => '09', 'sept' => '09', 'Septembre' => '09',
            'oct' => '10', 'octo' => '10', 'Octobre' => '10',
            'nov' => '11', 'nove' => '11', 'Novembre' => '11',
            'dec' => '12', 'dece' => '12', 'D&eacute;cembre' => '12',
        );
    }

    protected function loadUrl($url, $parameters = array(), $headers = array(), $baseUri = 'http://www.gamekult.com')
    {
        if (strstr($url, $baseUri) !== false) {
            $url = substr($url, strlen($baseUri));
        }
        if (!is_array($parameters)) {
            $parameters = array();
        }
        if (!is_array($headers)) {
            $headers = array();
        }

        $response = parent::loadUrl($url, $parameters, $headers, $baseUri . '/');
        return $response;
    }

    public function game($game)
    {
        if (is_int($game)) {
            $game = SiteGameKultGameQuery::create()
                ->filterById($game)
                ->findOne()
            ;
        }

        $response = $this->loadUrl($game->getSiteDetailUrl());
        $months   = $this->months;

//        $specs = $response->find('#game-specs .specifications li');
//        dump('specs => ' . count($specs));
//        dump($specs);

        $metaPlatforms = $response->find('#subheader .metas a.label');
        foreach ($metaPlatforms as $metaPlatform) {
            if (substr($metaPlatform->id, 0, 3) === 'pf-' && substr($metaPlatform->id, 3) !== 'multi') {
                $platform = SiteGameKultPlatformQuery::create()
                    ->filterByAbbr(substr($metaPlatform->id, 3))
                    ->findOneOrCreate();
                $platform->save();

                $release  = SiteGameKultGameReleaseQuery::create()
                    ->filterBySiteGameKultGame($game)
                    ->filterBySiteGameKultPlatform($platform)
                    ->findOneOrCreate();

                $release  = $release
                    ->setSiteDetailUrl('http://www.gamekult.com/jeux/getcontent/panorama/' . $game->getGamekultId() . '/' . $platform->getAbbr() . '/')
                ;

                $releasedAtDates = explode('<br>', $metaPlatform->title);
                foreach ($releasedAtDates as $releasedAtDate) {
                    list($releasedAtDateZone, $releasedAtDateValue) = explode(' : ', $releasedAtDate);
                    if (preg_match('#([0-9]{1,2}) ([A-Za-z&;]*) ([0-9]{4})#', $releasedAtDateValue, $releaseDateMatches)) {
                        $releaseAtDate = new DateTime($releaseDateMatches[3] . '-' . $months[$releaseDateMatches[2]] . '-' . ((intval($releaseDateMatches[1]) < 10) ? '0' : '') . $releaseDateMatches[1]);
                    }
                    elseif (preg_match('#([A-Za-z&;]*) ([0-9]{4})#', $releasedAtDateValue, $releaseDateMatches)) {
                        $releaseAtDate = new DateTime($releaseDateMatches[2] . '-' . $months[$releaseDateMatches[1]] . '-01');
                        $releaseAtDate = $releaseAtDate->modify($releaseAtDate->format('Y-m-t'));
                    }
                    else {
                        $releaseAtDate = null;
                    }
                    switch ($releasedAtDateZone) {
                        case 'France':
                            $release = $release->setFrReleasedAt($releaseAtDate);
                            break;
                        case 'États-Unis':
                            $release = $release->setUsReleasedAt($releaseAtDate);
                            break;
                        case 'Japon':
                            $release = $release->setJpReleasedAt($releaseAtDate);
                            break;
                        default:
                            dump($releasedAtDateZone);
                    }
                }

                $release
                    ->save();
            }
        }

        $game
            ->setRefreshedAt(new DateTime())
            ->save();

        return $game;
    }

    public function games($platformCode = 'last', $offset = 'last')
    {
        $config        = AppConfigSingleton::getInstance();
        $platformCode  = ($platformCode === 'last') ? $config->getValue('siteGameKultGamesLastPlaftorm', 'pc') : $platformCode;
        $offset        = ($offset === 'last') ? $config->getValue('siteGameKultGamesLastOffset', 1) : $offset;
        $this->writeln('[GAMEKULT] Chargement des jeux ' . $platformCode . ' page ' . $offset);
        $response      = $this->loadUrl('/jeux/jeux-' . $platformCode . '.html', array('p' => $offset));

        $gameElements = $response->find('#releases article');
        foreach ($gameElements as $gameElement) {
            $id      = null;
            $name    = trim($gameElement->find('.title a', 0)->innertext());
            $siteUrl = $gameElement->find('.title a', 0)->href;
            if (preg_match('#/jeux/[a-z0-9-]*-((J|SU)([0-9]*)).html#', $siteUrl, $idMatches)) {
                $id         = $idMatches[3];
                $typeId     = $idMatches[2];
                $gamekultId = $idMatches[1];
            }
            else {
                $id         = null;
                $typeId     = null;
                $gamekultId = null;
            }
            $imageSmallUrl = $gameElement->find('figure img', 0)->src;
            $platformLabel = strtolower($gameElement->find('.hgroup span.label', 0)->innertext());
            $this->writeln('[GAMEKULT][JEU] #' . $id . ' ' . $name);

            if (!is_null($id)) {
                $siteGameKultGame = SiteGameKultGameQuery::create()
                    ->filterById($id)
                    ->findOneOrCreate();

                $siteGameKultGame
                    ->setTypeId($typeId)
                    ->setGamekultId($gamekultId)
                    ->setName(trim($gameElement->find('.title a', 0)->innertext()))
                    ->setSiteDetailUrl('http://www.gamekult.com'. $siteUrl)
                    ->setImageSmallUrl($gameElement->find('figure img', 0)->src)
                    ->save()
                ;

                $siteGameKultPlatform = SiteGameKultPlatformQuery::create()
                    ->filterByAbbr($platformLabel)
                    ->findOneOrCreate();

                $siteGameKultPlatform
                    ->setListUrlAbbr($platformCode)
                    ->save();

                $siteGameKultRelease = SiteGameKultGameReleaseQuery::create()
                    ->filterBySiteGameKultGame($siteGameKultGame)
                    ->filterBySiteGameKultPlatform($siteGameKultPlatform)
                    ->findOneOrCreate();

                $siteGameKultRelease->save();
            }
        }

        if ($config->getValue('siteGameKultGamesLastPlaftorm') == $platformCode && $config->getValue('siteGameKultGamesLastOffset') == $offset) {
            $lastPage   = $response->find('.pagination li', -1);
            $isLastPage = ($lastPage->class !== false && trim($lastPage->class) === 'disabled') ? true : false;

            $nextOffset       = $isLastPage ? 1 : $offset + 1;
            $config->setValue('siteGameKultGamesLastOffset', $nextOffset);

            if ($isLastPage) {
                $platformsList    = $response->find('#filtermenu li a');
                $useNext          = false;
                $nextPlatformCode = null;
                foreach ($platformsList as $platformElem) {
                    preg_match("#^/jeux/jeux-([a-z0-9-]+)\.html$#", $platformElem->href, $matchesPlatformElem);
                    $currentPlatformCode = $matchesPlatformElem[1];
                    $currentPlatformCode = ($currentPlatformCode === 'video') ? 'switch' : $currentPlatformCode;
                    if ($currentPlatformCode == $platformCode) {
                        $useNext = true;
                        continue;
                    }
                    if ($useNext == true) {
                        $useNext         = false;
                        $nextPlatformCode = $currentPlatformCode;
                        break;
                    }
                }
                $nextPlatformCode = $useNext ? 'switch' : $nextPlatformCode;
                $config->setValue('siteGameKultGamesLastPlaftorm', $nextPlatformCode);
            }
        }
    }
}