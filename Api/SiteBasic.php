<?php

namespace IiMedias\VideoGamesBundle\Api;

use \Exeception;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class SiteBasic
{
    protected $output;

    public function __construct($output = null)
    {
        $this->output = $output;
    }

    protected function loadUrl($url, $parameters = array(), $headers = array(), $baseUri = '')
    {
        if (is_array($parameters) && count($parameters) > 0) {
            $count = 0;
            foreach ($parameters as $parameterKey => $parameterValue) {
                $url .= (($count > 0) ? '&' : '?') . $parameterKey . '=' . $parameterValue;
                $count++;
            }
        }

        $this->writeln('[LOADURL][URL] ' . $baseUri . $url);

        $client   = new Client(array('base_uri' => $baseUri, 'timeout' => 30.0));
        $response = $client->request('GET', $url);
        if ($response->getStatusCode() == 200) {
            $response = HtmlDomParser::str_get_html($response->getBody()->getContents());
        }
        else {
            throw new Exception($response->getStatusCode());
        }

        return $response;
    }

    protected function writeln($message)
    {
        if (!is_null($this->output) && $this->output->isVerbose()) {
            $this->output->writeln($message);
        }
    }
}