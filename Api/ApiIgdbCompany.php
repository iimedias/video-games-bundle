<?php

namespace IiMedias\VideoGamesBundle\Api;

use \DateTime;
use \Exception;
use IiMedias\VideoGamesBundle\Api\ApiBasicIgdb;
use IiMedias\VideoGamesBundle\Model\ApiCountQuery;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompany as ApiIgdbCompanyModel;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompanyQuery;

class ApiIgdbCompany extends ApiBasicIgdb
{
    protected $url;
    protected $fields;
    protected $limit;
    protected $offset;
    protected $order;

    public function __construct()
    {
        $this->url    = '/companies/';
        $this->fields = '*';
        $this->limit  = 50;
        $this->offset = 0;
        $this->order  = 'company.id:asc';
    }

    protected function constructAndLoadUrl()
    {
        $response = $this->loadUrl($this->url, array(
            'fields' => $this->fields,
            'limit'  => $this->limit,
            'offset' => $this->offset,
//            'order'  => $this->order,
        ));
        return $response;
    }

    public function refreshAll()
    {
        $totalCompanies = ApiIgdbCompanyQuery::create()
            ->count();
        $startOffset = intval(floor($totalCompanies / 50)) * 50;
//        $startOffset = 1600;

        for ($canContinue = true, $this->offset = $startOffset; $canContinue == true; ) {
            $apiCompanies = $this->constructAndLoadUrl();
            dump($apiCompanies);
            foreach ($apiCompanies->body as $apiCompany) {
                $igdbCompany = ApiIgdbCompanyQuery::create()
                    ->filterById($apiCompany->id)
                    ->findOneOrCreate();
                foreach (get_object_vars($apiCompany) as $ojbectPropertyName => $ojbectPropertyValue) {
                    if (!in_array($ojbectPropertyName, array('id', 'parent', 'name', 'slug', 'url', 'start_date_category', 'change_date_category', 'published', 'developed', 'logo', 'description', 'start_date', 'change_date', 'country', 'website', 'twitter', 'created_at', 'updated_at'))) {
                        dump('propriété inconnue : ' . $ojbectPropertyName);
                        dump($ojbectPropertyValue);
                        die();
                    }
                }
                $createdAt = new DateTime();
                $createdAt->setTimestamp(round($apiCompany->created_at / 1000));
                $updatedAt = new DateTime();
                $updatedAt->setTimestamp(round($apiCompany->updated_at / 1000));
                if (property_exists($apiCompany, 'start_date')) {
                    $startDate = new DateTime();
                    $startDate->setTimestamp(round($apiCompany->start_date / 1000));
                }
                else {
                    $startDate = null;
                }
                if (property_exists($apiCompany, 'change_date')) {
                    $changeDate = new DateTime();
                    $changeDate->setTimestamp(round($apiCompany->change_date / 1000));
                }
                else {
                    $changeDate = null;
                }
                $igdbCompany
                    ->setParentId(property_exists($apiCompany, 'parent_id') ? $apiCompany->parent_id : null)
                    ->setName($apiCompany->name)
                    ->setSlug($apiCompany->slug)
                    ->setUrl($apiCompany->url)
                    ->setWebsite(property_exists($apiCompany, 'website') ? $apiCompany->website : null)
                    ->setCountry(property_exists($apiCompany, 'country') ? $apiCompany->country : null)
                    ->setTwitter(property_exists($apiCompany, 'twitter') ? $apiCompany->twitter : null)
                    ->setDescription(property_exists($apiCompany, 'description') ? $apiCompany->description : null)
                    ->setPublished(property_exists($apiCompany, 'published') ? $apiCompany->published : array())
                    ->setDeveloped(property_exists($apiCompany, 'developed') ? $apiCompany->developed : array())
                    ->setLogoCloudinaryId(property_exists($apiCompany, 'logo') ? $apiCompany->logo->cloudinary_id : null)
                    ->setLogoWidth(property_exists($apiCompany, 'logo') ? $apiCompany->logo->width : null)
                    ->setLogoHeight(property_exists($apiCompany, 'height') ? $apiCompany->logo->height : null)
                    ->setStartDate($startDate)
                    ->setStartDateCategory(property_exists($apiCompany, 'start_date_category') ? $apiCompany->start_date_category : null)
                    ->setChangeDate($changeDate)
                    ->setChangeDateCategory(property_exists($apiCompany, 'change_date_category') ? $apiCompany->change_date_category : null)
                    ->setCreatedAt($createdAt)
                    ->setUpdatedAt($updatedAt)
                ;
//                try {
                $igdbCompany->save();
//                } catch (Exception $e) {
//                    $igdbPlatform
//                        ->setSummary(null)
//                        ->save();
//                }
            }
            if (count($apiCompanies->body) < 50) {
                $canContinue = false;
            }
            $this->offset = $this->offset + 50;
        }
    }
}