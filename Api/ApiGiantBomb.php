<?php

namespace IiMedias\VideoGamesBundle\Api;

use IiMedias\VideoGamesBundle\Api\ApiBasic;
use IiMedias\VideoGamesBundle\Model\ApiCountQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombAccessoryQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImage;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery;
use \DateTime;
use \Exception;
use Propel\Runtime\Exception\PropelException;

class ApiGiantBomb extends ApiBasic
{
    public function __construct()
    {
        $this->giantbombApiCount = ApiCountQuery::create()
            ->filterByKey('giantbomb')
            ->findOne()
        ;

        if (!is_null($this->giantbombApiCount)) {
            $this->current   = new DateTime('now');
            $this->lastReset = new DateTime('now -' . ($this->current->getTimestamp() % $this->giantbombApiCount->getMaxQueryInterval()) . ' seconds');
            if ($this->giantbombApiCount->getUpdatedAt()->getTimestamp() <= $this->lastReset->getTimestamp()) {;
                $this->giantbombApiCount
                    ->setRemainingQueryNumber($this->giantbombApiCount->getMaxQueryNumber())
                    ->save()
                ;
            }
        }
    }

    protected function loadUrl($url, $parameters = array(), $headers = array())
    {
        $giantbombApiCount = $this->giantbombApiCount;
        if (is_null($giantbombApiCount) || $giantbombApiCount->getRemainingQueryNumber() <= 0) {
            return null;
        }
        $baseUrl = 'http://www.giantbomb.com/api';
        if (strstr($url, $baseUrl) === false) {
            $url = $baseUrl . $url;
        }
        if (!is_array($parameters)) {
            $parameters = array();
        }
        if (!is_array($headers)) {
            $headers = array();
        }
        if (!isset($parameters['api_key'])) {
            $parameters['api_key'] = $giantbombApiCount->getValue();
        }
        if (!isset($parameters['format'])) {
            $parameters['format'] = 'json';
        }
        $giantbombApiCount
            ->setRemainingQueryNumber($giantbombApiCount->getRemainingQueryNumber() - 1)
            ->save()
        ;

        $response = parent::loadUrl($url, $parameters);
        return $response;
    }

    public function accessories()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/accessories', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseAccessory) {
                    $this->updateLocalAccessory($apiResponseAccessory);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function characters()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/characters', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseCharacter) {
                    $this->updateLocalCharacter($apiResponseCharacter);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function companies()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/companies', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseCompany) {
                    $this->updateLocalCompany($apiResponseCompany);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function concepts()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/concepts', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseConcept) {
                    $this->updateLocalConcept($apiResponseConcept);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function franchises()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/franchises', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseFranchise) {
                    $this->updateLocalFranchise($apiResponseFranchise);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function game($game)
    {
        if (is_int($game)) {
            $game = ApiGiantBombGameQuery::create()
                ->filterById($game)
                ->findOne()
            ;
        }

        $response = $this->loadUrl($game->getApiDetailUrl());
        $response = $response->body;
        if ($response->error === 'OK' && $response->version === '1.0') {
            $game = $this->updateLocalGame($response->results);
        }

        return $game;
    }

    public function games($page = 'last', $limit = 100)
    {
        switch ($page) {
            case 'all':
                $offset = 0;
                $loop   = true;
                break;
            case 'last':
                $count  = ApiGiantBombGameQuery::create()->find()->count();
                $offset = floor($count / $limit) * $limit;
                $loop   = false;
                break;
            default:
                $offset = $page * $limit;
                $loop   = false;
        }

        for ($continue = true; $continue == true;) {
            $continue = $loop;
            $response = $this->loadUrl('/games', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseGame) {
                    $this->updateLocalGame($apiResponseGame);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function gameRatings()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/game_ratings', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseGameRating) {
                    $this->updateLocalGameRating($apiResponseGameRating);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function genres()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/genres', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseGenre) {
                    $this->updateLocalGenre($apiResponseGenre);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function locations()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/locations', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseLocation) {
                    $this->updateLocalLocation($apiResponseLocation);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function objects()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/objects', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseObject) {
                    $this->updateLocalObject($apiResponseObject);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function people()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/people', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponsePerson) {
                    $this->updateLocalPerson($apiResponsePerson);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function platforms()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue === true;) {
            $response = $this->loadUrl('/platforms', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponsePlatform) {
                    $this->updateLocalPlatform($apiResponsePlatform);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function ratingBoards()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/rating_boards', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseRatingBoard) {
                    $this->updateLocalRatingBoard($apiResponseRatingBoard);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function regions()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue == 100;) {
            $response = $this->loadUrl('/regions', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseRegion) {
                    $this->updateLocalRegion($apiResponseRegion);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    public function release($release)
    {
        if (is_int($release)) {
            $release = ApiGiantBombGameReleaseQuery::create()
                ->filterById($release)
                ->findOne()
            ;
        }

        $response = $this->loadUrl($release->getApiDetailUrl());
        $response = $response->body;
        if ($response->error === 'OK' && $response->version === '1.0') {
            $release = $this->updateLocalRelease($response->results);
        }

        return $release;
    }

    public function themes()
    {
        $limit = 100;

        for ($offset = 0, $continue = true; $continue === true;) {
            $response = $this->loadUrl('/themes', array('offset' => $offset, 'limit' => $limit, 'sort' => 'id:asc'));
            $response = $response->body;
            if ($response->error === 'OK' && $response->version === '1.0') {
                foreach ($response->results as $apiResponseTheme) {
                    $this->updateLocalTheme($apiResponseTheme);
                }
            }
            else {
                $continue = false;
            }
            $offset += $limit;
            if ($offset > $response->number_of_total_results) {
                $continue = false;
            }
        }
    }

    protected function updateLocalAccessory($apiAccessoryData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombAccessoryQuery',
            $apiAccessoryData->id,
            $apiAccessoryData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalCharacter($apiCharacterData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery',
            $apiCharacterData->id,
            $apiCharacterData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'aliases'                => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'last_name'              => array('dataName' => 'last_name', 'propelName' => 'LastName', 'type' => 'string'),
                'real_name'              => array('dataName' => 'real_name', 'propelName' => 'RealName', 'type' => 'string'),
                'gender'                 => array('dataName' => 'gender', 'propelName' => 'Gender', 'type' => 'string'),
                'birthday'               => array('dataName' => 'gender', 'propelName' => 'Gender', 'type' => 'timestamp'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'first_appeared_in_game' => array('dataName' => 'first_appeared_in_game', 'propelName' => 'ApiGiantBombFirstGame', 'type' => 'localFunction', 'functionName' => 'updateLocalGame'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalCompany($apiCompanyData)
    {
        if (!is_object($apiCompanyData)) {
            return null;
        }
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery',
            $apiCompanyData->id,
            $apiCompanyData,
            array(
                'id'                => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'              => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'abbreviation'      => array('dataName' => 'abbreviation', 'propelName' => 'Abbr', 'type' => 'string'),
                'aliases'           => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'              => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'       => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'location_address'  => array('dataName' => 'location_address', 'propelName' => 'LocationAddress', 'type' => 'string'),
                'location_city'     => array('dataName' => 'location_city', 'propelName' => 'LocationCity', 'type' => 'string'),
                'location_country'  => array('dataName' => 'location_country', 'propelName' => 'LocationCountry', 'type' => 'string'),
                'location_state'    => array('dataName' => 'location_state', 'propelName' => 'LocationState', 'type' => 'string'),
                'phone'             => array('dataName' => 'phone', 'propelName' => 'Phone', 'type' => 'string'),
                'website'           => array('dataName' => 'website', 'propelName' => 'Website', 'type' => 'string'),
                'api_detail_url'    => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'   => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'             => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_founded'      => array('dataName' => 'date_founded', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_added'        => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated' => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalConcept($apiConceptData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery',
            $apiConceptData->id,
            $apiConceptData,
            array(
                'id'                => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'              => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'api_detail_url'    => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'   => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'deck'              => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'       => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'aliases'           => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'image'             => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'        => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated' => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalFranchise($apiFranchiseData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery',
            $apiFranchiseData->id,
            $apiFranchiseData,
            array(
                'id'                => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'              => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'api_detail_url'    => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'   => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'deck'              => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'       => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'aliases'           => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'image'             => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'        => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated' => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalGame($apiGameData)
    {
        $linkLocal = 'ApiGiantBombGame';

        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery',
            $apiGameData->id,
            $apiGameData,
            array(
                'id'                       => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                     => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'platforms'                => array('dataName' => 'platforms', 'propelName' => 'ApiGiantBombGamePlatform', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombPlatform', 'updateFunctionName' => 'updateLocalPlatform'),
                'developers'               => array('dataName' => 'developers', 'propelName' => 'ApiGiantBombGameDeveloper', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombCompany', 'updateFunctionName' => 'updateLocalCompany'),
                'publishers'               => array('dataName' => 'publishers', 'propelName' => 'ApiGiantBombGamePublisher', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombCompany', 'updateFunctionName' => 'updateLocalCompany'),
                'franchises'               => array('dataName' => 'franchises', 'propelName' => 'ApiGiantBombGameFranchise', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombFranchise', 'updateFunctionName' => 'updateLocalFranchise'),
                'genres'                   => array('dataName' => 'genres', 'propelName' => 'ApiGiantBombGameGenre', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombGenre', 'updateFunctionName' => 'updateLocalGenre'),
                'releases'                 => array('dataName' => 'releases', 'propelName' => 'ApiGiantBombGameRelease', 'type' => 'localFunction', 'functionName' => 'updateLocalRelease', 'multiple' => true, 'refId' => 'model_game_id'),
                'aliases'                  => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'                     => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'              => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'number_of_user_reviews'   => array('dataName' => 'number_of_user_reviews', 'propelName' => 'UserReviewsCount', 'type' => 'string'),
                'original_release_date'    => array('dataName' => 'original_release_date', 'propelName' => 'OriginalReleasedAt', 'type' => 'timestamp'),
                'expected_release_day'     => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_month'   => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_quarter' => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_year'    => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'image'                    => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'images'                   => array('dataName' => 'images', 'propelName' => 'ApiGiantBombImages', 'type' => 'images', 'link' => 'Game'),
                'videos'                   => array('dataName' => 'videos', 'propelName' => 'ApiGiantBombVideos', 'type' => 'videos', 'link' => 'Game'),
                'api_detail_url'           => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'          => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'date_added'               => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'        => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),

                'original_game_rating' => array('dataName' => 'original_game_rating', 'propelName' => null, 'type' => 'ignore'),
                'characters' => array('dataName' => 'characters', 'propelName' => null, 'type' => 'ignore'),
                'concepts' => array('dataName' => 'concepts', 'propelName' => null, 'type' => 'ignore'),
                'first_appearance_characters' => array('dataName' => 'first_appearance_characters', 'propelName' => null, 'type' => 'ignore'),
                'first_appearance_concepts' => array('dataName' => 'first_appearance_concepts', 'propelName' => null, 'type' => 'ignore'),
                'first_appearance_locations' => array('dataName' => 'first_appearance_locations', 'propelName' => null, 'type' => 'ignore'),
                'first_appearance_objects' => array('dataName' => 'first_appearance_objects', 'propelName' => null, 'type' => 'ignore'),
                'first_appearance_people' => array('dataName' => 'first_appearance_people', 'propelName' => null, 'type' => 'ignore'),
                'killed_characters' => array('dataName' => 'killed_characters', 'propelName' => null, 'type' => 'ignore'),
                'locations' => array('dataName' => 'locations', 'propelName' => null, 'type' => 'ignore'),
                'objects' => array('dataName' => 'objects', 'propelName' => null, 'type' => 'ignore'),
                'people' => array('dataName' => 'people', 'propelName' => null, 'type' => 'ignore'),
                'similar_games' => array('dataName' => 'similar_games', 'propelName' => null, 'type' => 'ignore'),
                'themes' => array('dataName' => 'themes', 'propelName' => null, 'type' => 'ignore'),
                'reviews' => array('dataName' => 'reviews', 'propelName' => 'Reviews', 'type' => 'ignore'),
            )
        );
    }

    protected function updateLocalGameRating($apiGameRatingData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery',
            $apiGameRatingData->id,
            $apiGameRatingData,
            array(
                'id'             => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'           => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'api_detail_url' => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'image'          => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'rating_board'   => array('dataName' => 'rating_board', 'propelName' => 'ApiGiantBombRatingBoard', 'type' => 'localFunction', 'functionName' => 'updateLocalRatingBoard'),
            )
        );
    }

    protected function updateLocalGenre($apiGenreData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombGenreQuery',
            $apiGenreData->id,
            $apiGenreData,
            array(
                'id'                => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'              => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'deck'              => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'       => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'    => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'   => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'             => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'        => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated' => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalLocation($apiLocationData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery',
            $apiLocationData->id,
            $apiLocationData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'aliases'                => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'first_appeared_in_game' => array('dataName' => 'first_appeared_in_game', 'propelName' => 'ApiGiantBombFirstGame', 'type' => 'localFunction', 'functionName' => 'updateLocalGame'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalObject($apiObjectData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery',
            $apiObjectData->id,
            $apiObjectData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'aliases'                => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'first_appeared_in_game' => array('dataName' => 'first_appeared_in_game', 'propelName' => 'ApiGiantBombFirstGame', 'type' => 'localFunction', 'functionName' => 'updateLocalGame'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalPerson($apiPersonData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery',
            $apiPersonData->id,
            $apiPersonData,
            array(
                'id'                  => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'aliases'             => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'deck'                => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'         => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'gender'              => array('dataName' => 'gender', 'propelName' => 'Gender', 'type' => 'string'),
                'birth_date'          => array('dataName' => 'birth_date', 'propelName' => 'BirthDate', 'type' => 'timestamp'),
                'death_date'          => array('dataName' => 'death_date', 'propelName' => 'DeathDate', 'type' => 'timestamp'),
                'hometown'            => array('dataName' => 'hometown', 'propelName' => 'Hometown', 'type' => 'string'),
                'country'             => array('dataName' => 'country', 'propelName' => 'Country', 'type' => 'string'),
                'api_detail_url'      => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'     => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'               => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'first_credited_game' => array('dataName' => 'first_credited_game', 'propelName' => 'ApiGiantBombCreditedGame', 'type' => 'localFunction', 'functionName' => 'updateLocalGame'),
                'date_added'          => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'   => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalPlatform($apiPlatformData)
    {
        if (!is_object($apiPlatformData)) {
            return null;
        }
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery',
            $apiPlatformData->id,
            $apiPlatformData,
            array(
                'id'                => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'              => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'api_detail_url'    => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'   => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'aliases'           => array('dataName' => 'aliases', 'propelName' => 'Aliases', 'type' => 'array', 'delimiter' => "\n"),
                'abbreviation'      => array('dataName' => 'abbreviation', 'propelName' => 'Abbr', 'type' => 'string'),
                'deck'              => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'       => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'install_base'      => array('dataName' => 'install_base', 'propelName' => 'InstallBase', 'type' => 'string'),
                'online_support'    => array('dataName' => 'online_support', 'propelName' => 'OnlineSupport', 'type' => 'string'),
                'original_price'    => array('dataName' => 'original_price', 'propelName' => 'OriginalPrice', 'type' => 'string'),
                'image'             => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'company'           => array('dataName' => 'company', 'propelName' => 'ApiGiantBombCompany', 'type' => 'localFunction', 'functionName' => 'updateLocalCompany'),
                'release_date'      => array('dataName' => 'release_date', 'propelName' => 'ReleasedAt', 'type' => 'timestamp'),
                'date_added'        => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated' => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalRatingBoard($apiRatingBoardData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery',
            $apiRatingBoardData->id,
            $apiRatingBoardData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalRegion($apiRegionData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery',
            $apiRegionData->id,
            $apiRegionData,
            array(
                'id'                     => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'                   => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'deck'                   => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'            => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'api_detail_url'         => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'        => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'image'                  => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'date_added'             => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'      => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
            )
        );
    }

    protected function updateLocalRelease($apiReleaseData)
    {
        $linkLocal = 'ApiGiantBombGameRelease';

        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery',
            $apiReleaseData->id,
            $apiReleaseData,
            array(
                'id'                       => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'model_game_id'            => array('dataName' => 'model_game_id', 'propelName' => 'ApiGiantBombGameId', 'type' => 'string'),
                'game'                     => array('dataName' => 'game', 'propelName' => null, 'type' => 'ignore'),
                'name'                     => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'platform'                 => array('dataName' => 'platform', 'propelName' => 'ApiGiantBombPlatform', 'type' => 'localFunction', 'functionName' => 'updateLocalPlatform'),
                'developers'               => array('dataName' => 'developers', 'propelName' => 'ApiGiantBombGameReleaseDeveloper', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombCompany', 'updateFunctionName' => 'updateLocalCompany'),
                'publishers'               => array('dataName' => 'publishers', 'propelName' => 'ApiGiantBombGameReleasePublisher', 'type' => 'localLinkFunction', 'linkLocal' => $linkLocal, 'linkRef' => 'ApiGiantBombCompany', 'updateFunctionName' => 'updateLocalCompany'),
                'region'                   => array('dataName' => 'region', 'propelName' => 'ApiGiantBombRegion', 'type' => 'localFunction', 'functionName' => 'updateLocalRegion'),
                'deck'                     => array('dataName' => 'deck', 'propelName' => 'Summary', 'type' => 'string'),
                'description'              => array('dataName' => 'description', 'propelName' => 'Description', 'type' => 'string'),
                'game_rating'              => array('dataName' => 'game_rating', 'propelName' => 'ApiGiantBombGameRating', 'type' => 'localFunction', 'functionName' => 'updateLocalGameRating'),
                'minimum_players'          => array('dataName' => 'minimum_players', 'propelName' => 'MinimumPlayers', 'type' => 'numeric'),
                'maximum_players'          => array('dataName' => 'maximum_players', 'propelName' => 'MaximumPlayers', 'type' => 'numeric'),
                'widescreen_support'       => array('dataName' => 'widescreen_support', 'propelName' => 'WidescreenSupport', 'type' => 'boolean'),
                'product_code_value'       => array('dataName' => 'product_code_value', 'propelName' => 'ProductCodeValue', 'type' => 'string'),
                'release_date'             => array('dataName' => 'release_date', 'propelName' => 'OriginalReleasedAt', 'type' => 'timestamp'),
                'expected_release_day'     => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_month'   => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_quarter' => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'expected_release_year'    => array('dataName' => 'expected_release_day', 'propelName' => 'ExpectedDayReleasedAt', 'type' => 'timestamp'),
                'image'                    => array('dataName' => 'image', 'propelName' => null, 'type' => 'image'),
                'images'                   => array('dataName' => 'images', 'propelName' => 'ApiGiantBombImages', 'type' => 'images', 'link' => 'GameRelease'),
                'api_detail_url'           => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url'          => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
                'date_added'               => array('dataName' => 'date_added', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),
                'date_last_updated'        => array('dataName' => 'date_last_updated', 'propelName' => 'CreatedAt', 'type' => 'timestamp'),

                'product_code_type' => array('dataName' => 'product_code_type', 'propelName' => 'ProductCodeType', 'type' => 'ignore'),
                'resolutions' => array('dataName' => 'resolutions', 'propelName' => 'Resolutions', 'type' => 'ignore'),
                'reviews' => array('dataName' => 'reviews', 'propelName' => 'Reviews', 'type' => 'ignore'),
                'singleplayer_features' => array('dataName' => 'singleplayer_features', 'propelName' => 'SinglePlayerFeatures', 'type' => 'ignore'),
            )
        );
    }

    protected function updateLocalTheme($apiThemeData)
    {
        return $this->updateLocalData(
            'IiMedias\VideoGamesBundle\Model\ApiGiantBombThemeQuery',
            $apiThemeData->id,
            $apiThemeData,
            array(
                'id'              => array('dataName' => 'id', 'propelName' => 'Id', 'type' => 'id'),
                'name'            => array('dataName' => 'name', 'propelName' => 'Name', 'type' => 'string'),
                'api_detail_url'  => array('dataName' => 'api_detail_url', 'propelName' => 'ApiDetailUrl', 'type' => 'string'),
                'site_detail_url' => array('dataName' => 'site_detail_url', 'propelName' => 'SiteDetailUrl', 'type' => 'string'),
            )
        );
    }

    protected function updateLocalData($modelQueryClass, $dataId, $data, $mappingData)
    {
        $modelData = call_user_func($modelQueryClass . '::create')
            ->filterById($dataId)
            ->findOneOrCreate();

        $links = array();

        foreach (get_object_vars($data) as $ojbectPropertyName => $ojbectPropertyValue) {
            if (!isset($mappingData[$ojbectPropertyName])) {
                dump('propriété ' . $modelQueryClass . ' inconnue : ' . $ojbectPropertyName);
                dump($ojbectPropertyValue);
                exit();
            }

            $propertyType       = $mappingData[$ojbectPropertyName]['type'];
            $propertyDataName   = $mappingData[$ojbectPropertyName]['dataName'];
            $propertyPropelName = $mappingData[$ojbectPropertyName]['propelName'];

            switch ($propertyType) {
                case 'id':
                    break;
                case 'null':
                    if (property_exists($data, $propertyDataName) && !is_null($data->$propertyDataName)) {
                        dump('propriété ' . $modelQueryClass . ' : ' . $ojbectPropertyName . ' non nulle');
                        dump($ojbectPropertyValue);
                        exit();
                    }
                    //$nullData  = property_exists($data, $propertyDataName) ? $data->$propertyDataName : call_user_func(array($modelData, 'get' . $propertyPropelName));
                    //$modelData = call_user_func(array($modelData, 'set' . $propertyPropelName), $nullData);
                    break;
                case 'boolean':
                    if (property_exists($data, $propertyDataName) && !is_bool($data->$propertyDataName) && !is_null($data->$propertyDataName)) {
                        dump('propriété ' . $modelQueryClass . ' : ' . $ojbectPropertyName . ' non booléenne');
                        dump($ojbectPropertyValue);
                        exit();
                    }
                    $booleanData = property_exists($data, $propertyDataName) ? $data->$propertyDataName : call_user_func(array($modelData, 'get' . $propertyPropelName));
                    $modelData   = call_user_func(array($modelData, 'set' . $propertyPropelName), $booleanData);
                    break;
                case 'numeric':
                    if (property_exists($data, $propertyDataName) && !is_numeric($data->$propertyDataName) && !is_null($data->$propertyDataName)) {
                        dump('propriété ' . $modelQueryClass . ' : ' . $ojbectPropertyName . ' non numérique');
                        dump($ojbectPropertyValue);
                        exit();
                    }
                    $floatData = property_exists($data, $propertyDataName) ? $data->$propertyDataName : call_user_func(array($modelData, 'get' . $propertyPropelName));
                    $modelData = call_user_func(array($modelData, 'set' . $propertyPropelName), $floatData);
                    break;
                case 'string':
                        $stringData = property_exists($data, $propertyDataName) ? $data->$propertyDataName : call_user_func(array($modelData, 'get' . $propertyPropelName));
                        $modelData = call_user_func(array($modelData, 'set' . $propertyPropelName), $stringData);
                    break;
                case 'array':
                    $delimiter = $mappingData[$ojbectPropertyName]['delimiter'];
                    if (property_exists($data, $propertyDataName) && !is_null($data->$propertyDataName)) {
                        $tmpArray  = str_replace("\r", '', $data->$propertyDataName);
                        $arrayData = explode($delimiter, $tmpArray);
                    }
                    elseif (property_exists($data, $propertyDataName)) {
                        $arrayData = array();
                    }
                    else {
                        $arrayData = call_user_func(array($modelData, 'get' . $propertyPropelName));
                    }
                    $modelData = call_user_func(array($modelData, 'set' . $propertyPropelName), $arrayData);
                    break;
                case 'localFunction':
                    if (isset($mappingData[$ojbectPropertyName]['multiple']) && $mappingData[$ojbectPropertyName]['functionName'] == true) {
                        $localFunctionData = property_exists($data, $propertyDataName) ? true : false;
                        foreach ($data->$propertyDataName as $singleData) {
                            $refId = $mappingData[$ojbectPropertyName]['refId'];
                            $singleData->$refId = $modelData->getId();
                            $singleFunctionData = call_user_func(array($this, $mappingData[$ojbectPropertyName]['functionName']), $singleData);
                        }
                    }
                    else {
                        $localFunctionData = (property_exists($data, $propertyDataName) && is_object($data->$propertyDataName)) ? call_user_func(array($this, $mappingData[$ojbectPropertyName]['functionName']), $data->$propertyDataName) : call_user_func(array($modelData, 'get' . $propertyPropelName));
                        $modelData         = call_user_func(array($modelData, 'set' . $propertyPropelName), $localFunctionData);
                    }
                    break;
                case 'localLinkFunction':
                    $localLinkDatas = property_exists($data, $propertyDataName) ? $data->$propertyDataName : call_user_func(array($modelData, 'get' . $propertyPropelName));
                    if (!is_null($localLinkDatas)) {
                        foreach ($localLinkDatas as $localLinkData) {
                            $classLink = 'IiMedias\\VideoGamesBundle\\Model\\' . $mappingData[$ojbectPropertyName]['propelName'];
                            $localLinkFunctionData = call_user_func(array($this, $mappingData[$ojbectPropertyName]['updateFunctionName']), $localLinkData);

                            if (!$modelData->isNew()) {
                                $localLinkQuery = call_user_func($classLink . 'Query::create');
                                $localLinkQuery = call_user_func(array($localLinkQuery, 'filterBy' . $mappingData[$ojbectPropertyName]['linkLocal']), $modelData);
                                $localLinkQuery = call_user_func(array($localLinkQuery, 'filterBy' . $mappingData[$ojbectPropertyName]['linkRef']), $localLinkFunctionData);
                                $localLink      = $localLinkQuery->findOne();
                            } else {
                                $localLink = null;
                            }

                            if (is_null($localLink)) {
                                $localLink = new $classLink();
                                $localLink = call_user_func(array($localLink, 'set' . $mappingData[$ojbectPropertyName]['linkRef']), $localLinkFunctionData);
                                $modelData = call_user_func(array($modelData, 'add' . $mappingData[$ojbectPropertyName]['propelName']), $localLink);
                            }
                        }
                    }
                    break;
                case 'timestamp':
                    if (property_exists($data, $propertyDataName) && !is_null($data->$propertyDataName) && is_string($data->$propertyDataName)) {
                        $timestampData = property_exists($data, $propertyDataName) ? new DateTime($data->$propertyDataName) : call_user_func(array($modelData, 'get' . $propertyPropelName));
                    }
                    elseif (property_exists($data, $propertyDataName)) {
                        $timestampData = $data->$propertyDataName;
                    }
                    else {
                        $arrayData = call_user_func(array($modelData, 'get' . $propertyPropelName));
                    }
                    $modelData     = call_user_func(array($modelData, 'set' . $propertyPropelName), $timestampData);
                    break;
                case 'image':
                    $imageIconData   = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'icon_url')) ? $data->image->icon_url : $modelData->getImageIconUrl();
                    $imageMediumData = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'medium_url')) ? $data->image->medium_url : $modelData->getImageMediumUrl();
                    $imageScreenData = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'screen_url')) ? $data->image->screen_url : $modelData->getImageScreenUrl();
                    $imageSmallData  = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'small_url')) ? $data->image->small_url : $modelData->getImageSmallUrl();
                    $imageSuperData  = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'super_url')) ? $data->image->super_url : $modelData->getImageSuperUrl();
                    $imageThumbData  = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'thumb_url')) ? $data->image->thumb_url : $modelData->getImageThumbUrl();
                    $imageTinyData   = (property_exists($data, 'image') && !is_null($data->image) && property_exists($data->image, 'tiny_url')) ? $data->image->tiny_url : $modelData->getImageTinyUrl();
                    $modelData = $modelData
                        ->setImageIconUrl($imageIconData)
                        ->setImageMediumUrl($imageMediumData)
                        ->setImageScreenUrl($imageScreenData)
                        ->setImageSmallUrl($imageSmallData)
                        ->setImageSuperUrl($imageSuperData)
                        ->setImageThumbUrl($imageThumbData)
                        ->setImageTinyUrl($imageTinyData)
                    ;
                    break;
                case 'images':
                    foreach ($data->images as $dataImage) {
                        $image = ApiGiantBombImageQuery::create();
                        $image = call_user_func(array($image, 'filterByApiGiantBomb' . $mappingData[$ojbectPropertyName]['link']), $modelData);
                        $image = $image
                            ->filterByImageIconUrl($dataImage->icon_url)
                            ->findOne();
                        if (is_null($image)) {
                            $image = new ApiGiantBombImage();
                            $modelData->addApiGiantBombImage($image);
                        }
                        $image
                            ->setImageIconUrl($dataImage->icon_url)
                            ->setImageMediumUrl($dataImage->medium_url)
                            ->setImageScreenUrl($dataImage->screen_url)
                            ->setImageSmallUrl($dataImage->small_url)
                            ->setImageSuperUrl($dataImage->super_url)
                            ->setImageThumbUrl($dataImage->thumb_url)
                            ->setImageTinyUrl($dataImage->tiny_url)
                            ->setTags(explode(', ', $dataImage->tags));
                    }
                    break;
                case 'videos':
//                    if (count($data->videos) > 0) {
//                        dump('attention partie videos pas faite');
//                        dump($data->videos);
//                        exit();
//                    }
                    break;
                case 'ignore':
                    break;
                default:
                    dump('type ' . $modelQueryClass . ' inconnu : ' . $propertyType);
                    exit();
            }
        }

        $modelData->save();

        return $modelData;
    }
}