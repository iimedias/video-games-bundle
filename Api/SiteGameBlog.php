<?php

namespace IiMedias\VideoGamesBundle\Api;

use \DateTime;
use IiMedias\AdminBundle\Singleton\AppConfigSingleton;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery;
use \Exception;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatformQuery;

class SiteGameBlog extends SiteBasic
{
    public function __construct($output)
    {
        $this->output = $output;
    }

    protected function loadUrl($url, $parameters = array(), $headers = array(), $baseUri = 'http://www.gameblog.fr')
    {
        if (strstr($url, $baseUri) !== false) {
            $url = substr($url, strlen($baseUri));
        }
        if (!is_array($parameters)) {
            $parameters = array();
        }
        if (!is_array($headers)) {
            $headers = array();
        }

        $response = parent::loadUrl($url, $parameters, $headers, $baseUri . '/');
        return $response;
    }

    public function game($game)
    {
        if (is_int($game)) {
            $game = SiteGameBlogGameQuery::create()
                ->filterById($game)
                ->findOne()
            ;
        }

        $response = $this->loadUrl($game->getSiteDetailUrl());

        dump($game);
        dump($game->getSiteDetailUrl());
    }

    public function games($offset = 'last')
    {
        $config   = AppConfigSingleton::getInstance();
        $offset   = ($offset === 'last') ? $config->getValue('siteGameBlogGamesLastOffset', 1) : $offset;
        $response = $this->loadUrl('/jeux.php', array('page' => $offset));

        $lastPageA = $response->find('#listeLettresJeux ul li a', -1);
        $lastPage  = intval(substr($lastPageA->href, 6));
        dump($lastPage);

        $gameElements = $response->find('table.jeux tr');
        foreach ($gameElements as $gameElement) {
            if (count($gameElement->find('th')) > 0) {
                continue;
            }
            
            $siteUrl     = $gameElement->find('a', 0)->href;
            preg_match('#/jeux/([0-9]*)_[a-z0-9-]*#', $siteUrl, $idMatches);
            $id          = $idMatches[1];
            $name        = $gameElement->find('a', 0)->plaintext;
            $platforms   = $gameElement->find('span.plateforme');
            
            $name = str_replace("\x8C", 'Œ', $name);
            $name = str_replace("\x92", '\'', $name);
            $name = str_replace("\x96", '-', $name);
            $name = str_replace("\xA0", ' ', $name);
            $name = str_replace("\xB0", '°', $name);
            $name = str_replace("\xB2", '²', $name);
            $name = str_replace("\xB3", '³', $name);
            $name = str_replace("\xC2", 'Â', $name);
            $name = str_replace("\xC3\xA9", 'É', $name);
            $name = str_replace("\xC3\xB4", 'ô', $name);
            $name = str_replace("\xC5'", 'Œ', $name);
            $name = str_replace("\xC9", 'É', $name);
            $name = str_replace("\xCA", 'Ê', $name);
            $name = str_replace("\xD4", 'Ô', $name);
            $name = str_replace("\xD8", 'Ø', $name);
            $name = str_replace("\xE0", 'à', $name);
            $name = str_replace("\xE2", 'â', $name);
            $name = str_replace("\xE4", 'ä', $name);
            $name = str_replace("\xE6", 'æ', $name);
            $name = str_replace("\xE7", 'ç', $name);
            $name = str_replace("\xE8", 'è', $name);
            $name = str_replace("\xE9", 'é', $name);
            $name = str_replace("\xEA", 'ê', $name);
            $name = str_replace("\xEB", 'ë', $name);
            $name = str_replace("\xEE", 'î', $name);
            $name = str_replace("\xEF", 'ï', $name);
            $name = str_replace("\xF1", 'ñ', $name);
            $name = str_replace("\xF3", 'ó', $name);
            $name = str_replace("\xF4", 'ô', $name);
            $name = str_replace("\xF6", 'ö', $name);
            $name = str_replace("\xF9", 'ù', $name);
            $name = str_replace("\xFB", 'û', $name);
            $name = str_replace("\xFC", 'ü', $name);

            $platformsList = array();
            foreach ($platforms as $platform) {
                $platformsList[] = trim($platform->plaintext);
            }

            $siteGameBlogGame = SiteGameBlogGameQuery::create()
                ->filterById($id)
                ->findOneOrCreate();

            $this->writeln('[GAMEBLOG][JEU] ' . $name . ' {' . implode('}{', $platformsList) . '} [' . $siteGameBlogGame->getId() . ']');

            try {
                $siteGameBlogGame
                    ->setName($name)
                    ->setSiteDetailUrl('http://www.gameblog.fr' . $siteUrl)
                    ->save();
            }
            catch (\Exception $e) {
                $siteGameBlogGame
                    ->setName('')
                    ->setSiteDetailUrl('http://www.gameblog.fr' . $siteUrl)
                    ->save();
            }

            foreach ($platforms as $platform) {
                $siteGameBlogPlatform = SiteGameBlogPlatformQuery::create()
                    ->filterByAbbr(trim($platform->plaintext))
                    ->findOneOrCreate();
                $siteGameBlogPlatform->save();
                
                $siteGameBlogRelease = SiteGameBlogGameReleaseQuery::create()
                    ->filterBySiteGameBlogGame($siteGameBlogGame)
                    ->filterBySiteGameBlogPlatform($siteGameBlogPlatform)
                    ->findOneOrCreate();
                
                $siteGameBlogRelease->save();
            }
        }

        $config->setValue('siteGameBlogGamesLastOffset', (($offset == $lastPage) ? 1 : $offset + 1));
    }
}