<?php

namespace IiMedias\VideoGamesBundle\Api;

use IiMedias\VideoGamesBundle\Model\ApiCountQuery;

class ApiBasicIgdb extends ApiBasic
{
    protected function loadUrl($url, $parameters = array(), $headers = array())
    {
        $igdbApiCount = ApiCountQuery::create()
            ->filterByKey('igdb')
            ->findOne()
        ;
        if (is_null($igdbApiCount) || is_null($igdbApiCount->getKey()) || trim($igdbApiCount->getKey()) == '' || $igdbApiCount->getRemainingQueryNumber() <= 0) {
            return null;
        }
        $url      = 'https://igdbcom-internet-game-database-v1.p.mashape.com' . $url;
        if (!is_array($headers)) {
            $headers = array();
        }
        if (!isset($headers['X-Mashape-Key'])) {
            $headers['X-Mashape-Key'] = $igdbApiCount->getValue();
        }
        $igdbApiCount
            ->setRemainingQueryNumber($igdbApiCount->getRemainingQueryNumber() - 1)
            ->save()
        ;

        $response = parent::loadUrl($url, $parameters, $headers);
        return $response;
    }
}