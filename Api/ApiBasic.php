<?php

namespace IiMedias\VideoGamesBundle\Api;

use Unirest\Request as UniRequest;

class ApiBasic
{
    protected function loadUrl($url, $parameters = array(), $headers = array())
    {
        if (is_array($parameters) && count($parameters) > 0) {
            $count = 0;
            foreach ($parameters as $parameterKey => $parameterValue) {
                $url .= (($count > 0) ? '&' : '?') . $parameterKey . '=' . $parameterValue;
                $count++;
            }
        }

//        dump($url);
//        exit();
        $response = UniRequest::get($url, $headers);
        return $response;
    }
}