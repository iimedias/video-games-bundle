<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Api\ApiGiantBomb;
use IiMedias\VideoGamesBundle\Api\SiteJeuxVideoCom;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use IiMedias\StaffBundle\Form\Type\StaffGroupType;
use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use IiMedias\StaffBundle\Form\Type\StaffElementType;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminJeuxVideoComController extends Controller
{
    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/jeuxvideocom", name="iimedias_videogames_adminjeuxvideocom_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $siteJeuxVideoComGames = SiteJeuxVideoComGameQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:AdminJeuxVideoCom:index.html.twig', array(
                'currentNav'            => 'videogames',
                'currentApiTab'         => 'jeuxvideocom',
                'currentSubTab'         => 'games',
                'siteJeuxVideoComGames' => $siteJeuxVideoComGames,
        ));
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/jeuxvideocom/{siteJeuxVideoComGameId}", name="iimedias_videogames_adminjeuxvideocom_game", requirements={"_locale"="\w{2}", "siteJeuxVideoComGameId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("siteJeuxVideoComGame", class="IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame", options={"mapping"={"siteJeuxVideoComGameId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function game(SiteJeuxVideoComGame $siteJeuxVideoComGame)
    {
        if (is_null($siteJeuxVideoComGame->getRefreshedAt()) || $siteJeuxVideoComGame->getRefreshedAt() < new DateTime('today')) {
            $siteJeuxVideoCom = new SiteJeuxVideoCom();
            $siteJeuxVideoComGame = $siteJeuxVideoCom->game($siteJeuxVideoComGame);
//            foreach ($apiGiantBombGame->getApiGiantBombGameReleases() as $apiGiantBombGameRelease) {
//                $apiGiantBomb->release($apiGiantBombGameRelease);
//            }
//            $apiGiantBombGame
//                ->setRefreshedAt(new DateTime('now'))
//                ->save();
        }

        return $this->render('IiMediasVideoGamesBundle:AdminGiantBomb:game.html.twig', array(
            'currentNav'       => 'videogames',
            'currentApiTab'    => 'giantbomb',
            'currentSubTab'    => 'games',
            'apiGiantBombGame' => $apiGiantBombGame,
        ));
    }
}
