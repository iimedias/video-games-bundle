<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Form\Type\GameReleaseType;
use IiMedias\VideoGamesBundle\Form\Type\PlatformType;
use IiMedias\VideoGamesBundle\Model\GameRelease;
use IiMedias\VideoGamesBundle\Model\GameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Platform;
use IiMedias\VideoGamesBundle\Model\PlatformQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use \DateTime;
use \Exception;
use Propel\Runtime\ActiveQuery\Criteria;
use IiMedias\VideoGamesBundle\Form\Type\GameType;
use IiMedias\VideoGamesBundle\Model\Game;
use IiMedias\VideoGamesBundle\Model\GameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminController extends Controller
{
    /**
     * Liste des jeux
     *
     * @access public
     * @since 1.0.0 28/10/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames", name="iimedias_videogames_admin_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $games = GameQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:Admin:index.html.twig', [
            'currentNav'    => 'videogames',
            'currentSubTab' => 'games',
            'games'         => $games,
        ]);
    }

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param Game $game
     * @Route("/admin/{_locale}/videogame/{gameId}", name="iimedias_videogames_admin_game", requirements={"_locale"="\w{2}", "gameId"="\d+"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("game", class="IiMedias\VideoGamesBundle\Model\Game", options={"mapping"={"gameId": "id"}})
     * @return void
     */
    public function game(Game $game)
    {
        return $this->render('IiMediasVideoGamesBundle:Admin:game.html.twig', [
            'currentNav'    => 'videogames',
            'currentApiTab' => 'local',
            'game'          => $game,
        ]);
    }

    /**
     * Formulaire des jeux
     *
     * @access public
     * @since 1.0.0 18/01/2017 Création -- sebii
     * @param Request $request
     * @Route("/admin/{_locale}/videogames/add", name="iimedias_videogames_admin_gameadd", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "gameId"="0"})
     * @Route("/admin/{_locale}/videogame/{gameId}/edit", name="iimedias_videogames_admin_gameedit", requirements={"_locale"="\w{2}", "gameId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function gameForm(Request $request, $_locale, $formMode, $gameId)
    {
        switch ($formMode) {
            case 'edit':
                $game        = GameQuery::getOneById($gameId);
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_gameedit', ['_locale' => $_locale, 'formMode' => $formMode, 'gameId' => $gameId]);
                if (is_null($game)) {
                    throw new Exception('This Video Game #' . $gameId . ' does not exists.');
                }
                break;
            case 'add':
            default:
                $game        = new Game();
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_gameadd', ['_locale' => $_locale]);
        }
        $form = $this->createForm(GameType::class, $game, ['action' => $actionRoute]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if (!is_null($game->getBoxart())) {
                dump($game->getBoxart());
                exit();
            }
            $game->save();
            return $this->redirectToRoute('iimedias_videogames_admin_game', ['gameId' => $game->getId()]);
        }

        return $this->render('IiMediasVideoGamesBundle:Admin:gameForm.html.twig', [
            'currentNav'    => 'videogames',
            'currentApiTab' => 'local',
            'form'          => $form->createView(),
            'formMode'      => $formMode,
            'game'          => $game,
        ]);
    }

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param Game $game
     * @Route("/admin/{_locale}/videogame/{gameId}/{releaseId}", name="iimedias_videogames_admin_release", requirements={"_locale"="\w{2}", "gameId"="\d+", "releaseId"="\d+"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("game", class="IiMedias\VideoGamesBundle\Model\Game", options={"mapping"={"gameId": "id"}})
     * @ParamConverter("release", class="IiMedias\VideoGamesBundle\Model\GameRelease", options={"mapping"={"gameId": "gameId", "releaseId": "id"}})
     * @return void
     */
    public function release(Game $game, GameRelease $release)
    {
        return $this->redirectToRoute('iimedias_videogames_admin_game', ['gameId' => $game->getId()]);
    }

    /**
     * Formulaire des sorties
     *
     * @access public
     * @since 1.0.0 06/12/2017 Création -- sebii
     * @param Request $request
     * @Route("/admin/{_locale}/videogame/{gameId}/release/add", name="iimedias_videogames_admin_releaseadd", requirements={"_locale"="\w{2}", "gameId"="\d+"}, defaults={"_locale"="fr", "formMode"="add", "releaseId"="0"})
     * @Route("/admin/{_locale}/videogame/{gameId}/realese/{releaseId}/edit", name="iimedias_videogames_admin_releaseedit", requirements={"_locale"="\w{2}", "gameId"="\d+", "releaseId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @Method({"GET", "POST"})
     * @ParamConverter("game", class="IiMedias\VideoGamesBundle\Model\Game", options={"mapping"={"gameId": "id"}})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function releaseForm(Request $request, $_locale, Game $game, $formMode, $releaseId)
    {
        switch ($formMode) {
            case 'edit':
                $release     = GameReleaseQuery::getOneByIdAndGame($releaseId, $game);
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_releaseedit', ['_locale' => $_locale, 'formMode' => $formMode, 'releaseId' => $releaseId, 'gameId' => $game->getId()]);
                if (is_null($release)) {
                    throw new Exception('This Release #' . $releaseId . ' of Video Game #' . $game->getId() . ' does not exists.');
                }
                break;
            case 'add':
            default:
                $release     = new GameRelease();
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_releaseadd', ['_locale' => $_locale, 'formMode' => $formMode, 'gameId' => $game->getId()]);
                $release
                    ->setGame($game)
                    ->setName($game->getName())
                ;
        }
        $form = $this->createForm(GameReleaseType::class, $release, ['action' => $actionRoute]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if (!is_null($game->getBoxart())) {
                dump($release->getBoxart());
                exit();
            }
            $release->save();
            return $this->redirectToRoute('iimedias_videogames_admin_release', ['gameId' => $game->getId(), 'releaseId' => $release->getId()]);
        }

        return $this->render('IiMediasVideoGamesBundle:Admin:releaseForm.html.twig', [
            'currentNav'    => 'videogames',
            'currentApiTab' => 'local',
            'form'          => $form->createView(),
            'formMode'      => $formMode,
            'game'          => $game,
            'release'       => $release,
        ]);
    }

    /**
     * Liste des plateformes
     *
     * @access public
     * @since 1.0.0 28/10/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/platforms", name="iimedias_videogames_admin_platforms", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function platforms()
    {
        $platforms = PlatformQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:Admin:platforms.html.twig', [
            'currentNav'    => 'videogames',
            'currentSubTab' => 'platforms',
            'games'         => $platforms,
        ]);
    }

    public function platform(Platform $platform)
    {
        return $this->render('IiMediasVideoGamesBundle:Admin:platform.html.twig', [
            'currentNav'    => 'videogames',
            'currentSubTab' => 'platforms',
        ]);
    }

    /**
     * Formulaire des plateformes
     *
     * @access public
     * @since 1.0.0 18/01/2017 Création -- sebii
     * @param Request $request
     * @Route("/admin/{_locale}/videogames/platforms/add", name="iimedias_videogames_admin_platformadd", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "platformId"="0"})
     * @Route("/admin/{_locale}/videogames/platform/{platformId}/edit", name="iimedias_videogames_admin_platformedit", requirements={"_locale"="\w{2}", "platformId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function platformForm(Request $request, $_locale, $formMode, $platformId)
    {
        switch ($formMode) {
            case 'edit':
                $game        = PlatformQuery::getOneById($platformId);
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_platformedit', ['_locale' => $_locale, 'formMode' => $formMode, 'gameId' => $platformId]);
                if (is_null($game)) {
                    throw new Exception('This Platform #' . $platformId . ' does not exists');
                }
                break;
            case 'add':
            default:
                $game        = new Platform();
                $actionRoute = $this->generateUrl('iimedias_videogames_admin_platformadd', ['_locale' => $_locale]);
        }
        $form = $this->createForm(PlatformType::class, $game, ['action' => $actionRoute]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $game->save();
            return $this->redirectToRoute('iimedias_videogames_admin_platforms');
        }

        return $this->render('IiMediasVideoGamesBundle:Admin:platformForm.html.twig', [
            'currentNav'    => 'videogames',
            'currentSubTab' => 'platforms',
            'form'          => $form->createView(),
            'formMode'      => $formMode,
        ]);
    }

    /**
     * Recherche les jeux dans l'API ou le Site demandé
     *
     * @access public
     * @since 1.0.0 10/10/2017 Création -- sebii
     * @param Request $request
     * @Route("/admin/{_locale}/videogames/game/apisearch.json", name="iimedias_videogames_admin_gameapisearch", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @param Request $request
     */
    public function gameApiSearch(Request $request)
    {
        $searchTerms  = $request->request->get('search', '');
        $jsonResponse = [
            'search_terms' => $searchTerms,
            'success'      => false,
            'error'        => null,
            'api_games'    => [],
            'count'        => null,
        ];
        switch ($request->request->get('gameApiName')) {
            case 'gameblog':
                $apiGames = SiteGameBlogGameQuery::create()
                    ->filterByName('%' . $searchTerms . '%', Criteria::LIKE)
                    ->orderByName(Criteria::ASC)
                    ->find();
                break;
            case 'gamekult':
                $apiGames = SiteGameKultGameQuery::create()
                    ->filterByName('%' . $searchTerms . '%', Criteria::LIKE)
                    ->orderByName(Criteria::ASC)
                    ->find();
                break;
            case 'giantbomb':
                $apiGames = ApiGiantBombGame::create()
                    ->filterByName('%' . $searchTerms . '%', Criteria::LIKE)
                    ->orderByName(Criteria::ASC)
                    ->find();
                break;
            case 'jeuxvideocom':
                $apiGames = SiteJeuxVideoComGameQuery::create()
                    ->filterByName('%' . $searchTerms . '%', Criteria::LIKE)
                    ->orderByName(Criteria::ASC)
                    ->find();
                break;
            default:
                $jsonResponse['error'] = 'API inconnue';
                return new JsonResponse($jsonResponse);
        }
        $jsonResponse['count'] = $apiGames->count();
        foreach ($apiGames as $apiGame) {
            $jsonResponse['api_games'][] = [
                'id'   => $apiGame->getId(),
                'name' => $apiGame->getName(),
            ];
        }
        return new JsonResponse($jsonResponse);
    }
}
