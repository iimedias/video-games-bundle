<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Model\GameQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class VideoGameController extends Controller
{
    /**
     * Index / Liste des projets en cours
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @Route("/{_locale}/videogames", name="iimedias_videogames_videogame_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $games = GameQuery::create()
            ->find();

        exit();

        return $this->render('IiMediasVideoGamesBundle:VideoGame:index.html.twig', array(
                'games' => $games,
        ));
    }
}
