<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Api\ApiGiantBomb;
use IiMedias\VideoGamesBundle\Api\SiteGameKult;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminGameKultController extends Controller
{
    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/gamekult", name="iimedias_videogames_admingamekult_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $siteGameKultGames = SiteGameKultGameQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:AdminGameKult:index.html.twig', array(
                'currentNav'        => 'videogames',
                'currentApiTab'     => 'gamekult',
                'currentSubTab'     => 'games',
                'siteGameKultGames' => $siteGameKultGames,
        ));
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 20/01/2017 Création -- sebii
     * @Route("/admin/{_locale}/videogames/gamekult/{siteGameKultGameId}", name="iimedias_videogames_admingamekult_game", requirements={"_locale"="\w{2}", "siteGameKultGameId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("siteGameKultGame", class="IiMedias\VideoGamesBundle\Model\SiteGameKultGame", options={"mapping"={"siteGameKultGameId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function game(SiteGameKultGame $siteGameKultGame)
    {
        if (is_null($siteGameKultGame->getRefreshedAt()) || $siteGameKultGame->getRefreshedAt() < new DateTime('today')) {
            $siteGameKult     = new SiteGameKult();
            $siteGameKultGame = $siteGameKult->game($siteGameKultGame);
            exit();
//            foreach ($apiGiantBombGame->getApiGiantBombGameReleases() as $apiGiantBombGameRelease) {
//                $apiGiantBomb->release($apiGiantBombGameRelease);
//            }
//            $apiGiantBombGame
//                ->setRefreshedAt(new DateTime('now'))
//                ->save();
        }

        return $this->render('IiMediasVideoGamesBundle:AdminGameKult:game.html.twig', array(
            'currentNav'       => 'videogames',
            'currentApiTab'    => 'giantbomb',
            'currentSubTab'    => 'games',
            'siteGameKultGame' => $siteGameKultGame,
        ));
    }
}
