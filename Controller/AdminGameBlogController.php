<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Api\SiteGameBlog;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminGameBlogController extends Controller
{
    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/gameblog", name="iimedias_videogames_admingameblog_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $siteGameBlogGames = SiteGameBlogGameQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:AdminGameBlog:index.html.twig', array(
                'currentNav'        => 'videogames',
                'currentApiTab'     => 'gameblog',
                'currentSubTab'     => 'games',
                'siteGameBlogGames' => $siteGameBlogGames,
        ));
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 24/01/2017 Création -- sebii
     * @Route("/admin/{_locale}/videogames/gameblog/{siteGameBlogGameId}", name="iimedias_videogames_admingameblog_game", requirements={"_locale"="\w{2}", "siteGameBlogGameId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("siteGameBlogGame", class="IiMedias\VideoGamesBundle\Model\SiteGameBlogGame", options={"mapping"={"siteGameBlogGameId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function game(SiteGameBlogGame $siteGameBlogGame)
    {
        if (is_null($siteGameBlogGame->getRefreshedAt()) || $siteGameBlogGame->getRefreshedAt() < new DateTime('today')) {
            $siteGameBlog     = new SiteGameBlog();
            $siteGameBlogGame = $siteGameBlog->game($siteGameBlogGame);
            exit();
//            foreach ($apiGiantBombGame->getApiGiantBombGameReleases() as $apiGiantBombGameRelease) {
//                $apiGiantBomb->release($apiGiantBombGameRelease);
//            }
//            $apiGiantBombGame
//                ->setRefreshedAt(new DateTime('now'))
//                ->save();
        }

        return $this->render('IiMediasVideoGamesBundle:AdminGiantBomb:game.html.twig', array(
            'currentNav'       => 'videogames',
            'currentApiTab'    => 'giantbomb',
            'currentSubTab'    => 'games',
            'apiGiantBombGame' => $apiGiantBombGame,
        ));
    }
}
