<?php

namespace IiMedias\VideoGamesBundle\Controller;

use IiMedias\VideoGamesBundle\Api\ApiGiantBomb;
use IiMedias\VideoGamesBundle\Form\Type\GameLinkGiantBombType;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\Game;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use IiMedias\StaffBundle\Model\StaffGroup;
use IiMedias\StaffBundle\Model\StaffGroupQuery;
use IiMedias\StaffBundle\Form\Type\StaffGroupType;
use IiMedias\StaffBundle\Model\StaffElement;
use IiMedias\StaffBundle\Model\StaffElementQuery;
use IiMedias\StaffBundle\Form\Type\StaffElementType;
use \DateTime;

/**
 * Class AdminController
 *
 * @package IiMedias\AdminBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class AdminGiantBombController extends Controller
{
    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/giantbomb", name="iimedias_videogames_admingiantbomb_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $apiGiantBombGames = ApiGiantBombGameQuery::create()
            ->find();

        return $this->render('IiMediasVideoGamesBundle:AdminGiantBomb:index.html.twig', array(
                'currentNav'        => 'videogames',
                'currentApiTab'     => 'giantbomb',
                'currentSubTab'     => 'games',
                'apiGiantBombGames' => $apiGiantBombGames,
        ));
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/giantbomb/{apiGiantBombGameId}", name="iimedias_videogames_admingiantbomb_game", requirements={"_locale"="\w{2}", "apiGiantBombGameId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("apiGiantBombGame", class="IiMedias\VideoGamesBundle\Model\ApiGiantBombGame", options={"mapping"={"apiGiantBombGameId": "id"}})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function game(ApiGiantBombGame $apiGiantBombGame)
    {
        if (is_null($apiGiantBombGame->getRefreshedAt()) || $apiGiantBombGame->getRefreshedAt() < new DateTime('today')) {
            $apiGiantBomb = new ApiGiantBomb();
            $apiGiantBombGame = $apiGiantBomb->game($apiGiantBombGame);
            foreach ($apiGiantBombGame->getApiGiantBombGameReleases() as $apiGiantBombGameRelease) {
                $apiGiantBomb->release($apiGiantBombGameRelease);
            }
            $apiGiantBombGame
                ->setRefreshedAt(new DateTime('now'))
                ->save();
        }

        return $this->render('IiMediasVideoGamesBundle:AdminGiantBomb:game.html.twig', array(
            'currentNav'       => 'videogames',
            'currentApiTab'    => 'giantbomb',
            'currentSubTab'    => 'games',
            'apiGiantBombGame' => $apiGiantBombGame,
        ));
    }

    /**
     * Liste des groupes de staffs
     *
     * @access public
     * @since 1.0.0 28/20/2016 Création -- sebii
     * @Route("/admin/{_locale}/videogames/giantbomb/{apiGiantBombGameId}/link", name="iimedias_videogames_admingiantbomb_gamelink", requirements={"_locale"="\w{2}", "apiGiantBombGameId"="\d+"}, defaults={"_locale"="fr"})
     * @ParamConverter("apiGiantBombGame", class="IiMedias\VideoGamesBundle\Model\ApiGiantBombGame", options={"mapping"={"apiGiantBombGameId": "id"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function gameLink(Request $request, ApiGiantBombGame $apiGiantBombGame)
    {
        $form = $this->createForm(GameLinkGiantBombType::class, $apiGiantBombGame);

        if ($request->isMethod('POST')) {
            if ($form->handleRequest($request) && $form->isValid()) {
                if (!is_null($apiGiantBombGame->getGamelink()) && trim($apiGiantBombGame->getGamelink()) != '') {
                    $game = new Game();
                    $game
                        ->setName($apiGiantBombGame->getGamelink())
                        ->save();
                    $apiGiantBombGame
                        ->setGame($game);
                }
                $apiGiantBombGame
                    ->save();
                return $this->redirect($this->generateUrl('iimedias_videogames_admingiantbomb_game', array('apiGiantBombGameId' => $apiGiantBombGame->getId())));
            }
        }

        return $this->render('IiMediasVideoGamesBundle:AdminGiantBomb:gameLink.html.twig', array(
            'currentNav'       => 'videogames',
            'currentApiTab'    => 'giantbomb',
            'currentSubTab'    => 'games',
            'apiGiantBombGame' => $apiGiantBombGame,
            'form'             => $form->createView(),
        ));
    }
}
