<?php

namespace IiMedias\VideoGamesBundle\Command;

use IiMedias\VideoGamesBundle\Api\SiteGameBlog;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class LoadGameBlogCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:videogames:load:gameblog')
            ->setDescription('Import de la liste des jeux de GameBlog')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Forcing')
            ->addOption('games', 'G', InputOption::VALUE_NONE, 'Scan de la liste des jeux')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('force')) {
            throw new \Exception('Ce site est inutilisable à cause de CloudFare, voir la solution ici : https://stackoverflow.com/questions/17182553/sites-not-accepting-wget-user-agent-header');
        }

        $gameblogSite = new SiteGameBlog($output);

        if ($input->getOption('games')) {
            $gameblogSite->games();
        }
    }
}
