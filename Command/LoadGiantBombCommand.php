<?php

namespace IiMedias\VideoGamesBundle\Command;

use IiMedias\VideoGamesBundle\Api\ApiGiantBomb;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class LoadGiantBombCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:videogames:load:giantbomb')
            ->setDescription('...')
            ->addOption('games', 'G', InputOption::VALUE_NONE, 'Scan de la liste des jeux')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $current = new DateTime('now');
        $current = $current->modify('-' . ($current->getTimestamp() % 60) . ' second');
        $secMinute = 60;
        $secHour   = $secMinute * 60;
        $secDay    = $secHour * 24;
        $secWeek   = $secDay * 7;

        $giantbombApi = new ApiGiantBomb();

        // Exécution les 1er et 15 du mois à 2h du matin pour les consoles
        if (in_array(intval($current->format('d')), array(1, 15)) && intval($current->format('H')) == 2 && intval($current->format('i')) == 2) {
            $giantbombApi->platforms();
        }

        if ($input->getOption('games')) {
            $giantbombApi->games();
        }

//        $giantbombApi->accessories();
//        $giantbombApi->characters();
//        $giantbombApi->franchises();
//        $giantbombApi->companies();
//        $giantbombApi->concepts();
//        $giantbombApi->gameRatings();
//        $giantbombApi->genres();
//        $giantbombApi->locations();
//        $giantbombApi->objects();
//        $giantbombApi->people();

//        $giantbombApi->ratingBoards();
//        $giantbombApi->regions();
//        $giantbombApi->themes();
    }
}
