<?php

namespace IiMedias\VideoGamesBundle\Command;

use IiMedias\VideoGamesBundle\Api\ApiIgdbCompany;
use IiMedias\VideoGamesBundle\Api\ApiIgdbPlatform;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadIgdbCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:videogames:load:igdb')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $apiIgdbPlatform = new ApiIgdbPlatform();
//        $apiIgdbPlatform->refreshAll();

        $apiIgdbCompany = new ApiIgdbCompany();
        $apiIgdbCompany->refreshAll();
    }

}
