<?php

namespace IiMedias\VideoGamesBundle\Command;

use IiMedias\VideoGamesBundle\Api\SiteGameKult;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class LoadGameKultCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:videogames:load:gamekult')
            ->setDescription('Import de la liste des jeux de GameKult')
            ->addOption('games', 'G', InputOption::VALUE_NONE, 'Scan de la liste des jeux')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $gamekultSite = new SiteGameKult($output);
        
        if ($input->getOption('games')) {
            $gamekultSite->games();
        }

//        if ($input->getOption('year') === true) {
//            $output->writeln('Janvier ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-01-01')));
//            $output->writeln('Février ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-02-01')));
//            $output->writeln('Mars ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-03-01')));
//            $output->writeln('Avril ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-04-01')));
//            $output->writeln('Mai ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-05-01')));
//            $output->writeln('Juin ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-06-01')));
//            $output->writeln('Juillet ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-07-01')));
//            $output->writeln('Août ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-08-01')));
//            $output->writeln('Septembre ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-09-01')));
//            $output->writeln('Octobre ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-10-01')));
//            $output->writeln('Novembre ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-11-01')));
//            $output->writeln('Décembre ' . $date->format('Y'));
//            $gamekultSite->games(new DateTime($date->format('Y-12-01')));
//        } else {
//            $gamekultSite->games($date);
//        }

//        $giantbombApi->accessories();
//        $giantbombApi->characters();
//        $giantbombApi->franchises();
//        $giantbombApi->companies();
//        $giantbombApi->concepts();
//        $giantbombApi->gameRatings();
//        $giantbombApi->genres();
//        $giantbombApi->locations();
//        $giantbombApi->objects();
//        $giantbombApi->people();

//        $giantbombApi->ratingBoards();
//        $giantbombApi->regions();
//        $giantbombApi->themes();
    }
}
