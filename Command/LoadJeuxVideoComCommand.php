<?php

namespace IiMedias\VideoGamesBundle\Command;

use IiMedias\VideoGamesBundle\Api\SiteJeuxVideoCom;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class LoadJeuxVideoComCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('iimedias:videogames:load:jeuxvideocom')
            ->setDescription('Chargement des données depuis le site de JeuxVideo.com')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Forcing')
            ->addOption('games', 'G', InputOption::VALUE_NONE, 'Force à rafraichir le scan de la liste des jeux')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('force')) {
            throw new \Exception('Ce site est inutilisable sur les serveurs OVH');
        }
        
        $jeuxVideoComSite = new SiteJeuxVideoCom($output);
        
        if ($input->getOption('games')) {
            $jeuxVideoComSite->games();
        }

//        $giantbombApi->accessories();
//        $giantbombApi->characters();
//        $giantbombApi->franchises();
//        $giantbombApi->companies();
//        $giantbombApi->concepts();
//        $giantbombApi->gameRatings();
//        $giantbombApi->genres();
//        $giantbombApi->locations();
//        $giantbombApi->objects();
//        $giantbombApi->people();

//        $giantbombApi->ratingBoards();
//        $giantbombApi->regions();
//        $giantbombApi->themes();
    }
}
