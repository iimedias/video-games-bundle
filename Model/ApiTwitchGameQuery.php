<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiTwitchGameQuery as BaseApiTwitchGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_api_twitch_vgatga' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiTwitchGameQuery extends BaseApiTwitchGameQuery
{
    public static function getOneByNameOrCreate($name)
    {
        $twitchGame = self::create()
            ->filterByName($name)
            ->findOne()
        ;

        if (is_null($twitchGame)) {
            $twitchGame = new ApiTwitchGame();
            $twitchGame
                ->setName($name)
                ->save()
            ;
        }

        return $twitchGame;
    }
}
