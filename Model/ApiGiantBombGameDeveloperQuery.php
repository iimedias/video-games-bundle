<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameDeveloperQuery as BaseApiGiantBombGameDeveloperQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_api_giantbomb_game_developer_vgaggd' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiGiantBombGameDeveloperQuery extends BaseApiGiantBombGameDeveloperQuery
{

}
