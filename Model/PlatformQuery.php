<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\PlatformQuery as BasePlatformQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_platform_vgaplf' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PlatformQuery extends BasePlatformQuery
{
    public static function getAll()
    {
        $platforms = self::create()
            ->orderByName();
        return $platforms;
    }
}
