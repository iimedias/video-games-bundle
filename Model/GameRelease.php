<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\GameRelease as BaseGameRelease;

/**
 * Skeleton subclass for representing a row from the 'videogames_game_release_vgagmr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GameRelease extends BaseGameRelease
{
    protected $boxart;

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @return mixed
     */
    public function getBoxart()
    {
        return $this->boxart;
    }

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param $boxart
     * @return GameRelease
     */
    public function setBoxart($boxart)
    {
        $this->boxart = $boxart;
        return $this;
    }
}
