<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\GameQuery as BaseGameQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_game_vgagam' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GameQuery extends BaseGameQuery
{
    /**
     * Récupère un Game via son id
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param integer $gameId
     * @return Game
     */
    public static function getOneById($gameId)
    {
        $game = self::create()
            ->filterById($gameId)
            ->findOne();
        return $game;
    }
}
