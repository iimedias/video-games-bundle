<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\SiteGameBlogGameRelease as BaseSiteGameBlogGameRelease;

/**
 * Skeleton subclass for representing a row from the 'videogames_site_gameblog_release_vgabrl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteGameBlogGameRelease extends BaseSiteGameBlogGameRelease
{

}
