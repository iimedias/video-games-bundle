<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\SiteGameKultPlatformQuery as BaseSiteGameKultPlatformQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_site_gamekult_platform_vgakpl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteGameKultPlatformQuery extends BaseSiteGameKultPlatformQuery
{

}
