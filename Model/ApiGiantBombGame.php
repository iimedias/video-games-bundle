<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGame as BaseApiGiantBombGame;

/**
 * Skeleton subclass for representing a row from the 'videogames_api_giantbomb_game_vgagga' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiGiantBombGame extends BaseApiGiantBombGame
{
    protected $gamelink;

    public function getGamelink()
    {
        return $this->gamelink;
    }

    public function setGamelink($gamelink)
    {
        $this->gamelink = $gamelink;
        return $this;
    }
}
