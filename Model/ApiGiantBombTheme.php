<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombTheme as BaseApiGiantBombTheme;

/**
 * Skeleton subclass for representing a row from the 'videogames_api_giantbomb_theme_vgagth' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiGiantBombTheme extends BaseApiGiantBombTheme
{

}
