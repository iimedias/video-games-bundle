<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatformQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_site_jeuxvideocom_platform_vgajpl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiteJeuxVideoComPlatformTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.SiteJeuxVideoComPlatformTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_site_jeuxvideocom_platform_vgajpl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComPlatform';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.SiteJeuxVideoComPlatform';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the vgajpl_id field
     */
    const COL_VGAJPL_ID = 'videogames_site_jeuxvideocom_platform_vgajpl.vgajpl_id';

    /**
     * the column name for the vgajpl_name field
     */
    const COL_VGAJPL_NAME = 'videogames_site_jeuxvideocom_platform_vgajpl.vgajpl_name';

    /**
     * the column name for the vgajpl_abbr field
     */
    const COL_VGAJPL_ABBR = 'videogames_site_jeuxvideocom_platform_vgajpl.vgajpl_abbr';

    /**
     * the column name for the vgajpl_list_url_abbr field
     */
    const COL_VGAJPL_LIST_URL_ABBR = 'videogames_site_jeuxvideocom_platform_vgajpl.vgajpl_list_url_abbr';

    /**
     * the column name for the vgajpl_refreshed_at field
     */
    const COL_VGAJPL_REFRESHED_AT = 'videogames_site_jeuxvideocom_platform_vgajpl.vgajpl_refreshed_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Abbr', 'ListUrlAbbr', 'RefreshedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'abbr', 'listUrlAbbr', 'refreshedAt', ),
        self::TYPE_COLNAME       => array(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_NAME, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ABBR, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_LIST_URL_ABBR, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_REFRESHED_AT, ),
        self::TYPE_FIELDNAME     => array('vgajpl_id', 'vgajpl_name', 'vgajpl_abbr', 'vgajpl_list_url_abbr', 'vgajpl_refreshed_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Abbr' => 2, 'ListUrlAbbr' => 3, 'RefreshedAt' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'abbr' => 2, 'listUrlAbbr' => 3, 'refreshedAt' => 4, ),
        self::TYPE_COLNAME       => array(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID => 0, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_NAME => 1, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ABBR => 2, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_LIST_URL_ABBR => 3, SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_REFRESHED_AT => 4, ),
        self::TYPE_FIELDNAME     => array('vgajpl_id' => 0, 'vgajpl_name' => 1, 'vgajpl_abbr' => 2, 'vgajpl_list_url_abbr' => 3, 'vgajpl_refreshed_at' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_site_jeuxvideocom_platform_vgajpl');
        $this->setPhpName('SiteJeuxVideoComPlatform');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComPlatform');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('vgajpl_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgajpl_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgajpl_abbr', 'Abbr', 'VARCHAR', false, 255, null);
        $this->addColumn('vgajpl_list_url_abbr', 'ListUrlAbbr', 'VARCHAR', false, 255, null);
        $this->addColumn('vgajpl_refreshed_at', 'RefreshedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SiteGameBlogGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgajrl_vgajpl_id',
    1 => ':vgajpl_id',
  ),
), 'CASCADE', 'CASCADE', 'SiteGameBlogGameReleases', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_site_jeuxvideocom_platform_vgajpl     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteJeuxVideoComGameReleaseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteJeuxVideoComPlatformTableMap::CLASS_DEFAULT : SiteJeuxVideoComPlatformTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteJeuxVideoComPlatform object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteJeuxVideoComPlatformTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteJeuxVideoComPlatformTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteJeuxVideoComPlatformTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteJeuxVideoComPlatformTableMap::OM_CLASS;
            /** @var SiteJeuxVideoComPlatform $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteJeuxVideoComPlatformTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteJeuxVideoComPlatformTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteJeuxVideoComPlatformTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteJeuxVideoComPlatform $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteJeuxVideoComPlatformTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID);
            $criteria->addSelectColumn(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_NAME);
            $criteria->addSelectColumn(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ABBR);
            $criteria->addSelectColumn(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_LIST_URL_ABBR);
            $criteria->addSelectColumn(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_REFRESHED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgajpl_id');
            $criteria->addSelectColumn($alias . '.vgajpl_name');
            $criteria->addSelectColumn($alias . '.vgajpl_abbr');
            $criteria->addSelectColumn($alias . '.vgajpl_list_url_abbr');
            $criteria->addSelectColumn($alias . '.vgajpl_refreshed_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteJeuxVideoComPlatformTableMap::DATABASE_NAME)->getTable(SiteJeuxVideoComPlatformTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteJeuxVideoComPlatformTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteJeuxVideoComPlatformTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteJeuxVideoComPlatformTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteJeuxVideoComPlatform or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteJeuxVideoComPlatform object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComPlatformTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteJeuxVideoComPlatformTableMap::DATABASE_NAME);
            $criteria->add(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteJeuxVideoComPlatformQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteJeuxVideoComPlatformTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteJeuxVideoComPlatformTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_site_jeuxvideocom_platform_vgajpl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteJeuxVideoComPlatformQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteJeuxVideoComPlatform or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteJeuxVideoComPlatform object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComPlatformTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteJeuxVideoComPlatform object
        }

        if ($criteria->containsKey(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID) && $criteria->keyContainsValue(SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteJeuxVideoComPlatformTableMap::COL_VGAJPL_ID.')');
        }


        // Set the correct dbName
        $query = SiteJeuxVideoComPlatformQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteJeuxVideoComPlatformTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteJeuxVideoComPlatformTableMap::buildTableMap();
