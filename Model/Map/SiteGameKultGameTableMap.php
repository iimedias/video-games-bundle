<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\SiteGameKultGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_site_gamekult_game_vgakga' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiteGameKultGameTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.SiteGameKultGameTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_site_gamekult_game_vgakga';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGame';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.SiteGameKultGame';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the vgakga_id field
     */
    const COL_VGAKGA_ID = 'videogames_site_gamekult_game_vgakga.vgakga_id';

    /**
     * the column name for the vgakga_type_id field
     */
    const COL_VGAKGA_TYPE_ID = 'videogames_site_gamekult_game_vgakga.vgakga_type_id';

    /**
     * the column name for the vgakga_gamekult_id field
     */
    const COL_VGAKGA_GAMEKULT_ID = 'videogames_site_gamekult_game_vgakga.vgakga_gamekult_id';

    /**
     * the column name for the vgakga_vgagam_id field
     */
    const COL_VGAKGA_VGAGAM_ID = 'videogames_site_gamekult_game_vgakga.vgakga_vgagam_id';

    /**
     * the column name for the vgakga_name field
     */
    const COL_VGAKGA_NAME = 'videogames_site_gamekult_game_vgakga.vgakga_name';

    /**
     * the column name for the vgakga_aliases field
     */
    const COL_VGAKGA_ALIASES = 'videogames_site_gamekult_game_vgakga.vgakga_aliases';

    /**
     * the column name for the vgakga_original_price field
     */
    const COL_VGAKGA_ORIGINAL_PRICE = 'videogames_site_gamekult_game_vgakga.vgakga_original_price';

    /**
     * the column name for the vgakga_platforms_array field
     */
    const COL_VGAKGA_PLATFORMS_ARRAY = 'videogames_site_gamekult_game_vgakga.vgakga_platforms_array';

    /**
     * the column name for the vgakga_genres_array field
     */
    const COL_VGAKGA_GENRES_ARRAY = 'videogames_site_gamekult_game_vgakga.vgakga_genres_array';

    /**
     * the column name for the vgakga_themes_array field
     */
    const COL_VGAKGA_THEMES_ARRAY = 'videogames_site_gamekult_game_vgakga.vgakga_themes_array';

    /**
     * the column name for the vgakga_companies_array field
     */
    const COL_VGAKGA_COMPANIES_ARRAY = 'videogames_site_gamekult_game_vgakga.vgakga_companies_array';

    /**
     * the column name for the vgakga_franchises_array field
     */
    const COL_VGAKGA_FRANCHISES_ARRAY = 'videogames_site_gamekult_game_vgakga.vgakga_franchises_array';

    /**
     * the column name for the vgakga_fr_released_at field
     */
    const COL_VGAKGA_FR_RELEASED_AT = 'videogames_site_gamekult_game_vgakga.vgakga_fr_released_at';

    /**
     * the column name for the vgakga_us_released_at field
     */
    const COL_VGAKGA_US_RELEASED_AT = 'videogames_site_gamekult_game_vgakga.vgakga_us_released_at';

    /**
     * the column name for the vgakga_jp_released_at field
     */
    const COL_VGAKGA_JP_RELEASED_AT = 'videogames_site_gamekult_game_vgakga.vgakga_jp_released_at';

    /**
     * the column name for the vgakga_image_small_url field
     */
    const COL_VGAKGA_IMAGE_SMALL_URL = 'videogames_site_gamekult_game_vgakga.vgakga_image_small_url';

    /**
     * the column name for the vgakga_site_detail_url field
     */
    const COL_VGAKGA_SITE_DETAIL_URL = 'videogames_site_gamekult_game_vgakga.vgakga_site_detail_url';

    /**
     * the column name for the vgakga_refreshed_at field
     */
    const COL_VGAKGA_REFRESHED_AT = 'videogames_site_gamekult_game_vgakga.vgakga_refreshed_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TypeId', 'GamekultId', 'GameId', 'Name', 'Aliases', 'OriginalPrice', 'PlatformsArray', 'GenresArray', 'ThemesArray', 'CompaniesArray', 'FranchisesArray', 'FrReleasedAt', 'UsReleasedAt', 'JpReleasedAt', 'ImageSmallUrl', 'SiteDetailUrl', 'RefreshedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'typeId', 'gamekultId', 'gameId', 'name', 'aliases', 'originalPrice', 'platformsArray', 'genresArray', 'themesArray', 'companiesArray', 'franchisesArray', 'frReleasedAt', 'usReleasedAt', 'jpReleasedAt', 'imageSmallUrl', 'siteDetailUrl', 'refreshedAt', ),
        self::TYPE_COLNAME       => array(SiteGameKultGameTableMap::COL_VGAKGA_ID, SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID, SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID, SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, SiteGameKultGameTableMap::COL_VGAKGA_NAME, SiteGameKultGameTableMap::COL_VGAKGA_ALIASES, SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE, SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY, SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY, SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY, SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY, SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY, SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT, SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT, SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT, SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL, SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL, SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT, ),
        self::TYPE_FIELDNAME     => array('vgakga_id', 'vgakga_type_id', 'vgakga_gamekult_id', 'vgakga_vgagam_id', 'vgakga_name', 'vgakga_aliases', 'vgakga_original_price', 'vgakga_platforms_array', 'vgakga_genres_array', 'vgakga_themes_array', 'vgakga_companies_array', 'vgakga_franchises_array', 'vgakga_fr_released_at', 'vgakga_us_released_at', 'vgakga_jp_released_at', 'vgakga_image_small_url', 'vgakga_site_detail_url', 'vgakga_refreshed_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TypeId' => 1, 'GamekultId' => 2, 'GameId' => 3, 'Name' => 4, 'Aliases' => 5, 'OriginalPrice' => 6, 'PlatformsArray' => 7, 'GenresArray' => 8, 'ThemesArray' => 9, 'CompaniesArray' => 10, 'FranchisesArray' => 11, 'FrReleasedAt' => 12, 'UsReleasedAt' => 13, 'JpReleasedAt' => 14, 'ImageSmallUrl' => 15, 'SiteDetailUrl' => 16, 'RefreshedAt' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'typeId' => 1, 'gamekultId' => 2, 'gameId' => 3, 'name' => 4, 'aliases' => 5, 'originalPrice' => 6, 'platformsArray' => 7, 'genresArray' => 8, 'themesArray' => 9, 'companiesArray' => 10, 'franchisesArray' => 11, 'frReleasedAt' => 12, 'usReleasedAt' => 13, 'jpReleasedAt' => 14, 'imageSmallUrl' => 15, 'siteDetailUrl' => 16, 'refreshedAt' => 17, ),
        self::TYPE_COLNAME       => array(SiteGameKultGameTableMap::COL_VGAKGA_ID => 0, SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID => 1, SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID => 2, SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID => 3, SiteGameKultGameTableMap::COL_VGAKGA_NAME => 4, SiteGameKultGameTableMap::COL_VGAKGA_ALIASES => 5, SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE => 6, SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY => 7, SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY => 8, SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY => 9, SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY => 10, SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY => 11, SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT => 12, SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT => 13, SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT => 14, SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL => 15, SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL => 16, SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT => 17, ),
        self::TYPE_FIELDNAME     => array('vgakga_id' => 0, 'vgakga_type_id' => 1, 'vgakga_gamekult_id' => 2, 'vgakga_vgagam_id' => 3, 'vgakga_name' => 4, 'vgakga_aliases' => 5, 'vgakga_original_price' => 6, 'vgakga_platforms_array' => 7, 'vgakga_genres_array' => 8, 'vgakga_themes_array' => 9, 'vgakga_companies_array' => 10, 'vgakga_franchises_array' => 11, 'vgakga_fr_released_at' => 12, 'vgakga_us_released_at' => 13, 'vgakga_jp_released_at' => 14, 'vgakga_image_small_url' => 15, 'vgakga_site_detail_url' => 16, 'vgakga_refreshed_at' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_site_gamekult_game_vgakga');
        $this->setPhpName('SiteGameKultGame');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGame');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgakga_id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('vgakga_type_id', 'TypeId', 'VARCHAR', true, 255, null);
        $this->addColumn('vgakga_gamekult_id', 'GamekultId', 'VARCHAR', true, 255, null);
        $this->addForeignKey('vgakga_vgagam_id', 'GameId', 'INTEGER', 'videogames_game_vgagam', 'vgagam_id', false, null, null);
        $this->addColumn('vgakga_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('vgakga_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_original_price', 'OriginalPrice', 'FLOAT', false, null, null);
        $this->addColumn('vgakga_platforms_array', 'PlatformsArray', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_genres_array', 'GenresArray', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_themes_array', 'ThemesArray', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_companies_array', 'CompaniesArray', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_franchises_array', 'FranchisesArray', 'ARRAY', false, null, null);
        $this->addColumn('vgakga_fr_released_at', 'FrReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakga_us_released_at', 'UsReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakga_jp_released_at', 'JpReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakga_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgakga_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgakga_refreshed_at', 'RefreshedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Game', '\\IiMedias\\VideoGamesBundle\\Model\\Game', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgakga_vgagam_id',
    1 => ':vgagam_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('SiteGameKultGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgakrl_vgakga_id',
    1 => ':vgakga_id',
  ),
), 'CASCADE', 'CASCADE', 'SiteGameKultGameReleases', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_site_gamekult_game_vgakga     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteGameKultGameReleaseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteGameKultGameTableMap::CLASS_DEFAULT : SiteGameKultGameTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteGameKultGame object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteGameKultGameTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteGameKultGameTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteGameKultGameTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteGameKultGameTableMap::OM_CLASS;
            /** @var SiteGameKultGame $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteGameKultGameTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteGameKultGameTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteGameKultGameTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteGameKultGame $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteGameKultGameTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_ID);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_NAME);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL);
            $criteria->addSelectColumn(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgakga_id');
            $criteria->addSelectColumn($alias . '.vgakga_type_id');
            $criteria->addSelectColumn($alias . '.vgakga_gamekult_id');
            $criteria->addSelectColumn($alias . '.vgakga_vgagam_id');
            $criteria->addSelectColumn($alias . '.vgakga_name');
            $criteria->addSelectColumn($alias . '.vgakga_aliases');
            $criteria->addSelectColumn($alias . '.vgakga_original_price');
            $criteria->addSelectColumn($alias . '.vgakga_platforms_array');
            $criteria->addSelectColumn($alias . '.vgakga_genres_array');
            $criteria->addSelectColumn($alias . '.vgakga_themes_array');
            $criteria->addSelectColumn($alias . '.vgakga_companies_array');
            $criteria->addSelectColumn($alias . '.vgakga_franchises_array');
            $criteria->addSelectColumn($alias . '.vgakga_fr_released_at');
            $criteria->addSelectColumn($alias . '.vgakga_us_released_at');
            $criteria->addSelectColumn($alias . '.vgakga_jp_released_at');
            $criteria->addSelectColumn($alias . '.vgakga_image_small_url');
            $criteria->addSelectColumn($alias . '.vgakga_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgakga_refreshed_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteGameKultGameTableMap::DATABASE_NAME)->getTable(SiteGameKultGameTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteGameKultGameTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteGameKultGameTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteGameKultGameTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteGameKultGame or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteGameKultGame object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGame) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteGameKultGameTableMap::DATABASE_NAME);
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_ID, (array) $values, Criteria::IN);
        }

        $query = SiteGameKultGameQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteGameKultGameTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteGameKultGameTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_site_gamekult_game_vgakga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteGameKultGameQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteGameKultGame or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteGameKultGame object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteGameKultGame object
        }


        // Set the correct dbName
        $query = SiteGameKultGameQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteGameKultGameTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteGameKultGameTableMap::buildTableMap();
