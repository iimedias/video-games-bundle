<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaform;
use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaformQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_igdb_platform_vgaipl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiIgdbPlaformTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiIgdbPlaformTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_igdb_platform_vgaipl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbPlaform';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiIgdbPlaform';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the vgaipl_id field
     */
    const COL_VGAIPL_ID = 'videogames_api_igdb_platform_vgaipl.vgaipl_id';

    /**
     * the column name for the vgaipl_name field
     */
    const COL_VGAIPL_NAME = 'videogames_api_igdb_platform_vgaipl.vgaipl_name';

    /**
     * the column name for the vgaipl_slug field
     */
    const COL_VGAIPL_SLUG = 'videogames_api_igdb_platform_vgaipl.vgaipl_slug';

    /**
     * the column name for the vgaipl_url field
     */
    const COL_VGAIPL_URL = 'videogames_api_igdb_platform_vgaipl.vgaipl_url';

    /**
     * the column name for the vgaipl_website field
     */
    const COL_VGAIPL_WEBSITE = 'videogames_api_igdb_platform_vgaipl.vgaipl_website';

    /**
     * the column name for the vgaipl_summary field
     */
    const COL_VGAIPL_SUMMARY = 'videogames_api_igdb_platform_vgaipl.vgaipl_summary';

    /**
     * the column name for the vgaipl_alternative_name field
     */
    const COL_VGAIPL_ALTERNATIVE_NAME = 'videogames_api_igdb_platform_vgaipl.vgaipl_alternative_name';

    /**
     * the column name for the vgaipl_generation field
     */
    const COL_VGAIPL_GENERATION = 'videogames_api_igdb_platform_vgaipl.vgaipl_generation';

    /**
     * the column name for the vgaipl_games field
     */
    const COL_VGAIPL_GAMES = 'videogames_api_igdb_platform_vgaipl.vgaipl_games';

    /**
     * the column name for the vgaipl_created_at field
     */
    const COL_VGAIPL_CREATED_AT = 'videogames_api_igdb_platform_vgaipl.vgaipl_created_at';

    /**
     * the column name for the vgaipl_updated_at field
     */
    const COL_VGAIPL_UPDATED_AT = 'videogames_api_igdb_platform_vgaipl.vgaipl_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Slug', 'Url', 'WebSite', 'Summary', 'AlternativeName', 'Generation', 'Games', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'slug', 'url', 'webSite', 'summary', 'alternativeName', 'generation', 'games', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, ApiIgdbPlaformTableMap::COL_VGAIPL_NAME, ApiIgdbPlaformTableMap::COL_VGAIPL_SLUG, ApiIgdbPlaformTableMap::COL_VGAIPL_URL, ApiIgdbPlaformTableMap::COL_VGAIPL_WEBSITE, ApiIgdbPlaformTableMap::COL_VGAIPL_SUMMARY, ApiIgdbPlaformTableMap::COL_VGAIPL_ALTERNATIVE_NAME, ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION, ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES, ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT, ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgaipl_id', 'vgaipl_name', 'vgaipl_slug', 'vgaipl_url', 'vgaipl_website', 'vgaipl_summary', 'vgaipl_alternative_name', 'vgaipl_generation', 'vgaipl_games', 'vgaipl_created_at', 'vgaipl_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Slug' => 2, 'Url' => 3, 'WebSite' => 4, 'Summary' => 5, 'AlternativeName' => 6, 'Generation' => 7, 'Games' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'slug' => 2, 'url' => 3, 'webSite' => 4, 'summary' => 5, 'alternativeName' => 6, 'generation' => 7, 'games' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(ApiIgdbPlaformTableMap::COL_VGAIPL_ID => 0, ApiIgdbPlaformTableMap::COL_VGAIPL_NAME => 1, ApiIgdbPlaformTableMap::COL_VGAIPL_SLUG => 2, ApiIgdbPlaformTableMap::COL_VGAIPL_URL => 3, ApiIgdbPlaformTableMap::COL_VGAIPL_WEBSITE => 4, ApiIgdbPlaformTableMap::COL_VGAIPL_SUMMARY => 5, ApiIgdbPlaformTableMap::COL_VGAIPL_ALTERNATIVE_NAME => 6, ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION => 7, ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES => 8, ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT => 9, ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('vgaipl_id' => 0, 'vgaipl_name' => 1, 'vgaipl_slug' => 2, 'vgaipl_url' => 3, 'vgaipl_website' => 4, 'vgaipl_summary' => 5, 'vgaipl_alternative_name' => 6, 'vgaipl_generation' => 7, 'vgaipl_games' => 8, 'vgaipl_created_at' => 9, 'vgaipl_updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_igdb_platform_vgaipl');
        $this->setPhpName('ApiIgdbPlaform');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbPlaform');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgaipl_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgaipl_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaipl_slug', 'Slug', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaipl_url', 'Url', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaipl_website', 'WebSite', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaipl_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaipl_alternative_name', 'AlternativeName', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaipl_generation', 'Generation', 'INTEGER', false, null, null);
        $this->addColumn('vgaipl_games', 'Games', 'ARRAY', false, null, null);
        $this->addColumn('vgaipl_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('vgaipl_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiIgdbPlaformTableMap::CLASS_DEFAULT : ApiIgdbPlaformTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiIgdbPlaform object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiIgdbPlaformTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiIgdbPlaformTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiIgdbPlaformTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiIgdbPlaformTableMap::OM_CLASS;
            /** @var ApiIgdbPlaform $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiIgdbPlaformTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiIgdbPlaformTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiIgdbPlaformTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiIgdbPlaform $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiIgdbPlaformTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_ID);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_NAME);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_SLUG);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_URL);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_WEBSITE);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_SUMMARY);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_ALTERNATIVE_NAME);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT);
            $criteria->addSelectColumn(ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgaipl_id');
            $criteria->addSelectColumn($alias . '.vgaipl_name');
            $criteria->addSelectColumn($alias . '.vgaipl_slug');
            $criteria->addSelectColumn($alias . '.vgaipl_url');
            $criteria->addSelectColumn($alias . '.vgaipl_website');
            $criteria->addSelectColumn($alias . '.vgaipl_summary');
            $criteria->addSelectColumn($alias . '.vgaipl_alternative_name');
            $criteria->addSelectColumn($alias . '.vgaipl_generation');
            $criteria->addSelectColumn($alias . '.vgaipl_games');
            $criteria->addSelectColumn($alias . '.vgaipl_created_at');
            $criteria->addSelectColumn($alias . '.vgaipl_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiIgdbPlaformTableMap::DATABASE_NAME)->getTable(ApiIgdbPlaformTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiIgdbPlaformTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiIgdbPlaformTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiIgdbPlaformTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiIgdbPlaform or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiIgdbPlaform object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbPlaformTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiIgdbPlaform) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiIgdbPlaformTableMap::DATABASE_NAME);
            $criteria->add(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, (array) $values, Criteria::IN);
        }

        $query = ApiIgdbPlaformQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiIgdbPlaformTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiIgdbPlaformTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_igdb_platform_vgaipl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiIgdbPlaformQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiIgdbPlaform or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiIgdbPlaform object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbPlaformTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiIgdbPlaform object
        }


        // Set the correct dbName
        $query = ApiIgdbPlaformQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiIgdbPlaformTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiIgdbPlaformTableMap::buildTableMap();
