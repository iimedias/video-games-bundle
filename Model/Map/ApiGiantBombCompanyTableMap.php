<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_company_vgagco' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombCompanyTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombCompanyTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_company_vgagco';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCompany';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombCompany';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 24;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 24;

    /**
     * the column name for the vgagco_id field
     */
    const COL_VGAGCO_ID = 'videogames_api_giantbomb_company_vgagco.vgagco_id';

    /**
     * the column name for the vgagco_name field
     */
    const COL_VGAGCO_NAME = 'videogames_api_giantbomb_company_vgagco.vgagco_name';

    /**
     * the column name for the vgagco_abbr field
     */
    const COL_VGAGCO_ABBR = 'videogames_api_giantbomb_company_vgagco.vgagco_abbr';

    /**
     * the column name for the vgagco_aliases field
     */
    const COL_VGAGCO_ALIASES = 'videogames_api_giantbomb_company_vgagco.vgagco_aliases';

    /**
     * the column name for the vgagco_summary field
     */
    const COL_VGAGCO_SUMMARY = 'videogames_api_giantbomb_company_vgagco.vgagco_summary';

    /**
     * the column name for the vgagco_description field
     */
    const COL_VGAGCO_DESCRIPTION = 'videogames_api_giantbomb_company_vgagco.vgagco_description';

    /**
     * the column name for the vgagco_location_address field
     */
    const COL_VGAGCO_LOCATION_ADDRESS = 'videogames_api_giantbomb_company_vgagco.vgagco_location_address';

    /**
     * the column name for the vgagco_location_city field
     */
    const COL_VGAGCO_LOCATION_CITY = 'videogames_api_giantbomb_company_vgagco.vgagco_location_city';

    /**
     * the column name for the vgagco_location_country field
     */
    const COL_VGAGCO_LOCATION_COUNTRY = 'videogames_api_giantbomb_company_vgagco.vgagco_location_country';

    /**
     * the column name for the vgagco_location_state field
     */
    const COL_VGAGCO_LOCATION_STATE = 'videogames_api_giantbomb_company_vgagco.vgagco_location_state';

    /**
     * the column name for the vgagco_phone field
     */
    const COL_VGAGCO_PHONE = 'videogames_api_giantbomb_company_vgagco.vgagco_phone';

    /**
     * the column name for the vgagco_website field
     */
    const COL_VGAGCO_WEBSITE = 'videogames_api_giantbomb_company_vgagco.vgagco_website';

    /**
     * the column name for the vgagco_founded_at field
     */
    const COL_VGAGCO_FOUNDED_AT = 'videogames_api_giantbomb_company_vgagco.vgagco_founded_at';

    /**
     * the column name for the vgagco_api_detail_url field
     */
    const COL_VGAGCO_API_DETAIL_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_api_detail_url';

    /**
     * the column name for the vgagco_site_detail_url field
     */
    const COL_VGAGCO_SITE_DETAIL_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_site_detail_url';

    /**
     * the column name for the vgagco_image_icon_url field
     */
    const COL_VGAGCO_IMAGE_ICON_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_icon_url';

    /**
     * the column name for the vgagco_image_medium_url field
     */
    const COL_VGAGCO_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_medium_url';

    /**
     * the column name for the vgagco_image_screen_url field
     */
    const COL_VGAGCO_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_screen_url';

    /**
     * the column name for the vgagco_image_small_url field
     */
    const COL_VGAGCO_IMAGE_SMALL_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_small_url';

    /**
     * the column name for the vgagco_image_super_url field
     */
    const COL_VGAGCO_IMAGE_SUPER_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_super_url';

    /**
     * the column name for the vgagco_image_thumb_url field
     */
    const COL_VGAGCO_IMAGE_THUMB_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_thumb_url';

    /**
     * the column name for the vgagco_image_tiny_url field
     */
    const COL_VGAGCO_IMAGE_TINY_URL = 'videogames_api_giantbomb_company_vgagco.vgagco_image_tiny_url';

    /**
     * the column name for the vgagco_created_at field
     */
    const COL_VGAGCO_CREATED_AT = 'videogames_api_giantbomb_company_vgagco.vgagco_created_at';

    /**
     * the column name for the vgagco_updated_at field
     */
    const COL_VGAGCO_UPDATED_AT = 'videogames_api_giantbomb_company_vgagco.vgagco_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Abbr', 'Aliases', 'Summary', 'Description', 'LocationAddress', 'LocationCity', 'LocationCountry', 'LocationState', 'Phone', 'Website', 'FoundedAt', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'abbr', 'aliases', 'summary', 'description', 'locationAddress', 'locationCity', 'locationCountry', 'locationState', 'phone', 'website', 'foundedAt', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME, ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR, ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES, ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY, ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE, ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE, ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE, ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT, ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL, ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT, ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagco_id', 'vgagco_name', 'vgagco_abbr', 'vgagco_aliases', 'vgagco_summary', 'vgagco_description', 'vgagco_location_address', 'vgagco_location_city', 'vgagco_location_country', 'vgagco_location_state', 'vgagco_phone', 'vgagco_website', 'vgagco_founded_at', 'vgagco_api_detail_url', 'vgagco_site_detail_url', 'vgagco_image_icon_url', 'vgagco_image_medium_url', 'vgagco_image_screen_url', 'vgagco_image_small_url', 'vgagco_image_super_url', 'vgagco_image_thumb_url', 'vgagco_image_tiny_url', 'vgagco_created_at', 'vgagco_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Abbr' => 2, 'Aliases' => 3, 'Summary' => 4, 'Description' => 5, 'LocationAddress' => 6, 'LocationCity' => 7, 'LocationCountry' => 8, 'LocationState' => 9, 'Phone' => 10, 'Website' => 11, 'FoundedAt' => 12, 'ApiDetailUrl' => 13, 'SiteDetailUrl' => 14, 'ImageIconUrl' => 15, 'ImageMediumUrl' => 16, 'ImageScreenUrl' => 17, 'ImageSmallUrl' => 18, 'ImageSuperUrl' => 19, 'ImageThumbUrl' => 20, 'ImageTinyUrl' => 21, 'CreatedAt' => 22, 'UpdatedAt' => 23, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'abbr' => 2, 'aliases' => 3, 'summary' => 4, 'description' => 5, 'locationAddress' => 6, 'locationCity' => 7, 'locationCountry' => 8, 'locationState' => 9, 'phone' => 10, 'website' => 11, 'foundedAt' => 12, 'apiDetailUrl' => 13, 'siteDetailUrl' => 14, 'imageIconUrl' => 15, 'imageMediumUrl' => 16, 'imageScreenUrl' => 17, 'imageSmallUrl' => 18, 'imageSuperUrl' => 19, 'imageThumbUrl' => 20, 'imageTinyUrl' => 21, 'createdAt' => 22, 'updatedAt' => 23, ),
        self::TYPE_COLNAME       => array(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID => 0, ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME => 1, ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR => 2, ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES => 3, ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY => 4, ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION => 5, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS => 6, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY => 7, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY => 8, ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE => 9, ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE => 10, ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE => 11, ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT => 12, ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL => 13, ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL => 14, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL => 15, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL => 16, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL => 17, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL => 18, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL => 19, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL => 20, ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL => 21, ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT => 22, ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT => 23, ),
        self::TYPE_FIELDNAME     => array('vgagco_id' => 0, 'vgagco_name' => 1, 'vgagco_abbr' => 2, 'vgagco_aliases' => 3, 'vgagco_summary' => 4, 'vgagco_description' => 5, 'vgagco_location_address' => 6, 'vgagco_location_city' => 7, 'vgagco_location_country' => 8, 'vgagco_location_state' => 9, 'vgagco_phone' => 10, 'vgagco_website' => 11, 'vgagco_founded_at' => 12, 'vgagco_api_detail_url' => 13, 'vgagco_site_detail_url' => 14, 'vgagco_image_icon_url' => 15, 'vgagco_image_medium_url' => 16, 'vgagco_image_screen_url' => 17, 'vgagco_image_small_url' => 18, 'vgagco_image_super_url' => 19, 'vgagco_image_thumb_url' => 20, 'vgagco_image_tiny_url' => 21, 'vgagco_created_at' => 22, 'vgagco_updated_at' => 23, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_company_vgagco');
        $this->setPhpName('ApiGiantBombCompany');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCompany');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagco_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagco_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_abbr', 'Abbr', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagco_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_location_address', 'LocationAddress', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_location_city', 'LocationCity', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_location_country', 'LocationCountry', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_location_state', 'LocationState', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_website', 'Website', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_founded_at', 'FoundedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagco_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagco_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagco_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagco_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombPlatform', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagpl_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombPlatforms', false);
        $this->addRelation('ApiGiantBombGameDeveloper', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameDeveloper', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggd_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameDevelopers', false);
        $this->addRelation('ApiGiantBombGamePublisher', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGamePublisher', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggu_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGamePublishers', false);
        $this->addRelation('ApiGiantBombGameReleaseDeveloper', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameReleaseDeveloper', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrd_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleaseDevelopers', false);
        $this->addRelation('ApiGiantBombGameReleasePublisher', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameReleasePublisher', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrp_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleasePublishers', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_company_vgagco     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombPlatformTableMap::clearInstancePool();
        ApiGiantBombGameDeveloperTableMap::clearInstancePool();
        ApiGiantBombGamePublisherTableMap::clearInstancePool();
        ApiGiantBombGameReleaseDeveloperTableMap::clearInstancePool();
        ApiGiantBombGameReleasePublisherTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombCompanyTableMap::CLASS_DEFAULT : ApiGiantBombCompanyTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombCompany object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombCompanyTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombCompanyTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombCompanyTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombCompanyTableMap::OM_CLASS;
            /** @var ApiGiantBombCompany $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombCompanyTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombCompanyTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombCompanyTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombCompany $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombCompanyTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagco_id');
            $criteria->addSelectColumn($alias . '.vgagco_name');
            $criteria->addSelectColumn($alias . '.vgagco_abbr');
            $criteria->addSelectColumn($alias . '.vgagco_aliases');
            $criteria->addSelectColumn($alias . '.vgagco_summary');
            $criteria->addSelectColumn($alias . '.vgagco_description');
            $criteria->addSelectColumn($alias . '.vgagco_location_address');
            $criteria->addSelectColumn($alias . '.vgagco_location_city');
            $criteria->addSelectColumn($alias . '.vgagco_location_country');
            $criteria->addSelectColumn($alias . '.vgagco_location_state');
            $criteria->addSelectColumn($alias . '.vgagco_phone');
            $criteria->addSelectColumn($alias . '.vgagco_website');
            $criteria->addSelectColumn($alias . '.vgagco_founded_at');
            $criteria->addSelectColumn($alias . '.vgagco_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagco_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagco_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagco_created_at');
            $criteria->addSelectColumn($alias . '.vgagco_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombCompanyTableMap::DATABASE_NAME)->getTable(ApiGiantBombCompanyTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombCompanyTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombCompanyTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombCompany or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombCompany object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombCompanyTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombCompanyQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombCompanyTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombCompanyTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_company_vgagco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombCompanyQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombCompany or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombCompany object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombCompany object
        }


        // Set the correct dbName
        $query = ApiGiantBombCompanyQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombCompanyTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombCompanyTableMap::buildTableMap();
