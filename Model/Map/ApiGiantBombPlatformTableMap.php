<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_platform_vgagpl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombPlatformTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombPlatformTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_platform_vgagpl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombPlatform';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the vgagpl_id field
     */
    const COL_VGAGPL_ID = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_id';

    /**
     * the column name for the vgagpl_name field
     */
    const COL_VGAGPL_NAME = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_name';

    /**
     * the column name for the vgagpl_abbr field
     */
    const COL_VGAGPL_ABBR = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_abbr';

    /**
     * the column name for the vgagpl_aliases field
     */
    const COL_VGAGPL_ALIASES = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_aliases';

    /**
     * the column name for the vgagpl_summary field
     */
    const COL_VGAGPL_SUMMARY = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_summary';

    /**
     * the column name for the vgagpl_description field
     */
    const COL_VGAGPL_DESCRIPTION = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_description';

    /**
     * the column name for the vgagpl_install_base field
     */
    const COL_VGAGPL_INSTALL_BASE = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_install_base';

    /**
     * the column name for the vgagpl_online_support field
     */
    const COL_VGAGPL_ONLINE_SUPPORT = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_online_support';

    /**
     * the column name for the vgagpl_original_price field
     */
    const COL_VGAGPL_ORIGINAL_PRICE = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_original_price';

    /**
     * the column name for the vgagpl_released_at field
     */
    const COL_VGAGPL_RELEASED_AT = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_released_at';

    /**
     * the column name for the vgagpl_api_detail_url field
     */
    const COL_VGAGPL_API_DETAIL_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_api_detail_url';

    /**
     * the column name for the vgagpl_site_detail_url field
     */
    const COL_VGAGPL_SITE_DETAIL_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_site_detail_url';

    /**
     * the column name for the vgagpl_image_icon_url field
     */
    const COL_VGAGPL_IMAGE_ICON_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_icon_url';

    /**
     * the column name for the vgagpl_image_medium_url field
     */
    const COL_VGAGPL_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_medium_url';

    /**
     * the column name for the vgagpl_image_screen_url field
     */
    const COL_VGAGPL_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_screen_url';

    /**
     * the column name for the vgagpl_image_small_url field
     */
    const COL_VGAGPL_IMAGE_SMALL_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_small_url';

    /**
     * the column name for the vgagpl_image_super_url field
     */
    const COL_VGAGPL_IMAGE_SUPER_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_super_url';

    /**
     * the column name for the vgagpl_image_thumb_url field
     */
    const COL_VGAGPL_IMAGE_THUMB_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_thumb_url';

    /**
     * the column name for the vgagpl_image_tiny_url field
     */
    const COL_VGAGPL_IMAGE_TINY_URL = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_image_tiny_url';

    /**
     * the column name for the vgagpl_vgagco_id field
     */
    const COL_VGAGPL_VGAGCO_ID = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_vgagco_id';

    /**
     * the column name for the vgagpl_created_at field
     */
    const COL_VGAGPL_CREATED_AT = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_created_at';

    /**
     * the column name for the vgagpl_updated_at field
     */
    const COL_VGAGPL_UPDATED_AT = 'videogames_api_giantbomb_platform_vgagpl.vgagpl_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Abbr', 'Aliases', 'Summary', 'Description', 'InstallBase', 'OnlineSupport', 'OriginalPrice', 'ReleasedAt', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'ApiGiantBombCompanyId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'abbr', 'aliases', 'summary', 'description', 'installBase', 'onlineSupport', 'originalPrice', 'releasedAt', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'apiGiantBombCompanyId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME, ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR, ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES, ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY, ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION, ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE, ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT, ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE, ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT, ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL, ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT, ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagpl_id', 'vgagpl_name', 'vgagpl_abbr', 'vgagpl_aliases', 'vgagpl_summary', 'vgagpl_description', 'vgagpl_install_base', 'vgagpl_online_support', 'vgagpl_original_price', 'vgagpl_released_at', 'vgagpl_api_detail_url', 'vgagpl_site_detail_url', 'vgagpl_image_icon_url', 'vgagpl_image_medium_url', 'vgagpl_image_screen_url', 'vgagpl_image_small_url', 'vgagpl_image_super_url', 'vgagpl_image_thumb_url', 'vgagpl_image_tiny_url', 'vgagpl_vgagco_id', 'vgagpl_created_at', 'vgagpl_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Abbr' => 2, 'Aliases' => 3, 'Summary' => 4, 'Description' => 5, 'InstallBase' => 6, 'OnlineSupport' => 7, 'OriginalPrice' => 8, 'ReleasedAt' => 9, 'ApiDetailUrl' => 10, 'SiteDetailUrl' => 11, 'ImageIconUrl' => 12, 'ImageMediumUrl' => 13, 'ImageScreenUrl' => 14, 'ImageSmallUrl' => 15, 'ImageSuperUrl' => 16, 'ImageThumbUrl' => 17, 'ImageTinyUrl' => 18, 'ApiGiantBombCompanyId' => 19, 'CreatedAt' => 20, 'UpdatedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'abbr' => 2, 'aliases' => 3, 'summary' => 4, 'description' => 5, 'installBase' => 6, 'onlineSupport' => 7, 'originalPrice' => 8, 'releasedAt' => 9, 'apiDetailUrl' => 10, 'siteDetailUrl' => 11, 'imageIconUrl' => 12, 'imageMediumUrl' => 13, 'imageScreenUrl' => 14, 'imageSmallUrl' => 15, 'imageSuperUrl' => 16, 'imageThumbUrl' => 17, 'imageTinyUrl' => 18, 'apiGiantBombCompanyId' => 19, 'createdAt' => 20, 'updatedAt' => 21, ),
        self::TYPE_COLNAME       => array(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID => 0, ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME => 1, ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR => 2, ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES => 3, ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY => 4, ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION => 5, ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE => 6, ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT => 7, ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE => 8, ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT => 9, ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL => 10, ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL => 11, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL => 12, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL => 13, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL => 14, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL => 15, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL => 16, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL => 17, ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL => 18, ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID => 19, ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT => 20, ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('vgagpl_id' => 0, 'vgagpl_name' => 1, 'vgagpl_abbr' => 2, 'vgagpl_aliases' => 3, 'vgagpl_summary' => 4, 'vgagpl_description' => 5, 'vgagpl_install_base' => 6, 'vgagpl_online_support' => 7, 'vgagpl_original_price' => 8, 'vgagpl_released_at' => 9, 'vgagpl_api_detail_url' => 10, 'vgagpl_site_detail_url' => 11, 'vgagpl_image_icon_url' => 12, 'vgagpl_image_medium_url' => 13, 'vgagpl_image_screen_url' => 14, 'vgagpl_image_small_url' => 15, 'vgagpl_image_super_url' => 16, 'vgagpl_image_thumb_url' => 17, 'vgagpl_image_tiny_url' => 18, 'vgagpl_vgagco_id' => 19, 'vgagpl_created_at' => 20, 'vgagpl_updated_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_platform_vgagpl');
        $this->setPhpName('ApiGiantBombPlatform');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagpl_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagpl_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpl_abbr', 'Abbr', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpl_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagpl_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_install_base', 'InstallBase', 'INTEGER', false, null, null);
        $this->addColumn('vgagpl_online_support', 'OnlineSupport', 'BOOLEAN', false, 1, null);
        $this->addColumn('vgagpl_original_price', 'OriginalPrice', 'FLOAT', false, null, null);
        $this->addColumn('vgagpl_released_at', 'ReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagpl_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpl_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpl_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpl_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('vgagpl_vgagco_id', 'ApiGiantBombCompanyId', 'INTEGER', 'videogames_api_giantbomb_company_vgagco', 'vgagco_id', false, null, null);
        $this->addColumn('vgagpl_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagpl_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombCompany', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCompany', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagpl_vgagco_id',
    1 => ':vgagco_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombGamePlatform', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGamePlatform', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggp_vgagpl_id',
    1 => ':vgagpl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGamePlatforms', false);
        $this->addRelation('ApiGiantBombGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrl_vgagpl_id',
    1 => ':vgagpl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleases', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_platform_vgagpl     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombGamePlatformTableMap::clearInstancePool();
        ApiGiantBombGameReleaseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombPlatformTableMap::CLASS_DEFAULT : ApiGiantBombPlatformTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombPlatform object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombPlatformTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombPlatformTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombPlatformTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombPlatformTableMap::OM_CLASS;
            /** @var ApiGiantBombPlatform $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombPlatformTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombPlatformTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombPlatformTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombPlatform $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombPlatformTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagpl_id');
            $criteria->addSelectColumn($alias . '.vgagpl_name');
            $criteria->addSelectColumn($alias . '.vgagpl_abbr');
            $criteria->addSelectColumn($alias . '.vgagpl_aliases');
            $criteria->addSelectColumn($alias . '.vgagpl_summary');
            $criteria->addSelectColumn($alias . '.vgagpl_description');
            $criteria->addSelectColumn($alias . '.vgagpl_install_base');
            $criteria->addSelectColumn($alias . '.vgagpl_online_support');
            $criteria->addSelectColumn($alias . '.vgagpl_original_price');
            $criteria->addSelectColumn($alias . '.vgagpl_released_at');
            $criteria->addSelectColumn($alias . '.vgagpl_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagpl_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagpl_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagpl_vgagco_id');
            $criteria->addSelectColumn($alias . '.vgagpl_created_at');
            $criteria->addSelectColumn($alias . '.vgagpl_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombPlatformTableMap::DATABASE_NAME)->getTable(ApiGiantBombPlatformTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombPlatformTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombPlatformTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombPlatform or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombPlatform object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombPlatformTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombPlatformQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombPlatformTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombPlatformTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_platform_vgagpl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombPlatformQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombPlatform or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombPlatform object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombPlatform object
        }


        // Set the correct dbName
        $query = ApiGiantBombPlatformQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombPlatformTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombPlatformTableMap::buildTableMap();
