<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_game_vgagga' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombGameTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombGameTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_game_vgagga';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombGame';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 24;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 24;

    /**
     * the column name for the vgagga_id field
     */
    const COL_VGAGGA_ID = 'videogames_api_giantbomb_game_vgagga.vgagga_id';

    /**
     * the column name for the vgagga_vgagam_id field
     */
    const COL_VGAGGA_VGAGAM_ID = 'videogames_api_giantbomb_game_vgagga.vgagga_vgagam_id';

    /**
     * the column name for the vgagga_name field
     */
    const COL_VGAGGA_NAME = 'videogames_api_giantbomb_game_vgagga.vgagga_name';

    /**
     * the column name for the vgagga_aliases field
     */
    const COL_VGAGGA_ALIASES = 'videogames_api_giantbomb_game_vgagga.vgagga_aliases';

    /**
     * the column name for the vgagga_summary field
     */
    const COL_VGAGGA_SUMMARY = 'videogames_api_giantbomb_game_vgagga.vgagga_summary';

    /**
     * the column name for the vgagga_description field
     */
    const COL_VGAGGA_DESCRIPTION = 'videogames_api_giantbomb_game_vgagga.vgagga_description';

    /**
     * the column name for the vgagga_original_released_at field
     */
    const COL_VGAGGA_ORIGINAL_RELEASED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_original_released_at';

    /**
     * the column name for the vgagga_expected_day_released_at field
     */
    const COL_VGAGGA_EXPECTED_DAY_RELEASED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_expected_day_released_at';

    /**
     * the column name for the vgagga_expected_month_released_at field
     */
    const COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_expected_month_released_at';

    /**
     * the column name for the vgagga_expected_quarter_released_at field
     */
    const COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_expected_quarter_released_at';

    /**
     * the column name for the vgagga_expected_year_released_at field
     */
    const COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_expected_year_released_at';

    /**
     * the column name for the vgagga_user_reviews_count field
     */
    const COL_VGAGGA_USER_REVIEWS_COUNT = 'videogames_api_giantbomb_game_vgagga.vgagga_user_reviews_count';

    /**
     * the column name for the vgagga_image_icon_url field
     */
    const COL_VGAGGA_IMAGE_ICON_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_icon_url';

    /**
     * the column name for the vgagga_image_medium_url field
     */
    const COL_VGAGGA_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_medium_url';

    /**
     * the column name for the vgagga_image_screen_url field
     */
    const COL_VGAGGA_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_screen_url';

    /**
     * the column name for the vgagga_image_small_url field
     */
    const COL_VGAGGA_IMAGE_SMALL_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_small_url';

    /**
     * the column name for the vgagga_image_super_url field
     */
    const COL_VGAGGA_IMAGE_SUPER_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_super_url';

    /**
     * the column name for the vgagga_image_thumb_url field
     */
    const COL_VGAGGA_IMAGE_THUMB_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_thumb_url';

    /**
     * the column name for the vgagga_image_tiny_url field
     */
    const COL_VGAGGA_IMAGE_TINY_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_image_tiny_url';

    /**
     * the column name for the vgagga_api_detail_url field
     */
    const COL_VGAGGA_API_DETAIL_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_api_detail_url';

    /**
     * the column name for the vgagga_site_detail_url field
     */
    const COL_VGAGGA_SITE_DETAIL_URL = 'videogames_api_giantbomb_game_vgagga.vgagga_site_detail_url';

    /**
     * the column name for the vgagga_created_at field
     */
    const COL_VGAGGA_CREATED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_created_at';

    /**
     * the column name for the vgagga_updated_at field
     */
    const COL_VGAGGA_UPDATED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_updated_at';

    /**
     * the column name for the vgagga_refreshed_at field
     */
    const COL_VGAGGA_REFRESHED_AT = 'videogames_api_giantbomb_game_vgagga.vgagga_refreshed_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'GameId', 'Name', 'Aliases', 'Summary', 'Description', 'OriginalReleasedAt', 'ExpectedDayReleasedAt', 'ExpectedMonthReleasedAt', 'ExpectedQuarterReleasedAt', 'ExpectedYearReleasedAt', 'UserReviewsCount', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'ApiDetailUrl', 'SiteDetailUrl', 'CreatedAt', 'UpdatedAt', 'RefreshedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'gameId', 'name', 'aliases', 'summary', 'description', 'originalReleasedAt', 'expectedDayReleasedAt', 'expectedMonthReleasedAt', 'expectedQuarterReleasedAt', 'expectedYearReleasedAt', 'userReviewsCount', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'apiDetailUrl', 'siteDetailUrl', 'createdAt', 'updatedAt', 'refreshedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameTableMap::COL_VGAGGA_ID, ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, ApiGiantBombGameTableMap::COL_VGAGGA_NAME, ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES, ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY, ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION, ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL, ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL, ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL, ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT, ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagga_id', 'vgagga_vgagam_id', 'vgagga_name', 'vgagga_aliases', 'vgagga_summary', 'vgagga_description', 'vgagga_original_released_at', 'vgagga_expected_day_released_at', 'vgagga_expected_month_released_at', 'vgagga_expected_quarter_released_at', 'vgagga_expected_year_released_at', 'vgagga_user_reviews_count', 'vgagga_image_icon_url', 'vgagga_image_medium_url', 'vgagga_image_screen_url', 'vgagga_image_small_url', 'vgagga_image_super_url', 'vgagga_image_thumb_url', 'vgagga_image_tiny_url', 'vgagga_api_detail_url', 'vgagga_site_detail_url', 'vgagga_created_at', 'vgagga_updated_at', 'vgagga_refreshed_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'GameId' => 1, 'Name' => 2, 'Aliases' => 3, 'Summary' => 4, 'Description' => 5, 'OriginalReleasedAt' => 6, 'ExpectedDayReleasedAt' => 7, 'ExpectedMonthReleasedAt' => 8, 'ExpectedQuarterReleasedAt' => 9, 'ExpectedYearReleasedAt' => 10, 'UserReviewsCount' => 11, 'ImageIconUrl' => 12, 'ImageMediumUrl' => 13, 'ImageScreenUrl' => 14, 'ImageSmallUrl' => 15, 'ImageSuperUrl' => 16, 'ImageThumbUrl' => 17, 'ImageTinyUrl' => 18, 'ApiDetailUrl' => 19, 'SiteDetailUrl' => 20, 'CreatedAt' => 21, 'UpdatedAt' => 22, 'RefreshedAt' => 23, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'gameId' => 1, 'name' => 2, 'aliases' => 3, 'summary' => 4, 'description' => 5, 'originalReleasedAt' => 6, 'expectedDayReleasedAt' => 7, 'expectedMonthReleasedAt' => 8, 'expectedQuarterReleasedAt' => 9, 'expectedYearReleasedAt' => 10, 'userReviewsCount' => 11, 'imageIconUrl' => 12, 'imageMediumUrl' => 13, 'imageScreenUrl' => 14, 'imageSmallUrl' => 15, 'imageSuperUrl' => 16, 'imageThumbUrl' => 17, 'imageTinyUrl' => 18, 'apiDetailUrl' => 19, 'siteDetailUrl' => 20, 'createdAt' => 21, 'updatedAt' => 22, 'refreshedAt' => 23, ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameTableMap::COL_VGAGGA_ID => 0, ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID => 1, ApiGiantBombGameTableMap::COL_VGAGGA_NAME => 2, ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES => 3, ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY => 4, ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION => 5, ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT => 6, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT => 7, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT => 8, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT => 9, ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT => 10, ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT => 11, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL => 12, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL => 13, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL => 14, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL => 15, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL => 16, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL => 17, ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL => 18, ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL => 19, ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL => 20, ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT => 21, ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT => 22, ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT => 23, ),
        self::TYPE_FIELDNAME     => array('vgagga_id' => 0, 'vgagga_vgagam_id' => 1, 'vgagga_name' => 2, 'vgagga_aliases' => 3, 'vgagga_summary' => 4, 'vgagga_description' => 5, 'vgagga_original_released_at' => 6, 'vgagga_expected_day_released_at' => 7, 'vgagga_expected_month_released_at' => 8, 'vgagga_expected_quarter_released_at' => 9, 'vgagga_expected_year_released_at' => 10, 'vgagga_user_reviews_count' => 11, 'vgagga_image_icon_url' => 12, 'vgagga_image_medium_url' => 13, 'vgagga_image_screen_url' => 14, 'vgagga_image_small_url' => 15, 'vgagga_image_super_url' => 16, 'vgagga_image_thumb_url' => 17, 'vgagga_image_tiny_url' => 18, 'vgagga_api_detail_url' => 19, 'vgagga_site_detail_url' => 20, 'vgagga_created_at' => 21, 'vgagga_updated_at' => 22, 'vgagga_refreshed_at' => 23, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_game_vgagga');
        $this->setPhpName('ApiGiantBombGame');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagga_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('vgagga_vgagam_id', 'GameId', 'INTEGER', 'videogames_game_vgagam', 'vgagam_id', false, null, null);
        $this->addColumn('vgagga_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagga_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagga_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_original_released_at', 'OriginalReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_expected_day_released_at', 'ExpectedDayReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_expected_month_released_at', 'ExpectedMonthReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_expected_quarter_released_at', 'ExpectedQuarterReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_expected_year_released_at', 'ExpectedYearReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_user_reviews_count', 'UserReviewsCount', 'INTEGER', false, null, null);
        $this->addColumn('vgagga_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagga_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagga_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagga_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagga_refreshed_at', 'RefreshedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Game', '\\IiMedias\\VideoGamesBundle\\Model\\Game', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagga_vgagam_id',
    1 => ':vgagam_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombFirstGameCharacter', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCharacter', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagch_first_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombFirstGameCharacters', false);
        $this->addRelation('ApiGiantBombFirstGameConcept', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombConcept', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagcc_first_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombFirstGameConcepts', false);
        $this->addRelation('ApiGiantBombGameOriginalRating', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameOriginalRating', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggo_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameOriginalRatings', false);
        $this->addRelation('ApiGiantBombGamePlatform', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGamePlatform', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggp_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGamePlatforms', false);
        $this->addRelation('ApiGiantBombGameDeveloper', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameDeveloper', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggd_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameDevelopers', false);
        $this->addRelation('ApiGiantBombGamePublisher', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGamePublisher', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggu_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGamePublishers', false);
        $this->addRelation('ApiGiantBombGameFranchise', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameFranchise', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggf_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameFranchises', false);
        $this->addRelation('ApiGiantBombGameGenre', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameGenre', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggg_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameGenres', false);
        $this->addRelation('ApiGiantBombGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrl_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleases', false);
        $this->addRelation('ApiGiantBombFirstGameLocation', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombLocation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaglo_first_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombFirstGameLocations', false);
        $this->addRelation('ApiGiantBombFirstGameObject', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombObject', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagob_first_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombFirstGameObjects', false);
        $this->addRelation('ApiGiantBombCreditedGamePerson', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPerson', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagpe_credited_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombCreditedGamepeople', false);
        $this->addRelation('ApiGiantBombImage', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombImage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagim_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombImages', false);
        $this->addRelation('ApiGiantBombVideo', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombVideo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagvi_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombVideos', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_game_vgagga     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombCharacterTableMap::clearInstancePool();
        ApiGiantBombConceptTableMap::clearInstancePool();
        ApiGiantBombGameOriginalRatingTableMap::clearInstancePool();
        ApiGiantBombGamePlatformTableMap::clearInstancePool();
        ApiGiantBombGameDeveloperTableMap::clearInstancePool();
        ApiGiantBombGamePublisherTableMap::clearInstancePool();
        ApiGiantBombGameFranchiseTableMap::clearInstancePool();
        ApiGiantBombGameGenreTableMap::clearInstancePool();
        ApiGiantBombGameReleaseTableMap::clearInstancePool();
        ApiGiantBombLocationTableMap::clearInstancePool();
        ApiGiantBombObjectTableMap::clearInstancePool();
        ApiGiantBombPersonTableMap::clearInstancePool();
        ApiGiantBombImageTableMap::clearInstancePool();
        ApiGiantBombVideoTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombGameTableMap::CLASS_DEFAULT : ApiGiantBombGameTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombGame object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombGameTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombGameTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombGameTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombGameTableMap::OM_CLASS;
            /** @var ApiGiantBombGame $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombGameTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombGameTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombGameTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombGame $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombGameTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_ID);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_NAME);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagga_id');
            $criteria->addSelectColumn($alias . '.vgagga_vgagam_id');
            $criteria->addSelectColumn($alias . '.vgagga_name');
            $criteria->addSelectColumn($alias . '.vgagga_aliases');
            $criteria->addSelectColumn($alias . '.vgagga_summary');
            $criteria->addSelectColumn($alias . '.vgagga_description');
            $criteria->addSelectColumn($alias . '.vgagga_original_released_at');
            $criteria->addSelectColumn($alias . '.vgagga_expected_day_released_at');
            $criteria->addSelectColumn($alias . '.vgagga_expected_month_released_at');
            $criteria->addSelectColumn($alias . '.vgagga_expected_quarter_released_at');
            $criteria->addSelectColumn($alias . '.vgagga_expected_year_released_at');
            $criteria->addSelectColumn($alias . '.vgagga_user_reviews_count');
            $criteria->addSelectColumn($alias . '.vgagga_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagga_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagga_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagga_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagga_created_at');
            $criteria->addSelectColumn($alias . '.vgagga_updated_at');
            $criteria->addSelectColumn($alias . '.vgagga_refreshed_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameTableMap::DATABASE_NAME)->getTable(ApiGiantBombGameTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombGameTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombGameTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombGame or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombGame object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombGameTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombGameQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombGameTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombGameTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_game_vgagga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombGameQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombGame or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombGame object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombGame object
        }


        // Set the correct dbName
        $query = ApiGiantBombGameQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombGameTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombGameTableMap::buildTableMap();
