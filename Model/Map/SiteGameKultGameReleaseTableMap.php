<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_site_gamekult_release_vgakrl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiteGameKultGameReleaseTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.SiteGameKultGameReleaseTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_site_gamekult_release_vgakrl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGameRelease';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.SiteGameKultGameRelease';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the vgakrl_id field
     */
    const COL_VGAKRL_ID = 'videogames_site_gamekult_release_vgakrl.vgakrl_id';

    /**
     * the column name for the vgakrl_vgakga_id field
     */
    const COL_VGAKRL_VGAKGA_ID = 'videogames_site_gamekult_release_vgakrl.vgakrl_vgakga_id';

    /**
     * the column name for the vgakrl_vgakpl_id field
     */
    const COL_VGAKRL_VGAKPL_ID = 'videogames_site_gamekult_release_vgakrl.vgakrl_vgakpl_id';

    /**
     * the column name for the vgakrl_fr_released_at field
     */
    const COL_VGAKRL_FR_RELEASED_AT = 'videogames_site_gamekult_release_vgakrl.vgakrl_fr_released_at';

    /**
     * the column name for the vgakrl_us_released_at field
     */
    const COL_VGAKRL_US_RELEASED_AT = 'videogames_site_gamekult_release_vgakrl.vgakrl_us_released_at';

    /**
     * the column name for the vgakrl_jp_released_at field
     */
    const COL_VGAKRL_JP_RELEASED_AT = 'videogames_site_gamekult_release_vgakrl.vgakrl_jp_released_at';

    /**
     * the column name for the vgakrl_site_detail_url field
     */
    const COL_VGAKRL_SITE_DETAIL_URL = 'videogames_site_gamekult_release_vgakrl.vgakrl_site_detail_url';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteGameKultGameId', 'SiteGameKultPlatformId', 'FrReleasedAt', 'UsReleasedAt', 'JpReleasedAt', 'SiteDetailUrl', ),
        self::TYPE_CAMELNAME     => array('id', 'siteGameKultGameId', 'siteGameKultPlatformId', 'frReleasedAt', 'usReleasedAt', 'jpReleasedAt', 'siteDetailUrl', ),
        self::TYPE_COLNAME       => array(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT, SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT, SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT, SiteGameKultGameReleaseTableMap::COL_VGAKRL_SITE_DETAIL_URL, ),
        self::TYPE_FIELDNAME     => array('vgakrl_id', 'vgakrl_vgakga_id', 'vgakrl_vgakpl_id', 'vgakrl_fr_released_at', 'vgakrl_us_released_at', 'vgakrl_jp_released_at', 'vgakrl_site_detail_url', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteGameKultGameId' => 1, 'SiteGameKultPlatformId' => 2, 'FrReleasedAt' => 3, 'UsReleasedAt' => 4, 'JpReleasedAt' => 5, 'SiteDetailUrl' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteGameKultGameId' => 1, 'siteGameKultPlatformId' => 2, 'frReleasedAt' => 3, 'usReleasedAt' => 4, 'jpReleasedAt' => 5, 'siteDetailUrl' => 6, ),
        self::TYPE_COLNAME       => array(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID => 0, SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID => 1, SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID => 2, SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT => 3, SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT => 4, SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT => 5, SiteGameKultGameReleaseTableMap::COL_VGAKRL_SITE_DETAIL_URL => 6, ),
        self::TYPE_FIELDNAME     => array('vgakrl_id' => 0, 'vgakrl_vgakga_id' => 1, 'vgakrl_vgakpl_id' => 2, 'vgakrl_fr_released_at' => 3, 'vgakrl_us_released_at' => 4, 'vgakrl_jp_released_at' => 5, 'vgakrl_site_detail_url' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_site_gamekult_release_vgakrl');
        $this->setPhpName('SiteGameKultGameRelease');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGameRelease');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('vgakrl_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('vgakrl_vgakga_id', 'SiteGameKultGameId', 'BIGINT', 'videogames_site_gamekult_game_vgakga', 'vgakga_id', true, null, null);
        $this->addForeignKey('vgakrl_vgakpl_id', 'SiteGameKultPlatformId', 'INTEGER', 'videogames_site_gamekult_platform_vgakpl', 'vgakpl_id', true, null, null);
        $this->addColumn('vgakrl_fr_released_at', 'FrReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakrl_us_released_at', 'UsReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakrl_jp_released_at', 'JpReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgakrl_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SiteGameKultGame', '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgakrl_vgakga_id',
    1 => ':vgakga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('SiteGameKultPlatform', '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultPlatform', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgakrl_vgakpl_id',
    1 => ':vgakpl_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteGameKultGameReleaseTableMap::CLASS_DEFAULT : SiteGameKultGameReleaseTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteGameKultGameRelease object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteGameKultGameReleaseTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteGameKultGameReleaseTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteGameKultGameReleaseTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteGameKultGameReleaseTableMap::OM_CLASS;
            /** @var SiteGameKultGameRelease $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteGameKultGameReleaseTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteGameKultGameReleaseTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteGameKultGameReleaseTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteGameKultGameRelease $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteGameKultGameReleaseTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT);
            $criteria->addSelectColumn(SiteGameKultGameReleaseTableMap::COL_VGAKRL_SITE_DETAIL_URL);
        } else {
            $criteria->addSelectColumn($alias . '.vgakrl_id');
            $criteria->addSelectColumn($alias . '.vgakrl_vgakga_id');
            $criteria->addSelectColumn($alias . '.vgakrl_vgakpl_id');
            $criteria->addSelectColumn($alias . '.vgakrl_fr_released_at');
            $criteria->addSelectColumn($alias . '.vgakrl_us_released_at');
            $criteria->addSelectColumn($alias . '.vgakrl_jp_released_at');
            $criteria->addSelectColumn($alias . '.vgakrl_site_detail_url');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteGameKultGameReleaseTableMap::DATABASE_NAME)->getTable(SiteGameKultGameReleaseTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteGameKultGameReleaseTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteGameKultGameReleaseTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteGameKultGameRelease or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteGameKultGameRelease object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
            $criteria->add(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteGameKultGameReleaseQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteGameKultGameReleaseTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteGameKultGameReleaseTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_site_gamekult_release_vgakrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteGameKultGameReleaseQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteGameKultGameRelease or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteGameKultGameRelease object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteGameKultGameRelease object
        }

        if ($criteria->containsKey(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID) && $criteria->keyContainsValue(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID.')');
        }


        // Set the correct dbName
        $query = SiteGameKultGameReleaseQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteGameKultGameReleaseTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteGameKultGameReleaseTableMap::buildTableMap();
