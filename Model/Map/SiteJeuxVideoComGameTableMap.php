<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_site_jeuxvideocom_game_vgajga' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiteJeuxVideoComGameTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.SiteJeuxVideoComGameTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_site_jeuxvideocom_game_vgajga';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGame';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.SiteJeuxVideoComGame';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the vgajga_id field
     */
    const COL_VGAJGA_ID = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_id';

    /**
     * the column name for the vgajga_vgagam_id field
     */
    const COL_VGAJGA_VGAGAM_ID = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_vgagam_id';

    /**
     * the column name for the vgajga_name field
     */
    const COL_VGAJGA_NAME = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_name';

    /**
     * the column name for the vgajga_released_at field
     */
    const COL_VGAJGA_RELEASED_AT = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_released_at';

    /**
     * the column name for the vgajga_site_detail_url field
     */
    const COL_VGAJGA_SITE_DETAIL_URL = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_site_detail_url';

    /**
     * the column name for the vgajga_refreshed_at field
     */
    const COL_VGAJGA_REFRESHED_AT = 'videogames_site_jeuxvideocom_game_vgajga.vgajga_refreshed_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'GameId', 'Name', 'ReleasedAt', 'SiteDetailUrl', 'RefreshedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'gameId', 'name', 'releasedAt', 'siteDetailUrl', 'refreshedAt', ),
        self::TYPE_COLNAME       => array(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, SiteJeuxVideoComGameTableMap::COL_VGAJGA_NAME, SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT, SiteJeuxVideoComGameTableMap::COL_VGAJGA_SITE_DETAIL_URL, SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT, ),
        self::TYPE_FIELDNAME     => array('vgajga_id', 'vgajga_vgagam_id', 'vgajga_name', 'vgajga_released_at', 'vgajga_site_detail_url', 'vgajga_refreshed_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'GameId' => 1, 'Name' => 2, 'ReleasedAt' => 3, 'SiteDetailUrl' => 4, 'RefreshedAt' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'gameId' => 1, 'name' => 2, 'releasedAt' => 3, 'siteDetailUrl' => 4, 'refreshedAt' => 5, ),
        self::TYPE_COLNAME       => array(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID => 0, SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID => 1, SiteJeuxVideoComGameTableMap::COL_VGAJGA_NAME => 2, SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT => 3, SiteJeuxVideoComGameTableMap::COL_VGAJGA_SITE_DETAIL_URL => 4, SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT => 5, ),
        self::TYPE_FIELDNAME     => array('vgajga_id' => 0, 'vgajga_vgagam_id' => 1, 'vgajga_name' => 2, 'vgajga_released_at' => 3, 'vgajga_site_detail_url' => 4, 'vgajga_refreshed_at' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_site_jeuxvideocom_game_vgajga');
        $this->setPhpName('SiteJeuxVideoComGame');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGame');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgajga_id', 'Id', 'BIGINT', true, null, null);
        $this->addForeignKey('vgajga_vgagam_id', 'GameId', 'INTEGER', 'videogames_game_vgagam', 'vgagam_id', false, null, null);
        $this->addColumn('vgajga_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('vgajga_released_at', 'ReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgajga_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgajga_refreshed_at', 'RefreshedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Game', '\\IiMedias\\VideoGamesBundle\\Model\\Game', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgajga_vgagam_id',
    1 => ':vgagam_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('SiteJeuxVideoComGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgajrl_vgajga_id',
    1 => ':vgajga_id',
  ),
), 'CASCADE', 'CASCADE', 'SiteJeuxVideoComGameReleases', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_site_jeuxvideocom_game_vgajga     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteJeuxVideoComGameReleaseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteJeuxVideoComGameTableMap::CLASS_DEFAULT : SiteJeuxVideoComGameTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteJeuxVideoComGame object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteJeuxVideoComGameTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteJeuxVideoComGameTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteJeuxVideoComGameTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteJeuxVideoComGameTableMap::OM_CLASS;
            /** @var SiteJeuxVideoComGame $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteJeuxVideoComGameTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteJeuxVideoComGameTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteJeuxVideoComGameTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteJeuxVideoComGame $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteJeuxVideoComGameTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID);
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID);
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_NAME);
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT);
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_SITE_DETAIL_URL);
            $criteria->addSelectColumn(SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgajga_id');
            $criteria->addSelectColumn($alias . '.vgajga_vgagam_id');
            $criteria->addSelectColumn($alias . '.vgajga_name');
            $criteria->addSelectColumn($alias . '.vgajga_released_at');
            $criteria->addSelectColumn($alias . '.vgajga_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgajga_refreshed_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteJeuxVideoComGameTableMap::DATABASE_NAME)->getTable(SiteJeuxVideoComGameTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteJeuxVideoComGameTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteJeuxVideoComGameTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteJeuxVideoComGame or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteJeuxVideoComGame object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
            $criteria->add(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, (array) $values, Criteria::IN);
        }

        $query = SiteJeuxVideoComGameQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteJeuxVideoComGameTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteJeuxVideoComGameTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_site_jeuxvideocom_game_vgajga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteJeuxVideoComGameQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteJeuxVideoComGame or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteJeuxVideoComGame object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteJeuxVideoComGame object
        }


        // Set the correct dbName
        $query = SiteJeuxVideoComGameQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteJeuxVideoComGameTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteJeuxVideoComGameTableMap::buildTableMap();
