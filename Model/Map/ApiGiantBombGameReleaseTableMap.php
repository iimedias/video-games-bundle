<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_release_vgagrl' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombGameReleaseTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombGameReleaseTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_release_vgagrl';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombGameRelease';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 28;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 28;

    /**
     * the column name for the vgagrl_id field
     */
    const COL_VGAGRL_ID = 'videogames_api_giantbomb_release_vgagrl.vgagrl_id';

    /**
     * the column name for the vgagrl_vgagga_id field
     */
    const COL_VGAGRL_VGAGGA_ID = 'videogames_api_giantbomb_release_vgagrl.vgagrl_vgagga_id';

    /**
     * the column name for the vgagrl_vgagpl_id field
     */
    const COL_VGAGRL_VGAGPL_ID = 'videogames_api_giantbomb_release_vgagrl.vgagrl_vgagpl_id';

    /**
     * the column name for the vgagrl_vgaggr_id field
     */
    const COL_VGAGRL_VGAGGR_ID = 'videogames_api_giantbomb_release_vgagrl.vgagrl_vgaggr_id';

    /**
     * the column name for the vgagrl_vgagre_id field
     */
    const COL_VGAGRL_VGAGRE_ID = 'videogames_api_giantbomb_release_vgagrl.vgagrl_vgagre_id';

    /**
     * the column name for the vgaggl_name field
     */
    const COL_VGAGGL_NAME = 'videogames_api_giantbomb_release_vgagrl.vgaggl_name';

    /**
     * the column name for the vgagrl_summary field
     */
    const COL_VGAGRL_SUMMARY = 'videogames_api_giantbomb_release_vgagrl.vgagrl_summary';

    /**
     * the column name for the vgagrl_description field
     */
    const COL_VGAGRL_DESCRIPTION = 'videogames_api_giantbomb_release_vgagrl.vgagrl_description';

    /**
     * the column name for the vgagrl_minimum_players field
     */
    const COL_VGAGRL_MINIMUM_PLAYERS = 'videogames_api_giantbomb_release_vgagrl.vgagrl_minimum_players';

    /**
     * the column name for the vgagrl_maximum_players field
     */
    const COL_VGAGRL_MAXIMUM_PLAYERS = 'videogames_api_giantbomb_release_vgagrl.vgagrl_maximum_players';

    /**
     * the column name for the vgagrl_widescreen_support field
     */
    const COL_VGAGRL_WIDESCREEN_SUPPORT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_widescreen_support';

    /**
     * the column name for the vgagrl_product_code_value field
     */
    const COL_VGAGRL_PRODUCT_CODE_VALUE = 'videogames_api_giantbomb_release_vgagrl.vgagrl_product_code_value';

    /**
     * the column name for the vgagrl_original_released_at field
     */
    const COL_VGAGRL_ORIGINAL_RELEASED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_original_released_at';

    /**
     * the column name for the vgagrl_expected_day_released_at field
     */
    const COL_VGAGRL_EXPECTED_DAY_RELEASED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_expected_day_released_at';

    /**
     * the column name for the vgagrl_expected_month_released_at field
     */
    const COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_expected_month_released_at';

    /**
     * the column name for the vgagrl_expected_quarter_released_at field
     */
    const COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_expected_quarter_released_at';

    /**
     * the column name for the vgagrl_expected_year_released_at field
     */
    const COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_expected_year_released_at';

    /**
     * the column name for the vgagrl_image_icon_url field
     */
    const COL_VGAGRL_IMAGE_ICON_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_icon_url';

    /**
     * the column name for the vgagrl_image_medium_url field
     */
    const COL_VGAGRL_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_medium_url';

    /**
     * the column name for the vgagrl_image_screen_url field
     */
    const COL_VGAGRL_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_screen_url';

    /**
     * the column name for the vgagrl_image_small_url field
     */
    const COL_VGAGRL_IMAGE_SMALL_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_small_url';

    /**
     * the column name for the vgagrl_image_super_url field
     */
    const COL_VGAGRL_IMAGE_SUPER_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_super_url';

    /**
     * the column name for the vgagrl_image_thumb_url field
     */
    const COL_VGAGRL_IMAGE_THUMB_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_thumb_url';

    /**
     * the column name for the vgagrl_image_tiny_url field
     */
    const COL_VGAGRL_IMAGE_TINY_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_image_tiny_url';

    /**
     * the column name for the vgagrl_api_detail_url field
     */
    const COL_VGAGRL_API_DETAIL_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_api_detail_url';

    /**
     * the column name for the vgagrl_site_detail_url field
     */
    const COL_VGAGRL_SITE_DETAIL_URL = 'videogames_api_giantbomb_release_vgagrl.vgagrl_site_detail_url';

    /**
     * the column name for the vgagrl_created_at field
     */
    const COL_VGAGRL_CREATED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_created_at';

    /**
     * the column name for the vgagrl_updated_at field
     */
    const COL_VGAGRL_UPDATED_AT = 'videogames_api_giantbomb_release_vgagrl.vgagrl_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ApiGiantBombGameId', 'ApiGiantBombPlatformId', 'ApiGiantBombGameRatingId', 'ApiGiantBombRegionId', 'Name', 'Summary', 'Description', 'MinimumPlayers', 'MaximumPlayers', 'WidescreenSupport', 'ProductCodeValue', 'OriginalReleasedAt', 'ExpectedDayReleasedAt', 'ExpectedMonthReleasedAt', 'ExpectedQuarterReleasedAt', 'ExpectedYearReleasedAt', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'ApiDetailUrl', 'SiteDetailUrl', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'apiGiantBombGameId', 'apiGiantBombPlatformId', 'apiGiantBombGameRatingId', 'apiGiantBombRegionId', 'name', 'summary', 'description', 'minimumPlayers', 'maximumPlayers', 'widescreenSupport', 'productCodeValue', 'originalReleasedAt', 'expectedDayReleasedAt', 'expectedMonthReleasedAt', 'expectedQuarterReleasedAt', 'expectedYearReleasedAt', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'apiDetailUrl', 'siteDetailUrl', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagrl_id', 'vgagrl_vgagga_id', 'vgagrl_vgagpl_id', 'vgagrl_vgaggr_id', 'vgagrl_vgagre_id', 'vgaggl_name', 'vgagrl_summary', 'vgagrl_description', 'vgagrl_minimum_players', 'vgagrl_maximum_players', 'vgagrl_widescreen_support', 'vgagrl_product_code_value', 'vgagrl_original_released_at', 'vgagrl_expected_day_released_at', 'vgagrl_expected_month_released_at', 'vgagrl_expected_quarter_released_at', 'vgagrl_expected_year_released_at', 'vgagrl_image_icon_url', 'vgagrl_image_medium_url', 'vgagrl_image_screen_url', 'vgagrl_image_small_url', 'vgagrl_image_super_url', 'vgagrl_image_thumb_url', 'vgagrl_image_tiny_url', 'vgagrl_api_detail_url', 'vgagrl_site_detail_url', 'vgagrl_created_at', 'vgagrl_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ApiGiantBombGameId' => 1, 'ApiGiantBombPlatformId' => 2, 'ApiGiantBombGameRatingId' => 3, 'ApiGiantBombRegionId' => 4, 'Name' => 5, 'Summary' => 6, 'Description' => 7, 'MinimumPlayers' => 8, 'MaximumPlayers' => 9, 'WidescreenSupport' => 10, 'ProductCodeValue' => 11, 'OriginalReleasedAt' => 12, 'ExpectedDayReleasedAt' => 13, 'ExpectedMonthReleasedAt' => 14, 'ExpectedQuarterReleasedAt' => 15, 'ExpectedYearReleasedAt' => 16, 'ImageIconUrl' => 17, 'ImageMediumUrl' => 18, 'ImageScreenUrl' => 19, 'ImageSmallUrl' => 20, 'ImageSuperUrl' => 21, 'ImageThumbUrl' => 22, 'ImageTinyUrl' => 23, 'ApiDetailUrl' => 24, 'SiteDetailUrl' => 25, 'CreatedAt' => 26, 'UpdatedAt' => 27, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'apiGiantBombGameId' => 1, 'apiGiantBombPlatformId' => 2, 'apiGiantBombGameRatingId' => 3, 'apiGiantBombRegionId' => 4, 'name' => 5, 'summary' => 6, 'description' => 7, 'minimumPlayers' => 8, 'maximumPlayers' => 9, 'widescreenSupport' => 10, 'productCodeValue' => 11, 'originalReleasedAt' => 12, 'expectedDayReleasedAt' => 13, 'expectedMonthReleasedAt' => 14, 'expectedQuarterReleasedAt' => 15, 'expectedYearReleasedAt' => 16, 'imageIconUrl' => 17, 'imageMediumUrl' => 18, 'imageScreenUrl' => 19, 'imageSmallUrl' => 20, 'imageSuperUrl' => 21, 'imageThumbUrl' => 22, 'imageTinyUrl' => 23, 'apiDetailUrl' => 24, 'siteDetailUrl' => 25, 'createdAt' => 26, 'updatedAt' => 27, ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID => 0, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID => 1, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID => 2, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID => 3, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID => 4, ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME => 5, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY => 6, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION => 7, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS => 8, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS => 9, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT => 10, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE => 11, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT => 12, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT => 13, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT => 14, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT => 15, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT => 16, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL => 17, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL => 18, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL => 19, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL => 20, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL => 21, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL => 22, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL => 23, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL => 24, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL => 25, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT => 26, ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT => 27, ),
        self::TYPE_FIELDNAME     => array('vgagrl_id' => 0, 'vgagrl_vgagga_id' => 1, 'vgagrl_vgagpl_id' => 2, 'vgagrl_vgaggr_id' => 3, 'vgagrl_vgagre_id' => 4, 'vgaggl_name' => 5, 'vgagrl_summary' => 6, 'vgagrl_description' => 7, 'vgagrl_minimum_players' => 8, 'vgagrl_maximum_players' => 9, 'vgagrl_widescreen_support' => 10, 'vgagrl_product_code_value' => 11, 'vgagrl_original_released_at' => 12, 'vgagrl_expected_day_released_at' => 13, 'vgagrl_expected_month_released_at' => 14, 'vgagrl_expected_quarter_released_at' => 15, 'vgagrl_expected_year_released_at' => 16, 'vgagrl_image_icon_url' => 17, 'vgagrl_image_medium_url' => 18, 'vgagrl_image_screen_url' => 19, 'vgagrl_image_small_url' => 20, 'vgagrl_image_super_url' => 21, 'vgagrl_image_thumb_url' => 22, 'vgagrl_image_tiny_url' => 23, 'vgagrl_api_detail_url' => 24, 'vgagrl_site_detail_url' => 25, 'vgagrl_created_at' => 26, 'vgagrl_updated_at' => 27, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_release_vgagrl');
        $this->setPhpName('ApiGiantBombGameRelease');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagrl_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('vgagrl_vgagga_id', 'ApiGiantBombGameId', 'INTEGER', 'videogames_api_giantbomb_game_vgagga', 'vgagga_id', true, null, null);
        $this->addForeignKey('vgagrl_vgagpl_id', 'ApiGiantBombPlatformId', 'INTEGER', 'videogames_api_giantbomb_platform_vgagpl', 'vgagpl_id', false, null, null);
        $this->addForeignKey('vgagrl_vgaggr_id', 'ApiGiantBombGameRatingId', 'INTEGER', 'videogames_api_giantbomb_game_rating_vgaggr', 'vgaggr_id', false, null, null);
        $this->addForeignKey('vgagrl_vgagre_id', 'ApiGiantBombRegionId', 'INTEGER', 'videogames_api_giantbomb_region_vgagre', 'vgagre_id', false, null, null);
        $this->addColumn('vgaggl_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrl_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_minimum_players', 'MinimumPlayers', 'INTEGER', false, null, null);
        $this->addColumn('vgagrl_maximum_players', 'MaximumPlayers', 'INTEGER', false, null, null);
        $this->addColumn('vgagrl_widescreen_support', 'WidescreenSupport', 'BOOLEAN', false, 1, null);
        $this->addColumn('vgagrl_product_code_value', 'ProductCodeValue', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrl_original_released_at', 'OriginalReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_expected_day_released_at', 'ExpectedDayReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_expected_month_released_at', 'ExpectedMonthReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_expected_quarter_released_at', 'ExpectedQuarterReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_expected_year_released_at', 'ExpectedYearReleasedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrl_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrl_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrl_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrl_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagrl_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombPlatform', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagrl_vgagpl_id',
    1 => ':vgagpl_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombGameRating', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagrl_vgaggr_id',
    1 => ':vgaggr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombRegion', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRegion', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagrl_vgagre_id',
    1 => ':vgagre_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombGameReleaseDeveloper', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameReleaseDeveloper', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrd_vgagrl_id',
    1 => ':vgagrl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleaseDevelopers', false);
        $this->addRelation('ApiGiantBombGameReleasePublisher', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameReleasePublisher', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrp_vgagrl_id',
    1 => ':vgagrl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleasePublishers', false);
        $this->addRelation('ApiGiantBombImage', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombImage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagim_vgagrl_id',
    1 => ':vgagrl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombImages', false);
        $this->addRelation('ApiGiantBombVideo', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombVideo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagvi_vgagrl_id',
    1 => ':vgagrl_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombVideos', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_release_vgagrl     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombGameReleaseDeveloperTableMap::clearInstancePool();
        ApiGiantBombGameReleasePublisherTableMap::clearInstancePool();
        ApiGiantBombImageTableMap::clearInstancePool();
        ApiGiantBombVideoTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombGameReleaseTableMap::CLASS_DEFAULT : ApiGiantBombGameReleaseTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombGameRelease object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombGameReleaseTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombGameReleaseTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombGameReleaseTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombGameReleaseTableMap::OM_CLASS;
            /** @var ApiGiantBombGameRelease $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombGameReleaseTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombGameReleaseTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombGameReleaseTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombGameRelease $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombGameReleaseTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagrl_id');
            $criteria->addSelectColumn($alias . '.vgagrl_vgagga_id');
            $criteria->addSelectColumn($alias . '.vgagrl_vgagpl_id');
            $criteria->addSelectColumn($alias . '.vgagrl_vgaggr_id');
            $criteria->addSelectColumn($alias . '.vgagrl_vgagre_id');
            $criteria->addSelectColumn($alias . '.vgaggl_name');
            $criteria->addSelectColumn($alias . '.vgagrl_summary');
            $criteria->addSelectColumn($alias . '.vgagrl_description');
            $criteria->addSelectColumn($alias . '.vgagrl_minimum_players');
            $criteria->addSelectColumn($alias . '.vgagrl_maximum_players');
            $criteria->addSelectColumn($alias . '.vgagrl_widescreen_support');
            $criteria->addSelectColumn($alias . '.vgagrl_product_code_value');
            $criteria->addSelectColumn($alias . '.vgagrl_original_released_at');
            $criteria->addSelectColumn($alias . '.vgagrl_expected_day_released_at');
            $criteria->addSelectColumn($alias . '.vgagrl_expected_month_released_at');
            $criteria->addSelectColumn($alias . '.vgagrl_expected_quarter_released_at');
            $criteria->addSelectColumn($alias . '.vgagrl_expected_year_released_at');
            $criteria->addSelectColumn($alias . '.vgagrl_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagrl_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagrl_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagrl_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagrl_created_at');
            $criteria->addSelectColumn($alias . '.vgagrl_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameReleaseTableMap::DATABASE_NAME)->getTable(ApiGiantBombGameReleaseTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombGameReleaseTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombGameReleaseTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombGameRelease or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombGameRelease object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombGameReleaseQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombGameReleaseTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombGameReleaseTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_release_vgagrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombGameReleaseQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombGameRelease or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombGameRelease object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombGameRelease object
        }


        // Set the correct dbName
        $query = ApiGiantBombGameReleaseQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombGameReleaseTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombGameReleaseTableMap::buildTableMap();
