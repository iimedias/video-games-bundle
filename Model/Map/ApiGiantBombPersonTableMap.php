<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_person_vgagpe' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombPersonTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombPersonTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_person_vgagpe';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPerson';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombPerson';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the vgagpe_id field
     */
    const COL_VGAGPE_ID = 'videogames_api_giantbomb_person_vgagpe.vgagpe_id';

    /**
     * the column name for the vgagpe_name field
     */
    const COL_VGAGPE_NAME = 'videogames_api_giantbomb_person_vgagpe.vgagpe_name';

    /**
     * the column name for the vgagpe_aliases field
     */
    const COL_VGAGPE_ALIASES = 'videogames_api_giantbomb_person_vgagpe.vgagpe_aliases';

    /**
     * the column name for the vgagpe_summary field
     */
    const COL_VGAGPE_SUMMARY = 'videogames_api_giantbomb_person_vgagpe.vgagpe_summary';

    /**
     * the column name for the vgagpe_description field
     */
    const COL_VGAGPE_DESCRIPTION = 'videogames_api_giantbomb_person_vgagpe.vgagpe_description';

    /**
     * the column name for the vgagpe_gender field
     */
    const COL_VGAGPE_GENDER = 'videogames_api_giantbomb_person_vgagpe.vgagpe_gender';

    /**
     * the column name for the vgagpe_birthdate field
     */
    const COL_VGAGPE_BIRTHDATE = 'videogames_api_giantbomb_person_vgagpe.vgagpe_birthdate';

    /**
     * the column name for the vgagpe_deathdate field
     */
    const COL_VGAGPE_DEATHDATE = 'videogames_api_giantbomb_person_vgagpe.vgagpe_deathdate';

    /**
     * the column name for the vgagpe_hometown field
     */
    const COL_VGAGPE_HOMETOWN = 'videogames_api_giantbomb_person_vgagpe.vgagpe_hometown';

    /**
     * the column name for the vgagpe_country field
     */
    const COL_VGAGPE_COUNTRY = 'videogames_api_giantbomb_person_vgagpe.vgagpe_country';

    /**
     * the column name for the vgagpe_api_detail_url field
     */
    const COL_VGAGPE_API_DETAIL_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_api_detail_url';

    /**
     * the column name for the vgagpe_site_detail_url field
     */
    const COL_VGAGPE_SITE_DETAIL_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_site_detail_url';

    /**
     * the column name for the vgagpe_image_icon_url field
     */
    const COL_VGAGPE_IMAGE_ICON_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_icon_url';

    /**
     * the column name for the vgagpe_image_medium_url field
     */
    const COL_VGAGPE_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_medium_url';

    /**
     * the column name for the vgagpe_image_screen_url field
     */
    const COL_VGAGPE_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_screen_url';

    /**
     * the column name for the vgagpe_image_small_url field
     */
    const COL_VGAGPE_IMAGE_SMALL_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_small_url';

    /**
     * the column name for the vgagpe_image_super_url field
     */
    const COL_VGAGPE_IMAGE_SUPER_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_super_url';

    /**
     * the column name for the vgagpe_image_thumb_url field
     */
    const COL_VGAGPE_IMAGE_THUMB_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_thumb_url';

    /**
     * the column name for the vgagpe_image_tiny_url field
     */
    const COL_VGAGPE_IMAGE_TINY_URL = 'videogames_api_giantbomb_person_vgagpe.vgagpe_image_tiny_url';

    /**
     * the column name for the vgagpe_credited_vgagga_id field
     */
    const COL_VGAGPE_CREDITED_VGAGGA_ID = 'videogames_api_giantbomb_person_vgagpe.vgagpe_credited_vgagga_id';

    /**
     * the column name for the vgagpe_created_at field
     */
    const COL_VGAGPE_CREATED_AT = 'videogames_api_giantbomb_person_vgagpe.vgagpe_created_at';

    /**
     * the column name for the vgagpe_updated_at field
     */
    const COL_VGAGPE_UPDATED_AT = 'videogames_api_giantbomb_person_vgagpe.vgagpe_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Aliases', 'Summary', 'Description', 'Gender', 'BirthDate', 'DeathDate', 'Hometown', 'Country', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'GiantBombGameId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'aliases', 'summary', 'description', 'gender', 'birthDate', 'deathDate', 'hometown', 'country', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'giantBombGameId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, ApiGiantBombPersonTableMap::COL_VGAGPE_NAME, ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES, ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY, ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION, ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER, ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE, ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE, ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN, ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY, ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL, ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT, ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagpe_id', 'vgagpe_name', 'vgagpe_aliases', 'vgagpe_summary', 'vgagpe_description', 'vgagpe_gender', 'vgagpe_birthdate', 'vgagpe_deathdate', 'vgagpe_hometown', 'vgagpe_country', 'vgagpe_api_detail_url', 'vgagpe_site_detail_url', 'vgagpe_image_icon_url', 'vgagpe_image_medium_url', 'vgagpe_image_screen_url', 'vgagpe_image_small_url', 'vgagpe_image_super_url', 'vgagpe_image_thumb_url', 'vgagpe_image_tiny_url', 'vgagpe_credited_vgagga_id', 'vgagpe_created_at', 'vgagpe_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Aliases' => 2, 'Summary' => 3, 'Description' => 4, 'Gender' => 5, 'BirthDate' => 6, 'DeathDate' => 7, 'Hometown' => 8, 'Country' => 9, 'ApiDetailUrl' => 10, 'SiteDetailUrl' => 11, 'ImageIconUrl' => 12, 'ImageMediumUrl' => 13, 'ImageScreenUrl' => 14, 'ImageSmallUrl' => 15, 'ImageSuperUrl' => 16, 'ImageThumbUrl' => 17, 'ImageTinyUrl' => 18, 'GiantBombGameId' => 19, 'CreatedAt' => 20, 'UpdatedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'aliases' => 2, 'summary' => 3, 'description' => 4, 'gender' => 5, 'birthDate' => 6, 'deathDate' => 7, 'hometown' => 8, 'country' => 9, 'apiDetailUrl' => 10, 'siteDetailUrl' => 11, 'imageIconUrl' => 12, 'imageMediumUrl' => 13, 'imageScreenUrl' => 14, 'imageSmallUrl' => 15, 'imageSuperUrl' => 16, 'imageThumbUrl' => 17, 'imageTinyUrl' => 18, 'giantBombGameId' => 19, 'createdAt' => 20, 'updatedAt' => 21, ),
        self::TYPE_COLNAME       => array(ApiGiantBombPersonTableMap::COL_VGAGPE_ID => 0, ApiGiantBombPersonTableMap::COL_VGAGPE_NAME => 1, ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES => 2, ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY => 3, ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION => 4, ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER => 5, ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE => 6, ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE => 7, ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN => 8, ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY => 9, ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL => 10, ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL => 11, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL => 12, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL => 13, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL => 14, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL => 15, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL => 16, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL => 17, ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL => 18, ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID => 19, ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT => 20, ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('vgagpe_id' => 0, 'vgagpe_name' => 1, 'vgagpe_aliases' => 2, 'vgagpe_summary' => 3, 'vgagpe_description' => 4, 'vgagpe_gender' => 5, 'vgagpe_birthdate' => 6, 'vgagpe_deathdate' => 7, 'vgagpe_hometown' => 8, 'vgagpe_country' => 9, 'vgagpe_api_detail_url' => 10, 'vgagpe_site_detail_url' => 11, 'vgagpe_image_icon_url' => 12, 'vgagpe_image_medium_url' => 13, 'vgagpe_image_screen_url' => 14, 'vgagpe_image_small_url' => 15, 'vgagpe_image_super_url' => 16, 'vgagpe_image_thumb_url' => 17, 'vgagpe_image_tiny_url' => 18, 'vgagpe_credited_vgagga_id' => 19, 'vgagpe_created_at' => 20, 'vgagpe_updated_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_person_vgagpe');
        $this->setPhpName('ApiGiantBombPerson');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPerson');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagpe_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagpe_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpe_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagpe_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_gender', 'Gender', 'INTEGER', false, null, null);
        $this->addColumn('vgagpe_birthdate', 'BirthDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagpe_deathdate', 'DeathDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagpe_hometown', 'Hometown', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpe_country', 'Country', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpe_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpe_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagpe_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagpe_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('vgagpe_credited_vgagga_id', 'GiantBombGameId', 'INTEGER', 'videogames_api_giantbomb_game_vgagga', 'vgagga_id', false, null, null);
        $this->addColumn('vgagpe_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagpe_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombCreditedGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagpe_credited_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombPersonTableMap::CLASS_DEFAULT : ApiGiantBombPersonTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombPerson object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombPersonTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombPersonTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombPersonTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombPersonTableMap::OM_CLASS;
            /** @var ApiGiantBombPerson $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombPersonTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombPersonTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombPersonTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombPerson $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombPersonTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_ID);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_NAME);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagpe_id');
            $criteria->addSelectColumn($alias . '.vgagpe_name');
            $criteria->addSelectColumn($alias . '.vgagpe_aliases');
            $criteria->addSelectColumn($alias . '.vgagpe_summary');
            $criteria->addSelectColumn($alias . '.vgagpe_description');
            $criteria->addSelectColumn($alias . '.vgagpe_gender');
            $criteria->addSelectColumn($alias . '.vgagpe_birthdate');
            $criteria->addSelectColumn($alias . '.vgagpe_deathdate');
            $criteria->addSelectColumn($alias . '.vgagpe_hometown');
            $criteria->addSelectColumn($alias . '.vgagpe_country');
            $criteria->addSelectColumn($alias . '.vgagpe_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagpe_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagpe_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagpe_credited_vgagga_id');
            $criteria->addSelectColumn($alias . '.vgagpe_created_at');
            $criteria->addSelectColumn($alias . '.vgagpe_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombPersonTableMap::DATABASE_NAME)->getTable(ApiGiantBombPersonTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombPersonTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombPersonTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombPersonTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombPerson or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombPerson object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombPersonTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombPersonQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombPersonTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombPersonTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_person_vgagpe table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombPersonQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombPerson or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombPerson object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombPerson object
        }


        // Set the correct dbName
        $query = ApiGiantBombPersonQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombPersonTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombPersonTableMap::buildTableMap();
