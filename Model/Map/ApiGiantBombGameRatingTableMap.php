<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_game_rating_vgaggr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombGameRatingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombGameRatingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_game_rating_vgaggr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombGameRating';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the vgaggr_id field
     */
    const COL_VGAGGR_ID = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_id';

    /**
     * the column name for the vgaggr_name field
     */
    const COL_VGAGGR_NAME = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_name';

    /**
     * the column name for the vgaggr_api_detail_url field
     */
    const COL_VGAGGR_API_DETAIL_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_api_detail_url';

    /**
     * the column name for the vgaggr_image_icon_url field
     */
    const COL_VGAGGR_IMAGE_ICON_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_icon_url';

    /**
     * the column name for the vgaggr_image_medium_url field
     */
    const COL_VGAGGR_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_medium_url';

    /**
     * the column name for the vgaggr_image_screen_url field
     */
    const COL_VGAGGR_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_screen_url';

    /**
     * the column name for the vgaggr_image_small_url field
     */
    const COL_VGAGGR_IMAGE_SMALL_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_small_url';

    /**
     * the column name for the vgaggr_image_super_url field
     */
    const COL_VGAGGR_IMAGE_SUPER_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_super_url';

    /**
     * the column name for the vgaggr_image_thumb_url field
     */
    const COL_VGAGGR_IMAGE_THUMB_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_thumb_url';

    /**
     * the column name for the vgaggr_image_tiny_url field
     */
    const COL_VGAGGR_IMAGE_TINY_URL = 'videogames_api_giantbomb_game_rating_vgaggr.vgaggr_image_tiny_url';

    /**
     * the column name for the vgagch_vgagrb_id field
     */
    const COL_VGAGCH_VGAGRB_ID = 'videogames_api_giantbomb_game_rating_vgaggr.vgagch_vgagrb_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'ApiDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'GiantBombRatingBoardId', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'apiDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'giantBombRatingBoardId', ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME, ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL, ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, ),
        self::TYPE_FIELDNAME     => array('vgaggr_id', 'vgaggr_name', 'vgaggr_api_detail_url', 'vgaggr_image_icon_url', 'vgaggr_image_medium_url', 'vgaggr_image_screen_url', 'vgaggr_image_small_url', 'vgaggr_image_super_url', 'vgaggr_image_thumb_url', 'vgaggr_image_tiny_url', 'vgagch_vgagrb_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'ApiDetailUrl' => 2, 'ImageIconUrl' => 3, 'ImageMediumUrl' => 4, 'ImageScreenUrl' => 5, 'ImageSmallUrl' => 6, 'ImageSuperUrl' => 7, 'ImageThumbUrl' => 8, 'ImageTinyUrl' => 9, 'GiantBombRatingBoardId' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'apiDetailUrl' => 2, 'imageIconUrl' => 3, 'imageMediumUrl' => 4, 'imageScreenUrl' => 5, 'imageSmallUrl' => 6, 'imageSuperUrl' => 7, 'imageThumbUrl' => 8, 'imageTinyUrl' => 9, 'giantBombRatingBoardId' => 10, ),
        self::TYPE_COLNAME       => array(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID => 0, ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME => 1, ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL => 2, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL => 3, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL => 4, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL => 5, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL => 6, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL => 7, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL => 8, ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL => 9, ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID => 10, ),
        self::TYPE_FIELDNAME     => array('vgaggr_id' => 0, 'vgaggr_name' => 1, 'vgaggr_api_detail_url' => 2, 'vgaggr_image_icon_url' => 3, 'vgaggr_image_medium_url' => 4, 'vgaggr_image_screen_url' => 5, 'vgaggr_image_small_url' => 6, 'vgaggr_image_super_url' => 7, 'vgaggr_image_thumb_url' => 8, 'vgaggr_image_tiny_url' => 9, 'vgagch_vgagrb_id' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_game_rating_vgaggr');
        $this->setPhpName('ApiGiantBombGameRating');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgaggr_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgaggr_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaggr_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaggr_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaggr_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('vgagch_vgagrb_id', 'GiantBombRatingBoardId', 'INTEGER', 'videogames_api_giantbomb_rating_board_vgagrb', 'vgagrb_id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombRatingBoard', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRatingBoard', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagch_vgagrb_id',
    1 => ':vgagrb_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombGameOriginalRating', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameOriginalRating', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggo_vgaggr_id',
    1 => ':vgaggr_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameOriginalRatings', false);
        $this->addRelation('ApiGiantBombGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagrl_vgaggr_id',
    1 => ':vgaggr_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameReleases', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_game_rating_vgaggr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombGameOriginalRatingTableMap::clearInstancePool();
        ApiGiantBombGameReleaseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombGameRatingTableMap::CLASS_DEFAULT : ApiGiantBombGameRatingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombGameRating object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombGameRatingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombGameRatingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombGameRatingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombGameRatingTableMap::OM_CLASS;
            /** @var ApiGiantBombGameRating $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombGameRatingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombGameRatingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombGameRatingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombGameRating $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombGameRatingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID);
        } else {
            $criteria->addSelectColumn($alias . '.vgaggr_id');
            $criteria->addSelectColumn($alias . '.vgaggr_name');
            $criteria->addSelectColumn($alias . '.vgaggr_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_small_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_super_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgaggr_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagch_vgagrb_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameRatingTableMap::DATABASE_NAME)->getTable(ApiGiantBombGameRatingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombGameRatingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombGameRatingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombGameRating or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombGameRating object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombGameRatingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombGameRatingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombGameRatingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_game_rating_vgaggr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombGameRatingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombGameRating or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombGameRating object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombGameRating object
        }


        // Set the correct dbName
        $query = ApiGiantBombGameRatingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombGameRatingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombGameRatingTableMap::buildTableMap();
