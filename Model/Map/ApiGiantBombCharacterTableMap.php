<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_character_vgagch' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombCharacterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombCharacterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_character_vgagch';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCharacter';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombCharacter';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the vgagch_id field
     */
    const COL_VGAGCH_ID = 'videogames_api_giantbomb_character_vgagch.vgagch_id';

    /**
     * the column name for the vgagch_name field
     */
    const COL_VGAGCH_NAME = 'videogames_api_giantbomb_character_vgagch.vgagch_name';

    /**
     * the column name for the vgagch_aliases field
     */
    const COL_VGAGCH_ALIASES = 'videogames_api_giantbomb_character_vgagch.vgagch_aliases';

    /**
     * the column name for the vgagch_summary field
     */
    const COL_VGAGCH_SUMMARY = 'videogames_api_giantbomb_character_vgagch.vgagch_summary';

    /**
     * the column name for the vgagch_description field
     */
    const COL_VGAGCH_DESCRIPTION = 'videogames_api_giantbomb_character_vgagch.vgagch_description';

    /**
     * the column name for the vgagch_real_name field
     */
    const COL_VGAGCH_REAL_NAME = 'videogames_api_giantbomb_character_vgagch.vgagch_real_name';

    /**
     * the column name for the vgagch_last_name field
     */
    const COL_VGAGCH_LAST_NAME = 'videogames_api_giantbomb_character_vgagch.vgagch_last_name';

    /**
     * the column name for the vgagch_gender field
     */
    const COL_VGAGCH_GENDER = 'videogames_api_giantbomb_character_vgagch.vgagch_gender';

    /**
     * the column name for the vgagch_birthday field
     */
    const COL_VGAGCH_BIRTHDAY = 'videogames_api_giantbomb_character_vgagch.vgagch_birthday';

    /**
     * the column name for the vgagch_api_detail_url field
     */
    const COL_VGAGCH_API_DETAIL_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_api_detail_url';

    /**
     * the column name for the vgagch_site_detail_url field
     */
    const COL_VGAGCH_SITE_DETAIL_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_site_detail_url';

    /**
     * the column name for the vgagch_image_icon_url field
     */
    const COL_VGAGCH_IMAGE_ICON_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_icon_url';

    /**
     * the column name for the vgagch_image_medium_url field
     */
    const COL_VGAGCH_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_medium_url';

    /**
     * the column name for the vgagch_image_screen_url field
     */
    const COL_VGAGCH_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_screen_url';

    /**
     * the column name for the vgagch_image_small_url field
     */
    const COL_VGAGCH_IMAGE_SMALL_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_small_url';

    /**
     * the column name for the vgagch_image_super_url field
     */
    const COL_VGAGCH_IMAGE_SUPER_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_super_url';

    /**
     * the column name for the vgagch_image_thumb_url field
     */
    const COL_VGAGCH_IMAGE_THUMB_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_thumb_url';

    /**
     * the column name for the vgagch_image_tiny_url field
     */
    const COL_VGAGCH_IMAGE_TINY_URL = 'videogames_api_giantbomb_character_vgagch.vgagch_image_tiny_url';

    /**
     * the column name for the vgagch_first_vgagga_id field
     */
    const COL_VGAGCH_FIRST_VGAGGA_ID = 'videogames_api_giantbomb_character_vgagch.vgagch_first_vgagga_id';

    /**
     * the column name for the vgagch_created_at field
     */
    const COL_VGAGCH_CREATED_AT = 'videogames_api_giantbomb_character_vgagch.vgagch_created_at';

    /**
     * the column name for the vgagch_updated_at field
     */
    const COL_VGAGCH_UPDATED_AT = 'videogames_api_giantbomb_character_vgagch.vgagch_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Aliases', 'Summary', 'Description', 'RealName', 'LastName', 'Gender', 'Birthday', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'GiantBombGameId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'aliases', 'summary', 'description', 'realName', 'lastName', 'gender', 'birthday', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'giantBombGameId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, ApiGiantBombCharacterTableMap::COL_VGAGCH_NAME, ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES, ApiGiantBombCharacterTableMap::COL_VGAGCH_SUMMARY, ApiGiantBombCharacterTableMap::COL_VGAGCH_DESCRIPTION, ApiGiantBombCharacterTableMap::COL_VGAGCH_REAL_NAME, ApiGiantBombCharacterTableMap::COL_VGAGCH_LAST_NAME, ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER, ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY, ApiGiantBombCharacterTableMap::COL_VGAGCH_API_DETAIL_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_SITE_DETAIL_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_ICON_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_MEDIUM_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SCREEN_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SMALL_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SUPER_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_THUMB_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_TINY_URL, ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT, ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagch_id', 'vgagch_name', 'vgagch_aliases', 'vgagch_summary', 'vgagch_description', 'vgagch_real_name', 'vgagch_last_name', 'vgagch_gender', 'vgagch_birthday', 'vgagch_api_detail_url', 'vgagch_site_detail_url', 'vgagch_image_icon_url', 'vgagch_image_medium_url', 'vgagch_image_screen_url', 'vgagch_image_small_url', 'vgagch_image_super_url', 'vgagch_image_thumb_url', 'vgagch_image_tiny_url', 'vgagch_first_vgagga_id', 'vgagch_created_at', 'vgagch_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Aliases' => 2, 'Summary' => 3, 'Description' => 4, 'RealName' => 5, 'LastName' => 6, 'Gender' => 7, 'Birthday' => 8, 'ApiDetailUrl' => 9, 'SiteDetailUrl' => 10, 'ImageIconUrl' => 11, 'ImageMediumUrl' => 12, 'ImageScreenUrl' => 13, 'ImageSmallUrl' => 14, 'ImageSuperUrl' => 15, 'ImageThumbUrl' => 16, 'ImageTinyUrl' => 17, 'GiantBombGameId' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'aliases' => 2, 'summary' => 3, 'description' => 4, 'realName' => 5, 'lastName' => 6, 'gender' => 7, 'birthday' => 8, 'apiDetailUrl' => 9, 'siteDetailUrl' => 10, 'imageIconUrl' => 11, 'imageMediumUrl' => 12, 'imageScreenUrl' => 13, 'imageSmallUrl' => 14, 'imageSuperUrl' => 15, 'imageThumbUrl' => 16, 'imageTinyUrl' => 17, 'giantBombGameId' => 18, 'createdAt' => 19, 'updatedAt' => 20, ),
        self::TYPE_COLNAME       => array(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID => 0, ApiGiantBombCharacterTableMap::COL_VGAGCH_NAME => 1, ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES => 2, ApiGiantBombCharacterTableMap::COL_VGAGCH_SUMMARY => 3, ApiGiantBombCharacterTableMap::COL_VGAGCH_DESCRIPTION => 4, ApiGiantBombCharacterTableMap::COL_VGAGCH_REAL_NAME => 5, ApiGiantBombCharacterTableMap::COL_VGAGCH_LAST_NAME => 6, ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER => 7, ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY => 8, ApiGiantBombCharacterTableMap::COL_VGAGCH_API_DETAIL_URL => 9, ApiGiantBombCharacterTableMap::COL_VGAGCH_SITE_DETAIL_URL => 10, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_ICON_URL => 11, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_MEDIUM_URL => 12, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SCREEN_URL => 13, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SMALL_URL => 14, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SUPER_URL => 15, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_THUMB_URL => 16, ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_TINY_URL => 17, ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID => 18, ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT => 19, ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('vgagch_id' => 0, 'vgagch_name' => 1, 'vgagch_aliases' => 2, 'vgagch_summary' => 3, 'vgagch_description' => 4, 'vgagch_real_name' => 5, 'vgagch_last_name' => 6, 'vgagch_gender' => 7, 'vgagch_birthday' => 8, 'vgagch_api_detail_url' => 9, 'vgagch_site_detail_url' => 10, 'vgagch_image_icon_url' => 11, 'vgagch_image_medium_url' => 12, 'vgagch_image_screen_url' => 13, 'vgagch_image_small_url' => 14, 'vgagch_image_super_url' => 15, 'vgagch_image_thumb_url' => 16, 'vgagch_image_tiny_url' => 17, 'vgagch_first_vgagga_id' => 18, 'vgagch_created_at' => 19, 'vgagch_updated_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_character_vgagch');
        $this->setPhpName('ApiGiantBombCharacter');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCharacter');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagch_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagch_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagch_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagch_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_real_name', 'RealName', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagch_last_name', 'LastName', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagch_gender', 'Gender', 'INTEGER', false, null, null);
        $this->addColumn('vgagch_birthday', 'Birthday', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagch_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagch_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagch_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagch_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('vgagch_first_vgagga_id', 'GiantBombGameId', 'INTEGER', 'videogames_api_giantbomb_game_vgagga', 'vgagga_id', false, null, null);
        $this->addColumn('vgagch_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagch_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombFirstGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagch_first_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombCharacterTableMap::CLASS_DEFAULT : ApiGiantBombCharacterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombCharacter object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombCharacterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombCharacterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombCharacterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombCharacterTableMap::OM_CLASS;
            /** @var ApiGiantBombCharacter $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombCharacterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombCharacterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombCharacterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombCharacter $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombCharacterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_NAME);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_REAL_NAME);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_LAST_NAME);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagch_id');
            $criteria->addSelectColumn($alias . '.vgagch_name');
            $criteria->addSelectColumn($alias . '.vgagch_aliases');
            $criteria->addSelectColumn($alias . '.vgagch_summary');
            $criteria->addSelectColumn($alias . '.vgagch_description');
            $criteria->addSelectColumn($alias . '.vgagch_real_name');
            $criteria->addSelectColumn($alias . '.vgagch_last_name');
            $criteria->addSelectColumn($alias . '.vgagch_gender');
            $criteria->addSelectColumn($alias . '.vgagch_birthday');
            $criteria->addSelectColumn($alias . '.vgagch_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagch_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagch_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagch_first_vgagga_id');
            $criteria->addSelectColumn($alias . '.vgagch_created_at');
            $criteria->addSelectColumn($alias . '.vgagch_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombCharacterTableMap::DATABASE_NAME)->getTable(ApiGiantBombCharacterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombCharacterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombCharacterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombCharacter or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombCharacter object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombCharacterTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombCharacterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombCharacterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombCharacterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_character_vgagch table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombCharacterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombCharacter or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombCharacter object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombCharacter object
        }


        // Set the correct dbName
        $query = ApiGiantBombCharacterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombCharacterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombCharacterTableMap::buildTableMap();
