<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_franchise_vgagfr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombFranchiseTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombFranchiseTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_franchise_vgagfr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombFranchise';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombFranchise';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the vgagfr_id field
     */
    const COL_VGAGFR_ID = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_id';

    /**
     * the column name for the vgagfr_name field
     */
    const COL_VGAGFR_NAME = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_name';

    /**
     * the column name for the vgagfr_aliases field
     */
    const COL_VGAGFR_ALIASES = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_aliases';

    /**
     * the column name for the vgagfr_summary field
     */
    const COL_VGAGFR_SUMMARY = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_summary';

    /**
     * the column name for the vgagfr_description field
     */
    const COL_VGAGFR_DESCRIPTION = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_description';

    /**
     * the column name for the vgagfr_api_detail_url field
     */
    const COL_VGAGFR_API_DETAIL_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_api_detail_url';

    /**
     * the column name for the vgagfr_site_detail_url field
     */
    const COL_VGAGFR_SITE_DETAIL_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_site_detail_url';

    /**
     * the column name for the vgagfr_image_icon_url field
     */
    const COL_VGAGFR_IMAGE_ICON_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_icon_url';

    /**
     * the column name for the vgagfr_image_medium_url field
     */
    const COL_VGAGFR_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_medium_url';

    /**
     * the column name for the vgagfr_image_screen_url field
     */
    const COL_VGAGFR_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_screen_url';

    /**
     * the column name for the vgagfr_image_small_url field
     */
    const COL_VGAGFR_IMAGE_SMALL_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_small_url';

    /**
     * the column name for the vgagfr_image_super_url field
     */
    const COL_VGAGFR_IMAGE_SUPER_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_super_url';

    /**
     * the column name for the vgagfr_image_thumb_url field
     */
    const COL_VGAGFR_IMAGE_THUMB_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_thumb_url';

    /**
     * the column name for the vgagfr_image_tiny_url field
     */
    const COL_VGAGFR_IMAGE_TINY_URL = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_image_tiny_url';

    /**
     * the column name for the vgagfr_created_at field
     */
    const COL_VGAGFR_CREATED_AT = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_created_at';

    /**
     * the column name for the vgagfr_updated_at field
     */
    const COL_VGAGFR_UPDATED_AT = 'videogames_api_giantbomb_franchise_vgagfr.vgagfr_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Aliases', 'Summary', 'Description', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'aliases', 'summary', 'description', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME, ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES, ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY, ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION, ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL, ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT, ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagfr_id', 'vgagfr_name', 'vgagfr_aliases', 'vgagfr_summary', 'vgagfr_description', 'vgagfr_api_detail_url', 'vgagfr_site_detail_url', 'vgagfr_image_icon_url', 'vgagfr_image_medium_url', 'vgagfr_image_screen_url', 'vgagfr_image_small_url', 'vgagfr_image_super_url', 'vgagfr_image_thumb_url', 'vgagfr_image_tiny_url', 'vgagfr_created_at', 'vgagfr_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Aliases' => 2, 'Summary' => 3, 'Description' => 4, 'ApiDetailUrl' => 5, 'SiteDetailUrl' => 6, 'ImageIconUrl' => 7, 'ImageMediumUrl' => 8, 'ImageScreenUrl' => 9, 'ImageSmallUrl' => 10, 'ImageSuperUrl' => 11, 'ImageThumbUrl' => 12, 'ImageTinyUrl' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'aliases' => 2, 'summary' => 3, 'description' => 4, 'apiDetailUrl' => 5, 'siteDetailUrl' => 6, 'imageIconUrl' => 7, 'imageMediumUrl' => 8, 'imageScreenUrl' => 9, 'imageSmallUrl' => 10, 'imageSuperUrl' => 11, 'imageThumbUrl' => 12, 'imageTinyUrl' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID => 0, ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME => 1, ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES => 2, ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY => 3, ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION => 4, ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL => 5, ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL => 6, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL => 7, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL => 8, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL => 9, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL => 10, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL => 11, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL => 12, ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL => 13, ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT => 14, ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('vgagfr_id' => 0, 'vgagfr_name' => 1, 'vgagfr_aliases' => 2, 'vgagfr_summary' => 3, 'vgagfr_description' => 4, 'vgagfr_api_detail_url' => 5, 'vgagfr_site_detail_url' => 6, 'vgagfr_image_icon_url' => 7, 'vgagfr_image_medium_url' => 8, 'vgagfr_image_screen_url' => 9, 'vgagfr_image_small_url' => 10, 'vgagfr_image_super_url' => 11, 'vgagfr_image_thumb_url' => 12, 'vgagfr_image_tiny_url' => 13, 'vgagfr_created_at' => 14, 'vgagfr_updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_franchise_vgagfr');
        $this->setPhpName('ApiGiantBombFranchise');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombFranchise');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagfr_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagfr_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagfr_aliases', 'Aliases', 'ARRAY', false, null, null);
        $this->addColumn('vgagfr_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagfr_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagfr_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagfr_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagfr_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombFirstFranchiseConcept', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombConcept', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagcc_first_vgagfr_id',
    1 => ':vgagfr_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombFirstFranchiseConcepts', false);
        $this->addRelation('ApiGiantBombGameFranchise', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameFranchise', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgaggf_vgagfr_id',
    1 => ':vgagfr_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombGameFranchises', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_franchise_vgagfr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombConceptTableMap::clearInstancePool();
        ApiGiantBombGameFranchiseTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombFranchiseTableMap::CLASS_DEFAULT : ApiGiantBombFranchiseTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombFranchise object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombFranchiseTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombFranchiseTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombFranchiseTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombFranchiseTableMap::OM_CLASS;
            /** @var ApiGiantBombFranchise $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombFranchiseTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombFranchiseTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombFranchiseTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombFranchise $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombFranchiseTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagfr_id');
            $criteria->addSelectColumn($alias . '.vgagfr_name');
            $criteria->addSelectColumn($alias . '.vgagfr_aliases');
            $criteria->addSelectColumn($alias . '.vgagfr_summary');
            $criteria->addSelectColumn($alias . '.vgagfr_description');
            $criteria->addSelectColumn($alias . '.vgagfr_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagfr_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagfr_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagfr_created_at');
            $criteria->addSelectColumn($alias . '.vgagfr_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombFranchiseTableMap::DATABASE_NAME)->getTable(ApiGiantBombFranchiseTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombFranchiseTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombFranchiseTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombFranchise or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombFranchise object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombFranchiseQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombFranchiseTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombFranchiseTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_franchise_vgagfr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombFranchiseQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombFranchise or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombFranchise object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombFranchise object
        }


        // Set the correct dbName
        $query = ApiGiantBombFranchiseQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombFranchiseTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombFranchiseTableMap::buildTableMap();
