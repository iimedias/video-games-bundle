<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\GameRelease;
use IiMedias\VideoGamesBundle\Model\GameReleaseQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_game_release_vgagmr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GameReleaseTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.GameReleaseTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_game_release_vgagmr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\GameRelease';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.GameRelease';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the vgagmr_id field
     */
    const COL_VGAGMR_ID = 'videogames_game_release_vgagmr.vgagmr_id';

    /**
     * the column name for the vgagmr_name field
     */
    const COL_VGAGMR_NAME = 'videogames_game_release_vgagmr.vgagmr_name';

    /**
     * the column name for the vgagmr_vgagam_id field
     */
    const COL_VGAGMR_VGAGAM_ID = 'videogames_game_release_vgagmr.vgagmr_vgagam_id';

    /**
     * the column name for the vgagmr_vgaplf_id field
     */
    const COL_VGAGMR_VGAPLF_ID = 'videogames_game_release_vgagmr.vgagmr_vgaplf_id';

    /**
     * the column name for the vgagmr_released_at field
     */
    const COL_VGAGMR_RELEASED_AT = 'videogames_game_release_vgagmr.vgagmr_released_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'GameId', 'PlatformId', 'ReleasedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'gameId', 'platformId', 'releasedAt', ),
        self::TYPE_COLNAME       => array(GameReleaseTableMap::COL_VGAGMR_ID, GameReleaseTableMap::COL_VGAGMR_NAME, GameReleaseTableMap::COL_VGAGMR_VGAGAM_ID, GameReleaseTableMap::COL_VGAGMR_VGAPLF_ID, GameReleaseTableMap::COL_VGAGMR_RELEASED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagmr_id', 'vgagmr_name', 'vgagmr_vgagam_id', 'vgagmr_vgaplf_id', 'vgagmr_released_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'GameId' => 2, 'PlatformId' => 3, 'ReleasedAt' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'gameId' => 2, 'platformId' => 3, 'releasedAt' => 4, ),
        self::TYPE_COLNAME       => array(GameReleaseTableMap::COL_VGAGMR_ID => 0, GameReleaseTableMap::COL_VGAGMR_NAME => 1, GameReleaseTableMap::COL_VGAGMR_VGAGAM_ID => 2, GameReleaseTableMap::COL_VGAGMR_VGAPLF_ID => 3, GameReleaseTableMap::COL_VGAGMR_RELEASED_AT => 4, ),
        self::TYPE_FIELDNAME     => array('vgagmr_id' => 0, 'vgagmr_name' => 1, 'vgagmr_vgagam_id' => 2, 'vgagmr_vgaplf_id' => 3, 'vgagmr_released_at' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_game_release_vgagmr');
        $this->setPhpName('GameRelease');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\GameRelease');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('vgagmr_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagmr_name', 'Name', 'VARCHAR', true, 255, null);
        $this->getColumn('vgagmr_name')->setPrimaryString(true);
        $this->addForeignKey('vgagmr_vgagam_id', 'GameId', 'INTEGER', 'videogames_game_vgagam', 'vgagam_id', true, null, null);
        $this->addForeignKey('vgagmr_vgaplf_id', 'PlatformId', 'INTEGER', 'videogames_platform_vgaplf', 'vgaplf_id', true, null, null);
        $this->addColumn('vgagmr_released_at', 'ReleasedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Game', '\\IiMedias\\VideoGamesBundle\\Model\\Game', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagmr_vgagam_id',
    1 => ':vgagam_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Platform', '\\IiMedias\\VideoGamesBundle\\Model\\Platform', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagmr_vgaplf_id',
    1 => ':vgaplf_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GameReleaseTableMap::CLASS_DEFAULT : GameReleaseTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GameRelease object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GameReleaseTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GameReleaseTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GameReleaseTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GameReleaseTableMap::OM_CLASS;
            /** @var GameRelease $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GameReleaseTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GameReleaseTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GameReleaseTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GameRelease $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GameReleaseTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GameReleaseTableMap::COL_VGAGMR_ID);
            $criteria->addSelectColumn(GameReleaseTableMap::COL_VGAGMR_NAME);
            $criteria->addSelectColumn(GameReleaseTableMap::COL_VGAGMR_VGAGAM_ID);
            $criteria->addSelectColumn(GameReleaseTableMap::COL_VGAGMR_VGAPLF_ID);
            $criteria->addSelectColumn(GameReleaseTableMap::COL_VGAGMR_RELEASED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagmr_id');
            $criteria->addSelectColumn($alias . '.vgagmr_name');
            $criteria->addSelectColumn($alias . '.vgagmr_vgagam_id');
            $criteria->addSelectColumn($alias . '.vgagmr_vgaplf_id');
            $criteria->addSelectColumn($alias . '.vgagmr_released_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GameReleaseTableMap::DATABASE_NAME)->getTable(GameReleaseTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GameReleaseTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GameReleaseTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GameReleaseTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GameRelease or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GameRelease object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameReleaseTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\GameRelease) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(GameReleaseTableMap::DATABASE_NAME);
            $criteria->add(GameReleaseTableMap::COL_VGAGMR_ID, (array) $values, Criteria::IN);
        }

        $query = GameReleaseQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GameReleaseTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GameReleaseTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_game_release_vgagmr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GameReleaseQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GameRelease or Criteria object.
     *
     * @param mixed               $criteria Criteria or GameRelease object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameReleaseTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GameRelease object
        }

        if ($criteria->containsKey(GameReleaseTableMap::COL_VGAGMR_ID) && $criteria->keyContainsValue(GameReleaseTableMap::COL_VGAGMR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.GameReleaseTableMap::COL_VGAGMR_ID.')');
        }


        // Set the correct dbName
        $query = GameReleaseQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GameReleaseTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GameReleaseTableMap::buildTableMap();
