<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombImage;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_image_vgagim' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombImageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombImageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_image_vgagim';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombImage';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombImage';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the vgagim_id field
     */
    const COL_VGAGIM_ID = 'videogames_api_giantbomb_image_vgagim.vgagim_id';

    /**
     * the column name for the vgagim_tags field
     */
    const COL_VGAGIM_TAGS = 'videogames_api_giantbomb_image_vgagim.vgagim_tags';

    /**
     * the column name for the vgagim_vgagga_id field
     */
    const COL_VGAGIM_VGAGGA_ID = 'videogames_api_giantbomb_image_vgagim.vgagim_vgagga_id';

    /**
     * the column name for the vgagim_vgagrl_id field
     */
    const COL_VGAGIM_VGAGRL_ID = 'videogames_api_giantbomb_image_vgagim.vgagim_vgagrl_id';

    /**
     * the column name for the vgagim_image_icon_url field
     */
    const COL_VGAGIM_IMAGE_ICON_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_icon_url';

    /**
     * the column name for the vgagim_image_medium_url field
     */
    const COL_VGAGIM_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_medium_url';

    /**
     * the column name for the vgagim_image_screen_url field
     */
    const COL_VGAGIM_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_screen_url';

    /**
     * the column name for the vgagim_image_small_url field
     */
    const COL_VGAGIM_IMAGE_SMALL_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_small_url';

    /**
     * the column name for the vgagim_image_super_url field
     */
    const COL_VGAGIM_IMAGE_SUPER_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_super_url';

    /**
     * the column name for the vgagim_image_thumb_url field
     */
    const COL_VGAGIM_IMAGE_THUMB_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_thumb_url';

    /**
     * the column name for the vgagim_image_tiny_url field
     */
    const COL_VGAGIM_IMAGE_TINY_URL = 'videogames_api_giantbomb_image_vgagim.vgagim_image_tiny_url';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Tags', 'ApiGiantBombGameId', 'ApiGiantBombGameReleaseId', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', ),
        self::TYPE_CAMELNAME     => array('id', 'tags', 'apiGiantBombGameId', 'apiGiantBombGameReleaseId', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', ),
        self::TYPE_COLNAME       => array(ApiGiantBombImageTableMap::COL_VGAGIM_ID, ApiGiantBombImageTableMap::COL_VGAGIM_TAGS, ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID, ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL, ),
        self::TYPE_FIELDNAME     => array('vgagim_id', 'vgagim_tags', 'vgagim_vgagga_id', 'vgagim_vgagrl_id', 'vgagim_image_icon_url', 'vgagim_image_medium_url', 'vgagim_image_screen_url', 'vgagim_image_small_url', 'vgagim_image_super_url', 'vgagim_image_thumb_url', 'vgagim_image_tiny_url', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Tags' => 1, 'ApiGiantBombGameId' => 2, 'ApiGiantBombGameReleaseId' => 3, 'ImageIconUrl' => 4, 'ImageMediumUrl' => 5, 'ImageScreenUrl' => 6, 'ImageSmallUrl' => 7, 'ImageSuperUrl' => 8, 'ImageThumbUrl' => 9, 'ImageTinyUrl' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'tags' => 1, 'apiGiantBombGameId' => 2, 'apiGiantBombGameReleaseId' => 3, 'imageIconUrl' => 4, 'imageMediumUrl' => 5, 'imageScreenUrl' => 6, 'imageSmallUrl' => 7, 'imageSuperUrl' => 8, 'imageThumbUrl' => 9, 'imageTinyUrl' => 10, ),
        self::TYPE_COLNAME       => array(ApiGiantBombImageTableMap::COL_VGAGIM_ID => 0, ApiGiantBombImageTableMap::COL_VGAGIM_TAGS => 1, ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID => 2, ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID => 3, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL => 4, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL => 5, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL => 6, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL => 7, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL => 8, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL => 9, ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL => 10, ),
        self::TYPE_FIELDNAME     => array('vgagim_id' => 0, 'vgagim_tags' => 1, 'vgagim_vgagga_id' => 2, 'vgagim_vgagrl_id' => 3, 'vgagim_image_icon_url' => 4, 'vgagim_image_medium_url' => 5, 'vgagim_image_screen_url' => 6, 'vgagim_image_small_url' => 7, 'vgagim_image_super_url' => 8, 'vgagim_image_thumb_url' => 9, 'vgagim_image_tiny_url' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_image_vgagim');
        $this->setPhpName('ApiGiantBombImage');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombImage');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('vgagim_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagim_tags', 'Tags', 'ARRAY', false, null, null);
        $this->addForeignKey('vgagim_vgagga_id', 'ApiGiantBombGameId', 'INTEGER', 'videogames_api_giantbomb_game_vgagga', 'vgagga_id', false, null, null);
        $this->addForeignKey('vgagim_vgagrl_id', 'ApiGiantBombGameReleaseId', 'INTEGER', 'videogames_api_giantbomb_release_vgagrl', 'vgagrl_id', false, null, null);
        $this->addColumn('vgagim_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagim_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagim_vgagga_id',
    1 => ':vgagga_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ApiGiantBombGameRelease', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vgagim_vgagrl_id',
    1 => ':vgagrl_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombImageTableMap::CLASS_DEFAULT : ApiGiantBombImageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombImage object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombImageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombImageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombImageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombImageTableMap::OM_CLASS;
            /** @var ApiGiantBombImage $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombImageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombImageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombImageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombImage $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombImageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_ID);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_TAGS);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL);
        } else {
            $criteria->addSelectColumn($alias . '.vgagim_id');
            $criteria->addSelectColumn($alias . '.vgagim_tags');
            $criteria->addSelectColumn($alias . '.vgagim_vgagga_id');
            $criteria->addSelectColumn($alias . '.vgagim_vgagrl_id');
            $criteria->addSelectColumn($alias . '.vgagim_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagim_image_tiny_url');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombImageTableMap::DATABASE_NAME)->getTable(ApiGiantBombImageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombImageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombImageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombImageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombImage or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombImage object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombImageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombImageTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombImageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombImageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombImageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_image_vgagim table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombImageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombImage or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombImage object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombImageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombImage object
        }

        if ($criteria->containsKey(ApiGiantBombImageTableMap::COL_VGAGIM_ID) && $criteria->keyContainsValue(ApiGiantBombImageTableMap::COL_VGAGIM_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ApiGiantBombImageTableMap::COL_VGAGIM_ID.')');
        }


        // Set the correct dbName
        $query = ApiGiantBombImageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombImageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombImageTableMap::buildTableMap();
