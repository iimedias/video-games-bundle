<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_giantbomb_rating_board_vgagrb' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiGiantBombRatingBoardTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiGiantBombRatingBoardTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_giantbomb_rating_board_vgagrb';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRatingBoard';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiGiantBombRatingBoard';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the vgagrb_id field
     */
    const COL_VGAGRB_ID = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_id';

    /**
     * the column name for the vgagrb_name field
     */
    const COL_VGAGRB_NAME = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_name';

    /**
     * the column name for the vgagrb_summary field
     */
    const COL_VGAGRB_SUMMARY = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_summary';

    /**
     * the column name for the vgagrb_description field
     */
    const COL_VGAGRB_DESCRIPTION = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_description';

    /**
     * the column name for the vgagrb_api_detail_url field
     */
    const COL_VGAGRB_API_DETAIL_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_api_detail_url';

    /**
     * the column name for the vgagrb_site_detail_url field
     */
    const COL_VGAGRB_SITE_DETAIL_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_site_detail_url';

    /**
     * the column name for the vgagrb_image_icon_url field
     */
    const COL_VGAGRB_IMAGE_ICON_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_icon_url';

    /**
     * the column name for the vgagrb_image_medium_url field
     */
    const COL_VGAGRB_IMAGE_MEDIUM_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_medium_url';

    /**
     * the column name for the vgagrb_image_screen_url field
     */
    const COL_VGAGRB_IMAGE_SCREEN_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_screen_url';

    /**
     * the column name for the vgagrb_image_small_url field
     */
    const COL_VGAGRB_IMAGE_SMALL_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_small_url';

    /**
     * the column name for the vgagrb_image_super_url field
     */
    const COL_VGAGRB_IMAGE_SUPER_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_super_url';

    /**
     * the column name for the vgagrb_image_thumb_url field
     */
    const COL_VGAGRB_IMAGE_THUMB_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_thumb_url';

    /**
     * the column name for the vgagrb_image_tiny_url field
     */
    const COL_VGAGRB_IMAGE_TINY_URL = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_image_tiny_url';

    /**
     * the column name for the vgagrb_created_at field
     */
    const COL_VGAGRB_CREATED_AT = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_created_at';

    /**
     * the column name for the vgagrb_updated_at field
     */
    const COL_VGAGRB_UPDATED_AT = 'videogames_api_giantbomb_rating_board_vgagrb.vgagrb_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Summary', 'Description', 'ApiDetailUrl', 'SiteDetailUrl', 'ImageIconUrl', 'ImageMediumUrl', 'ImageScreenUrl', 'ImageSmallUrl', 'ImageSuperUrl', 'ImageThumbUrl', 'ImageTinyUrl', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'summary', 'description', 'apiDetailUrl', 'siteDetailUrl', 'imageIconUrl', 'imageMediumUrl', 'imageScreenUrl', 'imageSmallUrl', 'imageSuperUrl', 'imageThumbUrl', 'imageTinyUrl', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgagrb_id', 'vgagrb_name', 'vgagrb_summary', 'vgagrb_description', 'vgagrb_api_detail_url', 'vgagrb_site_detail_url', 'vgagrb_image_icon_url', 'vgagrb_image_medium_url', 'vgagrb_image_screen_url', 'vgagrb_image_small_url', 'vgagrb_image_super_url', 'vgagrb_image_thumb_url', 'vgagrb_image_tiny_url', 'vgagrb_created_at', 'vgagrb_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Summary' => 2, 'Description' => 3, 'ApiDetailUrl' => 4, 'SiteDetailUrl' => 5, 'ImageIconUrl' => 6, 'ImageMediumUrl' => 7, 'ImageScreenUrl' => 8, 'ImageSmallUrl' => 9, 'ImageSuperUrl' => 10, 'ImageThumbUrl' => 11, 'ImageTinyUrl' => 12, 'CreatedAt' => 13, 'UpdatedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'summary' => 2, 'description' => 3, 'apiDetailUrl' => 4, 'siteDetailUrl' => 5, 'imageIconUrl' => 6, 'imageMediumUrl' => 7, 'imageScreenUrl' => 8, 'imageSmallUrl' => 9, 'imageSuperUrl' => 10, 'imageThumbUrl' => 11, 'imageTinyUrl' => 12, 'createdAt' => 13, 'updatedAt' => 14, ),
        self::TYPE_COLNAME       => array(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID => 0, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME => 1, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY => 2, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION => 3, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL => 4, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL => 5, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL => 6, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL => 7, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL => 8, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL => 9, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL => 10, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL => 11, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL => 12, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT => 13, ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('vgagrb_id' => 0, 'vgagrb_name' => 1, 'vgagrb_summary' => 2, 'vgagrb_description' => 3, 'vgagrb_api_detail_url' => 4, 'vgagrb_site_detail_url' => 5, 'vgagrb_image_icon_url' => 6, 'vgagrb_image_medium_url' => 7, 'vgagrb_image_screen_url' => 8, 'vgagrb_image_small_url' => 9, 'vgagrb_image_super_url' => 10, 'vgagrb_image_thumb_url' => 11, 'vgagrb_image_tiny_url' => 12, 'vgagrb_created_at' => 13, 'vgagrb_updated_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_giantbomb_rating_board_vgagrb');
        $this->setPhpName('ApiGiantBombRatingBoard');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRatingBoard');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgagrb_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgagrb_name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrb_summary', 'Summary', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_api_detail_url', 'ApiDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrb_site_detail_url', 'SiteDetailUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('vgagrb_image_icon_url', 'ImageIconUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_medium_url', 'ImageMediumUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_screen_url', 'ImageScreenUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_small_url', 'ImageSmallUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_super_url', 'ImageSuperUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_thumb_url', 'ImageThumbUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_image_tiny_url', 'ImageTinyUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgagrb_created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgagrb_updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ApiGiantBombRatingGame', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':vgagch_vgagrb_id',
    1 => ':vgagrb_id',
  ),
), 'CASCADE', 'CASCADE', 'ApiGiantBombRatingGames', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to videogames_api_giantbomb_rating_board_vgagrb     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ApiGiantBombGameRatingTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiGiantBombRatingBoardTableMap::CLASS_DEFAULT : ApiGiantBombRatingBoardTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiGiantBombRatingBoard object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiGiantBombRatingBoardTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiGiantBombRatingBoardTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiGiantBombRatingBoardTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiGiantBombRatingBoardTableMap::OM_CLASS;
            /** @var ApiGiantBombRatingBoard $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiGiantBombRatingBoardTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiGiantBombRatingBoardTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiGiantBombRatingBoardTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiGiantBombRatingBoard $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiGiantBombRatingBoardTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT);
            $criteria->addSelectColumn(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgagrb_id');
            $criteria->addSelectColumn($alias . '.vgagrb_name');
            $criteria->addSelectColumn($alias . '.vgagrb_summary');
            $criteria->addSelectColumn($alias . '.vgagrb_description');
            $criteria->addSelectColumn($alias . '.vgagrb_api_detail_url');
            $criteria->addSelectColumn($alias . '.vgagrb_site_detail_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_icon_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_medium_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_screen_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_small_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_super_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_thumb_url');
            $criteria->addSelectColumn($alias . '.vgagrb_image_tiny_url');
            $criteria->addSelectColumn($alias . '.vgagrb_created_at');
            $criteria->addSelectColumn($alias . '.vgagrb_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombRatingBoardTableMap::DATABASE_NAME)->getTable(ApiGiantBombRatingBoardTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiGiantBombRatingBoardTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiGiantBombRatingBoardTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiGiantBombRatingBoard or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiGiantBombRatingBoard object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, (array) $values, Criteria::IN);
        }

        $query = ApiGiantBombRatingBoardQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiGiantBombRatingBoardTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiGiantBombRatingBoardTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_rating_board_vgagrb table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiGiantBombRatingBoardQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiGiantBombRatingBoard or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiGiantBombRatingBoard object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiGiantBombRatingBoard object
        }


        // Set the correct dbName
        $query = ApiGiantBombRatingBoardQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiGiantBombRatingBoardTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiGiantBombRatingBoardTableMap::buildTableMap();
