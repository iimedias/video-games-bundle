<?php

namespace IiMedias\VideoGamesBundle\Model\Map;

use IiMedias\VideoGamesBundle\Model\ApiIgdbCompany;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompanyQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'videogames_api_igdb_company_vgaico' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApiIgdbCompanyTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.VideoGamesBundle.Model.Map.ApiIgdbCompanyTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'videogames_api_igdb_company_vgaico';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbCompany';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.VideoGamesBundle.Model.ApiIgdbCompany';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 20;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 20;

    /**
     * the column name for the vgaico_id field
     */
    const COL_VGAICO_ID = 'videogames_api_igdb_company_vgaico.vgaico_id';

    /**
     * the column name for the vgaico_parent_id field
     */
    const COL_VGAICO_PARENT_ID = 'videogames_api_igdb_company_vgaico.vgaico_parent_id';

    /**
     * the column name for the vgaico_name field
     */
    const COL_VGAICO_NAME = 'videogames_api_igdb_company_vgaico.vgaico_name';

    /**
     * the column name for the vgaico_slug field
     */
    const COL_VGAICO_SLUG = 'videogames_api_igdb_company_vgaico.vgaico_slug';

    /**
     * the column name for the vgaico_url field
     */
    const COL_VGAICO_URL = 'videogames_api_igdb_company_vgaico.vgaico_url';

    /**
     * the column name for the vgaico_website field
     */
    const COL_VGAICO_WEBSITE = 'videogames_api_igdb_company_vgaico.vgaico_website';

    /**
     * the column name for the vgaico_twitter field
     */
    const COL_VGAICO_TWITTER = 'videogames_api_igdb_company_vgaico.vgaico_twitter';

    /**
     * the column name for the vgaico_country field
     */
    const COL_VGAICO_COUNTRY = 'videogames_api_igdb_company_vgaico.vgaico_country';

    /**
     * the column name for the vgaico_logo_cloudinary_id field
     */
    const COL_VGAICO_LOGO_CLOUDINARY_ID = 'videogames_api_igdb_company_vgaico.vgaico_logo_cloudinary_id';

    /**
     * the column name for the vgaico_logo_width field
     */
    const COL_VGAICO_LOGO_WIDTH = 'videogames_api_igdb_company_vgaico.vgaico_logo_width';

    /**
     * the column name for the vgaico_logo_height field
     */
    const COL_VGAICO_LOGO_HEIGHT = 'videogames_api_igdb_company_vgaico.vgaico_logo_height';

    /**
     * the column name for the vgaico_description field
     */
    const COL_VGAICO_DESCRIPTION = 'videogames_api_igdb_company_vgaico.vgaico_description';

    /**
     * the column name for the vgaico_developed field
     */
    const COL_VGAICO_DEVELOPED = 'videogames_api_igdb_company_vgaico.vgaico_developed';

    /**
     * the column name for the vgaico_published field
     */
    const COL_VGAICO_PUBLISHED = 'videogames_api_igdb_company_vgaico.vgaico_published';

    /**
     * the column name for the vgaico_start_date field
     */
    const COL_VGAICO_START_DATE = 'videogames_api_igdb_company_vgaico.vgaico_start_date';

    /**
     * the column name for the vgaico_start_date_category field
     */
    const COL_VGAICO_START_DATE_CATEGORY = 'videogames_api_igdb_company_vgaico.vgaico_start_date_category';

    /**
     * the column name for the vgaico_change_date field
     */
    const COL_VGAICO_CHANGE_DATE = 'videogames_api_igdb_company_vgaico.vgaico_change_date';

    /**
     * the column name for the vgaico_change_date_category field
     */
    const COL_VGAICO_CHANGE_DATE_CATEGORY = 'videogames_api_igdb_company_vgaico.vgaico_change_date_category';

    /**
     * the column name for the vgaico_created_at field
     */
    const COL_VGAICO_CREATED_AT = 'videogames_api_igdb_company_vgaico.vgaico_created_at';

    /**
     * the column name for the vgaico_updated_at field
     */
    const COL_VGAICO_UPDATED_AT = 'videogames_api_igdb_company_vgaico.vgaico_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'Name', 'Slug', 'Url', 'WebSite', 'Twitter', 'Country', 'LogoCloudinaryId', 'LogoWidth', 'LogoHeight', 'Description', 'Developed', 'Published', 'StartDate', 'StartDateCategory', 'ChangeDate', 'ChangeDateCategory', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'name', 'slug', 'url', 'webSite', 'twitter', 'country', 'logoCloudinaryId', 'logoWidth', 'logoHeight', 'description', 'developed', 'published', 'startDate', 'startDateCategory', 'changeDate', 'changeDateCategory', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApiIgdbCompanyTableMap::COL_VGAICO_ID, ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID, ApiIgdbCompanyTableMap::COL_VGAICO_NAME, ApiIgdbCompanyTableMap::COL_VGAICO_SLUG, ApiIgdbCompanyTableMap::COL_VGAICO_URL, ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE, ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER, ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT, ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION, ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED, ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED, ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE, ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY, ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE, ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY, ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT, ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('vgaico_id', 'vgaico_parent_id', 'vgaico_name', 'vgaico_slug', 'vgaico_url', 'vgaico_website', 'vgaico_twitter', 'vgaico_country', 'vgaico_logo_cloudinary_id', 'vgaico_logo_width', 'vgaico_logo_height', 'vgaico_description', 'vgaico_developed', 'vgaico_published', 'vgaico_start_date', 'vgaico_start_date_category', 'vgaico_change_date', 'vgaico_change_date_category', 'vgaico_created_at', 'vgaico_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'Name' => 2, 'Slug' => 3, 'Url' => 4, 'WebSite' => 5, 'Twitter' => 6, 'Country' => 7, 'LogoCloudinaryId' => 8, 'LogoWidth' => 9, 'LogoHeight' => 10, 'Description' => 11, 'Developed' => 12, 'Published' => 13, 'StartDate' => 14, 'StartDateCategory' => 15, 'ChangeDate' => 16, 'ChangeDateCategory' => 17, 'CreatedAt' => 18, 'UpdatedAt' => 19, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'name' => 2, 'slug' => 3, 'url' => 4, 'webSite' => 5, 'twitter' => 6, 'country' => 7, 'logoCloudinaryId' => 8, 'logoWidth' => 9, 'logoHeight' => 10, 'description' => 11, 'developed' => 12, 'published' => 13, 'startDate' => 14, 'startDateCategory' => 15, 'changeDate' => 16, 'changeDateCategory' => 17, 'createdAt' => 18, 'updatedAt' => 19, ),
        self::TYPE_COLNAME       => array(ApiIgdbCompanyTableMap::COL_VGAICO_ID => 0, ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID => 1, ApiIgdbCompanyTableMap::COL_VGAICO_NAME => 2, ApiIgdbCompanyTableMap::COL_VGAICO_SLUG => 3, ApiIgdbCompanyTableMap::COL_VGAICO_URL => 4, ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE => 5, ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER => 6, ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY => 7, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID => 8, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH => 9, ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT => 10, ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION => 11, ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED => 12, ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED => 13, ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE => 14, ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY => 15, ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE => 16, ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY => 17, ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT => 18, ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT => 19, ),
        self::TYPE_FIELDNAME     => array('vgaico_id' => 0, 'vgaico_parent_id' => 1, 'vgaico_name' => 2, 'vgaico_slug' => 3, 'vgaico_url' => 4, 'vgaico_website' => 5, 'vgaico_twitter' => 6, 'vgaico_country' => 7, 'vgaico_logo_cloudinary_id' => 8, 'vgaico_logo_width' => 9, 'vgaico_logo_height' => 10, 'vgaico_description' => 11, 'vgaico_developed' => 12, 'vgaico_published' => 13, 'vgaico_start_date' => 14, 'vgaico_start_date_category' => 15, 'vgaico_change_date' => 16, 'vgaico_change_date_category' => 17, 'vgaico_created_at' => 18, 'vgaico_updated_at' => 19, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('videogames_api_igdb_company_vgaico');
        $this->setPhpName('ApiIgdbCompany');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbCompany');
        $this->setPackage('src.IiMedias.VideoGamesBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('vgaico_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vgaico_parent_id', 'ParentId', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaico_slug', 'Slug', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaico_url', 'Url', 'VARCHAR', true, 255, null);
        $this->addColumn('vgaico_website', 'WebSite', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaico_twitter', 'Twitter', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaico_country', 'Country', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_logo_cloudinary_id', 'LogoCloudinaryId', 'VARCHAR', false, 255, null);
        $this->addColumn('vgaico_logo_width', 'LogoWidth', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_logo_height', 'LogoHeight', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('vgaico_developed', 'Developed', 'ARRAY', true, null, null);
        $this->addColumn('vgaico_published', 'Published', 'ARRAY', true, null, null);
        $this->addColumn('vgaico_start_date', 'StartDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgaico_start_date_category', 'StartDateCategory', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_change_date', 'ChangeDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('vgaico_change_date_category', 'ChangeDateCategory', 'INTEGER', false, null, null);
        $this->addColumn('vgaico_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('vgaico_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApiIgdbCompanyTableMap::CLASS_DEFAULT : ApiIgdbCompanyTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApiIgdbCompany object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApiIgdbCompanyTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApiIgdbCompanyTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApiIgdbCompanyTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApiIgdbCompanyTableMap::OM_CLASS;
            /** @var ApiIgdbCompany $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApiIgdbCompanyTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApiIgdbCompanyTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApiIgdbCompanyTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApiIgdbCompany $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApiIgdbCompanyTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_ID);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_NAME);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_SLUG);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_URL);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT);
            $criteria->addSelectColumn(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.vgaico_id');
            $criteria->addSelectColumn($alias . '.vgaico_parent_id');
            $criteria->addSelectColumn($alias . '.vgaico_name');
            $criteria->addSelectColumn($alias . '.vgaico_slug');
            $criteria->addSelectColumn($alias . '.vgaico_url');
            $criteria->addSelectColumn($alias . '.vgaico_website');
            $criteria->addSelectColumn($alias . '.vgaico_twitter');
            $criteria->addSelectColumn($alias . '.vgaico_country');
            $criteria->addSelectColumn($alias . '.vgaico_logo_cloudinary_id');
            $criteria->addSelectColumn($alias . '.vgaico_logo_width');
            $criteria->addSelectColumn($alias . '.vgaico_logo_height');
            $criteria->addSelectColumn($alias . '.vgaico_description');
            $criteria->addSelectColumn($alias . '.vgaico_developed');
            $criteria->addSelectColumn($alias . '.vgaico_published');
            $criteria->addSelectColumn($alias . '.vgaico_start_date');
            $criteria->addSelectColumn($alias . '.vgaico_start_date_category');
            $criteria->addSelectColumn($alias . '.vgaico_change_date');
            $criteria->addSelectColumn($alias . '.vgaico_change_date_category');
            $criteria->addSelectColumn($alias . '.vgaico_created_at');
            $criteria->addSelectColumn($alias . '.vgaico_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApiIgdbCompanyTableMap::DATABASE_NAME)->getTable(ApiIgdbCompanyTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApiIgdbCompanyTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApiIgdbCompanyTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApiIgdbCompanyTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApiIgdbCompany or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApiIgdbCompany object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\VideoGamesBundle\Model\ApiIgdbCompany) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApiIgdbCompanyTableMap::DATABASE_NAME);
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_ID, (array) $values, Criteria::IN);
        }

        $query = ApiIgdbCompanyQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApiIgdbCompanyTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApiIgdbCompanyTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the videogames_api_igdb_company_vgaico table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApiIgdbCompanyQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApiIgdbCompany or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApiIgdbCompany object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApiIgdbCompany object
        }


        // Set the correct dbName
        $query = ApiIgdbCompanyQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApiIgdbCompanyTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApiIgdbCompanyTableMap::buildTableMap();
