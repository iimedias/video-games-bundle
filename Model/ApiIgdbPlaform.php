<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiIgdbPlaform as BaseApiIgdbPlaform;

/**
 * Skeleton subclass for representing a row from the 'videogames_api_igdb_platform_vgaipl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiIgdbPlaform extends BaseApiIgdbPlaform
{

}
