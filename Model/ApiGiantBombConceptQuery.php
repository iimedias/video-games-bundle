<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombConceptQuery as BaseApiGiantBombConceptQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_api_giantbomb_concept_vgagcc' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiGiantBombConceptQuery extends BaseApiGiantBombConceptQuery
{

}
