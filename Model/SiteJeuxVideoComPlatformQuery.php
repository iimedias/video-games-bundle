<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\SiteJeuxVideoComPlatformQuery as BaseSiteJeuxVideoComPlatformQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'videogames_site_jeuxvideocom_platform_vgajpl' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteJeuxVideoComPlatformQuery extends BaseSiteJeuxVideoComPlatformQuery
{

}
