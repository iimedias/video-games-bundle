<?php

namespace IiMedias\VideoGamesBundle\Model;

use IiMedias\VideoGamesBundle\Model\Base\Game as BaseGame;

/**
 * Skeleton subclass for representing a row from the 'videogames_game_vgagam' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Game extends BaseGame
{
    protected $boxart;

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @return mixed
     */
    public function getBoxart()
    {
        return $this->boxart;
    }

    /**
     *
     * @access public
     * @since 1.0.0 Création -- S.Bloino
     * @param $boxart
     * @return Game
     */
    public function setBoxart($boxart)
    {
        $this->boxart = $boxart;
        return $this;
    }

    public function getReleases()
    {
        $releases = GameReleaseQuery::getAllByGame($this);
        return $releases;
    }

    /* En dessous c'est en réflexion */

    protected $gameblogLinkIds;
    protected $gameblogLinkSearch;
    protected $gamekultLinkIds;
    protected $gamekultLinkSearch;
    protected $jeuxvideocomLinkIds;
    protected $jeuxvideocomLinkSearch;

    /* GameBlog */

    public function getGameblogLinkIds()
    {
        return $this->gameblogLinkIds;
    }

    public function getGameblogLinkSearch()
    {
        return $this->gameblogLinkSearch;
    }

    public function setGameblogLinkIds($gameblogLinkIds)
    {
        $this->gameblogLinkIds = $gameblogLinkIds;
        return $this;
    }

    public function setGameblogLinkSearch($gameblogLinkSearch)
    {
        $this->gameblogLinkSearch = $gameblogLinkSearch;
        return $this;
    }

    /* GameKult */

    public function getGamekultLinkIds()
    {
        return $this->gamekultLinkIds;
    }

    public function getGamekultLinkSearch()
    {
        return $this->gamekultLinkSearch;
    }

    public function setGamekultLinkIds($gamekultLinkIds)
    {
        $this->gamekultLinkIds = $gamekultLinkIds;
        return $this;
    }

    public function setGamekultLinkSearch($gamekultLinkSearch)
    {
        $this->gamekultLinkSearch = $gamekultLinkSearch;
        return $this;
    }

    /* JeuxVideo.com */

    public function getJeuxvideocomLinkIds()
    {
        return $this->jeuxvideocomLinkIds;
    }

    public function getJeuxvideocomLinkSearch()
    {
        return $this->jeuxvideocomLinkSearch;
    }

    public function setJeuxvideocomLinkIds($jeuxvideocomLinkIds)
    {
        $this->jeuxvideocomtLinkIds = $jeuxvideocomLinkIds;
        return $this;
    }

    public function setJeuxvideocomLinkSearch($jeuxvideocomLinkSearch)
    {
        $this->jeuxvideocomLinkSearch = $jeuxvideocomLinkSearch;
        return $this;
    }
}
