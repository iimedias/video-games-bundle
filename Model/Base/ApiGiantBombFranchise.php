<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept as ChildApiGiantBombConcept;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery as ChildApiGiantBombConceptQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise as ChildApiGiantBombFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery as ChildApiGiantBombFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise as ChildApiGiantBombGameFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery as ChildApiGiantBombGameFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombConceptTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombFranchiseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameFranchiseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_franchise_vgagfr' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombFranchise implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombFranchiseTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagfr_id field.
     *
     * @var        int
     */
    protected $vgagfr_id;

    /**
     * The value for the vgagfr_name field.
     *
     * @var        string
     */
    protected $vgagfr_name;

    /**
     * The value for the vgagfr_aliases field.
     *
     * @var        array
     */
    protected $vgagfr_aliases;

    /**
     * The unserialized $vgagfr_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagfr_aliases_unserialized;

    /**
     * The value for the vgagfr_summary field.
     *
     * @var        string
     */
    protected $vgagfr_summary;

    /**
     * The value for the vgagfr_description field.
     *
     * @var        string
     */
    protected $vgagfr_description;

    /**
     * The value for the vgagfr_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagfr_api_detail_url;

    /**
     * The value for the vgagfr_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagfr_site_detail_url;

    /**
     * The value for the vgagfr_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_icon_url;

    /**
     * The value for the vgagfr_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_medium_url;

    /**
     * The value for the vgagfr_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_screen_url;

    /**
     * The value for the vgagfr_image_small_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_small_url;

    /**
     * The value for the vgagfr_image_super_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_super_url;

    /**
     * The value for the vgagfr_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_thumb_url;

    /**
     * The value for the vgagfr_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagfr_image_tiny_url;

    /**
     * The value for the vgagfr_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagfr_created_at;

    /**
     * The value for the vgagfr_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagfr_updated_at;

    /**
     * @var        ObjectCollection|ChildApiGiantBombConcept[] Collection to store aggregation of ChildApiGiantBombConcept objects.
     */
    protected $collApiGiantBombFirstFranchiseConcepts;
    protected $collApiGiantBombFirstFranchiseConceptsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameFranchise[] Collection to store aggregation of ChildApiGiantBombGameFranchise objects.
     */
    protected $collApiGiantBombGameFranchises;
    protected $collApiGiantBombGameFranchisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombConcept[]
     */
    protected $apiGiantBombFirstFranchiseConceptsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameFranchise[]
     */
    protected $apiGiantBombGameFranchisesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombFranchise object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombFranchise</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombFranchise</code>, delegates to
     * <code>equals(ApiGiantBombFranchise)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombFranchise The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagfr_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagfr_id;
    }

    /**
     * Get the [vgagfr_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagfr_name;
    }

    /**
     * Get the [vgagfr_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagfr_aliases_unserialized) {
            $this->vgagfr_aliases_unserialized = array();
        }
        if (!$this->vgagfr_aliases_unserialized && null !== $this->vgagfr_aliases) {
            $vgagfr_aliases_unserialized = substr($this->vgagfr_aliases, 2, -2);
            $this->vgagfr_aliases_unserialized = $vgagfr_aliases_unserialized ? explode(' | ', $vgagfr_aliases_unserialized) : array();
        }

        return $this->vgagfr_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagfr_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagfr_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagfr_summary;
    }

    /**
     * Get the [vgagfr_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagfr_description;
    }

    /**
     * Get the [vgagfr_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagfr_api_detail_url;
    }

    /**
     * Get the [vgagfr_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagfr_site_detail_url;
    }

    /**
     * Get the [vgagfr_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagfr_image_icon_url;
    }

    /**
     * Get the [vgagfr_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagfr_image_medium_url;
    }

    /**
     * Get the [vgagfr_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagfr_image_screen_url;
    }

    /**
     * Get the [vgagfr_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagfr_image_small_url;
    }

    /**
     * Get the [vgagfr_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagfr_image_super_url;
    }

    /**
     * Get the [vgagfr_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagfr_image_thumb_url;
    }

    /**
     * Get the [vgagfr_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagfr_image_tiny_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgagfr_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagfr_created_at;
        } else {
            return $this->vgagfr_created_at instanceof \DateTimeInterface ? $this->vgagfr_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagfr_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagfr_updated_at;
        } else {
            return $this->vgagfr_updated_at instanceof \DateTimeInterface ? $this->vgagfr_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagfr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagfr_id !== $v) {
            $this->vgagfr_id = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagfr_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_name !== $v) {
            $this->vgagfr_name = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagfr_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagfr_aliases_unserialized !== $v) {
            $this->vgagfr_aliases_unserialized = $v;
            $this->vgagfr_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagfr_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagfr_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagfr_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_summary !== $v) {
            $this->vgagfr_summary = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagfr_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_description !== $v) {
            $this->vgagfr_description = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagfr_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_api_detail_url !== $v) {
            $this->vgagfr_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagfr_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_site_detail_url !== $v) {
            $this->vgagfr_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagfr_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_icon_url !== $v) {
            $this->vgagfr_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagfr_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_medium_url !== $v) {
            $this->vgagfr_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagfr_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_screen_url !== $v) {
            $this->vgagfr_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagfr_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_small_url !== $v) {
            $this->vgagfr_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagfr_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_super_url !== $v) {
            $this->vgagfr_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagfr_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_thumb_url !== $v) {
            $this->vgagfr_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagfr_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagfr_image_tiny_url !== $v) {
            $this->vgagfr_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Sets the value of [vgagfr_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagfr_created_at !== null || $dt !== null) {
            if ($this->vgagfr_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagfr_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagfr_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagfr_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagfr_updated_at !== null || $dt !== null) {
            if ($this->vgagfr_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagfr_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagfr_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_aliases = $col;
            $this->vgagfr_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagfr_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagfr_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombFranchiseTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagfr_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = ApiGiantBombFranchiseTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombFranchise'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombFranchiseQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collApiGiantBombFirstFranchiseConcepts = null;

            $this->collApiGiantBombGameFranchises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombFranchise::setDeleted()
     * @see ApiGiantBombFranchise::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombFranchiseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombFranchiseTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombFirstFranchiseConcepts !== null) {
                foreach ($this->collApiGiantBombFirstFranchiseConcepts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameFranchisesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameFranchisesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameFranchisesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameFranchisesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameFranchises !== null) {
                foreach ($this->collApiGiantBombGameFranchises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_id';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_name';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_summary';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_description';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagfr_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_franchise_vgagfr (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagfr_id':
                        $stmt->bindValue($identifier, $this->vgagfr_id, PDO::PARAM_INT);
                        break;
                    case 'vgagfr_name':
                        $stmt->bindValue($identifier, $this->vgagfr_name, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_aliases':
                        $stmt->bindValue($identifier, $this->vgagfr_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_summary':
                        $stmt->bindValue($identifier, $this->vgagfr_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_description':
                        $stmt->bindValue($identifier, $this->vgagfr_description, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagfr_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagfr_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagfr_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_created_at':
                        $stmt->bindValue($identifier, $this->vgagfr_created_at ? $this->vgagfr_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagfr_updated_at':
                        $stmt->bindValue($identifier, $this->vgagfr_updated_at ? $this->vgagfr_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombFranchiseTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getAliases();
                break;
            case 3:
                return $this->getSummary();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getApiDetailUrl();
                break;
            case 6:
                return $this->getSiteDetailUrl();
                break;
            case 7:
                return $this->getImageIconUrl();
                break;
            case 8:
                return $this->getImageMediumUrl();
                break;
            case 9:
                return $this->getImageScreenUrl();
                break;
            case 10:
                return $this->getImageSmallUrl();
                break;
            case 11:
                return $this->getImageSuperUrl();
                break;
            case 12:
                return $this->getImageThumbUrl();
                break;
            case 13:
                return $this->getImageTinyUrl();
                break;
            case 14:
                return $this->getCreatedAt();
                break;
            case 15:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombFranchise'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombFranchise'][$this->hashCode()] = true;
        $keys = ApiGiantBombFranchiseTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getAliases(),
            $keys[3] => $this->getSummary(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getApiDetailUrl(),
            $keys[6] => $this->getSiteDetailUrl(),
            $keys[7] => $this->getImageIconUrl(),
            $keys[8] => $this->getImageMediumUrl(),
            $keys[9] => $this->getImageScreenUrl(),
            $keys[10] => $this->getImageSmallUrl(),
            $keys[11] => $this->getImageSuperUrl(),
            $keys[12] => $this->getImageThumbUrl(),
            $keys[13] => $this->getImageTinyUrl(),
            $keys[14] => $this->getCreatedAt(),
            $keys[15] => $this->getUpdatedAt(),
        );
        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collApiGiantBombFirstFranchiseConcepts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombConcepts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_concept_vgagccs';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstFranchiseConcepts';
                }

                $result[$key] = $this->collApiGiantBombFirstFranchiseConcepts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameFranchises) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameFranchises';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_franchise_vgaggfs';
                        break;
                    default:
                        $key = 'ApiGiantBombGameFranchises';
                }

                $result[$key] = $this->collApiGiantBombGameFranchises->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombFranchiseTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 3:
                $this->setSummary($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setApiDetailUrl($value);
                break;
            case 6:
                $this->setSiteDetailUrl($value);
                break;
            case 7:
                $this->setImageIconUrl($value);
                break;
            case 8:
                $this->setImageMediumUrl($value);
                break;
            case 9:
                $this->setImageScreenUrl($value);
                break;
            case 10:
                $this->setImageSmallUrl($value);
                break;
            case 11:
                $this->setImageSuperUrl($value);
                break;
            case 12:
                $this->setImageThumbUrl($value);
                break;
            case 13:
                $this->setImageTinyUrl($value);
                break;
            case 14:
                $this->setCreatedAt($value);
                break;
            case 15:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombFranchiseTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAliases($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSummary($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescription($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setApiDetailUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImageIconUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImageMediumUrl($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImageScreenUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setImageSmallUrl($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setImageSuperUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageThumbUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setImageTinyUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUpdatedAt($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombFranchiseTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $this->vgagfr_id);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME, $this->vgagfr_name);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES, $this->vgagfr_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY, $this->vgagfr_summary);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION, $this->vgagfr_description);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL, $this->vgagfr_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL, $this->vgagfr_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL, $this->vgagfr_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL, $this->vgagfr_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL, $this->vgagfr_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL, $this->vgagfr_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL, $this->vgagfr_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL, $this->vgagfr_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL, $this->vgagfr_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT, $this->vgagfr_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT)) {
            $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT, $this->vgagfr_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombFranchiseQuery::create();
        $criteria->add(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $this->vgagfr_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagfr_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombFirstFranchiseConcepts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombFirstFranchiseConcept($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameFranchises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameFranchise($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombFirstFranchiseConcept' == $relationName) {
            return $this->initApiGiantBombFirstFranchiseConcepts();
        }
        if ('ApiGiantBombGameFranchise' == $relationName) {
            return $this->initApiGiantBombGameFranchises();
        }
    }

    /**
     * Clears out the collApiGiantBombFirstFranchiseConcepts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombFirstFranchiseConcepts()
     */
    public function clearApiGiantBombFirstFranchiseConcepts()
    {
        $this->collApiGiantBombFirstFranchiseConcepts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombFirstFranchiseConcepts collection loaded partially.
     */
    public function resetPartialApiGiantBombFirstFranchiseConcepts($v = true)
    {
        $this->collApiGiantBombFirstFranchiseConceptsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombFirstFranchiseConcepts collection.
     *
     * By default this just sets the collApiGiantBombFirstFranchiseConcepts collection to an empty array (like clearcollApiGiantBombFirstFranchiseConcepts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombFirstFranchiseConcepts($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombFirstFranchiseConcepts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombConceptTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombFirstFranchiseConcepts = new $collectionClassName;
        $this->collApiGiantBombFirstFranchiseConcepts->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept');
    }

    /**
     * Gets an array of ChildApiGiantBombConcept objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombFranchise is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombConcept[] List of ChildApiGiantBombConcept objects
     * @throws PropelException
     */
    public function getApiGiantBombFirstFranchiseConcepts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstFranchiseConceptsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstFranchiseConcepts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstFranchiseConcepts) {
                // return empty collection
                $this->initApiGiantBombFirstFranchiseConcepts();
            } else {
                $collApiGiantBombFirstFranchiseConcepts = ChildApiGiantBombConceptQuery::create(null, $criteria)
                    ->filterByApiGiantBombFirstFranchise($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombFirstFranchiseConceptsPartial && count($collApiGiantBombFirstFranchiseConcepts)) {
                        $this->initApiGiantBombFirstFranchiseConcepts(false);

                        foreach ($collApiGiantBombFirstFranchiseConcepts as $obj) {
                            if (false == $this->collApiGiantBombFirstFranchiseConcepts->contains($obj)) {
                                $this->collApiGiantBombFirstFranchiseConcepts->append($obj);
                            }
                        }

                        $this->collApiGiantBombFirstFranchiseConceptsPartial = true;
                    }

                    return $collApiGiantBombFirstFranchiseConcepts;
                }

                if ($partial && $this->collApiGiantBombFirstFranchiseConcepts) {
                    foreach ($this->collApiGiantBombFirstFranchiseConcepts as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombFirstFranchiseConcepts[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombFirstFranchiseConcepts = $collApiGiantBombFirstFranchiseConcepts;
                $this->collApiGiantBombFirstFranchiseConceptsPartial = false;
            }
        }

        return $this->collApiGiantBombFirstFranchiseConcepts;
    }

    /**
     * Sets a collection of ChildApiGiantBombConcept objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombFirstFranchiseConcepts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setApiGiantBombFirstFranchiseConcepts(Collection $apiGiantBombFirstFranchiseConcepts, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombConcept[] $apiGiantBombFirstFranchiseConceptsToDelete */
        $apiGiantBombFirstFranchiseConceptsToDelete = $this->getApiGiantBombFirstFranchiseConcepts(new Criteria(), $con)->diff($apiGiantBombFirstFranchiseConcepts);


        $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion = $apiGiantBombFirstFranchiseConceptsToDelete;

        foreach ($apiGiantBombFirstFranchiseConceptsToDelete as $apiGiantBombFirstFranchiseConceptRemoved) {
            $apiGiantBombFirstFranchiseConceptRemoved->setApiGiantBombFirstFranchise(null);
        }

        $this->collApiGiantBombFirstFranchiseConcepts = null;
        foreach ($apiGiantBombFirstFranchiseConcepts as $apiGiantBombFirstFranchiseConcept) {
            $this->addApiGiantBombFirstFranchiseConcept($apiGiantBombFirstFranchiseConcept);
        }

        $this->collApiGiantBombFirstFranchiseConcepts = $apiGiantBombFirstFranchiseConcepts;
        $this->collApiGiantBombFirstFranchiseConceptsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombConcept objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombConcept objects.
     * @throws PropelException
     */
    public function countApiGiantBombFirstFranchiseConcepts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstFranchiseConceptsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstFranchiseConcepts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstFranchiseConcepts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombFirstFranchiseConcepts());
            }

            $query = ChildApiGiantBombConceptQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFirstFranchise($this)
                ->count($con);
        }

        return count($this->collApiGiantBombFirstFranchiseConcepts);
    }

    /**
     * Method called to associate a ChildApiGiantBombConcept object to this object
     * through the ChildApiGiantBombConcept foreign key attribute.
     *
     * @param  ChildApiGiantBombConcept $l ChildApiGiantBombConcept
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function addApiGiantBombFirstFranchiseConcept(ChildApiGiantBombConcept $l)
    {
        if ($this->collApiGiantBombFirstFranchiseConcepts === null) {
            $this->initApiGiantBombFirstFranchiseConcepts();
            $this->collApiGiantBombFirstFranchiseConceptsPartial = true;
        }

        if (!$this->collApiGiantBombFirstFranchiseConcepts->contains($l)) {
            $this->doAddApiGiantBombFirstFranchiseConcept($l);

            if ($this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion and $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->remove($this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombConcept $apiGiantBombFirstFranchiseConcept The ChildApiGiantBombConcept object to add.
     */
    protected function doAddApiGiantBombFirstFranchiseConcept(ChildApiGiantBombConcept $apiGiantBombFirstFranchiseConcept)
    {
        $this->collApiGiantBombFirstFranchiseConcepts[]= $apiGiantBombFirstFranchiseConcept;
        $apiGiantBombFirstFranchiseConcept->setApiGiantBombFirstFranchise($this);
    }

    /**
     * @param  ChildApiGiantBombConcept $apiGiantBombFirstFranchiseConcept The ChildApiGiantBombConcept object to remove.
     * @return $this|ChildApiGiantBombFranchise The current object (for fluent API support)
     */
    public function removeApiGiantBombFirstFranchiseConcept(ChildApiGiantBombConcept $apiGiantBombFirstFranchiseConcept)
    {
        if ($this->getApiGiantBombFirstFranchiseConcepts()->contains($apiGiantBombFirstFranchiseConcept)) {
            $pos = $this->collApiGiantBombFirstFranchiseConcepts->search($apiGiantBombFirstFranchiseConcept);
            $this->collApiGiantBombFirstFranchiseConcepts->remove($pos);
            if (null === $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion) {
                $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion = clone $this->collApiGiantBombFirstFranchiseConcepts;
                $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion->clear();
            }
            $this->apiGiantBombFirstFranchiseConceptsScheduledForDeletion[]= $apiGiantBombFirstFranchiseConcept;
            $apiGiantBombFirstFranchiseConcept->setApiGiantBombFirstFranchise(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombFranchise is new, it will return
     * an empty collection; or if this ApiGiantBombFranchise has previously
     * been saved, it will retrieve related ApiGiantBombFirstFranchiseConcepts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombFranchise.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombConcept[] List of ChildApiGiantBombConcept objects
     */
    public function getApiGiantBombFirstFranchiseConceptsJoinApiGiantBombFirstGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombConceptQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombFirstGame', $joinBehavior);

        return $this->getApiGiantBombFirstFranchiseConcepts($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameFranchises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameFranchises()
     */
    public function clearApiGiantBombGameFranchises()
    {
        $this->collApiGiantBombGameFranchises = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameFranchises collection loaded partially.
     */
    public function resetPartialApiGiantBombGameFranchises($v = true)
    {
        $this->collApiGiantBombGameFranchisesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameFranchises collection.
     *
     * By default this just sets the collApiGiantBombGameFranchises collection to an empty array (like clearcollApiGiantBombGameFranchises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameFranchises($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameFranchises && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameFranchiseTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameFranchises = new $collectionClassName;
        $this->collApiGiantBombGameFranchises->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise');
    }

    /**
     * Gets an array of ChildApiGiantBombGameFranchise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombFranchise is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameFranchise[] List of ChildApiGiantBombGameFranchise objects
     * @throws PropelException
     */
    public function getApiGiantBombGameFranchises(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameFranchisesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameFranchises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameFranchises) {
                // return empty collection
                $this->initApiGiantBombGameFranchises();
            } else {
                $collApiGiantBombGameFranchises = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria)
                    ->filterByApiGiantBombFranchise($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameFranchisesPartial && count($collApiGiantBombGameFranchises)) {
                        $this->initApiGiantBombGameFranchises(false);

                        foreach ($collApiGiantBombGameFranchises as $obj) {
                            if (false == $this->collApiGiantBombGameFranchises->contains($obj)) {
                                $this->collApiGiantBombGameFranchises->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameFranchisesPartial = true;
                    }

                    return $collApiGiantBombGameFranchises;
                }

                if ($partial && $this->collApiGiantBombGameFranchises) {
                    foreach ($this->collApiGiantBombGameFranchises as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameFranchises[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameFranchises = $collApiGiantBombGameFranchises;
                $this->collApiGiantBombGameFranchisesPartial = false;
            }
        }

        return $this->collApiGiantBombGameFranchises;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameFranchise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameFranchises A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombFranchise The current object (for fluent API support)
     */
    public function setApiGiantBombGameFranchises(Collection $apiGiantBombGameFranchises, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameFranchise[] $apiGiantBombGameFranchisesToDelete */
        $apiGiantBombGameFranchisesToDelete = $this->getApiGiantBombGameFranchises(new Criteria(), $con)->diff($apiGiantBombGameFranchises);


        $this->apiGiantBombGameFranchisesScheduledForDeletion = $apiGiantBombGameFranchisesToDelete;

        foreach ($apiGiantBombGameFranchisesToDelete as $apiGiantBombGameFranchiseRemoved) {
            $apiGiantBombGameFranchiseRemoved->setApiGiantBombFranchise(null);
        }

        $this->collApiGiantBombGameFranchises = null;
        foreach ($apiGiantBombGameFranchises as $apiGiantBombGameFranchise) {
            $this->addApiGiantBombGameFranchise($apiGiantBombGameFranchise);
        }

        $this->collApiGiantBombGameFranchises = $apiGiantBombGameFranchises;
        $this->collApiGiantBombGameFranchisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameFranchise objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameFranchise objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameFranchises(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameFranchisesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameFranchises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameFranchises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameFranchises());
            }

            $query = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFranchise($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameFranchises);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameFranchise object to this object
     * through the ChildApiGiantBombGameFranchise foreign key attribute.
     *
     * @param  ChildApiGiantBombGameFranchise $l ChildApiGiantBombGameFranchise
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise The current object (for fluent API support)
     */
    public function addApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $l)
    {
        if ($this->collApiGiantBombGameFranchises === null) {
            $this->initApiGiantBombGameFranchises();
            $this->collApiGiantBombGameFranchisesPartial = true;
        }

        if (!$this->collApiGiantBombGameFranchises->contains($l)) {
            $this->doAddApiGiantBombGameFranchise($l);

            if ($this->apiGiantBombGameFranchisesScheduledForDeletion and $this->apiGiantBombGameFranchisesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameFranchisesScheduledForDeletion->remove($this->apiGiantBombGameFranchisesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise The ChildApiGiantBombGameFranchise object to add.
     */
    protected function doAddApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise)
    {
        $this->collApiGiantBombGameFranchises[]= $apiGiantBombGameFranchise;
        $apiGiantBombGameFranchise->setApiGiantBombFranchise($this);
    }

    /**
     * @param  ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise The ChildApiGiantBombGameFranchise object to remove.
     * @return $this|ChildApiGiantBombFranchise The current object (for fluent API support)
     */
    public function removeApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise)
    {
        if ($this->getApiGiantBombGameFranchises()->contains($apiGiantBombGameFranchise)) {
            $pos = $this->collApiGiantBombGameFranchises->search($apiGiantBombGameFranchise);
            $this->collApiGiantBombGameFranchises->remove($pos);
            if (null === $this->apiGiantBombGameFranchisesScheduledForDeletion) {
                $this->apiGiantBombGameFranchisesScheduledForDeletion = clone $this->collApiGiantBombGameFranchises;
                $this->apiGiantBombGameFranchisesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameFranchisesScheduledForDeletion[]= clone $apiGiantBombGameFranchise;
            $apiGiantBombGameFranchise->setApiGiantBombFranchise(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombFranchise is new, it will return
     * an empty collection; or if this ApiGiantBombFranchise has previously
     * been saved, it will retrieve related ApiGiantBombGameFranchises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombFranchise.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameFranchise[] List of ChildApiGiantBombGameFranchise objects
     */
    public function getApiGiantBombGameFranchisesJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGameFranchises($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->vgagfr_id = null;
        $this->vgagfr_name = null;
        $this->vgagfr_aliases = null;
        $this->vgagfr_aliases_unserialized = null;
        $this->vgagfr_summary = null;
        $this->vgagfr_description = null;
        $this->vgagfr_api_detail_url = null;
        $this->vgagfr_site_detail_url = null;
        $this->vgagfr_image_icon_url = null;
        $this->vgagfr_image_medium_url = null;
        $this->vgagfr_image_screen_url = null;
        $this->vgagfr_image_small_url = null;
        $this->vgagfr_image_super_url = null;
        $this->vgagfr_image_thumb_url = null;
        $this->vgagfr_image_tiny_url = null;
        $this->vgagfr_created_at = null;
        $this->vgagfr_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombFirstFranchiseConcepts) {
                foreach ($this->collApiGiantBombFirstFranchiseConcepts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameFranchises) {
                foreach ($this->collApiGiantBombGameFranchises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombFirstFranchiseConcepts = null;
        $this->collApiGiantBombGameFranchises = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombFranchiseTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
