<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\Game as ChildGame;
use IiMedias\VideoGamesBundle\Model\GameQuery as ChildGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGame as ChildSiteGameKultGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery as ChildSiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease as ChildSiteGameKultGameRelease;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery as ChildSiteGameKultGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultGameReleaseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_site_gamekult_game_vgakga' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class SiteGameKultGame implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\SiteGameKultGameTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgakga_id field.
     *
     * @var        string
     */
    protected $vgakga_id;

    /**
     * The value for the vgakga_type_id field.
     *
     * @var        string
     */
    protected $vgakga_type_id;

    /**
     * The value for the vgakga_gamekult_id field.
     *
     * @var        string
     */
    protected $vgakga_gamekult_id;

    /**
     * The value for the vgakga_vgagam_id field.
     *
     * @var        int
     */
    protected $vgakga_vgagam_id;

    /**
     * The value for the vgakga_name field.
     *
     * @var        string
     */
    protected $vgakga_name;

    /**
     * The value for the vgakga_aliases field.
     *
     * @var        array
     */
    protected $vgakga_aliases;

    /**
     * The unserialized $vgakga_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_aliases_unserialized;

    /**
     * The value for the vgakga_original_price field.
     *
     * @var        double
     */
    protected $vgakga_original_price;

    /**
     * The value for the vgakga_platforms_array field.
     *
     * @var        array
     */
    protected $vgakga_platforms_array;

    /**
     * The unserialized $vgakga_platforms_array value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_platforms_array_unserialized;

    /**
     * The value for the vgakga_genres_array field.
     *
     * @var        array
     */
    protected $vgakga_genres_array;

    /**
     * The unserialized $vgakga_genres_array value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_genres_array_unserialized;

    /**
     * The value for the vgakga_themes_array field.
     *
     * @var        array
     */
    protected $vgakga_themes_array;

    /**
     * The unserialized $vgakga_themes_array value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_themes_array_unserialized;

    /**
     * The value for the vgakga_companies_array field.
     *
     * @var        array
     */
    protected $vgakga_companies_array;

    /**
     * The unserialized $vgakga_companies_array value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_companies_array_unserialized;

    /**
     * The value for the vgakga_franchises_array field.
     *
     * @var        array
     */
    protected $vgakga_franchises_array;

    /**
     * The unserialized $vgakga_franchises_array value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgakga_franchises_array_unserialized;

    /**
     * The value for the vgakga_fr_released_at field.
     *
     * @var        DateTime
     */
    protected $vgakga_fr_released_at;

    /**
     * The value for the vgakga_us_released_at field.
     *
     * @var        DateTime
     */
    protected $vgakga_us_released_at;

    /**
     * The value for the vgakga_jp_released_at field.
     *
     * @var        DateTime
     */
    protected $vgakga_jp_released_at;

    /**
     * The value for the vgakga_image_small_url field.
     *
     * @var        string
     */
    protected $vgakga_image_small_url;

    /**
     * The value for the vgakga_site_detail_url field.
     *
     * @var        string
     */
    protected $vgakga_site_detail_url;

    /**
     * The value for the vgakga_refreshed_at field.
     *
     * @var        DateTime
     */
    protected $vgakga_refreshed_at;

    /**
     * @var        ChildGame
     */
    protected $aGame;

    /**
     * @var        ObjectCollection|ChildSiteGameKultGameRelease[] Collection to store aggregation of ChildSiteGameKultGameRelease objects.
     */
    protected $collSiteGameKultGameReleases;
    protected $collSiteGameKultGameReleasesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteGameKultGameRelease[]
     */
    protected $siteGameKultGameReleasesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\SiteGameKultGame object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SiteGameKultGame</code> instance.  If
     * <code>obj</code> is an instance of <code>SiteGameKultGame</code>, delegates to
     * <code>equals(SiteGameKultGame)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|SiteGameKultGame The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgakga_id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->vgakga_id;
    }

    /**
     * Get the [vgakga_type_id] column value.
     *
     * @return string
     */
    public function getTypeId()
    {
        return $this->vgakga_type_id;
    }

    /**
     * Get the [vgakga_gamekult_id] column value.
     *
     * @return string
     */
    public function getGamekultId()
    {
        return $this->vgakga_gamekult_id;
    }

    /**
     * Get the [vgakga_vgagam_id] column value.
     *
     * @return int
     */
    public function getGameId()
    {
        return $this->vgakga_vgagam_id;
    }

    /**
     * Get the [vgakga_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgakga_name;
    }

    /**
     * Get the [vgakga_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgakga_aliases_unserialized) {
            $this->vgakga_aliases_unserialized = array();
        }
        if (!$this->vgakga_aliases_unserialized && null !== $this->vgakga_aliases) {
            $vgakga_aliases_unserialized = substr($this->vgakga_aliases, 2, -2);
            $this->vgakga_aliases_unserialized = $vgakga_aliases_unserialized ? explode(' | ', $vgakga_aliases_unserialized) : array();
        }

        return $this->vgakga_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgakga_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgakga_original_price] column value.
     *
     * @return double
     */
    public function getOriginalPrice()
    {
        return $this->vgakga_original_price;
    }

    /**
     * Get the [vgakga_platforms_array] column value.
     *
     * @return array
     */
    public function getPlatformsArray()
    {
        if (null === $this->vgakga_platforms_array_unserialized) {
            $this->vgakga_platforms_array_unserialized = array();
        }
        if (!$this->vgakga_platforms_array_unserialized && null !== $this->vgakga_platforms_array) {
            $vgakga_platforms_array_unserialized = substr($this->vgakga_platforms_array, 2, -2);
            $this->vgakga_platforms_array_unserialized = $vgakga_platforms_array_unserialized ? explode(' | ', $vgakga_platforms_array_unserialized) : array();
        }

        return $this->vgakga_platforms_array_unserialized;
    }

    /**
     * Get the [vgakga_genres_array] column value.
     *
     * @return array
     */
    public function getGenresArray()
    {
        if (null === $this->vgakga_genres_array_unserialized) {
            $this->vgakga_genres_array_unserialized = array();
        }
        if (!$this->vgakga_genres_array_unserialized && null !== $this->vgakga_genres_array) {
            $vgakga_genres_array_unserialized = substr($this->vgakga_genres_array, 2, -2);
            $this->vgakga_genres_array_unserialized = $vgakga_genres_array_unserialized ? explode(' | ', $vgakga_genres_array_unserialized) : array();
        }

        return $this->vgakga_genres_array_unserialized;
    }

    /**
     * Get the [vgakga_themes_array] column value.
     *
     * @return array
     */
    public function getThemesArray()
    {
        if (null === $this->vgakga_themes_array_unserialized) {
            $this->vgakga_themes_array_unserialized = array();
        }
        if (!$this->vgakga_themes_array_unserialized && null !== $this->vgakga_themes_array) {
            $vgakga_themes_array_unserialized = substr($this->vgakga_themes_array, 2, -2);
            $this->vgakga_themes_array_unserialized = $vgakga_themes_array_unserialized ? explode(' | ', $vgakga_themes_array_unserialized) : array();
        }

        return $this->vgakga_themes_array_unserialized;
    }

    /**
     * Get the [vgakga_companies_array] column value.
     *
     * @return array
     */
    public function getCompaniesArray()
    {
        if (null === $this->vgakga_companies_array_unserialized) {
            $this->vgakga_companies_array_unserialized = array();
        }
        if (!$this->vgakga_companies_array_unserialized && null !== $this->vgakga_companies_array) {
            $vgakga_companies_array_unserialized = substr($this->vgakga_companies_array, 2, -2);
            $this->vgakga_companies_array_unserialized = $vgakga_companies_array_unserialized ? explode(' | ', $vgakga_companies_array_unserialized) : array();
        }

        return $this->vgakga_companies_array_unserialized;
    }

    /**
     * Get the [vgakga_franchises_array] column value.
     *
     * @return array
     */
    public function getFranchisesArray()
    {
        if (null === $this->vgakga_franchises_array_unserialized) {
            $this->vgakga_franchises_array_unserialized = array();
        }
        if (!$this->vgakga_franchises_array_unserialized && null !== $this->vgakga_franchises_array) {
            $vgakga_franchises_array_unserialized = substr($this->vgakga_franchises_array, 2, -2);
            $this->vgakga_franchises_array_unserialized = $vgakga_franchises_array_unserialized ? explode(' | ', $vgakga_franchises_array_unserialized) : array();
        }

        return $this->vgakga_franchises_array_unserialized;
    }

    /**
     * Get the [optionally formatted] temporal [vgakga_fr_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFrReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgakga_fr_released_at;
        } else {
            return $this->vgakga_fr_released_at instanceof \DateTimeInterface ? $this->vgakga_fr_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgakga_us_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUsReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgakga_us_released_at;
        } else {
            return $this->vgakga_us_released_at instanceof \DateTimeInterface ? $this->vgakga_us_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgakga_jp_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getJpReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgakga_jp_released_at;
        } else {
            return $this->vgakga_jp_released_at instanceof \DateTimeInterface ? $this->vgakga_jp_released_at->format($format) : null;
        }
    }

    /**
     * Get the [vgakga_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgakga_image_small_url;
    }

    /**
     * Get the [vgakga_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgakga_site_detail_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgakga_refreshed_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRefreshedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgakga_refreshed_at;
        } else {
            return $this->vgakga_refreshed_at instanceof \DateTimeInterface ? $this->vgakga_refreshed_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgakga_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_id !== $v) {
            $this->vgakga_id = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgakga_type_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_type_id !== $v) {
            $this->vgakga_type_id = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID] = true;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [vgakga_gamekult_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setGamekultId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_gamekult_id !== $v) {
            $this->vgakga_gamekult_id = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID] = true;
        }

        return $this;
    } // setGamekultId()

    /**
     * Set the value of [vgakga_vgagam_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgakga_vgagam_id !== $v) {
            $this->vgakga_vgagam_id = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID] = true;
        }

        if ($this->aGame !== null && $this->aGame->getId() !== $v) {
            $this->aGame = null;
        }

        return $this;
    } // setGameId()

    /**
     * Set the value of [vgakga_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_name !== $v) {
            $this->vgakga_name = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgakga_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgakga_aliases_unserialized !== $v) {
            $this->vgakga_aliases_unserialized = $v;
            $this->vgakga_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgakga_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgakga_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgakga_original_price] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setOriginalPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->vgakga_original_price !== $v) {
            $this->vgakga_original_price = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE] = true;
        }

        return $this;
    } // setOriginalPrice()

    /**
     * Set the value of [vgakga_platforms_array] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setPlatformsArray($v)
    {
        if ($this->vgakga_platforms_array_unserialized !== $v) {
            $this->vgakga_platforms_array_unserialized = $v;
            $this->vgakga_platforms_array = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY] = true;
        }

        return $this;
    } // setPlatformsArray()

    /**
     * Set the value of [vgakga_genres_array] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setGenresArray($v)
    {
        if ($this->vgakga_genres_array_unserialized !== $v) {
            $this->vgakga_genres_array_unserialized = $v;
            $this->vgakga_genres_array = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY] = true;
        }

        return $this;
    } // setGenresArray()

    /**
     * Set the value of [vgakga_themes_array] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setThemesArray($v)
    {
        if ($this->vgakga_themes_array_unserialized !== $v) {
            $this->vgakga_themes_array_unserialized = $v;
            $this->vgakga_themes_array = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY] = true;
        }

        return $this;
    } // setThemesArray()

    /**
     * Set the value of [vgakga_companies_array] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setCompaniesArray($v)
    {
        if ($this->vgakga_companies_array_unserialized !== $v) {
            $this->vgakga_companies_array_unserialized = $v;
            $this->vgakga_companies_array = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY] = true;
        }

        return $this;
    } // setCompaniesArray()

    /**
     * Set the value of [vgakga_franchises_array] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setFranchisesArray($v)
    {
        if ($this->vgakga_franchises_array_unserialized !== $v) {
            $this->vgakga_franchises_array_unserialized = $v;
            $this->vgakga_franchises_array = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY] = true;
        }

        return $this;
    } // setFranchisesArray()

    /**
     * Sets the value of [vgakga_fr_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setFrReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgakga_fr_released_at !== null || $dt !== null) {
            if ($this->vgakga_fr_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgakga_fr_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgakga_fr_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setFrReleasedAt()

    /**
     * Sets the value of [vgakga_us_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setUsReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgakga_us_released_at !== null || $dt !== null) {
            if ($this->vgakga_us_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgakga_us_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgakga_us_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUsReleasedAt()

    /**
     * Sets the value of [vgakga_jp_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setJpReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgakga_jp_released_at !== null || $dt !== null) {
            if ($this->vgakga_jp_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgakga_jp_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgakga_jp_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setJpReleasedAt()

    /**
     * Set the value of [vgakga_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_image_small_url !== $v) {
            $this->vgakga_image_small_url = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgakga_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgakga_site_detail_url !== $v) {
            $this->vgakga_site_detail_url = $v;
            $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Sets the value of [vgakga_refreshed_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function setRefreshedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgakga_refreshed_at !== null || $dt !== null) {
            if ($this->vgakga_refreshed_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgakga_refreshed_at->format("Y-m-d H:i:s.u")) {
                $this->vgakga_refreshed_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setRefreshedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SiteGameKultGameTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SiteGameKultGameTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_type_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SiteGameKultGameTableMap::translateFieldName('GamekultId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_gamekult_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SiteGameKultGameTableMap::translateFieldName('GameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_vgagam_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SiteGameKultGameTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SiteGameKultGameTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_aliases = $col;
            $this->vgakga_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SiteGameKultGameTableMap::translateFieldName('OriginalPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_original_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SiteGameKultGameTableMap::translateFieldName('PlatformsArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_platforms_array = $col;
            $this->vgakga_platforms_array_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SiteGameKultGameTableMap::translateFieldName('GenresArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_genres_array = $col;
            $this->vgakga_genres_array_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SiteGameKultGameTableMap::translateFieldName('ThemesArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_themes_array = $col;
            $this->vgakga_themes_array_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SiteGameKultGameTableMap::translateFieldName('CompaniesArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_companies_array = $col;
            $this->vgakga_companies_array_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SiteGameKultGameTableMap::translateFieldName('FranchisesArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_franchises_array = $col;
            $this->vgakga_franchises_array_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SiteGameKultGameTableMap::translateFieldName('FrReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgakga_fr_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SiteGameKultGameTableMap::translateFieldName('UsReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgakga_us_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SiteGameKultGameTableMap::translateFieldName('JpReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgakga_jp_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SiteGameKultGameTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SiteGameKultGameTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgakga_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SiteGameKultGameTableMap::translateFieldName('RefreshedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgakga_refreshed_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = SiteGameKultGameTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGame'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aGame !== null && $this->vgakga_vgagam_id !== $this->aGame->getId()) {
            $this->aGame = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSiteGameKultGameQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aGame = null;
            $this->collSiteGameKultGameReleases = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SiteGameKultGame::setDeleted()
     * @see SiteGameKultGame::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSiteGameKultGameQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SiteGameKultGameTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aGame !== null) {
                if ($this->aGame->isModified() || $this->aGame->isNew()) {
                    $affectedRows += $this->aGame->save($con);
                }
                $this->setGame($this->aGame);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->siteGameKultGameReleasesScheduledForDeletion !== null) {
                if (!$this->siteGameKultGameReleasesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery::create()
                        ->filterByPrimaryKeys($this->siteGameKultGameReleasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteGameKultGameReleasesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteGameKultGameReleases !== null) {
                foreach ($this->collSiteGameKultGameReleases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_id';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_type_id';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_gamekult_id';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_vgagam_id';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_name';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_aliases';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_original_price';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_platforms_array';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_genres_array';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_themes_array';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_companies_array';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_franchises_array';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_fr_released_at';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_us_released_at';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_jp_released_at';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_image_small_url';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_site_detail_url';
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgakga_refreshed_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_site_gamekult_game_vgakga (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgakga_id':
                        $stmt->bindValue($identifier, $this->vgakga_id, PDO::PARAM_INT);
                        break;
                    case 'vgakga_type_id':
                        $stmt->bindValue($identifier, $this->vgakga_type_id, PDO::PARAM_STR);
                        break;
                    case 'vgakga_gamekult_id':
                        $stmt->bindValue($identifier, $this->vgakga_gamekult_id, PDO::PARAM_STR);
                        break;
                    case 'vgakga_vgagam_id':
                        $stmt->bindValue($identifier, $this->vgakga_vgagam_id, PDO::PARAM_INT);
                        break;
                    case 'vgakga_name':
                        $stmt->bindValue($identifier, $this->vgakga_name, PDO::PARAM_STR);
                        break;
                    case 'vgakga_aliases':
                        $stmt->bindValue($identifier, $this->vgakga_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgakga_original_price':
                        $stmt->bindValue($identifier, $this->vgakga_original_price, PDO::PARAM_STR);
                        break;
                    case 'vgakga_platforms_array':
                        $stmt->bindValue($identifier, $this->vgakga_platforms_array, PDO::PARAM_STR);
                        break;
                    case 'vgakga_genres_array':
                        $stmt->bindValue($identifier, $this->vgakga_genres_array, PDO::PARAM_STR);
                        break;
                    case 'vgakga_themes_array':
                        $stmt->bindValue($identifier, $this->vgakga_themes_array, PDO::PARAM_STR);
                        break;
                    case 'vgakga_companies_array':
                        $stmt->bindValue($identifier, $this->vgakga_companies_array, PDO::PARAM_STR);
                        break;
                    case 'vgakga_franchises_array':
                        $stmt->bindValue($identifier, $this->vgakga_franchises_array, PDO::PARAM_STR);
                        break;
                    case 'vgakga_fr_released_at':
                        $stmt->bindValue($identifier, $this->vgakga_fr_released_at ? $this->vgakga_fr_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgakga_us_released_at':
                        $stmt->bindValue($identifier, $this->vgakga_us_released_at ? $this->vgakga_us_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgakga_jp_released_at':
                        $stmt->bindValue($identifier, $this->vgakga_jp_released_at ? $this->vgakga_jp_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgakga_image_small_url':
                        $stmt->bindValue($identifier, $this->vgakga_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgakga_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgakga_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgakga_refreshed_at':
                        $stmt->bindValue($identifier, $this->vgakga_refreshed_at ? $this->vgakga_refreshed_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteGameKultGameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTypeId();
                break;
            case 2:
                return $this->getGamekultId();
                break;
            case 3:
                return $this->getGameId();
                break;
            case 4:
                return $this->getName();
                break;
            case 5:
                return $this->getAliases();
                break;
            case 6:
                return $this->getOriginalPrice();
                break;
            case 7:
                return $this->getPlatformsArray();
                break;
            case 8:
                return $this->getGenresArray();
                break;
            case 9:
                return $this->getThemesArray();
                break;
            case 10:
                return $this->getCompaniesArray();
                break;
            case 11:
                return $this->getFranchisesArray();
                break;
            case 12:
                return $this->getFrReleasedAt();
                break;
            case 13:
                return $this->getUsReleasedAt();
                break;
            case 14:
                return $this->getJpReleasedAt();
                break;
            case 15:
                return $this->getImageSmallUrl();
                break;
            case 16:
                return $this->getSiteDetailUrl();
                break;
            case 17:
                return $this->getRefreshedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SiteGameKultGame'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SiteGameKultGame'][$this->hashCode()] = true;
        $keys = SiteGameKultGameTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTypeId(),
            $keys[2] => $this->getGamekultId(),
            $keys[3] => $this->getGameId(),
            $keys[4] => $this->getName(),
            $keys[5] => $this->getAliases(),
            $keys[6] => $this->getOriginalPrice(),
            $keys[7] => $this->getPlatformsArray(),
            $keys[8] => $this->getGenresArray(),
            $keys[9] => $this->getThemesArray(),
            $keys[10] => $this->getCompaniesArray(),
            $keys[11] => $this->getFranchisesArray(),
            $keys[12] => $this->getFrReleasedAt(),
            $keys[13] => $this->getUsReleasedAt(),
            $keys[14] => $this->getJpReleasedAt(),
            $keys[15] => $this->getImageSmallUrl(),
            $keys[16] => $this->getSiteDetailUrl(),
            $keys[17] => $this->getRefreshedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'game';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_game_vgagam';
                        break;
                    default:
                        $key = 'Game';
                }

                $result[$key] = $this->aGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSiteGameKultGameReleases) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteGameKultGameReleases';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_site_gamekult_release_vgakrls';
                        break;
                    default:
                        $key = 'SiteGameKultGameReleases';
                }

                $result[$key] = $this->collSiteGameKultGameReleases->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteGameKultGameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTypeId($value);
                break;
            case 2:
                $this->setGamekultId($value);
                break;
            case 3:
                $this->setGameId($value);
                break;
            case 4:
                $this->setName($value);
                break;
            case 5:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 6:
                $this->setOriginalPrice($value);
                break;
            case 7:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setPlatformsArray($value);
                break;
            case 8:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setGenresArray($value);
                break;
            case 9:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setThemesArray($value);
                break;
            case 10:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setCompaniesArray($value);
                break;
            case 11:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setFranchisesArray($value);
                break;
            case 12:
                $this->setFrReleasedAt($value);
                break;
            case 13:
                $this->setUsReleasedAt($value);
                break;
            case 14:
                $this->setJpReleasedAt($value);
                break;
            case 15:
                $this->setImageSmallUrl($value);
                break;
            case 16:
                $this->setSiteDetailUrl($value);
                break;
            case 17:
                $this->setRefreshedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SiteGameKultGameTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTypeId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setGamekultId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setGameId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAliases($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setOriginalPrice($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPlatformsArray($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setGenresArray($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setThemesArray($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCompaniesArray($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFranchisesArray($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setFrReleasedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUsReleasedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setJpReleasedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setImageSmallUrl($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setRefreshedAt($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SiteGameKultGameTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ID)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_ID, $this->vgakga_id);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID, $this->vgakga_type_id);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID, $this->vgakga_gamekult_id);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $this->vgakga_vgagam_id);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_NAME)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_NAME, $this->vgakga_name);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES, $this->vgakga_aliases);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE, $this->vgakga_original_price);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY, $this->vgakga_platforms_array);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY, $this->vgakga_genres_array);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY, $this->vgakga_themes_array);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY, $this->vgakga_companies_array);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY, $this->vgakga_franchises_array);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT, $this->vgakga_fr_released_at);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT, $this->vgakga_us_released_at);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT, $this->vgakga_jp_released_at);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL, $this->vgakga_image_small_url);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL, $this->vgakga_site_detail_url);
        }
        if ($this->isColumnModified(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT)) {
            $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT, $this->vgakga_refreshed_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSiteGameKultGameQuery::create();
        $criteria->add(SiteGameKultGameTableMap::COL_VGAKGA_ID, $this->vgakga_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgakga_id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\SiteGameKultGame (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setGamekultId($this->getGamekultId());
        $copyObj->setGameId($this->getGameId());
        $copyObj->setName($this->getName());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setOriginalPrice($this->getOriginalPrice());
        $copyObj->setPlatformsArray($this->getPlatformsArray());
        $copyObj->setGenresArray($this->getGenresArray());
        $copyObj->setThemesArray($this->getThemesArray());
        $copyObj->setCompaniesArray($this->getCompaniesArray());
        $copyObj->setFranchisesArray($this->getFranchisesArray());
        $copyObj->setFrReleasedAt($this->getFrReleasedAt());
        $copyObj->setUsReleasedAt($this->getUsReleasedAt());
        $copyObj->setJpReleasedAt($this->getJpReleasedAt());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setRefreshedAt($this->getRefreshedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSiteGameKultGameReleases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteGameKultGameRelease($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultGame Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildGame object.
     *
     * @param  ChildGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     * @throws PropelException
     */
    public function setGame(ChildGame $v = null)
    {
        if ($v === null) {
            $this->setGameId(NULL);
        } else {
            $this->setGameId($v->getId());
        }

        $this->aGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildGame object, it will not be re-added.
        if ($v !== null) {
            $v->addSiteGameKultGame($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildGame The associated ChildGame object.
     * @throws PropelException
     */
    public function getGame(ConnectionInterface $con = null)
    {
        if ($this->aGame === null && ($this->vgakga_vgagam_id !== null)) {
            $this->aGame = ChildGameQuery::create()->findPk($this->vgakga_vgagam_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aGame->addSiteGameKultGames($this);
             */
        }

        return $this->aGame;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SiteGameKultGameRelease' == $relationName) {
            return $this->initSiteGameKultGameReleases();
        }
    }

    /**
     * Clears out the collSiteGameKultGameReleases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteGameKultGameReleases()
     */
    public function clearSiteGameKultGameReleases()
    {
        $this->collSiteGameKultGameReleases = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteGameKultGameReleases collection loaded partially.
     */
    public function resetPartialSiteGameKultGameReleases($v = true)
    {
        $this->collSiteGameKultGameReleasesPartial = $v;
    }

    /**
     * Initializes the collSiteGameKultGameReleases collection.
     *
     * By default this just sets the collSiteGameKultGameReleases collection to an empty array (like clearcollSiteGameKultGameReleases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteGameKultGameReleases($overrideExisting = true)
    {
        if (null !== $this->collSiteGameKultGameReleases && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteGameKultGameReleaseTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteGameKultGameReleases = new $collectionClassName;
        $this->collSiteGameKultGameReleases->setModel('\IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease');
    }

    /**
     * Gets an array of ChildSiteGameKultGameRelease objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSiteGameKultGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteGameKultGameRelease[] List of ChildSiteGameKultGameRelease objects
     * @throws PropelException
     */
    public function getSiteGameKultGameReleases(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameKultGameReleasesPartial && !$this->isNew();
        if (null === $this->collSiteGameKultGameReleases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSiteGameKultGameReleases) {
                // return empty collection
                $this->initSiteGameKultGameReleases();
            } else {
                $collSiteGameKultGameReleases = ChildSiteGameKultGameReleaseQuery::create(null, $criteria)
                    ->filterBySiteGameKultGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteGameKultGameReleasesPartial && count($collSiteGameKultGameReleases)) {
                        $this->initSiteGameKultGameReleases(false);

                        foreach ($collSiteGameKultGameReleases as $obj) {
                            if (false == $this->collSiteGameKultGameReleases->contains($obj)) {
                                $this->collSiteGameKultGameReleases->append($obj);
                            }
                        }

                        $this->collSiteGameKultGameReleasesPartial = true;
                    }

                    return $collSiteGameKultGameReleases;
                }

                if ($partial && $this->collSiteGameKultGameReleases) {
                    foreach ($this->collSiteGameKultGameReleases as $obj) {
                        if ($obj->isNew()) {
                            $collSiteGameKultGameReleases[] = $obj;
                        }
                    }
                }

                $this->collSiteGameKultGameReleases = $collSiteGameKultGameReleases;
                $this->collSiteGameKultGameReleasesPartial = false;
            }
        }

        return $this->collSiteGameKultGameReleases;
    }

    /**
     * Sets a collection of ChildSiteGameKultGameRelease objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteGameKultGameReleases A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSiteGameKultGame The current object (for fluent API support)
     */
    public function setSiteGameKultGameReleases(Collection $siteGameKultGameReleases, ConnectionInterface $con = null)
    {
        /** @var ChildSiteGameKultGameRelease[] $siteGameKultGameReleasesToDelete */
        $siteGameKultGameReleasesToDelete = $this->getSiteGameKultGameReleases(new Criteria(), $con)->diff($siteGameKultGameReleases);


        $this->siteGameKultGameReleasesScheduledForDeletion = $siteGameKultGameReleasesToDelete;

        foreach ($siteGameKultGameReleasesToDelete as $siteGameKultGameReleaseRemoved) {
            $siteGameKultGameReleaseRemoved->setSiteGameKultGame(null);
        }

        $this->collSiteGameKultGameReleases = null;
        foreach ($siteGameKultGameReleases as $siteGameKultGameRelease) {
            $this->addSiteGameKultGameRelease($siteGameKultGameRelease);
        }

        $this->collSiteGameKultGameReleases = $siteGameKultGameReleases;
        $this->collSiteGameKultGameReleasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteGameKultGameRelease objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteGameKultGameRelease objects.
     * @throws PropelException
     */
    public function countSiteGameKultGameReleases(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameKultGameReleasesPartial && !$this->isNew();
        if (null === $this->collSiteGameKultGameReleases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteGameKultGameReleases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteGameKultGameReleases());
            }

            $query = ChildSiteGameKultGameReleaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySiteGameKultGame($this)
                ->count($con);
        }

        return count($this->collSiteGameKultGameReleases);
    }

    /**
     * Method called to associate a ChildSiteGameKultGameRelease object to this object
     * through the ChildSiteGameKultGameRelease foreign key attribute.
     *
     * @param  ChildSiteGameKultGameRelease $l ChildSiteGameKultGameRelease
     * @return $this|\IiMedias\VideoGamesBundle\Model\SiteGameKultGame The current object (for fluent API support)
     */
    public function addSiteGameKultGameRelease(ChildSiteGameKultGameRelease $l)
    {
        if ($this->collSiteGameKultGameReleases === null) {
            $this->initSiteGameKultGameReleases();
            $this->collSiteGameKultGameReleasesPartial = true;
        }

        if (!$this->collSiteGameKultGameReleases->contains($l)) {
            $this->doAddSiteGameKultGameRelease($l);

            if ($this->siteGameKultGameReleasesScheduledForDeletion and $this->siteGameKultGameReleasesScheduledForDeletion->contains($l)) {
                $this->siteGameKultGameReleasesScheduledForDeletion->remove($this->siteGameKultGameReleasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteGameKultGameRelease $siteGameKultGameRelease The ChildSiteGameKultGameRelease object to add.
     */
    protected function doAddSiteGameKultGameRelease(ChildSiteGameKultGameRelease $siteGameKultGameRelease)
    {
        $this->collSiteGameKultGameReleases[]= $siteGameKultGameRelease;
        $siteGameKultGameRelease->setSiteGameKultGame($this);
    }

    /**
     * @param  ChildSiteGameKultGameRelease $siteGameKultGameRelease The ChildSiteGameKultGameRelease object to remove.
     * @return $this|ChildSiteGameKultGame The current object (for fluent API support)
     */
    public function removeSiteGameKultGameRelease(ChildSiteGameKultGameRelease $siteGameKultGameRelease)
    {
        if ($this->getSiteGameKultGameReleases()->contains($siteGameKultGameRelease)) {
            $pos = $this->collSiteGameKultGameReleases->search($siteGameKultGameRelease);
            $this->collSiteGameKultGameReleases->remove($pos);
            if (null === $this->siteGameKultGameReleasesScheduledForDeletion) {
                $this->siteGameKultGameReleasesScheduledForDeletion = clone $this->collSiteGameKultGameReleases;
                $this->siteGameKultGameReleasesScheduledForDeletion->clear();
            }
            $this->siteGameKultGameReleasesScheduledForDeletion[]= clone $siteGameKultGameRelease;
            $siteGameKultGameRelease->setSiteGameKultGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SiteGameKultGame is new, it will return
     * an empty collection; or if this SiteGameKultGame has previously
     * been saved, it will retrieve related SiteGameKultGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SiteGameKultGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteGameKultGameRelease[] List of ChildSiteGameKultGameRelease objects
     */
    public function getSiteGameKultGameReleasesJoinSiteGameKultPlatform(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteGameKultGameReleaseQuery::create(null, $criteria);
        $query->joinWith('SiteGameKultPlatform', $joinBehavior);

        return $this->getSiteGameKultGameReleases($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aGame) {
            $this->aGame->removeSiteGameKultGame($this);
        }
        $this->vgakga_id = null;
        $this->vgakga_type_id = null;
        $this->vgakga_gamekult_id = null;
        $this->vgakga_vgagam_id = null;
        $this->vgakga_name = null;
        $this->vgakga_aliases = null;
        $this->vgakga_aliases_unserialized = null;
        $this->vgakga_original_price = null;
        $this->vgakga_platforms_array = null;
        $this->vgakga_platforms_array_unserialized = null;
        $this->vgakga_genres_array = null;
        $this->vgakga_genres_array_unserialized = null;
        $this->vgakga_themes_array = null;
        $this->vgakga_themes_array_unserialized = null;
        $this->vgakga_companies_array = null;
        $this->vgakga_companies_array_unserialized = null;
        $this->vgakga_franchises_array = null;
        $this->vgakga_franchises_array_unserialized = null;
        $this->vgakga_fr_released_at = null;
        $this->vgakga_us_released_at = null;
        $this->vgakga_jp_released_at = null;
        $this->vgakga_image_small_url = null;
        $this->vgakga_site_detail_url = null;
        $this->vgakga_refreshed_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSiteGameKultGameReleases) {
                foreach ($this->collSiteGameKultGameReleases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSiteGameKultGameReleases = null;
        $this->aGame = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SiteGameKultGameTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
