<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept as ChildApiGiantBombConcept;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery as ChildApiGiantBombConceptQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombConceptTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_concept_vgagcc' table.
 *
 *
 *
 * @method     ChildApiGiantBombConceptQuery orderById($order = Criteria::ASC) Order by the vgagcc_id column
 * @method     ChildApiGiantBombConceptQuery orderByName($order = Criteria::ASC) Order by the vgagcc_name column
 * @method     ChildApiGiantBombConceptQuery orderByAliases($order = Criteria::ASC) Order by the vgagcc_aliases column
 * @method     ChildApiGiantBombConceptQuery orderBySummary($order = Criteria::ASC) Order by the vgagcc_summary column
 * @method     ChildApiGiantBombConceptQuery orderByDescription($order = Criteria::ASC) Order by the vgagcc_description column
 * @method     ChildApiGiantBombConceptQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagcc_api_detail_url column
 * @method     ChildApiGiantBombConceptQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagcc_site_detail_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagcc_image_icon_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagcc_image_medium_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagcc_image_screen_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagcc_image_small_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagcc_image_super_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagcc_image_thumb_url column
 * @method     ChildApiGiantBombConceptQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagcc_image_tiny_url column
 * @method     ChildApiGiantBombConceptQuery orderByGiantBombFirstGameId($order = Criteria::ASC) Order by the vgagcc_first_vgagga_id column
 * @method     ChildApiGiantBombConceptQuery orderByGiantBombFirstFranchiseId($order = Criteria::ASC) Order by the vgagcc_first_vgagfr_id column
 * @method     ChildApiGiantBombConceptQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagcc_created_at column
 * @method     ChildApiGiantBombConceptQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagcc_updated_at column
 *
 * @method     ChildApiGiantBombConceptQuery groupById() Group by the vgagcc_id column
 * @method     ChildApiGiantBombConceptQuery groupByName() Group by the vgagcc_name column
 * @method     ChildApiGiantBombConceptQuery groupByAliases() Group by the vgagcc_aliases column
 * @method     ChildApiGiantBombConceptQuery groupBySummary() Group by the vgagcc_summary column
 * @method     ChildApiGiantBombConceptQuery groupByDescription() Group by the vgagcc_description column
 * @method     ChildApiGiantBombConceptQuery groupByApiDetailUrl() Group by the vgagcc_api_detail_url column
 * @method     ChildApiGiantBombConceptQuery groupBySiteDetailUrl() Group by the vgagcc_site_detail_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageIconUrl() Group by the vgagcc_image_icon_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageMediumUrl() Group by the vgagcc_image_medium_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageScreenUrl() Group by the vgagcc_image_screen_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageSmallUrl() Group by the vgagcc_image_small_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageSuperUrl() Group by the vgagcc_image_super_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageThumbUrl() Group by the vgagcc_image_thumb_url column
 * @method     ChildApiGiantBombConceptQuery groupByImageTinyUrl() Group by the vgagcc_image_tiny_url column
 * @method     ChildApiGiantBombConceptQuery groupByGiantBombFirstGameId() Group by the vgagcc_first_vgagga_id column
 * @method     ChildApiGiantBombConceptQuery groupByGiantBombFirstFranchiseId() Group by the vgagcc_first_vgagfr_id column
 * @method     ChildApiGiantBombConceptQuery groupByCreatedAt() Group by the vgagcc_created_at column
 * @method     ChildApiGiantBombConceptQuery groupByUpdatedAt() Group by the vgagcc_updated_at column
 *
 * @method     ChildApiGiantBombConceptQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombConceptQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombConceptQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombConceptQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombConceptQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombConceptQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombConceptQuery leftJoinApiGiantBombFirstGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombConceptQuery rightJoinApiGiantBombFirstGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombConceptQuery innerJoinApiGiantBombFirstGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombConceptQuery joinWithApiGiantBombFirstGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombConceptQuery leftJoinWithApiGiantBombFirstGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombConceptQuery rightJoinWithApiGiantBombFirstGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombConceptQuery innerJoinWithApiGiantBombFirstGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombConceptQuery leftJoinApiGiantBombFirstFranchise($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstFranchise relation
 * @method     ChildApiGiantBombConceptQuery rightJoinApiGiantBombFirstFranchise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstFranchise relation
 * @method     ChildApiGiantBombConceptQuery innerJoinApiGiantBombFirstFranchise($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstFranchise relation
 *
 * @method     ChildApiGiantBombConceptQuery joinWithApiGiantBombFirstFranchise($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstFranchise relation
 *
 * @method     ChildApiGiantBombConceptQuery leftJoinWithApiGiantBombFirstFranchise() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstFranchise relation
 * @method     ChildApiGiantBombConceptQuery rightJoinWithApiGiantBombFirstFranchise() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstFranchise relation
 * @method     ChildApiGiantBombConceptQuery innerJoinWithApiGiantBombFirstFranchise() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstFranchise relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombConcept findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombConcept matching the query
 * @method     ChildApiGiantBombConcept findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombConcept matching the query, or a new ChildApiGiantBombConcept object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombConcept findOneById(int $vgagcc_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_id column
 * @method     ChildApiGiantBombConcept findOneByName(string $vgagcc_name) Return the first ChildApiGiantBombConcept filtered by the vgagcc_name column
 * @method     ChildApiGiantBombConcept findOneByAliases(array $vgagcc_aliases) Return the first ChildApiGiantBombConcept filtered by the vgagcc_aliases column
 * @method     ChildApiGiantBombConcept findOneBySummary(string $vgagcc_summary) Return the first ChildApiGiantBombConcept filtered by the vgagcc_summary column
 * @method     ChildApiGiantBombConcept findOneByDescription(string $vgagcc_description) Return the first ChildApiGiantBombConcept filtered by the vgagcc_description column
 * @method     ChildApiGiantBombConcept findOneByApiDetailUrl(string $vgagcc_api_detail_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_api_detail_url column
 * @method     ChildApiGiantBombConcept findOneBySiteDetailUrl(string $vgagcc_site_detail_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_site_detail_url column
 * @method     ChildApiGiantBombConcept findOneByImageIconUrl(string $vgagcc_image_icon_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_icon_url column
 * @method     ChildApiGiantBombConcept findOneByImageMediumUrl(string $vgagcc_image_medium_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_medium_url column
 * @method     ChildApiGiantBombConcept findOneByImageScreenUrl(string $vgagcc_image_screen_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_screen_url column
 * @method     ChildApiGiantBombConcept findOneByImageSmallUrl(string $vgagcc_image_small_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_small_url column
 * @method     ChildApiGiantBombConcept findOneByImageSuperUrl(string $vgagcc_image_super_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_super_url column
 * @method     ChildApiGiantBombConcept findOneByImageThumbUrl(string $vgagcc_image_thumb_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_thumb_url column
 * @method     ChildApiGiantBombConcept findOneByImageTinyUrl(string $vgagcc_image_tiny_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_tiny_url column
 * @method     ChildApiGiantBombConcept findOneByGiantBombFirstGameId(int $vgagcc_first_vgagga_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_first_vgagga_id column
 * @method     ChildApiGiantBombConcept findOneByGiantBombFirstFranchiseId(int $vgagcc_first_vgagfr_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_first_vgagfr_id column
 * @method     ChildApiGiantBombConcept findOneByCreatedAt(string $vgagcc_created_at) Return the first ChildApiGiantBombConcept filtered by the vgagcc_created_at column
 * @method     ChildApiGiantBombConcept findOneByUpdatedAt(string $vgagcc_updated_at) Return the first ChildApiGiantBombConcept filtered by the vgagcc_updated_at column *

 * @method     ChildApiGiantBombConcept requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombConcept by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombConcept matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombConcept requireOneById(int $vgagcc_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByName(string $vgagcc_name) Return the first ChildApiGiantBombConcept filtered by the vgagcc_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByAliases(array $vgagcc_aliases) Return the first ChildApiGiantBombConcept filtered by the vgagcc_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneBySummary(string $vgagcc_summary) Return the first ChildApiGiantBombConcept filtered by the vgagcc_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByDescription(string $vgagcc_description) Return the first ChildApiGiantBombConcept filtered by the vgagcc_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByApiDetailUrl(string $vgagcc_api_detail_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneBySiteDetailUrl(string $vgagcc_site_detail_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageIconUrl(string $vgagcc_image_icon_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageMediumUrl(string $vgagcc_image_medium_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageScreenUrl(string $vgagcc_image_screen_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageSmallUrl(string $vgagcc_image_small_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageSuperUrl(string $vgagcc_image_super_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageThumbUrl(string $vgagcc_image_thumb_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByImageTinyUrl(string $vgagcc_image_tiny_url) Return the first ChildApiGiantBombConcept filtered by the vgagcc_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByGiantBombFirstGameId(int $vgagcc_first_vgagga_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_first_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByGiantBombFirstFranchiseId(int $vgagcc_first_vgagfr_id) Return the first ChildApiGiantBombConcept filtered by the vgagcc_first_vgagfr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByCreatedAt(string $vgagcc_created_at) Return the first ChildApiGiantBombConcept filtered by the vgagcc_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombConcept requireOneByUpdatedAt(string $vgagcc_updated_at) Return the first ChildApiGiantBombConcept filtered by the vgagcc_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombConcept[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombConcept objects based on current ModelCriteria
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findById(int $vgagcc_id) Return ChildApiGiantBombConcept objects filtered by the vgagcc_id column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByName(string $vgagcc_name) Return ChildApiGiantBombConcept objects filtered by the vgagcc_name column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByAliases(array $vgagcc_aliases) Return ChildApiGiantBombConcept objects filtered by the vgagcc_aliases column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findBySummary(string $vgagcc_summary) Return ChildApiGiantBombConcept objects filtered by the vgagcc_summary column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByDescription(string $vgagcc_description) Return ChildApiGiantBombConcept objects filtered by the vgagcc_description column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByApiDetailUrl(string $vgagcc_api_detail_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_api_detail_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findBySiteDetailUrl(string $vgagcc_site_detail_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_site_detail_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageIconUrl(string $vgagcc_image_icon_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_icon_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageMediumUrl(string $vgagcc_image_medium_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_medium_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageScreenUrl(string $vgagcc_image_screen_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_screen_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageSmallUrl(string $vgagcc_image_small_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_small_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageSuperUrl(string $vgagcc_image_super_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_super_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageThumbUrl(string $vgagcc_image_thumb_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_thumb_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByImageTinyUrl(string $vgagcc_image_tiny_url) Return ChildApiGiantBombConcept objects filtered by the vgagcc_image_tiny_url column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByGiantBombFirstGameId(int $vgagcc_first_vgagga_id) Return ChildApiGiantBombConcept objects filtered by the vgagcc_first_vgagga_id column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByGiantBombFirstFranchiseId(int $vgagcc_first_vgagfr_id) Return ChildApiGiantBombConcept objects filtered by the vgagcc_first_vgagfr_id column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByCreatedAt(string $vgagcc_created_at) Return ChildApiGiantBombConcept objects filtered by the vgagcc_created_at column
 * @method     ChildApiGiantBombConcept[]|ObjectCollection findByUpdatedAt(string $vgagcc_updated_at) Return ChildApiGiantBombConcept objects filtered by the vgagcc_updated_at column
 * @method     ChildApiGiantBombConcept[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombConceptQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombConceptQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombConcept', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombConceptQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombConceptQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombConceptQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombConceptQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombConcept|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombConceptTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombConcept A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagcc_id, vgagcc_name, vgagcc_aliases, vgagcc_summary, vgagcc_description, vgagcc_api_detail_url, vgagcc_site_detail_url, vgagcc_image_icon_url, vgagcc_image_medium_url, vgagcc_image_screen_url, vgagcc_image_small_url, vgagcc_image_super_url, vgagcc_image_thumb_url, vgagcc_image_tiny_url, vgagcc_first_vgagga_id, vgagcc_first_vgagfr_id, vgagcc_created_at, vgagcc_updated_at FROM videogames_api_giantbomb_concept_vgagcc WHERE vgagcc_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombConcept $obj */
            $obj = new ChildApiGiantBombConcept();
            $obj->hydrate($row);
            ApiGiantBombConceptTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombConcept|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagcc_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagcc_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagcc_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagcc_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagcc_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagcc_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagcc_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagcc_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagcc_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagcc_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagcc_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagcc_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagcc_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagcc_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagcc_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagcc_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagcc_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagcc_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagcc_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagcc_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagcc_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagcc_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagcc_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagcc_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagcc_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagcc_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagcc_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagcc_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagcc_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagcc_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagcc_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagcc_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagcc_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagcc_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagcc_first_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombFirstGameId(1234); // WHERE vgagcc_first_vgagga_id = 1234
     * $query->filterByGiantBombFirstGameId(array(12, 34)); // WHERE vgagcc_first_vgagga_id IN (12, 34)
     * $query->filterByGiantBombFirstGameId(array('min' => 12)); // WHERE vgagcc_first_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombFirstGame()
     *
     * @param     mixed $giantBombFirstGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByGiantBombFirstGameId($giantBombFirstGameId = null, $comparison = null)
    {
        if (is_array($giantBombFirstGameId)) {
            $useMinMax = false;
            if (isset($giantBombFirstGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $giantBombFirstGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombFirstGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $giantBombFirstGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $giantBombFirstGameId, $comparison);
    }

    /**
     * Filter the query on the vgagcc_first_vgagfr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombFirstFranchiseId(1234); // WHERE vgagcc_first_vgagfr_id = 1234
     * $query->filterByGiantBombFirstFranchiseId(array(12, 34)); // WHERE vgagcc_first_vgagfr_id IN (12, 34)
     * $query->filterByGiantBombFirstFranchiseId(array('min' => 12)); // WHERE vgagcc_first_vgagfr_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombFirstFranchise()
     *
     * @param     mixed $giantBombFirstFranchiseId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByGiantBombFirstFranchiseId($giantBombFirstFranchiseId = null, $comparison = null)
    {
        if (is_array($giantBombFirstFranchiseId)) {
            $useMinMax = false;
            if (isset($giantBombFirstFranchiseId['min'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $giantBombFirstFranchiseId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombFirstFranchiseId['max'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $giantBombFirstFranchiseId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $giantBombFirstFranchiseId, $comparison);
    }

    /**
     * Filter the query on the vgagcc_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagcc_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagcc_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagcc_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagcc_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagcc_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagcc_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagcc_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombFirstGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise|ObjectCollection $apiGiantBombFranchise The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstFranchise($apiGiantBombFranchise, $comparison = null)
    {
        if ($apiGiantBombFranchise instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise) {
            return $this
                ->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $apiGiantBombFranchise->getId(), $comparison);
        } elseif ($apiGiantBombFranchise instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $apiGiantBombFranchise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombFirstFranchise() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstFranchise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstFranchise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstFranchise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstFranchise');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstFranchise relation ApiGiantBombFranchise object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstFranchiseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstFranchise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstFranchise', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombConcept $apiGiantBombConcept Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombConceptQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombConcept = null)
    {
        if ($apiGiantBombConcept) {
            $this->addUsingAlias(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $apiGiantBombConcept->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_concept_vgagcc table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombConceptTableMap::clearInstancePool();
            ApiGiantBombConceptTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombConceptTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombConceptTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombConceptTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombConceptQuery
