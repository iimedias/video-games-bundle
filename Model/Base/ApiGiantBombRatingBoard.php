<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating as ChildApiGiantBombGameRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery as ChildApiGiantBombGameRatingQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard as ChildApiGiantBombRatingBoard;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery as ChildApiGiantBombRatingBoardQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameRatingTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombRatingBoardTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_rating_board_vgagrb' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombRatingBoard implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombRatingBoardTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagrb_id field.
     *
     * @var        int
     */
    protected $vgagrb_id;

    /**
     * The value for the vgagrb_name field.
     *
     * @var        string
     */
    protected $vgagrb_name;

    /**
     * The value for the vgagrb_summary field.
     *
     * @var        string
     */
    protected $vgagrb_summary;

    /**
     * The value for the vgagrb_description field.
     *
     * @var        string
     */
    protected $vgagrb_description;

    /**
     * The value for the vgagrb_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagrb_api_detail_url;

    /**
     * The value for the vgagrb_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagrb_site_detail_url;

    /**
     * The value for the vgagrb_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_icon_url;

    /**
     * The value for the vgagrb_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_medium_url;

    /**
     * The value for the vgagrb_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_screen_url;

    /**
     * The value for the vgagrb_image_small_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_small_url;

    /**
     * The value for the vgagrb_image_super_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_super_url;

    /**
     * The value for the vgagrb_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_thumb_url;

    /**
     * The value for the vgagrb_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagrb_image_tiny_url;

    /**
     * The value for the vgagrb_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagrb_created_at;

    /**
     * The value for the vgagrb_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagrb_updated_at;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameRating[] Collection to store aggregation of ChildApiGiantBombGameRating objects.
     */
    protected $collApiGiantBombRatingGames;
    protected $collApiGiantBombRatingGamesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameRating[]
     */
    protected $apiGiantBombRatingGamesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombRatingBoard object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombRatingBoard</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombRatingBoard</code>, delegates to
     * <code>equals(ApiGiantBombRatingBoard)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombRatingBoard The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagrb_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagrb_id;
    }

    /**
     * Get the [vgagrb_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagrb_name;
    }

    /**
     * Get the [vgagrb_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagrb_summary;
    }

    /**
     * Get the [vgagrb_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagrb_description;
    }

    /**
     * Get the [vgagrb_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagrb_api_detail_url;
    }

    /**
     * Get the [vgagrb_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagrb_site_detail_url;
    }

    /**
     * Get the [vgagrb_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagrb_image_icon_url;
    }

    /**
     * Get the [vgagrb_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagrb_image_medium_url;
    }

    /**
     * Get the [vgagrb_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagrb_image_screen_url;
    }

    /**
     * Get the [vgagrb_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagrb_image_small_url;
    }

    /**
     * Get the [vgagrb_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagrb_image_super_url;
    }

    /**
     * Get the [vgagrb_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagrb_image_thumb_url;
    }

    /**
     * Get the [vgagrb_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagrb_image_tiny_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgagrb_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrb_created_at;
        } else {
            return $this->vgagrb_created_at instanceof \DateTimeInterface ? $this->vgagrb_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrb_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrb_updated_at;
        } else {
            return $this->vgagrb_updated_at instanceof \DateTimeInterface ? $this->vgagrb_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagrb_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrb_id !== $v) {
            $this->vgagrb_id = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagrb_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_name !== $v) {
            $this->vgagrb_name = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagrb_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_summary !== $v) {
            $this->vgagrb_summary = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagrb_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_description !== $v) {
            $this->vgagrb_description = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagrb_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_api_detail_url !== $v) {
            $this->vgagrb_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagrb_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_site_detail_url !== $v) {
            $this->vgagrb_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagrb_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_icon_url !== $v) {
            $this->vgagrb_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagrb_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_medium_url !== $v) {
            $this->vgagrb_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagrb_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_screen_url !== $v) {
            $this->vgagrb_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagrb_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_small_url !== $v) {
            $this->vgagrb_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagrb_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_super_url !== $v) {
            $this->vgagrb_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagrb_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_thumb_url !== $v) {
            $this->vgagrb_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagrb_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrb_image_tiny_url !== $v) {
            $this->vgagrb_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Sets the value of [vgagrb_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrb_created_at !== null || $dt !== null) {
            if ($this->vgagrb_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrb_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrb_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagrb_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrb_updated_at !== null || $dt !== null) {
            if ($this->vgagrb_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrb_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrb_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrb_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrb_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombRatingBoardTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrb_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = ApiGiantBombRatingBoardTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRatingBoard'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombRatingBoardQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collApiGiantBombRatingGames = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombRatingBoard::setDeleted()
     * @see ApiGiantBombRatingBoard::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombRatingBoardQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombRatingBoardTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombRatingGamesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombRatingGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombRatingGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombRatingGamesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombRatingGames !== null) {
                foreach ($this->collApiGiantBombRatingGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_id';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_name';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_summary';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_description';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrb_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_rating_board_vgagrb (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagrb_id':
                        $stmt->bindValue($identifier, $this->vgagrb_id, PDO::PARAM_INT);
                        break;
                    case 'vgagrb_name':
                        $stmt->bindValue($identifier, $this->vgagrb_name, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_summary':
                        $stmt->bindValue($identifier, $this->vgagrb_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_description':
                        $stmt->bindValue($identifier, $this->vgagrb_description, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagrb_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagrb_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagrb_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_created_at':
                        $stmt->bindValue($identifier, $this->vgagrb_created_at ? $this->vgagrb_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrb_updated_at':
                        $stmt->bindValue($identifier, $this->vgagrb_updated_at ? $this->vgagrb_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombRatingBoardTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getSummary();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getApiDetailUrl();
                break;
            case 5:
                return $this->getSiteDetailUrl();
                break;
            case 6:
                return $this->getImageIconUrl();
                break;
            case 7:
                return $this->getImageMediumUrl();
                break;
            case 8:
                return $this->getImageScreenUrl();
                break;
            case 9:
                return $this->getImageSmallUrl();
                break;
            case 10:
                return $this->getImageSuperUrl();
                break;
            case 11:
                return $this->getImageThumbUrl();
                break;
            case 12:
                return $this->getImageTinyUrl();
                break;
            case 13:
                return $this->getCreatedAt();
                break;
            case 14:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombRatingBoard'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombRatingBoard'][$this->hashCode()] = true;
        $keys = ApiGiantBombRatingBoardTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getSummary(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getApiDetailUrl(),
            $keys[5] => $this->getSiteDetailUrl(),
            $keys[6] => $this->getImageIconUrl(),
            $keys[7] => $this->getImageMediumUrl(),
            $keys[8] => $this->getImageScreenUrl(),
            $keys[9] => $this->getImageSmallUrl(),
            $keys[10] => $this->getImageSuperUrl(),
            $keys[11] => $this->getImageThumbUrl(),
            $keys[12] => $this->getImageTinyUrl(),
            $keys[13] => $this->getCreatedAt(),
            $keys[14] => $this->getUpdatedAt(),
        );
        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collApiGiantBombRatingGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameRatings';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_rating_vgaggrs';
                        break;
                    default:
                        $key = 'ApiGiantBombRatingGames';
                }

                $result[$key] = $this->collApiGiantBombRatingGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombRatingBoardTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setSummary($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setApiDetailUrl($value);
                break;
            case 5:
                $this->setSiteDetailUrl($value);
                break;
            case 6:
                $this->setImageIconUrl($value);
                break;
            case 7:
                $this->setImageMediumUrl($value);
                break;
            case 8:
                $this->setImageScreenUrl($value);
                break;
            case 9:
                $this->setImageSmallUrl($value);
                break;
            case 10:
                $this->setImageSuperUrl($value);
                break;
            case 11:
                $this->setImageThumbUrl($value);
                break;
            case 12:
                $this->setImageTinyUrl($value);
                break;
            case 13:
                $this->setCreatedAt($value);
                break;
            case 14:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombRatingBoardTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSummary($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setApiDetailUrl($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setImageIconUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImageMediumUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImageScreenUrl($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImageSmallUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setImageSuperUrl($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setImageThumbUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageTinyUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCreatedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUpdatedAt($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $this->vgagrb_id);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME, $this->vgagrb_name);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY, $this->vgagrb_summary);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION, $this->vgagrb_description);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL, $this->vgagrb_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL, $this->vgagrb_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL, $this->vgagrb_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL, $this->vgagrb_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL, $this->vgagrb_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL, $this->vgagrb_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL, $this->vgagrb_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL, $this->vgagrb_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL, $this->vgagrb_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT, $this->vgagrb_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT)) {
            $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT, $this->vgagrb_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombRatingBoardQuery::create();
        $criteria->add(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $this->vgagrb_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagrb_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombRatingGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombRatingGame($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombRatingGame' == $relationName) {
            return $this->initApiGiantBombRatingGames();
        }
    }

    /**
     * Clears out the collApiGiantBombRatingGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombRatingGames()
     */
    public function clearApiGiantBombRatingGames()
    {
        $this->collApiGiantBombRatingGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombRatingGames collection loaded partially.
     */
    public function resetPartialApiGiantBombRatingGames($v = true)
    {
        $this->collApiGiantBombRatingGamesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombRatingGames collection.
     *
     * By default this just sets the collApiGiantBombRatingGames collection to an empty array (like clearcollApiGiantBombRatingGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombRatingGames($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombRatingGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameRatingTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombRatingGames = new $collectionClassName;
        $this->collApiGiantBombRatingGames->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating');
    }

    /**
     * Gets an array of ChildApiGiantBombGameRating objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombRatingBoard is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameRating[] List of ChildApiGiantBombGameRating objects
     * @throws PropelException
     */
    public function getApiGiantBombRatingGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombRatingGamesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombRatingGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombRatingGames) {
                // return empty collection
                $this->initApiGiantBombRatingGames();
            } else {
                $collApiGiantBombRatingGames = ChildApiGiantBombGameRatingQuery::create(null, $criteria)
                    ->filterByApiGiantBombRatingBoard($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombRatingGamesPartial && count($collApiGiantBombRatingGames)) {
                        $this->initApiGiantBombRatingGames(false);

                        foreach ($collApiGiantBombRatingGames as $obj) {
                            if (false == $this->collApiGiantBombRatingGames->contains($obj)) {
                                $this->collApiGiantBombRatingGames->append($obj);
                            }
                        }

                        $this->collApiGiantBombRatingGamesPartial = true;
                    }

                    return $collApiGiantBombRatingGames;
                }

                if ($partial && $this->collApiGiantBombRatingGames) {
                    foreach ($this->collApiGiantBombRatingGames as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombRatingGames[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombRatingGames = $collApiGiantBombRatingGames;
                $this->collApiGiantBombRatingGamesPartial = false;
            }
        }

        return $this->collApiGiantBombRatingGames;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameRating objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombRatingGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function setApiGiantBombRatingGames(Collection $apiGiantBombRatingGames, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameRating[] $apiGiantBombRatingGamesToDelete */
        $apiGiantBombRatingGamesToDelete = $this->getApiGiantBombRatingGames(new Criteria(), $con)->diff($apiGiantBombRatingGames);


        $this->apiGiantBombRatingGamesScheduledForDeletion = $apiGiantBombRatingGamesToDelete;

        foreach ($apiGiantBombRatingGamesToDelete as $apiGiantBombRatingGameRemoved) {
            $apiGiantBombRatingGameRemoved->setApiGiantBombRatingBoard(null);
        }

        $this->collApiGiantBombRatingGames = null;
        foreach ($apiGiantBombRatingGames as $apiGiantBombRatingGame) {
            $this->addApiGiantBombRatingGame($apiGiantBombRatingGame);
        }

        $this->collApiGiantBombRatingGames = $apiGiantBombRatingGames;
        $this->collApiGiantBombRatingGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameRating objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameRating objects.
     * @throws PropelException
     */
    public function countApiGiantBombRatingGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombRatingGamesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombRatingGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombRatingGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombRatingGames());
            }

            $query = ChildApiGiantBombGameRatingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombRatingBoard($this)
                ->count($con);
        }

        return count($this->collApiGiantBombRatingGames);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameRating object to this object
     * through the ChildApiGiantBombGameRating foreign key attribute.
     *
     * @param  ChildApiGiantBombGameRating $l ChildApiGiantBombGameRating
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function addApiGiantBombRatingGame(ChildApiGiantBombGameRating $l)
    {
        if ($this->collApiGiantBombRatingGames === null) {
            $this->initApiGiantBombRatingGames();
            $this->collApiGiantBombRatingGamesPartial = true;
        }

        if (!$this->collApiGiantBombRatingGames->contains($l)) {
            $this->doAddApiGiantBombRatingGame($l);

            if ($this->apiGiantBombRatingGamesScheduledForDeletion and $this->apiGiantBombRatingGamesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombRatingGamesScheduledForDeletion->remove($this->apiGiantBombRatingGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameRating $apiGiantBombRatingGame The ChildApiGiantBombGameRating object to add.
     */
    protected function doAddApiGiantBombRatingGame(ChildApiGiantBombGameRating $apiGiantBombRatingGame)
    {
        $this->collApiGiantBombRatingGames[]= $apiGiantBombRatingGame;
        $apiGiantBombRatingGame->setApiGiantBombRatingBoard($this);
    }

    /**
     * @param  ChildApiGiantBombGameRating $apiGiantBombRatingGame The ChildApiGiantBombGameRating object to remove.
     * @return $this|ChildApiGiantBombRatingBoard The current object (for fluent API support)
     */
    public function removeApiGiantBombRatingGame(ChildApiGiantBombGameRating $apiGiantBombRatingGame)
    {
        if ($this->getApiGiantBombRatingGames()->contains($apiGiantBombRatingGame)) {
            $pos = $this->collApiGiantBombRatingGames->search($apiGiantBombRatingGame);
            $this->collApiGiantBombRatingGames->remove($pos);
            if (null === $this->apiGiantBombRatingGamesScheduledForDeletion) {
                $this->apiGiantBombRatingGamesScheduledForDeletion = clone $this->collApiGiantBombRatingGames;
                $this->apiGiantBombRatingGamesScheduledForDeletion->clear();
            }
            $this->apiGiantBombRatingGamesScheduledForDeletion[]= $apiGiantBombRatingGame;
            $apiGiantBombRatingGame->setApiGiantBombRatingBoard(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->vgagrb_id = null;
        $this->vgagrb_name = null;
        $this->vgagrb_summary = null;
        $this->vgagrb_description = null;
        $this->vgagrb_api_detail_url = null;
        $this->vgagrb_site_detail_url = null;
        $this->vgagrb_image_icon_url = null;
        $this->vgagrb_image_medium_url = null;
        $this->vgagrb_image_screen_url = null;
        $this->vgagrb_image_small_url = null;
        $this->vgagrb_image_super_url = null;
        $this->vgagrb_image_thumb_url = null;
        $this->vgagrb_image_tiny_url = null;
        $this->vgagrb_created_at = null;
        $this->vgagrb_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombRatingGames) {
                foreach ($this->collApiGiantBombRatingGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombRatingGames = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombRatingBoardTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
