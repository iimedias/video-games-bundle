<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating as ChildApiGiantBombGameRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery as ChildApiGiantBombGameRatingQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper as ChildApiGiantBombGameReleaseDeveloper;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery as ChildApiGiantBombGameReleaseDeveloperQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher as ChildApiGiantBombGameReleasePublisher;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery as ChildApiGiantBombGameReleasePublisherQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImage as ChildApiGiantBombImage;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery as ChildApiGiantBombImageQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform as ChildApiGiantBombPlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery as ChildApiGiantBombPlatformQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion as ChildApiGiantBombRegion;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery as ChildApiGiantBombRegionQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo as ChildApiGiantBombVideo;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery as ChildApiGiantBombVideoQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseDeveloperTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleasePublisherTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombImageTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombVideoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_release_vgagrl' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombGameRelease implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombGameReleaseTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagrl_id field.
     *
     * @var        int
     */
    protected $vgagrl_id;

    /**
     * The value for the vgagrl_vgagga_id field.
     *
     * @var        int
     */
    protected $vgagrl_vgagga_id;

    /**
     * The value for the vgagrl_vgagpl_id field.
     *
     * @var        int
     */
    protected $vgagrl_vgagpl_id;

    /**
     * The value for the vgagrl_vgaggr_id field.
     *
     * @var        int
     */
    protected $vgagrl_vgaggr_id;

    /**
     * The value for the vgagrl_vgagre_id field.
     *
     * @var        int
     */
    protected $vgagrl_vgagre_id;

    /**
     * The value for the vgaggl_name field.
     *
     * @var        string
     */
    protected $vgaggl_name;

    /**
     * The value for the vgagrl_summary field.
     *
     * @var        string
     */
    protected $vgagrl_summary;

    /**
     * The value for the vgagrl_description field.
     *
     * @var        string
     */
    protected $vgagrl_description;

    /**
     * The value for the vgagrl_minimum_players field.
     *
     * @var        int
     */
    protected $vgagrl_minimum_players;

    /**
     * The value for the vgagrl_maximum_players field.
     *
     * @var        int
     */
    protected $vgagrl_maximum_players;

    /**
     * The value for the vgagrl_widescreen_support field.
     *
     * @var        boolean
     */
    protected $vgagrl_widescreen_support;

    /**
     * The value for the vgagrl_product_code_value field.
     *
     * @var        string
     */
    protected $vgagrl_product_code_value;

    /**
     * The value for the vgagrl_original_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_original_released_at;

    /**
     * The value for the vgagrl_expected_day_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_expected_day_released_at;

    /**
     * The value for the vgagrl_expected_month_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_expected_month_released_at;

    /**
     * The value for the vgagrl_expected_quarter_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_expected_quarter_released_at;

    /**
     * The value for the vgagrl_expected_year_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_expected_year_released_at;

    /**
     * The value for the vgagrl_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_icon_url;

    /**
     * The value for the vgagrl_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_medium_url;

    /**
     * The value for the vgagrl_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_screen_url;

    /**
     * The value for the vgagrl_image_small_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_small_url;

    /**
     * The value for the vgagrl_image_super_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_super_url;

    /**
     * The value for the vgagrl_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_thumb_url;

    /**
     * The value for the vgagrl_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagrl_image_tiny_url;

    /**
     * The value for the vgagrl_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagrl_api_detail_url;

    /**
     * The value for the vgagrl_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagrl_site_detail_url;

    /**
     * The value for the vgagrl_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_created_at;

    /**
     * The value for the vgagrl_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagrl_updated_at;

    /**
     * @var        ChildApiGiantBombGame
     */
    protected $aApiGiantBombGame;

    /**
     * @var        ChildApiGiantBombPlatform
     */
    protected $aApiGiantBombPlatform;

    /**
     * @var        ChildApiGiantBombGameRating
     */
    protected $aApiGiantBombGameRating;

    /**
     * @var        ChildApiGiantBombRegion
     */
    protected $aApiGiantBombRegion;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] Collection to store aggregation of ChildApiGiantBombGameReleaseDeveloper objects.
     */
    protected $collApiGiantBombGameReleaseDevelopers;
    protected $collApiGiantBombGameReleaseDevelopersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameReleasePublisher[] Collection to store aggregation of ChildApiGiantBombGameReleasePublisher objects.
     */
    protected $collApiGiantBombGameReleasePublishers;
    protected $collApiGiantBombGameReleasePublishersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombImage[] Collection to store aggregation of ChildApiGiantBombImage objects.
     */
    protected $collApiGiantBombImages;
    protected $collApiGiantBombImagesPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombVideo[] Collection to store aggregation of ChildApiGiantBombVideo objects.
     */
    protected $collApiGiantBombVideos;
    protected $collApiGiantBombVideosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[]
     */
    protected $apiGiantBombGameReleaseDevelopersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameReleasePublisher[]
     */
    protected $apiGiantBombGameReleasePublishersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombImage[]
     */
    protected $apiGiantBombImagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombVideo[]
     */
    protected $apiGiantBombVideosScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameRelease object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombGameRelease</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombGameRelease</code>, delegates to
     * <code>equals(ApiGiantBombGameRelease)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombGameRelease The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagrl_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagrl_id;
    }

    /**
     * Get the [vgagrl_vgagga_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombGameId()
    {
        return $this->vgagrl_vgagga_id;
    }

    /**
     * Get the [vgagrl_vgagpl_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombPlatformId()
    {
        return $this->vgagrl_vgagpl_id;
    }

    /**
     * Get the [vgagrl_vgaggr_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombGameRatingId()
    {
        return $this->vgagrl_vgaggr_id;
    }

    /**
     * Get the [vgagrl_vgagre_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombRegionId()
    {
        return $this->vgagrl_vgagre_id;
    }

    /**
     * Get the [vgaggl_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgaggl_name;
    }

    /**
     * Get the [vgagrl_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagrl_summary;
    }

    /**
     * Get the [vgagrl_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagrl_description;
    }

    /**
     * Get the [vgagrl_minimum_players] column value.
     *
     * @return int
     */
    public function getMinimumPlayers()
    {
        return $this->vgagrl_minimum_players;
    }

    /**
     * Get the [vgagrl_maximum_players] column value.
     *
     * @return int
     */
    public function getMaximumPlayers()
    {
        return $this->vgagrl_maximum_players;
    }

    /**
     * Get the [vgagrl_widescreen_support] column value.
     *
     * @return boolean
     */
    public function getWidescreenSupport()
    {
        return $this->vgagrl_widescreen_support;
    }

    /**
     * Get the [vgagrl_widescreen_support] column value.
     *
     * @return boolean
     */
    public function isWidescreenSupport()
    {
        return $this->getWidescreenSupport();
    }

    /**
     * Get the [vgagrl_product_code_value] column value.
     *
     * @return string
     */
    public function getProductCodeValue()
    {
        return $this->vgagrl_product_code_value;
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_original_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getOriginalReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_original_released_at;
        } else {
            return $this->vgagrl_original_released_at instanceof \DateTimeInterface ? $this->vgagrl_original_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_expected_day_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedDayReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_expected_day_released_at;
        } else {
            return $this->vgagrl_expected_day_released_at instanceof \DateTimeInterface ? $this->vgagrl_expected_day_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_expected_month_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedMonthReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_expected_month_released_at;
        } else {
            return $this->vgagrl_expected_month_released_at instanceof \DateTimeInterface ? $this->vgagrl_expected_month_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_expected_quarter_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedQuarterReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_expected_quarter_released_at;
        } else {
            return $this->vgagrl_expected_quarter_released_at instanceof \DateTimeInterface ? $this->vgagrl_expected_quarter_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_expected_year_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedYearReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_expected_year_released_at;
        } else {
            return $this->vgagrl_expected_year_released_at instanceof \DateTimeInterface ? $this->vgagrl_expected_year_released_at->format($format) : null;
        }
    }

    /**
     * Get the [vgagrl_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagrl_image_icon_url;
    }

    /**
     * Get the [vgagrl_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagrl_image_medium_url;
    }

    /**
     * Get the [vgagrl_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagrl_image_screen_url;
    }

    /**
     * Get the [vgagrl_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagrl_image_small_url;
    }

    /**
     * Get the [vgagrl_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagrl_image_super_url;
    }

    /**
     * Get the [vgagrl_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagrl_image_thumb_url;
    }

    /**
     * Get the [vgagrl_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagrl_image_tiny_url;
    }

    /**
     * Get the [vgagrl_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagrl_api_detail_url;
    }

    /**
     * Get the [vgagrl_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagrl_site_detail_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_created_at;
        } else {
            return $this->vgagrl_created_at instanceof \DateTimeInterface ? $this->vgagrl_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagrl_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagrl_updated_at;
        } else {
            return $this->vgagrl_updated_at instanceof \DateTimeInterface ? $this->vgagrl_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagrl_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_id !== $v) {
            $this->vgagrl_id = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagrl_vgagga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_vgagga_id !== $v) {
            $this->vgagrl_vgagga_id = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID] = true;
        }

        if ($this->aApiGiantBombGame !== null && $this->aApiGiantBombGame->getId() !== $v) {
            $this->aApiGiantBombGame = null;
        }

        return $this;
    } // setApiGiantBombGameId()

    /**
     * Set the value of [vgagrl_vgagpl_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombPlatformId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_vgagpl_id !== $v) {
            $this->vgagrl_vgagpl_id = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID] = true;
        }

        if ($this->aApiGiantBombPlatform !== null && $this->aApiGiantBombPlatform->getId() !== $v) {
            $this->aApiGiantBombPlatform = null;
        }

        return $this;
    } // setApiGiantBombPlatformId()

    /**
     * Set the value of [vgagrl_vgaggr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombGameRatingId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_vgaggr_id !== $v) {
            $this->vgagrl_vgaggr_id = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID] = true;
        }

        if ($this->aApiGiantBombGameRating !== null && $this->aApiGiantBombGameRating->getId() !== $v) {
            $this->aApiGiantBombGameRating = null;
        }

        return $this;
    } // setApiGiantBombGameRatingId()

    /**
     * Set the value of [vgagrl_vgagre_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombRegionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_vgagre_id !== $v) {
            $this->vgagrl_vgagre_id = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID] = true;
        }

        if ($this->aApiGiantBombRegion !== null && $this->aApiGiantBombRegion->getId() !== $v) {
            $this->aApiGiantBombRegion = null;
        }

        return $this;
    } // setApiGiantBombRegionId()

    /**
     * Set the value of [vgaggl_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggl_name !== $v) {
            $this->vgaggl_name = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagrl_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_summary !== $v) {
            $this->vgagrl_summary = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagrl_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_description !== $v) {
            $this->vgagrl_description = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagrl_minimum_players] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setMinimumPlayers($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_minimum_players !== $v) {
            $this->vgagrl_minimum_players = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS] = true;
        }

        return $this;
    } // setMinimumPlayers()

    /**
     * Set the value of [vgagrl_maximum_players] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setMaximumPlayers($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagrl_maximum_players !== $v) {
            $this->vgagrl_maximum_players = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS] = true;
        }

        return $this;
    } // setMaximumPlayers()

    /**
     * Sets the value of the [vgagrl_widescreen_support] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setWidescreenSupport($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->vgagrl_widescreen_support !== $v) {
            $this->vgagrl_widescreen_support = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT] = true;
        }

        return $this;
    } // setWidescreenSupport()

    /**
     * Set the value of [vgagrl_product_code_value] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setProductCodeValue($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_product_code_value !== $v) {
            $this->vgagrl_product_code_value = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE] = true;
        }

        return $this;
    } // setProductCodeValue()

    /**
     * Sets the value of [vgagrl_original_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setOriginalReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_original_released_at !== null || $dt !== null) {
            if ($this->vgagrl_original_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_original_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_original_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setOriginalReleasedAt()

    /**
     * Sets the value of [vgagrl_expected_day_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setExpectedDayReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_expected_day_released_at !== null || $dt !== null) {
            if ($this->vgagrl_expected_day_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_expected_day_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_expected_day_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedDayReleasedAt()

    /**
     * Sets the value of [vgagrl_expected_month_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setExpectedMonthReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_expected_month_released_at !== null || $dt !== null) {
            if ($this->vgagrl_expected_month_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_expected_month_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_expected_month_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedMonthReleasedAt()

    /**
     * Sets the value of [vgagrl_expected_quarter_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setExpectedQuarterReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_expected_quarter_released_at !== null || $dt !== null) {
            if ($this->vgagrl_expected_quarter_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_expected_quarter_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_expected_quarter_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedQuarterReleasedAt()

    /**
     * Sets the value of [vgagrl_expected_year_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setExpectedYearReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_expected_year_released_at !== null || $dt !== null) {
            if ($this->vgagrl_expected_year_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_expected_year_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_expected_year_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedYearReleasedAt()

    /**
     * Set the value of [vgagrl_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_icon_url !== $v) {
            $this->vgagrl_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagrl_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_medium_url !== $v) {
            $this->vgagrl_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagrl_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_screen_url !== $v) {
            $this->vgagrl_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagrl_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_small_url !== $v) {
            $this->vgagrl_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagrl_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_super_url !== $v) {
            $this->vgagrl_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagrl_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_thumb_url !== $v) {
            $this->vgagrl_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagrl_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_image_tiny_url !== $v) {
            $this->vgagrl_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagrl_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_api_detail_url !== $v) {
            $this->vgagrl_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagrl_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagrl_site_detail_url !== $v) {
            $this->vgagrl_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Sets the value of [vgagrl_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_created_at !== null || $dt !== null) {
            if ($this->vgagrl_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagrl_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagrl_updated_at !== null || $dt !== null) {
            if ($this->vgagrl_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagrl_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagrl_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ApiGiantBombGameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_vgagga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ApiGiantBombPlatformId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_vgagpl_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ApiGiantBombGameRatingId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_vgaggr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ApiGiantBombRegionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_vgagre_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggl_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('MinimumPlayers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_minimum_players = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('MaximumPlayers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_maximum_players = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('WidescreenSupport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_widescreen_support = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ProductCodeValue', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_product_code_value = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('OriginalReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_original_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ExpectedDayReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_expected_day_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ExpectedMonthReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_expected_month_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ExpectedQuarterReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_expected_quarter_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ExpectedYearReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_expected_year_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagrl_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : ApiGiantBombGameReleaseTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagrl_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 28; // 28 = ApiGiantBombGameReleaseTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombGame !== null && $this->vgagrl_vgagga_id !== $this->aApiGiantBombGame->getId()) {
            $this->aApiGiantBombGame = null;
        }
        if ($this->aApiGiantBombPlatform !== null && $this->vgagrl_vgagpl_id !== $this->aApiGiantBombPlatform->getId()) {
            $this->aApiGiantBombPlatform = null;
        }
        if ($this->aApiGiantBombGameRating !== null && $this->vgagrl_vgaggr_id !== $this->aApiGiantBombGameRating->getId()) {
            $this->aApiGiantBombGameRating = null;
        }
        if ($this->aApiGiantBombRegion !== null && $this->vgagrl_vgagre_id !== $this->aApiGiantBombRegion->getId()) {
            $this->aApiGiantBombRegion = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombGameReleaseQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombGame = null;
            $this->aApiGiantBombPlatform = null;
            $this->aApiGiantBombGameRating = null;
            $this->aApiGiantBombRegion = null;
            $this->collApiGiantBombGameReleaseDevelopers = null;

            $this->collApiGiantBombGameReleasePublishers = null;

            $this->collApiGiantBombImages = null;

            $this->collApiGiantBombVideos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombGameRelease::setDeleted()
     * @see ApiGiantBombGameRelease::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombGameReleaseQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombGameReleaseTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombGame !== null) {
                if ($this->aApiGiantBombGame->isModified() || $this->aApiGiantBombGame->isNew()) {
                    $affectedRows += $this->aApiGiantBombGame->save($con);
                }
                $this->setApiGiantBombGame($this->aApiGiantBombGame);
            }

            if ($this->aApiGiantBombPlatform !== null) {
                if ($this->aApiGiantBombPlatform->isModified() || $this->aApiGiantBombPlatform->isNew()) {
                    $affectedRows += $this->aApiGiantBombPlatform->save($con);
                }
                $this->setApiGiantBombPlatform($this->aApiGiantBombPlatform);
            }

            if ($this->aApiGiantBombGameRating !== null) {
                if ($this->aApiGiantBombGameRating->isModified() || $this->aApiGiantBombGameRating->isNew()) {
                    $affectedRows += $this->aApiGiantBombGameRating->save($con);
                }
                $this->setApiGiantBombGameRating($this->aApiGiantBombGameRating);
            }

            if ($this->aApiGiantBombRegion !== null) {
                if ($this->aApiGiantBombRegion->isModified() || $this->aApiGiantBombRegion->isNew()) {
                    $affectedRows += $this->aApiGiantBombRegion->save($con);
                }
                $this->setApiGiantBombRegion($this->aApiGiantBombRegion);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleaseDevelopers !== null) {
                foreach ($this->collApiGiantBombGameReleaseDevelopers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleasePublishersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleasePublishersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleasePublishersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleasePublishersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleasePublishers !== null) {
                foreach ($this->collApiGiantBombGameReleasePublishers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombImagesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombImagesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombImagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombImagesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombImages !== null) {
                foreach ($this->collApiGiantBombImages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombVideosScheduledForDeletion !== null) {
                if (!$this->apiGiantBombVideosScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombVideosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombVideosScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombVideos !== null) {
                foreach ($this->collApiGiantBombVideos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_vgagga_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_vgagpl_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_vgaggr_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_vgagre_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggl_name';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_summary';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_description';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_minimum_players';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_maximum_players';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_widescreen_support';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_product_code_value';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_original_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_expected_day_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_expected_month_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_expected_quarter_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_expected_year_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagrl_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_release_vgagrl (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagrl_id':
                        $stmt->bindValue($identifier, $this->vgagrl_id, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_vgagga_id':
                        $stmt->bindValue($identifier, $this->vgagrl_vgagga_id, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_vgagpl_id':
                        $stmt->bindValue($identifier, $this->vgagrl_vgagpl_id, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_vgaggr_id':
                        $stmt->bindValue($identifier, $this->vgagrl_vgaggr_id, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_vgagre_id':
                        $stmt->bindValue($identifier, $this->vgagrl_vgagre_id, PDO::PARAM_INT);
                        break;
                    case 'vgaggl_name':
                        $stmt->bindValue($identifier, $this->vgaggl_name, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_summary':
                        $stmt->bindValue($identifier, $this->vgagrl_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_description':
                        $stmt->bindValue($identifier, $this->vgagrl_description, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_minimum_players':
                        $stmt->bindValue($identifier, $this->vgagrl_minimum_players, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_maximum_players':
                        $stmt->bindValue($identifier, $this->vgagrl_maximum_players, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_widescreen_support':
                        $stmt->bindValue($identifier, (int) $this->vgagrl_widescreen_support, PDO::PARAM_INT);
                        break;
                    case 'vgagrl_product_code_value':
                        $stmt->bindValue($identifier, $this->vgagrl_product_code_value, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_original_released_at':
                        $stmt->bindValue($identifier, $this->vgagrl_original_released_at ? $this->vgagrl_original_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_expected_day_released_at':
                        $stmt->bindValue($identifier, $this->vgagrl_expected_day_released_at ? $this->vgagrl_expected_day_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_expected_month_released_at':
                        $stmt->bindValue($identifier, $this->vgagrl_expected_month_released_at ? $this->vgagrl_expected_month_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_expected_quarter_released_at':
                        $stmt->bindValue($identifier, $this->vgagrl_expected_quarter_released_at ? $this->vgagrl_expected_quarter_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_expected_year_released_at':
                        $stmt->bindValue($identifier, $this->vgagrl_expected_year_released_at ? $this->vgagrl_expected_year_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagrl_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagrl_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagrl_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_created_at':
                        $stmt->bindValue($identifier, $this->vgagrl_created_at ? $this->vgagrl_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagrl_updated_at':
                        $stmt->bindValue($identifier, $this->vgagrl_updated_at ? $this->vgagrl_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameReleaseTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getApiGiantBombGameId();
                break;
            case 2:
                return $this->getApiGiantBombPlatformId();
                break;
            case 3:
                return $this->getApiGiantBombGameRatingId();
                break;
            case 4:
                return $this->getApiGiantBombRegionId();
                break;
            case 5:
                return $this->getName();
                break;
            case 6:
                return $this->getSummary();
                break;
            case 7:
                return $this->getDescription();
                break;
            case 8:
                return $this->getMinimumPlayers();
                break;
            case 9:
                return $this->getMaximumPlayers();
                break;
            case 10:
                return $this->getWidescreenSupport();
                break;
            case 11:
                return $this->getProductCodeValue();
                break;
            case 12:
                return $this->getOriginalReleasedAt();
                break;
            case 13:
                return $this->getExpectedDayReleasedAt();
                break;
            case 14:
                return $this->getExpectedMonthReleasedAt();
                break;
            case 15:
                return $this->getExpectedQuarterReleasedAt();
                break;
            case 16:
                return $this->getExpectedYearReleasedAt();
                break;
            case 17:
                return $this->getImageIconUrl();
                break;
            case 18:
                return $this->getImageMediumUrl();
                break;
            case 19:
                return $this->getImageScreenUrl();
                break;
            case 20:
                return $this->getImageSmallUrl();
                break;
            case 21:
                return $this->getImageSuperUrl();
                break;
            case 22:
                return $this->getImageThumbUrl();
                break;
            case 23:
                return $this->getImageTinyUrl();
                break;
            case 24:
                return $this->getApiDetailUrl();
                break;
            case 25:
                return $this->getSiteDetailUrl();
                break;
            case 26:
                return $this->getCreatedAt();
                break;
            case 27:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombGameRelease'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombGameRelease'][$this->hashCode()] = true;
        $keys = ApiGiantBombGameReleaseTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getApiGiantBombGameId(),
            $keys[2] => $this->getApiGiantBombPlatformId(),
            $keys[3] => $this->getApiGiantBombGameRatingId(),
            $keys[4] => $this->getApiGiantBombRegionId(),
            $keys[5] => $this->getName(),
            $keys[6] => $this->getSummary(),
            $keys[7] => $this->getDescription(),
            $keys[8] => $this->getMinimumPlayers(),
            $keys[9] => $this->getMaximumPlayers(),
            $keys[10] => $this->getWidescreenSupport(),
            $keys[11] => $this->getProductCodeValue(),
            $keys[12] => $this->getOriginalReleasedAt(),
            $keys[13] => $this->getExpectedDayReleasedAt(),
            $keys[14] => $this->getExpectedMonthReleasedAt(),
            $keys[15] => $this->getExpectedQuarterReleasedAt(),
            $keys[16] => $this->getExpectedYearReleasedAt(),
            $keys[17] => $this->getImageIconUrl(),
            $keys[18] => $this->getImageMediumUrl(),
            $keys[19] => $this->getImageScreenUrl(),
            $keys[20] => $this->getImageSmallUrl(),
            $keys[21] => $this->getImageSuperUrl(),
            $keys[22] => $this->getImageThumbUrl(),
            $keys[23] => $this->getImageTinyUrl(),
            $keys[24] => $this->getApiDetailUrl(),
            $keys[25] => $this->getSiteDetailUrl(),
            $keys[26] => $this->getCreatedAt(),
            $keys[27] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[26]] instanceof \DateTime) {
            $result[$keys[26]] = $result[$keys[26]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTime) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGame';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_vgagga';
                        break;
                    default:
                        $key = 'ApiGiantBombGame';
                }

                $result[$key] = $this->aApiGiantBombGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiGiantBombPlatform) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombPlatform';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_platform_vgagpl';
                        break;
                    default:
                        $key = 'ApiGiantBombPlatform';
                }

                $result[$key] = $this->aApiGiantBombPlatform->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiGiantBombGameRating) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameRating';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_rating_vgaggr';
                        break;
                    default:
                        $key = 'ApiGiantBombGameRating';
                }

                $result[$key] = $this->aApiGiantBombGameRating->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiGiantBombRegion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombRegion';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_region_vgagre';
                        break;
                    default:
                        $key = 'ApiGiantBombRegion';
                }

                $result[$key] = $this->aApiGiantBombRegion->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collApiGiantBombGameReleaseDevelopers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleaseDevelopers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_release_publisher_vgagrds';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleaseDevelopers';
                }

                $result[$key] = $this->collApiGiantBombGameReleaseDevelopers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleasePublishers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleasePublishers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_release_publisher_vgagrps';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleasePublishers';
                }

                $result[$key] = $this->collApiGiantBombGameReleasePublishers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombImages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombImages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_image_vgagims';
                        break;
                    default:
                        $key = 'ApiGiantBombImages';
                }

                $result[$key] = $this->collApiGiantBombImages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombVideos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombVideos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_video_vgagvis';
                        break;
                    default:
                        $key = 'ApiGiantBombVideos';
                }

                $result[$key] = $this->collApiGiantBombVideos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameReleaseTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setApiGiantBombGameId($value);
                break;
            case 2:
                $this->setApiGiantBombPlatformId($value);
                break;
            case 3:
                $this->setApiGiantBombGameRatingId($value);
                break;
            case 4:
                $this->setApiGiantBombRegionId($value);
                break;
            case 5:
                $this->setName($value);
                break;
            case 6:
                $this->setSummary($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
            case 8:
                $this->setMinimumPlayers($value);
                break;
            case 9:
                $this->setMaximumPlayers($value);
                break;
            case 10:
                $this->setWidescreenSupport($value);
                break;
            case 11:
                $this->setProductCodeValue($value);
                break;
            case 12:
                $this->setOriginalReleasedAt($value);
                break;
            case 13:
                $this->setExpectedDayReleasedAt($value);
                break;
            case 14:
                $this->setExpectedMonthReleasedAt($value);
                break;
            case 15:
                $this->setExpectedQuarterReleasedAt($value);
                break;
            case 16:
                $this->setExpectedYearReleasedAt($value);
                break;
            case 17:
                $this->setImageIconUrl($value);
                break;
            case 18:
                $this->setImageMediumUrl($value);
                break;
            case 19:
                $this->setImageScreenUrl($value);
                break;
            case 20:
                $this->setImageSmallUrl($value);
                break;
            case 21:
                $this->setImageSuperUrl($value);
                break;
            case 22:
                $this->setImageThumbUrl($value);
                break;
            case 23:
                $this->setImageTinyUrl($value);
                break;
            case 24:
                $this->setApiDetailUrl($value);
                break;
            case 25:
                $this->setSiteDetailUrl($value);
                break;
            case 26:
                $this->setCreatedAt($value);
                break;
            case 27:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombGameReleaseTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setApiGiantBombGameId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setApiGiantBombPlatformId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setApiGiantBombGameRatingId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setApiGiantBombRegionId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setName($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSummary($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDescription($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setMinimumPlayers($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setMaximumPlayers($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setWidescreenSupport($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setProductCodeValue($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setOriginalReleasedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setExpectedDayReleasedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setExpectedMonthReleasedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setExpectedQuarterReleasedAt($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setExpectedYearReleasedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setImageIconUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setImageMediumUrl($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setImageScreenUrl($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setImageSmallUrl($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setImageSuperUrl($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setImageThumbUrl($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setImageTinyUrl($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setApiDetailUrl($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setCreatedAt($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setUpdatedAt($arr[$keys[27]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $this->vgagrl_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $this->vgagrl_vgagga_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $this->vgagrl_vgagpl_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $this->vgagrl_vgaggr_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $this->vgagrl_vgagre_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME, $this->vgaggl_name);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY, $this->vgagrl_summary);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION, $this->vgagrl_description);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS, $this->vgagrl_minimum_players);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS, $this->vgagrl_maximum_players);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT, $this->vgagrl_widescreen_support);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE, $this->vgagrl_product_code_value);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT, $this->vgagrl_original_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT, $this->vgagrl_expected_day_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT, $this->vgagrl_expected_month_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT, $this->vgagrl_expected_quarter_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT, $this->vgagrl_expected_year_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL, $this->vgagrl_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL, $this->vgagrl_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL, $this->vgagrl_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL, $this->vgagrl_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL, $this->vgagrl_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL, $this->vgagrl_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL, $this->vgagrl_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL, $this->vgagrl_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL, $this->vgagrl_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT, $this->vgagrl_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT)) {
            $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT, $this->vgagrl_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombGameReleaseQuery::create();
        $criteria->add(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $this->vgagrl_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagrl_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setApiGiantBombGameId($this->getApiGiantBombGameId());
        $copyObj->setApiGiantBombPlatformId($this->getApiGiantBombPlatformId());
        $copyObj->setApiGiantBombGameRatingId($this->getApiGiantBombGameRatingId());
        $copyObj->setApiGiantBombRegionId($this->getApiGiantBombRegionId());
        $copyObj->setName($this->getName());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setMinimumPlayers($this->getMinimumPlayers());
        $copyObj->setMaximumPlayers($this->getMaximumPlayers());
        $copyObj->setWidescreenSupport($this->getWidescreenSupport());
        $copyObj->setProductCodeValue($this->getProductCodeValue());
        $copyObj->setOriginalReleasedAt($this->getOriginalReleasedAt());
        $copyObj->setExpectedDayReleasedAt($this->getExpectedDayReleasedAt());
        $copyObj->setExpectedMonthReleasedAt($this->getExpectedMonthReleasedAt());
        $copyObj->setExpectedQuarterReleasedAt($this->getExpectedQuarterReleasedAt());
        $copyObj->setExpectedYearReleasedAt($this->getExpectedYearReleasedAt());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombGameReleaseDevelopers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameReleaseDeveloper($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleasePublishers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameReleasePublisher($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombImages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombImage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombVideos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombVideo($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGame object.
     *
     * @param  ChildApiGiantBombGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombGame(ChildApiGiantBombGame $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombGameId(NULL);
        } else {
            $this->setApiGiantBombGameId($v->getId());
        }

        $this->aApiGiantBombGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGame object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombGameRelease($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGame The associated ChildApiGiantBombGame object.
     * @throws PropelException
     */
    public function getApiGiantBombGame(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombGame === null && ($this->vgagrl_vgagga_id !== null)) {
            $this->aApiGiantBombGame = ChildApiGiantBombGameQuery::create()->findPk($this->vgagrl_vgagga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombGame->addApiGiantBombGameReleases($this);
             */
        }

        return $this->aApiGiantBombGame;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombPlatform object.
     *
     * @param  ChildApiGiantBombPlatform $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombPlatform(ChildApiGiantBombPlatform $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombPlatformId(NULL);
        } else {
            $this->setApiGiantBombPlatformId($v->getId());
        }

        $this->aApiGiantBombPlatform = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombPlatform object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombGameRelease($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombPlatform object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombPlatform The associated ChildApiGiantBombPlatform object.
     * @throws PropelException
     */
    public function getApiGiantBombPlatform(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombPlatform === null && ($this->vgagrl_vgagpl_id !== null)) {
            $this->aApiGiantBombPlatform = ChildApiGiantBombPlatformQuery::create()->findPk($this->vgagrl_vgagpl_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombPlatform->addApiGiantBombGameReleases($this);
             */
        }

        return $this->aApiGiantBombPlatform;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGameRating object.
     *
     * @param  ChildApiGiantBombGameRating $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombGameRating(ChildApiGiantBombGameRating $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombGameRatingId(NULL);
        } else {
            $this->setApiGiantBombGameRatingId($v->getId());
        }

        $this->aApiGiantBombGameRating = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGameRating object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombGameRelease($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGameRating object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGameRating The associated ChildApiGiantBombGameRating object.
     * @throws PropelException
     */
    public function getApiGiantBombGameRating(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombGameRating === null && ($this->vgagrl_vgaggr_id !== null)) {
            $this->aApiGiantBombGameRating = ChildApiGiantBombGameRatingQuery::create()->findPk($this->vgagrl_vgaggr_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombGameRating->addApiGiantBombGameReleases($this);
             */
        }

        return $this->aApiGiantBombGameRating;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombRegion object.
     *
     * @param  ChildApiGiantBombRegion $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombRegion(ChildApiGiantBombRegion $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombRegionId(NULL);
        } else {
            $this->setApiGiantBombRegionId($v->getId());
        }

        $this->aApiGiantBombRegion = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombRegion object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombGameRelease($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombRegion object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombRegion The associated ChildApiGiantBombRegion object.
     * @throws PropelException
     */
    public function getApiGiantBombRegion(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombRegion === null && ($this->vgagrl_vgagre_id !== null)) {
            $this->aApiGiantBombRegion = ChildApiGiantBombRegionQuery::create()->findPk($this->vgagrl_vgagre_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombRegion->addApiGiantBombGameReleases($this);
             */
        }

        return $this->aApiGiantBombRegion;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombGameReleaseDeveloper' == $relationName) {
            return $this->initApiGiantBombGameReleaseDevelopers();
        }
        if ('ApiGiantBombGameReleasePublisher' == $relationName) {
            return $this->initApiGiantBombGameReleasePublishers();
        }
        if ('ApiGiantBombImage' == $relationName) {
            return $this->initApiGiantBombImages();
        }
        if ('ApiGiantBombVideo' == $relationName) {
            return $this->initApiGiantBombVideos();
        }
    }

    /**
     * Clears out the collApiGiantBombGameReleaseDevelopers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleaseDevelopers()
     */
    public function clearApiGiantBombGameReleaseDevelopers()
    {
        $this->collApiGiantBombGameReleaseDevelopers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleaseDevelopers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleaseDevelopers($v = true)
    {
        $this->collApiGiantBombGameReleaseDevelopersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleaseDevelopers collection.
     *
     * By default this just sets the collApiGiantBombGameReleaseDevelopers collection to an empty array (like clearcollApiGiantBombGameReleaseDevelopers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleaseDevelopers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleaseDevelopers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleaseDeveloperTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleaseDevelopers = new $collectionClassName;
        $this->collApiGiantBombGameReleaseDevelopers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper');
    }

    /**
     * Gets an array of ChildApiGiantBombGameReleaseDeveloper objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRelease is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] List of ChildApiGiantBombGameReleaseDeveloper objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleaseDevelopers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleaseDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleaseDevelopers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleaseDevelopers) {
                // return empty collection
                $this->initApiGiantBombGameReleaseDevelopers();
            } else {
                $collApiGiantBombGameReleaseDevelopers = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRelease($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleaseDevelopersPartial && count($collApiGiantBombGameReleaseDevelopers)) {
                        $this->initApiGiantBombGameReleaseDevelopers(false);

                        foreach ($collApiGiantBombGameReleaseDevelopers as $obj) {
                            if (false == $this->collApiGiantBombGameReleaseDevelopers->contains($obj)) {
                                $this->collApiGiantBombGameReleaseDevelopers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleaseDevelopersPartial = true;
                    }

                    return $collApiGiantBombGameReleaseDevelopers;
                }

                if ($partial && $this->collApiGiantBombGameReleaseDevelopers) {
                    foreach ($this->collApiGiantBombGameReleaseDevelopers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleaseDevelopers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleaseDevelopers = $collApiGiantBombGameReleaseDevelopers;
                $this->collApiGiantBombGameReleaseDevelopersPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleaseDevelopers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameReleaseDeveloper objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleaseDevelopers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleaseDevelopers(Collection $apiGiantBombGameReleaseDevelopers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameReleaseDeveloper[] $apiGiantBombGameReleaseDevelopersToDelete */
        $apiGiantBombGameReleaseDevelopersToDelete = $this->getApiGiantBombGameReleaseDevelopers(new Criteria(), $con)->diff($apiGiantBombGameReleaseDevelopers);


        $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = $apiGiantBombGameReleaseDevelopersToDelete;

        foreach ($apiGiantBombGameReleaseDevelopersToDelete as $apiGiantBombGameReleaseDeveloperRemoved) {
            $apiGiantBombGameReleaseDeveloperRemoved->setApiGiantBombGameRelease(null);
        }

        $this->collApiGiantBombGameReleaseDevelopers = null;
        foreach ($apiGiantBombGameReleaseDevelopers as $apiGiantBombGameReleaseDeveloper) {
            $this->addApiGiantBombGameReleaseDeveloper($apiGiantBombGameReleaseDeveloper);
        }

        $this->collApiGiantBombGameReleaseDevelopers = $apiGiantBombGameReleaseDevelopers;
        $this->collApiGiantBombGameReleaseDevelopersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameReleaseDeveloper objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameReleaseDeveloper objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleaseDevelopers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleaseDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleaseDevelopers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleaseDevelopers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleaseDevelopers());
            }

            $query = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRelease($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleaseDevelopers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameReleaseDeveloper object to this object
     * through the ChildApiGiantBombGameReleaseDeveloper foreign key attribute.
     *
     * @param  ChildApiGiantBombGameReleaseDeveloper $l ChildApiGiantBombGameReleaseDeveloper
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function addApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $l)
    {
        if ($this->collApiGiantBombGameReleaseDevelopers === null) {
            $this->initApiGiantBombGameReleaseDevelopers();
            $this->collApiGiantBombGameReleaseDevelopersPartial = true;
        }

        if (!$this->collApiGiantBombGameReleaseDevelopers->contains($l)) {
            $this->doAddApiGiantBombGameReleaseDeveloper($l);

            if ($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion and $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->remove($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper The ChildApiGiantBombGameReleaseDeveloper object to add.
     */
    protected function doAddApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper)
    {
        $this->collApiGiantBombGameReleaseDevelopers[]= $apiGiantBombGameReleaseDeveloper;
        $apiGiantBombGameReleaseDeveloper->setApiGiantBombGameRelease($this);
    }

    /**
     * @param  ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper The ChildApiGiantBombGameReleaseDeveloper object to remove.
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function removeApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper)
    {
        if ($this->getApiGiantBombGameReleaseDevelopers()->contains($apiGiantBombGameReleaseDeveloper)) {
            $pos = $this->collApiGiantBombGameReleaseDevelopers->search($apiGiantBombGameReleaseDeveloper);
            $this->collApiGiantBombGameReleaseDevelopers->remove($pos);
            if (null === $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion) {
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = clone $this->collApiGiantBombGameReleaseDevelopers;
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion[]= clone $apiGiantBombGameReleaseDeveloper;
            $apiGiantBombGameReleaseDeveloper->setApiGiantBombGameRelease(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRelease is new, it will return
     * an empty collection; or if this ApiGiantBombGameRelease has previously
     * been saved, it will retrieve related ApiGiantBombGameReleaseDevelopers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRelease.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] List of ChildApiGiantBombGameReleaseDeveloper objects
     */
    public function getApiGiantBombGameReleaseDevelopersJoinApiGiantBombCompany(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombCompany', $joinBehavior);

        return $this->getApiGiantBombGameReleaseDevelopers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleasePublishers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleasePublishers()
     */
    public function clearApiGiantBombGameReleasePublishers()
    {
        $this->collApiGiantBombGameReleasePublishers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleasePublishers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleasePublishers($v = true)
    {
        $this->collApiGiantBombGameReleasePublishersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleasePublishers collection.
     *
     * By default this just sets the collApiGiantBombGameReleasePublishers collection to an empty array (like clearcollApiGiantBombGameReleasePublishers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleasePublishers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleasePublishers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleasePublisherTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleasePublishers = new $collectionClassName;
        $this->collApiGiantBombGameReleasePublishers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher');
    }

    /**
     * Gets an array of ChildApiGiantBombGameReleasePublisher objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRelease is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameReleasePublisher[] List of ChildApiGiantBombGameReleasePublisher objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleasePublishers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleasePublishers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleasePublishers) {
                // return empty collection
                $this->initApiGiantBombGameReleasePublishers();
            } else {
                $collApiGiantBombGameReleasePublishers = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRelease($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleasePublishersPartial && count($collApiGiantBombGameReleasePublishers)) {
                        $this->initApiGiantBombGameReleasePublishers(false);

                        foreach ($collApiGiantBombGameReleasePublishers as $obj) {
                            if (false == $this->collApiGiantBombGameReleasePublishers->contains($obj)) {
                                $this->collApiGiantBombGameReleasePublishers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleasePublishersPartial = true;
                    }

                    return $collApiGiantBombGameReleasePublishers;
                }

                if ($partial && $this->collApiGiantBombGameReleasePublishers) {
                    foreach ($this->collApiGiantBombGameReleasePublishers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleasePublishers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleasePublishers = $collApiGiantBombGameReleasePublishers;
                $this->collApiGiantBombGameReleasePublishersPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleasePublishers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameReleasePublisher objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleasePublishers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleasePublishers(Collection $apiGiantBombGameReleasePublishers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameReleasePublisher[] $apiGiantBombGameReleasePublishersToDelete */
        $apiGiantBombGameReleasePublishersToDelete = $this->getApiGiantBombGameReleasePublishers(new Criteria(), $con)->diff($apiGiantBombGameReleasePublishers);


        $this->apiGiantBombGameReleasePublishersScheduledForDeletion = $apiGiantBombGameReleasePublishersToDelete;

        foreach ($apiGiantBombGameReleasePublishersToDelete as $apiGiantBombGameReleasePublisherRemoved) {
            $apiGiantBombGameReleasePublisherRemoved->setApiGiantBombGameRelease(null);
        }

        $this->collApiGiantBombGameReleasePublishers = null;
        foreach ($apiGiantBombGameReleasePublishers as $apiGiantBombGameReleasePublisher) {
            $this->addApiGiantBombGameReleasePublisher($apiGiantBombGameReleasePublisher);
        }

        $this->collApiGiantBombGameReleasePublishers = $apiGiantBombGameReleasePublishers;
        $this->collApiGiantBombGameReleasePublishersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameReleasePublisher objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameReleasePublisher objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleasePublishers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleasePublishers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleasePublishers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleasePublishers());
            }

            $query = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRelease($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleasePublishers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameReleasePublisher object to this object
     * through the ChildApiGiantBombGameReleasePublisher foreign key attribute.
     *
     * @param  ChildApiGiantBombGameReleasePublisher $l ChildApiGiantBombGameReleasePublisher
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function addApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $l)
    {
        if ($this->collApiGiantBombGameReleasePublishers === null) {
            $this->initApiGiantBombGameReleasePublishers();
            $this->collApiGiantBombGameReleasePublishersPartial = true;
        }

        if (!$this->collApiGiantBombGameReleasePublishers->contains($l)) {
            $this->doAddApiGiantBombGameReleasePublisher($l);

            if ($this->apiGiantBombGameReleasePublishersScheduledForDeletion and $this->apiGiantBombGameReleasePublishersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion->remove($this->apiGiantBombGameReleasePublishersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher The ChildApiGiantBombGameReleasePublisher object to add.
     */
    protected function doAddApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher)
    {
        $this->collApiGiantBombGameReleasePublishers[]= $apiGiantBombGameReleasePublisher;
        $apiGiantBombGameReleasePublisher->setApiGiantBombGameRelease($this);
    }

    /**
     * @param  ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher The ChildApiGiantBombGameReleasePublisher object to remove.
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function removeApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher)
    {
        if ($this->getApiGiantBombGameReleasePublishers()->contains($apiGiantBombGameReleasePublisher)) {
            $pos = $this->collApiGiantBombGameReleasePublishers->search($apiGiantBombGameReleasePublisher);
            $this->collApiGiantBombGameReleasePublishers->remove($pos);
            if (null === $this->apiGiantBombGameReleasePublishersScheduledForDeletion) {
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion = clone $this->collApiGiantBombGameReleasePublishers;
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleasePublishersScheduledForDeletion[]= clone $apiGiantBombGameReleasePublisher;
            $apiGiantBombGameReleasePublisher->setApiGiantBombGameRelease(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRelease is new, it will return
     * an empty collection; or if this ApiGiantBombGameRelease has previously
     * been saved, it will retrieve related ApiGiantBombGameReleasePublishers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRelease.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameReleasePublisher[] List of ChildApiGiantBombGameReleasePublisher objects
     */
    public function getApiGiantBombGameReleasePublishersJoinApiGiantBombCompany(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombCompany', $joinBehavior);

        return $this->getApiGiantBombGameReleasePublishers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombImages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombImages()
     */
    public function clearApiGiantBombImages()
    {
        $this->collApiGiantBombImages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombImages collection loaded partially.
     */
    public function resetPartialApiGiantBombImages($v = true)
    {
        $this->collApiGiantBombImagesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombImages collection.
     *
     * By default this just sets the collApiGiantBombImages collection to an empty array (like clearcollApiGiantBombImages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombImages($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombImages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombImageTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombImages = new $collectionClassName;
        $this->collApiGiantBombImages->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage');
    }

    /**
     * Gets an array of ChildApiGiantBombImage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRelease is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombImage[] List of ChildApiGiantBombImage objects
     * @throws PropelException
     */
    public function getApiGiantBombImages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombImagesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombImages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombImages) {
                // return empty collection
                $this->initApiGiantBombImages();
            } else {
                $collApiGiantBombImages = ChildApiGiantBombImageQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRelease($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombImagesPartial && count($collApiGiantBombImages)) {
                        $this->initApiGiantBombImages(false);

                        foreach ($collApiGiantBombImages as $obj) {
                            if (false == $this->collApiGiantBombImages->contains($obj)) {
                                $this->collApiGiantBombImages->append($obj);
                            }
                        }

                        $this->collApiGiantBombImagesPartial = true;
                    }

                    return $collApiGiantBombImages;
                }

                if ($partial && $this->collApiGiantBombImages) {
                    foreach ($this->collApiGiantBombImages as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombImages[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombImages = $collApiGiantBombImages;
                $this->collApiGiantBombImagesPartial = false;
            }
        }

        return $this->collApiGiantBombImages;
    }

    /**
     * Sets a collection of ChildApiGiantBombImage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombImages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombImages(Collection $apiGiantBombImages, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombImage[] $apiGiantBombImagesToDelete */
        $apiGiantBombImagesToDelete = $this->getApiGiantBombImages(new Criteria(), $con)->diff($apiGiantBombImages);


        $this->apiGiantBombImagesScheduledForDeletion = $apiGiantBombImagesToDelete;

        foreach ($apiGiantBombImagesToDelete as $apiGiantBombImageRemoved) {
            $apiGiantBombImageRemoved->setApiGiantBombGameRelease(null);
        }

        $this->collApiGiantBombImages = null;
        foreach ($apiGiantBombImages as $apiGiantBombImage) {
            $this->addApiGiantBombImage($apiGiantBombImage);
        }

        $this->collApiGiantBombImages = $apiGiantBombImages;
        $this->collApiGiantBombImagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombImage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombImage objects.
     * @throws PropelException
     */
    public function countApiGiantBombImages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombImagesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombImages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombImages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombImages());
            }

            $query = ChildApiGiantBombImageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRelease($this)
                ->count($con);
        }

        return count($this->collApiGiantBombImages);
    }

    /**
     * Method called to associate a ChildApiGiantBombImage object to this object
     * through the ChildApiGiantBombImage foreign key attribute.
     *
     * @param  ChildApiGiantBombImage $l ChildApiGiantBombImage
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function addApiGiantBombImage(ChildApiGiantBombImage $l)
    {
        if ($this->collApiGiantBombImages === null) {
            $this->initApiGiantBombImages();
            $this->collApiGiantBombImagesPartial = true;
        }

        if (!$this->collApiGiantBombImages->contains($l)) {
            $this->doAddApiGiantBombImage($l);

            if ($this->apiGiantBombImagesScheduledForDeletion and $this->apiGiantBombImagesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombImagesScheduledForDeletion->remove($this->apiGiantBombImagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombImage $apiGiantBombImage The ChildApiGiantBombImage object to add.
     */
    protected function doAddApiGiantBombImage(ChildApiGiantBombImage $apiGiantBombImage)
    {
        $this->collApiGiantBombImages[]= $apiGiantBombImage;
        $apiGiantBombImage->setApiGiantBombGameRelease($this);
    }

    /**
     * @param  ChildApiGiantBombImage $apiGiantBombImage The ChildApiGiantBombImage object to remove.
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function removeApiGiantBombImage(ChildApiGiantBombImage $apiGiantBombImage)
    {
        if ($this->getApiGiantBombImages()->contains($apiGiantBombImage)) {
            $pos = $this->collApiGiantBombImages->search($apiGiantBombImage);
            $this->collApiGiantBombImages->remove($pos);
            if (null === $this->apiGiantBombImagesScheduledForDeletion) {
                $this->apiGiantBombImagesScheduledForDeletion = clone $this->collApiGiantBombImages;
                $this->apiGiantBombImagesScheduledForDeletion->clear();
            }
            $this->apiGiantBombImagesScheduledForDeletion[]= $apiGiantBombImage;
            $apiGiantBombImage->setApiGiantBombGameRelease(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRelease is new, it will return
     * an empty collection; or if this ApiGiantBombGameRelease has previously
     * been saved, it will retrieve related ApiGiantBombImages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRelease.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombImage[] List of ChildApiGiantBombImage objects
     */
    public function getApiGiantBombImagesJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombImageQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombImages($query, $con);
    }

    /**
     * Clears out the collApiGiantBombVideos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombVideos()
     */
    public function clearApiGiantBombVideos()
    {
        $this->collApiGiantBombVideos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombVideos collection loaded partially.
     */
    public function resetPartialApiGiantBombVideos($v = true)
    {
        $this->collApiGiantBombVideosPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombVideos collection.
     *
     * By default this just sets the collApiGiantBombVideos collection to an empty array (like clearcollApiGiantBombVideos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombVideos($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombVideos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombVideoTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombVideos = new $collectionClassName;
        $this->collApiGiantBombVideos->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo');
    }

    /**
     * Gets an array of ChildApiGiantBombVideo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRelease is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombVideo[] List of ChildApiGiantBombVideo objects
     * @throws PropelException
     */
    public function getApiGiantBombVideos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombVideosPartial && !$this->isNew();
        if (null === $this->collApiGiantBombVideos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombVideos) {
                // return empty collection
                $this->initApiGiantBombVideos();
            } else {
                $collApiGiantBombVideos = ChildApiGiantBombVideoQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRelease($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombVideosPartial && count($collApiGiantBombVideos)) {
                        $this->initApiGiantBombVideos(false);

                        foreach ($collApiGiantBombVideos as $obj) {
                            if (false == $this->collApiGiantBombVideos->contains($obj)) {
                                $this->collApiGiantBombVideos->append($obj);
                            }
                        }

                        $this->collApiGiantBombVideosPartial = true;
                    }

                    return $collApiGiantBombVideos;
                }

                if ($partial && $this->collApiGiantBombVideos) {
                    foreach ($this->collApiGiantBombVideos as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombVideos[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombVideos = $collApiGiantBombVideos;
                $this->collApiGiantBombVideosPartial = false;
            }
        }

        return $this->collApiGiantBombVideos;
    }

    /**
     * Sets a collection of ChildApiGiantBombVideo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombVideos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function setApiGiantBombVideos(Collection $apiGiantBombVideos, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombVideo[] $apiGiantBombVideosToDelete */
        $apiGiantBombVideosToDelete = $this->getApiGiantBombVideos(new Criteria(), $con)->diff($apiGiantBombVideos);


        $this->apiGiantBombVideosScheduledForDeletion = $apiGiantBombVideosToDelete;

        foreach ($apiGiantBombVideosToDelete as $apiGiantBombVideoRemoved) {
            $apiGiantBombVideoRemoved->setApiGiantBombGameRelease(null);
        }

        $this->collApiGiantBombVideos = null;
        foreach ($apiGiantBombVideos as $apiGiantBombVideo) {
            $this->addApiGiantBombVideo($apiGiantBombVideo);
        }

        $this->collApiGiantBombVideos = $apiGiantBombVideos;
        $this->collApiGiantBombVideosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombVideo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombVideo objects.
     * @throws PropelException
     */
    public function countApiGiantBombVideos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombVideosPartial && !$this->isNew();
        if (null === $this->collApiGiantBombVideos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombVideos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombVideos());
            }

            $query = ChildApiGiantBombVideoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRelease($this)
                ->count($con);
        }

        return count($this->collApiGiantBombVideos);
    }

    /**
     * Method called to associate a ChildApiGiantBombVideo object to this object
     * through the ChildApiGiantBombVideo foreign key attribute.
     *
     * @param  ChildApiGiantBombVideo $l ChildApiGiantBombVideo
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function addApiGiantBombVideo(ChildApiGiantBombVideo $l)
    {
        if ($this->collApiGiantBombVideos === null) {
            $this->initApiGiantBombVideos();
            $this->collApiGiantBombVideosPartial = true;
        }

        if (!$this->collApiGiantBombVideos->contains($l)) {
            $this->doAddApiGiantBombVideo($l);

            if ($this->apiGiantBombVideosScheduledForDeletion and $this->apiGiantBombVideosScheduledForDeletion->contains($l)) {
                $this->apiGiantBombVideosScheduledForDeletion->remove($this->apiGiantBombVideosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombVideo $apiGiantBombVideo The ChildApiGiantBombVideo object to add.
     */
    protected function doAddApiGiantBombVideo(ChildApiGiantBombVideo $apiGiantBombVideo)
    {
        $this->collApiGiantBombVideos[]= $apiGiantBombVideo;
        $apiGiantBombVideo->setApiGiantBombGameRelease($this);
    }

    /**
     * @param  ChildApiGiantBombVideo $apiGiantBombVideo The ChildApiGiantBombVideo object to remove.
     * @return $this|ChildApiGiantBombGameRelease The current object (for fluent API support)
     */
    public function removeApiGiantBombVideo(ChildApiGiantBombVideo $apiGiantBombVideo)
    {
        if ($this->getApiGiantBombVideos()->contains($apiGiantBombVideo)) {
            $pos = $this->collApiGiantBombVideos->search($apiGiantBombVideo);
            $this->collApiGiantBombVideos->remove($pos);
            if (null === $this->apiGiantBombVideosScheduledForDeletion) {
                $this->apiGiantBombVideosScheduledForDeletion = clone $this->collApiGiantBombVideos;
                $this->apiGiantBombVideosScheduledForDeletion->clear();
            }
            $this->apiGiantBombVideosScheduledForDeletion[]= $apiGiantBombVideo;
            $apiGiantBombVideo->setApiGiantBombGameRelease(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRelease is new, it will return
     * an empty collection; or if this ApiGiantBombGameRelease has previously
     * been saved, it will retrieve related ApiGiantBombVideos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRelease.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombVideo[] List of ChildApiGiantBombVideo objects
     */
    public function getApiGiantBombVideosJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombVideoQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombVideos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombGame) {
            $this->aApiGiantBombGame->removeApiGiantBombGameRelease($this);
        }
        if (null !== $this->aApiGiantBombPlatform) {
            $this->aApiGiantBombPlatform->removeApiGiantBombGameRelease($this);
        }
        if (null !== $this->aApiGiantBombGameRating) {
            $this->aApiGiantBombGameRating->removeApiGiantBombGameRelease($this);
        }
        if (null !== $this->aApiGiantBombRegion) {
            $this->aApiGiantBombRegion->removeApiGiantBombGameRelease($this);
        }
        $this->vgagrl_id = null;
        $this->vgagrl_vgagga_id = null;
        $this->vgagrl_vgagpl_id = null;
        $this->vgagrl_vgaggr_id = null;
        $this->vgagrl_vgagre_id = null;
        $this->vgaggl_name = null;
        $this->vgagrl_summary = null;
        $this->vgagrl_description = null;
        $this->vgagrl_minimum_players = null;
        $this->vgagrl_maximum_players = null;
        $this->vgagrl_widescreen_support = null;
        $this->vgagrl_product_code_value = null;
        $this->vgagrl_original_released_at = null;
        $this->vgagrl_expected_day_released_at = null;
        $this->vgagrl_expected_month_released_at = null;
        $this->vgagrl_expected_quarter_released_at = null;
        $this->vgagrl_expected_year_released_at = null;
        $this->vgagrl_image_icon_url = null;
        $this->vgagrl_image_medium_url = null;
        $this->vgagrl_image_screen_url = null;
        $this->vgagrl_image_small_url = null;
        $this->vgagrl_image_super_url = null;
        $this->vgagrl_image_thumb_url = null;
        $this->vgagrl_image_tiny_url = null;
        $this->vgagrl_api_detail_url = null;
        $this->vgagrl_site_detail_url = null;
        $this->vgagrl_created_at = null;
        $this->vgagrl_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombGameReleaseDevelopers) {
                foreach ($this->collApiGiantBombGameReleaseDevelopers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleasePublishers) {
                foreach ($this->collApiGiantBombGameReleasePublishers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombImages) {
                foreach ($this->collApiGiantBombImages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombVideos) {
                foreach ($this->collApiGiantBombVideos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombGameReleaseDevelopers = null;
        $this->collApiGiantBombGameReleasePublishers = null;
        $this->collApiGiantBombImages = null;
        $this->collApiGiantBombVideos = null;
        $this->aApiGiantBombGame = null;
        $this->aApiGiantBombPlatform = null;
        $this->aApiGiantBombGameRating = null;
        $this->aApiGiantBombRegion = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombGameReleaseTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
