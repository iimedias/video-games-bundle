<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper as ChildApiGiantBombGameDeveloper;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery as ChildApiGiantBombGameDeveloperQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameDeveloperTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_game_developer_vgaggd' table.
 *
 *
 *
 * @method     ChildApiGiantBombGameDeveloperQuery orderById($order = Criteria::ASC) Order by the vgaggd_id column
 * @method     ChildApiGiantBombGameDeveloperQuery orderByApiGiantBombGameId($order = Criteria::ASC) Order by the vgaggd_vgagga_id column
 * @method     ChildApiGiantBombGameDeveloperQuery orderByApiGiantBombCompanyId($order = Criteria::ASC) Order by the vgaggd_vgagco_id column
 *
 * @method     ChildApiGiantBombGameDeveloperQuery groupById() Group by the vgaggd_id column
 * @method     ChildApiGiantBombGameDeveloperQuery groupByApiGiantBombGameId() Group by the vgaggd_vgagga_id column
 * @method     ChildApiGiantBombGameDeveloperQuery groupByApiGiantBombCompanyId() Group by the vgaggd_vgagco_id column
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoinApiGiantBombGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoinApiGiantBombGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoinApiGiantBombGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameDeveloperQuery joinWithApiGiantBombGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoinWithApiGiantBombGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoinWithApiGiantBombGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoinWithApiGiantBombGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoinApiGiantBombCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoinApiGiantBombCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoinApiGiantBombCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombCompany relation
 *
 * @method     ChildApiGiantBombGameDeveloperQuery joinWithApiGiantBombCompany($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombCompany relation
 *
 * @method     ChildApiGiantBombGameDeveloperQuery leftJoinWithApiGiantBombCompany() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombGameDeveloperQuery rightJoinWithApiGiantBombCompany() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombGameDeveloperQuery innerJoinWithApiGiantBombCompany() Adds a INNER JOIN clause and with to the query using the ApiGiantBombCompany relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombGameDeveloper findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameDeveloper matching the query
 * @method     ChildApiGiantBombGameDeveloper findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameDeveloper matching the query, or a new ChildApiGiantBombGameDeveloper object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombGameDeveloper findOneById(int $vgaggd_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_id column
 * @method     ChildApiGiantBombGameDeveloper findOneByApiGiantBombGameId(int $vgaggd_vgagga_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_vgagga_id column
 * @method     ChildApiGiantBombGameDeveloper findOneByApiGiantBombCompanyId(int $vgaggd_vgagco_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_vgagco_id column *

 * @method     ChildApiGiantBombGameDeveloper requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombGameDeveloper by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameDeveloper requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameDeveloper matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameDeveloper requireOneById(int $vgaggd_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameDeveloper requireOneByApiGiantBombGameId(int $vgaggd_vgagga_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameDeveloper requireOneByApiGiantBombCompanyId(int $vgaggd_vgagco_id) Return the first ChildApiGiantBombGameDeveloper filtered by the vgaggd_vgagco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameDeveloper[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombGameDeveloper objects based on current ModelCriteria
 * @method     ChildApiGiantBombGameDeveloper[]|ObjectCollection findById(int $vgaggd_id) Return ChildApiGiantBombGameDeveloper objects filtered by the vgaggd_id column
 * @method     ChildApiGiantBombGameDeveloper[]|ObjectCollection findByApiGiantBombGameId(int $vgaggd_vgagga_id) Return ChildApiGiantBombGameDeveloper objects filtered by the vgaggd_vgagga_id column
 * @method     ChildApiGiantBombGameDeveloper[]|ObjectCollection findByApiGiantBombCompanyId(int $vgaggd_vgagco_id) Return ChildApiGiantBombGameDeveloper objects filtered by the vgaggd_vgagco_id column
 * @method     ChildApiGiantBombGameDeveloper[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombGameDeveloperQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameDeveloperQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameDeveloper', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombGameDeveloperQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombGameDeveloperQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombGameDeveloperQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombGameDeveloperQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombGameDeveloper|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameDeveloperTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombGameDeveloperTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameDeveloper A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaggd_id, vgaggd_vgagga_id, vgaggd_vgagco_id FROM videogames_api_giantbomb_game_developer_vgaggd WHERE vgaggd_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombGameDeveloper $obj */
            $obj = new ChildApiGiantBombGameDeveloper();
            $obj->hydrate($row);
            ApiGiantBombGameDeveloperTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombGameDeveloper|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaggd_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaggd_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaggd_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaggd_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaggd_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombGameId(1234); // WHERE vgaggd_vgagga_id = 1234
     * $query->filterByApiGiantBombGameId(array(12, 34)); // WHERE vgaggd_vgagga_id IN (12, 34)
     * $query->filterByApiGiantBombGameId(array('min' => 12)); // WHERE vgaggd_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombGame()
     *
     * @param     mixed $apiGiantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameId($apiGiantBombGameId = null, $comparison = null)
    {
        if (is_array($apiGiantBombGameId)) {
            $useMinMax = false;
            if (isset($apiGiantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGGA_ID, $apiGiantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGGA_ID, $apiGiantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGGA_ID, $apiGiantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgaggd_vgagco_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombCompanyId(1234); // WHERE vgaggd_vgagco_id = 1234
     * $query->filterByApiGiantBombCompanyId(array(12, 34)); // WHERE vgaggd_vgagco_id IN (12, 34)
     * $query->filterByApiGiantBombCompanyId(array('min' => 12)); // WHERE vgaggd_vgagco_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombCompany()
     *
     * @param     mixed $apiGiantBombCompanyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCompanyId($apiGiantBombCompanyId = null, $comparison = null)
    {
        if (is_array($apiGiantBombCompanyId)) {
            $useMinMax = false;
            if (isset($apiGiantBombCompanyId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGCO_ID, $apiGiantBombCompanyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombCompanyId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGCO_ID, $apiGiantBombCompanyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGCO_ID, $apiGiantBombCompanyId, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany|ObjectCollection $apiGiantBombCompany The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCompany($apiGiantBombCompany, $comparison = null)
    {
        if ($apiGiantBombCompany instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany) {
            return $this
                ->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGCO_ID, $apiGiantBombCompany->getId(), $comparison);
        } elseif ($apiGiantBombCompany instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_VGAGCO_ID, $apiGiantBombCompany->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombCompany() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombCompany relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function joinApiGiantBombCompany($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombCompany');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombCompany');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombCompany relation ApiGiantBombCompany object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombCompanyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombCompany', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombGameDeveloperQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombGameDeveloper = null)
    {
        if ($apiGiantBombGameDeveloper) {
            $this->addUsingAlias(ApiGiantBombGameDeveloperTableMap::COL_VGAGGD_ID, $apiGiantBombGameDeveloper->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_game_developer_vgaggd table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameDeveloperTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombGameDeveloperTableMap::clearInstancePool();
            ApiGiantBombGameDeveloperTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameDeveloperTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombGameDeveloperTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombGameDeveloperTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombGameDeveloperTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombGameDeveloperQuery
