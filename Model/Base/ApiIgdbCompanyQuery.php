<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompany as ChildApiIgdbCompany;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompanyQuery as ChildApiIgdbCompanyQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiIgdbCompanyTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_igdb_company_vgaico' table.
 *
 *
 *
 * @method     ChildApiIgdbCompanyQuery orderById($order = Criteria::ASC) Order by the vgaico_id column
 * @method     ChildApiIgdbCompanyQuery orderByParentId($order = Criteria::ASC) Order by the vgaico_parent_id column
 * @method     ChildApiIgdbCompanyQuery orderByName($order = Criteria::ASC) Order by the vgaico_name column
 * @method     ChildApiIgdbCompanyQuery orderBySlug($order = Criteria::ASC) Order by the vgaico_slug column
 * @method     ChildApiIgdbCompanyQuery orderByUrl($order = Criteria::ASC) Order by the vgaico_url column
 * @method     ChildApiIgdbCompanyQuery orderByWebSite($order = Criteria::ASC) Order by the vgaico_website column
 * @method     ChildApiIgdbCompanyQuery orderByTwitter($order = Criteria::ASC) Order by the vgaico_twitter column
 * @method     ChildApiIgdbCompanyQuery orderByCountry($order = Criteria::ASC) Order by the vgaico_country column
 * @method     ChildApiIgdbCompanyQuery orderByLogoCloudinaryId($order = Criteria::ASC) Order by the vgaico_logo_cloudinary_id column
 * @method     ChildApiIgdbCompanyQuery orderByLogoWidth($order = Criteria::ASC) Order by the vgaico_logo_width column
 * @method     ChildApiIgdbCompanyQuery orderByLogoHeight($order = Criteria::ASC) Order by the vgaico_logo_height column
 * @method     ChildApiIgdbCompanyQuery orderByDescription($order = Criteria::ASC) Order by the vgaico_description column
 * @method     ChildApiIgdbCompanyQuery orderByDeveloped($order = Criteria::ASC) Order by the vgaico_developed column
 * @method     ChildApiIgdbCompanyQuery orderByPublished($order = Criteria::ASC) Order by the vgaico_published column
 * @method     ChildApiIgdbCompanyQuery orderByStartDate($order = Criteria::ASC) Order by the vgaico_start_date column
 * @method     ChildApiIgdbCompanyQuery orderByStartDateCategory($order = Criteria::ASC) Order by the vgaico_start_date_category column
 * @method     ChildApiIgdbCompanyQuery orderByChangeDate($order = Criteria::ASC) Order by the vgaico_change_date column
 * @method     ChildApiIgdbCompanyQuery orderByChangeDateCategory($order = Criteria::ASC) Order by the vgaico_change_date_category column
 * @method     ChildApiIgdbCompanyQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgaico_created_at column
 * @method     ChildApiIgdbCompanyQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgaico_updated_at column
 *
 * @method     ChildApiIgdbCompanyQuery groupById() Group by the vgaico_id column
 * @method     ChildApiIgdbCompanyQuery groupByParentId() Group by the vgaico_parent_id column
 * @method     ChildApiIgdbCompanyQuery groupByName() Group by the vgaico_name column
 * @method     ChildApiIgdbCompanyQuery groupBySlug() Group by the vgaico_slug column
 * @method     ChildApiIgdbCompanyQuery groupByUrl() Group by the vgaico_url column
 * @method     ChildApiIgdbCompanyQuery groupByWebSite() Group by the vgaico_website column
 * @method     ChildApiIgdbCompanyQuery groupByTwitter() Group by the vgaico_twitter column
 * @method     ChildApiIgdbCompanyQuery groupByCountry() Group by the vgaico_country column
 * @method     ChildApiIgdbCompanyQuery groupByLogoCloudinaryId() Group by the vgaico_logo_cloudinary_id column
 * @method     ChildApiIgdbCompanyQuery groupByLogoWidth() Group by the vgaico_logo_width column
 * @method     ChildApiIgdbCompanyQuery groupByLogoHeight() Group by the vgaico_logo_height column
 * @method     ChildApiIgdbCompanyQuery groupByDescription() Group by the vgaico_description column
 * @method     ChildApiIgdbCompanyQuery groupByDeveloped() Group by the vgaico_developed column
 * @method     ChildApiIgdbCompanyQuery groupByPublished() Group by the vgaico_published column
 * @method     ChildApiIgdbCompanyQuery groupByStartDate() Group by the vgaico_start_date column
 * @method     ChildApiIgdbCompanyQuery groupByStartDateCategory() Group by the vgaico_start_date_category column
 * @method     ChildApiIgdbCompanyQuery groupByChangeDate() Group by the vgaico_change_date column
 * @method     ChildApiIgdbCompanyQuery groupByChangeDateCategory() Group by the vgaico_change_date_category column
 * @method     ChildApiIgdbCompanyQuery groupByCreatedAt() Group by the vgaico_created_at column
 * @method     ChildApiIgdbCompanyQuery groupByUpdatedAt() Group by the vgaico_updated_at column
 *
 * @method     ChildApiIgdbCompanyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiIgdbCompanyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiIgdbCompanyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiIgdbCompanyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiIgdbCompanyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiIgdbCompanyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiIgdbCompany findOne(ConnectionInterface $con = null) Return the first ChildApiIgdbCompany matching the query
 * @method     ChildApiIgdbCompany findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiIgdbCompany matching the query, or a new ChildApiIgdbCompany object populated from the query conditions when no match is found
 *
 * @method     ChildApiIgdbCompany findOneById(int $vgaico_id) Return the first ChildApiIgdbCompany filtered by the vgaico_id column
 * @method     ChildApiIgdbCompany findOneByParentId(int $vgaico_parent_id) Return the first ChildApiIgdbCompany filtered by the vgaico_parent_id column
 * @method     ChildApiIgdbCompany findOneByName(string $vgaico_name) Return the first ChildApiIgdbCompany filtered by the vgaico_name column
 * @method     ChildApiIgdbCompany findOneBySlug(string $vgaico_slug) Return the first ChildApiIgdbCompany filtered by the vgaico_slug column
 * @method     ChildApiIgdbCompany findOneByUrl(string $vgaico_url) Return the first ChildApiIgdbCompany filtered by the vgaico_url column
 * @method     ChildApiIgdbCompany findOneByWebSite(string $vgaico_website) Return the first ChildApiIgdbCompany filtered by the vgaico_website column
 * @method     ChildApiIgdbCompany findOneByTwitter(string $vgaico_twitter) Return the first ChildApiIgdbCompany filtered by the vgaico_twitter column
 * @method     ChildApiIgdbCompany findOneByCountry(int $vgaico_country) Return the first ChildApiIgdbCompany filtered by the vgaico_country column
 * @method     ChildApiIgdbCompany findOneByLogoCloudinaryId(string $vgaico_logo_cloudinary_id) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_cloudinary_id column
 * @method     ChildApiIgdbCompany findOneByLogoWidth(int $vgaico_logo_width) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_width column
 * @method     ChildApiIgdbCompany findOneByLogoHeight(int $vgaico_logo_height) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_height column
 * @method     ChildApiIgdbCompany findOneByDescription(string $vgaico_description) Return the first ChildApiIgdbCompany filtered by the vgaico_description column
 * @method     ChildApiIgdbCompany findOneByDeveloped(array $vgaico_developed) Return the first ChildApiIgdbCompany filtered by the vgaico_developed column
 * @method     ChildApiIgdbCompany findOneByPublished(array $vgaico_published) Return the first ChildApiIgdbCompany filtered by the vgaico_published column
 * @method     ChildApiIgdbCompany findOneByStartDate(string $vgaico_start_date) Return the first ChildApiIgdbCompany filtered by the vgaico_start_date column
 * @method     ChildApiIgdbCompany findOneByStartDateCategory(int $vgaico_start_date_category) Return the first ChildApiIgdbCompany filtered by the vgaico_start_date_category column
 * @method     ChildApiIgdbCompany findOneByChangeDate(string $vgaico_change_date) Return the first ChildApiIgdbCompany filtered by the vgaico_change_date column
 * @method     ChildApiIgdbCompany findOneByChangeDateCategory(int $vgaico_change_date_category) Return the first ChildApiIgdbCompany filtered by the vgaico_change_date_category column
 * @method     ChildApiIgdbCompany findOneByCreatedAt(string $vgaico_created_at) Return the first ChildApiIgdbCompany filtered by the vgaico_created_at column
 * @method     ChildApiIgdbCompany findOneByUpdatedAt(string $vgaico_updated_at) Return the first ChildApiIgdbCompany filtered by the vgaico_updated_at column *

 * @method     ChildApiIgdbCompany requirePk($key, ConnectionInterface $con = null) Return the ChildApiIgdbCompany by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOne(ConnectionInterface $con = null) Return the first ChildApiIgdbCompany matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiIgdbCompany requireOneById(int $vgaico_id) Return the first ChildApiIgdbCompany filtered by the vgaico_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByParentId(int $vgaico_parent_id) Return the first ChildApiIgdbCompany filtered by the vgaico_parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByName(string $vgaico_name) Return the first ChildApiIgdbCompany filtered by the vgaico_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneBySlug(string $vgaico_slug) Return the first ChildApiIgdbCompany filtered by the vgaico_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByUrl(string $vgaico_url) Return the first ChildApiIgdbCompany filtered by the vgaico_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByWebSite(string $vgaico_website) Return the first ChildApiIgdbCompany filtered by the vgaico_website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByTwitter(string $vgaico_twitter) Return the first ChildApiIgdbCompany filtered by the vgaico_twitter column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByCountry(int $vgaico_country) Return the first ChildApiIgdbCompany filtered by the vgaico_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByLogoCloudinaryId(string $vgaico_logo_cloudinary_id) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_cloudinary_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByLogoWidth(int $vgaico_logo_width) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_width column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByLogoHeight(int $vgaico_logo_height) Return the first ChildApiIgdbCompany filtered by the vgaico_logo_height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByDescription(string $vgaico_description) Return the first ChildApiIgdbCompany filtered by the vgaico_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByDeveloped(array $vgaico_developed) Return the first ChildApiIgdbCompany filtered by the vgaico_developed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByPublished(array $vgaico_published) Return the first ChildApiIgdbCompany filtered by the vgaico_published column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByStartDate(string $vgaico_start_date) Return the first ChildApiIgdbCompany filtered by the vgaico_start_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByStartDateCategory(int $vgaico_start_date_category) Return the first ChildApiIgdbCompany filtered by the vgaico_start_date_category column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByChangeDate(string $vgaico_change_date) Return the first ChildApiIgdbCompany filtered by the vgaico_change_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByChangeDateCategory(int $vgaico_change_date_category) Return the first ChildApiIgdbCompany filtered by the vgaico_change_date_category column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByCreatedAt(string $vgaico_created_at) Return the first ChildApiIgdbCompany filtered by the vgaico_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbCompany requireOneByUpdatedAt(string $vgaico_updated_at) Return the first ChildApiIgdbCompany filtered by the vgaico_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiIgdbCompany[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiIgdbCompany objects based on current ModelCriteria
 * @method     ChildApiIgdbCompany[]|ObjectCollection findById(int $vgaico_id) Return ChildApiIgdbCompany objects filtered by the vgaico_id column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByParentId(int $vgaico_parent_id) Return ChildApiIgdbCompany objects filtered by the vgaico_parent_id column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByName(string $vgaico_name) Return ChildApiIgdbCompany objects filtered by the vgaico_name column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findBySlug(string $vgaico_slug) Return ChildApiIgdbCompany objects filtered by the vgaico_slug column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByUrl(string $vgaico_url) Return ChildApiIgdbCompany objects filtered by the vgaico_url column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByWebSite(string $vgaico_website) Return ChildApiIgdbCompany objects filtered by the vgaico_website column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByTwitter(string $vgaico_twitter) Return ChildApiIgdbCompany objects filtered by the vgaico_twitter column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByCountry(int $vgaico_country) Return ChildApiIgdbCompany objects filtered by the vgaico_country column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByLogoCloudinaryId(string $vgaico_logo_cloudinary_id) Return ChildApiIgdbCompany objects filtered by the vgaico_logo_cloudinary_id column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByLogoWidth(int $vgaico_logo_width) Return ChildApiIgdbCompany objects filtered by the vgaico_logo_width column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByLogoHeight(int $vgaico_logo_height) Return ChildApiIgdbCompany objects filtered by the vgaico_logo_height column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByDescription(string $vgaico_description) Return ChildApiIgdbCompany objects filtered by the vgaico_description column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByDeveloped(array $vgaico_developed) Return ChildApiIgdbCompany objects filtered by the vgaico_developed column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByPublished(array $vgaico_published) Return ChildApiIgdbCompany objects filtered by the vgaico_published column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByStartDate(string $vgaico_start_date) Return ChildApiIgdbCompany objects filtered by the vgaico_start_date column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByStartDateCategory(int $vgaico_start_date_category) Return ChildApiIgdbCompany objects filtered by the vgaico_start_date_category column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByChangeDate(string $vgaico_change_date) Return ChildApiIgdbCompany objects filtered by the vgaico_change_date column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByChangeDateCategory(int $vgaico_change_date_category) Return ChildApiIgdbCompany objects filtered by the vgaico_change_date_category column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByCreatedAt(string $vgaico_created_at) Return ChildApiIgdbCompany objects filtered by the vgaico_created_at column
 * @method     ChildApiIgdbCompany[]|ObjectCollection findByUpdatedAt(string $vgaico_updated_at) Return ChildApiIgdbCompany objects filtered by the vgaico_updated_at column
 * @method     ChildApiIgdbCompany[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiIgdbCompanyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiIgdbCompanyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbCompany', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiIgdbCompanyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiIgdbCompanyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiIgdbCompanyQuery) {
            return $criteria;
        }
        $query = new ChildApiIgdbCompanyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiIgdbCompany|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiIgdbCompanyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiIgdbCompany A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaico_id, vgaico_parent_id, vgaico_name, vgaico_slug, vgaico_url, vgaico_website, vgaico_twitter, vgaico_country, vgaico_logo_cloudinary_id, vgaico_logo_width, vgaico_logo_height, vgaico_description, vgaico_developed, vgaico_published, vgaico_start_date, vgaico_start_date_category, vgaico_change_date, vgaico_change_date_category, vgaico_created_at, vgaico_updated_at FROM videogames_api_igdb_company_vgaico WHERE vgaico_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiIgdbCompany $obj */
            $obj = new ChildApiIgdbCompany();
            $obj->hydrate($row);
            ApiIgdbCompanyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiIgdbCompany|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaico_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaico_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaico_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaico_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaico_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE vgaico_parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE vgaico_parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE vgaico_parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the vgaico_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaico_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaico_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgaico_slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE vgaico_slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE vgaico_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the vgaico_url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE vgaico_url = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE vgaico_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_URL, $url, $comparison);
    }

    /**
     * Filter the query on the vgaico_website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebSite('fooValue');   // WHERE vgaico_website = 'fooValue'
     * $query->filterByWebSite('%fooValue%'); // WHERE vgaico_website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $webSite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByWebSite($webSite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($webSite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE, $webSite, $comparison);
    }

    /**
     * Filter the query on the vgaico_twitter column
     *
     * Example usage:
     * <code>
     * $query->filterByTwitter('fooValue');   // WHERE vgaico_twitter = 'fooValue'
     * $query->filterByTwitter('%fooValue%'); // WHERE vgaico_twitter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $twitter The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByTwitter($twitter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($twitter)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER, $twitter, $comparison);
    }

    /**
     * Filter the query on the vgaico_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry(1234); // WHERE vgaico_country = 1234
     * $query->filterByCountry(array(12, 34)); // WHERE vgaico_country IN (12, 34)
     * $query->filterByCountry(array('min' => 12)); // WHERE vgaico_country > 12
     * </code>
     *
     * @param     mixed $country The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (is_array($country)) {
            $useMinMax = false;
            if (isset($country['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY, $country['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($country['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY, $country['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the vgaico_logo_cloudinary_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLogoCloudinaryId('fooValue');   // WHERE vgaico_logo_cloudinary_id = 'fooValue'
     * $query->filterByLogoCloudinaryId('%fooValue%'); // WHERE vgaico_logo_cloudinary_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logoCloudinaryId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByLogoCloudinaryId($logoCloudinaryId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logoCloudinaryId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID, $logoCloudinaryId, $comparison);
    }

    /**
     * Filter the query on the vgaico_logo_width column
     *
     * Example usage:
     * <code>
     * $query->filterByLogoWidth(1234); // WHERE vgaico_logo_width = 1234
     * $query->filterByLogoWidth(array(12, 34)); // WHERE vgaico_logo_width IN (12, 34)
     * $query->filterByLogoWidth(array('min' => 12)); // WHERE vgaico_logo_width > 12
     * </code>
     *
     * @param     mixed $logoWidth The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByLogoWidth($logoWidth = null, $comparison = null)
    {
        if (is_array($logoWidth)) {
            $useMinMax = false;
            if (isset($logoWidth['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH, $logoWidth['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logoWidth['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH, $logoWidth['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH, $logoWidth, $comparison);
    }

    /**
     * Filter the query on the vgaico_logo_height column
     *
     * Example usage:
     * <code>
     * $query->filterByLogoHeight(1234); // WHERE vgaico_logo_height = 1234
     * $query->filterByLogoHeight(array(12, 34)); // WHERE vgaico_logo_height IN (12, 34)
     * $query->filterByLogoHeight(array('min' => 12)); // WHERE vgaico_logo_height > 12
     * </code>
     *
     * @param     mixed $logoHeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByLogoHeight($logoHeight = null, $comparison = null)
    {
        if (is_array($logoHeight)) {
            $useMinMax = false;
            if (isset($logoHeight['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT, $logoHeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($logoHeight['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT, $logoHeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT, $logoHeight, $comparison);
    }

    /**
     * Filter the query on the vgaico_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgaico_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgaico_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgaico_developed column
     *
     * @param     array $developed The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByDeveloped($developed = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($developed as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($developed as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($developed as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED, $developed, $comparison);
    }

    /**
     * Filter the query on the vgaico_published column
     *
     * @param     array $published The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByPublished($published = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($published as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($published as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($published as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED, $published, $comparison);
    }

    /**
     * Filter the query on the vgaico_start_date column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDate('2011-03-14'); // WHERE vgaico_start_date = '2011-03-14'
     * $query->filterByStartDate('now'); // WHERE vgaico_start_date = '2011-03-14'
     * $query->filterByStartDate(array('max' => 'yesterday')); // WHERE vgaico_start_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $startDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByStartDate($startDate = null, $comparison = null)
    {
        if (is_array($startDate)) {
            $useMinMax = false;
            if (isset($startDate['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE, $startDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startDate['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE, $startDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE, $startDate, $comparison);
    }

    /**
     * Filter the query on the vgaico_start_date_category column
     *
     * Example usage:
     * <code>
     * $query->filterByStartDateCategory(1234); // WHERE vgaico_start_date_category = 1234
     * $query->filterByStartDateCategory(array(12, 34)); // WHERE vgaico_start_date_category IN (12, 34)
     * $query->filterByStartDateCategory(array('min' => 12)); // WHERE vgaico_start_date_category > 12
     * </code>
     *
     * @param     mixed $startDateCategory The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByStartDateCategory($startDateCategory = null, $comparison = null)
    {
        if (is_array($startDateCategory)) {
            $useMinMax = false;
            if (isset($startDateCategory['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY, $startDateCategory['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startDateCategory['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY, $startDateCategory['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY, $startDateCategory, $comparison);
    }

    /**
     * Filter the query on the vgaico_change_date column
     *
     * Example usage:
     * <code>
     * $query->filterByChangeDate('2011-03-14'); // WHERE vgaico_change_date = '2011-03-14'
     * $query->filterByChangeDate('now'); // WHERE vgaico_change_date = '2011-03-14'
     * $query->filterByChangeDate(array('max' => 'yesterday')); // WHERE vgaico_change_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $changeDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByChangeDate($changeDate = null, $comparison = null)
    {
        if (is_array($changeDate)) {
            $useMinMax = false;
            if (isset($changeDate['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE, $changeDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($changeDate['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE, $changeDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE, $changeDate, $comparison);
    }

    /**
     * Filter the query on the vgaico_change_date_category column
     *
     * Example usage:
     * <code>
     * $query->filterByChangeDateCategory(1234); // WHERE vgaico_change_date_category = 1234
     * $query->filterByChangeDateCategory(array(12, 34)); // WHERE vgaico_change_date_category IN (12, 34)
     * $query->filterByChangeDateCategory(array('min' => 12)); // WHERE vgaico_change_date_category > 12
     * </code>
     *
     * @param     mixed $changeDateCategory The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByChangeDateCategory($changeDateCategory = null, $comparison = null)
    {
        if (is_array($changeDateCategory)) {
            $useMinMax = false;
            if (isset($changeDateCategory['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY, $changeDateCategory['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($changeDateCategory['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY, $changeDateCategory['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY, $changeDateCategory, $comparison);
    }

    /**
     * Filter the query on the vgaico_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgaico_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgaico_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgaico_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgaico_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgaico_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgaico_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgaico_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiIgdbCompany $apiIgdbCompany Object to remove from the list of results
     *
     * @return $this|ChildApiIgdbCompanyQuery The current query, for fluid interface
     */
    public function prune($apiIgdbCompany = null)
    {
        if ($apiIgdbCompany) {
            $this->addUsingAlias(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $apiIgdbCompany->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_igdb_company_vgaico table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiIgdbCompanyTableMap::clearInstancePool();
            ApiIgdbCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiIgdbCompanyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiIgdbCompanyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiIgdbCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiIgdbCompanyQuery
