<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion as ChildApiGiantBombRegion;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery as ChildApiGiantBombRegionQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombRegionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_region_vgagre' table.
 *
 *
 *
 * @method     ChildApiGiantBombRegionQuery orderById($order = Criteria::ASC) Order by the vgagre_id column
 * @method     ChildApiGiantBombRegionQuery orderByName($order = Criteria::ASC) Order by the vgagre_name column
 * @method     ChildApiGiantBombRegionQuery orderBySummary($order = Criteria::ASC) Order by the vgagre_summary column
 * @method     ChildApiGiantBombRegionQuery orderByDescription($order = Criteria::ASC) Order by the vgagre_description column
 * @method     ChildApiGiantBombRegionQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagre_api_detail_url column
 * @method     ChildApiGiantBombRegionQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagre_site_detail_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagre_image_icon_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagre_image_medium_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagre_image_screen_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagre_image_small_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagre_image_super_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagre_image_thumb_url column
 * @method     ChildApiGiantBombRegionQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagre_image_tiny_url column
 * @method     ChildApiGiantBombRegionQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagre_created_at column
 * @method     ChildApiGiantBombRegionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagre_updated_at column
 *
 * @method     ChildApiGiantBombRegionQuery groupById() Group by the vgagre_id column
 * @method     ChildApiGiantBombRegionQuery groupByName() Group by the vgagre_name column
 * @method     ChildApiGiantBombRegionQuery groupBySummary() Group by the vgagre_summary column
 * @method     ChildApiGiantBombRegionQuery groupByDescription() Group by the vgagre_description column
 * @method     ChildApiGiantBombRegionQuery groupByApiDetailUrl() Group by the vgagre_api_detail_url column
 * @method     ChildApiGiantBombRegionQuery groupBySiteDetailUrl() Group by the vgagre_site_detail_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageIconUrl() Group by the vgagre_image_icon_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageMediumUrl() Group by the vgagre_image_medium_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageScreenUrl() Group by the vgagre_image_screen_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageSmallUrl() Group by the vgagre_image_small_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageSuperUrl() Group by the vgagre_image_super_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageThumbUrl() Group by the vgagre_image_thumb_url column
 * @method     ChildApiGiantBombRegionQuery groupByImageTinyUrl() Group by the vgagre_image_tiny_url column
 * @method     ChildApiGiantBombRegionQuery groupByCreatedAt() Group by the vgagre_created_at column
 * @method     ChildApiGiantBombRegionQuery groupByUpdatedAt() Group by the vgagre_updated_at column
 *
 * @method     ChildApiGiantBombRegionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombRegionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombRegionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombRegionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombRegionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombRegionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombRegionQuery leftJoinApiGiantBombGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombRegionQuery rightJoinApiGiantBombGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombRegionQuery innerJoinApiGiantBombGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombRegionQuery joinWithApiGiantBombGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombRegionQuery leftJoinWithApiGiantBombGameRelease() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombRegionQuery rightJoinWithApiGiantBombGameRelease() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombRegionQuery innerJoinWithApiGiantBombGameRelease() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombRegion findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombRegion matching the query
 * @method     ChildApiGiantBombRegion findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombRegion matching the query, or a new ChildApiGiantBombRegion object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombRegion findOneById(int $vgagre_id) Return the first ChildApiGiantBombRegion filtered by the vgagre_id column
 * @method     ChildApiGiantBombRegion findOneByName(string $vgagre_name) Return the first ChildApiGiantBombRegion filtered by the vgagre_name column
 * @method     ChildApiGiantBombRegion findOneBySummary(string $vgagre_summary) Return the first ChildApiGiantBombRegion filtered by the vgagre_summary column
 * @method     ChildApiGiantBombRegion findOneByDescription(string $vgagre_description) Return the first ChildApiGiantBombRegion filtered by the vgagre_description column
 * @method     ChildApiGiantBombRegion findOneByApiDetailUrl(string $vgagre_api_detail_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_api_detail_url column
 * @method     ChildApiGiantBombRegion findOneBySiteDetailUrl(string $vgagre_site_detail_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_site_detail_url column
 * @method     ChildApiGiantBombRegion findOneByImageIconUrl(string $vgagre_image_icon_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_icon_url column
 * @method     ChildApiGiantBombRegion findOneByImageMediumUrl(string $vgagre_image_medium_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_medium_url column
 * @method     ChildApiGiantBombRegion findOneByImageScreenUrl(string $vgagre_image_screen_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_screen_url column
 * @method     ChildApiGiantBombRegion findOneByImageSmallUrl(string $vgagre_image_small_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_small_url column
 * @method     ChildApiGiantBombRegion findOneByImageSuperUrl(string $vgagre_image_super_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_super_url column
 * @method     ChildApiGiantBombRegion findOneByImageThumbUrl(string $vgagre_image_thumb_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_thumb_url column
 * @method     ChildApiGiantBombRegion findOneByImageTinyUrl(string $vgagre_image_tiny_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_tiny_url column
 * @method     ChildApiGiantBombRegion findOneByCreatedAt(string $vgagre_created_at) Return the first ChildApiGiantBombRegion filtered by the vgagre_created_at column
 * @method     ChildApiGiantBombRegion findOneByUpdatedAt(string $vgagre_updated_at) Return the first ChildApiGiantBombRegion filtered by the vgagre_updated_at column *

 * @method     ChildApiGiantBombRegion requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombRegion by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombRegion matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombRegion requireOneById(int $vgagre_id) Return the first ChildApiGiantBombRegion filtered by the vgagre_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByName(string $vgagre_name) Return the first ChildApiGiantBombRegion filtered by the vgagre_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneBySummary(string $vgagre_summary) Return the first ChildApiGiantBombRegion filtered by the vgagre_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByDescription(string $vgagre_description) Return the first ChildApiGiantBombRegion filtered by the vgagre_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByApiDetailUrl(string $vgagre_api_detail_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneBySiteDetailUrl(string $vgagre_site_detail_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageIconUrl(string $vgagre_image_icon_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageMediumUrl(string $vgagre_image_medium_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageScreenUrl(string $vgagre_image_screen_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageSmallUrl(string $vgagre_image_small_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageSuperUrl(string $vgagre_image_super_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageThumbUrl(string $vgagre_image_thumb_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByImageTinyUrl(string $vgagre_image_tiny_url) Return the first ChildApiGiantBombRegion filtered by the vgagre_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByCreatedAt(string $vgagre_created_at) Return the first ChildApiGiantBombRegion filtered by the vgagre_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRegion requireOneByUpdatedAt(string $vgagre_updated_at) Return the first ChildApiGiantBombRegion filtered by the vgagre_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombRegion[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombRegion objects based on current ModelCriteria
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findById(int $vgagre_id) Return ChildApiGiantBombRegion objects filtered by the vgagre_id column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByName(string $vgagre_name) Return ChildApiGiantBombRegion objects filtered by the vgagre_name column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findBySummary(string $vgagre_summary) Return ChildApiGiantBombRegion objects filtered by the vgagre_summary column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByDescription(string $vgagre_description) Return ChildApiGiantBombRegion objects filtered by the vgagre_description column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByApiDetailUrl(string $vgagre_api_detail_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_api_detail_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findBySiteDetailUrl(string $vgagre_site_detail_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_site_detail_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageIconUrl(string $vgagre_image_icon_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_icon_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageMediumUrl(string $vgagre_image_medium_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_medium_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageScreenUrl(string $vgagre_image_screen_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_screen_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageSmallUrl(string $vgagre_image_small_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_small_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageSuperUrl(string $vgagre_image_super_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_super_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageThumbUrl(string $vgagre_image_thumb_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_thumb_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByImageTinyUrl(string $vgagre_image_tiny_url) Return ChildApiGiantBombRegion objects filtered by the vgagre_image_tiny_url column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByCreatedAt(string $vgagre_created_at) Return ChildApiGiantBombRegion objects filtered by the vgagre_created_at column
 * @method     ChildApiGiantBombRegion[]|ObjectCollection findByUpdatedAt(string $vgagre_updated_at) Return ChildApiGiantBombRegion objects filtered by the vgagre_updated_at column
 * @method     ChildApiGiantBombRegion[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombRegionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombRegionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRegion', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombRegionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombRegionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombRegionQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombRegionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombRegion|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombRegionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombRegionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombRegion A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagre_id, vgagre_name, vgagre_summary, vgagre_description, vgagre_api_detail_url, vgagre_site_detail_url, vgagre_image_icon_url, vgagre_image_medium_url, vgagre_image_screen_url, vgagre_image_small_url, vgagre_image_super_url, vgagre_image_thumb_url, vgagre_image_tiny_url, vgagre_created_at, vgagre_updated_at FROM videogames_api_giantbomb_region_vgagre WHERE vgagre_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombRegion $obj */
            $obj = new ChildApiGiantBombRegion();
            $obj->hydrate($row);
            ApiGiantBombRegionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombRegion|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagre_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagre_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagre_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagre_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagre_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagre_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagre_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagre_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagre_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagre_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagre_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagre_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagre_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagre_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagre_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagre_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagre_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagre_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagre_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagre_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagre_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagre_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagre_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagre_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagre_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagre_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagre_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagre_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagre_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagre_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagre_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagre_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagre_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagre_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagre_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagre_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagre_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagre_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagre_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagre_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease|ObjectCollection $apiGiantBombGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRelease($apiGiantBombGameRelease, $comparison = null)
    {
        if ($apiGiantBombGameRelease instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease) {
            return $this
                ->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $apiGiantBombGameRelease->getApiGiantBombRegionId(), $comparison);
        } elseif ($apiGiantBombGameRelease instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameRelease($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameRelease');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameRelease relation ApiGiantBombGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameRelease', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombRegion $apiGiantBombRegion Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombRegionQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombRegion = null)
    {
        if ($apiGiantBombRegion) {
            $this->addUsingAlias(ApiGiantBombRegionTableMap::COL_VGAGRE_ID, $apiGiantBombRegion->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_region_vgagre table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRegionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombRegionTableMap::clearInstancePool();
            ApiGiantBombRegionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRegionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombRegionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombRegionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombRegionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombRegionQuery
