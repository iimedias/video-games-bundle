<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGame as ChildSiteGameBlogGame;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery as ChildSiteGameBlogGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameBlogGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_gameblog_game_vgabga' table.
 *
 *
 *
 * @method     ChildSiteGameBlogGameQuery orderById($order = Criteria::ASC) Order by the vgabga_id column
 * @method     ChildSiteGameBlogGameQuery orderByGameId($order = Criteria::ASC) Order by the vgabga_vgagam_id column
 * @method     ChildSiteGameBlogGameQuery orderByName($order = Criteria::ASC) Order by the vgabga_name column
 * @method     ChildSiteGameBlogGameQuery orderByReleasedAt($order = Criteria::ASC) Order by the vgabga_released_at column
 * @method     ChildSiteGameBlogGameQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgabga_site_detail_url column
 * @method     ChildSiteGameBlogGameQuery orderByRefreshedAt($order = Criteria::ASC) Order by the vgakga_refreshed_at column
 *
 * @method     ChildSiteGameBlogGameQuery groupById() Group by the vgabga_id column
 * @method     ChildSiteGameBlogGameQuery groupByGameId() Group by the vgabga_vgagam_id column
 * @method     ChildSiteGameBlogGameQuery groupByName() Group by the vgabga_name column
 * @method     ChildSiteGameBlogGameQuery groupByReleasedAt() Group by the vgabga_released_at column
 * @method     ChildSiteGameBlogGameQuery groupBySiteDetailUrl() Group by the vgabga_site_detail_url column
 * @method     ChildSiteGameBlogGameQuery groupByRefreshedAt() Group by the vgakga_refreshed_at column
 *
 * @method     ChildSiteGameBlogGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteGameBlogGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteGameBlogGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteGameBlogGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteGameBlogGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteGameBlogGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteGameBlogGameQuery leftJoinGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the Game relation
 * @method     ChildSiteGameBlogGameQuery rightJoinGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Game relation
 * @method     ChildSiteGameBlogGameQuery innerJoinGame($relationAlias = null) Adds a INNER JOIN clause to the query using the Game relation
 *
 * @method     ChildSiteGameBlogGameQuery joinWithGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Game relation
 *
 * @method     ChildSiteGameBlogGameQuery leftJoinWithGame() Adds a LEFT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteGameBlogGameQuery rightJoinWithGame() Adds a RIGHT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteGameBlogGameQuery innerJoinWithGame() Adds a INNER JOIN clause and with to the query using the Game relation
 *
 * @method     ChildSiteGameBlogGameQuery leftJoinSiteGameBlogGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameBlogGameRelease relation
 * @method     ChildSiteGameBlogGameQuery rightJoinSiteGameBlogGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameBlogGameRelease relation
 * @method     ChildSiteGameBlogGameQuery innerJoinSiteGameBlogGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameBlogGameRelease relation
 *
 * @method     ChildSiteGameBlogGameQuery joinWithSiteGameBlogGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameBlogGameRelease relation
 *
 * @method     ChildSiteGameBlogGameQuery leftJoinWithSiteGameBlogGameRelease() Adds a LEFT JOIN clause and with to the query using the SiteGameBlogGameRelease relation
 * @method     ChildSiteGameBlogGameQuery rightJoinWithSiteGameBlogGameRelease() Adds a RIGHT JOIN clause and with to the query using the SiteGameBlogGameRelease relation
 * @method     ChildSiteGameBlogGameQuery innerJoinWithSiteGameBlogGameRelease() Adds a INNER JOIN clause and with to the query using the SiteGameBlogGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameBlogGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteGameBlogGame findOne(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGame matching the query
 * @method     ChildSiteGameBlogGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGame matching the query, or a new ChildSiteGameBlogGame object populated from the query conditions when no match is found
 *
 * @method     ChildSiteGameBlogGame findOneById(string $vgabga_id) Return the first ChildSiteGameBlogGame filtered by the vgabga_id column
 * @method     ChildSiteGameBlogGame findOneByGameId(int $vgabga_vgagam_id) Return the first ChildSiteGameBlogGame filtered by the vgabga_vgagam_id column
 * @method     ChildSiteGameBlogGame findOneByName(string $vgabga_name) Return the first ChildSiteGameBlogGame filtered by the vgabga_name column
 * @method     ChildSiteGameBlogGame findOneByReleasedAt(string $vgabga_released_at) Return the first ChildSiteGameBlogGame filtered by the vgabga_released_at column
 * @method     ChildSiteGameBlogGame findOneBySiteDetailUrl(string $vgabga_site_detail_url) Return the first ChildSiteGameBlogGame filtered by the vgabga_site_detail_url column
 * @method     ChildSiteGameBlogGame findOneByRefreshedAt(string $vgakga_refreshed_at) Return the first ChildSiteGameBlogGame filtered by the vgakga_refreshed_at column *

 * @method     ChildSiteGameBlogGame requirePk($key, ConnectionInterface $con = null) Return the ChildSiteGameBlogGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOne(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameBlogGame requireOneById(string $vgabga_id) Return the first ChildSiteGameBlogGame filtered by the vgabga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOneByGameId(int $vgabga_vgagam_id) Return the first ChildSiteGameBlogGame filtered by the vgabga_vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOneByName(string $vgabga_name) Return the first ChildSiteGameBlogGame filtered by the vgabga_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOneByReleasedAt(string $vgabga_released_at) Return the first ChildSiteGameBlogGame filtered by the vgabga_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOneBySiteDetailUrl(string $vgabga_site_detail_url) Return the first ChildSiteGameBlogGame filtered by the vgabga_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGame requireOneByRefreshedAt(string $vgakga_refreshed_at) Return the first ChildSiteGameBlogGame filtered by the vgakga_refreshed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameBlogGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteGameBlogGame objects based on current ModelCriteria
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findById(string $vgabga_id) Return ChildSiteGameBlogGame objects filtered by the vgabga_id column
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findByGameId(int $vgabga_vgagam_id) Return ChildSiteGameBlogGame objects filtered by the vgabga_vgagam_id column
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findByName(string $vgabga_name) Return ChildSiteGameBlogGame objects filtered by the vgabga_name column
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findByReleasedAt(string $vgabga_released_at) Return ChildSiteGameBlogGame objects filtered by the vgabga_released_at column
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findBySiteDetailUrl(string $vgabga_site_detail_url) Return ChildSiteGameBlogGame objects filtered by the vgabga_site_detail_url column
 * @method     ChildSiteGameBlogGame[]|ObjectCollection findByRefreshedAt(string $vgakga_refreshed_at) Return ChildSiteGameBlogGame objects filtered by the vgakga_refreshed_at column
 * @method     ChildSiteGameBlogGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteGameBlogGameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteGameBlogGameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameBlogGame', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteGameBlogGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteGameBlogGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteGameBlogGameQuery) {
            return $criteria;
        }
        $query = new ChildSiteGameBlogGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteGameBlogGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameBlogGameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteGameBlogGameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameBlogGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgabga_id, vgabga_vgagam_id, vgabga_name, vgabga_released_at, vgabga_site_detail_url, vgakga_refreshed_at FROM videogames_site_gameblog_game_vgabga WHERE vgabga_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteGameBlogGame $obj */
            $obj = new ChildSiteGameBlogGame();
            $obj->hydrate($row);
            SiteGameBlogGameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteGameBlogGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgabga_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgabga_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgabga_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgabga_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgabga_vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGameId(1234); // WHERE vgabga_vgagam_id = 1234
     * $query->filterByGameId(array(12, 34)); // WHERE vgabga_vgagam_id IN (12, 34)
     * $query->filterByGameId(array('min' => 12)); // WHERE vgabga_vgagam_id > 12
     * </code>
     *
     * @see       filterByGame()
     *
     * @param     mixed $gameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByGameId($gameId = null, $comparison = null)
    {
        if (is_array($gameId)) {
            $useMinMax = false;
            if (isset($gameId['min'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_VGAGAM_ID, $gameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gameId['max'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_VGAGAM_ID, $gameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_VGAGAM_ID, $gameId, $comparison);
    }

    /**
     * Filter the query on the vgabga_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgabga_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgabga_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgabga_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByReleasedAt('2011-03-14'); // WHERE vgabga_released_at = '2011-03-14'
     * $query->filterByReleasedAt('now'); // WHERE vgabga_released_at = '2011-03-14'
     * $query->filterByReleasedAt(array('max' => 'yesterday')); // WHERE vgabga_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $releasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByReleasedAt($releasedAt = null, $comparison = null)
    {
        if (is_array($releasedAt)) {
            $useMinMax = false;
            if (isset($releasedAt['min'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_RELEASED_AT, $releasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($releasedAt['max'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_RELEASED_AT, $releasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_RELEASED_AT, $releasedAt, $comparison);
    }

    /**
     * Filter the query on the vgabga_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgabga_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgabga_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgakga_refreshed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRefreshedAt('2011-03-14'); // WHERE vgakga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt('now'); // WHERE vgakga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt(array('max' => 'yesterday')); // WHERE vgakga_refreshed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $refreshedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByRefreshedAt($refreshedAt = null, $comparison = null)
    {
        if (is_array($refreshedAt)) {
            $useMinMax = false;
            if (isset($refreshedAt['min'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refreshedAt['max'])) {
                $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\Game object
     *
     * @param \IiMedias\VideoGamesBundle\Model\Game|ObjectCollection $game The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterByGame($game, $comparison = null)
    {
        if ($game instanceof \IiMedias\VideoGamesBundle\Model\Game) {
            return $this
                ->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_VGAGAM_ID, $game->getId(), $comparison);
        } elseif ($game instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_VGAGAM_ID, $game->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\Game or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Game relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function joinGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Game');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Game');
        }

        return $this;
    }

    /**
     * Use the Game relation Game object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameQuery A secondary query class using the current class as primary query
     */
    public function useGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Game', '\IiMedias\VideoGamesBundle\Model\GameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameRelease|ObjectCollection $siteGameBlogGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogGameRelease($siteGameBlogGameRelease, $comparison = null)
    {
        if ($siteGameBlogGameRelease instanceof \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameRelease) {
            return $this
                ->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $siteGameBlogGameRelease->getSiteGameBlogGameId(), $comparison);
        } elseif ($siteGameBlogGameRelease instanceof ObjectCollection) {
            return $this
                ->useSiteGameBlogGameReleaseQuery()
                ->filterByPrimaryKeys($siteGameBlogGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteGameBlogGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameBlogGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function joinSiteGameBlogGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameBlogGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameBlogGameRelease');
        }

        return $this;
    }

    /**
     * Use the SiteGameBlogGameRelease relation SiteGameBlogGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameBlogGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameBlogGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameBlogGameRelease', '\IiMedias\VideoGamesBundle\Model\SiteGameBlogGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteGameBlogGame $siteGameBlogGame Object to remove from the list of results
     *
     * @return $this|ChildSiteGameBlogGameQuery The current query, for fluid interface
     */
    public function prune($siteGameBlogGame = null)
    {
        if ($siteGameBlogGame) {
            $this->addUsingAlias(SiteGameBlogGameTableMap::COL_VGABGA_ID, $siteGameBlogGame->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_gameblog_game_vgabga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameBlogGameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteGameBlogGameTableMap::clearInstancePool();
            SiteGameBlogGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameBlogGameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteGameBlogGameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteGameBlogGameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteGameBlogGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteGameBlogGameQuery
