<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombAccessory as ChildApiGiantBombAccessory;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombAccessoryQuery as ChildApiGiantBombAccessoryQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombAccessoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_accessory_vgagac' table.
 *
 *
 *
 * @method     ChildApiGiantBombAccessoryQuery orderById($order = Criteria::ASC) Order by the vgagac_id column
 * @method     ChildApiGiantBombAccessoryQuery orderByName($order = Criteria::ASC) Order by the vgagac_name column
 * @method     ChildApiGiantBombAccessoryQuery orderBySummary($order = Criteria::ASC) Order by the vgagac_summary column
 * @method     ChildApiGiantBombAccessoryQuery orderByDescription($order = Criteria::ASC) Order by the vgagac_description column
 * @method     ChildApiGiantBombAccessoryQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagac_api_detail_url column
 * @method     ChildApiGiantBombAccessoryQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagac_site_detail_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagac_image_icon_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagac_image_medium_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagac_image_screen_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagac_image_small_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagac_image_super_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagac_image_thumb_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagac_image_tiny_url column
 * @method     ChildApiGiantBombAccessoryQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagac_created_at column
 * @method     ChildApiGiantBombAccessoryQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagac_updated_at column
 *
 * @method     ChildApiGiantBombAccessoryQuery groupById() Group by the vgagac_id column
 * @method     ChildApiGiantBombAccessoryQuery groupByName() Group by the vgagac_name column
 * @method     ChildApiGiantBombAccessoryQuery groupBySummary() Group by the vgagac_summary column
 * @method     ChildApiGiantBombAccessoryQuery groupByDescription() Group by the vgagac_description column
 * @method     ChildApiGiantBombAccessoryQuery groupByApiDetailUrl() Group by the vgagac_api_detail_url column
 * @method     ChildApiGiantBombAccessoryQuery groupBySiteDetailUrl() Group by the vgagac_site_detail_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageIconUrl() Group by the vgagac_image_icon_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageMediumUrl() Group by the vgagac_image_medium_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageScreenUrl() Group by the vgagac_image_screen_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageSmallUrl() Group by the vgagac_image_small_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageSuperUrl() Group by the vgagac_image_super_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageThumbUrl() Group by the vgagac_image_thumb_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByImageTinyUrl() Group by the vgagac_image_tiny_url column
 * @method     ChildApiGiantBombAccessoryQuery groupByCreatedAt() Group by the vgagac_created_at column
 * @method     ChildApiGiantBombAccessoryQuery groupByUpdatedAt() Group by the vgagac_updated_at column
 *
 * @method     ChildApiGiantBombAccessoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombAccessoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombAccessoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombAccessoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombAccessoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombAccessoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombAccessory findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombAccessory matching the query
 * @method     ChildApiGiantBombAccessory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombAccessory matching the query, or a new ChildApiGiantBombAccessory object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombAccessory findOneById(int $vgagac_id) Return the first ChildApiGiantBombAccessory filtered by the vgagac_id column
 * @method     ChildApiGiantBombAccessory findOneByName(string $vgagac_name) Return the first ChildApiGiantBombAccessory filtered by the vgagac_name column
 * @method     ChildApiGiantBombAccessory findOneBySummary(string $vgagac_summary) Return the first ChildApiGiantBombAccessory filtered by the vgagac_summary column
 * @method     ChildApiGiantBombAccessory findOneByDescription(string $vgagac_description) Return the first ChildApiGiantBombAccessory filtered by the vgagac_description column
 * @method     ChildApiGiantBombAccessory findOneByApiDetailUrl(string $vgagac_api_detail_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_api_detail_url column
 * @method     ChildApiGiantBombAccessory findOneBySiteDetailUrl(string $vgagac_site_detail_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_site_detail_url column
 * @method     ChildApiGiantBombAccessory findOneByImageIconUrl(string $vgagac_image_icon_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_icon_url column
 * @method     ChildApiGiantBombAccessory findOneByImageMediumUrl(string $vgagac_image_medium_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_medium_url column
 * @method     ChildApiGiantBombAccessory findOneByImageScreenUrl(string $vgagac_image_screen_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_screen_url column
 * @method     ChildApiGiantBombAccessory findOneByImageSmallUrl(string $vgagac_image_small_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_small_url column
 * @method     ChildApiGiantBombAccessory findOneByImageSuperUrl(string $vgagac_image_super_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_super_url column
 * @method     ChildApiGiantBombAccessory findOneByImageThumbUrl(string $vgagac_image_thumb_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_thumb_url column
 * @method     ChildApiGiantBombAccessory findOneByImageTinyUrl(string $vgagac_image_tiny_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_tiny_url column
 * @method     ChildApiGiantBombAccessory findOneByCreatedAt(string $vgagac_created_at) Return the first ChildApiGiantBombAccessory filtered by the vgagac_created_at column
 * @method     ChildApiGiantBombAccessory findOneByUpdatedAt(string $vgagac_updated_at) Return the first ChildApiGiantBombAccessory filtered by the vgagac_updated_at column *

 * @method     ChildApiGiantBombAccessory requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombAccessory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombAccessory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombAccessory requireOneById(int $vgagac_id) Return the first ChildApiGiantBombAccessory filtered by the vgagac_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByName(string $vgagac_name) Return the first ChildApiGiantBombAccessory filtered by the vgagac_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneBySummary(string $vgagac_summary) Return the first ChildApiGiantBombAccessory filtered by the vgagac_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByDescription(string $vgagac_description) Return the first ChildApiGiantBombAccessory filtered by the vgagac_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByApiDetailUrl(string $vgagac_api_detail_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneBySiteDetailUrl(string $vgagac_site_detail_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageIconUrl(string $vgagac_image_icon_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageMediumUrl(string $vgagac_image_medium_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageScreenUrl(string $vgagac_image_screen_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageSmallUrl(string $vgagac_image_small_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageSuperUrl(string $vgagac_image_super_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageThumbUrl(string $vgagac_image_thumb_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByImageTinyUrl(string $vgagac_image_tiny_url) Return the first ChildApiGiantBombAccessory filtered by the vgagac_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByCreatedAt(string $vgagac_created_at) Return the first ChildApiGiantBombAccessory filtered by the vgagac_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombAccessory requireOneByUpdatedAt(string $vgagac_updated_at) Return the first ChildApiGiantBombAccessory filtered by the vgagac_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombAccessory objects based on current ModelCriteria
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findById(int $vgagac_id) Return ChildApiGiantBombAccessory objects filtered by the vgagac_id column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByName(string $vgagac_name) Return ChildApiGiantBombAccessory objects filtered by the vgagac_name column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findBySummary(string $vgagac_summary) Return ChildApiGiantBombAccessory objects filtered by the vgagac_summary column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByDescription(string $vgagac_description) Return ChildApiGiantBombAccessory objects filtered by the vgagac_description column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByApiDetailUrl(string $vgagac_api_detail_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_api_detail_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findBySiteDetailUrl(string $vgagac_site_detail_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_site_detail_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageIconUrl(string $vgagac_image_icon_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_icon_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageMediumUrl(string $vgagac_image_medium_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_medium_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageScreenUrl(string $vgagac_image_screen_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_screen_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageSmallUrl(string $vgagac_image_small_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_small_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageSuperUrl(string $vgagac_image_super_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_super_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageThumbUrl(string $vgagac_image_thumb_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_thumb_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByImageTinyUrl(string $vgagac_image_tiny_url) Return ChildApiGiantBombAccessory objects filtered by the vgagac_image_tiny_url column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByCreatedAt(string $vgagac_created_at) Return ChildApiGiantBombAccessory objects filtered by the vgagac_created_at column
 * @method     ChildApiGiantBombAccessory[]|ObjectCollection findByUpdatedAt(string $vgagac_updated_at) Return ChildApiGiantBombAccessory objects filtered by the vgagac_updated_at column
 * @method     ChildApiGiantBombAccessory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombAccessoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombAccessoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombAccessory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombAccessoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombAccessoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombAccessoryQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombAccessoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombAccessory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombAccessoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombAccessoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombAccessory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagac_id, vgagac_name, vgagac_summary, vgagac_description, vgagac_api_detail_url, vgagac_site_detail_url, vgagac_image_icon_url, vgagac_image_medium_url, vgagac_image_screen_url, vgagac_image_small_url, vgagac_image_super_url, vgagac_image_thumb_url, vgagac_image_tiny_url, vgagac_created_at, vgagac_updated_at FROM videogames_api_giantbomb_accessory_vgagac WHERE vgagac_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombAccessory $obj */
            $obj = new ChildApiGiantBombAccessory();
            $obj->hydrate($row);
            ApiGiantBombAccessoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombAccessory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagac_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagac_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagac_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagac_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagac_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagac_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagac_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagac_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagac_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagac_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagac_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagac_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagac_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagac_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagac_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagac_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagac_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagac_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagac_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagac_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagac_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagac_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagac_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagac_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagac_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagac_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagac_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagac_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagac_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagac_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagac_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagac_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagac_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagac_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagac_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagac_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagac_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagac_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagac_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagac_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombAccessory $apiGiantBombAccessory Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombAccessoryQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombAccessory = null)
    {
        if ($apiGiantBombAccessory) {
            $this->addUsingAlias(ApiGiantBombAccessoryTableMap::COL_VGAGAC_ID, $apiGiantBombAccessory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_accessory_vgagac table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombAccessoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombAccessoryTableMap::clearInstancePool();
            ApiGiantBombAccessoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombAccessoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombAccessoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombAccessoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombAccessoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombAccessoryQuery
