<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating as ChildApiGiantBombGameOriginalRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery as ChildApiGiantBombGameOriginalRatingQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating as ChildApiGiantBombGameRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery as ChildApiGiantBombGameRatingQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard as ChildApiGiantBombRatingBoard;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery as ChildApiGiantBombRatingBoardQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameOriginalRatingTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameRatingTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_game_rating_vgaggr' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombGameRating implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombGameRatingTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgaggr_id field.
     *
     * @var        int
     */
    protected $vgaggr_id;

    /**
     * The value for the vgaggr_name field.
     *
     * @var        string
     */
    protected $vgaggr_name;

    /**
     * The value for the vgaggr_api_detail_url field.
     *
     * @var        string
     */
    protected $vgaggr_api_detail_url;

    /**
     * The value for the vgaggr_image_icon_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_icon_url;

    /**
     * The value for the vgaggr_image_medium_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_medium_url;

    /**
     * The value for the vgaggr_image_screen_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_screen_url;

    /**
     * The value for the vgaggr_image_small_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_small_url;

    /**
     * The value for the vgaggr_image_super_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_super_url;

    /**
     * The value for the vgaggr_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_thumb_url;

    /**
     * The value for the vgaggr_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgaggr_image_tiny_url;

    /**
     * The value for the vgagch_vgagrb_id field.
     *
     * @var        int
     */
    protected $vgagch_vgagrb_id;

    /**
     * @var        ChildApiGiantBombRatingBoard
     */
    protected $aApiGiantBombRatingBoard;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameOriginalRating[] Collection to store aggregation of ChildApiGiantBombGameOriginalRating objects.
     */
    protected $collApiGiantBombGameOriginalRatings;
    protected $collApiGiantBombGameOriginalRatingsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameRelease[] Collection to store aggregation of ChildApiGiantBombGameRelease objects.
     */
    protected $collApiGiantBombGameReleases;
    protected $collApiGiantBombGameReleasesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameOriginalRating[]
     */
    protected $apiGiantBombGameOriginalRatingsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameRelease[]
     */
    protected $apiGiantBombGameReleasesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameRating object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombGameRating</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombGameRating</code>, delegates to
     * <code>equals(ApiGiantBombGameRating)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombGameRating The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgaggr_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgaggr_id;
    }

    /**
     * Get the [vgaggr_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgaggr_name;
    }

    /**
     * Get the [vgaggr_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgaggr_api_detail_url;
    }

    /**
     * Get the [vgaggr_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgaggr_image_icon_url;
    }

    /**
     * Get the [vgaggr_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgaggr_image_medium_url;
    }

    /**
     * Get the [vgaggr_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgaggr_image_screen_url;
    }

    /**
     * Get the [vgaggr_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgaggr_image_small_url;
    }

    /**
     * Get the [vgaggr_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgaggr_image_super_url;
    }

    /**
     * Get the [vgaggr_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgaggr_image_thumb_url;
    }

    /**
     * Get the [vgaggr_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgaggr_image_tiny_url;
    }

    /**
     * Get the [vgagch_vgagrb_id] column value.
     *
     * @return int
     */
    public function getGiantBombRatingBoardId()
    {
        return $this->vgagch_vgagrb_id;
    }

    /**
     * Set the value of [vgaggr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaggr_id !== $v) {
            $this->vgaggr_id = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgaggr_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_name !== $v) {
            $this->vgaggr_name = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgaggr_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_api_detail_url !== $v) {
            $this->vgaggr_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgaggr_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_icon_url !== $v) {
            $this->vgaggr_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgaggr_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_medium_url !== $v) {
            $this->vgaggr_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgaggr_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_screen_url !== $v) {
            $this->vgaggr_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgaggr_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_small_url !== $v) {
            $this->vgaggr_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgaggr_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_super_url !== $v) {
            $this->vgaggr_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgaggr_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_thumb_url !== $v) {
            $this->vgaggr_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgaggr_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaggr_image_tiny_url !== $v) {
            $this->vgaggr_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagch_vgagrb_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setGiantBombRatingBoardId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagch_vgagrb_id !== $v) {
            $this->vgagch_vgagrb_id = $v;
            $this->modifiedColumns[ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID] = true;
        }

        if ($this->aApiGiantBombRatingBoard !== null && $this->aApiGiantBombRatingBoard->getId() !== $v) {
            $this->aApiGiantBombRatingBoard = null;
        }

        return $this;
    } // setGiantBombRatingBoardId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaggr_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombGameRatingTableMap::translateFieldName('GiantBombRatingBoardId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagch_vgagrb_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = ApiGiantBombGameRatingTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombRatingBoard !== null && $this->vgagch_vgagrb_id !== $this->aApiGiantBombRatingBoard->getId()) {
            $this->aApiGiantBombRatingBoard = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombGameRatingQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombRatingBoard = null;
            $this->collApiGiantBombGameOriginalRatings = null;

            $this->collApiGiantBombGameReleases = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombGameRating::setDeleted()
     * @see ApiGiantBombGameRating::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombGameRatingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombGameRatingTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombRatingBoard !== null) {
                if ($this->aApiGiantBombRatingBoard->isModified() || $this->aApiGiantBombRatingBoard->isNew()) {
                    $affectedRows += $this->aApiGiantBombRatingBoard->save($con);
                }
                $this->setApiGiantBombRatingBoard($this->aApiGiantBombRatingBoard);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombGameOriginalRatingsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameOriginalRatingsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameOriginalRatingsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameOriginalRatings !== null) {
                foreach ($this->collApiGiantBombGameOriginalRatings as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleasesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleasesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleasesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleases !== null) {
                foreach ($this->collApiGiantBombGameReleases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_name';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaggr_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagch_vgagrb_id';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_game_rating_vgaggr (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgaggr_id':
                        $stmt->bindValue($identifier, $this->vgaggr_id, PDO::PARAM_INT);
                        break;
                    case 'vgaggr_name':
                        $stmt->bindValue($identifier, $this->vgaggr_name, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgaggr_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_small_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_super_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgaggr_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgaggr_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagch_vgagrb_id':
                        $stmt->bindValue($identifier, $this->vgagch_vgagrb_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameRatingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getApiDetailUrl();
                break;
            case 3:
                return $this->getImageIconUrl();
                break;
            case 4:
                return $this->getImageMediumUrl();
                break;
            case 5:
                return $this->getImageScreenUrl();
                break;
            case 6:
                return $this->getImageSmallUrl();
                break;
            case 7:
                return $this->getImageSuperUrl();
                break;
            case 8:
                return $this->getImageThumbUrl();
                break;
            case 9:
                return $this->getImageTinyUrl();
                break;
            case 10:
                return $this->getGiantBombRatingBoardId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombGameRating'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombGameRating'][$this->hashCode()] = true;
        $keys = ApiGiantBombGameRatingTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getApiDetailUrl(),
            $keys[3] => $this->getImageIconUrl(),
            $keys[4] => $this->getImageMediumUrl(),
            $keys[5] => $this->getImageScreenUrl(),
            $keys[6] => $this->getImageSmallUrl(),
            $keys[7] => $this->getImageSuperUrl(),
            $keys[8] => $this->getImageThumbUrl(),
            $keys[9] => $this->getImageTinyUrl(),
            $keys[10] => $this->getGiantBombRatingBoardId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombRatingBoard) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombRatingBoard';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_rating_board_vgagrb';
                        break;
                    default:
                        $key = 'ApiGiantBombRatingBoard';
                }

                $result[$key] = $this->aApiGiantBombRatingBoard->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collApiGiantBombGameOriginalRatings) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameOriginalRatings';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_original_rating_vgaggos';
                        break;
                    default:
                        $key = 'ApiGiantBombGameOriginalRatings';
                }

                $result[$key] = $this->collApiGiantBombGameOriginalRatings->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleases) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleases';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_release_vgagrls';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleases';
                }

                $result[$key] = $this->collApiGiantBombGameReleases->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameRatingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setApiDetailUrl($value);
                break;
            case 3:
                $this->setImageIconUrl($value);
                break;
            case 4:
                $this->setImageMediumUrl($value);
                break;
            case 5:
                $this->setImageScreenUrl($value);
                break;
            case 6:
                $this->setImageSmallUrl($value);
                break;
            case 7:
                $this->setImageSuperUrl($value);
                break;
            case 8:
                $this->setImageThumbUrl($value);
                break;
            case 9:
                $this->setImageTinyUrl($value);
                break;
            case 10:
                $this->setGiantBombRatingBoardId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombGameRatingTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setApiDetailUrl($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setImageIconUrl($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setImageMediumUrl($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setImageScreenUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setImageSmallUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImageSuperUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImageThumbUrl($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImageTinyUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setGiantBombRatingBoardId($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombGameRatingTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $this->vgaggr_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME, $this->vgaggr_name);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL, $this->vgaggr_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL, $this->vgaggr_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL, $this->vgaggr_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL, $this->vgaggr_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL, $this->vgaggr_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL, $this->vgaggr_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL, $this->vgaggr_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL, $this->vgaggr_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID)) {
            $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $this->vgagch_vgagrb_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombGameRatingQuery::create();
        $criteria->add(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $this->vgaggr_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgaggr_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setGiantBombRatingBoardId($this->getGiantBombRatingBoardId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombGameOriginalRatings() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameOriginalRating($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameRelease($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombRatingBoard object.
     *
     * @param  ChildApiGiantBombRatingBoard $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombRatingBoard(ChildApiGiantBombRatingBoard $v = null)
    {
        if ($v === null) {
            $this->setGiantBombRatingBoardId(NULL);
        } else {
            $this->setGiantBombRatingBoardId($v->getId());
        }

        $this->aApiGiantBombRatingBoard = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombRatingBoard object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombRatingGame($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombRatingBoard object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombRatingBoard The associated ChildApiGiantBombRatingBoard object.
     * @throws PropelException
     */
    public function getApiGiantBombRatingBoard(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombRatingBoard === null && ($this->vgagch_vgagrb_id !== null)) {
            $this->aApiGiantBombRatingBoard = ChildApiGiantBombRatingBoardQuery::create()->findPk($this->vgagch_vgagrb_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombRatingBoard->addApiGiantBombRatingGames($this);
             */
        }

        return $this->aApiGiantBombRatingBoard;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombGameOriginalRating' == $relationName) {
            return $this->initApiGiantBombGameOriginalRatings();
        }
        if ('ApiGiantBombGameRelease' == $relationName) {
            return $this->initApiGiantBombGameReleases();
        }
    }

    /**
     * Clears out the collApiGiantBombGameOriginalRatings collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameOriginalRatings()
     */
    public function clearApiGiantBombGameOriginalRatings()
    {
        $this->collApiGiantBombGameOriginalRatings = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameOriginalRatings collection loaded partially.
     */
    public function resetPartialApiGiantBombGameOriginalRatings($v = true)
    {
        $this->collApiGiantBombGameOriginalRatingsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameOriginalRatings collection.
     *
     * By default this just sets the collApiGiantBombGameOriginalRatings collection to an empty array (like clearcollApiGiantBombGameOriginalRatings());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameOriginalRatings($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameOriginalRatings && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameOriginalRatingTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameOriginalRatings = new $collectionClassName;
        $this->collApiGiantBombGameOriginalRatings->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating');
    }

    /**
     * Gets an array of ChildApiGiantBombGameOriginalRating objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRating is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameOriginalRating[] List of ChildApiGiantBombGameOriginalRating objects
     * @throws PropelException
     */
    public function getApiGiantBombGameOriginalRatings(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameOriginalRatingsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameOriginalRatings || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameOriginalRatings) {
                // return empty collection
                $this->initApiGiantBombGameOriginalRatings();
            } else {
                $collApiGiantBombGameOriginalRatings = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRating($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameOriginalRatingsPartial && count($collApiGiantBombGameOriginalRatings)) {
                        $this->initApiGiantBombGameOriginalRatings(false);

                        foreach ($collApiGiantBombGameOriginalRatings as $obj) {
                            if (false == $this->collApiGiantBombGameOriginalRatings->contains($obj)) {
                                $this->collApiGiantBombGameOriginalRatings->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameOriginalRatingsPartial = true;
                    }

                    return $collApiGiantBombGameOriginalRatings;
                }

                if ($partial && $this->collApiGiantBombGameOriginalRatings) {
                    foreach ($this->collApiGiantBombGameOriginalRatings as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameOriginalRatings[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameOriginalRatings = $collApiGiantBombGameOriginalRatings;
                $this->collApiGiantBombGameOriginalRatingsPartial = false;
            }
        }

        return $this->collApiGiantBombGameOriginalRatings;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameOriginalRating objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameOriginalRatings A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setApiGiantBombGameOriginalRatings(Collection $apiGiantBombGameOriginalRatings, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameOriginalRating[] $apiGiantBombGameOriginalRatingsToDelete */
        $apiGiantBombGameOriginalRatingsToDelete = $this->getApiGiantBombGameOriginalRatings(new Criteria(), $con)->diff($apiGiantBombGameOriginalRatings);


        $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = $apiGiantBombGameOriginalRatingsToDelete;

        foreach ($apiGiantBombGameOriginalRatingsToDelete as $apiGiantBombGameOriginalRatingRemoved) {
            $apiGiantBombGameOriginalRatingRemoved->setApiGiantBombGameRating(null);
        }

        $this->collApiGiantBombGameOriginalRatings = null;
        foreach ($apiGiantBombGameOriginalRatings as $apiGiantBombGameOriginalRating) {
            $this->addApiGiantBombGameOriginalRating($apiGiantBombGameOriginalRating);
        }

        $this->collApiGiantBombGameOriginalRatings = $apiGiantBombGameOriginalRatings;
        $this->collApiGiantBombGameOriginalRatingsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameOriginalRating objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameOriginalRating objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameOriginalRatings(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameOriginalRatingsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameOriginalRatings || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameOriginalRatings) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameOriginalRatings());
            }

            $query = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRating($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameOriginalRatings);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameOriginalRating object to this object
     * through the ChildApiGiantBombGameOriginalRating foreign key attribute.
     *
     * @param  ChildApiGiantBombGameOriginalRating $l ChildApiGiantBombGameOriginalRating
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function addApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $l)
    {
        if ($this->collApiGiantBombGameOriginalRatings === null) {
            $this->initApiGiantBombGameOriginalRatings();
            $this->collApiGiantBombGameOriginalRatingsPartial = true;
        }

        if (!$this->collApiGiantBombGameOriginalRatings->contains($l)) {
            $this->doAddApiGiantBombGameOriginalRating($l);

            if ($this->apiGiantBombGameOriginalRatingsScheduledForDeletion and $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->remove($this->apiGiantBombGameOriginalRatingsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating The ChildApiGiantBombGameOriginalRating object to add.
     */
    protected function doAddApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating)
    {
        $this->collApiGiantBombGameOriginalRatings[]= $apiGiantBombGameOriginalRating;
        $apiGiantBombGameOriginalRating->setApiGiantBombGameRating($this);
    }

    /**
     * @param  ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating The ChildApiGiantBombGameOriginalRating object to remove.
     * @return $this|ChildApiGiantBombGameRating The current object (for fluent API support)
     */
    public function removeApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating)
    {
        if ($this->getApiGiantBombGameOriginalRatings()->contains($apiGiantBombGameOriginalRating)) {
            $pos = $this->collApiGiantBombGameOriginalRatings->search($apiGiantBombGameOriginalRating);
            $this->collApiGiantBombGameOriginalRatings->remove($pos);
            if (null === $this->apiGiantBombGameOriginalRatingsScheduledForDeletion) {
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = clone $this->collApiGiantBombGameOriginalRatings;
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameOriginalRatingsScheduledForDeletion[]= clone $apiGiantBombGameOriginalRating;
            $apiGiantBombGameOriginalRating->setApiGiantBombGameRating(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRating is new, it will return
     * an empty collection; or if this ApiGiantBombGameRating has previously
     * been saved, it will retrieve related ApiGiantBombGameOriginalRatings from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRating.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameOriginalRating[] List of ChildApiGiantBombGameOriginalRating objects
     */
    public function getApiGiantBombGameOriginalRatingsJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGameOriginalRatings($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleases()
     */
    public function clearApiGiantBombGameReleases()
    {
        $this->collApiGiantBombGameReleases = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleases collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleases($v = true)
    {
        $this->collApiGiantBombGameReleasesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleases collection.
     *
     * By default this just sets the collApiGiantBombGameReleases collection to an empty array (like clearcollApiGiantBombGameReleases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleases($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleases && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleaseTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleases = new $collectionClassName;
        $this->collApiGiantBombGameReleases->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease');
    }

    /**
     * Gets an array of ChildApiGiantBombGameRelease objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGameRating is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleases(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                // return empty collection
                $this->initApiGiantBombGameReleases();
            } else {
                $collApiGiantBombGameReleases = ChildApiGiantBombGameReleaseQuery::create(null, $criteria)
                    ->filterByApiGiantBombGameRating($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleasesPartial && count($collApiGiantBombGameReleases)) {
                        $this->initApiGiantBombGameReleases(false);

                        foreach ($collApiGiantBombGameReleases as $obj) {
                            if (false == $this->collApiGiantBombGameReleases->contains($obj)) {
                                $this->collApiGiantBombGameReleases->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleasesPartial = true;
                    }

                    return $collApiGiantBombGameReleases;
                }

                if ($partial && $this->collApiGiantBombGameReleases) {
                    foreach ($this->collApiGiantBombGameReleases as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleases[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleases = $collApiGiantBombGameReleases;
                $this->collApiGiantBombGameReleasesPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleases;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameRelease objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleases A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGameRating The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleases(Collection $apiGiantBombGameReleases, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameRelease[] $apiGiantBombGameReleasesToDelete */
        $apiGiantBombGameReleasesToDelete = $this->getApiGiantBombGameReleases(new Criteria(), $con)->diff($apiGiantBombGameReleases);


        $this->apiGiantBombGameReleasesScheduledForDeletion = $apiGiantBombGameReleasesToDelete;

        foreach ($apiGiantBombGameReleasesToDelete as $apiGiantBombGameReleaseRemoved) {
            $apiGiantBombGameReleaseRemoved->setApiGiantBombGameRating(null);
        }

        $this->collApiGiantBombGameReleases = null;
        foreach ($apiGiantBombGameReleases as $apiGiantBombGameRelease) {
            $this->addApiGiantBombGameRelease($apiGiantBombGameRelease);
        }

        $this->collApiGiantBombGameReleases = $apiGiantBombGameReleases;
        $this->collApiGiantBombGameReleasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameRelease objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameRelease objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleases(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleases());
            }

            $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGameRating($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleases);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameRelease object to this object
     * through the ChildApiGiantBombGameRelease foreign key attribute.
     *
     * @param  ChildApiGiantBombGameRelease $l ChildApiGiantBombGameRelease
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating The current object (for fluent API support)
     */
    public function addApiGiantBombGameRelease(ChildApiGiantBombGameRelease $l)
    {
        if ($this->collApiGiantBombGameReleases === null) {
            $this->initApiGiantBombGameReleases();
            $this->collApiGiantBombGameReleasesPartial = true;
        }

        if (!$this->collApiGiantBombGameReleases->contains($l)) {
            $this->doAddApiGiantBombGameRelease($l);

            if ($this->apiGiantBombGameReleasesScheduledForDeletion and $this->apiGiantBombGameReleasesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleasesScheduledForDeletion->remove($this->apiGiantBombGameReleasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to add.
     */
    protected function doAddApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        $this->collApiGiantBombGameReleases[]= $apiGiantBombGameRelease;
        $apiGiantBombGameRelease->setApiGiantBombGameRating($this);
    }

    /**
     * @param  ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to remove.
     * @return $this|ChildApiGiantBombGameRating The current object (for fluent API support)
     */
    public function removeApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        if ($this->getApiGiantBombGameReleases()->contains($apiGiantBombGameRelease)) {
            $pos = $this->collApiGiantBombGameReleases->search($apiGiantBombGameRelease);
            $this->collApiGiantBombGameReleases->remove($pos);
            if (null === $this->apiGiantBombGameReleasesScheduledForDeletion) {
                $this->apiGiantBombGameReleasesScheduledForDeletion = clone $this->collApiGiantBombGameReleases;
                $this->apiGiantBombGameReleasesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleasesScheduledForDeletion[]= $apiGiantBombGameRelease;
            $apiGiantBombGameRelease->setApiGiantBombGameRating(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRating is new, it will return
     * an empty collection; or if this ApiGiantBombGameRating has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRating.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRating is new, it will return
     * an empty collection; or if this ApiGiantBombGameRating has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRating.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombPlatform(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombPlatform', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGameRating is new, it will return
     * an empty collection; or if this ApiGiantBombGameRating has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGameRating.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombRegion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombRegion', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombRatingBoard) {
            $this->aApiGiantBombRatingBoard->removeApiGiantBombRatingGame($this);
        }
        $this->vgaggr_id = null;
        $this->vgaggr_name = null;
        $this->vgaggr_api_detail_url = null;
        $this->vgaggr_image_icon_url = null;
        $this->vgaggr_image_medium_url = null;
        $this->vgaggr_image_screen_url = null;
        $this->vgaggr_image_small_url = null;
        $this->vgaggr_image_super_url = null;
        $this->vgaggr_image_thumb_url = null;
        $this->vgaggr_image_tiny_url = null;
        $this->vgagch_vgagrb_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombGameOriginalRatings) {
                foreach ($this->collApiGiantBombGameOriginalRatings as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleases) {
                foreach ($this->collApiGiantBombGameReleases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombGameOriginalRatings = null;
        $this->collApiGiantBombGameReleases = null;
        $this->aApiGiantBombRatingBoard = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombGameRatingTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
