<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany as ChildApiGiantBombCompany;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery as ChildApiGiantBombCompanyQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombCompanyTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_company_vgagco' table.
 *
 *
 *
 * @method     ChildApiGiantBombCompanyQuery orderById($order = Criteria::ASC) Order by the vgagco_id column
 * @method     ChildApiGiantBombCompanyQuery orderByName($order = Criteria::ASC) Order by the vgagco_name column
 * @method     ChildApiGiantBombCompanyQuery orderByAbbr($order = Criteria::ASC) Order by the vgagco_abbr column
 * @method     ChildApiGiantBombCompanyQuery orderByAliases($order = Criteria::ASC) Order by the vgagco_aliases column
 * @method     ChildApiGiantBombCompanyQuery orderBySummary($order = Criteria::ASC) Order by the vgagco_summary column
 * @method     ChildApiGiantBombCompanyQuery orderByDescription($order = Criteria::ASC) Order by the vgagco_description column
 * @method     ChildApiGiantBombCompanyQuery orderByLocationAddress($order = Criteria::ASC) Order by the vgagco_location_address column
 * @method     ChildApiGiantBombCompanyQuery orderByLocationCity($order = Criteria::ASC) Order by the vgagco_location_city column
 * @method     ChildApiGiantBombCompanyQuery orderByLocationCountry($order = Criteria::ASC) Order by the vgagco_location_country column
 * @method     ChildApiGiantBombCompanyQuery orderByLocationState($order = Criteria::ASC) Order by the vgagco_location_state column
 * @method     ChildApiGiantBombCompanyQuery orderByPhone($order = Criteria::ASC) Order by the vgagco_phone column
 * @method     ChildApiGiantBombCompanyQuery orderByWebsite($order = Criteria::ASC) Order by the vgagco_website column
 * @method     ChildApiGiantBombCompanyQuery orderByFoundedAt($order = Criteria::ASC) Order by the vgagco_founded_at column
 * @method     ChildApiGiantBombCompanyQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagco_api_detail_url column
 * @method     ChildApiGiantBombCompanyQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagco_site_detail_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagco_image_icon_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagco_image_medium_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagco_image_screen_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagco_image_small_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagco_image_super_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagco_image_thumb_url column
 * @method     ChildApiGiantBombCompanyQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagco_image_tiny_url column
 * @method     ChildApiGiantBombCompanyQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagco_created_at column
 * @method     ChildApiGiantBombCompanyQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagco_updated_at column
 *
 * @method     ChildApiGiantBombCompanyQuery groupById() Group by the vgagco_id column
 * @method     ChildApiGiantBombCompanyQuery groupByName() Group by the vgagco_name column
 * @method     ChildApiGiantBombCompanyQuery groupByAbbr() Group by the vgagco_abbr column
 * @method     ChildApiGiantBombCompanyQuery groupByAliases() Group by the vgagco_aliases column
 * @method     ChildApiGiantBombCompanyQuery groupBySummary() Group by the vgagco_summary column
 * @method     ChildApiGiantBombCompanyQuery groupByDescription() Group by the vgagco_description column
 * @method     ChildApiGiantBombCompanyQuery groupByLocationAddress() Group by the vgagco_location_address column
 * @method     ChildApiGiantBombCompanyQuery groupByLocationCity() Group by the vgagco_location_city column
 * @method     ChildApiGiantBombCompanyQuery groupByLocationCountry() Group by the vgagco_location_country column
 * @method     ChildApiGiantBombCompanyQuery groupByLocationState() Group by the vgagco_location_state column
 * @method     ChildApiGiantBombCompanyQuery groupByPhone() Group by the vgagco_phone column
 * @method     ChildApiGiantBombCompanyQuery groupByWebsite() Group by the vgagco_website column
 * @method     ChildApiGiantBombCompanyQuery groupByFoundedAt() Group by the vgagco_founded_at column
 * @method     ChildApiGiantBombCompanyQuery groupByApiDetailUrl() Group by the vgagco_api_detail_url column
 * @method     ChildApiGiantBombCompanyQuery groupBySiteDetailUrl() Group by the vgagco_site_detail_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageIconUrl() Group by the vgagco_image_icon_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageMediumUrl() Group by the vgagco_image_medium_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageScreenUrl() Group by the vgagco_image_screen_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageSmallUrl() Group by the vgagco_image_small_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageSuperUrl() Group by the vgagco_image_super_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageThumbUrl() Group by the vgagco_image_thumb_url column
 * @method     ChildApiGiantBombCompanyQuery groupByImageTinyUrl() Group by the vgagco_image_tiny_url column
 * @method     ChildApiGiantBombCompanyQuery groupByCreatedAt() Group by the vgagco_created_at column
 * @method     ChildApiGiantBombCompanyQuery groupByUpdatedAt() Group by the vgagco_updated_at column
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombCompanyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombCompanyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombCompanyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombCompanyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinApiGiantBombPlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinApiGiantBombPlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinApiGiantBombPlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombCompanyQuery joinWithApiGiantBombPlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWithApiGiantBombPlatform() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinWithApiGiantBombPlatform() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinWithApiGiantBombPlatform() Adds a INNER JOIN clause and with to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery joinWithApiGiantBombGameDeveloper($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWithApiGiantBombGameDeveloper() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinWithApiGiantBombGameDeveloper() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinWithApiGiantBombGameDeveloper() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinApiGiantBombGamePublisher($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinApiGiantBombGamePublisher($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinApiGiantBombGamePublisher($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombCompanyQuery joinWithApiGiantBombGamePublisher($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWithApiGiantBombGamePublisher() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinWithApiGiantBombGamePublisher() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinWithApiGiantBombGamePublisher() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery joinWithApiGiantBombGameReleaseDeveloper($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWithApiGiantBombGameReleaseDeveloper() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinWithApiGiantBombGameReleaseDeveloper() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinWithApiGiantBombGameReleaseDeveloper() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     ChildApiGiantBombCompanyQuery joinWithApiGiantBombGameReleasePublisher($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     ChildApiGiantBombCompanyQuery leftJoinWithApiGiantBombGameReleasePublisher() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombCompanyQuery rightJoinWithApiGiantBombGameReleasePublisher() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombCompanyQuery innerJoinWithApiGiantBombGameReleasePublisher() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombCompany findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombCompany matching the query
 * @method     ChildApiGiantBombCompany findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombCompany matching the query, or a new ChildApiGiantBombCompany object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombCompany findOneById(int $vgagco_id) Return the first ChildApiGiantBombCompany filtered by the vgagco_id column
 * @method     ChildApiGiantBombCompany findOneByName(string $vgagco_name) Return the first ChildApiGiantBombCompany filtered by the vgagco_name column
 * @method     ChildApiGiantBombCompany findOneByAbbr(string $vgagco_abbr) Return the first ChildApiGiantBombCompany filtered by the vgagco_abbr column
 * @method     ChildApiGiantBombCompany findOneByAliases(array $vgagco_aliases) Return the first ChildApiGiantBombCompany filtered by the vgagco_aliases column
 * @method     ChildApiGiantBombCompany findOneBySummary(string $vgagco_summary) Return the first ChildApiGiantBombCompany filtered by the vgagco_summary column
 * @method     ChildApiGiantBombCompany findOneByDescription(string $vgagco_description) Return the first ChildApiGiantBombCompany filtered by the vgagco_description column
 * @method     ChildApiGiantBombCompany findOneByLocationAddress(string $vgagco_location_address) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_address column
 * @method     ChildApiGiantBombCompany findOneByLocationCity(string $vgagco_location_city) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_city column
 * @method     ChildApiGiantBombCompany findOneByLocationCountry(string $vgagco_location_country) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_country column
 * @method     ChildApiGiantBombCompany findOneByLocationState(string $vgagco_location_state) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_state column
 * @method     ChildApiGiantBombCompany findOneByPhone(string $vgagco_phone) Return the first ChildApiGiantBombCompany filtered by the vgagco_phone column
 * @method     ChildApiGiantBombCompany findOneByWebsite(string $vgagco_website) Return the first ChildApiGiantBombCompany filtered by the vgagco_website column
 * @method     ChildApiGiantBombCompany findOneByFoundedAt(string $vgagco_founded_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_founded_at column
 * @method     ChildApiGiantBombCompany findOneByApiDetailUrl(string $vgagco_api_detail_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_api_detail_url column
 * @method     ChildApiGiantBombCompany findOneBySiteDetailUrl(string $vgagco_site_detail_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_site_detail_url column
 * @method     ChildApiGiantBombCompany findOneByImageIconUrl(string $vgagco_image_icon_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_icon_url column
 * @method     ChildApiGiantBombCompany findOneByImageMediumUrl(string $vgagco_image_medium_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_medium_url column
 * @method     ChildApiGiantBombCompany findOneByImageScreenUrl(string $vgagco_image_screen_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_screen_url column
 * @method     ChildApiGiantBombCompany findOneByImageSmallUrl(string $vgagco_image_small_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_small_url column
 * @method     ChildApiGiantBombCompany findOneByImageSuperUrl(string $vgagco_image_super_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_super_url column
 * @method     ChildApiGiantBombCompany findOneByImageThumbUrl(string $vgagco_image_thumb_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_thumb_url column
 * @method     ChildApiGiantBombCompany findOneByImageTinyUrl(string $vgagco_image_tiny_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_tiny_url column
 * @method     ChildApiGiantBombCompany findOneByCreatedAt(string $vgagco_created_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_created_at column
 * @method     ChildApiGiantBombCompany findOneByUpdatedAt(string $vgagco_updated_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_updated_at column *

 * @method     ChildApiGiantBombCompany requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombCompany by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombCompany matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombCompany requireOneById(int $vgagco_id) Return the first ChildApiGiantBombCompany filtered by the vgagco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByName(string $vgagco_name) Return the first ChildApiGiantBombCompany filtered by the vgagco_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByAbbr(string $vgagco_abbr) Return the first ChildApiGiantBombCompany filtered by the vgagco_abbr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByAliases(array $vgagco_aliases) Return the first ChildApiGiantBombCompany filtered by the vgagco_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneBySummary(string $vgagco_summary) Return the first ChildApiGiantBombCompany filtered by the vgagco_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByDescription(string $vgagco_description) Return the first ChildApiGiantBombCompany filtered by the vgagco_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByLocationAddress(string $vgagco_location_address) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByLocationCity(string $vgagco_location_city) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByLocationCountry(string $vgagco_location_country) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByLocationState(string $vgagco_location_state) Return the first ChildApiGiantBombCompany filtered by the vgagco_location_state column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByPhone(string $vgagco_phone) Return the first ChildApiGiantBombCompany filtered by the vgagco_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByWebsite(string $vgagco_website) Return the first ChildApiGiantBombCompany filtered by the vgagco_website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByFoundedAt(string $vgagco_founded_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_founded_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByApiDetailUrl(string $vgagco_api_detail_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneBySiteDetailUrl(string $vgagco_site_detail_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageIconUrl(string $vgagco_image_icon_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageMediumUrl(string $vgagco_image_medium_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageScreenUrl(string $vgagco_image_screen_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageSmallUrl(string $vgagco_image_small_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageSuperUrl(string $vgagco_image_super_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageThumbUrl(string $vgagco_image_thumb_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByImageTinyUrl(string $vgagco_image_tiny_url) Return the first ChildApiGiantBombCompany filtered by the vgagco_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByCreatedAt(string $vgagco_created_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCompany requireOneByUpdatedAt(string $vgagco_updated_at) Return the first ChildApiGiantBombCompany filtered by the vgagco_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombCompany[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombCompany objects based on current ModelCriteria
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findById(int $vgagco_id) Return ChildApiGiantBombCompany objects filtered by the vgagco_id column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByName(string $vgagco_name) Return ChildApiGiantBombCompany objects filtered by the vgagco_name column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByAbbr(string $vgagco_abbr) Return ChildApiGiantBombCompany objects filtered by the vgagco_abbr column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByAliases(array $vgagco_aliases) Return ChildApiGiantBombCompany objects filtered by the vgagco_aliases column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findBySummary(string $vgagco_summary) Return ChildApiGiantBombCompany objects filtered by the vgagco_summary column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByDescription(string $vgagco_description) Return ChildApiGiantBombCompany objects filtered by the vgagco_description column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByLocationAddress(string $vgagco_location_address) Return ChildApiGiantBombCompany objects filtered by the vgagco_location_address column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByLocationCity(string $vgagco_location_city) Return ChildApiGiantBombCompany objects filtered by the vgagco_location_city column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByLocationCountry(string $vgagco_location_country) Return ChildApiGiantBombCompany objects filtered by the vgagco_location_country column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByLocationState(string $vgagco_location_state) Return ChildApiGiantBombCompany objects filtered by the vgagco_location_state column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByPhone(string $vgagco_phone) Return ChildApiGiantBombCompany objects filtered by the vgagco_phone column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByWebsite(string $vgagco_website) Return ChildApiGiantBombCompany objects filtered by the vgagco_website column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByFoundedAt(string $vgagco_founded_at) Return ChildApiGiantBombCompany objects filtered by the vgagco_founded_at column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByApiDetailUrl(string $vgagco_api_detail_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_api_detail_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findBySiteDetailUrl(string $vgagco_site_detail_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_site_detail_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageIconUrl(string $vgagco_image_icon_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_icon_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageMediumUrl(string $vgagco_image_medium_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_medium_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageScreenUrl(string $vgagco_image_screen_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_screen_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageSmallUrl(string $vgagco_image_small_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_small_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageSuperUrl(string $vgagco_image_super_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_super_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageThumbUrl(string $vgagco_image_thumb_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_thumb_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByImageTinyUrl(string $vgagco_image_tiny_url) Return ChildApiGiantBombCompany objects filtered by the vgagco_image_tiny_url column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByCreatedAt(string $vgagco_created_at) Return ChildApiGiantBombCompany objects filtered by the vgagco_created_at column
 * @method     ChildApiGiantBombCompany[]|ObjectCollection findByUpdatedAt(string $vgagco_updated_at) Return ChildApiGiantBombCompany objects filtered by the vgagco_updated_at column
 * @method     ChildApiGiantBombCompany[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombCompanyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombCompanyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCompany', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombCompanyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombCompanyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombCompanyQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombCompanyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombCompany|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombCompanyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombCompany A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagco_id, vgagco_name, vgagco_abbr, vgagco_aliases, vgagco_summary, vgagco_description, vgagco_location_address, vgagco_location_city, vgagco_location_country, vgagco_location_state, vgagco_phone, vgagco_website, vgagco_founded_at, vgagco_api_detail_url, vgagco_site_detail_url, vgagco_image_icon_url, vgagco_image_medium_url, vgagco_image_screen_url, vgagco_image_small_url, vgagco_image_super_url, vgagco_image_thumb_url, vgagco_image_tiny_url, vgagco_created_at, vgagco_updated_at FROM videogames_api_giantbomb_company_vgagco WHERE vgagco_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombCompany $obj */
            $obj = new ChildApiGiantBombCompany();
            $obj->hydrate($row);
            ApiGiantBombCompanyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombCompany|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagco_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagco_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagco_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagco_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagco_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagco_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagco_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagco_abbr column
     *
     * Example usage:
     * <code>
     * $query->filterByAbbr('fooValue');   // WHERE vgagco_abbr = 'fooValue'
     * $query->filterByAbbr('%fooValue%'); // WHERE vgagco_abbr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $abbr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByAbbr($abbr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($abbr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR, $abbr, $comparison);
    }

    /**
     * Filter the query on the vgagco_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagco_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagco_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagco_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagco_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagco_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagco_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagco_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagco_location_address column
     *
     * Example usage:
     * <code>
     * $query->filterByLocationAddress('fooValue');   // WHERE vgagco_location_address = 'fooValue'
     * $query->filterByLocationAddress('%fooValue%'); // WHERE vgagco_location_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $locationAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByLocationAddress($locationAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($locationAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS, $locationAddress, $comparison);
    }

    /**
     * Filter the query on the vgagco_location_city column
     *
     * Example usage:
     * <code>
     * $query->filterByLocationCity('fooValue');   // WHERE vgagco_location_city = 'fooValue'
     * $query->filterByLocationCity('%fooValue%'); // WHERE vgagco_location_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $locationCity The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByLocationCity($locationCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($locationCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY, $locationCity, $comparison);
    }

    /**
     * Filter the query on the vgagco_location_country column
     *
     * Example usage:
     * <code>
     * $query->filterByLocationCountry('fooValue');   // WHERE vgagco_location_country = 'fooValue'
     * $query->filterByLocationCountry('%fooValue%'); // WHERE vgagco_location_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $locationCountry The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByLocationCountry($locationCountry = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($locationCountry)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY, $locationCountry, $comparison);
    }

    /**
     * Filter the query on the vgagco_location_state column
     *
     * Example usage:
     * <code>
     * $query->filterByLocationState('fooValue');   // WHERE vgagco_location_state = 'fooValue'
     * $query->filterByLocationState('%fooValue%'); // WHERE vgagco_location_state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $locationState The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByLocationState($locationState = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($locationState)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE, $locationState, $comparison);
    }

    /**
     * Filter the query on the vgagco_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE vgagco_phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE vgagco_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the vgagco_website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE vgagco_website = 'fooValue'
     * $query->filterByWebsite('%fooValue%'); // WHERE vgagco_website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the vgagco_founded_at column
     *
     * Example usage:
     * <code>
     * $query->filterByFoundedAt('2011-03-14'); // WHERE vgagco_founded_at = '2011-03-14'
     * $query->filterByFoundedAt('now'); // WHERE vgagco_founded_at = '2011-03-14'
     * $query->filterByFoundedAt(array('max' => 'yesterday')); // WHERE vgagco_founded_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $foundedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByFoundedAt($foundedAt = null, $comparison = null)
    {
        if (is_array($foundedAt)) {
            $useMinMax = false;
            if (isset($foundedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT, $foundedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($foundedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT, $foundedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT, $foundedAt, $comparison);
    }

    /**
     * Filter the query on the vgagco_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagco_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagco_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagco_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagco_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagco_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagco_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagco_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagco_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagco_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagco_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagco_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagco_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagco_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagco_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagco_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagco_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagco_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagco_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagco_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagco_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagco_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagco_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagco_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagco_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagco_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagco_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform|ObjectCollection $apiGiantBombPlatform the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombPlatform($apiGiantBombPlatform, $comparison = null)
    {
        if ($apiGiantBombPlatform instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform) {
            return $this
                ->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombPlatform->getApiGiantBombCompanyId(), $comparison);
        } elseif ($apiGiantBombPlatform instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombPlatformQuery()
                ->filterByPrimaryKeys($apiGiantBombPlatform->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombPlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombPlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function joinApiGiantBombPlatform($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombPlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombPlatform');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombPlatform relation ApiGiantBombPlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombPlatformQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombPlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombPlatform', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper|ObjectCollection $apiGiantBombGameDeveloper the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameDeveloper($apiGiantBombGameDeveloper, $comparison = null)
    {
        if ($apiGiantBombGameDeveloper instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper) {
            return $this
                ->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombGameDeveloper->getApiGiantBombCompanyId(), $comparison);
        } elseif ($apiGiantBombGameDeveloper instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameDeveloperQuery()
                ->filterByPrimaryKeys($apiGiantBombGameDeveloper->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameDeveloper() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameDeveloper relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameDeveloper($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameDeveloper');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameDeveloper');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameDeveloper relation ApiGiantBombGameDeveloper object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameDeveloperQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameDeveloper($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameDeveloper', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher|ObjectCollection $apiGiantBombGamePublisher the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGamePublisher($apiGiantBombGamePublisher, $comparison = null)
    {
        if ($apiGiantBombGamePublisher instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher) {
            return $this
                ->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombGamePublisher->getApiGiantBombCompanyId(), $comparison);
        } elseif ($apiGiantBombGamePublisher instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGamePublisherQuery()
                ->filterByPrimaryKeys($apiGiantBombGamePublisher->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGamePublisher() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGamePublisher relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGamePublisher($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGamePublisher');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGamePublisher');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGamePublisher relation ApiGiantBombGamePublisher object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGamePublisherQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGamePublisher($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGamePublisher', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper|ObjectCollection $apiGiantBombGameReleaseDeveloper the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameReleaseDeveloper($apiGiantBombGameReleaseDeveloper, $comparison = null)
    {
        if ($apiGiantBombGameReleaseDeveloper instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper) {
            return $this
                ->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombGameReleaseDeveloper->getApiGiantBombCompanyId(), $comparison);
        } elseif ($apiGiantBombGameReleaseDeveloper instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseDeveloperQuery()
                ->filterByPrimaryKeys($apiGiantBombGameReleaseDeveloper->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameReleaseDeveloper() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameReleaseDeveloper($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameReleaseDeveloper');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameReleaseDeveloper');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameReleaseDeveloper relation ApiGiantBombGameReleaseDeveloper object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseDeveloperQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameReleaseDeveloper($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameReleaseDeveloper', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher|ObjectCollection $apiGiantBombGameReleasePublisher the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameReleasePublisher($apiGiantBombGameReleasePublisher, $comparison = null)
    {
        if ($apiGiantBombGameReleasePublisher instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher) {
            return $this
                ->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombGameReleasePublisher->getApiGiantBombCompanyId(), $comparison);
        } elseif ($apiGiantBombGameReleasePublisher instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleasePublisherQuery()
                ->filterByPrimaryKeys($apiGiantBombGameReleasePublisher->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameReleasePublisher() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameReleasePublisher($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameReleasePublisher');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameReleasePublisher');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameReleasePublisher relation ApiGiantBombGameReleasePublisher object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleasePublisherQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameReleasePublisher($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameReleasePublisher', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombCompany $apiGiantBombCompany Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombCompanyQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombCompany = null)
    {
        if ($apiGiantBombCompany) {
            $this->addUsingAlias(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $apiGiantBombCompany->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_company_vgagco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombCompanyTableMap::clearInstancePool();
            ApiGiantBombCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombCompanyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombCompanyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombCompanyQuery
