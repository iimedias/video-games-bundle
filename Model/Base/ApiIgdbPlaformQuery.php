<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaform as ChildApiIgdbPlaform;
use IiMedias\VideoGamesBundle\Model\ApiIgdbPlaformQuery as ChildApiIgdbPlaformQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiIgdbPlaformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_igdb_platform_vgaipl' table.
 *
 *
 *
 * @method     ChildApiIgdbPlaformQuery orderById($order = Criteria::ASC) Order by the vgaipl_id column
 * @method     ChildApiIgdbPlaformQuery orderByName($order = Criteria::ASC) Order by the vgaipl_name column
 * @method     ChildApiIgdbPlaformQuery orderBySlug($order = Criteria::ASC) Order by the vgaipl_slug column
 * @method     ChildApiIgdbPlaformQuery orderByUrl($order = Criteria::ASC) Order by the vgaipl_url column
 * @method     ChildApiIgdbPlaformQuery orderByWebSite($order = Criteria::ASC) Order by the vgaipl_website column
 * @method     ChildApiIgdbPlaformQuery orderBySummary($order = Criteria::ASC) Order by the vgaipl_summary column
 * @method     ChildApiIgdbPlaformQuery orderByAlternativeName($order = Criteria::ASC) Order by the vgaipl_alternative_name column
 * @method     ChildApiIgdbPlaformQuery orderByGeneration($order = Criteria::ASC) Order by the vgaipl_generation column
 * @method     ChildApiIgdbPlaformQuery orderByGames($order = Criteria::ASC) Order by the vgaipl_games column
 * @method     ChildApiIgdbPlaformQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgaipl_created_at column
 * @method     ChildApiIgdbPlaformQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgaipl_updated_at column
 *
 * @method     ChildApiIgdbPlaformQuery groupById() Group by the vgaipl_id column
 * @method     ChildApiIgdbPlaformQuery groupByName() Group by the vgaipl_name column
 * @method     ChildApiIgdbPlaformQuery groupBySlug() Group by the vgaipl_slug column
 * @method     ChildApiIgdbPlaformQuery groupByUrl() Group by the vgaipl_url column
 * @method     ChildApiIgdbPlaformQuery groupByWebSite() Group by the vgaipl_website column
 * @method     ChildApiIgdbPlaformQuery groupBySummary() Group by the vgaipl_summary column
 * @method     ChildApiIgdbPlaformQuery groupByAlternativeName() Group by the vgaipl_alternative_name column
 * @method     ChildApiIgdbPlaformQuery groupByGeneration() Group by the vgaipl_generation column
 * @method     ChildApiIgdbPlaformQuery groupByGames() Group by the vgaipl_games column
 * @method     ChildApiIgdbPlaformQuery groupByCreatedAt() Group by the vgaipl_created_at column
 * @method     ChildApiIgdbPlaformQuery groupByUpdatedAt() Group by the vgaipl_updated_at column
 *
 * @method     ChildApiIgdbPlaformQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiIgdbPlaformQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiIgdbPlaformQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiIgdbPlaformQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiIgdbPlaformQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiIgdbPlaformQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiIgdbPlaform findOne(ConnectionInterface $con = null) Return the first ChildApiIgdbPlaform matching the query
 * @method     ChildApiIgdbPlaform findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiIgdbPlaform matching the query, or a new ChildApiIgdbPlaform object populated from the query conditions when no match is found
 *
 * @method     ChildApiIgdbPlaform findOneById(int $vgaipl_id) Return the first ChildApiIgdbPlaform filtered by the vgaipl_id column
 * @method     ChildApiIgdbPlaform findOneByName(string $vgaipl_name) Return the first ChildApiIgdbPlaform filtered by the vgaipl_name column
 * @method     ChildApiIgdbPlaform findOneBySlug(string $vgaipl_slug) Return the first ChildApiIgdbPlaform filtered by the vgaipl_slug column
 * @method     ChildApiIgdbPlaform findOneByUrl(string $vgaipl_url) Return the first ChildApiIgdbPlaform filtered by the vgaipl_url column
 * @method     ChildApiIgdbPlaform findOneByWebSite(string $vgaipl_website) Return the first ChildApiIgdbPlaform filtered by the vgaipl_website column
 * @method     ChildApiIgdbPlaform findOneBySummary(string $vgaipl_summary) Return the first ChildApiIgdbPlaform filtered by the vgaipl_summary column
 * @method     ChildApiIgdbPlaform findOneByAlternativeName(string $vgaipl_alternative_name) Return the first ChildApiIgdbPlaform filtered by the vgaipl_alternative_name column
 * @method     ChildApiIgdbPlaform findOneByGeneration(int $vgaipl_generation) Return the first ChildApiIgdbPlaform filtered by the vgaipl_generation column
 * @method     ChildApiIgdbPlaform findOneByGames(array $vgaipl_games) Return the first ChildApiIgdbPlaform filtered by the vgaipl_games column
 * @method     ChildApiIgdbPlaform findOneByCreatedAt(string $vgaipl_created_at) Return the first ChildApiIgdbPlaform filtered by the vgaipl_created_at column
 * @method     ChildApiIgdbPlaform findOneByUpdatedAt(string $vgaipl_updated_at) Return the first ChildApiIgdbPlaform filtered by the vgaipl_updated_at column *

 * @method     ChildApiIgdbPlaform requirePk($key, ConnectionInterface $con = null) Return the ChildApiIgdbPlaform by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOne(ConnectionInterface $con = null) Return the first ChildApiIgdbPlaform matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiIgdbPlaform requireOneById(int $vgaipl_id) Return the first ChildApiIgdbPlaform filtered by the vgaipl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByName(string $vgaipl_name) Return the first ChildApiIgdbPlaform filtered by the vgaipl_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneBySlug(string $vgaipl_slug) Return the first ChildApiIgdbPlaform filtered by the vgaipl_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByUrl(string $vgaipl_url) Return the first ChildApiIgdbPlaform filtered by the vgaipl_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByWebSite(string $vgaipl_website) Return the first ChildApiIgdbPlaform filtered by the vgaipl_website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneBySummary(string $vgaipl_summary) Return the first ChildApiIgdbPlaform filtered by the vgaipl_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByAlternativeName(string $vgaipl_alternative_name) Return the first ChildApiIgdbPlaform filtered by the vgaipl_alternative_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByGeneration(int $vgaipl_generation) Return the first ChildApiIgdbPlaform filtered by the vgaipl_generation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByGames(array $vgaipl_games) Return the first ChildApiIgdbPlaform filtered by the vgaipl_games column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByCreatedAt(string $vgaipl_created_at) Return the first ChildApiIgdbPlaform filtered by the vgaipl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiIgdbPlaform requireOneByUpdatedAt(string $vgaipl_updated_at) Return the first ChildApiIgdbPlaform filtered by the vgaipl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiIgdbPlaform[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiIgdbPlaform objects based on current ModelCriteria
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findById(int $vgaipl_id) Return ChildApiIgdbPlaform objects filtered by the vgaipl_id column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByName(string $vgaipl_name) Return ChildApiIgdbPlaform objects filtered by the vgaipl_name column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findBySlug(string $vgaipl_slug) Return ChildApiIgdbPlaform objects filtered by the vgaipl_slug column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByUrl(string $vgaipl_url) Return ChildApiIgdbPlaform objects filtered by the vgaipl_url column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByWebSite(string $vgaipl_website) Return ChildApiIgdbPlaform objects filtered by the vgaipl_website column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findBySummary(string $vgaipl_summary) Return ChildApiIgdbPlaform objects filtered by the vgaipl_summary column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByAlternativeName(string $vgaipl_alternative_name) Return ChildApiIgdbPlaform objects filtered by the vgaipl_alternative_name column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByGeneration(int $vgaipl_generation) Return ChildApiIgdbPlaform objects filtered by the vgaipl_generation column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByGames(array $vgaipl_games) Return ChildApiIgdbPlaform objects filtered by the vgaipl_games column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByCreatedAt(string $vgaipl_created_at) Return ChildApiIgdbPlaform objects filtered by the vgaipl_created_at column
 * @method     ChildApiIgdbPlaform[]|ObjectCollection findByUpdatedAt(string $vgaipl_updated_at) Return ChildApiIgdbPlaform objects filtered by the vgaipl_updated_at column
 * @method     ChildApiIgdbPlaform[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiIgdbPlaformQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiIgdbPlaformQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbPlaform', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiIgdbPlaformQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiIgdbPlaformQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiIgdbPlaformQuery) {
            return $criteria;
        }
        $query = new ChildApiIgdbPlaformQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiIgdbPlaform|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiIgdbPlaformTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiIgdbPlaformTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiIgdbPlaform A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaipl_id, vgaipl_name, vgaipl_slug, vgaipl_url, vgaipl_website, vgaipl_summary, vgaipl_alternative_name, vgaipl_generation, vgaipl_games, vgaipl_created_at, vgaipl_updated_at FROM videogames_api_igdb_platform_vgaipl WHERE vgaipl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiIgdbPlaform $obj */
            $obj = new ChildApiIgdbPlaform();
            $obj->hydrate($row);
            ApiIgdbPlaformTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiIgdbPlaform|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaipl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaipl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaipl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaipl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaipl_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaipl_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaipl_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgaipl_slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE vgaipl_slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE vgaipl_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the vgaipl_url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE vgaipl_url = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE vgaipl_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the vgaipl_website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebSite('fooValue');   // WHERE vgaipl_website = 'fooValue'
     * $query->filterByWebSite('%fooValue%'); // WHERE vgaipl_website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $webSite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByWebSite($webSite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($webSite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_WEBSITE, $webSite, $comparison);
    }

    /**
     * Filter the query on the vgaipl_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgaipl_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgaipl_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgaipl_alternative_name column
     *
     * Example usage:
     * <code>
     * $query->filterByAlternativeName('fooValue');   // WHERE vgaipl_alternative_name = 'fooValue'
     * $query->filterByAlternativeName('%fooValue%'); // WHERE vgaipl_alternative_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alternativeName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByAlternativeName($alternativeName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alternativeName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ALTERNATIVE_NAME, $alternativeName, $comparison);
    }

    /**
     * Filter the query on the vgaipl_generation column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneration(1234); // WHERE vgaipl_generation = 1234
     * $query->filterByGeneration(array(12, 34)); // WHERE vgaipl_generation IN (12, 34)
     * $query->filterByGeneration(array('min' => 12)); // WHERE vgaipl_generation > 12
     * </code>
     *
     * @param     mixed $generation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByGeneration($generation = null, $comparison = null)
    {
        if (is_array($generation)) {
            $useMinMax = false;
            if (isset($generation['min'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION, $generation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($generation['max'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION, $generation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_GENERATION, $generation, $comparison);
    }

    /**
     * Filter the query on the vgaipl_games column
     *
     * @param     array $games The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByGames($games = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($games as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($games as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($games as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES, $games, $comparison);
    }

    /**
     * Filter the query on the vgaipl_games column
     * @param     mixed $games The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByGame($games = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($games)) {
                $games = '%| ' . $games . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $games = '%| ' . $games . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $games, $comparison);
            } else {
                $this->addAnd($key, $games, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_GAMES, $games, $comparison);
    }

    /**
     * Filter the query on the vgaipl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgaipl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgaipl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgaipl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgaipl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgaipl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgaipl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgaipl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiIgdbPlaform $apiIgdbPlaform Object to remove from the list of results
     *
     * @return $this|ChildApiIgdbPlaformQuery The current query, for fluid interface
     */
    public function prune($apiIgdbPlaform = null)
    {
        if ($apiIgdbPlaform) {
            $this->addUsingAlias(ApiIgdbPlaformTableMap::COL_VGAIPL_ID, $apiIgdbPlaform->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_igdb_platform_vgaipl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbPlaformTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiIgdbPlaformTableMap::clearInstancePool();
            ApiIgdbPlaformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbPlaformTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiIgdbPlaformTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiIgdbPlaformTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiIgdbPlaformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiIgdbPlaformQuery
