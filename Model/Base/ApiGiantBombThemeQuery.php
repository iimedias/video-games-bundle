<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombTheme as ChildApiGiantBombTheme;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombThemeQuery as ChildApiGiantBombThemeQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombThemeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_theme_vgagth' table.
 *
 *
 *
 * @method     ChildApiGiantBombThemeQuery orderById($order = Criteria::ASC) Order by the vgagth_id column
 * @method     ChildApiGiantBombThemeQuery orderByName($order = Criteria::ASC) Order by the vgagth_name column
 * @method     ChildApiGiantBombThemeQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagth_api_detail_url column
 * @method     ChildApiGiantBombThemeQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagth_site_detail_url column
 *
 * @method     ChildApiGiantBombThemeQuery groupById() Group by the vgagth_id column
 * @method     ChildApiGiantBombThemeQuery groupByName() Group by the vgagth_name column
 * @method     ChildApiGiantBombThemeQuery groupByApiDetailUrl() Group by the vgagth_api_detail_url column
 * @method     ChildApiGiantBombThemeQuery groupBySiteDetailUrl() Group by the vgagth_site_detail_url column
 *
 * @method     ChildApiGiantBombThemeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombThemeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombThemeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombThemeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombThemeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombThemeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombTheme findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombTheme matching the query
 * @method     ChildApiGiantBombTheme findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombTheme matching the query, or a new ChildApiGiantBombTheme object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombTheme findOneById(int $vgagth_id) Return the first ChildApiGiantBombTheme filtered by the vgagth_id column
 * @method     ChildApiGiantBombTheme findOneByName(string $vgagth_name) Return the first ChildApiGiantBombTheme filtered by the vgagth_name column
 * @method     ChildApiGiantBombTheme findOneByApiDetailUrl(string $vgagth_api_detail_url) Return the first ChildApiGiantBombTheme filtered by the vgagth_api_detail_url column
 * @method     ChildApiGiantBombTheme findOneBySiteDetailUrl(string $vgagth_site_detail_url) Return the first ChildApiGiantBombTheme filtered by the vgagth_site_detail_url column *

 * @method     ChildApiGiantBombTheme requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombTheme by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombTheme requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombTheme matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombTheme requireOneById(int $vgagth_id) Return the first ChildApiGiantBombTheme filtered by the vgagth_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombTheme requireOneByName(string $vgagth_name) Return the first ChildApiGiantBombTheme filtered by the vgagth_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombTheme requireOneByApiDetailUrl(string $vgagth_api_detail_url) Return the first ChildApiGiantBombTheme filtered by the vgagth_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombTheme requireOneBySiteDetailUrl(string $vgagth_site_detail_url) Return the first ChildApiGiantBombTheme filtered by the vgagth_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombTheme[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombTheme objects based on current ModelCriteria
 * @method     ChildApiGiantBombTheme[]|ObjectCollection findById(int $vgagth_id) Return ChildApiGiantBombTheme objects filtered by the vgagth_id column
 * @method     ChildApiGiantBombTheme[]|ObjectCollection findByName(string $vgagth_name) Return ChildApiGiantBombTheme objects filtered by the vgagth_name column
 * @method     ChildApiGiantBombTheme[]|ObjectCollection findByApiDetailUrl(string $vgagth_api_detail_url) Return ChildApiGiantBombTheme objects filtered by the vgagth_api_detail_url column
 * @method     ChildApiGiantBombTheme[]|ObjectCollection findBySiteDetailUrl(string $vgagth_site_detail_url) Return ChildApiGiantBombTheme objects filtered by the vgagth_site_detail_url column
 * @method     ChildApiGiantBombTheme[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombThemeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombThemeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombTheme', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombThemeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombThemeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombThemeQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombThemeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombTheme|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombThemeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombThemeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombTheme A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagth_id, vgagth_name, vgagth_api_detail_url, vgagth_site_detail_url FROM videogames_api_giantbomb_theme_vgagth WHERE vgagth_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombTheme $obj */
            $obj = new ChildApiGiantBombTheme();
            $obj->hydrate($row);
            ApiGiantBombThemeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombTheme|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagth_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagth_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagth_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagth_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagth_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagth_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagth_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagth_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagth_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagth_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagth_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagth_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagth_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombTheme $apiGiantBombTheme Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombThemeQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombTheme = null)
    {
        if ($apiGiantBombTheme) {
            $this->addUsingAlias(ApiGiantBombThemeTableMap::COL_VGAGTH_ID, $apiGiantBombTheme->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_theme_vgagth table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombThemeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombThemeTableMap::clearInstancePool();
            ApiGiantBombThemeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombThemeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombThemeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombThemeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombThemeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombThemeQuery
