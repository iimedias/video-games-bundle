<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteGameKultPlatform as ChildSiteGameKultPlatform;
use IiMedias\VideoGamesBundle\Model\SiteGameKultPlatformQuery as ChildSiteGameKultPlatformQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultPlatformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_gamekult_platform_vgakpl' table.
 *
 *
 *
 * @method     ChildSiteGameKultPlatformQuery orderById($order = Criteria::ASC) Order by the vgakpl_id column
 * @method     ChildSiteGameKultPlatformQuery orderByName($order = Criteria::ASC) Order by the vgakpl_name column
 * @method     ChildSiteGameKultPlatformQuery orderByAbbr($order = Criteria::ASC) Order by the vgakpl_abbr column
 * @method     ChildSiteGameKultPlatformQuery orderByListUrlAbbr($order = Criteria::ASC) Order by the vgakpl_list_url_abbr column
 * @method     ChildSiteGameKultPlatformQuery orderByRefreshedAt($order = Criteria::ASC) Order by the vgakpl_refreshed_at column
 *
 * @method     ChildSiteGameKultPlatformQuery groupById() Group by the vgakpl_id column
 * @method     ChildSiteGameKultPlatformQuery groupByName() Group by the vgakpl_name column
 * @method     ChildSiteGameKultPlatformQuery groupByAbbr() Group by the vgakpl_abbr column
 * @method     ChildSiteGameKultPlatformQuery groupByListUrlAbbr() Group by the vgakpl_list_url_abbr column
 * @method     ChildSiteGameKultPlatformQuery groupByRefreshedAt() Group by the vgakpl_refreshed_at column
 *
 * @method     ChildSiteGameKultPlatformQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteGameKultPlatformQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteGameKultPlatformQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteGameKultPlatformQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteGameKultPlatformQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteGameKultPlatformQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteGameKultPlatformQuery leftJoinSiteGameKultGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultPlatformQuery rightJoinSiteGameKultGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultPlatformQuery innerJoinSiteGameKultGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameKultGameRelease relation
 *
 * @method     ChildSiteGameKultPlatformQuery joinWithSiteGameKultGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameKultGameRelease relation
 *
 * @method     ChildSiteGameKultPlatformQuery leftJoinWithSiteGameKultGameRelease() Adds a LEFT JOIN clause and with to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultPlatformQuery rightJoinWithSiteGameKultGameRelease() Adds a RIGHT JOIN clause and with to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultPlatformQuery innerJoinWithSiteGameKultGameRelease() Adds a INNER JOIN clause and with to the query using the SiteGameKultGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteGameKultPlatform findOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultPlatform matching the query
 * @method     ChildSiteGameKultPlatform findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteGameKultPlatform matching the query, or a new ChildSiteGameKultPlatform object populated from the query conditions when no match is found
 *
 * @method     ChildSiteGameKultPlatform findOneById(int $vgakpl_id) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_id column
 * @method     ChildSiteGameKultPlatform findOneByName(string $vgakpl_name) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_name column
 * @method     ChildSiteGameKultPlatform findOneByAbbr(string $vgakpl_abbr) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_abbr column
 * @method     ChildSiteGameKultPlatform findOneByListUrlAbbr(string $vgakpl_list_url_abbr) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_list_url_abbr column
 * @method     ChildSiteGameKultPlatform findOneByRefreshedAt(string $vgakpl_refreshed_at) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_refreshed_at column *

 * @method     ChildSiteGameKultPlatform requirePk($key, ConnectionInterface $con = null) Return the ChildSiteGameKultPlatform by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultPlatform requireOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultPlatform matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultPlatform requireOneById(int $vgakpl_id) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultPlatform requireOneByName(string $vgakpl_name) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultPlatform requireOneByAbbr(string $vgakpl_abbr) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_abbr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultPlatform requireOneByListUrlAbbr(string $vgakpl_list_url_abbr) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_list_url_abbr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultPlatform requireOneByRefreshedAt(string $vgakpl_refreshed_at) Return the first ChildSiteGameKultPlatform filtered by the vgakpl_refreshed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteGameKultPlatform objects based on current ModelCriteria
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection findById(int $vgakpl_id) Return ChildSiteGameKultPlatform objects filtered by the vgakpl_id column
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection findByName(string $vgakpl_name) Return ChildSiteGameKultPlatform objects filtered by the vgakpl_name column
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection findByAbbr(string $vgakpl_abbr) Return ChildSiteGameKultPlatform objects filtered by the vgakpl_abbr column
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection findByListUrlAbbr(string $vgakpl_list_url_abbr) Return ChildSiteGameKultPlatform objects filtered by the vgakpl_list_url_abbr column
 * @method     ChildSiteGameKultPlatform[]|ObjectCollection findByRefreshedAt(string $vgakpl_refreshed_at) Return ChildSiteGameKultPlatform objects filtered by the vgakpl_refreshed_at column
 * @method     ChildSiteGameKultPlatform[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteGameKultPlatformQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteGameKultPlatformQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultPlatform', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteGameKultPlatformQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteGameKultPlatformQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteGameKultPlatformQuery) {
            return $criteria;
        }
        $query = new ChildSiteGameKultPlatformQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteGameKultPlatform|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameKultPlatformTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteGameKultPlatformTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultPlatform A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgakpl_id, vgakpl_name, vgakpl_abbr, vgakpl_list_url_abbr, vgakpl_refreshed_at FROM videogames_site_gamekult_platform_vgakpl WHERE vgakpl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteGameKultPlatform $obj */
            $obj = new ChildSiteGameKultPlatform();
            $obj->hydrate($row);
            SiteGameKultPlatformTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteGameKultPlatform|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgakpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgakpl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgakpl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgakpl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgakpl_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgakpl_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgakpl_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgakpl_abbr column
     *
     * Example usage:
     * <code>
     * $query->filterByAbbr('fooValue');   // WHERE vgakpl_abbr = 'fooValue'
     * $query->filterByAbbr('%fooValue%'); // WHERE vgakpl_abbr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $abbr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByAbbr($abbr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($abbr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ABBR, $abbr, $comparison);
    }

    /**
     * Filter the query on the vgakpl_list_url_abbr column
     *
     * Example usage:
     * <code>
     * $query->filterByListUrlAbbr('fooValue');   // WHERE vgakpl_list_url_abbr = 'fooValue'
     * $query->filterByListUrlAbbr('%fooValue%'); // WHERE vgakpl_list_url_abbr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $listUrlAbbr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByListUrlAbbr($listUrlAbbr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($listUrlAbbr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_LIST_URL_ABBR, $listUrlAbbr, $comparison);
    }

    /**
     * Filter the query on the vgakpl_refreshed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRefreshedAt('2011-03-14'); // WHERE vgakpl_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt('now'); // WHERE vgakpl_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt(array('max' => 'yesterday')); // WHERE vgakpl_refreshed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $refreshedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterByRefreshedAt($refreshedAt = null, $comparison = null)
    {
        if (is_array($refreshedAt)) {
            $useMinMax = false;
            if (isset($refreshedAt['min'])) {
                $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_REFRESHED_AT, $refreshedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refreshedAt['max'])) {
                $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_REFRESHED_AT, $refreshedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_REFRESHED_AT, $refreshedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease|ObjectCollection $siteGameKultGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultGameRelease($siteGameKultGameRelease, $comparison = null)
    {
        if ($siteGameKultGameRelease instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease) {
            return $this
                ->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $siteGameKultGameRelease->getSiteGameKultPlatformId(), $comparison);
        } elseif ($siteGameKultGameRelease instanceof ObjectCollection) {
            return $this
                ->useSiteGameKultGameReleaseQuery()
                ->filterByPrimaryKeys($siteGameKultGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteGameKultGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameKultGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function joinSiteGameKultGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameKultGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameKultGameRelease');
        }

        return $this;
    }

    /**
     * Use the SiteGameKultGameRelease relation SiteGameKultGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameKultGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameKultGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameKultGameRelease', '\IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteGameKultPlatform $siteGameKultPlatform Object to remove from the list of results
     *
     * @return $this|ChildSiteGameKultPlatformQuery The current query, for fluid interface
     */
    public function prune($siteGameKultPlatform = null)
    {
        if ($siteGameKultPlatform) {
            $this->addUsingAlias(SiteGameKultPlatformTableMap::COL_VGAKPL_ID, $siteGameKultPlatform->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_gamekult_platform_vgakpl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultPlatformTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteGameKultPlatformTableMap::clearInstancePool();
            SiteGameKultPlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultPlatformTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteGameKultPlatformTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteGameKultPlatformTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteGameKultPlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteGameKultPlatformQuery
