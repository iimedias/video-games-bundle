<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard as ChildApiGiantBombRatingBoard;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery as ChildApiGiantBombRatingBoardQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombRatingBoardTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_rating_board_vgagrb' table.
 *
 *
 *
 * @method     ChildApiGiantBombRatingBoardQuery orderById($order = Criteria::ASC) Order by the vgagrb_id column
 * @method     ChildApiGiantBombRatingBoardQuery orderByName($order = Criteria::ASC) Order by the vgagrb_name column
 * @method     ChildApiGiantBombRatingBoardQuery orderBySummary($order = Criteria::ASC) Order by the vgagrb_summary column
 * @method     ChildApiGiantBombRatingBoardQuery orderByDescription($order = Criteria::ASC) Order by the vgagrb_description column
 * @method     ChildApiGiantBombRatingBoardQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagrb_api_detail_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagrb_site_detail_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagrb_image_icon_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagrb_image_medium_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagrb_image_screen_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagrb_image_small_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagrb_image_super_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagrb_image_thumb_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagrb_image_tiny_url column
 * @method     ChildApiGiantBombRatingBoardQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagrb_created_at column
 * @method     ChildApiGiantBombRatingBoardQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagrb_updated_at column
 *
 * @method     ChildApiGiantBombRatingBoardQuery groupById() Group by the vgagrb_id column
 * @method     ChildApiGiantBombRatingBoardQuery groupByName() Group by the vgagrb_name column
 * @method     ChildApiGiantBombRatingBoardQuery groupBySummary() Group by the vgagrb_summary column
 * @method     ChildApiGiantBombRatingBoardQuery groupByDescription() Group by the vgagrb_description column
 * @method     ChildApiGiantBombRatingBoardQuery groupByApiDetailUrl() Group by the vgagrb_api_detail_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupBySiteDetailUrl() Group by the vgagrb_site_detail_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageIconUrl() Group by the vgagrb_image_icon_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageMediumUrl() Group by the vgagrb_image_medium_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageScreenUrl() Group by the vgagrb_image_screen_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageSmallUrl() Group by the vgagrb_image_small_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageSuperUrl() Group by the vgagrb_image_super_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageThumbUrl() Group by the vgagrb_image_thumb_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByImageTinyUrl() Group by the vgagrb_image_tiny_url column
 * @method     ChildApiGiantBombRatingBoardQuery groupByCreatedAt() Group by the vgagrb_created_at column
 * @method     ChildApiGiantBombRatingBoardQuery groupByUpdatedAt() Group by the vgagrb_updated_at column
 *
 * @method     ChildApiGiantBombRatingBoardQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombRatingBoardQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombRatingBoardQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombRatingBoardQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombRatingBoardQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombRatingBoardQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombRatingBoardQuery leftJoinApiGiantBombRatingGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombRatingGame relation
 * @method     ChildApiGiantBombRatingBoardQuery rightJoinApiGiantBombRatingGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombRatingGame relation
 * @method     ChildApiGiantBombRatingBoardQuery innerJoinApiGiantBombRatingGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombRatingGame relation
 *
 * @method     ChildApiGiantBombRatingBoardQuery joinWithApiGiantBombRatingGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombRatingGame relation
 *
 * @method     ChildApiGiantBombRatingBoardQuery leftJoinWithApiGiantBombRatingGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombRatingGame relation
 * @method     ChildApiGiantBombRatingBoardQuery rightJoinWithApiGiantBombRatingGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombRatingGame relation
 * @method     ChildApiGiantBombRatingBoardQuery innerJoinWithApiGiantBombRatingGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombRatingGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombRatingBoard findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombRatingBoard matching the query
 * @method     ChildApiGiantBombRatingBoard findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombRatingBoard matching the query, or a new ChildApiGiantBombRatingBoard object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombRatingBoard findOneById(int $vgagrb_id) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_id column
 * @method     ChildApiGiantBombRatingBoard findOneByName(string $vgagrb_name) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_name column
 * @method     ChildApiGiantBombRatingBoard findOneBySummary(string $vgagrb_summary) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_summary column
 * @method     ChildApiGiantBombRatingBoard findOneByDescription(string $vgagrb_description) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_description column
 * @method     ChildApiGiantBombRatingBoard findOneByApiDetailUrl(string $vgagrb_api_detail_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_api_detail_url column
 * @method     ChildApiGiantBombRatingBoard findOneBySiteDetailUrl(string $vgagrb_site_detail_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_site_detail_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageIconUrl(string $vgagrb_image_icon_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_icon_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageMediumUrl(string $vgagrb_image_medium_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_medium_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageScreenUrl(string $vgagrb_image_screen_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_screen_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageSmallUrl(string $vgagrb_image_small_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_small_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageSuperUrl(string $vgagrb_image_super_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_super_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageThumbUrl(string $vgagrb_image_thumb_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_thumb_url column
 * @method     ChildApiGiantBombRatingBoard findOneByImageTinyUrl(string $vgagrb_image_tiny_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_tiny_url column
 * @method     ChildApiGiantBombRatingBoard findOneByCreatedAt(string $vgagrb_created_at) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_created_at column
 * @method     ChildApiGiantBombRatingBoard findOneByUpdatedAt(string $vgagrb_updated_at) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_updated_at column *

 * @method     ChildApiGiantBombRatingBoard requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombRatingBoard by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombRatingBoard matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombRatingBoard requireOneById(int $vgagrb_id) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByName(string $vgagrb_name) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneBySummary(string $vgagrb_summary) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByDescription(string $vgagrb_description) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByApiDetailUrl(string $vgagrb_api_detail_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneBySiteDetailUrl(string $vgagrb_site_detail_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageIconUrl(string $vgagrb_image_icon_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageMediumUrl(string $vgagrb_image_medium_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageScreenUrl(string $vgagrb_image_screen_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageSmallUrl(string $vgagrb_image_small_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageSuperUrl(string $vgagrb_image_super_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageThumbUrl(string $vgagrb_image_thumb_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByImageTinyUrl(string $vgagrb_image_tiny_url) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByCreatedAt(string $vgagrb_created_at) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombRatingBoard requireOneByUpdatedAt(string $vgagrb_updated_at) Return the first ChildApiGiantBombRatingBoard filtered by the vgagrb_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombRatingBoard objects based on current ModelCriteria
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findById(int $vgagrb_id) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_id column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByName(string $vgagrb_name) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_name column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findBySummary(string $vgagrb_summary) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_summary column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByDescription(string $vgagrb_description) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_description column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByApiDetailUrl(string $vgagrb_api_detail_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_api_detail_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findBySiteDetailUrl(string $vgagrb_site_detail_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_site_detail_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageIconUrl(string $vgagrb_image_icon_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_icon_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageMediumUrl(string $vgagrb_image_medium_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_medium_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageScreenUrl(string $vgagrb_image_screen_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_screen_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageSmallUrl(string $vgagrb_image_small_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_small_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageSuperUrl(string $vgagrb_image_super_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_super_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageThumbUrl(string $vgagrb_image_thumb_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_thumb_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByImageTinyUrl(string $vgagrb_image_tiny_url) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_image_tiny_url column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByCreatedAt(string $vgagrb_created_at) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_created_at column
 * @method     ChildApiGiantBombRatingBoard[]|ObjectCollection findByUpdatedAt(string $vgagrb_updated_at) Return ChildApiGiantBombRatingBoard objects filtered by the vgagrb_updated_at column
 * @method     ChildApiGiantBombRatingBoard[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombRatingBoardQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombRatingBoardQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombRatingBoard', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombRatingBoardQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombRatingBoardQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombRatingBoardQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombRatingBoardQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombRatingBoard|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombRatingBoardTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombRatingBoard A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagrb_id, vgagrb_name, vgagrb_summary, vgagrb_description, vgagrb_api_detail_url, vgagrb_site_detail_url, vgagrb_image_icon_url, vgagrb_image_medium_url, vgagrb_image_screen_url, vgagrb_image_small_url, vgagrb_image_super_url, vgagrb_image_thumb_url, vgagrb_image_tiny_url, vgagrb_created_at, vgagrb_updated_at FROM videogames_api_giantbomb_rating_board_vgagrb WHERE vgagrb_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombRatingBoard $obj */
            $obj = new ChildApiGiantBombRatingBoard();
            $obj->hydrate($row);
            ApiGiantBombRatingBoardTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombRatingBoard|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagrb_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagrb_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagrb_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagrb_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagrb_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagrb_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagrb_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagrb_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagrb_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagrb_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagrb_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagrb_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagrb_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagrb_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagrb_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagrb_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagrb_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagrb_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagrb_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagrb_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagrb_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagrb_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagrb_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagrb_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagrb_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagrb_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagrb_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagrb_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagrb_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagrb_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagrb_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagrb_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrb_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagrb_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagrb_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagrb_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagrb_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagrb_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagrb_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagrb_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating|ObjectCollection $apiGiantBombGameRating the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombRatingGame($apiGiantBombGameRating, $comparison = null)
    {
        if ($apiGiantBombGameRating instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating) {
            return $this
                ->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $apiGiantBombGameRating->getGiantBombRatingBoardId(), $comparison);
        } elseif ($apiGiantBombGameRating instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombRatingGameQuery()
                ->filterByPrimaryKeys($apiGiantBombGameRating->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombRatingGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombRatingGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function joinApiGiantBombRatingGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombRatingGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombRatingGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombRatingGame relation ApiGiantBombGameRating object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombRatingGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombRatingGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombRatingGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombRatingBoard $apiGiantBombRatingBoard Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombRatingBoardQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombRatingBoard = null)
    {
        if ($apiGiantBombRatingBoard) {
            $this->addUsingAlias(ApiGiantBombRatingBoardTableMap::COL_VGAGRB_ID, $apiGiantBombRatingBoard->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_rating_board_vgagrb table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombRatingBoardTableMap::clearInstancePool();
            ApiGiantBombRatingBoardTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombRatingBoardTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombRatingBoardTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombRatingBoardTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombRatingBoardQuery
