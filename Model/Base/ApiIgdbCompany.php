<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiIgdbCompanyQuery as ChildApiIgdbCompanyQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiIgdbCompanyTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_igdb_company_vgaico' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiIgdbCompany implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiIgdbCompanyTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgaico_id field.
     *
     * @var        int
     */
    protected $vgaico_id;

    /**
     * The value for the vgaico_parent_id field.
     *
     * @var        int
     */
    protected $vgaico_parent_id;

    /**
     * The value for the vgaico_name field.
     *
     * @var        string
     */
    protected $vgaico_name;

    /**
     * The value for the vgaico_slug field.
     *
     * @var        string
     */
    protected $vgaico_slug;

    /**
     * The value for the vgaico_url field.
     *
     * @var        string
     */
    protected $vgaico_url;

    /**
     * The value for the vgaico_website field.
     *
     * @var        string
     */
    protected $vgaico_website;

    /**
     * The value for the vgaico_twitter field.
     *
     * @var        string
     */
    protected $vgaico_twitter;

    /**
     * The value for the vgaico_country field.
     *
     * @var        int
     */
    protected $vgaico_country;

    /**
     * The value for the vgaico_logo_cloudinary_id field.
     *
     * @var        string
     */
    protected $vgaico_logo_cloudinary_id;

    /**
     * The value for the vgaico_logo_width field.
     *
     * @var        int
     */
    protected $vgaico_logo_width;

    /**
     * The value for the vgaico_logo_height field.
     *
     * @var        int
     */
    protected $vgaico_logo_height;

    /**
     * The value for the vgaico_description field.
     *
     * @var        string
     */
    protected $vgaico_description;

    /**
     * The value for the vgaico_developed field.
     *
     * @var        array
     */
    protected $vgaico_developed;

    /**
     * The unserialized $vgaico_developed value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgaico_developed_unserialized;

    /**
     * The value for the vgaico_published field.
     *
     * @var        array
     */
    protected $vgaico_published;

    /**
     * The unserialized $vgaico_published value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgaico_published_unserialized;

    /**
     * The value for the vgaico_start_date field.
     *
     * @var        DateTime
     */
    protected $vgaico_start_date;

    /**
     * The value for the vgaico_start_date_category field.
     *
     * @var        int
     */
    protected $vgaico_start_date_category;

    /**
     * The value for the vgaico_change_date field.
     *
     * @var        DateTime
     */
    protected $vgaico_change_date;

    /**
     * The value for the vgaico_change_date_category field.
     *
     * @var        int
     */
    protected $vgaico_change_date_category;

    /**
     * The value for the vgaico_created_at field.
     *
     * @var        DateTime
     */
    protected $vgaico_created_at;

    /**
     * The value for the vgaico_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgaico_updated_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiIgdbCompany object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiIgdbCompany</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiIgdbCompany</code>, delegates to
     * <code>equals(ApiIgdbCompany)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiIgdbCompany The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgaico_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgaico_id;
    }

    /**
     * Get the [vgaico_parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->vgaico_parent_id;
    }

    /**
     * Get the [vgaico_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgaico_name;
    }

    /**
     * Get the [vgaico_slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->vgaico_slug;
    }

    /**
     * Get the [vgaico_url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->vgaico_url;
    }

    /**
     * Get the [vgaico_website] column value.
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->vgaico_website;
    }

    /**
     * Get the [vgaico_twitter] column value.
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->vgaico_twitter;
    }

    /**
     * Get the [vgaico_country] column value.
     *
     * @return int
     */
    public function getCountry()
    {
        return $this->vgaico_country;
    }

    /**
     * Get the [vgaico_logo_cloudinary_id] column value.
     *
     * @return string
     */
    public function getLogoCloudinaryId()
    {
        return $this->vgaico_logo_cloudinary_id;
    }

    /**
     * Get the [vgaico_logo_width] column value.
     *
     * @return int
     */
    public function getLogoWidth()
    {
        return $this->vgaico_logo_width;
    }

    /**
     * Get the [vgaico_logo_height] column value.
     *
     * @return int
     */
    public function getLogoHeight()
    {
        return $this->vgaico_logo_height;
    }

    /**
     * Get the [vgaico_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgaico_description;
    }

    /**
     * Get the [vgaico_developed] column value.
     *
     * @return array
     */
    public function getDeveloped()
    {
        if (null === $this->vgaico_developed_unserialized) {
            $this->vgaico_developed_unserialized = array();
        }
        if (!$this->vgaico_developed_unserialized && null !== $this->vgaico_developed) {
            $vgaico_developed_unserialized = substr($this->vgaico_developed, 2, -2);
            $this->vgaico_developed_unserialized = $vgaico_developed_unserialized ? explode(' | ', $vgaico_developed_unserialized) : array();
        }

        return $this->vgaico_developed_unserialized;
    }

    /**
     * Get the [vgaico_published] column value.
     *
     * @return array
     */
    public function getPublished()
    {
        if (null === $this->vgaico_published_unserialized) {
            $this->vgaico_published_unserialized = array();
        }
        if (!$this->vgaico_published_unserialized && null !== $this->vgaico_published) {
            $vgaico_published_unserialized = substr($this->vgaico_published, 2, -2);
            $this->vgaico_published_unserialized = $vgaico_published_unserialized ? explode(' | ', $vgaico_published_unserialized) : array();
        }

        return $this->vgaico_published_unserialized;
    }

    /**
     * Get the [optionally formatted] temporal [vgaico_start_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartDate($format = NULL)
    {
        if ($format === null) {
            return $this->vgaico_start_date;
        } else {
            return $this->vgaico_start_date instanceof \DateTimeInterface ? $this->vgaico_start_date->format($format) : null;
        }
    }

    /**
     * Get the [vgaico_start_date_category] column value.
     *
     * @return int
     */
    public function getStartDateCategory()
    {
        return $this->vgaico_start_date_category;
    }

    /**
     * Get the [optionally formatted] temporal [vgaico_change_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getChangeDate($format = NULL)
    {
        if ($format === null) {
            return $this->vgaico_change_date;
        } else {
            return $this->vgaico_change_date instanceof \DateTimeInterface ? $this->vgaico_change_date->format($format) : null;
        }
    }

    /**
     * Get the [vgaico_change_date_category] column value.
     *
     * @return int
     */
    public function getChangeDateCategory()
    {
        return $this->vgaico_change_date_category;
    }

    /**
     * Get the [optionally formatted] temporal [vgaico_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgaico_created_at;
        } else {
            return $this->vgaico_created_at instanceof \DateTimeInterface ? $this->vgaico_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgaico_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgaico_updated_at;
        } else {
            return $this->vgaico_updated_at instanceof \DateTimeInterface ? $this->vgaico_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgaico_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_id !== $v) {
            $this->vgaico_id = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgaico_parent_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_parent_id !== $v) {
            $this->vgaico_parent_id = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID] = true;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [vgaico_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_name !== $v) {
            $this->vgaico_name = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgaico_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_slug !== $v) {
            $this->vgaico_slug = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [vgaico_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_url !== $v) {
            $this->vgaico_url = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [vgaico_website] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setWebSite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_website !== $v) {
            $this->vgaico_website = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE] = true;
        }

        return $this;
    } // setWebSite()

    /**
     * Set the value of [vgaico_twitter] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setTwitter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_twitter !== $v) {
            $this->vgaico_twitter = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER] = true;
        }

        return $this;
    } // setTwitter()

    /**
     * Set the value of [vgaico_country] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_country !== $v) {
            $this->vgaico_country = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Set the value of [vgaico_logo_cloudinary_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setLogoCloudinaryId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_logo_cloudinary_id !== $v) {
            $this->vgaico_logo_cloudinary_id = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID] = true;
        }

        return $this;
    } // setLogoCloudinaryId()

    /**
     * Set the value of [vgaico_logo_width] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setLogoWidth($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_logo_width !== $v) {
            $this->vgaico_logo_width = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH] = true;
        }

        return $this;
    } // setLogoWidth()

    /**
     * Set the value of [vgaico_logo_height] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setLogoHeight($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_logo_height !== $v) {
            $this->vgaico_logo_height = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT] = true;
        }

        return $this;
    } // setLogoHeight()

    /**
     * Set the value of [vgaico_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgaico_description !== $v) {
            $this->vgaico_description = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgaico_developed] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setDeveloped($v)
    {
        if ($this->vgaico_developed_unserialized !== $v) {
            $this->vgaico_developed_unserialized = $v;
            $this->vgaico_developed = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED] = true;
        }

        return $this;
    } // setDeveloped()

    /**
     * Set the value of [vgaico_published] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setPublished($v)
    {
        if ($this->vgaico_published_unserialized !== $v) {
            $this->vgaico_published_unserialized = $v;
            $this->vgaico_published = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED] = true;
        }

        return $this;
    } // setPublished()

    /**
     * Sets the value of [vgaico_start_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setStartDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgaico_start_date !== null || $dt !== null) {
            if ($this->vgaico_start_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgaico_start_date->format("Y-m-d H:i:s.u")) {
                $this->vgaico_start_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setStartDate()

    /**
     * Set the value of [vgaico_start_date_category] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setStartDateCategory($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_start_date_category !== $v) {
            $this->vgaico_start_date_category = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY] = true;
        }

        return $this;
    } // setStartDateCategory()

    /**
     * Sets the value of [vgaico_change_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setChangeDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgaico_change_date !== null || $dt !== null) {
            if ($this->vgaico_change_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgaico_change_date->format("Y-m-d H:i:s.u")) {
                $this->vgaico_change_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setChangeDate()

    /**
     * Set the value of [vgaico_change_date_category] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setChangeDateCategory($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgaico_change_date_category !== $v) {
            $this->vgaico_change_date_category = $v;
            $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY] = true;
        }

        return $this;
    } // setChangeDateCategory()

    /**
     * Sets the value of [vgaico_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgaico_created_at !== null || $dt !== null) {
            if ($this->vgaico_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgaico_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgaico_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgaico_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgaico_updated_at !== null || $dt !== null) {
            if ($this->vgaico_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgaico_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgaico_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('WebSite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Twitter', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_twitter = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_country = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('LogoCloudinaryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_logo_cloudinary_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('LogoWidth', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_logo_width = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('LogoHeight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_logo_height = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Developed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_developed = $col;
            $this->vgaico_developed_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('Published', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_published = $col;
            $this->vgaico_published_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('StartDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgaico_start_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('StartDateCategory', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_start_date_category = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('ChangeDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgaico_change_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('ChangeDateCategory', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgaico_change_date_category = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgaico_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiIgdbCompanyTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgaico_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 20; // 20 = ApiIgdbCompanyTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiIgdbCompany'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiIgdbCompanyQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiIgdbCompany::setDeleted()
     * @see ApiIgdbCompany::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiIgdbCompanyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiIgdbCompanyTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiIgdbCompanyTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_id';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_parent_id';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_name';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_slug';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_url';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_website';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_twitter';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_country';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_logo_cloudinary_id';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_logo_width';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_logo_height';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_description';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_developed';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_published';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_start_date';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_start_date_category';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_change_date';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_change_date_category';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_created_at';
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgaico_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_igdb_company_vgaico (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgaico_id':
                        $stmt->bindValue($identifier, $this->vgaico_id, PDO::PARAM_INT);
                        break;
                    case 'vgaico_parent_id':
                        $stmt->bindValue($identifier, $this->vgaico_parent_id, PDO::PARAM_INT);
                        break;
                    case 'vgaico_name':
                        $stmt->bindValue($identifier, $this->vgaico_name, PDO::PARAM_STR);
                        break;
                    case 'vgaico_slug':
                        $stmt->bindValue($identifier, $this->vgaico_slug, PDO::PARAM_STR);
                        break;
                    case 'vgaico_url':
                        $stmt->bindValue($identifier, $this->vgaico_url, PDO::PARAM_STR);
                        break;
                    case 'vgaico_website':
                        $stmt->bindValue($identifier, $this->vgaico_website, PDO::PARAM_STR);
                        break;
                    case 'vgaico_twitter':
                        $stmt->bindValue($identifier, $this->vgaico_twitter, PDO::PARAM_STR);
                        break;
                    case 'vgaico_country':
                        $stmt->bindValue($identifier, $this->vgaico_country, PDO::PARAM_INT);
                        break;
                    case 'vgaico_logo_cloudinary_id':
                        $stmt->bindValue($identifier, $this->vgaico_logo_cloudinary_id, PDO::PARAM_STR);
                        break;
                    case 'vgaico_logo_width':
                        $stmt->bindValue($identifier, $this->vgaico_logo_width, PDO::PARAM_INT);
                        break;
                    case 'vgaico_logo_height':
                        $stmt->bindValue($identifier, $this->vgaico_logo_height, PDO::PARAM_INT);
                        break;
                    case 'vgaico_description':
                        $stmt->bindValue($identifier, $this->vgaico_description, PDO::PARAM_STR);
                        break;
                    case 'vgaico_developed':
                        $stmt->bindValue($identifier, $this->vgaico_developed, PDO::PARAM_STR);
                        break;
                    case 'vgaico_published':
                        $stmt->bindValue($identifier, $this->vgaico_published, PDO::PARAM_STR);
                        break;
                    case 'vgaico_start_date':
                        $stmt->bindValue($identifier, $this->vgaico_start_date ? $this->vgaico_start_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgaico_start_date_category':
                        $stmt->bindValue($identifier, $this->vgaico_start_date_category, PDO::PARAM_INT);
                        break;
                    case 'vgaico_change_date':
                        $stmt->bindValue($identifier, $this->vgaico_change_date ? $this->vgaico_change_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgaico_change_date_category':
                        $stmt->bindValue($identifier, $this->vgaico_change_date_category, PDO::PARAM_INT);
                        break;
                    case 'vgaico_created_at':
                        $stmt->bindValue($identifier, $this->vgaico_created_at ? $this->vgaico_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgaico_updated_at':
                        $stmt->bindValue($identifier, $this->vgaico_updated_at ? $this->vgaico_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiIgdbCompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getParentId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getSlug();
                break;
            case 4:
                return $this->getUrl();
                break;
            case 5:
                return $this->getWebSite();
                break;
            case 6:
                return $this->getTwitter();
                break;
            case 7:
                return $this->getCountry();
                break;
            case 8:
                return $this->getLogoCloudinaryId();
                break;
            case 9:
                return $this->getLogoWidth();
                break;
            case 10:
                return $this->getLogoHeight();
                break;
            case 11:
                return $this->getDescription();
                break;
            case 12:
                return $this->getDeveloped();
                break;
            case 13:
                return $this->getPublished();
                break;
            case 14:
                return $this->getStartDate();
                break;
            case 15:
                return $this->getStartDateCategory();
                break;
            case 16:
                return $this->getChangeDate();
                break;
            case 17:
                return $this->getChangeDateCategory();
                break;
            case 18:
                return $this->getCreatedAt();
                break;
            case 19:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['ApiIgdbCompany'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiIgdbCompany'][$this->hashCode()] = true;
        $keys = ApiIgdbCompanyTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getParentId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getSlug(),
            $keys[4] => $this->getUrl(),
            $keys[5] => $this->getWebSite(),
            $keys[6] => $this->getTwitter(),
            $keys[7] => $this->getCountry(),
            $keys[8] => $this->getLogoCloudinaryId(),
            $keys[9] => $this->getLogoWidth(),
            $keys[10] => $this->getLogoHeight(),
            $keys[11] => $this->getDescription(),
            $keys[12] => $this->getDeveloped(),
            $keys[13] => $this->getPublished(),
            $keys[14] => $this->getStartDate(),
            $keys[15] => $this->getStartDateCategory(),
            $keys[16] => $this->getChangeDate(),
            $keys[17] => $this->getChangeDateCategory(),
            $keys[18] => $this->getCreatedAt(),
            $keys[19] => $this->getUpdatedAt(),
        );
        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTime) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiIgdbCompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setParentId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setSlug($value);
                break;
            case 4:
                $this->setUrl($value);
                break;
            case 5:
                $this->setWebSite($value);
                break;
            case 6:
                $this->setTwitter($value);
                break;
            case 7:
                $this->setCountry($value);
                break;
            case 8:
                $this->setLogoCloudinaryId($value);
                break;
            case 9:
                $this->setLogoWidth($value);
                break;
            case 10:
                $this->setLogoHeight($value);
                break;
            case 11:
                $this->setDescription($value);
                break;
            case 12:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setDeveloped($value);
                break;
            case 13:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setPublished($value);
                break;
            case 14:
                $this->setStartDate($value);
                break;
            case 15:
                $this->setStartDateCategory($value);
                break;
            case 16:
                $this->setChangeDate($value);
                break;
            case 17:
                $this->setChangeDateCategory($value);
                break;
            case 18:
                $this->setCreatedAt($value);
                break;
            case 19:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiIgdbCompanyTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setParentId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSlug($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUrl($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setWebSite($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTwitter($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCountry($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLogoCloudinaryId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLogoWidth($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLogoHeight($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDescription($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setDeveloped($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPublished($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setStartDate($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setStartDateCategory($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setChangeDate($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setChangeDateCategory($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCreatedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUpdatedAt($arr[$keys[19]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiIgdbCompany The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiIgdbCompanyTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_ID)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $this->vgaico_id);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_PARENT_ID, $this->vgaico_parent_id);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_NAME)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_NAME, $this->vgaico_name);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_SLUG)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_SLUG, $this->vgaico_slug);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_URL)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_URL, $this->vgaico_url);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_WEBSITE, $this->vgaico_website);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_TWITTER, $this->vgaico_twitter);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_COUNTRY, $this->vgaico_country);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_CLOUDINARY_ID, $this->vgaico_logo_cloudinary_id);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_WIDTH, $this->vgaico_logo_width);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_LOGO_HEIGHT, $this->vgaico_logo_height);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_DESCRIPTION, $this->vgaico_description);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_DEVELOPED, $this->vgaico_developed);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_PUBLISHED, $this->vgaico_published);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE, $this->vgaico_start_date);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_START_DATE_CATEGORY, $this->vgaico_start_date_category);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE, $this->vgaico_change_date);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_CHANGE_DATE_CATEGORY, $this->vgaico_change_date_category);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_CREATED_AT, $this->vgaico_created_at);
        }
        if ($this->isColumnModified(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT)) {
            $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_UPDATED_AT, $this->vgaico_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiIgdbCompanyQuery::create();
        $criteria->add(ApiIgdbCompanyTableMap::COL_VGAICO_ID, $this->vgaico_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgaico_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiIgdbCompany (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setName($this->getName());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setWebSite($this->getWebSite());
        $copyObj->setTwitter($this->getTwitter());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setLogoCloudinaryId($this->getLogoCloudinaryId());
        $copyObj->setLogoWidth($this->getLogoWidth());
        $copyObj->setLogoHeight($this->getLogoHeight());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setDeveloped($this->getDeveloped());
        $copyObj->setPublished($this->getPublished());
        $copyObj->setStartDate($this->getStartDate());
        $copyObj->setStartDateCategory($this->getStartDateCategory());
        $copyObj->setChangeDate($this->getChangeDate());
        $copyObj->setChangeDateCategory($this->getChangeDateCategory());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiIgdbCompany Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->vgaico_id = null;
        $this->vgaico_parent_id = null;
        $this->vgaico_name = null;
        $this->vgaico_slug = null;
        $this->vgaico_url = null;
        $this->vgaico_website = null;
        $this->vgaico_twitter = null;
        $this->vgaico_country = null;
        $this->vgaico_logo_cloudinary_id = null;
        $this->vgaico_logo_width = null;
        $this->vgaico_logo_height = null;
        $this->vgaico_description = null;
        $this->vgaico_developed = null;
        $this->vgaico_developed_unserialized = null;
        $this->vgaico_published = null;
        $this->vgaico_published_unserialized = null;
        $this->vgaico_start_date = null;
        $this->vgaico_start_date_category = null;
        $this->vgaico_change_date = null;
        $this->vgaico_change_date_category = null;
        $this->vgaico_created_at = null;
        $this->vgaico_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiIgdbCompanyTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
