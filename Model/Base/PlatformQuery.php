<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\Platform as ChildPlatform;
use IiMedias\VideoGamesBundle\Model\PlatformQuery as ChildPlatformQuery;
use IiMedias\VideoGamesBundle\Model\Map\PlatformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_platform_vgaplf' table.
 *
 *
 *
 * @method     ChildPlatformQuery orderById($order = Criteria::ASC) Order by the vgaplf_id column
 * @method     ChildPlatformQuery orderByName($order = Criteria::ASC) Order by the vgaplf_name column
 *
 * @method     ChildPlatformQuery groupById() Group by the vgaplf_id column
 * @method     ChildPlatformQuery groupByName() Group by the vgaplf_name column
 *
 * @method     ChildPlatformQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlatformQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlatformQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlatformQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlatformQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlatformQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlatformQuery leftJoinGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameRelease relation
 * @method     ChildPlatformQuery rightJoinGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameRelease relation
 * @method     ChildPlatformQuery innerJoinGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the GameRelease relation
 *
 * @method     ChildPlatformQuery joinWithGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameRelease relation
 *
 * @method     ChildPlatformQuery leftJoinWithGameRelease() Adds a LEFT JOIN clause and with to the query using the GameRelease relation
 * @method     ChildPlatformQuery rightJoinWithGameRelease() Adds a RIGHT JOIN clause and with to the query using the GameRelease relation
 * @method     ChildPlatformQuery innerJoinWithGameRelease() Adds a INNER JOIN clause and with to the query using the GameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPlatform findOne(ConnectionInterface $con = null) Return the first ChildPlatform matching the query
 * @method     ChildPlatform findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlatform matching the query, or a new ChildPlatform object populated from the query conditions when no match is found
 *
 * @method     ChildPlatform findOneById(int $vgaplf_id) Return the first ChildPlatform filtered by the vgaplf_id column
 * @method     ChildPlatform findOneByName(string $vgaplf_name) Return the first ChildPlatform filtered by the vgaplf_name column *

 * @method     ChildPlatform requirePk($key, ConnectionInterface $con = null) Return the ChildPlatform by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlatform requireOne(ConnectionInterface $con = null) Return the first ChildPlatform matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlatform requireOneById(int $vgaplf_id) Return the first ChildPlatform filtered by the vgaplf_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlatform requireOneByName(string $vgaplf_name) Return the first ChildPlatform filtered by the vgaplf_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlatform[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlatform objects based on current ModelCriteria
 * @method     ChildPlatform[]|ObjectCollection findById(int $vgaplf_id) Return ChildPlatform objects filtered by the vgaplf_id column
 * @method     ChildPlatform[]|ObjectCollection findByName(string $vgaplf_name) Return ChildPlatform objects filtered by the vgaplf_name column
 * @method     ChildPlatform[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlatformQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\PlatformQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\Platform', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlatformQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlatformQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlatformQuery) {
            return $criteria;
        }
        $query = new ChildPlatformQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlatform|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlatformTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlatformTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlatform A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaplf_id, vgaplf_name FROM videogames_platform_vgaplf WHERE vgaplf_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlatform $obj */
            $obj = new ChildPlatform();
            $obj->hydrate($row);
            PlatformTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlatform|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaplf_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaplf_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaplf_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaplf_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaplf_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaplf_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaplf_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\GameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\GameRelease|ObjectCollection $gameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPlatformQuery The current query, for fluid interface
     */
    public function filterByGameRelease($gameRelease, $comparison = null)
    {
        if ($gameRelease instanceof \IiMedias\VideoGamesBundle\Model\GameRelease) {
            return $this
                ->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $gameRelease->getPlatformId(), $comparison);
        } elseif ($gameRelease instanceof ObjectCollection) {
            return $this
                ->useGameReleaseQuery()
                ->filterByPrimaryKeys($gameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\GameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function joinGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameRelease');
        }

        return $this;
    }

    /**
     * Use the GameRelease relation GameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameRelease', '\IiMedias\VideoGamesBundle\Model\GameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlatform $platform Object to remove from the list of results
     *
     * @return $this|ChildPlatformQuery The current query, for fluid interface
     */
    public function prune($platform = null)
    {
        if ($platform) {
            $this->addUsingAlias(PlatformTableMap::COL_VGAPLF_ID, $platform->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_platform_vgaplf table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlatformTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlatformTableMap::clearInstancePool();
            PlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlatformTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlatformTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlatformTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlatformQuery
