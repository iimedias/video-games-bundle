<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\Game as ChildGame;
use IiMedias\VideoGamesBundle\Model\GameQuery as ChildGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\GameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_game_vgagam' table.
 *
 *
 *
 * @method     ChildGameQuery orderById($order = Criteria::ASC) Order by the vgagam_id column
 * @method     ChildGameQuery orderByName($order = Criteria::ASC) Order by the vgagam_name column
 *
 * @method     ChildGameQuery groupById() Group by the vgagam_id column
 * @method     ChildGameQuery groupByName() Group by the vgagam_name column
 *
 * @method     ChildGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildGameQuery leftJoinGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameRelease relation
 * @method     ChildGameQuery rightJoinGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameRelease relation
 * @method     ChildGameQuery innerJoinGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the GameRelease relation
 *
 * @method     ChildGameQuery joinWithGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameRelease relation
 *
 * @method     ChildGameQuery leftJoinWithGameRelease() Adds a LEFT JOIN clause and with to the query using the GameRelease relation
 * @method     ChildGameQuery rightJoinWithGameRelease() Adds a RIGHT JOIN clause and with to the query using the GameRelease relation
 * @method     ChildGameQuery innerJoinWithGameRelease() Adds a INNER JOIN clause and with to the query using the GameRelease relation
 *
 * @method     ChildGameQuery leftJoinSiteJeuxVideoComGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteJeuxVideoComGame relation
 * @method     ChildGameQuery rightJoinSiteJeuxVideoComGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteJeuxVideoComGame relation
 * @method     ChildGameQuery innerJoinSiteJeuxVideoComGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildGameQuery joinWithSiteJeuxVideoComGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildGameQuery leftJoinWithSiteJeuxVideoComGame() Adds a LEFT JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 * @method     ChildGameQuery rightJoinWithSiteJeuxVideoComGame() Adds a RIGHT JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 * @method     ChildGameQuery innerJoinWithSiteJeuxVideoComGame() Adds a INNER JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildGameQuery leftJoinSiteGameBlogGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameBlogGame relation
 * @method     ChildGameQuery rightJoinSiteGameBlogGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameBlogGame relation
 * @method     ChildGameQuery innerJoinSiteGameBlogGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameBlogGame relation
 *
 * @method     ChildGameQuery joinWithSiteGameBlogGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameBlogGame relation
 *
 * @method     ChildGameQuery leftJoinWithSiteGameBlogGame() Adds a LEFT JOIN clause and with to the query using the SiteGameBlogGame relation
 * @method     ChildGameQuery rightJoinWithSiteGameBlogGame() Adds a RIGHT JOIN clause and with to the query using the SiteGameBlogGame relation
 * @method     ChildGameQuery innerJoinWithSiteGameBlogGame() Adds a INNER JOIN clause and with to the query using the SiteGameBlogGame relation
 *
 * @method     ChildGameQuery leftJoinSiteGameKultGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameKultGame relation
 * @method     ChildGameQuery rightJoinSiteGameKultGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameKultGame relation
 * @method     ChildGameQuery innerJoinSiteGameKultGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameKultGame relation
 *
 * @method     ChildGameQuery joinWithSiteGameKultGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameKultGame relation
 *
 * @method     ChildGameQuery leftJoinWithSiteGameKultGame() Adds a LEFT JOIN clause and with to the query using the SiteGameKultGame relation
 * @method     ChildGameQuery rightJoinWithSiteGameKultGame() Adds a RIGHT JOIN clause and with to the query using the SiteGameKultGame relation
 * @method     ChildGameQuery innerJoinWithSiteGameKultGame() Adds a INNER JOIN clause and with to the query using the SiteGameKultGame relation
 *
 * @method     ChildGameQuery leftJoinApiGiantBombGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildGameQuery rightJoinApiGiantBombGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildGameQuery innerJoinApiGiantBombGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGame relation
 *
 * @method     ChildGameQuery joinWithApiGiantBombGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildGameQuery leftJoinWithApiGiantBombGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildGameQuery rightJoinWithApiGiantBombGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildGameQuery innerJoinWithApiGiantBombGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildGameQuery leftJoinApiTwitchGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildGameQuery rightJoinApiTwitchGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiTwitchGame relation
 * @method     ChildGameQuery innerJoinApiTwitchGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiTwitchGame relation
 *
 * @method     ChildGameQuery joinWithApiTwitchGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiTwitchGame relation
 *
 * @method     ChildGameQuery leftJoinWithApiTwitchGame() Adds a LEFT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildGameQuery rightJoinWithApiTwitchGame() Adds a RIGHT JOIN clause and with to the query using the ApiTwitchGame relation
 * @method     ChildGameQuery innerJoinWithApiTwitchGame() Adds a INNER JOIN clause and with to the query using the ApiTwitchGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameReleaseQuery|\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery|\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildGame findOne(ConnectionInterface $con = null) Return the first ChildGame matching the query
 * @method     ChildGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildGame matching the query, or a new ChildGame object populated from the query conditions when no match is found
 *
 * @method     ChildGame findOneById(int $vgagam_id) Return the first ChildGame filtered by the vgagam_id column
 * @method     ChildGame findOneByName(string $vgagam_name) Return the first ChildGame filtered by the vgagam_name column *

 * @method     ChildGame requirePk($key, ConnectionInterface $con = null) Return the ChildGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGame requireOne(ConnectionInterface $con = null) Return the first ChildGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGame requireOneById(int $vgagam_id) Return the first ChildGame filtered by the vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildGame requireOneByName(string $vgagam_name) Return the first ChildGame filtered by the vgagam_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildGame objects based on current ModelCriteria
 * @method     ChildGame[]|ObjectCollection findById(int $vgagam_id) Return ChildGame objects filtered by the vgagam_id column
 * @method     ChildGame[]|ObjectCollection findByName(string $vgagam_name) Return ChildGame objects filtered by the vgagam_name column
 * @method     ChildGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class GameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\GameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\Game', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildGameQuery) {
            return $criteria;
        }
        $query = new ChildGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = GameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagam_id, vgagam_name FROM videogames_game_vgagam WHERE vgagam_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildGame $obj */
            $obj = new ChildGame();
            $obj->hydrate($row);
            GameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagam_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagam_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagam_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagam_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagam_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagam_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(GameTableMap::COL_VGAGAM_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\GameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\GameRelease|ObjectCollection $gameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterByGameRelease($gameRelease, $comparison = null)
    {
        if ($gameRelease instanceof \IiMedias\VideoGamesBundle\Model\GameRelease) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $gameRelease->getGameId(), $comparison);
        } elseif ($gameRelease instanceof ObjectCollection) {
            return $this
                ->useGameReleaseQuery()
                ->filterByPrimaryKeys($gameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\GameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameRelease');
        }

        return $this;
    }

    /**
     * Use the GameRelease relation GameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameRelease', '\IiMedias\VideoGamesBundle\Model\GameReleaseQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame|ObjectCollection $siteJeuxVideoComGame the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComGame($siteJeuxVideoComGame, $comparison = null)
    {
        if ($siteJeuxVideoComGame instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $siteJeuxVideoComGame->getGameId(), $comparison);
        } elseif ($siteJeuxVideoComGame instanceof ObjectCollection) {
            return $this
                ->useSiteJeuxVideoComGameQuery()
                ->filterByPrimaryKeys($siteJeuxVideoComGame->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteJeuxVideoComGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteJeuxVideoComGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinSiteJeuxVideoComGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteJeuxVideoComGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteJeuxVideoComGame');
        }

        return $this;
    }

    /**
     * Use the SiteJeuxVideoComGame relation SiteJeuxVideoComGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteJeuxVideoComGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteJeuxVideoComGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteJeuxVideoComGame', '\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame|ObjectCollection $siteGameBlogGame the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogGame($siteGameBlogGame, $comparison = null)
    {
        if ($siteGameBlogGame instanceof \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $siteGameBlogGame->getGameId(), $comparison);
        } elseif ($siteGameBlogGame instanceof ObjectCollection) {
            return $this
                ->useSiteGameBlogGameQuery()
                ->filterByPrimaryKeys($siteGameBlogGame->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteGameBlogGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameBlogGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinSiteGameBlogGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameBlogGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameBlogGame');
        }

        return $this;
    }

    /**
     * Use the SiteGameBlogGame relation SiteGameBlogGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameBlogGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteGameBlogGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameBlogGame', '\IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameKultGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameKultGame|ObjectCollection $siteGameKultGame the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultGame($siteGameKultGame, $comparison = null)
    {
        if ($siteGameKultGame instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGame) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $siteGameKultGame->getGameId(), $comparison);
        } elseif ($siteGameKultGame instanceof ObjectCollection) {
            return $this
                ->useSiteGameKultGameQuery()
                ->filterByPrimaryKeys($siteGameKultGame->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteGameKultGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameKultGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameKultGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinSiteGameKultGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameKultGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameKultGame');
        }

        return $this;
    }

    /**
     * Use the SiteGameKultGame relation SiteGameKultGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameKultGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteGameKultGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameKultGame', '\IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $apiGiantBombGame->getGameId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameQuery()
                ->filterByPrimaryKeys($apiGiantBombGame->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiTwitchGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiTwitchGame|ObjectCollection $apiTwitchGame the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildGameQuery The current query, for fluid interface
     */
    public function filterByApiTwitchGame($apiTwitchGame, $comparison = null)
    {
        if ($apiTwitchGame instanceof \IiMedias\VideoGamesBundle\Model\ApiTwitchGame) {
            return $this
                ->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $apiTwitchGame->getGameId(), $comparison);
        } elseif ($apiTwitchGame instanceof ObjectCollection) {
            return $this
                ->useApiTwitchGameQuery()
                ->filterByPrimaryKeys($apiTwitchGame->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiTwitchGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiTwitchGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiTwitchGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function joinApiTwitchGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiTwitchGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiTwitchGame');
        }

        return $this;
    }

    /**
     * Use the ApiTwitchGame relation ApiTwitchGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery A secondary query class using the current class as primary query
     */
    public function useApiTwitchGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiTwitchGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiTwitchGame', '\IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildGame $game Object to remove from the list of results
     *
     * @return $this|ChildGameQuery The current query, for fluid interface
     */
    public function prune($game = null)
    {
        if ($game) {
            $this->addUsingAlias(GameTableMap::COL_VGAGAM_ID, $game->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_game_vgagam table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            GameTableMap::clearInstancePool();
            GameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(GameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            GameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            GameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // GameQuery
