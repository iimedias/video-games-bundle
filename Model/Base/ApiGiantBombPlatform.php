<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany as ChildApiGiantBombCompany;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery as ChildApiGiantBombCompanyQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform as ChildApiGiantBombGamePlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery as ChildApiGiantBombGamePlatformQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform as ChildApiGiantBombPlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery as ChildApiGiantBombPlatformQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGamePlatformTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPlatformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_platform_vgagpl' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombPlatform implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombPlatformTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagpl_id field.
     *
     * @var        int
     */
    protected $vgagpl_id;

    /**
     * The value for the vgagpl_name field.
     *
     * @var        string
     */
    protected $vgagpl_name;

    /**
     * The value for the vgagpl_abbr field.
     *
     * @var        string
     */
    protected $vgagpl_abbr;

    /**
     * The value for the vgagpl_aliases field.
     *
     * @var        array
     */
    protected $vgagpl_aliases;

    /**
     * The unserialized $vgagpl_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagpl_aliases_unserialized;

    /**
     * The value for the vgagpl_summary field.
     *
     * @var        string
     */
    protected $vgagpl_summary;

    /**
     * The value for the vgagpl_description field.
     *
     * @var        string
     */
    protected $vgagpl_description;

    /**
     * The value for the vgagpl_install_base field.
     *
     * @var        int
     */
    protected $vgagpl_install_base;

    /**
     * The value for the vgagpl_online_support field.
     *
     * @var        boolean
     */
    protected $vgagpl_online_support;

    /**
     * The value for the vgagpl_original_price field.
     *
     * @var        double
     */
    protected $vgagpl_original_price;

    /**
     * The value for the vgagpl_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagpl_released_at;

    /**
     * The value for the vgagpl_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagpl_api_detail_url;

    /**
     * The value for the vgagpl_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagpl_site_detail_url;

    /**
     * The value for the vgagpl_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_icon_url;

    /**
     * The value for the vgagpl_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_medium_url;

    /**
     * The value for the vgagpl_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_screen_url;

    /**
     * The value for the vgagpl_image_small_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_small_url;

    /**
     * The value for the vgagpl_image_super_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_super_url;

    /**
     * The value for the vgagpl_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_thumb_url;

    /**
     * The value for the vgagpl_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagpl_image_tiny_url;

    /**
     * The value for the vgagpl_vgagco_id field.
     *
     * @var        int
     */
    protected $vgagpl_vgagco_id;

    /**
     * The value for the vgagpl_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagpl_created_at;

    /**
     * The value for the vgagpl_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagpl_updated_at;

    /**
     * @var        ChildApiGiantBombCompany
     */
    protected $aApiGiantBombCompany;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGamePlatform[] Collection to store aggregation of ChildApiGiantBombGamePlatform objects.
     */
    protected $collApiGiantBombGamePlatforms;
    protected $collApiGiantBombGamePlatformsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameRelease[] Collection to store aggregation of ChildApiGiantBombGameRelease objects.
     */
    protected $collApiGiantBombGameReleases;
    protected $collApiGiantBombGameReleasesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGamePlatform[]
     */
    protected $apiGiantBombGamePlatformsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameRelease[]
     */
    protected $apiGiantBombGameReleasesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombPlatform object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombPlatform</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombPlatform</code>, delegates to
     * <code>equals(ApiGiantBombPlatform)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombPlatform The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagpl_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagpl_id;
    }

    /**
     * Get the [vgagpl_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagpl_name;
    }

    /**
     * Get the [vgagpl_abbr] column value.
     *
     * @return string
     */
    public function getAbbr()
    {
        return $this->vgagpl_abbr;
    }

    /**
     * Get the [vgagpl_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagpl_aliases_unserialized) {
            $this->vgagpl_aliases_unserialized = array();
        }
        if (!$this->vgagpl_aliases_unserialized && null !== $this->vgagpl_aliases) {
            $vgagpl_aliases_unserialized = substr($this->vgagpl_aliases, 2, -2);
            $this->vgagpl_aliases_unserialized = $vgagpl_aliases_unserialized ? explode(' | ', $vgagpl_aliases_unserialized) : array();
        }

        return $this->vgagpl_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagpl_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagpl_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagpl_summary;
    }

    /**
     * Get the [vgagpl_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagpl_description;
    }

    /**
     * Get the [vgagpl_install_base] column value.
     *
     * @return int
     */
    public function getInstallBase()
    {
        return $this->vgagpl_install_base;
    }

    /**
     * Get the [vgagpl_online_support] column value.
     *
     * @return boolean
     */
    public function getOnlineSupport()
    {
        return $this->vgagpl_online_support;
    }

    /**
     * Get the [vgagpl_online_support] column value.
     *
     * @return boolean
     */
    public function isOnlineSupport()
    {
        return $this->getOnlineSupport();
    }

    /**
     * Get the [vgagpl_original_price] column value.
     *
     * @return double
     */
    public function getOriginalPrice()
    {
        return $this->vgagpl_original_price;
    }

    /**
     * Get the [optionally formatted] temporal [vgagpl_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpl_released_at;
        } else {
            return $this->vgagpl_released_at instanceof \DateTimeInterface ? $this->vgagpl_released_at->format($format) : null;
        }
    }

    /**
     * Get the [vgagpl_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagpl_api_detail_url;
    }

    /**
     * Get the [vgagpl_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagpl_site_detail_url;
    }

    /**
     * Get the [vgagpl_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagpl_image_icon_url;
    }

    /**
     * Get the [vgagpl_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagpl_image_medium_url;
    }

    /**
     * Get the [vgagpl_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagpl_image_screen_url;
    }

    /**
     * Get the [vgagpl_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagpl_image_small_url;
    }

    /**
     * Get the [vgagpl_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagpl_image_super_url;
    }

    /**
     * Get the [vgagpl_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagpl_image_thumb_url;
    }

    /**
     * Get the [vgagpl_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagpl_image_tiny_url;
    }

    /**
     * Get the [vgagpl_vgagco_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombCompanyId()
    {
        return $this->vgagpl_vgagco_id;
    }

    /**
     * Get the [optionally formatted] temporal [vgagpl_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpl_created_at;
        } else {
            return $this->vgagpl_created_at instanceof \DateTimeInterface ? $this->vgagpl_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagpl_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpl_updated_at;
        } else {
            return $this->vgagpl_updated_at instanceof \DateTimeInterface ? $this->vgagpl_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagpl_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpl_id !== $v) {
            $this->vgagpl_id = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagpl_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_name !== $v) {
            $this->vgagpl_name = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagpl_abbr] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setAbbr($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_abbr !== $v) {
            $this->vgagpl_abbr = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR] = true;
        }

        return $this;
    } // setAbbr()

    /**
     * Set the value of [vgagpl_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagpl_aliases_unserialized !== $v) {
            $this->vgagpl_aliases_unserialized = $v;
            $this->vgagpl_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagpl_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagpl_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagpl_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_summary !== $v) {
            $this->vgagpl_summary = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagpl_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_description !== $v) {
            $this->vgagpl_description = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagpl_install_base] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setInstallBase($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpl_install_base !== $v) {
            $this->vgagpl_install_base = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE] = true;
        }

        return $this;
    } // setInstallBase()

    /**
     * Sets the value of the [vgagpl_online_support] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setOnlineSupport($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->vgagpl_online_support !== $v) {
            $this->vgagpl_online_support = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT] = true;
        }

        return $this;
    } // setOnlineSupport()

    /**
     * Set the value of [vgagpl_original_price] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setOriginalPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->vgagpl_original_price !== $v) {
            $this->vgagpl_original_price = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE] = true;
        }

        return $this;
    } // setOriginalPrice()

    /**
     * Sets the value of [vgagpl_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpl_released_at !== null || $dt !== null) {
            if ($this->vgagpl_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpl_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagpl_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setReleasedAt()

    /**
     * Set the value of [vgagpl_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_api_detail_url !== $v) {
            $this->vgagpl_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagpl_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_site_detail_url !== $v) {
            $this->vgagpl_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagpl_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_icon_url !== $v) {
            $this->vgagpl_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagpl_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_medium_url !== $v) {
            $this->vgagpl_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagpl_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_screen_url !== $v) {
            $this->vgagpl_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagpl_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_small_url !== $v) {
            $this->vgagpl_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagpl_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_super_url !== $v) {
            $this->vgagpl_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagpl_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_thumb_url !== $v) {
            $this->vgagpl_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagpl_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpl_image_tiny_url !== $v) {
            $this->vgagpl_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagpl_vgagco_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setApiGiantBombCompanyId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpl_vgagco_id !== $v) {
            $this->vgagpl_vgagco_id = $v;
            $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID] = true;
        }

        if ($this->aApiGiantBombCompany !== null && $this->aApiGiantBombCompany->getId() !== $v) {
            $this->aApiGiantBombCompany = null;
        }

        return $this;
    } // setApiGiantBombCompanyId()

    /**
     * Sets the value of [vgagpl_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpl_created_at !== null || $dt !== null) {
            if ($this->vgagpl_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpl_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagpl_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagpl_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpl_updated_at !== null || $dt !== null) {
            if ($this->vgagpl_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpl_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagpl_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Abbr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_abbr = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_aliases = $col;
            $this->vgagpl_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('InstallBase', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_install_base = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('OnlineSupport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_online_support = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('OriginalPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_original_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpl_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('ApiGiantBombCompanyId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpl_vgagco_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpl_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApiGiantBombPlatformTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpl_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = ApiGiantBombPlatformTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombCompany !== null && $this->vgagpl_vgagco_id !== $this->aApiGiantBombCompany->getId()) {
            $this->aApiGiantBombCompany = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombPlatformQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombCompany = null;
            $this->collApiGiantBombGamePlatforms = null;

            $this->collApiGiantBombGameReleases = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombPlatform::setDeleted()
     * @see ApiGiantBombPlatform::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombPlatformQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombPlatformTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombCompany !== null) {
                if ($this->aApiGiantBombCompany->isModified() || $this->aApiGiantBombCompany->isNew()) {
                    $affectedRows += $this->aApiGiantBombCompany->save($con);
                }
                $this->setApiGiantBombCompany($this->aApiGiantBombCompany);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombGamePlatformsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGamePlatformsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGamePlatformsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGamePlatformsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGamePlatforms !== null) {
                foreach ($this->collApiGiantBombGamePlatforms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleasesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleasesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleasesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleases !== null) {
                foreach ($this->collApiGiantBombGameReleases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_id';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_name';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_abbr';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_summary';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_description';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_install_base';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_online_support';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_original_price';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_vgagco_id';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpl_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_platform_vgagpl (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagpl_id':
                        $stmt->bindValue($identifier, $this->vgagpl_id, PDO::PARAM_INT);
                        break;
                    case 'vgagpl_name':
                        $stmt->bindValue($identifier, $this->vgagpl_name, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_abbr':
                        $stmt->bindValue($identifier, $this->vgagpl_abbr, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_aliases':
                        $stmt->bindValue($identifier, $this->vgagpl_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_summary':
                        $stmt->bindValue($identifier, $this->vgagpl_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_description':
                        $stmt->bindValue($identifier, $this->vgagpl_description, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_install_base':
                        $stmt->bindValue($identifier, $this->vgagpl_install_base, PDO::PARAM_INT);
                        break;
                    case 'vgagpl_online_support':
                        $stmt->bindValue($identifier, (int) $this->vgagpl_online_support, PDO::PARAM_INT);
                        break;
                    case 'vgagpl_original_price':
                        $stmt->bindValue($identifier, $this->vgagpl_original_price, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_released_at':
                        $stmt->bindValue($identifier, $this->vgagpl_released_at ? $this->vgagpl_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagpl_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagpl_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagpl_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_vgagco_id':
                        $stmt->bindValue($identifier, $this->vgagpl_vgagco_id, PDO::PARAM_INT);
                        break;
                    case 'vgagpl_created_at':
                        $stmt->bindValue($identifier, $this->vgagpl_created_at ? $this->vgagpl_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagpl_updated_at':
                        $stmt->bindValue($identifier, $this->vgagpl_updated_at ? $this->vgagpl_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombPlatformTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getAbbr();
                break;
            case 3:
                return $this->getAliases();
                break;
            case 4:
                return $this->getSummary();
                break;
            case 5:
                return $this->getDescription();
                break;
            case 6:
                return $this->getInstallBase();
                break;
            case 7:
                return $this->getOnlineSupport();
                break;
            case 8:
                return $this->getOriginalPrice();
                break;
            case 9:
                return $this->getReleasedAt();
                break;
            case 10:
                return $this->getApiDetailUrl();
                break;
            case 11:
                return $this->getSiteDetailUrl();
                break;
            case 12:
                return $this->getImageIconUrl();
                break;
            case 13:
                return $this->getImageMediumUrl();
                break;
            case 14:
                return $this->getImageScreenUrl();
                break;
            case 15:
                return $this->getImageSmallUrl();
                break;
            case 16:
                return $this->getImageSuperUrl();
                break;
            case 17:
                return $this->getImageThumbUrl();
                break;
            case 18:
                return $this->getImageTinyUrl();
                break;
            case 19:
                return $this->getApiGiantBombCompanyId();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombPlatform'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombPlatform'][$this->hashCode()] = true;
        $keys = ApiGiantBombPlatformTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getAbbr(),
            $keys[3] => $this->getAliases(),
            $keys[4] => $this->getSummary(),
            $keys[5] => $this->getDescription(),
            $keys[6] => $this->getInstallBase(),
            $keys[7] => $this->getOnlineSupport(),
            $keys[8] => $this->getOriginalPrice(),
            $keys[9] => $this->getReleasedAt(),
            $keys[10] => $this->getApiDetailUrl(),
            $keys[11] => $this->getSiteDetailUrl(),
            $keys[12] => $this->getImageIconUrl(),
            $keys[13] => $this->getImageMediumUrl(),
            $keys[14] => $this->getImageScreenUrl(),
            $keys[15] => $this->getImageSmallUrl(),
            $keys[16] => $this->getImageSuperUrl(),
            $keys[17] => $this->getImageThumbUrl(),
            $keys[18] => $this->getImageTinyUrl(),
            $keys[19] => $this->getApiGiantBombCompanyId(),
            $keys[20] => $this->getCreatedAt(),
            $keys[21] => $this->getUpdatedAt(),
        );
        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTime) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombCompany) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombCompany';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_company_vgagco';
                        break;
                    default:
                        $key = 'ApiGiantBombCompany';
                }

                $result[$key] = $this->aApiGiantBombCompany->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collApiGiantBombGamePlatforms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGamePlatforms';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_platform_vgaggps';
                        break;
                    default:
                        $key = 'ApiGiantBombGamePlatforms';
                }

                $result[$key] = $this->collApiGiantBombGamePlatforms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleases) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleases';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_release_vgagrls';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleases';
                }

                $result[$key] = $this->collApiGiantBombGameReleases->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombPlatformTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setAbbr($value);
                break;
            case 3:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 4:
                $this->setSummary($value);
                break;
            case 5:
                $this->setDescription($value);
                break;
            case 6:
                $this->setInstallBase($value);
                break;
            case 7:
                $this->setOnlineSupport($value);
                break;
            case 8:
                $this->setOriginalPrice($value);
                break;
            case 9:
                $this->setReleasedAt($value);
                break;
            case 10:
                $this->setApiDetailUrl($value);
                break;
            case 11:
                $this->setSiteDetailUrl($value);
                break;
            case 12:
                $this->setImageIconUrl($value);
                break;
            case 13:
                $this->setImageMediumUrl($value);
                break;
            case 14:
                $this->setImageScreenUrl($value);
                break;
            case 15:
                $this->setImageSmallUrl($value);
                break;
            case 16:
                $this->setImageSuperUrl($value);
                break;
            case 17:
                $this->setImageThumbUrl($value);
                break;
            case 18:
                $this->setImageTinyUrl($value);
                break;
            case 19:
                $this->setApiGiantBombCompanyId($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombPlatformTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAbbr($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAliases($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSummary($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDescription($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setInstallBase($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setOnlineSupport($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setOriginalPrice($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setReleasedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setApiDetailUrl($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageIconUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setImageMediumUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setImageScreenUrl($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setImageSmallUrl($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setImageSuperUrl($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setImageThumbUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setImageTinyUrl($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setApiGiantBombCompanyId($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setUpdatedAt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombPlatformTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $this->vgagpl_id);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME, $this->vgagpl_name);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR, $this->vgagpl_abbr);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES, $this->vgagpl_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY, $this->vgagpl_summary);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION, $this->vgagpl_description);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE, $this->vgagpl_install_base);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT, $this->vgagpl_online_support);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE, $this->vgagpl_original_price);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT, $this->vgagpl_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL, $this->vgagpl_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL, $this->vgagpl_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL, $this->vgagpl_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL, $this->vgagpl_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL, $this->vgagpl_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL, $this->vgagpl_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL, $this->vgagpl_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL, $this->vgagpl_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL, $this->vgagpl_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $this->vgagpl_vgagco_id);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT, $this->vgagpl_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT)) {
            $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT, $this->vgagpl_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombPlatformQuery::create();
        $criteria->add(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $this->vgagpl_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagpl_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setAbbr($this->getAbbr());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setInstallBase($this->getInstallBase());
        $copyObj->setOnlineSupport($this->getOnlineSupport());
        $copyObj->setOriginalPrice($this->getOriginalPrice());
        $copyObj->setReleasedAt($this->getReleasedAt());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setApiGiantBombCompanyId($this->getApiGiantBombCompanyId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombGamePlatforms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGamePlatform($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameRelease($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombCompany object.
     *
     * @param  ChildApiGiantBombCompany $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombCompany(ChildApiGiantBombCompany $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombCompanyId(NULL);
        } else {
            $this->setApiGiantBombCompanyId($v->getId());
        }

        $this->aApiGiantBombCompany = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombCompany object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombPlatform($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombCompany object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombCompany The associated ChildApiGiantBombCompany object.
     * @throws PropelException
     */
    public function getApiGiantBombCompany(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombCompany === null && ($this->vgagpl_vgagco_id !== null)) {
            $this->aApiGiantBombCompany = ChildApiGiantBombCompanyQuery::create()->findPk($this->vgagpl_vgagco_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombCompany->addApiGiantBombPlatforms($this);
             */
        }

        return $this->aApiGiantBombCompany;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombGamePlatform' == $relationName) {
            return $this->initApiGiantBombGamePlatforms();
        }
        if ('ApiGiantBombGameRelease' == $relationName) {
            return $this->initApiGiantBombGameReleases();
        }
    }

    /**
     * Clears out the collApiGiantBombGamePlatforms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGamePlatforms()
     */
    public function clearApiGiantBombGamePlatforms()
    {
        $this->collApiGiantBombGamePlatforms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGamePlatforms collection loaded partially.
     */
    public function resetPartialApiGiantBombGamePlatforms($v = true)
    {
        $this->collApiGiantBombGamePlatformsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGamePlatforms collection.
     *
     * By default this just sets the collApiGiantBombGamePlatforms collection to an empty array (like clearcollApiGiantBombGamePlatforms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGamePlatforms($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGamePlatforms && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGamePlatformTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGamePlatforms = new $collectionClassName;
        $this->collApiGiantBombGamePlatforms->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform');
    }

    /**
     * Gets an array of ChildApiGiantBombGamePlatform objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombPlatform is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGamePlatform[] List of ChildApiGiantBombGamePlatform objects
     * @throws PropelException
     */
    public function getApiGiantBombGamePlatforms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePlatforms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePlatforms) {
                // return empty collection
                $this->initApiGiantBombGamePlatforms();
            } else {
                $collApiGiantBombGamePlatforms = ChildApiGiantBombGamePlatformQuery::create(null, $criteria)
                    ->filterByApiGiantBombPlatform($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGamePlatformsPartial && count($collApiGiantBombGamePlatforms)) {
                        $this->initApiGiantBombGamePlatforms(false);

                        foreach ($collApiGiantBombGamePlatforms as $obj) {
                            if (false == $this->collApiGiantBombGamePlatforms->contains($obj)) {
                                $this->collApiGiantBombGamePlatforms->append($obj);
                            }
                        }

                        $this->collApiGiantBombGamePlatformsPartial = true;
                    }

                    return $collApiGiantBombGamePlatforms;
                }

                if ($partial && $this->collApiGiantBombGamePlatforms) {
                    foreach ($this->collApiGiantBombGamePlatforms as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGamePlatforms[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGamePlatforms = $collApiGiantBombGamePlatforms;
                $this->collApiGiantBombGamePlatformsPartial = false;
            }
        }

        return $this->collApiGiantBombGamePlatforms;
    }

    /**
     * Sets a collection of ChildApiGiantBombGamePlatform objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGamePlatforms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setApiGiantBombGamePlatforms(Collection $apiGiantBombGamePlatforms, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGamePlatform[] $apiGiantBombGamePlatformsToDelete */
        $apiGiantBombGamePlatformsToDelete = $this->getApiGiantBombGamePlatforms(new Criteria(), $con)->diff($apiGiantBombGamePlatforms);


        $this->apiGiantBombGamePlatformsScheduledForDeletion = $apiGiantBombGamePlatformsToDelete;

        foreach ($apiGiantBombGamePlatformsToDelete as $apiGiantBombGamePlatformRemoved) {
            $apiGiantBombGamePlatformRemoved->setApiGiantBombPlatform(null);
        }

        $this->collApiGiantBombGamePlatforms = null;
        foreach ($apiGiantBombGamePlatforms as $apiGiantBombGamePlatform) {
            $this->addApiGiantBombGamePlatform($apiGiantBombGamePlatform);
        }

        $this->collApiGiantBombGamePlatforms = $apiGiantBombGamePlatforms;
        $this->collApiGiantBombGamePlatformsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGamePlatform objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGamePlatform objects.
     * @throws PropelException
     */
    public function countApiGiantBombGamePlatforms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePlatforms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePlatforms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGamePlatforms());
            }

            $query = ChildApiGiantBombGamePlatformQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombPlatform($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGamePlatforms);
    }

    /**
     * Method called to associate a ChildApiGiantBombGamePlatform object to this object
     * through the ChildApiGiantBombGamePlatform foreign key attribute.
     *
     * @param  ChildApiGiantBombGamePlatform $l ChildApiGiantBombGamePlatform
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function addApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $l)
    {
        if ($this->collApiGiantBombGamePlatforms === null) {
            $this->initApiGiantBombGamePlatforms();
            $this->collApiGiantBombGamePlatformsPartial = true;
        }

        if (!$this->collApiGiantBombGamePlatforms->contains($l)) {
            $this->doAddApiGiantBombGamePlatform($l);

            if ($this->apiGiantBombGamePlatformsScheduledForDeletion and $this->apiGiantBombGamePlatformsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGamePlatformsScheduledForDeletion->remove($this->apiGiantBombGamePlatformsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform The ChildApiGiantBombGamePlatform object to add.
     */
    protected function doAddApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform)
    {
        $this->collApiGiantBombGamePlatforms[]= $apiGiantBombGamePlatform;
        $apiGiantBombGamePlatform->setApiGiantBombPlatform($this);
    }

    /**
     * @param  ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform The ChildApiGiantBombGamePlatform object to remove.
     * @return $this|ChildApiGiantBombPlatform The current object (for fluent API support)
     */
    public function removeApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform)
    {
        if ($this->getApiGiantBombGamePlatforms()->contains($apiGiantBombGamePlatform)) {
            $pos = $this->collApiGiantBombGamePlatforms->search($apiGiantBombGamePlatform);
            $this->collApiGiantBombGamePlatforms->remove($pos);
            if (null === $this->apiGiantBombGamePlatformsScheduledForDeletion) {
                $this->apiGiantBombGamePlatformsScheduledForDeletion = clone $this->collApiGiantBombGamePlatforms;
                $this->apiGiantBombGamePlatformsScheduledForDeletion->clear();
            }
            $this->apiGiantBombGamePlatformsScheduledForDeletion[]= clone $apiGiantBombGamePlatform;
            $apiGiantBombGamePlatform->setApiGiantBombPlatform(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombPlatform is new, it will return
     * an empty collection; or if this ApiGiantBombPlatform has previously
     * been saved, it will retrieve related ApiGiantBombGamePlatforms from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombPlatform.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGamePlatform[] List of ChildApiGiantBombGamePlatform objects
     */
    public function getApiGiantBombGamePlatformsJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGamePlatformQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGamePlatforms($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleases()
     */
    public function clearApiGiantBombGameReleases()
    {
        $this->collApiGiantBombGameReleases = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleases collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleases($v = true)
    {
        $this->collApiGiantBombGameReleasesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleases collection.
     *
     * By default this just sets the collApiGiantBombGameReleases collection to an empty array (like clearcollApiGiantBombGameReleases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleases($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleases && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleaseTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleases = new $collectionClassName;
        $this->collApiGiantBombGameReleases->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease');
    }

    /**
     * Gets an array of ChildApiGiantBombGameRelease objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombPlatform is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleases(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                // return empty collection
                $this->initApiGiantBombGameReleases();
            } else {
                $collApiGiantBombGameReleases = ChildApiGiantBombGameReleaseQuery::create(null, $criteria)
                    ->filterByApiGiantBombPlatform($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleasesPartial && count($collApiGiantBombGameReleases)) {
                        $this->initApiGiantBombGameReleases(false);

                        foreach ($collApiGiantBombGameReleases as $obj) {
                            if (false == $this->collApiGiantBombGameReleases->contains($obj)) {
                                $this->collApiGiantBombGameReleases->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleasesPartial = true;
                    }

                    return $collApiGiantBombGameReleases;
                }

                if ($partial && $this->collApiGiantBombGameReleases) {
                    foreach ($this->collApiGiantBombGameReleases as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleases[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleases = $collApiGiantBombGameReleases;
                $this->collApiGiantBombGameReleasesPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleases;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameRelease objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleases A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombPlatform The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleases(Collection $apiGiantBombGameReleases, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameRelease[] $apiGiantBombGameReleasesToDelete */
        $apiGiantBombGameReleasesToDelete = $this->getApiGiantBombGameReleases(new Criteria(), $con)->diff($apiGiantBombGameReleases);


        $this->apiGiantBombGameReleasesScheduledForDeletion = $apiGiantBombGameReleasesToDelete;

        foreach ($apiGiantBombGameReleasesToDelete as $apiGiantBombGameReleaseRemoved) {
            $apiGiantBombGameReleaseRemoved->setApiGiantBombPlatform(null);
        }

        $this->collApiGiantBombGameReleases = null;
        foreach ($apiGiantBombGameReleases as $apiGiantBombGameRelease) {
            $this->addApiGiantBombGameRelease($apiGiantBombGameRelease);
        }

        $this->collApiGiantBombGameReleases = $apiGiantBombGameReleases;
        $this->collApiGiantBombGameReleasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameRelease objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameRelease objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleases(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleases());
            }

            $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombPlatform($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleases);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameRelease object to this object
     * through the ChildApiGiantBombGameRelease foreign key attribute.
     *
     * @param  ChildApiGiantBombGameRelease $l ChildApiGiantBombGameRelease
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform The current object (for fluent API support)
     */
    public function addApiGiantBombGameRelease(ChildApiGiantBombGameRelease $l)
    {
        if ($this->collApiGiantBombGameReleases === null) {
            $this->initApiGiantBombGameReleases();
            $this->collApiGiantBombGameReleasesPartial = true;
        }

        if (!$this->collApiGiantBombGameReleases->contains($l)) {
            $this->doAddApiGiantBombGameRelease($l);

            if ($this->apiGiantBombGameReleasesScheduledForDeletion and $this->apiGiantBombGameReleasesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleasesScheduledForDeletion->remove($this->apiGiantBombGameReleasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to add.
     */
    protected function doAddApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        $this->collApiGiantBombGameReleases[]= $apiGiantBombGameRelease;
        $apiGiantBombGameRelease->setApiGiantBombPlatform($this);
    }

    /**
     * @param  ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to remove.
     * @return $this|ChildApiGiantBombPlatform The current object (for fluent API support)
     */
    public function removeApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        if ($this->getApiGiantBombGameReleases()->contains($apiGiantBombGameRelease)) {
            $pos = $this->collApiGiantBombGameReleases->search($apiGiantBombGameRelease);
            $this->collApiGiantBombGameReleases->remove($pos);
            if (null === $this->apiGiantBombGameReleasesScheduledForDeletion) {
                $this->apiGiantBombGameReleasesScheduledForDeletion = clone $this->collApiGiantBombGameReleases;
                $this->apiGiantBombGameReleasesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleasesScheduledForDeletion[]= $apiGiantBombGameRelease;
            $apiGiantBombGameRelease->setApiGiantBombPlatform(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombPlatform is new, it will return
     * an empty collection; or if this ApiGiantBombPlatform has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombPlatform.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombPlatform is new, it will return
     * an empty collection; or if this ApiGiantBombPlatform has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombPlatform.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombGameRating(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRating', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombPlatform is new, it will return
     * an empty collection; or if this ApiGiantBombPlatform has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombPlatform.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombRegion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombRegion', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombCompany) {
            $this->aApiGiantBombCompany->removeApiGiantBombPlatform($this);
        }
        $this->vgagpl_id = null;
        $this->vgagpl_name = null;
        $this->vgagpl_abbr = null;
        $this->vgagpl_aliases = null;
        $this->vgagpl_aliases_unserialized = null;
        $this->vgagpl_summary = null;
        $this->vgagpl_description = null;
        $this->vgagpl_install_base = null;
        $this->vgagpl_online_support = null;
        $this->vgagpl_original_price = null;
        $this->vgagpl_released_at = null;
        $this->vgagpl_api_detail_url = null;
        $this->vgagpl_site_detail_url = null;
        $this->vgagpl_image_icon_url = null;
        $this->vgagpl_image_medium_url = null;
        $this->vgagpl_image_screen_url = null;
        $this->vgagpl_image_small_url = null;
        $this->vgagpl_image_super_url = null;
        $this->vgagpl_image_thumb_url = null;
        $this->vgagpl_image_tiny_url = null;
        $this->vgagpl_vgagco_id = null;
        $this->vgagpl_created_at = null;
        $this->vgagpl_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombGamePlatforms) {
                foreach ($this->collApiGiantBombGamePlatforms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleases) {
                foreach ($this->collApiGiantBombGameReleases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombGamePlatforms = null;
        $this->collApiGiantBombGameReleases = null;
        $this->aApiGiantBombCompany = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombPlatformTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
