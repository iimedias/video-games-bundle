<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise as ChildApiGiantBombFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery as ChildApiGiantBombFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombFranchiseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_franchise_vgagfr' table.
 *
 *
 *
 * @method     ChildApiGiantBombFranchiseQuery orderById($order = Criteria::ASC) Order by the vgagfr_id column
 * @method     ChildApiGiantBombFranchiseQuery orderByName($order = Criteria::ASC) Order by the vgagfr_name column
 * @method     ChildApiGiantBombFranchiseQuery orderByAliases($order = Criteria::ASC) Order by the vgagfr_aliases column
 * @method     ChildApiGiantBombFranchiseQuery orderBySummary($order = Criteria::ASC) Order by the vgagfr_summary column
 * @method     ChildApiGiantBombFranchiseQuery orderByDescription($order = Criteria::ASC) Order by the vgagfr_description column
 * @method     ChildApiGiantBombFranchiseQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagfr_api_detail_url column
 * @method     ChildApiGiantBombFranchiseQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagfr_site_detail_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagfr_image_icon_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagfr_image_medium_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagfr_image_screen_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagfr_image_small_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagfr_image_super_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagfr_image_thumb_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagfr_image_tiny_url column
 * @method     ChildApiGiantBombFranchiseQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagfr_created_at column
 * @method     ChildApiGiantBombFranchiseQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagfr_updated_at column
 *
 * @method     ChildApiGiantBombFranchiseQuery groupById() Group by the vgagfr_id column
 * @method     ChildApiGiantBombFranchiseQuery groupByName() Group by the vgagfr_name column
 * @method     ChildApiGiantBombFranchiseQuery groupByAliases() Group by the vgagfr_aliases column
 * @method     ChildApiGiantBombFranchiseQuery groupBySummary() Group by the vgagfr_summary column
 * @method     ChildApiGiantBombFranchiseQuery groupByDescription() Group by the vgagfr_description column
 * @method     ChildApiGiantBombFranchiseQuery groupByApiDetailUrl() Group by the vgagfr_api_detail_url column
 * @method     ChildApiGiantBombFranchiseQuery groupBySiteDetailUrl() Group by the vgagfr_site_detail_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageIconUrl() Group by the vgagfr_image_icon_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageMediumUrl() Group by the vgagfr_image_medium_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageScreenUrl() Group by the vgagfr_image_screen_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageSmallUrl() Group by the vgagfr_image_small_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageSuperUrl() Group by the vgagfr_image_super_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageThumbUrl() Group by the vgagfr_image_thumb_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByImageTinyUrl() Group by the vgagfr_image_tiny_url column
 * @method     ChildApiGiantBombFranchiseQuery groupByCreatedAt() Group by the vgagfr_created_at column
 * @method     ChildApiGiantBombFranchiseQuery groupByUpdatedAt() Group by the vgagfr_updated_at column
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombFranchiseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombFranchiseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombFranchiseQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombFranchiseQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoinApiGiantBombFirstFranchiseConcept($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstFranchiseConcept relation
 * @method     ChildApiGiantBombFranchiseQuery rightJoinApiGiantBombFirstFranchiseConcept($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstFranchiseConcept relation
 * @method     ChildApiGiantBombFranchiseQuery innerJoinApiGiantBombFirstFranchiseConcept($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstFranchiseConcept relation
 *
 * @method     ChildApiGiantBombFranchiseQuery joinWithApiGiantBombFirstFranchiseConcept($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstFranchiseConcept relation
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoinWithApiGiantBombFirstFranchiseConcept() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstFranchiseConcept relation
 * @method     ChildApiGiantBombFranchiseQuery rightJoinWithApiGiantBombFirstFranchiseConcept() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstFranchiseConcept relation
 * @method     ChildApiGiantBombFranchiseQuery innerJoinWithApiGiantBombFirstFranchiseConcept() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstFranchiseConcept relation
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoinApiGiantBombGameFranchise($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombFranchiseQuery rightJoinApiGiantBombGameFranchise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombFranchiseQuery innerJoinApiGiantBombGameFranchise($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     ChildApiGiantBombFranchiseQuery joinWithApiGiantBombGameFranchise($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     ChildApiGiantBombFranchiseQuery leftJoinWithApiGiantBombGameFranchise() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombFranchiseQuery rightJoinWithApiGiantBombGameFranchise() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombFranchiseQuery innerJoinWithApiGiantBombGameFranchise() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombFranchise findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombFranchise matching the query
 * @method     ChildApiGiantBombFranchise findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombFranchise matching the query, or a new ChildApiGiantBombFranchise object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombFranchise findOneById(int $vgagfr_id) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_id column
 * @method     ChildApiGiantBombFranchise findOneByName(string $vgagfr_name) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_name column
 * @method     ChildApiGiantBombFranchise findOneByAliases(array $vgagfr_aliases) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_aliases column
 * @method     ChildApiGiantBombFranchise findOneBySummary(string $vgagfr_summary) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_summary column
 * @method     ChildApiGiantBombFranchise findOneByDescription(string $vgagfr_description) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_description column
 * @method     ChildApiGiantBombFranchise findOneByApiDetailUrl(string $vgagfr_api_detail_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_api_detail_url column
 * @method     ChildApiGiantBombFranchise findOneBySiteDetailUrl(string $vgagfr_site_detail_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_site_detail_url column
 * @method     ChildApiGiantBombFranchise findOneByImageIconUrl(string $vgagfr_image_icon_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_icon_url column
 * @method     ChildApiGiantBombFranchise findOneByImageMediumUrl(string $vgagfr_image_medium_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_medium_url column
 * @method     ChildApiGiantBombFranchise findOneByImageScreenUrl(string $vgagfr_image_screen_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_screen_url column
 * @method     ChildApiGiantBombFranchise findOneByImageSmallUrl(string $vgagfr_image_small_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_small_url column
 * @method     ChildApiGiantBombFranchise findOneByImageSuperUrl(string $vgagfr_image_super_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_super_url column
 * @method     ChildApiGiantBombFranchise findOneByImageThumbUrl(string $vgagfr_image_thumb_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_thumb_url column
 * @method     ChildApiGiantBombFranchise findOneByImageTinyUrl(string $vgagfr_image_tiny_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_tiny_url column
 * @method     ChildApiGiantBombFranchise findOneByCreatedAt(string $vgagfr_created_at) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_created_at column
 * @method     ChildApiGiantBombFranchise findOneByUpdatedAt(string $vgagfr_updated_at) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_updated_at column *

 * @method     ChildApiGiantBombFranchise requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombFranchise by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombFranchise matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombFranchise requireOneById(int $vgagfr_id) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByName(string $vgagfr_name) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByAliases(array $vgagfr_aliases) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneBySummary(string $vgagfr_summary) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByDescription(string $vgagfr_description) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByApiDetailUrl(string $vgagfr_api_detail_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneBySiteDetailUrl(string $vgagfr_site_detail_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageIconUrl(string $vgagfr_image_icon_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageMediumUrl(string $vgagfr_image_medium_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageScreenUrl(string $vgagfr_image_screen_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageSmallUrl(string $vgagfr_image_small_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageSuperUrl(string $vgagfr_image_super_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageThumbUrl(string $vgagfr_image_thumb_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByImageTinyUrl(string $vgagfr_image_tiny_url) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByCreatedAt(string $vgagfr_created_at) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombFranchise requireOneByUpdatedAt(string $vgagfr_updated_at) Return the first ChildApiGiantBombFranchise filtered by the vgagfr_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombFranchise objects based on current ModelCriteria
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findById(int $vgagfr_id) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_id column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByName(string $vgagfr_name) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_name column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByAliases(array $vgagfr_aliases) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_aliases column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findBySummary(string $vgagfr_summary) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_summary column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByDescription(string $vgagfr_description) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_description column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByApiDetailUrl(string $vgagfr_api_detail_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_api_detail_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findBySiteDetailUrl(string $vgagfr_site_detail_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_site_detail_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageIconUrl(string $vgagfr_image_icon_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_icon_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageMediumUrl(string $vgagfr_image_medium_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_medium_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageScreenUrl(string $vgagfr_image_screen_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_screen_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageSmallUrl(string $vgagfr_image_small_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_small_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageSuperUrl(string $vgagfr_image_super_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_super_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageThumbUrl(string $vgagfr_image_thumb_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_thumb_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByImageTinyUrl(string $vgagfr_image_tiny_url) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_image_tiny_url column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByCreatedAt(string $vgagfr_created_at) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_created_at column
 * @method     ChildApiGiantBombFranchise[]|ObjectCollection findByUpdatedAt(string $vgagfr_updated_at) Return ChildApiGiantBombFranchise objects filtered by the vgagfr_updated_at column
 * @method     ChildApiGiantBombFranchise[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombFranchiseQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombFranchiseQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombFranchise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombFranchiseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombFranchiseQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombFranchiseQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombFranchiseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombFranchise|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombFranchiseTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombFranchise A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagfr_id, vgagfr_name, vgagfr_aliases, vgagfr_summary, vgagfr_description, vgagfr_api_detail_url, vgagfr_site_detail_url, vgagfr_image_icon_url, vgagfr_image_medium_url, vgagfr_image_screen_url, vgagfr_image_small_url, vgagfr_image_super_url, vgagfr_image_thumb_url, vgagfr_image_tiny_url, vgagfr_created_at, vgagfr_updated_at FROM videogames_api_giantbomb_franchise_vgagfr WHERE vgagfr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombFranchise $obj */
            $obj = new ChildApiGiantBombFranchise();
            $obj->hydrate($row);
            ApiGiantBombFranchiseTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombFranchise|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagfr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagfr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagfr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagfr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagfr_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagfr_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagfr_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagfr_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagfr_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagfr_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagfr_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagfr_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagfr_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagfr_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagfr_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagfr_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagfr_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagfr_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagfr_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagfr_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagfr_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagfr_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagfr_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagfr_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagfr_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagfr_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagfr_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagfr_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagfr_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagfr_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagfr_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagfr_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagfr_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagfr_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagfr_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagfr_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagfr_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagfr_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagfr_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagfr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagfr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagfr_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept|ObjectCollection $apiGiantBombConcept the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstFranchiseConcept($apiGiantBombConcept, $comparison = null)
    {
        if ($apiGiantBombConcept instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept) {
            return $this
                ->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $apiGiantBombConcept->getGiantBombFirstFranchiseId(), $comparison);
        } elseif ($apiGiantBombConcept instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombFirstFranchiseConceptQuery()
                ->filterByPrimaryKeys($apiGiantBombConcept->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombFirstFranchiseConcept() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstFranchiseConcept relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstFranchiseConcept($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstFranchiseConcept');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstFranchiseConcept');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstFranchiseConcept relation ApiGiantBombConcept object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstFranchiseConceptQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstFranchiseConcept($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstFranchiseConcept', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise|ObjectCollection $apiGiantBombGameFranchise the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameFranchise($apiGiantBombGameFranchise, $comparison = null)
    {
        if ($apiGiantBombGameFranchise instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise) {
            return $this
                ->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $apiGiantBombGameFranchise->getApiGiantBombFranchiseId(), $comparison);
        } elseif ($apiGiantBombGameFranchise instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameFranchiseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameFranchise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameFranchise() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameFranchise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameFranchise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameFranchise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameFranchise');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameFranchise relation ApiGiantBombGameFranchise object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameFranchiseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameFranchise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameFranchise', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombFranchise $apiGiantBombFranchise Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombFranchiseQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombFranchise = null)
    {
        if ($apiGiantBombFranchise) {
            $this->addUsingAlias(ApiGiantBombFranchiseTableMap::COL_VGAGFR_ID, $apiGiantBombFranchise->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_franchise_vgagfr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombFranchiseTableMap::clearInstancePool();
            ApiGiantBombFranchiseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombFranchiseTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombFranchiseTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombFranchiseTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombFranchiseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombFranchiseQuery
