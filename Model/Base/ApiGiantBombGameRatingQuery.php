<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating as ChildApiGiantBombGameRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery as ChildApiGiantBombGameRatingQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameRatingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_game_rating_vgaggr' table.
 *
 *
 *
 * @method     ChildApiGiantBombGameRatingQuery orderById($order = Criteria::ASC) Order by the vgaggr_id column
 * @method     ChildApiGiantBombGameRatingQuery orderByName($order = Criteria::ASC) Order by the vgaggr_name column
 * @method     ChildApiGiantBombGameRatingQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgaggr_api_detail_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgaggr_image_icon_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgaggr_image_medium_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgaggr_image_screen_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgaggr_image_small_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgaggr_image_super_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgaggr_image_thumb_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgaggr_image_tiny_url column
 * @method     ChildApiGiantBombGameRatingQuery orderByGiantBombRatingBoardId($order = Criteria::ASC) Order by the vgagch_vgagrb_id column
 *
 * @method     ChildApiGiantBombGameRatingQuery groupById() Group by the vgaggr_id column
 * @method     ChildApiGiantBombGameRatingQuery groupByName() Group by the vgaggr_name column
 * @method     ChildApiGiantBombGameRatingQuery groupByApiDetailUrl() Group by the vgaggr_api_detail_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageIconUrl() Group by the vgaggr_image_icon_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageMediumUrl() Group by the vgaggr_image_medium_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageScreenUrl() Group by the vgaggr_image_screen_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageSmallUrl() Group by the vgaggr_image_small_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageSuperUrl() Group by the vgaggr_image_super_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageThumbUrl() Group by the vgaggr_image_thumb_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByImageTinyUrl() Group by the vgaggr_image_tiny_url column
 * @method     ChildApiGiantBombGameRatingQuery groupByGiantBombRatingBoardId() Group by the vgagch_vgagrb_id column
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombGameRatingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombGameRatingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameRatingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameRatingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinApiGiantBombRatingBoard($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombRatingBoard relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinApiGiantBombRatingBoard($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombRatingBoard relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinApiGiantBombRatingBoard($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombRatingBoard relation
 *
 * @method     ChildApiGiantBombGameRatingQuery joinWithApiGiantBombRatingBoard($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombRatingBoard relation
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinWithApiGiantBombRatingBoard() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombRatingBoard relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinWithApiGiantBombRatingBoard() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombRatingBoard relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinWithApiGiantBombRatingBoard() Adds a INNER JOIN clause and with to the query using the ApiGiantBombRatingBoard relation
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameRatingQuery joinWithApiGiantBombGameOriginalRating($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinWithApiGiantBombGameOriginalRating() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinWithApiGiantBombGameOriginalRating() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinWithApiGiantBombGameOriginalRating() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinApiGiantBombGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinApiGiantBombGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinApiGiantBombGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombGameRatingQuery joinWithApiGiantBombGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombGameRatingQuery leftJoinWithApiGiantBombGameRelease() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameRatingQuery rightJoinWithApiGiantBombGameRelease() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameRatingQuery innerJoinWithApiGiantBombGameRelease() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombGameRating findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRating matching the query
 * @method     ChildApiGiantBombGameRating findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRating matching the query, or a new ChildApiGiantBombGameRating object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombGameRating findOneById(int $vgaggr_id) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_id column
 * @method     ChildApiGiantBombGameRating findOneByName(string $vgaggr_name) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_name column
 * @method     ChildApiGiantBombGameRating findOneByApiDetailUrl(string $vgaggr_api_detail_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_api_detail_url column
 * @method     ChildApiGiantBombGameRating findOneByImageIconUrl(string $vgaggr_image_icon_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_icon_url column
 * @method     ChildApiGiantBombGameRating findOneByImageMediumUrl(string $vgaggr_image_medium_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_medium_url column
 * @method     ChildApiGiantBombGameRating findOneByImageScreenUrl(string $vgaggr_image_screen_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_screen_url column
 * @method     ChildApiGiantBombGameRating findOneByImageSmallUrl(string $vgaggr_image_small_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_small_url column
 * @method     ChildApiGiantBombGameRating findOneByImageSuperUrl(string $vgaggr_image_super_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_super_url column
 * @method     ChildApiGiantBombGameRating findOneByImageThumbUrl(string $vgaggr_image_thumb_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_thumb_url column
 * @method     ChildApiGiantBombGameRating findOneByImageTinyUrl(string $vgaggr_image_tiny_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_tiny_url column
 * @method     ChildApiGiantBombGameRating findOneByGiantBombRatingBoardId(int $vgagch_vgagrb_id) Return the first ChildApiGiantBombGameRating filtered by the vgagch_vgagrb_id column *

 * @method     ChildApiGiantBombGameRating requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombGameRating by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRating matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameRating requireOneById(int $vgaggr_id) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByName(string $vgaggr_name) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByApiDetailUrl(string $vgaggr_api_detail_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageIconUrl(string $vgaggr_image_icon_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageMediumUrl(string $vgaggr_image_medium_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageScreenUrl(string $vgaggr_image_screen_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageSmallUrl(string $vgaggr_image_small_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageSuperUrl(string $vgaggr_image_super_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageThumbUrl(string $vgaggr_image_thumb_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByImageTinyUrl(string $vgaggr_image_tiny_url) Return the first ChildApiGiantBombGameRating filtered by the vgaggr_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRating requireOneByGiantBombRatingBoardId(int $vgagch_vgagrb_id) Return the first ChildApiGiantBombGameRating filtered by the vgagch_vgagrb_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombGameRating objects based on current ModelCriteria
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findById(int $vgaggr_id) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_id column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByName(string $vgaggr_name) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_name column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByApiDetailUrl(string $vgaggr_api_detail_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_api_detail_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageIconUrl(string $vgaggr_image_icon_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_icon_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageMediumUrl(string $vgaggr_image_medium_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_medium_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageScreenUrl(string $vgaggr_image_screen_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_screen_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageSmallUrl(string $vgaggr_image_small_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_small_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageSuperUrl(string $vgaggr_image_super_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_super_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageThumbUrl(string $vgaggr_image_thumb_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_thumb_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByImageTinyUrl(string $vgaggr_image_tiny_url) Return ChildApiGiantBombGameRating objects filtered by the vgaggr_image_tiny_url column
 * @method     ChildApiGiantBombGameRating[]|ObjectCollection findByGiantBombRatingBoardId(int $vgagch_vgagrb_id) Return ChildApiGiantBombGameRating objects filtered by the vgagch_vgagrb_id column
 * @method     ChildApiGiantBombGameRating[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombGameRatingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameRatingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRating', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombGameRatingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombGameRatingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombGameRatingQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombGameRatingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombGameRating|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombGameRatingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameRating A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaggr_id, vgaggr_name, vgaggr_api_detail_url, vgaggr_image_icon_url, vgaggr_image_medium_url, vgaggr_image_screen_url, vgaggr_image_small_url, vgaggr_image_super_url, vgaggr_image_thumb_url, vgaggr_image_tiny_url, vgagch_vgagrb_id FROM videogames_api_giantbomb_game_rating_vgaggr WHERE vgaggr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombGameRating $obj */
            $obj = new ChildApiGiantBombGameRating();
            $obj->hydrate($row);
            ApiGiantBombGameRatingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombGameRating|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaggr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaggr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaggr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaggr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaggr_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaggr_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaggr_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgaggr_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgaggr_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgaggr_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgaggr_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgaggr_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgaggr_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgaggr_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgaggr_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgaggr_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgaggr_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgaggr_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgaggr_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgaggr_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgaggr_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgaggr_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgaggr_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgaggr_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgaggr_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_vgagrb_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombRatingBoardId(1234); // WHERE vgagch_vgagrb_id = 1234
     * $query->filterByGiantBombRatingBoardId(array(12, 34)); // WHERE vgagch_vgagrb_id IN (12, 34)
     * $query->filterByGiantBombRatingBoardId(array('min' => 12)); // WHERE vgagch_vgagrb_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombRatingBoard()
     *
     * @param     mixed $giantBombRatingBoardId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByGiantBombRatingBoardId($giantBombRatingBoardId = null, $comparison = null)
    {
        if (is_array($giantBombRatingBoardId)) {
            $useMinMax = false;
            if (isset($giantBombRatingBoardId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $giantBombRatingBoardId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombRatingBoardId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $giantBombRatingBoardId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $giantBombRatingBoardId, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard|ObjectCollection $apiGiantBombRatingBoard The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombRatingBoard($apiGiantBombRatingBoard, $comparison = null)
    {
        if ($apiGiantBombRatingBoard instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard) {
            return $this
                ->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $apiGiantBombRatingBoard->getId(), $comparison);
        } elseif ($apiGiantBombRatingBoard instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGCH_VGAGRB_ID, $apiGiantBombRatingBoard->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombRatingBoard() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoard or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombRatingBoard relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function joinApiGiantBombRatingBoard($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombRatingBoard');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombRatingBoard');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombRatingBoard relation ApiGiantBombRatingBoard object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombRatingBoardQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombRatingBoard($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombRatingBoard', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombRatingBoardQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating|ObjectCollection $apiGiantBombGameOriginalRating the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameOriginalRating($apiGiantBombGameOriginalRating, $comparison = null)
    {
        if ($apiGiantBombGameOriginalRating instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating) {
            return $this
                ->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $apiGiantBombGameOriginalRating->getApiGiantBombGameRatingId(), $comparison);
        } elseif ($apiGiantBombGameOriginalRating instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameOriginalRatingQuery()
                ->filterByPrimaryKeys($apiGiantBombGameOriginalRating->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameOriginalRating() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameOriginalRating($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameOriginalRating');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameOriginalRating');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameOriginalRating relation ApiGiantBombGameOriginalRating object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameOriginalRatingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameOriginalRating($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameOriginalRating', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease|ObjectCollection $apiGiantBombGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRelease($apiGiantBombGameRelease, $comparison = null)
    {
        if ($apiGiantBombGameRelease instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease) {
            return $this
                ->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $apiGiantBombGameRelease->getApiGiantBombGameRatingId(), $comparison);
        } elseif ($apiGiantBombGameRelease instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameRelease($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameRelease');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameRelease relation ApiGiantBombGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameRelease', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombGameRating $apiGiantBombGameRating Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombGameRatingQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombGameRating = null)
    {
        if ($apiGiantBombGameRating) {
            $this->addUsingAlias(ApiGiantBombGameRatingTableMap::COL_VGAGGR_ID, $apiGiantBombGameRating->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_game_rating_vgaggr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombGameRatingTableMap::clearInstancePool();
            ApiGiantBombGameRatingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameRatingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombGameRatingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombGameRatingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombGameRatingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombGameRatingQuery
