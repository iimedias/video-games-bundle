<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame as ChildApiTwitchGame;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery as ChildApiTwitchGameQuery;
use IiMedias\VideoGamesBundle\Model\Game as ChildGame;
use IiMedias\VideoGamesBundle\Model\GameQuery as ChildGameQuery;
use IiMedias\VideoGamesBundle\Model\GameRelease as ChildGameRelease;
use IiMedias\VideoGamesBundle\Model\GameReleaseQuery as ChildGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGame as ChildSiteGameBlogGame;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery as ChildSiteGameBlogGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGame as ChildSiteGameKultGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery as ChildSiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame as ChildSiteJeuxVideoComGame;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery as ChildSiteJeuxVideoComGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiTwitchGameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\GameReleaseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\GameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameBlogGameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultGameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\SiteJeuxVideoComGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'videogames_game_vgagam' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class Game implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\GameTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagam_id field.
     *
     * @var        int
     */
    protected $vgagam_id;

    /**
     * The value for the vgagam_name field.
     *
     * @var        string
     */
    protected $vgagam_name;

    /**
     * @var        ObjectCollection|ChildGameRelease[] Collection to store aggregation of ChildGameRelease objects.
     */
    protected $collGameReleases;
    protected $collGameReleasesPartial;

    /**
     * @var        ObjectCollection|ChildSiteJeuxVideoComGame[] Collection to store aggregation of ChildSiteJeuxVideoComGame objects.
     */
    protected $collSiteJeuxVideoComGames;
    protected $collSiteJeuxVideoComGamesPartial;

    /**
     * @var        ObjectCollection|ChildSiteGameBlogGame[] Collection to store aggregation of ChildSiteGameBlogGame objects.
     */
    protected $collSiteGameBlogGames;
    protected $collSiteGameBlogGamesPartial;

    /**
     * @var        ObjectCollection|ChildSiteGameKultGame[] Collection to store aggregation of ChildSiteGameKultGame objects.
     */
    protected $collSiteGameKultGames;
    protected $collSiteGameKultGamesPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGame[] Collection to store aggregation of ChildApiGiantBombGame objects.
     */
    protected $collApiGiantBombGames;
    protected $collApiGiantBombGamesPartial;

    /**
     * @var        ObjectCollection|ChildApiTwitchGame[] Collection to store aggregation of ChildApiTwitchGame objects.
     */
    protected $collApiTwitchGames;
    protected $collApiTwitchGamesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildGameRelease[]
     */
    protected $gameReleasesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteJeuxVideoComGame[]
     */
    protected $siteJeuxVideoComGamesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteGameBlogGame[]
     */
    protected $siteGameBlogGamesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteGameKultGame[]
     */
    protected $siteGameKultGamesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGame[]
     */
    protected $apiGiantBombGamesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiTwitchGame[]
     */
    protected $apiTwitchGamesScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\Game object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Game</code> instance.  If
     * <code>obj</code> is an instance of <code>Game</code>, delegates to
     * <code>equals(Game)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Game The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagam_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagam_id;
    }

    /**
     * Get the [vgagam_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagam_name;
    }

    /**
     * Set the value of [vgagam_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagam_id !== $v) {
            $this->vgagam_id = $v;
            $this->modifiedColumns[GameTableMap::COL_VGAGAM_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagam_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagam_name !== $v) {
            $this->vgagam_name = $v;
            $this->modifiedColumns[GameTableMap::COL_VGAGAM_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : GameTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagam_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : GameTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagam_name = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 2; // 2 = GameTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\Game'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(GameTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildGameQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collGameReleases = null;

            $this->collSiteJeuxVideoComGames = null;

            $this->collSiteGameBlogGames = null;

            $this->collSiteGameKultGames = null;

            $this->collApiGiantBombGames = null;

            $this->collApiTwitchGames = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Game::setDeleted()
     * @see Game::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildGameQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(GameTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                GameTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->gameReleasesScheduledForDeletion !== null) {
                if (!$this->gameReleasesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\GameReleaseQuery::create()
                        ->filterByPrimaryKeys($this->gameReleasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->gameReleasesScheduledForDeletion = null;
                }
            }

            if ($this->collGameReleases !== null) {
                foreach ($this->collGameReleases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteJeuxVideoComGamesScheduledForDeletion !== null) {
                if (!$this->siteJeuxVideoComGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery::create()
                        ->filterByPrimaryKeys($this->siteJeuxVideoComGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteJeuxVideoComGamesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteJeuxVideoComGames !== null) {
                foreach ($this->collSiteJeuxVideoComGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteGameBlogGamesScheduledForDeletion !== null) {
                if (!$this->siteGameBlogGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery::create()
                        ->filterByPrimaryKeys($this->siteGameBlogGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteGameBlogGamesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteGameBlogGames !== null) {
                foreach ($this->collSiteGameBlogGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteGameKultGamesScheduledForDeletion !== null) {
                if (!$this->siteGameKultGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery::create()
                        ->filterByPrimaryKeys($this->siteGameKultGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteGameKultGamesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteGameKultGames !== null) {
                foreach ($this->collSiteGameKultGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGamesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGamesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGames !== null) {
                foreach ($this->collApiGiantBombGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiTwitchGamesScheduledForDeletion !== null) {
                if (!$this->apiTwitchGamesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery::create()
                        ->filterByPrimaryKeys($this->apiTwitchGamesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiTwitchGamesScheduledForDeletion = null;
                }
            }

            if ($this->collApiTwitchGames !== null) {
                foreach ($this->collApiTwitchGames as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[GameTableMap::COL_VGAGAM_ID] = true;
        if (null !== $this->vgagam_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . GameTableMap::COL_VGAGAM_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(GameTableMap::COL_VGAGAM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagam_id';
        }
        if ($this->isColumnModified(GameTableMap::COL_VGAGAM_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagam_name';
        }

        $sql = sprintf(
            'INSERT INTO videogames_game_vgagam (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagam_id':
                        $stmt->bindValue($identifier, $this->vgagam_id, PDO::PARAM_INT);
                        break;
                    case 'vgagam_name':
                        $stmt->bindValue($identifier, $this->vgagam_name, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Game'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Game'][$this->hashCode()] = true;
        $keys = GameTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collGameReleases) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'gameReleases';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_game_release_vgagmrs';
                        break;
                    default:
                        $key = 'GameReleases';
                }

                $result[$key] = $this->collGameReleases->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteJeuxVideoComGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteJeuxVideoComGames';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_site_jeuxvideocom_game_vgajgas';
                        break;
                    default:
                        $key = 'SiteJeuxVideoComGames';
                }

                $result[$key] = $this->collSiteJeuxVideoComGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteGameBlogGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteGameBlogGames';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_site_gameblog_game_vgabgas';
                        break;
                    default:
                        $key = 'SiteGameBlogGames';
                }

                $result[$key] = $this->collSiteGameBlogGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteGameKultGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteGameKultGames';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_site_gamekult_game_vgakgas';
                        break;
                    default:
                        $key = 'SiteGameKultGames';
                }

                $result[$key] = $this->collSiteGameKultGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGames';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_vgaggas';
                        break;
                    default:
                        $key = 'ApiGiantBombGames';
                }

                $result[$key] = $this->collApiGiantBombGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiTwitchGames) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiTwitchGames';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_twitch_vgatgas';
                        break;
                    default:
                        $key = 'ApiTwitchGames';
                }

                $result[$key] = $this->collApiTwitchGames->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = GameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = GameTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(GameTableMap::DATABASE_NAME);

        if ($this->isColumnModified(GameTableMap::COL_VGAGAM_ID)) {
            $criteria->add(GameTableMap::COL_VGAGAM_ID, $this->vgagam_id);
        }
        if ($this->isColumnModified(GameTableMap::COL_VGAGAM_NAME)) {
            $criteria->add(GameTableMap::COL_VGAGAM_NAME, $this->vgagam_name);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildGameQuery::create();
        $criteria->add(GameTableMap::COL_VGAGAM_ID, $this->vgagam_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagam_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\Game (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getGameReleases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addGameRelease($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteJeuxVideoComGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteJeuxVideoComGame($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteGameBlogGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteGameBlogGame($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteGameKultGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteGameKultGame($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGame($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiTwitchGames() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiTwitchGame($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\Game Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('GameRelease' == $relationName) {
            return $this->initGameReleases();
        }
        if ('SiteJeuxVideoComGame' == $relationName) {
            return $this->initSiteJeuxVideoComGames();
        }
        if ('SiteGameBlogGame' == $relationName) {
            return $this->initSiteGameBlogGames();
        }
        if ('SiteGameKultGame' == $relationName) {
            return $this->initSiteGameKultGames();
        }
        if ('ApiGiantBombGame' == $relationName) {
            return $this->initApiGiantBombGames();
        }
        if ('ApiTwitchGame' == $relationName) {
            return $this->initApiTwitchGames();
        }
    }

    /**
     * Clears out the collGameReleases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addGameReleases()
     */
    public function clearGameReleases()
    {
        $this->collGameReleases = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collGameReleases collection loaded partially.
     */
    public function resetPartialGameReleases($v = true)
    {
        $this->collGameReleasesPartial = $v;
    }

    /**
     * Initializes the collGameReleases collection.
     *
     * By default this just sets the collGameReleases collection to an empty array (like clearcollGameReleases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initGameReleases($overrideExisting = true)
    {
        if (null !== $this->collGameReleases && !$overrideExisting) {
            return;
        }

        $collectionClassName = GameReleaseTableMap::getTableMap()->getCollectionClassName();

        $this->collGameReleases = new $collectionClassName;
        $this->collGameReleases->setModel('\IiMedias\VideoGamesBundle\Model\GameRelease');
    }

    /**
     * Gets an array of ChildGameRelease objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildGameRelease[] List of ChildGameRelease objects
     * @throws PropelException
     */
    public function getGameReleases(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collGameReleasesPartial && !$this->isNew();
        if (null === $this->collGameReleases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collGameReleases) {
                // return empty collection
                $this->initGameReleases();
            } else {
                $collGameReleases = ChildGameReleaseQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collGameReleasesPartial && count($collGameReleases)) {
                        $this->initGameReleases(false);

                        foreach ($collGameReleases as $obj) {
                            if (false == $this->collGameReleases->contains($obj)) {
                                $this->collGameReleases->append($obj);
                            }
                        }

                        $this->collGameReleasesPartial = true;
                    }

                    return $collGameReleases;
                }

                if ($partial && $this->collGameReleases) {
                    foreach ($this->collGameReleases as $obj) {
                        if ($obj->isNew()) {
                            $collGameReleases[] = $obj;
                        }
                    }
                }

                $this->collGameReleases = $collGameReleases;
                $this->collGameReleasesPartial = false;
            }
        }

        return $this->collGameReleases;
    }

    /**
     * Sets a collection of ChildGameRelease objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $gameReleases A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setGameReleases(Collection $gameReleases, ConnectionInterface $con = null)
    {
        /** @var ChildGameRelease[] $gameReleasesToDelete */
        $gameReleasesToDelete = $this->getGameReleases(new Criteria(), $con)->diff($gameReleases);


        $this->gameReleasesScheduledForDeletion = $gameReleasesToDelete;

        foreach ($gameReleasesToDelete as $gameReleaseRemoved) {
            $gameReleaseRemoved->setGame(null);
        }

        $this->collGameReleases = null;
        foreach ($gameReleases as $gameRelease) {
            $this->addGameRelease($gameRelease);
        }

        $this->collGameReleases = $gameReleases;
        $this->collGameReleasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related GameRelease objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related GameRelease objects.
     * @throws PropelException
     */
    public function countGameReleases(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collGameReleasesPartial && !$this->isNew();
        if (null === $this->collGameReleases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collGameReleases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getGameReleases());
            }

            $query = ChildGameReleaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collGameReleases);
    }

    /**
     * Method called to associate a ChildGameRelease object to this object
     * through the ChildGameRelease foreign key attribute.
     *
     * @param  ChildGameRelease $l ChildGameRelease
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addGameRelease(ChildGameRelease $l)
    {
        if ($this->collGameReleases === null) {
            $this->initGameReleases();
            $this->collGameReleasesPartial = true;
        }

        if (!$this->collGameReleases->contains($l)) {
            $this->doAddGameRelease($l);

            if ($this->gameReleasesScheduledForDeletion and $this->gameReleasesScheduledForDeletion->contains($l)) {
                $this->gameReleasesScheduledForDeletion->remove($this->gameReleasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildGameRelease $gameRelease The ChildGameRelease object to add.
     */
    protected function doAddGameRelease(ChildGameRelease $gameRelease)
    {
        $this->collGameReleases[]= $gameRelease;
        $gameRelease->setGame($this);
    }

    /**
     * @param  ChildGameRelease $gameRelease The ChildGameRelease object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeGameRelease(ChildGameRelease $gameRelease)
    {
        if ($this->getGameReleases()->contains($gameRelease)) {
            $pos = $this->collGameReleases->search($gameRelease);
            $this->collGameReleases->remove($pos);
            if (null === $this->gameReleasesScheduledForDeletion) {
                $this->gameReleasesScheduledForDeletion = clone $this->collGameReleases;
                $this->gameReleasesScheduledForDeletion->clear();
            }
            $this->gameReleasesScheduledForDeletion[]= clone $gameRelease;
            $gameRelease->setGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Game is new, it will return
     * an empty collection; or if this Game has previously
     * been saved, it will retrieve related GameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Game.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGameRelease[] List of ChildGameRelease objects
     */
    public function getGameReleasesJoinPlatform(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGameReleaseQuery::create(null, $criteria);
        $query->joinWith('Platform', $joinBehavior);

        return $this->getGameReleases($query, $con);
    }

    /**
     * Clears out the collSiteJeuxVideoComGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteJeuxVideoComGames()
     */
    public function clearSiteJeuxVideoComGames()
    {
        $this->collSiteJeuxVideoComGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteJeuxVideoComGames collection loaded partially.
     */
    public function resetPartialSiteJeuxVideoComGames($v = true)
    {
        $this->collSiteJeuxVideoComGamesPartial = $v;
    }

    /**
     * Initializes the collSiteJeuxVideoComGames collection.
     *
     * By default this just sets the collSiteJeuxVideoComGames collection to an empty array (like clearcollSiteJeuxVideoComGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteJeuxVideoComGames($overrideExisting = true)
    {
        if (null !== $this->collSiteJeuxVideoComGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteJeuxVideoComGameTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteJeuxVideoComGames = new $collectionClassName;
        $this->collSiteJeuxVideoComGames->setModel('\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame');
    }

    /**
     * Gets an array of ChildSiteJeuxVideoComGame objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteJeuxVideoComGame[] List of ChildSiteJeuxVideoComGame objects
     * @throws PropelException
     */
    public function getSiteJeuxVideoComGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteJeuxVideoComGamesPartial && !$this->isNew();
        if (null === $this->collSiteJeuxVideoComGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSiteJeuxVideoComGames) {
                // return empty collection
                $this->initSiteJeuxVideoComGames();
            } else {
                $collSiteJeuxVideoComGames = ChildSiteJeuxVideoComGameQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteJeuxVideoComGamesPartial && count($collSiteJeuxVideoComGames)) {
                        $this->initSiteJeuxVideoComGames(false);

                        foreach ($collSiteJeuxVideoComGames as $obj) {
                            if (false == $this->collSiteJeuxVideoComGames->contains($obj)) {
                                $this->collSiteJeuxVideoComGames->append($obj);
                            }
                        }

                        $this->collSiteJeuxVideoComGamesPartial = true;
                    }

                    return $collSiteJeuxVideoComGames;
                }

                if ($partial && $this->collSiteJeuxVideoComGames) {
                    foreach ($this->collSiteJeuxVideoComGames as $obj) {
                        if ($obj->isNew()) {
                            $collSiteJeuxVideoComGames[] = $obj;
                        }
                    }
                }

                $this->collSiteJeuxVideoComGames = $collSiteJeuxVideoComGames;
                $this->collSiteJeuxVideoComGamesPartial = false;
            }
        }

        return $this->collSiteJeuxVideoComGames;
    }

    /**
     * Sets a collection of ChildSiteJeuxVideoComGame objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteJeuxVideoComGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setSiteJeuxVideoComGames(Collection $siteJeuxVideoComGames, ConnectionInterface $con = null)
    {
        /** @var ChildSiteJeuxVideoComGame[] $siteJeuxVideoComGamesToDelete */
        $siteJeuxVideoComGamesToDelete = $this->getSiteJeuxVideoComGames(new Criteria(), $con)->diff($siteJeuxVideoComGames);


        $this->siteJeuxVideoComGamesScheduledForDeletion = $siteJeuxVideoComGamesToDelete;

        foreach ($siteJeuxVideoComGamesToDelete as $siteJeuxVideoComGameRemoved) {
            $siteJeuxVideoComGameRemoved->setGame(null);
        }

        $this->collSiteJeuxVideoComGames = null;
        foreach ($siteJeuxVideoComGames as $siteJeuxVideoComGame) {
            $this->addSiteJeuxVideoComGame($siteJeuxVideoComGame);
        }

        $this->collSiteJeuxVideoComGames = $siteJeuxVideoComGames;
        $this->collSiteJeuxVideoComGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteJeuxVideoComGame objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteJeuxVideoComGame objects.
     * @throws PropelException
     */
    public function countSiteJeuxVideoComGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteJeuxVideoComGamesPartial && !$this->isNew();
        if (null === $this->collSiteJeuxVideoComGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteJeuxVideoComGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteJeuxVideoComGames());
            }

            $query = ChildSiteJeuxVideoComGameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collSiteJeuxVideoComGames);
    }

    /**
     * Method called to associate a ChildSiteJeuxVideoComGame object to this object
     * through the ChildSiteJeuxVideoComGame foreign key attribute.
     *
     * @param  ChildSiteJeuxVideoComGame $l ChildSiteJeuxVideoComGame
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addSiteJeuxVideoComGame(ChildSiteJeuxVideoComGame $l)
    {
        if ($this->collSiteJeuxVideoComGames === null) {
            $this->initSiteJeuxVideoComGames();
            $this->collSiteJeuxVideoComGamesPartial = true;
        }

        if (!$this->collSiteJeuxVideoComGames->contains($l)) {
            $this->doAddSiteJeuxVideoComGame($l);

            if ($this->siteJeuxVideoComGamesScheduledForDeletion and $this->siteJeuxVideoComGamesScheduledForDeletion->contains($l)) {
                $this->siteJeuxVideoComGamesScheduledForDeletion->remove($this->siteJeuxVideoComGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteJeuxVideoComGame $siteJeuxVideoComGame The ChildSiteJeuxVideoComGame object to add.
     */
    protected function doAddSiteJeuxVideoComGame(ChildSiteJeuxVideoComGame $siteJeuxVideoComGame)
    {
        $this->collSiteJeuxVideoComGames[]= $siteJeuxVideoComGame;
        $siteJeuxVideoComGame->setGame($this);
    }

    /**
     * @param  ChildSiteJeuxVideoComGame $siteJeuxVideoComGame The ChildSiteJeuxVideoComGame object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeSiteJeuxVideoComGame(ChildSiteJeuxVideoComGame $siteJeuxVideoComGame)
    {
        if ($this->getSiteJeuxVideoComGames()->contains($siteJeuxVideoComGame)) {
            $pos = $this->collSiteJeuxVideoComGames->search($siteJeuxVideoComGame);
            $this->collSiteJeuxVideoComGames->remove($pos);
            if (null === $this->siteJeuxVideoComGamesScheduledForDeletion) {
                $this->siteJeuxVideoComGamesScheduledForDeletion = clone $this->collSiteJeuxVideoComGames;
                $this->siteJeuxVideoComGamesScheduledForDeletion->clear();
            }
            $this->siteJeuxVideoComGamesScheduledForDeletion[]= $siteJeuxVideoComGame;
            $siteJeuxVideoComGame->setGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collSiteGameBlogGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteGameBlogGames()
     */
    public function clearSiteGameBlogGames()
    {
        $this->collSiteGameBlogGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteGameBlogGames collection loaded partially.
     */
    public function resetPartialSiteGameBlogGames($v = true)
    {
        $this->collSiteGameBlogGamesPartial = $v;
    }

    /**
     * Initializes the collSiteGameBlogGames collection.
     *
     * By default this just sets the collSiteGameBlogGames collection to an empty array (like clearcollSiteGameBlogGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteGameBlogGames($overrideExisting = true)
    {
        if (null !== $this->collSiteGameBlogGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteGameBlogGameTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteGameBlogGames = new $collectionClassName;
        $this->collSiteGameBlogGames->setModel('\IiMedias\VideoGamesBundle\Model\SiteGameBlogGame');
    }

    /**
     * Gets an array of ChildSiteGameBlogGame objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteGameBlogGame[] List of ChildSiteGameBlogGame objects
     * @throws PropelException
     */
    public function getSiteGameBlogGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameBlogGamesPartial && !$this->isNew();
        if (null === $this->collSiteGameBlogGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSiteGameBlogGames) {
                // return empty collection
                $this->initSiteGameBlogGames();
            } else {
                $collSiteGameBlogGames = ChildSiteGameBlogGameQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteGameBlogGamesPartial && count($collSiteGameBlogGames)) {
                        $this->initSiteGameBlogGames(false);

                        foreach ($collSiteGameBlogGames as $obj) {
                            if (false == $this->collSiteGameBlogGames->contains($obj)) {
                                $this->collSiteGameBlogGames->append($obj);
                            }
                        }

                        $this->collSiteGameBlogGamesPartial = true;
                    }

                    return $collSiteGameBlogGames;
                }

                if ($partial && $this->collSiteGameBlogGames) {
                    foreach ($this->collSiteGameBlogGames as $obj) {
                        if ($obj->isNew()) {
                            $collSiteGameBlogGames[] = $obj;
                        }
                    }
                }

                $this->collSiteGameBlogGames = $collSiteGameBlogGames;
                $this->collSiteGameBlogGamesPartial = false;
            }
        }

        return $this->collSiteGameBlogGames;
    }

    /**
     * Sets a collection of ChildSiteGameBlogGame objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteGameBlogGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setSiteGameBlogGames(Collection $siteGameBlogGames, ConnectionInterface $con = null)
    {
        /** @var ChildSiteGameBlogGame[] $siteGameBlogGamesToDelete */
        $siteGameBlogGamesToDelete = $this->getSiteGameBlogGames(new Criteria(), $con)->diff($siteGameBlogGames);


        $this->siteGameBlogGamesScheduledForDeletion = $siteGameBlogGamesToDelete;

        foreach ($siteGameBlogGamesToDelete as $siteGameBlogGameRemoved) {
            $siteGameBlogGameRemoved->setGame(null);
        }

        $this->collSiteGameBlogGames = null;
        foreach ($siteGameBlogGames as $siteGameBlogGame) {
            $this->addSiteGameBlogGame($siteGameBlogGame);
        }

        $this->collSiteGameBlogGames = $siteGameBlogGames;
        $this->collSiteGameBlogGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteGameBlogGame objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteGameBlogGame objects.
     * @throws PropelException
     */
    public function countSiteGameBlogGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameBlogGamesPartial && !$this->isNew();
        if (null === $this->collSiteGameBlogGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteGameBlogGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteGameBlogGames());
            }

            $query = ChildSiteGameBlogGameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collSiteGameBlogGames);
    }

    /**
     * Method called to associate a ChildSiteGameBlogGame object to this object
     * through the ChildSiteGameBlogGame foreign key attribute.
     *
     * @param  ChildSiteGameBlogGame $l ChildSiteGameBlogGame
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addSiteGameBlogGame(ChildSiteGameBlogGame $l)
    {
        if ($this->collSiteGameBlogGames === null) {
            $this->initSiteGameBlogGames();
            $this->collSiteGameBlogGamesPartial = true;
        }

        if (!$this->collSiteGameBlogGames->contains($l)) {
            $this->doAddSiteGameBlogGame($l);

            if ($this->siteGameBlogGamesScheduledForDeletion and $this->siteGameBlogGamesScheduledForDeletion->contains($l)) {
                $this->siteGameBlogGamesScheduledForDeletion->remove($this->siteGameBlogGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteGameBlogGame $siteGameBlogGame The ChildSiteGameBlogGame object to add.
     */
    protected function doAddSiteGameBlogGame(ChildSiteGameBlogGame $siteGameBlogGame)
    {
        $this->collSiteGameBlogGames[]= $siteGameBlogGame;
        $siteGameBlogGame->setGame($this);
    }

    /**
     * @param  ChildSiteGameBlogGame $siteGameBlogGame The ChildSiteGameBlogGame object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeSiteGameBlogGame(ChildSiteGameBlogGame $siteGameBlogGame)
    {
        if ($this->getSiteGameBlogGames()->contains($siteGameBlogGame)) {
            $pos = $this->collSiteGameBlogGames->search($siteGameBlogGame);
            $this->collSiteGameBlogGames->remove($pos);
            if (null === $this->siteGameBlogGamesScheduledForDeletion) {
                $this->siteGameBlogGamesScheduledForDeletion = clone $this->collSiteGameBlogGames;
                $this->siteGameBlogGamesScheduledForDeletion->clear();
            }
            $this->siteGameBlogGamesScheduledForDeletion[]= $siteGameBlogGame;
            $siteGameBlogGame->setGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collSiteGameKultGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteGameKultGames()
     */
    public function clearSiteGameKultGames()
    {
        $this->collSiteGameKultGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteGameKultGames collection loaded partially.
     */
    public function resetPartialSiteGameKultGames($v = true)
    {
        $this->collSiteGameKultGamesPartial = $v;
    }

    /**
     * Initializes the collSiteGameKultGames collection.
     *
     * By default this just sets the collSiteGameKultGames collection to an empty array (like clearcollSiteGameKultGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteGameKultGames($overrideExisting = true)
    {
        if (null !== $this->collSiteGameKultGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteGameKultGameTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteGameKultGames = new $collectionClassName;
        $this->collSiteGameKultGames->setModel('\IiMedias\VideoGamesBundle\Model\SiteGameKultGame');
    }

    /**
     * Gets an array of ChildSiteGameKultGame objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteGameKultGame[] List of ChildSiteGameKultGame objects
     * @throws PropelException
     */
    public function getSiteGameKultGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameKultGamesPartial && !$this->isNew();
        if (null === $this->collSiteGameKultGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSiteGameKultGames) {
                // return empty collection
                $this->initSiteGameKultGames();
            } else {
                $collSiteGameKultGames = ChildSiteGameKultGameQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteGameKultGamesPartial && count($collSiteGameKultGames)) {
                        $this->initSiteGameKultGames(false);

                        foreach ($collSiteGameKultGames as $obj) {
                            if (false == $this->collSiteGameKultGames->contains($obj)) {
                                $this->collSiteGameKultGames->append($obj);
                            }
                        }

                        $this->collSiteGameKultGamesPartial = true;
                    }

                    return $collSiteGameKultGames;
                }

                if ($partial && $this->collSiteGameKultGames) {
                    foreach ($this->collSiteGameKultGames as $obj) {
                        if ($obj->isNew()) {
                            $collSiteGameKultGames[] = $obj;
                        }
                    }
                }

                $this->collSiteGameKultGames = $collSiteGameKultGames;
                $this->collSiteGameKultGamesPartial = false;
            }
        }

        return $this->collSiteGameKultGames;
    }

    /**
     * Sets a collection of ChildSiteGameKultGame objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteGameKultGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setSiteGameKultGames(Collection $siteGameKultGames, ConnectionInterface $con = null)
    {
        /** @var ChildSiteGameKultGame[] $siteGameKultGamesToDelete */
        $siteGameKultGamesToDelete = $this->getSiteGameKultGames(new Criteria(), $con)->diff($siteGameKultGames);


        $this->siteGameKultGamesScheduledForDeletion = $siteGameKultGamesToDelete;

        foreach ($siteGameKultGamesToDelete as $siteGameKultGameRemoved) {
            $siteGameKultGameRemoved->setGame(null);
        }

        $this->collSiteGameKultGames = null;
        foreach ($siteGameKultGames as $siteGameKultGame) {
            $this->addSiteGameKultGame($siteGameKultGame);
        }

        $this->collSiteGameKultGames = $siteGameKultGames;
        $this->collSiteGameKultGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteGameKultGame objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteGameKultGame objects.
     * @throws PropelException
     */
    public function countSiteGameKultGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteGameKultGamesPartial && !$this->isNew();
        if (null === $this->collSiteGameKultGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteGameKultGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteGameKultGames());
            }

            $query = ChildSiteGameKultGameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collSiteGameKultGames);
    }

    /**
     * Method called to associate a ChildSiteGameKultGame object to this object
     * through the ChildSiteGameKultGame foreign key attribute.
     *
     * @param  ChildSiteGameKultGame $l ChildSiteGameKultGame
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addSiteGameKultGame(ChildSiteGameKultGame $l)
    {
        if ($this->collSiteGameKultGames === null) {
            $this->initSiteGameKultGames();
            $this->collSiteGameKultGamesPartial = true;
        }

        if (!$this->collSiteGameKultGames->contains($l)) {
            $this->doAddSiteGameKultGame($l);

            if ($this->siteGameKultGamesScheduledForDeletion and $this->siteGameKultGamesScheduledForDeletion->contains($l)) {
                $this->siteGameKultGamesScheduledForDeletion->remove($this->siteGameKultGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteGameKultGame $siteGameKultGame The ChildSiteGameKultGame object to add.
     */
    protected function doAddSiteGameKultGame(ChildSiteGameKultGame $siteGameKultGame)
    {
        $this->collSiteGameKultGames[]= $siteGameKultGame;
        $siteGameKultGame->setGame($this);
    }

    /**
     * @param  ChildSiteGameKultGame $siteGameKultGame The ChildSiteGameKultGame object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeSiteGameKultGame(ChildSiteGameKultGame $siteGameKultGame)
    {
        if ($this->getSiteGameKultGames()->contains($siteGameKultGame)) {
            $pos = $this->collSiteGameKultGames->search($siteGameKultGame);
            $this->collSiteGameKultGames->remove($pos);
            if (null === $this->siteGameKultGamesScheduledForDeletion) {
                $this->siteGameKultGamesScheduledForDeletion = clone $this->collSiteGameKultGames;
                $this->siteGameKultGamesScheduledForDeletion->clear();
            }
            $this->siteGameKultGamesScheduledForDeletion[]= $siteGameKultGame;
            $siteGameKultGame->setGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGames()
     */
    public function clearApiGiantBombGames()
    {
        $this->collApiGiantBombGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGames collection loaded partially.
     */
    public function resetPartialApiGiantBombGames($v = true)
    {
        $this->collApiGiantBombGamesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGames collection.
     *
     * By default this just sets the collApiGiantBombGames collection to an empty array (like clearcollApiGiantBombGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGames($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGames = new $collectionClassName;
        $this->collApiGiantBombGames->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame');
    }

    /**
     * Gets an array of ChildApiGiantBombGame objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGame[] List of ChildApiGiantBombGame objects
     * @throws PropelException
     */
    public function getApiGiantBombGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGames) {
                // return empty collection
                $this->initApiGiantBombGames();
            } else {
                $collApiGiantBombGames = ChildApiGiantBombGameQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGamesPartial && count($collApiGiantBombGames)) {
                        $this->initApiGiantBombGames(false);

                        foreach ($collApiGiantBombGames as $obj) {
                            if (false == $this->collApiGiantBombGames->contains($obj)) {
                                $this->collApiGiantBombGames->append($obj);
                            }
                        }

                        $this->collApiGiantBombGamesPartial = true;
                    }

                    return $collApiGiantBombGames;
                }

                if ($partial && $this->collApiGiantBombGames) {
                    foreach ($this->collApiGiantBombGames as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGames[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGames = $collApiGiantBombGames;
                $this->collApiGiantBombGamesPartial = false;
            }
        }

        return $this->collApiGiantBombGames;
    }

    /**
     * Sets a collection of ChildApiGiantBombGame objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setApiGiantBombGames(Collection $apiGiantBombGames, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGame[] $apiGiantBombGamesToDelete */
        $apiGiantBombGamesToDelete = $this->getApiGiantBombGames(new Criteria(), $con)->diff($apiGiantBombGames);


        $this->apiGiantBombGamesScheduledForDeletion = $apiGiantBombGamesToDelete;

        foreach ($apiGiantBombGamesToDelete as $apiGiantBombGameRemoved) {
            $apiGiantBombGameRemoved->setGame(null);
        }

        $this->collApiGiantBombGames = null;
        foreach ($apiGiantBombGames as $apiGiantBombGame) {
            $this->addApiGiantBombGame($apiGiantBombGame);
        }

        $this->collApiGiantBombGames = $apiGiantBombGames;
        $this->collApiGiantBombGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGame objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGame objects.
     * @throws PropelException
     */
    public function countApiGiantBombGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGames());
            }

            $query = ChildApiGiantBombGameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGames);
    }

    /**
     * Method called to associate a ChildApiGiantBombGame object to this object
     * through the ChildApiGiantBombGame foreign key attribute.
     *
     * @param  ChildApiGiantBombGame $l ChildApiGiantBombGame
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addApiGiantBombGame(ChildApiGiantBombGame $l)
    {
        if ($this->collApiGiantBombGames === null) {
            $this->initApiGiantBombGames();
            $this->collApiGiantBombGamesPartial = true;
        }

        if (!$this->collApiGiantBombGames->contains($l)) {
            $this->doAddApiGiantBombGame($l);

            if ($this->apiGiantBombGamesScheduledForDeletion and $this->apiGiantBombGamesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGamesScheduledForDeletion->remove($this->apiGiantBombGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGame $apiGiantBombGame The ChildApiGiantBombGame object to add.
     */
    protected function doAddApiGiantBombGame(ChildApiGiantBombGame $apiGiantBombGame)
    {
        $this->collApiGiantBombGames[]= $apiGiantBombGame;
        $apiGiantBombGame->setGame($this);
    }

    /**
     * @param  ChildApiGiantBombGame $apiGiantBombGame The ChildApiGiantBombGame object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGame(ChildApiGiantBombGame $apiGiantBombGame)
    {
        if ($this->getApiGiantBombGames()->contains($apiGiantBombGame)) {
            $pos = $this->collApiGiantBombGames->search($apiGiantBombGame);
            $this->collApiGiantBombGames->remove($pos);
            if (null === $this->apiGiantBombGamesScheduledForDeletion) {
                $this->apiGiantBombGamesScheduledForDeletion = clone $this->collApiGiantBombGames;
                $this->apiGiantBombGamesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGamesScheduledForDeletion[]= $apiGiantBombGame;
            $apiGiantBombGame->setGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiTwitchGames collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiTwitchGames()
     */
    public function clearApiTwitchGames()
    {
        $this->collApiTwitchGames = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiTwitchGames collection loaded partially.
     */
    public function resetPartialApiTwitchGames($v = true)
    {
        $this->collApiTwitchGamesPartial = $v;
    }

    /**
     * Initializes the collApiTwitchGames collection.
     *
     * By default this just sets the collApiTwitchGames collection to an empty array (like clearcollApiTwitchGames());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiTwitchGames($overrideExisting = true)
    {
        if (null !== $this->collApiTwitchGames && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiTwitchGameTableMap::getTableMap()->getCollectionClassName();

        $this->collApiTwitchGames = new $collectionClassName;
        $this->collApiTwitchGames->setModel('\IiMedias\VideoGamesBundle\Model\ApiTwitchGame');
    }

    /**
     * Gets an array of ChildApiTwitchGame objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiTwitchGame[] List of ChildApiTwitchGame objects
     * @throws PropelException
     */
    public function getApiTwitchGames(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiTwitchGamesPartial && !$this->isNew();
        if (null === $this->collApiTwitchGames || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiTwitchGames) {
                // return empty collection
                $this->initApiTwitchGames();
            } else {
                $collApiTwitchGames = ChildApiTwitchGameQuery::create(null, $criteria)
                    ->filterByGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiTwitchGamesPartial && count($collApiTwitchGames)) {
                        $this->initApiTwitchGames(false);

                        foreach ($collApiTwitchGames as $obj) {
                            if (false == $this->collApiTwitchGames->contains($obj)) {
                                $this->collApiTwitchGames->append($obj);
                            }
                        }

                        $this->collApiTwitchGamesPartial = true;
                    }

                    return $collApiTwitchGames;
                }

                if ($partial && $this->collApiTwitchGames) {
                    foreach ($this->collApiTwitchGames as $obj) {
                        if ($obj->isNew()) {
                            $collApiTwitchGames[] = $obj;
                        }
                    }
                }

                $this->collApiTwitchGames = $collApiTwitchGames;
                $this->collApiTwitchGamesPartial = false;
            }
        }

        return $this->collApiTwitchGames;
    }

    /**
     * Sets a collection of ChildApiTwitchGame objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiTwitchGames A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function setApiTwitchGames(Collection $apiTwitchGames, ConnectionInterface $con = null)
    {
        /** @var ChildApiTwitchGame[] $apiTwitchGamesToDelete */
        $apiTwitchGamesToDelete = $this->getApiTwitchGames(new Criteria(), $con)->diff($apiTwitchGames);


        $this->apiTwitchGamesScheduledForDeletion = $apiTwitchGamesToDelete;

        foreach ($apiTwitchGamesToDelete as $apiTwitchGameRemoved) {
            $apiTwitchGameRemoved->setGame(null);
        }

        $this->collApiTwitchGames = null;
        foreach ($apiTwitchGames as $apiTwitchGame) {
            $this->addApiTwitchGame($apiTwitchGame);
        }

        $this->collApiTwitchGames = $apiTwitchGames;
        $this->collApiTwitchGamesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiTwitchGame objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiTwitchGame objects.
     * @throws PropelException
     */
    public function countApiTwitchGames(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiTwitchGamesPartial && !$this->isNew();
        if (null === $this->collApiTwitchGames || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiTwitchGames) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiTwitchGames());
            }

            $query = ChildApiTwitchGameQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGame($this)
                ->count($con);
        }

        return count($this->collApiTwitchGames);
    }

    /**
     * Method called to associate a ChildApiTwitchGame object to this object
     * through the ChildApiTwitchGame foreign key attribute.
     *
     * @param  ChildApiTwitchGame $l ChildApiTwitchGame
     * @return $this|\IiMedias\VideoGamesBundle\Model\Game The current object (for fluent API support)
     */
    public function addApiTwitchGame(ChildApiTwitchGame $l)
    {
        if ($this->collApiTwitchGames === null) {
            $this->initApiTwitchGames();
            $this->collApiTwitchGamesPartial = true;
        }

        if (!$this->collApiTwitchGames->contains($l)) {
            $this->doAddApiTwitchGame($l);

            if ($this->apiTwitchGamesScheduledForDeletion and $this->apiTwitchGamesScheduledForDeletion->contains($l)) {
                $this->apiTwitchGamesScheduledForDeletion->remove($this->apiTwitchGamesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiTwitchGame $apiTwitchGame The ChildApiTwitchGame object to add.
     */
    protected function doAddApiTwitchGame(ChildApiTwitchGame $apiTwitchGame)
    {
        $this->collApiTwitchGames[]= $apiTwitchGame;
        $apiTwitchGame->setGame($this);
    }

    /**
     * @param  ChildApiTwitchGame $apiTwitchGame The ChildApiTwitchGame object to remove.
     * @return $this|ChildGame The current object (for fluent API support)
     */
    public function removeApiTwitchGame(ChildApiTwitchGame $apiTwitchGame)
    {
        if ($this->getApiTwitchGames()->contains($apiTwitchGame)) {
            $pos = $this->collApiTwitchGames->search($apiTwitchGame);
            $this->collApiTwitchGames->remove($pos);
            if (null === $this->apiTwitchGamesScheduledForDeletion) {
                $this->apiTwitchGamesScheduledForDeletion = clone $this->collApiTwitchGames;
                $this->apiTwitchGamesScheduledForDeletion->clear();
            }
            $this->apiTwitchGamesScheduledForDeletion[]= $apiTwitchGame;
            $apiTwitchGame->setGame(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->vgagam_id = null;
        $this->vgagam_name = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collGameReleases) {
                foreach ($this->collGameReleases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteJeuxVideoComGames) {
                foreach ($this->collSiteJeuxVideoComGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteGameBlogGames) {
                foreach ($this->collSiteGameBlogGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteGameKultGames) {
                foreach ($this->collSiteGameKultGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGames) {
                foreach ($this->collApiGiantBombGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiTwitchGames) {
                foreach ($this->collApiTwitchGames as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collGameReleases = null;
        $this->collSiteJeuxVideoComGames = null;
        $this->collSiteGameBlogGames = null;
        $this->collSiteGameKultGames = null;
        $this->collApiGiantBombGames = null;
        $this->collApiTwitchGames = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string The value of the 'vgagam_name' column
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
