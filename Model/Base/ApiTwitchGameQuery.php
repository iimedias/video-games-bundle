<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\StreamBundle\Model\Channel;
use IiMedias\StreamBundle\Model\GameData;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGame as ChildApiTwitchGame;
use IiMedias\VideoGamesBundle\Model\ApiTwitchGameQuery as ChildApiTwitchGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiTwitchGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_twitch_vgatga' table.
 *
 *
 *
 * @method     ChildApiTwitchGameQuery orderById($order = Criteria::ASC) Order by the vgatga_id column
 * @method     ChildApiTwitchGameQuery orderByGameId($order = Criteria::ASC) Order by the vgatga_vgagam_id column
 * @method     ChildApiTwitchGameQuery orderByName($order = Criteria::ASC) Order by the vgatga_name column
 *
 * @method     ChildApiTwitchGameQuery groupById() Group by the vgatga_id column
 * @method     ChildApiTwitchGameQuery groupByGameId() Group by the vgatga_vgagam_id column
 * @method     ChildApiTwitchGameQuery groupByName() Group by the vgatga_name column
 *
 * @method     ChildApiTwitchGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiTwitchGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiTwitchGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiTwitchGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiTwitchGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiTwitchGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiTwitchGameQuery leftJoinGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the Game relation
 * @method     ChildApiTwitchGameQuery rightJoinGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Game relation
 * @method     ChildApiTwitchGameQuery innerJoinGame($relationAlias = null) Adds a INNER JOIN clause to the query using the Game relation
 *
 * @method     ChildApiTwitchGameQuery joinWithGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Game relation
 *
 * @method     ChildApiTwitchGameQuery leftJoinWithGame() Adds a LEFT JOIN clause and with to the query using the Game relation
 * @method     ChildApiTwitchGameQuery rightJoinWithGame() Adds a RIGHT JOIN clause and with to the query using the Game relation
 * @method     ChildApiTwitchGameQuery innerJoinWithGame() Adds a INNER JOIN clause and with to the query using the Game relation
 *
 * @method     ChildApiTwitchGameQuery leftJoinChannel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Channel relation
 * @method     ChildApiTwitchGameQuery rightJoinChannel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Channel relation
 * @method     ChildApiTwitchGameQuery innerJoinChannel($relationAlias = null) Adds a INNER JOIN clause to the query using the Channel relation
 *
 * @method     ChildApiTwitchGameQuery joinWithChannel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Channel relation
 *
 * @method     ChildApiTwitchGameQuery leftJoinWithChannel() Adds a LEFT JOIN clause and with to the query using the Channel relation
 * @method     ChildApiTwitchGameQuery rightJoinWithChannel() Adds a RIGHT JOIN clause and with to the query using the Channel relation
 * @method     ChildApiTwitchGameQuery innerJoinWithChannel() Adds a INNER JOIN clause and with to the query using the Channel relation
 *
 * @method     ChildApiTwitchGameQuery leftJoinGameData($relationAlias = null) Adds a LEFT JOIN clause to the query using the GameData relation
 * @method     ChildApiTwitchGameQuery rightJoinGameData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GameData relation
 * @method     ChildApiTwitchGameQuery innerJoinGameData($relationAlias = null) Adds a INNER JOIN clause to the query using the GameData relation
 *
 * @method     ChildApiTwitchGameQuery joinWithGameData($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GameData relation
 *
 * @method     ChildApiTwitchGameQuery leftJoinWithGameData() Adds a LEFT JOIN clause and with to the query using the GameData relation
 * @method     ChildApiTwitchGameQuery rightJoinWithGameData() Adds a RIGHT JOIN clause and with to the query using the GameData relation
 * @method     ChildApiTwitchGameQuery innerJoinWithGameData() Adds a INNER JOIN clause and with to the query using the GameData relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameQuery|\IiMedias\StreamBundle\Model\ChannelQuery|\IiMedias\StreamBundle\Model\GameDataQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiTwitchGame findOne(ConnectionInterface $con = null) Return the first ChildApiTwitchGame matching the query
 * @method     ChildApiTwitchGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiTwitchGame matching the query, or a new ChildApiTwitchGame object populated from the query conditions when no match is found
 *
 * @method     ChildApiTwitchGame findOneById(int $vgatga_id) Return the first ChildApiTwitchGame filtered by the vgatga_id column
 * @method     ChildApiTwitchGame findOneByGameId(int $vgatga_vgagam_id) Return the first ChildApiTwitchGame filtered by the vgatga_vgagam_id column
 * @method     ChildApiTwitchGame findOneByName(string $vgatga_name) Return the first ChildApiTwitchGame filtered by the vgatga_name column *

 * @method     ChildApiTwitchGame requirePk($key, ConnectionInterface $con = null) Return the ChildApiTwitchGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiTwitchGame requireOne(ConnectionInterface $con = null) Return the first ChildApiTwitchGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiTwitchGame requireOneById(int $vgatga_id) Return the first ChildApiTwitchGame filtered by the vgatga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiTwitchGame requireOneByGameId(int $vgatga_vgagam_id) Return the first ChildApiTwitchGame filtered by the vgatga_vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiTwitchGame requireOneByName(string $vgatga_name) Return the first ChildApiTwitchGame filtered by the vgatga_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiTwitchGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiTwitchGame objects based on current ModelCriteria
 * @method     ChildApiTwitchGame[]|ObjectCollection findById(int $vgatga_id) Return ChildApiTwitchGame objects filtered by the vgatga_id column
 * @method     ChildApiTwitchGame[]|ObjectCollection findByGameId(int $vgatga_vgagam_id) Return ChildApiTwitchGame objects filtered by the vgatga_vgagam_id column
 * @method     ChildApiTwitchGame[]|ObjectCollection findByName(string $vgatga_name) Return ChildApiTwitchGame objects filtered by the vgatga_name column
 * @method     ChildApiTwitchGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiTwitchGameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiTwitchGameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiTwitchGame', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiTwitchGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiTwitchGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiTwitchGameQuery) {
            return $criteria;
        }
        $query = new ChildApiTwitchGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiTwitchGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiTwitchGameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiTwitchGameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiTwitchGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgatga_id, vgatga_vgagam_id, vgatga_name FROM videogames_api_twitch_vgatga WHERE vgatga_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiTwitchGame $obj */
            $obj = new ChildApiTwitchGame();
            $obj->hydrate($row);
            ApiTwitchGameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiTwitchGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgatga_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgatga_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgatga_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgatga_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgatga_vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGameId(1234); // WHERE vgatga_vgagam_id = 1234
     * $query->filterByGameId(array(12, 34)); // WHERE vgatga_vgagam_id IN (12, 34)
     * $query->filterByGameId(array('min' => 12)); // WHERE vgatga_vgagam_id > 12
     * </code>
     *
     * @see       filterByGame()
     *
     * @param     mixed $gameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByGameId($gameId = null, $comparison = null)
    {
        if (is_array($gameId)) {
            $useMinMax = false;
            if (isset($gameId['min'])) {
                $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_VGAGAM_ID, $gameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gameId['max'])) {
                $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_VGAGAM_ID, $gameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_VGAGAM_ID, $gameId, $comparison);
    }

    /**
     * Filter the query on the vgatga_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgatga_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgatga_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\Game object
     *
     * @param \IiMedias\VideoGamesBundle\Model\Game|ObjectCollection $game The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByGame($game, $comparison = null)
    {
        if ($game instanceof \IiMedias\VideoGamesBundle\Model\Game) {
            return $this
                ->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_VGAGAM_ID, $game->getId(), $comparison);
        } elseif ($game instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_VGAGAM_ID, $game->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\Game or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Game relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function joinGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Game');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Game');
        }

        return $this;
    }

    /**
     * Use the Game relation Game object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameQuery A secondary query class using the current class as primary query
     */
    public function useGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Game', '\IiMedias\VideoGamesBundle\Model\GameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\Channel object
     *
     * @param \IiMedias\StreamBundle\Model\Channel|ObjectCollection $channel the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByChannel($channel, $comparison = null)
    {
        if ($channel instanceof \IiMedias\StreamBundle\Model\Channel) {
            return $this
                ->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $channel->getTwitchGameId(), $comparison);
        } elseif ($channel instanceof ObjectCollection) {
            return $this
                ->useChannelQuery()
                ->filterByPrimaryKeys($channel->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChannel() only accepts arguments of type \IiMedias\StreamBundle\Model\Channel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Channel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function joinChannel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Channel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Channel');
        }

        return $this;
    }

    /**
     * Use the Channel relation Channel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\ChannelQuery A secondary query class using the current class as primary query
     */
    public function useChannelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChannel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Channel', '\IiMedias\StreamBundle\Model\ChannelQuery');
    }

    /**
     * Filter the query by a related \IiMedias\StreamBundle\Model\GameData object
     *
     * @param \IiMedias\StreamBundle\Model\GameData|ObjectCollection $gameData the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function filterByGameData($gameData, $comparison = null)
    {
        if ($gameData instanceof \IiMedias\StreamBundle\Model\GameData) {
            return $this
                ->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $gameData->getTwitchGameId(), $comparison);
        } elseif ($gameData instanceof ObjectCollection) {
            return $this
                ->useGameDataQuery()
                ->filterByPrimaryKeys($gameData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGameData() only accepts arguments of type \IiMedias\StreamBundle\Model\GameData or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GameData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function joinGameData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GameData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GameData');
        }

        return $this;
    }

    /**
     * Use the GameData relation GameData object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\StreamBundle\Model\GameDataQuery A secondary query class using the current class as primary query
     */
    public function useGameDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGameData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GameData', '\IiMedias\StreamBundle\Model\GameDataQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiTwitchGame $apiTwitchGame Object to remove from the list of results
     *
     * @return $this|ChildApiTwitchGameQuery The current query, for fluid interface
     */
    public function prune($apiTwitchGame = null)
    {
        if ($apiTwitchGame) {
            $this->addUsingAlias(ApiTwitchGameTableMap::COL_VGATGA_ID, $apiTwitchGame->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_twitch_vgatga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiTwitchGameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiTwitchGameTableMap::clearInstancePool();
            ApiTwitchGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiTwitchGameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiTwitchGameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiTwitchGameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiTwitchGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiTwitchGameQuery
