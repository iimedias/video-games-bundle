<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGame as ChildSiteGameKultGame;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery as ChildSiteGameKultGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_gamekult_game_vgakga' table.
 *
 *
 *
 * @method     ChildSiteGameKultGameQuery orderById($order = Criteria::ASC) Order by the vgakga_id column
 * @method     ChildSiteGameKultGameQuery orderByTypeId($order = Criteria::ASC) Order by the vgakga_type_id column
 * @method     ChildSiteGameKultGameQuery orderByGamekultId($order = Criteria::ASC) Order by the vgakga_gamekult_id column
 * @method     ChildSiteGameKultGameQuery orderByGameId($order = Criteria::ASC) Order by the vgakga_vgagam_id column
 * @method     ChildSiteGameKultGameQuery orderByName($order = Criteria::ASC) Order by the vgakga_name column
 * @method     ChildSiteGameKultGameQuery orderByAliases($order = Criteria::ASC) Order by the vgakga_aliases column
 * @method     ChildSiteGameKultGameQuery orderByOriginalPrice($order = Criteria::ASC) Order by the vgakga_original_price column
 * @method     ChildSiteGameKultGameQuery orderByPlatformsArray($order = Criteria::ASC) Order by the vgakga_platforms_array column
 * @method     ChildSiteGameKultGameQuery orderByGenresArray($order = Criteria::ASC) Order by the vgakga_genres_array column
 * @method     ChildSiteGameKultGameQuery orderByThemesArray($order = Criteria::ASC) Order by the vgakga_themes_array column
 * @method     ChildSiteGameKultGameQuery orderByCompaniesArray($order = Criteria::ASC) Order by the vgakga_companies_array column
 * @method     ChildSiteGameKultGameQuery orderByFranchisesArray($order = Criteria::ASC) Order by the vgakga_franchises_array column
 * @method     ChildSiteGameKultGameQuery orderByFrReleasedAt($order = Criteria::ASC) Order by the vgakga_fr_released_at column
 * @method     ChildSiteGameKultGameQuery orderByUsReleasedAt($order = Criteria::ASC) Order by the vgakga_us_released_at column
 * @method     ChildSiteGameKultGameQuery orderByJpReleasedAt($order = Criteria::ASC) Order by the vgakga_jp_released_at column
 * @method     ChildSiteGameKultGameQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgakga_image_small_url column
 * @method     ChildSiteGameKultGameQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgakga_site_detail_url column
 * @method     ChildSiteGameKultGameQuery orderByRefreshedAt($order = Criteria::ASC) Order by the vgakga_refreshed_at column
 *
 * @method     ChildSiteGameKultGameQuery groupById() Group by the vgakga_id column
 * @method     ChildSiteGameKultGameQuery groupByTypeId() Group by the vgakga_type_id column
 * @method     ChildSiteGameKultGameQuery groupByGamekultId() Group by the vgakga_gamekult_id column
 * @method     ChildSiteGameKultGameQuery groupByGameId() Group by the vgakga_vgagam_id column
 * @method     ChildSiteGameKultGameQuery groupByName() Group by the vgakga_name column
 * @method     ChildSiteGameKultGameQuery groupByAliases() Group by the vgakga_aliases column
 * @method     ChildSiteGameKultGameQuery groupByOriginalPrice() Group by the vgakga_original_price column
 * @method     ChildSiteGameKultGameQuery groupByPlatformsArray() Group by the vgakga_platforms_array column
 * @method     ChildSiteGameKultGameQuery groupByGenresArray() Group by the vgakga_genres_array column
 * @method     ChildSiteGameKultGameQuery groupByThemesArray() Group by the vgakga_themes_array column
 * @method     ChildSiteGameKultGameQuery groupByCompaniesArray() Group by the vgakga_companies_array column
 * @method     ChildSiteGameKultGameQuery groupByFranchisesArray() Group by the vgakga_franchises_array column
 * @method     ChildSiteGameKultGameQuery groupByFrReleasedAt() Group by the vgakga_fr_released_at column
 * @method     ChildSiteGameKultGameQuery groupByUsReleasedAt() Group by the vgakga_us_released_at column
 * @method     ChildSiteGameKultGameQuery groupByJpReleasedAt() Group by the vgakga_jp_released_at column
 * @method     ChildSiteGameKultGameQuery groupByImageSmallUrl() Group by the vgakga_image_small_url column
 * @method     ChildSiteGameKultGameQuery groupBySiteDetailUrl() Group by the vgakga_site_detail_url column
 * @method     ChildSiteGameKultGameQuery groupByRefreshedAt() Group by the vgakga_refreshed_at column
 *
 * @method     ChildSiteGameKultGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteGameKultGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteGameKultGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteGameKultGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteGameKultGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteGameKultGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteGameKultGameQuery leftJoinGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the Game relation
 * @method     ChildSiteGameKultGameQuery rightJoinGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Game relation
 * @method     ChildSiteGameKultGameQuery innerJoinGame($relationAlias = null) Adds a INNER JOIN clause to the query using the Game relation
 *
 * @method     ChildSiteGameKultGameQuery joinWithGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Game relation
 *
 * @method     ChildSiteGameKultGameQuery leftJoinWithGame() Adds a LEFT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteGameKultGameQuery rightJoinWithGame() Adds a RIGHT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteGameKultGameQuery innerJoinWithGame() Adds a INNER JOIN clause and with to the query using the Game relation
 *
 * @method     ChildSiteGameKultGameQuery leftJoinSiteGameKultGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultGameQuery rightJoinSiteGameKultGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultGameQuery innerJoinSiteGameKultGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameKultGameRelease relation
 *
 * @method     ChildSiteGameKultGameQuery joinWithSiteGameKultGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameKultGameRelease relation
 *
 * @method     ChildSiteGameKultGameQuery leftJoinWithSiteGameKultGameRelease() Adds a LEFT JOIN clause and with to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultGameQuery rightJoinWithSiteGameKultGameRelease() Adds a RIGHT JOIN clause and with to the query using the SiteGameKultGameRelease relation
 * @method     ChildSiteGameKultGameQuery innerJoinWithSiteGameKultGameRelease() Adds a INNER JOIN clause and with to the query using the SiteGameKultGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteGameKultGame findOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultGame matching the query
 * @method     ChildSiteGameKultGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteGameKultGame matching the query, or a new ChildSiteGameKultGame object populated from the query conditions when no match is found
 *
 * @method     ChildSiteGameKultGame findOneById(string $vgakga_id) Return the first ChildSiteGameKultGame filtered by the vgakga_id column
 * @method     ChildSiteGameKultGame findOneByTypeId(string $vgakga_type_id) Return the first ChildSiteGameKultGame filtered by the vgakga_type_id column
 * @method     ChildSiteGameKultGame findOneByGamekultId(string $vgakga_gamekult_id) Return the first ChildSiteGameKultGame filtered by the vgakga_gamekult_id column
 * @method     ChildSiteGameKultGame findOneByGameId(int $vgakga_vgagam_id) Return the first ChildSiteGameKultGame filtered by the vgakga_vgagam_id column
 * @method     ChildSiteGameKultGame findOneByName(string $vgakga_name) Return the first ChildSiteGameKultGame filtered by the vgakga_name column
 * @method     ChildSiteGameKultGame findOneByAliases(array $vgakga_aliases) Return the first ChildSiteGameKultGame filtered by the vgakga_aliases column
 * @method     ChildSiteGameKultGame findOneByOriginalPrice(double $vgakga_original_price) Return the first ChildSiteGameKultGame filtered by the vgakga_original_price column
 * @method     ChildSiteGameKultGame findOneByPlatformsArray(array $vgakga_platforms_array) Return the first ChildSiteGameKultGame filtered by the vgakga_platforms_array column
 * @method     ChildSiteGameKultGame findOneByGenresArray(array $vgakga_genres_array) Return the first ChildSiteGameKultGame filtered by the vgakga_genres_array column
 * @method     ChildSiteGameKultGame findOneByThemesArray(array $vgakga_themes_array) Return the first ChildSiteGameKultGame filtered by the vgakga_themes_array column
 * @method     ChildSiteGameKultGame findOneByCompaniesArray(array $vgakga_companies_array) Return the first ChildSiteGameKultGame filtered by the vgakga_companies_array column
 * @method     ChildSiteGameKultGame findOneByFranchisesArray(array $vgakga_franchises_array) Return the first ChildSiteGameKultGame filtered by the vgakga_franchises_array column
 * @method     ChildSiteGameKultGame findOneByFrReleasedAt(string $vgakga_fr_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_fr_released_at column
 * @method     ChildSiteGameKultGame findOneByUsReleasedAt(string $vgakga_us_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_us_released_at column
 * @method     ChildSiteGameKultGame findOneByJpReleasedAt(string $vgakga_jp_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_jp_released_at column
 * @method     ChildSiteGameKultGame findOneByImageSmallUrl(string $vgakga_image_small_url) Return the first ChildSiteGameKultGame filtered by the vgakga_image_small_url column
 * @method     ChildSiteGameKultGame findOneBySiteDetailUrl(string $vgakga_site_detail_url) Return the first ChildSiteGameKultGame filtered by the vgakga_site_detail_url column
 * @method     ChildSiteGameKultGame findOneByRefreshedAt(string $vgakga_refreshed_at) Return the first ChildSiteGameKultGame filtered by the vgakga_refreshed_at column *

 * @method     ChildSiteGameKultGame requirePk($key, ConnectionInterface $con = null) Return the ChildSiteGameKultGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultGame requireOneById(string $vgakga_id) Return the first ChildSiteGameKultGame filtered by the vgakga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByTypeId(string $vgakga_type_id) Return the first ChildSiteGameKultGame filtered by the vgakga_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByGamekultId(string $vgakga_gamekult_id) Return the first ChildSiteGameKultGame filtered by the vgakga_gamekult_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByGameId(int $vgakga_vgagam_id) Return the first ChildSiteGameKultGame filtered by the vgakga_vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByName(string $vgakga_name) Return the first ChildSiteGameKultGame filtered by the vgakga_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByAliases(array $vgakga_aliases) Return the first ChildSiteGameKultGame filtered by the vgakga_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByOriginalPrice(double $vgakga_original_price) Return the first ChildSiteGameKultGame filtered by the vgakga_original_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByPlatformsArray(array $vgakga_platforms_array) Return the first ChildSiteGameKultGame filtered by the vgakga_platforms_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByGenresArray(array $vgakga_genres_array) Return the first ChildSiteGameKultGame filtered by the vgakga_genres_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByThemesArray(array $vgakga_themes_array) Return the first ChildSiteGameKultGame filtered by the vgakga_themes_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByCompaniesArray(array $vgakga_companies_array) Return the first ChildSiteGameKultGame filtered by the vgakga_companies_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByFranchisesArray(array $vgakga_franchises_array) Return the first ChildSiteGameKultGame filtered by the vgakga_franchises_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByFrReleasedAt(string $vgakga_fr_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_fr_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByUsReleasedAt(string $vgakga_us_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_us_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByJpReleasedAt(string $vgakga_jp_released_at) Return the first ChildSiteGameKultGame filtered by the vgakga_jp_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByImageSmallUrl(string $vgakga_image_small_url) Return the first ChildSiteGameKultGame filtered by the vgakga_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneBySiteDetailUrl(string $vgakga_site_detail_url) Return the first ChildSiteGameKultGame filtered by the vgakga_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGame requireOneByRefreshedAt(string $vgakga_refreshed_at) Return the first ChildSiteGameKultGame filtered by the vgakga_refreshed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteGameKultGame objects based on current ModelCriteria
 * @method     ChildSiteGameKultGame[]|ObjectCollection findById(string $vgakga_id) Return ChildSiteGameKultGame objects filtered by the vgakga_id column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByTypeId(string $vgakga_type_id) Return ChildSiteGameKultGame objects filtered by the vgakga_type_id column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByGamekultId(string $vgakga_gamekult_id) Return ChildSiteGameKultGame objects filtered by the vgakga_gamekult_id column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByGameId(int $vgakga_vgagam_id) Return ChildSiteGameKultGame objects filtered by the vgakga_vgagam_id column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByName(string $vgakga_name) Return ChildSiteGameKultGame objects filtered by the vgakga_name column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByAliases(array $vgakga_aliases) Return ChildSiteGameKultGame objects filtered by the vgakga_aliases column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByOriginalPrice(double $vgakga_original_price) Return ChildSiteGameKultGame objects filtered by the vgakga_original_price column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByPlatformsArray(array $vgakga_platforms_array) Return ChildSiteGameKultGame objects filtered by the vgakga_platforms_array column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByGenresArray(array $vgakga_genres_array) Return ChildSiteGameKultGame objects filtered by the vgakga_genres_array column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByThemesArray(array $vgakga_themes_array) Return ChildSiteGameKultGame objects filtered by the vgakga_themes_array column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByCompaniesArray(array $vgakga_companies_array) Return ChildSiteGameKultGame objects filtered by the vgakga_companies_array column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByFranchisesArray(array $vgakga_franchises_array) Return ChildSiteGameKultGame objects filtered by the vgakga_franchises_array column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByFrReleasedAt(string $vgakga_fr_released_at) Return ChildSiteGameKultGame objects filtered by the vgakga_fr_released_at column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByUsReleasedAt(string $vgakga_us_released_at) Return ChildSiteGameKultGame objects filtered by the vgakga_us_released_at column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByJpReleasedAt(string $vgakga_jp_released_at) Return ChildSiteGameKultGame objects filtered by the vgakga_jp_released_at column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByImageSmallUrl(string $vgakga_image_small_url) Return ChildSiteGameKultGame objects filtered by the vgakga_image_small_url column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findBySiteDetailUrl(string $vgakga_site_detail_url) Return ChildSiteGameKultGame objects filtered by the vgakga_site_detail_url column
 * @method     ChildSiteGameKultGame[]|ObjectCollection findByRefreshedAt(string $vgakga_refreshed_at) Return ChildSiteGameKultGame objects filtered by the vgakga_refreshed_at column
 * @method     ChildSiteGameKultGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteGameKultGameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteGameKultGameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGame', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteGameKultGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteGameKultGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteGameKultGameQuery) {
            return $criteria;
        }
        $query = new ChildSiteGameKultGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteGameKultGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteGameKultGameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgakga_id, vgakga_type_id, vgakga_gamekult_id, vgakga_vgagam_id, vgakga_name, vgakga_aliases, vgakga_original_price, vgakga_platforms_array, vgakga_genres_array, vgakga_themes_array, vgakga_companies_array, vgakga_franchises_array, vgakga_fr_released_at, vgakga_us_released_at, vgakga_jp_released_at, vgakga_image_small_url, vgakga_site_detail_url, vgakga_refreshed_at FROM videogames_site_gamekult_game_vgakga WHERE vgakga_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteGameKultGame $obj */
            $obj = new ChildSiteGameKultGame();
            $obj->hydrate($row);
            SiteGameKultGameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteGameKultGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgakga_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgakga_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgakga_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgakga_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgakga_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId('fooValue');   // WHERE vgakga_type_id = 'fooValue'
     * $query->filterByTypeId('%fooValue%'); // WHERE vgakga_type_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_TYPE_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the vgakga_gamekult_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGamekultId('fooValue');   // WHERE vgakga_gamekult_id = 'fooValue'
     * $query->filterByGamekultId('%fooValue%'); // WHERE vgakga_gamekult_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gamekultId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByGamekultId($gamekultId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gamekultId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_GAMEKULT_ID, $gamekultId, $comparison);
    }

    /**
     * Filter the query on the vgakga_vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGameId(1234); // WHERE vgakga_vgagam_id = 1234
     * $query->filterByGameId(array(12, 34)); // WHERE vgakga_vgagam_id IN (12, 34)
     * $query->filterByGameId(array('min' => 12)); // WHERE vgakga_vgagam_id > 12
     * </code>
     *
     * @see       filterByGame()
     *
     * @param     mixed $gameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByGameId($gameId = null, $comparison = null)
    {
        if (is_array($gameId)) {
            $useMinMax = false;
            if (isset($gameId['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $gameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gameId['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $gameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $gameId, $comparison);
    }

    /**
     * Filter the query on the vgakga_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgakga_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgakga_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgakga_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgakga_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgakga_original_price column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalPrice(1234); // WHERE vgakga_original_price = 1234
     * $query->filterByOriginalPrice(array(12, 34)); // WHERE vgakga_original_price IN (12, 34)
     * $query->filterByOriginalPrice(array('min' => 12)); // WHERE vgakga_original_price > 12
     * </code>
     *
     * @param     mixed $originalPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByOriginalPrice($originalPrice = null, $comparison = null)
    {
        if (is_array($originalPrice)) {
            $useMinMax = false;
            if (isset($originalPrice['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE, $originalPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originalPrice['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE, $originalPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ORIGINAL_PRICE, $originalPrice, $comparison);
    }

    /**
     * Filter the query on the vgakga_platforms_array column
     *
     * @param     array $platformsArray The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByPlatformsArray($platformsArray = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($platformsArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($platformsArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($platformsArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_PLATFORMS_ARRAY, $platformsArray, $comparison);
    }

    /**
     * Filter the query on the vgakga_genres_array column
     *
     * @param     array $genresArray The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByGenresArray($genresArray = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($genresArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($genresArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($genresArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_GENRES_ARRAY, $genresArray, $comparison);
    }

    /**
     * Filter the query on the vgakga_themes_array column
     *
     * @param     array $themesArray The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByThemesArray($themesArray = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($themesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($themesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($themesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_THEMES_ARRAY, $themesArray, $comparison);
    }

    /**
     * Filter the query on the vgakga_companies_array column
     *
     * @param     array $companiesArray The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByCompaniesArray($companiesArray = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($companiesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($companiesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($companiesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_COMPANIES_ARRAY, $companiesArray, $comparison);
    }

    /**
     * Filter the query on the vgakga_franchises_array column
     *
     * @param     array $franchisesArray The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByFranchisesArray($franchisesArray = null, $comparison = null)
    {
        $key = $this->getAliasedColName(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($franchisesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($franchisesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($franchisesArray as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_FRANCHISES_ARRAY, $franchisesArray, $comparison);
    }

    /**
     * Filter the query on the vgakga_fr_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByFrReleasedAt('2011-03-14'); // WHERE vgakga_fr_released_at = '2011-03-14'
     * $query->filterByFrReleasedAt('now'); // WHERE vgakga_fr_released_at = '2011-03-14'
     * $query->filterByFrReleasedAt(array('max' => 'yesterday')); // WHERE vgakga_fr_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $frReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByFrReleasedAt($frReleasedAt = null, $comparison = null)
    {
        if (is_array($frReleasedAt)) {
            $useMinMax = false;
            if (isset($frReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT, $frReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($frReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT, $frReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_FR_RELEASED_AT, $frReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakga_us_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUsReleasedAt('2011-03-14'); // WHERE vgakga_us_released_at = '2011-03-14'
     * $query->filterByUsReleasedAt('now'); // WHERE vgakga_us_released_at = '2011-03-14'
     * $query->filterByUsReleasedAt(array('max' => 'yesterday')); // WHERE vgakga_us_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $usReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByUsReleasedAt($usReleasedAt = null, $comparison = null)
    {
        if (is_array($usReleasedAt)) {
            $useMinMax = false;
            if (isset($usReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT, $usReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT, $usReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_US_RELEASED_AT, $usReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakga_jp_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByJpReleasedAt('2011-03-14'); // WHERE vgakga_jp_released_at = '2011-03-14'
     * $query->filterByJpReleasedAt('now'); // WHERE vgakga_jp_released_at = '2011-03-14'
     * $query->filterByJpReleasedAt(array('max' => 'yesterday')); // WHERE vgakga_jp_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $jpReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByJpReleasedAt($jpReleasedAt = null, $comparison = null)
    {
        if (is_array($jpReleasedAt)) {
            $useMinMax = false;
            if (isset($jpReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT, $jpReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jpReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT, $jpReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_JP_RELEASED_AT, $jpReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakga_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgakga_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgakga_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgakga_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgakga_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgakga_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgakga_refreshed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRefreshedAt('2011-03-14'); // WHERE vgakga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt('now'); // WHERE vgakga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt(array('max' => 'yesterday')); // WHERE vgakga_refreshed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $refreshedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByRefreshedAt($refreshedAt = null, $comparison = null)
    {
        if (is_array($refreshedAt)) {
            $useMinMax = false;
            if (isset($refreshedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refreshedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_REFRESHED_AT, $refreshedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\Game object
     *
     * @param \IiMedias\VideoGamesBundle\Model\Game|ObjectCollection $game The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterByGame($game, $comparison = null)
    {
        if ($game instanceof \IiMedias\VideoGamesBundle\Model\Game) {
            return $this
                ->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $game->getId(), $comparison);
        } elseif ($game instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_VGAGAM_ID, $game->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\Game or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Game relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function joinGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Game');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Game');
        }

        return $this;
    }

    /**
     * Use the Game relation Game object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameQuery A secondary query class using the current class as primary query
     */
    public function useGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Game', '\IiMedias\VideoGamesBundle\Model\GameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease|ObjectCollection $siteGameKultGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultGameRelease($siteGameKultGameRelease, $comparison = null)
    {
        if ($siteGameKultGameRelease instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease) {
            return $this
                ->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $siteGameKultGameRelease->getSiteGameKultGameId(), $comparison);
        } elseif ($siteGameKultGameRelease instanceof ObjectCollection) {
            return $this
                ->useSiteGameKultGameReleaseQuery()
                ->filterByPrimaryKeys($siteGameKultGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteGameKultGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameKultGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function joinSiteGameKultGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameKultGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameKultGameRelease');
        }

        return $this;
    }

    /**
     * Use the SiteGameKultGameRelease relation SiteGameKultGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameKultGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameKultGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameKultGameRelease', '\IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteGameKultGame $siteGameKultGame Object to remove from the list of results
     *
     * @return $this|ChildSiteGameKultGameQuery The current query, for fluid interface
     */
    public function prune($siteGameKultGame = null)
    {
        if ($siteGameKultGame) {
            $this->addUsingAlias(SiteGameKultGameTableMap::COL_VGAKGA_ID, $siteGameKultGame->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_gamekult_game_vgakga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteGameKultGameTableMap::clearInstancePool();
            SiteGameKultGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteGameKultGameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteGameKultGameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteGameKultGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteGameKultGameQuery
