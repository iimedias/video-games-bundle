<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany as ChildApiGiantBombCompany;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery as ChildApiGiantBombCompanyQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper as ChildApiGiantBombGameDeveloper;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery as ChildApiGiantBombGameDeveloperQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher as ChildApiGiantBombGamePublisher;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery as ChildApiGiantBombGamePublisherQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper as ChildApiGiantBombGameReleaseDeveloper;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery as ChildApiGiantBombGameReleaseDeveloperQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher as ChildApiGiantBombGameReleasePublisher;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery as ChildApiGiantBombGameReleasePublisherQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform as ChildApiGiantBombPlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery as ChildApiGiantBombPlatformQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombCompanyTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameDeveloperTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGamePublisherTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseDeveloperTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleasePublisherTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPlatformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_company_vgagco' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombCompany implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombCompanyTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagco_id field.
     *
     * @var        int
     */
    protected $vgagco_id;

    /**
     * The value for the vgagco_name field.
     *
     * @var        string
     */
    protected $vgagco_name;

    /**
     * The value for the vgagco_abbr field.
     *
     * @var        string
     */
    protected $vgagco_abbr;

    /**
     * The value for the vgagco_aliases field.
     *
     * @var        array
     */
    protected $vgagco_aliases;

    /**
     * The unserialized $vgagco_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagco_aliases_unserialized;

    /**
     * The value for the vgagco_summary field.
     *
     * @var        string
     */
    protected $vgagco_summary;

    /**
     * The value for the vgagco_description field.
     *
     * @var        string
     */
    protected $vgagco_description;

    /**
     * The value for the vgagco_location_address field.
     *
     * @var        string
     */
    protected $vgagco_location_address;

    /**
     * The value for the vgagco_location_city field.
     *
     * @var        string
     */
    protected $vgagco_location_city;

    /**
     * The value for the vgagco_location_country field.
     *
     * @var        string
     */
    protected $vgagco_location_country;

    /**
     * The value for the vgagco_location_state field.
     *
     * @var        string
     */
    protected $vgagco_location_state;

    /**
     * The value for the vgagco_phone field.
     *
     * @var        string
     */
    protected $vgagco_phone;

    /**
     * The value for the vgagco_website field.
     *
     * @var        string
     */
    protected $vgagco_website;

    /**
     * The value for the vgagco_founded_at field.
     *
     * @var        DateTime
     */
    protected $vgagco_founded_at;

    /**
     * The value for the vgagco_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagco_api_detail_url;

    /**
     * The value for the vgagco_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagco_site_detail_url;

    /**
     * The value for the vgagco_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagco_image_icon_url;

    /**
     * The value for the vgagco_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagco_image_medium_url;

    /**
     * The value for the vgagco_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagco_image_screen_url;

    /**
     * The value for the vgagco_image_small_url field.
     *
     * @var        string
     */
    protected $vgagco_image_small_url;

    /**
     * The value for the vgagco_image_super_url field.
     *
     * @var        string
     */
    protected $vgagco_image_super_url;

    /**
     * The value for the vgagco_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagco_image_thumb_url;

    /**
     * The value for the vgagco_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagco_image_tiny_url;

    /**
     * The value for the vgagco_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagco_created_at;

    /**
     * The value for the vgagco_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagco_updated_at;

    /**
     * @var        ObjectCollection|ChildApiGiantBombPlatform[] Collection to store aggregation of ChildApiGiantBombPlatform objects.
     */
    protected $collApiGiantBombPlatforms;
    protected $collApiGiantBombPlatformsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameDeveloper[] Collection to store aggregation of ChildApiGiantBombGameDeveloper objects.
     */
    protected $collApiGiantBombGameDevelopers;
    protected $collApiGiantBombGameDevelopersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGamePublisher[] Collection to store aggregation of ChildApiGiantBombGamePublisher objects.
     */
    protected $collApiGiantBombGamePublishers;
    protected $collApiGiantBombGamePublishersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] Collection to store aggregation of ChildApiGiantBombGameReleaseDeveloper objects.
     */
    protected $collApiGiantBombGameReleaseDevelopers;
    protected $collApiGiantBombGameReleaseDevelopersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameReleasePublisher[] Collection to store aggregation of ChildApiGiantBombGameReleasePublisher objects.
     */
    protected $collApiGiantBombGameReleasePublishers;
    protected $collApiGiantBombGameReleasePublishersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombPlatform[]
     */
    protected $apiGiantBombPlatformsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameDeveloper[]
     */
    protected $apiGiantBombGameDevelopersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGamePublisher[]
     */
    protected $apiGiantBombGamePublishersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[]
     */
    protected $apiGiantBombGameReleaseDevelopersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameReleasePublisher[]
     */
    protected $apiGiantBombGameReleasePublishersScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombCompany object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombCompany</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombCompany</code>, delegates to
     * <code>equals(ApiGiantBombCompany)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombCompany The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagco_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagco_id;
    }

    /**
     * Get the [vgagco_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagco_name;
    }

    /**
     * Get the [vgagco_abbr] column value.
     *
     * @return string
     */
    public function getAbbr()
    {
        return $this->vgagco_abbr;
    }

    /**
     * Get the [vgagco_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagco_aliases_unserialized) {
            $this->vgagco_aliases_unserialized = array();
        }
        if (!$this->vgagco_aliases_unserialized && null !== $this->vgagco_aliases) {
            $vgagco_aliases_unserialized = substr($this->vgagco_aliases, 2, -2);
            $this->vgagco_aliases_unserialized = $vgagco_aliases_unserialized ? explode(' | ', $vgagco_aliases_unserialized) : array();
        }

        return $this->vgagco_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagco_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagco_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagco_summary;
    }

    /**
     * Get the [vgagco_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagco_description;
    }

    /**
     * Get the [vgagco_location_address] column value.
     *
     * @return string
     */
    public function getLocationAddress()
    {
        return $this->vgagco_location_address;
    }

    /**
     * Get the [vgagco_location_city] column value.
     *
     * @return string
     */
    public function getLocationCity()
    {
        return $this->vgagco_location_city;
    }

    /**
     * Get the [vgagco_location_country] column value.
     *
     * @return string
     */
    public function getLocationCountry()
    {
        return $this->vgagco_location_country;
    }

    /**
     * Get the [vgagco_location_state] column value.
     *
     * @return string
     */
    public function getLocationState()
    {
        return $this->vgagco_location_state;
    }

    /**
     * Get the [vgagco_phone] column value.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->vgagco_phone;
    }

    /**
     * Get the [vgagco_website] column value.
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->vgagco_website;
    }

    /**
     * Get the [optionally formatted] temporal [vgagco_founded_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFoundedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagco_founded_at;
        } else {
            return $this->vgagco_founded_at instanceof \DateTimeInterface ? $this->vgagco_founded_at->format($format) : null;
        }
    }

    /**
     * Get the [vgagco_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagco_api_detail_url;
    }

    /**
     * Get the [vgagco_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagco_site_detail_url;
    }

    /**
     * Get the [vgagco_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagco_image_icon_url;
    }

    /**
     * Get the [vgagco_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagco_image_medium_url;
    }

    /**
     * Get the [vgagco_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagco_image_screen_url;
    }

    /**
     * Get the [vgagco_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagco_image_small_url;
    }

    /**
     * Get the [vgagco_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagco_image_super_url;
    }

    /**
     * Get the [vgagco_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagco_image_thumb_url;
    }

    /**
     * Get the [vgagco_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagco_image_tiny_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgagco_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagco_created_at;
        } else {
            return $this->vgagco_created_at instanceof \DateTimeInterface ? $this->vgagco_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagco_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagco_updated_at;
        } else {
            return $this->vgagco_updated_at instanceof \DateTimeInterface ? $this->vgagco_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagco_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagco_id !== $v) {
            $this->vgagco_id = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagco_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_name !== $v) {
            $this->vgagco_name = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagco_abbr] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setAbbr($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_abbr !== $v) {
            $this->vgagco_abbr = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR] = true;
        }

        return $this;
    } // setAbbr()

    /**
     * Set the value of [vgagco_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagco_aliases_unserialized !== $v) {
            $this->vgagco_aliases_unserialized = $v;
            $this->vgagco_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagco_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagco_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagco_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_summary !== $v) {
            $this->vgagco_summary = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagco_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_description !== $v) {
            $this->vgagco_description = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagco_location_address] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setLocationAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_location_address !== $v) {
            $this->vgagco_location_address = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS] = true;
        }

        return $this;
    } // setLocationAddress()

    /**
     * Set the value of [vgagco_location_city] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setLocationCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_location_city !== $v) {
            $this->vgagco_location_city = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY] = true;
        }

        return $this;
    } // setLocationCity()

    /**
     * Set the value of [vgagco_location_country] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setLocationCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_location_country !== $v) {
            $this->vgagco_location_country = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY] = true;
        }

        return $this;
    } // setLocationCountry()

    /**
     * Set the value of [vgagco_location_state] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setLocationState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_location_state !== $v) {
            $this->vgagco_location_state = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE] = true;
        }

        return $this;
    } // setLocationState()

    /**
     * Set the value of [vgagco_phone] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_phone !== $v) {
            $this->vgagco_phone = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [vgagco_website] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_website !== $v) {
            $this->vgagco_website = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE] = true;
        }

        return $this;
    } // setWebsite()

    /**
     * Sets the value of [vgagco_founded_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setFoundedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagco_founded_at !== null || $dt !== null) {
            if ($this->vgagco_founded_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagco_founded_at->format("Y-m-d H:i:s.u")) {
                $this->vgagco_founded_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setFoundedAt()

    /**
     * Set the value of [vgagco_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_api_detail_url !== $v) {
            $this->vgagco_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagco_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_site_detail_url !== $v) {
            $this->vgagco_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagco_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_icon_url !== $v) {
            $this->vgagco_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagco_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_medium_url !== $v) {
            $this->vgagco_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagco_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_screen_url !== $v) {
            $this->vgagco_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagco_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_small_url !== $v) {
            $this->vgagco_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagco_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_super_url !== $v) {
            $this->vgagco_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagco_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_thumb_url !== $v) {
            $this->vgagco_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagco_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagco_image_tiny_url !== $v) {
            $this->vgagco_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Sets the value of [vgagco_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagco_created_at !== null || $dt !== null) {
            if ($this->vgagco_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagco_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagco_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagco_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagco_updated_at !== null || $dt !== null) {
            if ($this->vgagco_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagco_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagco_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Abbr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_abbr = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_aliases = $col;
            $this->vgagco_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('LocationAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_location_address = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('LocationCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_location_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('LocationCountry', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_location_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('LocationState', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_location_state = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('Website', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('FoundedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagco_founded_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagco_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagco_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ApiGiantBombCompanyTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagco_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 24; // 24 = ApiGiantBombCompanyTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCompany'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombCompanyQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collApiGiantBombPlatforms = null;

            $this->collApiGiantBombGameDevelopers = null;

            $this->collApiGiantBombGamePublishers = null;

            $this->collApiGiantBombGameReleaseDevelopers = null;

            $this->collApiGiantBombGameReleasePublishers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombCompany::setDeleted()
     * @see ApiGiantBombCompany::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombCompanyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCompanyTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombCompanyTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombPlatformsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombPlatformsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombPlatformsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombPlatformsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombPlatforms !== null) {
                foreach ($this->collApiGiantBombPlatforms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameDevelopersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameDevelopersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameDevelopersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameDevelopersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameDevelopers !== null) {
                foreach ($this->collApiGiantBombGameDevelopers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGamePublishersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGamePublishersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGamePublishersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGamePublishersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGamePublishers !== null) {
                foreach ($this->collApiGiantBombGamePublishers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleaseDevelopers !== null) {
                foreach ($this->collApiGiantBombGameReleaseDevelopers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleasePublishersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleasePublishersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleasePublishersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleasePublishersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleasePublishers !== null) {
                foreach ($this->collApiGiantBombGameReleasePublishers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_id';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_name';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_abbr';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_summary';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_description';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_location_address';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_location_city';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_location_country';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_location_state';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_phone';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_website';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_founded_at';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagco_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_company_vgagco (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagco_id':
                        $stmt->bindValue($identifier, $this->vgagco_id, PDO::PARAM_INT);
                        break;
                    case 'vgagco_name':
                        $stmt->bindValue($identifier, $this->vgagco_name, PDO::PARAM_STR);
                        break;
                    case 'vgagco_abbr':
                        $stmt->bindValue($identifier, $this->vgagco_abbr, PDO::PARAM_STR);
                        break;
                    case 'vgagco_aliases':
                        $stmt->bindValue($identifier, $this->vgagco_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagco_summary':
                        $stmt->bindValue($identifier, $this->vgagco_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagco_description':
                        $stmt->bindValue($identifier, $this->vgagco_description, PDO::PARAM_STR);
                        break;
                    case 'vgagco_location_address':
                        $stmt->bindValue($identifier, $this->vgagco_location_address, PDO::PARAM_STR);
                        break;
                    case 'vgagco_location_city':
                        $stmt->bindValue($identifier, $this->vgagco_location_city, PDO::PARAM_STR);
                        break;
                    case 'vgagco_location_country':
                        $stmt->bindValue($identifier, $this->vgagco_location_country, PDO::PARAM_STR);
                        break;
                    case 'vgagco_location_state':
                        $stmt->bindValue($identifier, $this->vgagco_location_state, PDO::PARAM_STR);
                        break;
                    case 'vgagco_phone':
                        $stmt->bindValue($identifier, $this->vgagco_phone, PDO::PARAM_STR);
                        break;
                    case 'vgagco_website':
                        $stmt->bindValue($identifier, $this->vgagco_website, PDO::PARAM_STR);
                        break;
                    case 'vgagco_founded_at':
                        $stmt->bindValue($identifier, $this->vgagco_founded_at ? $this->vgagco_founded_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagco_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagco_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagco_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagco_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagco_created_at':
                        $stmt->bindValue($identifier, $this->vgagco_created_at ? $this->vgagco_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagco_updated_at':
                        $stmt->bindValue($identifier, $this->vgagco_updated_at ? $this->vgagco_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombCompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getAbbr();
                break;
            case 3:
                return $this->getAliases();
                break;
            case 4:
                return $this->getSummary();
                break;
            case 5:
                return $this->getDescription();
                break;
            case 6:
                return $this->getLocationAddress();
                break;
            case 7:
                return $this->getLocationCity();
                break;
            case 8:
                return $this->getLocationCountry();
                break;
            case 9:
                return $this->getLocationState();
                break;
            case 10:
                return $this->getPhone();
                break;
            case 11:
                return $this->getWebsite();
                break;
            case 12:
                return $this->getFoundedAt();
                break;
            case 13:
                return $this->getApiDetailUrl();
                break;
            case 14:
                return $this->getSiteDetailUrl();
                break;
            case 15:
                return $this->getImageIconUrl();
                break;
            case 16:
                return $this->getImageMediumUrl();
                break;
            case 17:
                return $this->getImageScreenUrl();
                break;
            case 18:
                return $this->getImageSmallUrl();
                break;
            case 19:
                return $this->getImageSuperUrl();
                break;
            case 20:
                return $this->getImageThumbUrl();
                break;
            case 21:
                return $this->getImageTinyUrl();
                break;
            case 22:
                return $this->getCreatedAt();
                break;
            case 23:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombCompany'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombCompany'][$this->hashCode()] = true;
        $keys = ApiGiantBombCompanyTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getAbbr(),
            $keys[3] => $this->getAliases(),
            $keys[4] => $this->getSummary(),
            $keys[5] => $this->getDescription(),
            $keys[6] => $this->getLocationAddress(),
            $keys[7] => $this->getLocationCity(),
            $keys[8] => $this->getLocationCountry(),
            $keys[9] => $this->getLocationState(),
            $keys[10] => $this->getPhone(),
            $keys[11] => $this->getWebsite(),
            $keys[12] => $this->getFoundedAt(),
            $keys[13] => $this->getApiDetailUrl(),
            $keys[14] => $this->getSiteDetailUrl(),
            $keys[15] => $this->getImageIconUrl(),
            $keys[16] => $this->getImageMediumUrl(),
            $keys[17] => $this->getImageScreenUrl(),
            $keys[18] => $this->getImageSmallUrl(),
            $keys[19] => $this->getImageSuperUrl(),
            $keys[20] => $this->getImageThumbUrl(),
            $keys[21] => $this->getImageTinyUrl(),
            $keys[22] => $this->getCreatedAt(),
            $keys[23] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[22]] instanceof \DateTime) {
            $result[$keys[22]] = $result[$keys[22]]->format('c');
        }

        if ($result[$keys[23]] instanceof \DateTime) {
            $result[$keys[23]] = $result[$keys[23]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collApiGiantBombPlatforms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombPlatforms';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_platform_vgagpls';
                        break;
                    default:
                        $key = 'ApiGiantBombPlatforms';
                }

                $result[$key] = $this->collApiGiantBombPlatforms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameDevelopers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameDevelopers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_developer_vgaggds';
                        break;
                    default:
                        $key = 'ApiGiantBombGameDevelopers';
                }

                $result[$key] = $this->collApiGiantBombGameDevelopers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGamePublishers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGamePublishers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_publisher_vgaggus';
                        break;
                    default:
                        $key = 'ApiGiantBombGamePublishers';
                }

                $result[$key] = $this->collApiGiantBombGamePublishers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleaseDevelopers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleaseDevelopers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_release_publisher_vgagrds';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleaseDevelopers';
                }

                $result[$key] = $this->collApiGiantBombGameReleaseDevelopers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleasePublishers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleasePublishers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_release_publisher_vgagrps';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleasePublishers';
                }

                $result[$key] = $this->collApiGiantBombGameReleasePublishers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombCompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setAbbr($value);
                break;
            case 3:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 4:
                $this->setSummary($value);
                break;
            case 5:
                $this->setDescription($value);
                break;
            case 6:
                $this->setLocationAddress($value);
                break;
            case 7:
                $this->setLocationCity($value);
                break;
            case 8:
                $this->setLocationCountry($value);
                break;
            case 9:
                $this->setLocationState($value);
                break;
            case 10:
                $this->setPhone($value);
                break;
            case 11:
                $this->setWebsite($value);
                break;
            case 12:
                $this->setFoundedAt($value);
                break;
            case 13:
                $this->setApiDetailUrl($value);
                break;
            case 14:
                $this->setSiteDetailUrl($value);
                break;
            case 15:
                $this->setImageIconUrl($value);
                break;
            case 16:
                $this->setImageMediumUrl($value);
                break;
            case 17:
                $this->setImageScreenUrl($value);
                break;
            case 18:
                $this->setImageSmallUrl($value);
                break;
            case 19:
                $this->setImageSuperUrl($value);
                break;
            case 20:
                $this->setImageThumbUrl($value);
                break;
            case 21:
                $this->setImageTinyUrl($value);
                break;
            case 22:
                $this->setCreatedAt($value);
                break;
            case 23:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombCompanyTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAbbr($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAliases($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSummary($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDescription($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setLocationAddress($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setLocationCity($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLocationCountry($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLocationState($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPhone($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setWebsite($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setFoundedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setApiDetailUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setImageIconUrl($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setImageMediumUrl($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setImageScreenUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setImageSmallUrl($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setImageSuperUrl($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setImageThumbUrl($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setImageTinyUrl($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setCreatedAt($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setUpdatedAt($arr[$keys[23]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombCompanyTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $this->vgagco_id);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_NAME, $this->vgagco_name);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_ABBR, $this->vgagco_abbr);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_ALIASES, $this->vgagco_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_SUMMARY, $this->vgagco_summary);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_DESCRIPTION, $this->vgagco_description);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_ADDRESS, $this->vgagco_location_address);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_CITY, $this->vgagco_location_city);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_COUNTRY, $this->vgagco_location_country);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_LOCATION_STATE, $this->vgagco_location_state);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_PHONE, $this->vgagco_phone);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_WEBSITE, $this->vgagco_website);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_FOUNDED_AT, $this->vgagco_founded_at);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_API_DETAIL_URL, $this->vgagco_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_SITE_DETAIL_URL, $this->vgagco_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_ICON_URL, $this->vgagco_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_MEDIUM_URL, $this->vgagco_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SCREEN_URL, $this->vgagco_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SMALL_URL, $this->vgagco_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_SUPER_URL, $this->vgagco_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_THUMB_URL, $this->vgagco_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_IMAGE_TINY_URL, $this->vgagco_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_CREATED_AT, $this->vgagco_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT)) {
            $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_UPDATED_AT, $this->vgagco_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombCompanyQuery::create();
        $criteria->add(ApiGiantBombCompanyTableMap::COL_VGAGCO_ID, $this->vgagco_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagco_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setAbbr($this->getAbbr());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setLocationAddress($this->getLocationAddress());
        $copyObj->setLocationCity($this->getLocationCity());
        $copyObj->setLocationCountry($this->getLocationCountry());
        $copyObj->setLocationState($this->getLocationState());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setWebsite($this->getWebsite());
        $copyObj->setFoundedAt($this->getFoundedAt());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombPlatforms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombPlatform($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameDevelopers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameDeveloper($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGamePublishers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGamePublisher($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleaseDevelopers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameReleaseDeveloper($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleasePublishers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameReleasePublisher($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombPlatform' == $relationName) {
            return $this->initApiGiantBombPlatforms();
        }
        if ('ApiGiantBombGameDeveloper' == $relationName) {
            return $this->initApiGiantBombGameDevelopers();
        }
        if ('ApiGiantBombGamePublisher' == $relationName) {
            return $this->initApiGiantBombGamePublishers();
        }
        if ('ApiGiantBombGameReleaseDeveloper' == $relationName) {
            return $this->initApiGiantBombGameReleaseDevelopers();
        }
        if ('ApiGiantBombGameReleasePublisher' == $relationName) {
            return $this->initApiGiantBombGameReleasePublishers();
        }
    }

    /**
     * Clears out the collApiGiantBombPlatforms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombPlatforms()
     */
    public function clearApiGiantBombPlatforms()
    {
        $this->collApiGiantBombPlatforms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombPlatforms collection loaded partially.
     */
    public function resetPartialApiGiantBombPlatforms($v = true)
    {
        $this->collApiGiantBombPlatformsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombPlatforms collection.
     *
     * By default this just sets the collApiGiantBombPlatforms collection to an empty array (like clearcollApiGiantBombPlatforms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombPlatforms($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombPlatforms && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombPlatformTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombPlatforms = new $collectionClassName;
        $this->collApiGiantBombPlatforms->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform');
    }

    /**
     * Gets an array of ChildApiGiantBombPlatform objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombPlatform[] List of ChildApiGiantBombPlatform objects
     * @throws PropelException
     */
    public function getApiGiantBombPlatforms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombPlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombPlatforms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombPlatforms) {
                // return empty collection
                $this->initApiGiantBombPlatforms();
            } else {
                $collApiGiantBombPlatforms = ChildApiGiantBombPlatformQuery::create(null, $criteria)
                    ->filterByApiGiantBombCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombPlatformsPartial && count($collApiGiantBombPlatforms)) {
                        $this->initApiGiantBombPlatforms(false);

                        foreach ($collApiGiantBombPlatforms as $obj) {
                            if (false == $this->collApiGiantBombPlatforms->contains($obj)) {
                                $this->collApiGiantBombPlatforms->append($obj);
                            }
                        }

                        $this->collApiGiantBombPlatformsPartial = true;
                    }

                    return $collApiGiantBombPlatforms;
                }

                if ($partial && $this->collApiGiantBombPlatforms) {
                    foreach ($this->collApiGiantBombPlatforms as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombPlatforms[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombPlatforms = $collApiGiantBombPlatforms;
                $this->collApiGiantBombPlatformsPartial = false;
            }
        }

        return $this->collApiGiantBombPlatforms;
    }

    /**
     * Sets a collection of ChildApiGiantBombPlatform objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombPlatforms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiGiantBombPlatforms(Collection $apiGiantBombPlatforms, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombPlatform[] $apiGiantBombPlatformsToDelete */
        $apiGiantBombPlatformsToDelete = $this->getApiGiantBombPlatforms(new Criteria(), $con)->diff($apiGiantBombPlatforms);


        $this->apiGiantBombPlatformsScheduledForDeletion = $apiGiantBombPlatformsToDelete;

        foreach ($apiGiantBombPlatformsToDelete as $apiGiantBombPlatformRemoved) {
            $apiGiantBombPlatformRemoved->setApiGiantBombCompany(null);
        }

        $this->collApiGiantBombPlatforms = null;
        foreach ($apiGiantBombPlatforms as $apiGiantBombPlatform) {
            $this->addApiGiantBombPlatform($apiGiantBombPlatform);
        }

        $this->collApiGiantBombPlatforms = $apiGiantBombPlatforms;
        $this->collApiGiantBombPlatformsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombPlatform objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombPlatform objects.
     * @throws PropelException
     */
    public function countApiGiantBombPlatforms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombPlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombPlatforms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombPlatforms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombPlatforms());
            }

            $query = ChildApiGiantBombPlatformQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCompany($this)
                ->count($con);
        }

        return count($this->collApiGiantBombPlatforms);
    }

    /**
     * Method called to associate a ChildApiGiantBombPlatform object to this object
     * through the ChildApiGiantBombPlatform foreign key attribute.
     *
     * @param  ChildApiGiantBombPlatform $l ChildApiGiantBombPlatform
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addApiGiantBombPlatform(ChildApiGiantBombPlatform $l)
    {
        if ($this->collApiGiantBombPlatforms === null) {
            $this->initApiGiantBombPlatforms();
            $this->collApiGiantBombPlatformsPartial = true;
        }

        if (!$this->collApiGiantBombPlatforms->contains($l)) {
            $this->doAddApiGiantBombPlatform($l);

            if ($this->apiGiantBombPlatformsScheduledForDeletion and $this->apiGiantBombPlatformsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombPlatformsScheduledForDeletion->remove($this->apiGiantBombPlatformsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombPlatform $apiGiantBombPlatform The ChildApiGiantBombPlatform object to add.
     */
    protected function doAddApiGiantBombPlatform(ChildApiGiantBombPlatform $apiGiantBombPlatform)
    {
        $this->collApiGiantBombPlatforms[]= $apiGiantBombPlatform;
        $apiGiantBombPlatform->setApiGiantBombCompany($this);
    }

    /**
     * @param  ChildApiGiantBombPlatform $apiGiantBombPlatform The ChildApiGiantBombPlatform object to remove.
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeApiGiantBombPlatform(ChildApiGiantBombPlatform $apiGiantBombPlatform)
    {
        if ($this->getApiGiantBombPlatforms()->contains($apiGiantBombPlatform)) {
            $pos = $this->collApiGiantBombPlatforms->search($apiGiantBombPlatform);
            $this->collApiGiantBombPlatforms->remove($pos);
            if (null === $this->apiGiantBombPlatformsScheduledForDeletion) {
                $this->apiGiantBombPlatformsScheduledForDeletion = clone $this->collApiGiantBombPlatforms;
                $this->apiGiantBombPlatformsScheduledForDeletion->clear();
            }
            $this->apiGiantBombPlatformsScheduledForDeletion[]= $apiGiantBombPlatform;
            $apiGiantBombPlatform->setApiGiantBombCompany(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombGameDevelopers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameDevelopers()
     */
    public function clearApiGiantBombGameDevelopers()
    {
        $this->collApiGiantBombGameDevelopers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameDevelopers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameDevelopers($v = true)
    {
        $this->collApiGiantBombGameDevelopersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameDevelopers collection.
     *
     * By default this just sets the collApiGiantBombGameDevelopers collection to an empty array (like clearcollApiGiantBombGameDevelopers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameDevelopers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameDevelopers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameDeveloperTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameDevelopers = new $collectionClassName;
        $this->collApiGiantBombGameDevelopers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper');
    }

    /**
     * Gets an array of ChildApiGiantBombGameDeveloper objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameDeveloper[] List of ChildApiGiantBombGameDeveloper objects
     * @throws PropelException
     */
    public function getApiGiantBombGameDevelopers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameDevelopers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameDevelopers) {
                // return empty collection
                $this->initApiGiantBombGameDevelopers();
            } else {
                $collApiGiantBombGameDevelopers = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria)
                    ->filterByApiGiantBombCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameDevelopersPartial && count($collApiGiantBombGameDevelopers)) {
                        $this->initApiGiantBombGameDevelopers(false);

                        foreach ($collApiGiantBombGameDevelopers as $obj) {
                            if (false == $this->collApiGiantBombGameDevelopers->contains($obj)) {
                                $this->collApiGiantBombGameDevelopers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameDevelopersPartial = true;
                    }

                    return $collApiGiantBombGameDevelopers;
                }

                if ($partial && $this->collApiGiantBombGameDevelopers) {
                    foreach ($this->collApiGiantBombGameDevelopers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameDevelopers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameDevelopers = $collApiGiantBombGameDevelopers;
                $this->collApiGiantBombGameDevelopersPartial = false;
            }
        }

        return $this->collApiGiantBombGameDevelopers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameDeveloper objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameDevelopers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiGiantBombGameDevelopers(Collection $apiGiantBombGameDevelopers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameDeveloper[] $apiGiantBombGameDevelopersToDelete */
        $apiGiantBombGameDevelopersToDelete = $this->getApiGiantBombGameDevelopers(new Criteria(), $con)->diff($apiGiantBombGameDevelopers);


        $this->apiGiantBombGameDevelopersScheduledForDeletion = $apiGiantBombGameDevelopersToDelete;

        foreach ($apiGiantBombGameDevelopersToDelete as $apiGiantBombGameDeveloperRemoved) {
            $apiGiantBombGameDeveloperRemoved->setApiGiantBombCompany(null);
        }

        $this->collApiGiantBombGameDevelopers = null;
        foreach ($apiGiantBombGameDevelopers as $apiGiantBombGameDeveloper) {
            $this->addApiGiantBombGameDeveloper($apiGiantBombGameDeveloper);
        }

        $this->collApiGiantBombGameDevelopers = $apiGiantBombGameDevelopers;
        $this->collApiGiantBombGameDevelopersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameDeveloper objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameDeveloper objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameDevelopers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameDevelopers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameDevelopers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameDevelopers());
            }

            $query = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCompany($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameDevelopers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameDeveloper object to this object
     * through the ChildApiGiantBombGameDeveloper foreign key attribute.
     *
     * @param  ChildApiGiantBombGameDeveloper $l ChildApiGiantBombGameDeveloper
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $l)
    {
        if ($this->collApiGiantBombGameDevelopers === null) {
            $this->initApiGiantBombGameDevelopers();
            $this->collApiGiantBombGameDevelopersPartial = true;
        }

        if (!$this->collApiGiantBombGameDevelopers->contains($l)) {
            $this->doAddApiGiantBombGameDeveloper($l);

            if ($this->apiGiantBombGameDevelopersScheduledForDeletion and $this->apiGiantBombGameDevelopersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameDevelopersScheduledForDeletion->remove($this->apiGiantBombGameDevelopersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper The ChildApiGiantBombGameDeveloper object to add.
     */
    protected function doAddApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper)
    {
        $this->collApiGiantBombGameDevelopers[]= $apiGiantBombGameDeveloper;
        $apiGiantBombGameDeveloper->setApiGiantBombCompany($this);
    }

    /**
     * @param  ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper The ChildApiGiantBombGameDeveloper object to remove.
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper)
    {
        if ($this->getApiGiantBombGameDevelopers()->contains($apiGiantBombGameDeveloper)) {
            $pos = $this->collApiGiantBombGameDevelopers->search($apiGiantBombGameDeveloper);
            $this->collApiGiantBombGameDevelopers->remove($pos);
            if (null === $this->apiGiantBombGameDevelopersScheduledForDeletion) {
                $this->apiGiantBombGameDevelopersScheduledForDeletion = clone $this->collApiGiantBombGameDevelopers;
                $this->apiGiantBombGameDevelopersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameDevelopersScheduledForDeletion[]= clone $apiGiantBombGameDeveloper;
            $apiGiantBombGameDeveloper->setApiGiantBombCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombCompany is new, it will return
     * an empty collection; or if this ApiGiantBombCompany has previously
     * been saved, it will retrieve related ApiGiantBombGameDevelopers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombCompany.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameDeveloper[] List of ChildApiGiantBombGameDeveloper objects
     */
    public function getApiGiantBombGameDevelopersJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGameDevelopers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGamePublishers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGamePublishers()
     */
    public function clearApiGiantBombGamePublishers()
    {
        $this->collApiGiantBombGamePublishers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGamePublishers collection loaded partially.
     */
    public function resetPartialApiGiantBombGamePublishers($v = true)
    {
        $this->collApiGiantBombGamePublishersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGamePublishers collection.
     *
     * By default this just sets the collApiGiantBombGamePublishers collection to an empty array (like clearcollApiGiantBombGamePublishers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGamePublishers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGamePublishers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGamePublisherTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGamePublishers = new $collectionClassName;
        $this->collApiGiantBombGamePublishers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher');
    }

    /**
     * Gets an array of ChildApiGiantBombGamePublisher objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGamePublisher[] List of ChildApiGiantBombGamePublisher objects
     * @throws PropelException
     */
    public function getApiGiantBombGamePublishers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePublishers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePublishers) {
                // return empty collection
                $this->initApiGiantBombGamePublishers();
            } else {
                $collApiGiantBombGamePublishers = ChildApiGiantBombGamePublisherQuery::create(null, $criteria)
                    ->filterByApiGiantBombCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGamePublishersPartial && count($collApiGiantBombGamePublishers)) {
                        $this->initApiGiantBombGamePublishers(false);

                        foreach ($collApiGiantBombGamePublishers as $obj) {
                            if (false == $this->collApiGiantBombGamePublishers->contains($obj)) {
                                $this->collApiGiantBombGamePublishers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGamePublishersPartial = true;
                    }

                    return $collApiGiantBombGamePublishers;
                }

                if ($partial && $this->collApiGiantBombGamePublishers) {
                    foreach ($this->collApiGiantBombGamePublishers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGamePublishers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGamePublishers = $collApiGiantBombGamePublishers;
                $this->collApiGiantBombGamePublishersPartial = false;
            }
        }

        return $this->collApiGiantBombGamePublishers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGamePublisher objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGamePublishers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiGiantBombGamePublishers(Collection $apiGiantBombGamePublishers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGamePublisher[] $apiGiantBombGamePublishersToDelete */
        $apiGiantBombGamePublishersToDelete = $this->getApiGiantBombGamePublishers(new Criteria(), $con)->diff($apiGiantBombGamePublishers);


        $this->apiGiantBombGamePublishersScheduledForDeletion = $apiGiantBombGamePublishersToDelete;

        foreach ($apiGiantBombGamePublishersToDelete as $apiGiantBombGamePublisherRemoved) {
            $apiGiantBombGamePublisherRemoved->setApiGiantBombCompany(null);
        }

        $this->collApiGiantBombGamePublishers = null;
        foreach ($apiGiantBombGamePublishers as $apiGiantBombGamePublisher) {
            $this->addApiGiantBombGamePublisher($apiGiantBombGamePublisher);
        }

        $this->collApiGiantBombGamePublishers = $apiGiantBombGamePublishers;
        $this->collApiGiantBombGamePublishersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGamePublisher objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGamePublisher objects.
     * @throws PropelException
     */
    public function countApiGiantBombGamePublishers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePublishers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePublishers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGamePublishers());
            }

            $query = ChildApiGiantBombGamePublisherQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCompany($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGamePublishers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGamePublisher object to this object
     * through the ChildApiGiantBombGamePublisher foreign key attribute.
     *
     * @param  ChildApiGiantBombGamePublisher $l ChildApiGiantBombGamePublisher
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $l)
    {
        if ($this->collApiGiantBombGamePublishers === null) {
            $this->initApiGiantBombGamePublishers();
            $this->collApiGiantBombGamePublishersPartial = true;
        }

        if (!$this->collApiGiantBombGamePublishers->contains($l)) {
            $this->doAddApiGiantBombGamePublisher($l);

            if ($this->apiGiantBombGamePublishersScheduledForDeletion and $this->apiGiantBombGamePublishersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGamePublishersScheduledForDeletion->remove($this->apiGiantBombGamePublishersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher The ChildApiGiantBombGamePublisher object to add.
     */
    protected function doAddApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher)
    {
        $this->collApiGiantBombGamePublishers[]= $apiGiantBombGamePublisher;
        $apiGiantBombGamePublisher->setApiGiantBombCompany($this);
    }

    /**
     * @param  ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher The ChildApiGiantBombGamePublisher object to remove.
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher)
    {
        if ($this->getApiGiantBombGamePublishers()->contains($apiGiantBombGamePublisher)) {
            $pos = $this->collApiGiantBombGamePublishers->search($apiGiantBombGamePublisher);
            $this->collApiGiantBombGamePublishers->remove($pos);
            if (null === $this->apiGiantBombGamePublishersScheduledForDeletion) {
                $this->apiGiantBombGamePublishersScheduledForDeletion = clone $this->collApiGiantBombGamePublishers;
                $this->apiGiantBombGamePublishersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGamePublishersScheduledForDeletion[]= clone $apiGiantBombGamePublisher;
            $apiGiantBombGamePublisher->setApiGiantBombCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombCompany is new, it will return
     * an empty collection; or if this ApiGiantBombCompany has previously
     * been saved, it will retrieve related ApiGiantBombGamePublishers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombCompany.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGamePublisher[] List of ChildApiGiantBombGamePublisher objects
     */
    public function getApiGiantBombGamePublishersJoinApiGiantBombGame(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGamePublisherQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGame', $joinBehavior);

        return $this->getApiGiantBombGamePublishers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleaseDevelopers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleaseDevelopers()
     */
    public function clearApiGiantBombGameReleaseDevelopers()
    {
        $this->collApiGiantBombGameReleaseDevelopers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleaseDevelopers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleaseDevelopers($v = true)
    {
        $this->collApiGiantBombGameReleaseDevelopersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleaseDevelopers collection.
     *
     * By default this just sets the collApiGiantBombGameReleaseDevelopers collection to an empty array (like clearcollApiGiantBombGameReleaseDevelopers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleaseDevelopers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleaseDevelopers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleaseDeveloperTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleaseDevelopers = new $collectionClassName;
        $this->collApiGiantBombGameReleaseDevelopers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper');
    }

    /**
     * Gets an array of ChildApiGiantBombGameReleaseDeveloper objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] List of ChildApiGiantBombGameReleaseDeveloper objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleaseDevelopers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleaseDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleaseDevelopers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleaseDevelopers) {
                // return empty collection
                $this->initApiGiantBombGameReleaseDevelopers();
            } else {
                $collApiGiantBombGameReleaseDevelopers = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria)
                    ->filterByApiGiantBombCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleaseDevelopersPartial && count($collApiGiantBombGameReleaseDevelopers)) {
                        $this->initApiGiantBombGameReleaseDevelopers(false);

                        foreach ($collApiGiantBombGameReleaseDevelopers as $obj) {
                            if (false == $this->collApiGiantBombGameReleaseDevelopers->contains($obj)) {
                                $this->collApiGiantBombGameReleaseDevelopers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleaseDevelopersPartial = true;
                    }

                    return $collApiGiantBombGameReleaseDevelopers;
                }

                if ($partial && $this->collApiGiantBombGameReleaseDevelopers) {
                    foreach ($this->collApiGiantBombGameReleaseDevelopers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleaseDevelopers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleaseDevelopers = $collApiGiantBombGameReleaseDevelopers;
                $this->collApiGiantBombGameReleaseDevelopersPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleaseDevelopers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameReleaseDeveloper objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleaseDevelopers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleaseDevelopers(Collection $apiGiantBombGameReleaseDevelopers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameReleaseDeveloper[] $apiGiantBombGameReleaseDevelopersToDelete */
        $apiGiantBombGameReleaseDevelopersToDelete = $this->getApiGiantBombGameReleaseDevelopers(new Criteria(), $con)->diff($apiGiantBombGameReleaseDevelopers);


        $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = $apiGiantBombGameReleaseDevelopersToDelete;

        foreach ($apiGiantBombGameReleaseDevelopersToDelete as $apiGiantBombGameReleaseDeveloperRemoved) {
            $apiGiantBombGameReleaseDeveloperRemoved->setApiGiantBombCompany(null);
        }

        $this->collApiGiantBombGameReleaseDevelopers = null;
        foreach ($apiGiantBombGameReleaseDevelopers as $apiGiantBombGameReleaseDeveloper) {
            $this->addApiGiantBombGameReleaseDeveloper($apiGiantBombGameReleaseDeveloper);
        }

        $this->collApiGiantBombGameReleaseDevelopers = $apiGiantBombGameReleaseDevelopers;
        $this->collApiGiantBombGameReleaseDevelopersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameReleaseDeveloper objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameReleaseDeveloper objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleaseDevelopers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleaseDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleaseDevelopers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleaseDevelopers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleaseDevelopers());
            }

            $query = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCompany($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleaseDevelopers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameReleaseDeveloper object to this object
     * through the ChildApiGiantBombGameReleaseDeveloper foreign key attribute.
     *
     * @param  ChildApiGiantBombGameReleaseDeveloper $l ChildApiGiantBombGameReleaseDeveloper
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $l)
    {
        if ($this->collApiGiantBombGameReleaseDevelopers === null) {
            $this->initApiGiantBombGameReleaseDevelopers();
            $this->collApiGiantBombGameReleaseDevelopersPartial = true;
        }

        if (!$this->collApiGiantBombGameReleaseDevelopers->contains($l)) {
            $this->doAddApiGiantBombGameReleaseDeveloper($l);

            if ($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion and $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->remove($this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper The ChildApiGiantBombGameReleaseDeveloper object to add.
     */
    protected function doAddApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper)
    {
        $this->collApiGiantBombGameReleaseDevelopers[]= $apiGiantBombGameReleaseDeveloper;
        $apiGiantBombGameReleaseDeveloper->setApiGiantBombCompany($this);
    }

    /**
     * @param  ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper The ChildApiGiantBombGameReleaseDeveloper object to remove.
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeApiGiantBombGameReleaseDeveloper(ChildApiGiantBombGameReleaseDeveloper $apiGiantBombGameReleaseDeveloper)
    {
        if ($this->getApiGiantBombGameReleaseDevelopers()->contains($apiGiantBombGameReleaseDeveloper)) {
            $pos = $this->collApiGiantBombGameReleaseDevelopers->search($apiGiantBombGameReleaseDeveloper);
            $this->collApiGiantBombGameReleaseDevelopers->remove($pos);
            if (null === $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion) {
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion = clone $this->collApiGiantBombGameReleaseDevelopers;
                $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleaseDevelopersScheduledForDeletion[]= clone $apiGiantBombGameReleaseDeveloper;
            $apiGiantBombGameReleaseDeveloper->setApiGiantBombCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombCompany is new, it will return
     * an empty collection; or if this ApiGiantBombCompany has previously
     * been saved, it will retrieve related ApiGiantBombGameReleaseDevelopers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombCompany.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameReleaseDeveloper[] List of ChildApiGiantBombGameReleaseDeveloper objects
     */
    public function getApiGiantBombGameReleaseDevelopersJoinApiGiantBombGameRelease(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseDeveloperQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRelease', $joinBehavior);

        return $this->getApiGiantBombGameReleaseDevelopers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleasePublishers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleasePublishers()
     */
    public function clearApiGiantBombGameReleasePublishers()
    {
        $this->collApiGiantBombGameReleasePublishers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleasePublishers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleasePublishers($v = true)
    {
        $this->collApiGiantBombGameReleasePublishersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleasePublishers collection.
     *
     * By default this just sets the collApiGiantBombGameReleasePublishers collection to an empty array (like clearcollApiGiantBombGameReleasePublishers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleasePublishers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleasePublishers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleasePublisherTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleasePublishers = new $collectionClassName;
        $this->collApiGiantBombGameReleasePublishers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher');
    }

    /**
     * Gets an array of ChildApiGiantBombGameReleasePublisher objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameReleasePublisher[] List of ChildApiGiantBombGameReleasePublisher objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleasePublishers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleasePublishers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleasePublishers) {
                // return empty collection
                $this->initApiGiantBombGameReleasePublishers();
            } else {
                $collApiGiantBombGameReleasePublishers = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria)
                    ->filterByApiGiantBombCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleasePublishersPartial && count($collApiGiantBombGameReleasePublishers)) {
                        $this->initApiGiantBombGameReleasePublishers(false);

                        foreach ($collApiGiantBombGameReleasePublishers as $obj) {
                            if (false == $this->collApiGiantBombGameReleasePublishers->contains($obj)) {
                                $this->collApiGiantBombGameReleasePublishers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleasePublishersPartial = true;
                    }

                    return $collApiGiantBombGameReleasePublishers;
                }

                if ($partial && $this->collApiGiantBombGameReleasePublishers) {
                    foreach ($this->collApiGiantBombGameReleasePublishers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleasePublishers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleasePublishers = $collApiGiantBombGameReleasePublishers;
                $this->collApiGiantBombGameReleasePublishersPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleasePublishers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameReleasePublisher objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleasePublishers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleasePublishers(Collection $apiGiantBombGameReleasePublishers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameReleasePublisher[] $apiGiantBombGameReleasePublishersToDelete */
        $apiGiantBombGameReleasePublishersToDelete = $this->getApiGiantBombGameReleasePublishers(new Criteria(), $con)->diff($apiGiantBombGameReleasePublishers);


        $this->apiGiantBombGameReleasePublishersScheduledForDeletion = $apiGiantBombGameReleasePublishersToDelete;

        foreach ($apiGiantBombGameReleasePublishersToDelete as $apiGiantBombGameReleasePublisherRemoved) {
            $apiGiantBombGameReleasePublisherRemoved->setApiGiantBombCompany(null);
        }

        $this->collApiGiantBombGameReleasePublishers = null;
        foreach ($apiGiantBombGameReleasePublishers as $apiGiantBombGameReleasePublisher) {
            $this->addApiGiantBombGameReleasePublisher($apiGiantBombGameReleasePublisher);
        }

        $this->collApiGiantBombGameReleasePublishers = $apiGiantBombGameReleasePublishers;
        $this->collApiGiantBombGameReleasePublishersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameReleasePublisher objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameReleasePublisher objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleasePublishers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleasePublishers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleasePublishers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleasePublishers());
            }

            $query = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCompany($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleasePublishers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameReleasePublisher object to this object
     * through the ChildApiGiantBombGameReleasePublisher foreign key attribute.
     *
     * @param  ChildApiGiantBombGameReleasePublisher $l ChildApiGiantBombGameReleasePublisher
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany The current object (for fluent API support)
     */
    public function addApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $l)
    {
        if ($this->collApiGiantBombGameReleasePublishers === null) {
            $this->initApiGiantBombGameReleasePublishers();
            $this->collApiGiantBombGameReleasePublishersPartial = true;
        }

        if (!$this->collApiGiantBombGameReleasePublishers->contains($l)) {
            $this->doAddApiGiantBombGameReleasePublisher($l);

            if ($this->apiGiantBombGameReleasePublishersScheduledForDeletion and $this->apiGiantBombGameReleasePublishersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion->remove($this->apiGiantBombGameReleasePublishersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher The ChildApiGiantBombGameReleasePublisher object to add.
     */
    protected function doAddApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher)
    {
        $this->collApiGiantBombGameReleasePublishers[]= $apiGiantBombGameReleasePublisher;
        $apiGiantBombGameReleasePublisher->setApiGiantBombCompany($this);
    }

    /**
     * @param  ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher The ChildApiGiantBombGameReleasePublisher object to remove.
     * @return $this|ChildApiGiantBombCompany The current object (for fluent API support)
     */
    public function removeApiGiantBombGameReleasePublisher(ChildApiGiantBombGameReleasePublisher $apiGiantBombGameReleasePublisher)
    {
        if ($this->getApiGiantBombGameReleasePublishers()->contains($apiGiantBombGameReleasePublisher)) {
            $pos = $this->collApiGiantBombGameReleasePublishers->search($apiGiantBombGameReleasePublisher);
            $this->collApiGiantBombGameReleasePublishers->remove($pos);
            if (null === $this->apiGiantBombGameReleasePublishersScheduledForDeletion) {
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion = clone $this->collApiGiantBombGameReleasePublishers;
                $this->apiGiantBombGameReleasePublishersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleasePublishersScheduledForDeletion[]= clone $apiGiantBombGameReleasePublisher;
            $apiGiantBombGameReleasePublisher->setApiGiantBombCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombCompany is new, it will return
     * an empty collection; or if this ApiGiantBombCompany has previously
     * been saved, it will retrieve related ApiGiantBombGameReleasePublishers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombCompany.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameReleasePublisher[] List of ChildApiGiantBombGameReleasePublisher objects
     */
    public function getApiGiantBombGameReleasePublishersJoinApiGiantBombGameRelease(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleasePublisherQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRelease', $joinBehavior);

        return $this->getApiGiantBombGameReleasePublishers($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->vgagco_id = null;
        $this->vgagco_name = null;
        $this->vgagco_abbr = null;
        $this->vgagco_aliases = null;
        $this->vgagco_aliases_unserialized = null;
        $this->vgagco_summary = null;
        $this->vgagco_description = null;
        $this->vgagco_location_address = null;
        $this->vgagco_location_city = null;
        $this->vgagco_location_country = null;
        $this->vgagco_location_state = null;
        $this->vgagco_phone = null;
        $this->vgagco_website = null;
        $this->vgagco_founded_at = null;
        $this->vgagco_api_detail_url = null;
        $this->vgagco_site_detail_url = null;
        $this->vgagco_image_icon_url = null;
        $this->vgagco_image_medium_url = null;
        $this->vgagco_image_screen_url = null;
        $this->vgagco_image_small_url = null;
        $this->vgagco_image_super_url = null;
        $this->vgagco_image_thumb_url = null;
        $this->vgagco_image_tiny_url = null;
        $this->vgagco_created_at = null;
        $this->vgagco_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombPlatforms) {
                foreach ($this->collApiGiantBombPlatforms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameDevelopers) {
                foreach ($this->collApiGiantBombGameDevelopers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGamePublishers) {
                foreach ($this->collApiGiantBombGamePublishers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleaseDevelopers) {
                foreach ($this->collApiGiantBombGameReleaseDevelopers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleasePublishers) {
                foreach ($this->collApiGiantBombGameReleasePublishers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombPlatforms = null;
        $this->collApiGiantBombGameDevelopers = null;
        $this->collApiGiantBombGamePublishers = null;
        $this->collApiGiantBombGameReleaseDevelopers = null;
        $this->collApiGiantBombGameReleasePublishers = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombCompanyTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
