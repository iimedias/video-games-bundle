<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter as ChildApiGiantBombCharacter;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery as ChildApiGiantBombCharacterQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombCharacterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_character_vgagch' table.
 *
 *
 *
 * @method     ChildApiGiantBombCharacterQuery orderById($order = Criteria::ASC) Order by the vgagch_id column
 * @method     ChildApiGiantBombCharacterQuery orderByName($order = Criteria::ASC) Order by the vgagch_name column
 * @method     ChildApiGiantBombCharacterQuery orderByAliases($order = Criteria::ASC) Order by the vgagch_aliases column
 * @method     ChildApiGiantBombCharacterQuery orderBySummary($order = Criteria::ASC) Order by the vgagch_summary column
 * @method     ChildApiGiantBombCharacterQuery orderByDescription($order = Criteria::ASC) Order by the vgagch_description column
 * @method     ChildApiGiantBombCharacterQuery orderByRealName($order = Criteria::ASC) Order by the vgagch_real_name column
 * @method     ChildApiGiantBombCharacterQuery orderByLastName($order = Criteria::ASC) Order by the vgagch_last_name column
 * @method     ChildApiGiantBombCharacterQuery orderByGender($order = Criteria::ASC) Order by the vgagch_gender column
 * @method     ChildApiGiantBombCharacterQuery orderByBirthday($order = Criteria::ASC) Order by the vgagch_birthday column
 * @method     ChildApiGiantBombCharacterQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagch_api_detail_url column
 * @method     ChildApiGiantBombCharacterQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagch_site_detail_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagch_image_icon_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagch_image_medium_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagch_image_screen_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagch_image_small_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagch_image_super_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagch_image_thumb_url column
 * @method     ChildApiGiantBombCharacterQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagch_image_tiny_url column
 * @method     ChildApiGiantBombCharacterQuery orderByGiantBombGameId($order = Criteria::ASC) Order by the vgagch_first_vgagga_id column
 * @method     ChildApiGiantBombCharacterQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagch_created_at column
 * @method     ChildApiGiantBombCharacterQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagch_updated_at column
 *
 * @method     ChildApiGiantBombCharacterQuery groupById() Group by the vgagch_id column
 * @method     ChildApiGiantBombCharacterQuery groupByName() Group by the vgagch_name column
 * @method     ChildApiGiantBombCharacterQuery groupByAliases() Group by the vgagch_aliases column
 * @method     ChildApiGiantBombCharacterQuery groupBySummary() Group by the vgagch_summary column
 * @method     ChildApiGiantBombCharacterQuery groupByDescription() Group by the vgagch_description column
 * @method     ChildApiGiantBombCharacterQuery groupByRealName() Group by the vgagch_real_name column
 * @method     ChildApiGiantBombCharacterQuery groupByLastName() Group by the vgagch_last_name column
 * @method     ChildApiGiantBombCharacterQuery groupByGender() Group by the vgagch_gender column
 * @method     ChildApiGiantBombCharacterQuery groupByBirthday() Group by the vgagch_birthday column
 * @method     ChildApiGiantBombCharacterQuery groupByApiDetailUrl() Group by the vgagch_api_detail_url column
 * @method     ChildApiGiantBombCharacterQuery groupBySiteDetailUrl() Group by the vgagch_site_detail_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageIconUrl() Group by the vgagch_image_icon_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageMediumUrl() Group by the vgagch_image_medium_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageScreenUrl() Group by the vgagch_image_screen_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageSmallUrl() Group by the vgagch_image_small_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageSuperUrl() Group by the vgagch_image_super_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageThumbUrl() Group by the vgagch_image_thumb_url column
 * @method     ChildApiGiantBombCharacterQuery groupByImageTinyUrl() Group by the vgagch_image_tiny_url column
 * @method     ChildApiGiantBombCharacterQuery groupByGiantBombGameId() Group by the vgagch_first_vgagga_id column
 * @method     ChildApiGiantBombCharacterQuery groupByCreatedAt() Group by the vgagch_created_at column
 * @method     ChildApiGiantBombCharacterQuery groupByUpdatedAt() Group by the vgagch_updated_at column
 *
 * @method     ChildApiGiantBombCharacterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombCharacterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombCharacterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombCharacterQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombCharacterQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombCharacterQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombCharacterQuery leftJoinApiGiantBombFirstGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombCharacterQuery rightJoinApiGiantBombFirstGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombCharacterQuery innerJoinApiGiantBombFirstGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombCharacterQuery joinWithApiGiantBombFirstGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombCharacterQuery leftJoinWithApiGiantBombFirstGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombCharacterQuery rightJoinWithApiGiantBombFirstGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombCharacterQuery innerJoinWithApiGiantBombFirstGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombCharacter findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombCharacter matching the query
 * @method     ChildApiGiantBombCharacter findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombCharacter matching the query, or a new ChildApiGiantBombCharacter object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombCharacter findOneById(int $vgagch_id) Return the first ChildApiGiantBombCharacter filtered by the vgagch_id column
 * @method     ChildApiGiantBombCharacter findOneByName(string $vgagch_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_name column
 * @method     ChildApiGiantBombCharacter findOneByAliases(array $vgagch_aliases) Return the first ChildApiGiantBombCharacter filtered by the vgagch_aliases column
 * @method     ChildApiGiantBombCharacter findOneBySummary(string $vgagch_summary) Return the first ChildApiGiantBombCharacter filtered by the vgagch_summary column
 * @method     ChildApiGiantBombCharacter findOneByDescription(string $vgagch_description) Return the first ChildApiGiantBombCharacter filtered by the vgagch_description column
 * @method     ChildApiGiantBombCharacter findOneByRealName(string $vgagch_real_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_real_name column
 * @method     ChildApiGiantBombCharacter findOneByLastName(string $vgagch_last_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_last_name column
 * @method     ChildApiGiantBombCharacter findOneByGender(int $vgagch_gender) Return the first ChildApiGiantBombCharacter filtered by the vgagch_gender column
 * @method     ChildApiGiantBombCharacter findOneByBirthday(string $vgagch_birthday) Return the first ChildApiGiantBombCharacter filtered by the vgagch_birthday column
 * @method     ChildApiGiantBombCharacter findOneByApiDetailUrl(string $vgagch_api_detail_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_api_detail_url column
 * @method     ChildApiGiantBombCharacter findOneBySiteDetailUrl(string $vgagch_site_detail_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_site_detail_url column
 * @method     ChildApiGiantBombCharacter findOneByImageIconUrl(string $vgagch_image_icon_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_icon_url column
 * @method     ChildApiGiantBombCharacter findOneByImageMediumUrl(string $vgagch_image_medium_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_medium_url column
 * @method     ChildApiGiantBombCharacter findOneByImageScreenUrl(string $vgagch_image_screen_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_screen_url column
 * @method     ChildApiGiantBombCharacter findOneByImageSmallUrl(string $vgagch_image_small_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_small_url column
 * @method     ChildApiGiantBombCharacter findOneByImageSuperUrl(string $vgagch_image_super_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_super_url column
 * @method     ChildApiGiantBombCharacter findOneByImageThumbUrl(string $vgagch_image_thumb_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_thumb_url column
 * @method     ChildApiGiantBombCharacter findOneByImageTinyUrl(string $vgagch_image_tiny_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_tiny_url column
 * @method     ChildApiGiantBombCharacter findOneByGiantBombGameId(int $vgagch_first_vgagga_id) Return the first ChildApiGiantBombCharacter filtered by the vgagch_first_vgagga_id column
 * @method     ChildApiGiantBombCharacter findOneByCreatedAt(string $vgagch_created_at) Return the first ChildApiGiantBombCharacter filtered by the vgagch_created_at column
 * @method     ChildApiGiantBombCharacter findOneByUpdatedAt(string $vgagch_updated_at) Return the first ChildApiGiantBombCharacter filtered by the vgagch_updated_at column *

 * @method     ChildApiGiantBombCharacter requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombCharacter by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombCharacter matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombCharacter requireOneById(int $vgagch_id) Return the first ChildApiGiantBombCharacter filtered by the vgagch_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByName(string $vgagch_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByAliases(array $vgagch_aliases) Return the first ChildApiGiantBombCharacter filtered by the vgagch_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneBySummary(string $vgagch_summary) Return the first ChildApiGiantBombCharacter filtered by the vgagch_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByDescription(string $vgagch_description) Return the first ChildApiGiantBombCharacter filtered by the vgagch_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByRealName(string $vgagch_real_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_real_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByLastName(string $vgagch_last_name) Return the first ChildApiGiantBombCharacter filtered by the vgagch_last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByGender(int $vgagch_gender) Return the first ChildApiGiantBombCharacter filtered by the vgagch_gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByBirthday(string $vgagch_birthday) Return the first ChildApiGiantBombCharacter filtered by the vgagch_birthday column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByApiDetailUrl(string $vgagch_api_detail_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneBySiteDetailUrl(string $vgagch_site_detail_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageIconUrl(string $vgagch_image_icon_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageMediumUrl(string $vgagch_image_medium_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageScreenUrl(string $vgagch_image_screen_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageSmallUrl(string $vgagch_image_small_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageSuperUrl(string $vgagch_image_super_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageThumbUrl(string $vgagch_image_thumb_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByImageTinyUrl(string $vgagch_image_tiny_url) Return the first ChildApiGiantBombCharacter filtered by the vgagch_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByGiantBombGameId(int $vgagch_first_vgagga_id) Return the first ChildApiGiantBombCharacter filtered by the vgagch_first_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByCreatedAt(string $vgagch_created_at) Return the first ChildApiGiantBombCharacter filtered by the vgagch_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombCharacter requireOneByUpdatedAt(string $vgagch_updated_at) Return the first ChildApiGiantBombCharacter filtered by the vgagch_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombCharacter objects based on current ModelCriteria
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findById(int $vgagch_id) Return ChildApiGiantBombCharacter objects filtered by the vgagch_id column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByName(string $vgagch_name) Return ChildApiGiantBombCharacter objects filtered by the vgagch_name column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByAliases(array $vgagch_aliases) Return ChildApiGiantBombCharacter objects filtered by the vgagch_aliases column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findBySummary(string $vgagch_summary) Return ChildApiGiantBombCharacter objects filtered by the vgagch_summary column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByDescription(string $vgagch_description) Return ChildApiGiantBombCharacter objects filtered by the vgagch_description column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByRealName(string $vgagch_real_name) Return ChildApiGiantBombCharacter objects filtered by the vgagch_real_name column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByLastName(string $vgagch_last_name) Return ChildApiGiantBombCharacter objects filtered by the vgagch_last_name column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByGender(int $vgagch_gender) Return ChildApiGiantBombCharacter objects filtered by the vgagch_gender column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByBirthday(string $vgagch_birthday) Return ChildApiGiantBombCharacter objects filtered by the vgagch_birthday column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByApiDetailUrl(string $vgagch_api_detail_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_api_detail_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findBySiteDetailUrl(string $vgagch_site_detail_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_site_detail_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageIconUrl(string $vgagch_image_icon_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_icon_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageMediumUrl(string $vgagch_image_medium_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_medium_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageScreenUrl(string $vgagch_image_screen_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_screen_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageSmallUrl(string $vgagch_image_small_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_small_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageSuperUrl(string $vgagch_image_super_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_super_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageThumbUrl(string $vgagch_image_thumb_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_thumb_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByImageTinyUrl(string $vgagch_image_tiny_url) Return ChildApiGiantBombCharacter objects filtered by the vgagch_image_tiny_url column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByGiantBombGameId(int $vgagch_first_vgagga_id) Return ChildApiGiantBombCharacter objects filtered by the vgagch_first_vgagga_id column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByCreatedAt(string $vgagch_created_at) Return ChildApiGiantBombCharacter objects filtered by the vgagch_created_at column
 * @method     ChildApiGiantBombCharacter[]|ObjectCollection findByUpdatedAt(string $vgagch_updated_at) Return ChildApiGiantBombCharacter objects filtered by the vgagch_updated_at column
 * @method     ChildApiGiantBombCharacter[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombCharacterQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombCharacterQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombCharacter', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombCharacterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombCharacterQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombCharacterQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombCharacterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombCharacter|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombCharacterTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombCharacter A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagch_id, vgagch_name, vgagch_aliases, vgagch_summary, vgagch_description, vgagch_real_name, vgagch_last_name, vgagch_gender, vgagch_birthday, vgagch_api_detail_url, vgagch_site_detail_url, vgagch_image_icon_url, vgagch_image_medium_url, vgagch_image_screen_url, vgagch_image_small_url, vgagch_image_super_url, vgagch_image_thumb_url, vgagch_image_tiny_url, vgagch_first_vgagga_id, vgagch_created_at, vgagch_updated_at FROM videogames_api_giantbomb_character_vgagch WHERE vgagch_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombCharacter $obj */
            $obj = new ChildApiGiantBombCharacter();
            $obj->hydrate($row);
            ApiGiantBombCharacterTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombCharacter|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagch_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagch_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagch_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagch_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagch_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagch_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagch_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagch_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagch_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagch_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagch_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagch_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagch_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagch_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagch_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagch_real_name column
     *
     * Example usage:
     * <code>
     * $query->filterByRealName('fooValue');   // WHERE vgagch_real_name = 'fooValue'
     * $query->filterByRealName('%fooValue%'); // WHERE vgagch_real_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $realName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByRealName($realName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($realName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_REAL_NAME, $realName, $comparison);
    }

    /**
     * Filter the query on the vgagch_last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE vgagch_last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%'); // WHERE vgagch_last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the vgagch_gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender(1234); // WHERE vgagch_gender = 1234
     * $query->filterByGender(array(12, 34)); // WHERE vgagch_gender IN (12, 34)
     * $query->filterByGender(array('min' => 12)); // WHERE vgagch_gender > 12
     * </code>
     *
     * @param     mixed $gender The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (is_array($gender)) {
            $useMinMax = false;
            if (isset($gender['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER, $gender['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gender['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER, $gender['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the vgagch_birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE vgagch_birthday = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE vgagch_birthday = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE vgagch_birthday > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the vgagch_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagch_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagch_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagch_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagch_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagch_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagch_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagch_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagch_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagch_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagch_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagch_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagch_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagch_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagch_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagch_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagch_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagch_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagch_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagch_first_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombGameId(1234); // WHERE vgagch_first_vgagga_id = 1234
     * $query->filterByGiantBombGameId(array(12, 34)); // WHERE vgagch_first_vgagga_id IN (12, 34)
     * $query->filterByGiantBombGameId(array('min' => 12)); // WHERE vgagch_first_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombFirstGame()
     *
     * @param     mixed $giantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByGiantBombGameId($giantBombGameId = null, $comparison = null)
    {
        if (is_array($giantBombGameId)) {
            $useMinMax = false;
            if (isset($giantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, $giantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, $giantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, $giantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgagch_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagch_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagch_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagch_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagch_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagch_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagch_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagch_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_FIRST_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombFirstGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombCharacter $apiGiantBombCharacter Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombCharacterQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombCharacter = null)
    {
        if ($apiGiantBombCharacter) {
            $this->addUsingAlias(ApiGiantBombCharacterTableMap::COL_VGAGCH_ID, $apiGiantBombCharacter->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_character_vgagch table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombCharacterTableMap::clearInstancePool();
            ApiGiantBombCharacterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombCharacterTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombCharacterTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombCharacterTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombCharacterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombCharacterQuery
