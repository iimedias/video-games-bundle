<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery as ChildApiGiantBombImageQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombImageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_image_vgagim' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombImage implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombImageTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagim_id field.
     *
     * @var        int
     */
    protected $vgagim_id;

    /**
     * The value for the vgagim_tags field.
     *
     * @var        array
     */
    protected $vgagim_tags;

    /**
     * The unserialized $vgagim_tags value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagim_tags_unserialized;

    /**
     * The value for the vgagim_vgagga_id field.
     *
     * @var        int
     */
    protected $vgagim_vgagga_id;

    /**
     * The value for the vgagim_vgagrl_id field.
     *
     * @var        int
     */
    protected $vgagim_vgagrl_id;

    /**
     * The value for the vgagim_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagim_image_icon_url;

    /**
     * The value for the vgagim_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagim_image_medium_url;

    /**
     * The value for the vgagim_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagim_image_screen_url;

    /**
     * The value for the vgagim_image_small_url field.
     *
     * @var        string
     */
    protected $vgagim_image_small_url;

    /**
     * The value for the vgagim_image_super_url field.
     *
     * @var        string
     */
    protected $vgagim_image_super_url;

    /**
     * The value for the vgagim_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagim_image_thumb_url;

    /**
     * The value for the vgagim_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagim_image_tiny_url;

    /**
     * @var        ChildApiGiantBombGame
     */
    protected $aApiGiantBombGame;

    /**
     * @var        ChildApiGiantBombGameRelease
     */
    protected $aApiGiantBombGameRelease;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombImage object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombImage</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombImage</code>, delegates to
     * <code>equals(ApiGiantBombImage)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombImage The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagim_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagim_id;
    }

    /**
     * Get the [vgagim_tags] column value.
     *
     * @return array
     */
    public function getTags()
    {
        if (null === $this->vgagim_tags_unserialized) {
            $this->vgagim_tags_unserialized = array();
        }
        if (!$this->vgagim_tags_unserialized && null !== $this->vgagim_tags) {
            $vgagim_tags_unserialized = substr($this->vgagim_tags, 2, -2);
            $this->vgagim_tags_unserialized = $vgagim_tags_unserialized ? explode(' | ', $vgagim_tags_unserialized) : array();
        }

        return $this->vgagim_tags_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagim_tags] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasTag($value)
    {
        return in_array($value, $this->getTags());
    } // hasTag()

    /**
     * Get the [vgagim_vgagga_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombGameId()
    {
        return $this->vgagim_vgagga_id;
    }

    /**
     * Get the [vgagim_vgagrl_id] column value.
     *
     * @return int
     */
    public function getApiGiantBombGameReleaseId()
    {
        return $this->vgagim_vgagrl_id;
    }

    /**
     * Get the [vgagim_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagim_image_icon_url;
    }

    /**
     * Get the [vgagim_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagim_image_medium_url;
    }

    /**
     * Get the [vgagim_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagim_image_screen_url;
    }

    /**
     * Get the [vgagim_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagim_image_small_url;
    }

    /**
     * Get the [vgagim_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagim_image_super_url;
    }

    /**
     * Get the [vgagim_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagim_image_thumb_url;
    }

    /**
     * Get the [vgagim_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagim_image_tiny_url;
    }

    /**
     * Set the value of [vgagim_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagim_id !== $v) {
            $this->vgagim_id = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagim_tags] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setTags($v)
    {
        if ($this->vgagim_tags_unserialized !== $v) {
            $this->vgagim_tags_unserialized = $v;
            $this->vgagim_tags = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_TAGS] = true;
        }

        return $this;
    } // setTags()

    /**
     * Adds a value to the [vgagim_tags] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function addTag($value)
    {
        $currentArray = $this->getTags();
        $currentArray []= $value;
        $this->setTags($currentArray);

        return $this;
    } // addTag()

    /**
     * Removes a value from the [vgagim_tags] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function removeTag($value)
    {
        $targetArray = array();
        foreach ($this->getTags() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setTags($targetArray);

        return $this;
    } // removeTag()

    /**
     * Set the value of [vgagim_vgagga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setApiGiantBombGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagim_vgagga_id !== $v) {
            $this->vgagim_vgagga_id = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID] = true;
        }

        if ($this->aApiGiantBombGame !== null && $this->aApiGiantBombGame->getId() !== $v) {
            $this->aApiGiantBombGame = null;
        }

        return $this;
    } // setApiGiantBombGameId()

    /**
     * Set the value of [vgagim_vgagrl_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleaseId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagim_vgagrl_id !== $v) {
            $this->vgagim_vgagrl_id = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID] = true;
        }

        if ($this->aApiGiantBombGameRelease !== null && $this->aApiGiantBombGameRelease->getId() !== $v) {
            $this->aApiGiantBombGameRelease = null;
        }

        return $this;
    } // setApiGiantBombGameReleaseId()

    /**
     * Set the value of [vgagim_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_icon_url !== $v) {
            $this->vgagim_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagim_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_medium_url !== $v) {
            $this->vgagim_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagim_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_screen_url !== $v) {
            $this->vgagim_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagim_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_small_url !== $v) {
            $this->vgagim_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagim_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_super_url !== $v) {
            $this->vgagim_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagim_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_thumb_url !== $v) {
            $this->vgagim_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagim_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagim_image_tiny_url !== $v) {
            $this->vgagim_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombImageTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombImageTableMap::translateFieldName('Tags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_tags = $col;
            $this->vgagim_tags_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ApiGiantBombGameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_vgagga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ApiGiantBombGameReleaseId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_vgagrl_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombImageTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagim_image_tiny_url = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = ApiGiantBombImageTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombImage'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombGame !== null && $this->vgagim_vgagga_id !== $this->aApiGiantBombGame->getId()) {
            $this->aApiGiantBombGame = null;
        }
        if ($this->aApiGiantBombGameRelease !== null && $this->vgagim_vgagrl_id !== $this->aApiGiantBombGameRelease->getId()) {
            $this->aApiGiantBombGameRelease = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombImageTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombImageQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombGame = null;
            $this->aApiGiantBombGameRelease = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombImage::setDeleted()
     * @see ApiGiantBombImage::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombImageTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombImageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombImageTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombImageTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombGame !== null) {
                if ($this->aApiGiantBombGame->isModified() || $this->aApiGiantBombGame->isNew()) {
                    $affectedRows += $this->aApiGiantBombGame->save($con);
                }
                $this->setApiGiantBombGame($this->aApiGiantBombGame);
            }

            if ($this->aApiGiantBombGameRelease !== null) {
                if ($this->aApiGiantBombGameRelease->isModified() || $this->aApiGiantBombGameRelease->isNew()) {
                    $affectedRows += $this->aApiGiantBombGameRelease->save($con);
                }
                $this->setApiGiantBombGameRelease($this->aApiGiantBombGameRelease);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ApiGiantBombImageTableMap::COL_VGAGIM_ID] = true;
        if (null !== $this->vgagim_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ApiGiantBombImageTableMap::COL_VGAGIM_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_id';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_tags';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_vgagga_id';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_vgagrl_id';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagim_image_tiny_url';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_image_vgagim (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagim_id':
                        $stmt->bindValue($identifier, $this->vgagim_id, PDO::PARAM_INT);
                        break;
                    case 'vgagim_tags':
                        $stmt->bindValue($identifier, $this->vgagim_tags, PDO::PARAM_STR);
                        break;
                    case 'vgagim_vgagga_id':
                        $stmt->bindValue($identifier, $this->vgagim_vgagga_id, PDO::PARAM_INT);
                        break;
                    case 'vgagim_vgagrl_id':
                        $stmt->bindValue($identifier, $this->vgagim_vgagrl_id, PDO::PARAM_INT);
                        break;
                    case 'vgagim_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagim_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagim_image_tiny_url, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombImageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTags();
                break;
            case 2:
                return $this->getApiGiantBombGameId();
                break;
            case 3:
                return $this->getApiGiantBombGameReleaseId();
                break;
            case 4:
                return $this->getImageIconUrl();
                break;
            case 5:
                return $this->getImageMediumUrl();
                break;
            case 6:
                return $this->getImageScreenUrl();
                break;
            case 7:
                return $this->getImageSmallUrl();
                break;
            case 8:
                return $this->getImageSuperUrl();
                break;
            case 9:
                return $this->getImageThumbUrl();
                break;
            case 10:
                return $this->getImageTinyUrl();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombImage'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombImage'][$this->hashCode()] = true;
        $keys = ApiGiantBombImageTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTags(),
            $keys[2] => $this->getApiGiantBombGameId(),
            $keys[3] => $this->getApiGiantBombGameReleaseId(),
            $keys[4] => $this->getImageIconUrl(),
            $keys[5] => $this->getImageMediumUrl(),
            $keys[6] => $this->getImageScreenUrl(),
            $keys[7] => $this->getImageSmallUrl(),
            $keys[8] => $this->getImageSuperUrl(),
            $keys[9] => $this->getImageThumbUrl(),
            $keys[10] => $this->getImageTinyUrl(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGame';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_vgagga';
                        break;
                    default:
                        $key = 'ApiGiantBombGame';
                }

                $result[$key] = $this->aApiGiantBombGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiGiantBombGameRelease) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameRelease';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_release_vgagrl';
                        break;
                    default:
                        $key = 'ApiGiantBombGameRelease';
                }

                $result[$key] = $this->aApiGiantBombGameRelease->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombImageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setTags($value);
                break;
            case 2:
                $this->setApiGiantBombGameId($value);
                break;
            case 3:
                $this->setApiGiantBombGameReleaseId($value);
                break;
            case 4:
                $this->setImageIconUrl($value);
                break;
            case 5:
                $this->setImageMediumUrl($value);
                break;
            case 6:
                $this->setImageScreenUrl($value);
                break;
            case 7:
                $this->setImageSmallUrl($value);
                break;
            case 8:
                $this->setImageSuperUrl($value);
                break;
            case 9:
                $this->setImageThumbUrl($value);
                break;
            case 10:
                $this->setImageTinyUrl($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombImageTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTags($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setApiGiantBombGameId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setApiGiantBombGameReleaseId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setImageIconUrl($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setImageMediumUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setImageScreenUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImageSmallUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImageSuperUrl($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImageThumbUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setImageTinyUrl($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombImageTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_ID)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_ID, $this->vgagim_id);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_TAGS)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_TAGS, $this->vgagim_tags);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGGA_ID, $this->vgagim_vgagga_id);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_VGAGRL_ID, $this->vgagim_vgagrl_id);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_ICON_URL, $this->vgagim_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_MEDIUM_URL, $this->vgagim_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SCREEN_URL, $this->vgagim_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SMALL_URL, $this->vgagim_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_SUPER_URL, $this->vgagim_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_THUMB_URL, $this->vgagim_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_IMAGE_TINY_URL, $this->vgagim_image_tiny_url);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombImageQuery::create();
        $criteria->add(ApiGiantBombImageTableMap::COL_VGAGIM_ID, $this->vgagim_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagim_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTags($this->getTags());
        $copyObj->setApiGiantBombGameId($this->getApiGiantBombGameId());
        $copyObj->setApiGiantBombGameReleaseId($this->getApiGiantBombGameReleaseId());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGame object.
     *
     * @param  ChildApiGiantBombGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombGame(ChildApiGiantBombGame $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombGameId(NULL);
        } else {
            $this->setApiGiantBombGameId($v->getId());
        }

        $this->aApiGiantBombGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGame object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombImage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGame The associated ChildApiGiantBombGame object.
     * @throws PropelException
     */
    public function getApiGiantBombGame(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombGame === null && ($this->vgagim_vgagga_id !== null)) {
            $this->aApiGiantBombGame = ChildApiGiantBombGameQuery::create()->findPk($this->vgagim_vgagga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombGame->addApiGiantBombImages($this);
             */
        }

        return $this->aApiGiantBombGame;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGameRelease object.
     *
     * @param  ChildApiGiantBombGameRelease $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombGameRelease(ChildApiGiantBombGameRelease $v = null)
    {
        if ($v === null) {
            $this->setApiGiantBombGameReleaseId(NULL);
        } else {
            $this->setApiGiantBombGameReleaseId($v->getId());
        }

        $this->aApiGiantBombGameRelease = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGameRelease object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombImage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGameRelease object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGameRelease The associated ChildApiGiantBombGameRelease object.
     * @throws PropelException
     */
    public function getApiGiantBombGameRelease(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombGameRelease === null && ($this->vgagim_vgagrl_id !== null)) {
            $this->aApiGiantBombGameRelease = ChildApiGiantBombGameReleaseQuery::create()->findPk($this->vgagim_vgagrl_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombGameRelease->addApiGiantBombImages($this);
             */
        }

        return $this->aApiGiantBombGameRelease;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombGame) {
            $this->aApiGiantBombGame->removeApiGiantBombImage($this);
        }
        if (null !== $this->aApiGiantBombGameRelease) {
            $this->aApiGiantBombGameRelease->removeApiGiantBombImage($this);
        }
        $this->vgagim_id = null;
        $this->vgagim_tags = null;
        $this->vgagim_tags_unserialized = null;
        $this->vgagim_vgagga_id = null;
        $this->vgagim_vgagrl_id = null;
        $this->vgagim_image_icon_url = null;
        $this->vgagim_image_medium_url = null;
        $this->vgagim_image_screen_url = null;
        $this->vgagim_image_small_url = null;
        $this->vgagim_image_super_url = null;
        $this->vgagim_image_thumb_url = null;
        $this->vgagim_image_tiny_url = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aApiGiantBombGame = null;
        $this->aApiGiantBombGameRelease = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombImageTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
