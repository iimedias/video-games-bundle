<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameRelease as ChildSiteJeuxVideoComGameRelease;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameReleaseQuery as ChildSiteJeuxVideoComGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteJeuxVideoComGameReleaseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_jeuxvideocom_release_vgajrl' table.
 *
 *
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery orderById($order = Criteria::ASC) Order by the vgajrl_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery orderBySiteJeuxVideoComGameId($order = Criteria::ASC) Order by the vgajrl_vgajga_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery orderBySiteJeuxVideoComPlatformId($order = Criteria::ASC) Order by the vgajrl_vgajpl_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgajrl_site_detail_url column
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery groupById() Group by the vgajrl_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery groupBySiteJeuxVideoComGameId() Group by the vgajrl_vgajga_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery groupBySiteJeuxVideoComPlatformId() Group by the vgajrl_vgajpl_id column
 * @method     ChildSiteJeuxVideoComGameReleaseQuery groupBySiteDetailUrl() Group by the vgajrl_site_detail_url column
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoinSiteJeuxVideoComGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteJeuxVideoComGame relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoinSiteJeuxVideoComGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteJeuxVideoComGame relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoinSiteJeuxVideoComGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery joinWithSiteJeuxVideoComGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoinWithSiteJeuxVideoComGame() Adds a LEFT JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoinWithSiteJeuxVideoComGame() Adds a RIGHT JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoinWithSiteJeuxVideoComGame() Adds a INNER JOIN clause and with to the query using the SiteJeuxVideoComGame relation
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoinSiteJeuxVideoComPlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteJeuxVideoComPlatform relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoinSiteJeuxVideoComPlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteJeuxVideoComPlatform relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoinSiteJeuxVideoComPlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteJeuxVideoComPlatform relation
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery joinWithSiteJeuxVideoComPlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteJeuxVideoComPlatform relation
 *
 * @method     ChildSiteJeuxVideoComGameReleaseQuery leftJoinWithSiteJeuxVideoComPlatform() Adds a LEFT JOIN clause and with to the query using the SiteJeuxVideoComPlatform relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery rightJoinWithSiteJeuxVideoComPlatform() Adds a RIGHT JOIN clause and with to the query using the SiteJeuxVideoComPlatform relation
 * @method     ChildSiteJeuxVideoComGameReleaseQuery innerJoinWithSiteJeuxVideoComPlatform() Adds a INNER JOIN clause and with to the query using the SiteJeuxVideoComPlatform relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery|\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatformQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteJeuxVideoComGameRelease findOne(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGameRelease matching the query
 * @method     ChildSiteJeuxVideoComGameRelease findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGameRelease matching the query, or a new ChildSiteJeuxVideoComGameRelease object populated from the query conditions when no match is found
 *
 * @method     ChildSiteJeuxVideoComGameRelease findOneById(int $vgajrl_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_id column
 * @method     ChildSiteJeuxVideoComGameRelease findOneBySiteJeuxVideoComGameId(string $vgajrl_vgajga_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_vgajga_id column
 * @method     ChildSiteJeuxVideoComGameRelease findOneBySiteJeuxVideoComPlatformId(int $vgajrl_vgajpl_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_vgajpl_id column
 * @method     ChildSiteJeuxVideoComGameRelease findOneBySiteDetailUrl(string $vgajrl_site_detail_url) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_site_detail_url column *

 * @method     ChildSiteJeuxVideoComGameRelease requirePk($key, ConnectionInterface $con = null) Return the ChildSiteJeuxVideoComGameRelease by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGameRelease requireOne(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGameRelease matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteJeuxVideoComGameRelease requireOneById(int $vgajrl_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGameRelease requireOneBySiteJeuxVideoComGameId(string $vgajrl_vgajga_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_vgajga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGameRelease requireOneBySiteJeuxVideoComPlatformId(int $vgajrl_vgajpl_id) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_vgajpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGameRelease requireOneBySiteDetailUrl(string $vgajrl_site_detail_url) Return the first ChildSiteJeuxVideoComGameRelease filtered by the vgajrl_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteJeuxVideoComGameRelease[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteJeuxVideoComGameRelease objects based on current ModelCriteria
 * @method     ChildSiteJeuxVideoComGameRelease[]|ObjectCollection findById(int $vgajrl_id) Return ChildSiteJeuxVideoComGameRelease objects filtered by the vgajrl_id column
 * @method     ChildSiteJeuxVideoComGameRelease[]|ObjectCollection findBySiteJeuxVideoComGameId(string $vgajrl_vgajga_id) Return ChildSiteJeuxVideoComGameRelease objects filtered by the vgajrl_vgajga_id column
 * @method     ChildSiteJeuxVideoComGameRelease[]|ObjectCollection findBySiteJeuxVideoComPlatformId(int $vgajrl_vgajpl_id) Return ChildSiteJeuxVideoComGameRelease objects filtered by the vgajrl_vgajpl_id column
 * @method     ChildSiteJeuxVideoComGameRelease[]|ObjectCollection findBySiteDetailUrl(string $vgajrl_site_detail_url) Return ChildSiteJeuxVideoComGameRelease objects filtered by the vgajrl_site_detail_url column
 * @method     ChildSiteJeuxVideoComGameRelease[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteJeuxVideoComGameReleaseQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteJeuxVideoComGameReleaseQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGameRelease', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteJeuxVideoComGameReleaseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteJeuxVideoComGameReleaseQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteJeuxVideoComGameReleaseQuery) {
            return $criteria;
        }
        $query = new ChildSiteJeuxVideoComGameReleaseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteJeuxVideoComGameRelease|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteJeuxVideoComGameReleaseTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteJeuxVideoComGameReleaseTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteJeuxVideoComGameRelease A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgajrl_id, vgajrl_vgajga_id, vgajrl_vgajpl_id, vgajrl_site_detail_url FROM videogames_site_jeuxvideocom_release_vgajrl WHERE vgajrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteJeuxVideoComGameRelease $obj */
            $obj = new ChildSiteJeuxVideoComGameRelease();
            $obj->hydrate($row);
            SiteJeuxVideoComGameReleaseTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteJeuxVideoComGameRelease|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgajrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgajrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgajrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgajrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgajrl_vgajga_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteJeuxVideoComGameId(1234); // WHERE vgajrl_vgajga_id = 1234
     * $query->filterBySiteJeuxVideoComGameId(array(12, 34)); // WHERE vgajrl_vgajga_id IN (12, 34)
     * $query->filterBySiteJeuxVideoComGameId(array('min' => 12)); // WHERE vgajrl_vgajga_id > 12
     * </code>
     *
     * @see       filterBySiteJeuxVideoComGame()
     *
     * @param     mixed $siteJeuxVideoComGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComGameId($siteJeuxVideoComGameId = null, $comparison = null)
    {
        if (is_array($siteJeuxVideoComGameId)) {
            $useMinMax = false;
            if (isset($siteJeuxVideoComGameId['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJGA_ID, $siteJeuxVideoComGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteJeuxVideoComGameId['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJGA_ID, $siteJeuxVideoComGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJGA_ID, $siteJeuxVideoComGameId, $comparison);
    }

    /**
     * Filter the query on the vgajrl_vgajpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteJeuxVideoComPlatformId(1234); // WHERE vgajrl_vgajpl_id = 1234
     * $query->filterBySiteJeuxVideoComPlatformId(array(12, 34)); // WHERE vgajrl_vgajpl_id IN (12, 34)
     * $query->filterBySiteJeuxVideoComPlatformId(array('min' => 12)); // WHERE vgajrl_vgajpl_id > 12
     * </code>
     *
     * @see       filterBySiteJeuxVideoComPlatform()
     *
     * @param     mixed $siteJeuxVideoComPlatformId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComPlatformId($siteJeuxVideoComPlatformId = null, $comparison = null)
    {
        if (is_array($siteJeuxVideoComPlatformId)) {
            $useMinMax = false;
            if (isset($siteJeuxVideoComPlatformId['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJPL_ID, $siteJeuxVideoComPlatformId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteJeuxVideoComPlatformId['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJPL_ID, $siteJeuxVideoComPlatformId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJPL_ID, $siteJeuxVideoComPlatformId, $comparison);
    }

    /**
     * Filter the query on the vgajrl_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgajrl_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgajrl_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame|ObjectCollection $siteJeuxVideoComGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComGame($siteJeuxVideoComGame, $comparison = null)
    {
        if ($siteJeuxVideoComGame instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame) {
            return $this
                ->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJGA_ID, $siteJeuxVideoComGame->getId(), $comparison);
        } elseif ($siteJeuxVideoComGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJGA_ID, $siteJeuxVideoComGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteJeuxVideoComGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteJeuxVideoComGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteJeuxVideoComGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteJeuxVideoComGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteJeuxVideoComGame');
        }

        return $this;
    }

    /**
     * Use the SiteJeuxVideoComGame relation SiteJeuxVideoComGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteJeuxVideoComGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteJeuxVideoComGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteJeuxVideoComGame', '\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform|ObjectCollection $siteJeuxVideoComPlatform The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComPlatform($siteJeuxVideoComPlatform, $comparison = null)
    {
        if ($siteJeuxVideoComPlatform instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform) {
            return $this
                ->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJPL_ID, $siteJeuxVideoComPlatform->getId(), $comparison);
        } elseif ($siteJeuxVideoComPlatform instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_VGAJPL_ID, $siteJeuxVideoComPlatform->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteJeuxVideoComPlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteJeuxVideoComPlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteJeuxVideoComPlatform($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteJeuxVideoComPlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteJeuxVideoComPlatform');
        }

        return $this;
    }

    /**
     * Use the SiteJeuxVideoComPlatform relation SiteJeuxVideoComPlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatformQuery A secondary query class using the current class as primary query
     */
    public function useSiteJeuxVideoComPlatformQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteJeuxVideoComPlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteJeuxVideoComPlatform', '\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComPlatformQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteJeuxVideoComGameRelease $siteJeuxVideoComGameRelease Object to remove from the list of results
     *
     * @return $this|ChildSiteJeuxVideoComGameReleaseQuery The current query, for fluid interface
     */
    public function prune($siteJeuxVideoComGameRelease = null)
    {
        if ($siteJeuxVideoComGameRelease) {
            $this->addUsingAlias(SiteJeuxVideoComGameReleaseTableMap::COL_VGAJRL_ID, $siteJeuxVideoComGameRelease->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_jeuxvideocom_release_vgajrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameReleaseTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteJeuxVideoComGameReleaseTableMap::clearInstancePool();
            SiteJeuxVideoComGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameReleaseTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteJeuxVideoComGameReleaseTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteJeuxVideoComGameReleaseTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteJeuxVideoComGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteJeuxVideoComGameReleaseQuery
