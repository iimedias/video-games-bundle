<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform as ChildApiGiantBombPlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery as ChildApiGiantBombPlatformQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPlatformTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_platform_vgagpl' table.
 *
 *
 *
 * @method     ChildApiGiantBombPlatformQuery orderById($order = Criteria::ASC) Order by the vgagpl_id column
 * @method     ChildApiGiantBombPlatformQuery orderByName($order = Criteria::ASC) Order by the vgagpl_name column
 * @method     ChildApiGiantBombPlatformQuery orderByAbbr($order = Criteria::ASC) Order by the vgagpl_abbr column
 * @method     ChildApiGiantBombPlatformQuery orderByAliases($order = Criteria::ASC) Order by the vgagpl_aliases column
 * @method     ChildApiGiantBombPlatformQuery orderBySummary($order = Criteria::ASC) Order by the vgagpl_summary column
 * @method     ChildApiGiantBombPlatformQuery orderByDescription($order = Criteria::ASC) Order by the vgagpl_description column
 * @method     ChildApiGiantBombPlatformQuery orderByInstallBase($order = Criteria::ASC) Order by the vgagpl_install_base column
 * @method     ChildApiGiantBombPlatformQuery orderByOnlineSupport($order = Criteria::ASC) Order by the vgagpl_online_support column
 * @method     ChildApiGiantBombPlatformQuery orderByOriginalPrice($order = Criteria::ASC) Order by the vgagpl_original_price column
 * @method     ChildApiGiantBombPlatformQuery orderByReleasedAt($order = Criteria::ASC) Order by the vgagpl_released_at column
 * @method     ChildApiGiantBombPlatformQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagpl_api_detail_url column
 * @method     ChildApiGiantBombPlatformQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagpl_site_detail_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagpl_image_icon_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagpl_image_medium_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagpl_image_screen_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagpl_image_small_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagpl_image_super_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagpl_image_thumb_url column
 * @method     ChildApiGiantBombPlatformQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagpl_image_tiny_url column
 * @method     ChildApiGiantBombPlatformQuery orderByApiGiantBombCompanyId($order = Criteria::ASC) Order by the vgagpl_vgagco_id column
 * @method     ChildApiGiantBombPlatformQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagpl_created_at column
 * @method     ChildApiGiantBombPlatformQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagpl_updated_at column
 *
 * @method     ChildApiGiantBombPlatformQuery groupById() Group by the vgagpl_id column
 * @method     ChildApiGiantBombPlatformQuery groupByName() Group by the vgagpl_name column
 * @method     ChildApiGiantBombPlatformQuery groupByAbbr() Group by the vgagpl_abbr column
 * @method     ChildApiGiantBombPlatformQuery groupByAliases() Group by the vgagpl_aliases column
 * @method     ChildApiGiantBombPlatformQuery groupBySummary() Group by the vgagpl_summary column
 * @method     ChildApiGiantBombPlatformQuery groupByDescription() Group by the vgagpl_description column
 * @method     ChildApiGiantBombPlatformQuery groupByInstallBase() Group by the vgagpl_install_base column
 * @method     ChildApiGiantBombPlatformQuery groupByOnlineSupport() Group by the vgagpl_online_support column
 * @method     ChildApiGiantBombPlatformQuery groupByOriginalPrice() Group by the vgagpl_original_price column
 * @method     ChildApiGiantBombPlatformQuery groupByReleasedAt() Group by the vgagpl_released_at column
 * @method     ChildApiGiantBombPlatformQuery groupByApiDetailUrl() Group by the vgagpl_api_detail_url column
 * @method     ChildApiGiantBombPlatformQuery groupBySiteDetailUrl() Group by the vgagpl_site_detail_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageIconUrl() Group by the vgagpl_image_icon_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageMediumUrl() Group by the vgagpl_image_medium_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageScreenUrl() Group by the vgagpl_image_screen_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageSmallUrl() Group by the vgagpl_image_small_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageSuperUrl() Group by the vgagpl_image_super_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageThumbUrl() Group by the vgagpl_image_thumb_url column
 * @method     ChildApiGiantBombPlatformQuery groupByImageTinyUrl() Group by the vgagpl_image_tiny_url column
 * @method     ChildApiGiantBombPlatformQuery groupByApiGiantBombCompanyId() Group by the vgagpl_vgagco_id column
 * @method     ChildApiGiantBombPlatformQuery groupByCreatedAt() Group by the vgagpl_created_at column
 * @method     ChildApiGiantBombPlatformQuery groupByUpdatedAt() Group by the vgagpl_updated_at column
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombPlatformQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombPlatformQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombPlatformQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombPlatformQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinApiGiantBombCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinApiGiantBombCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinApiGiantBombCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombCompany relation
 *
 * @method     ChildApiGiantBombPlatformQuery joinWithApiGiantBombCompany($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombCompany relation
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinWithApiGiantBombCompany() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinWithApiGiantBombCompany() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombCompany relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinWithApiGiantBombCompany() Adds a INNER JOIN clause and with to the query using the ApiGiantBombCompany relation
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinApiGiantBombGamePlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinApiGiantBombGamePlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinApiGiantBombGamePlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombPlatformQuery joinWithApiGiantBombGamePlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinWithApiGiantBombGamePlatform() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinWithApiGiantBombGamePlatform() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinWithApiGiantBombGamePlatform() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinApiGiantBombGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinApiGiantBombGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinApiGiantBombGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombPlatformQuery joinWithApiGiantBombGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombPlatformQuery leftJoinWithApiGiantBombGameRelease() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombPlatformQuery rightJoinWithApiGiantBombGameRelease() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombPlatformQuery innerJoinWithApiGiantBombGameRelease() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombPlatform findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombPlatform matching the query
 * @method     ChildApiGiantBombPlatform findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombPlatform matching the query, or a new ChildApiGiantBombPlatform object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombPlatform findOneById(int $vgagpl_id) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_id column
 * @method     ChildApiGiantBombPlatform findOneByName(string $vgagpl_name) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_name column
 * @method     ChildApiGiantBombPlatform findOneByAbbr(string $vgagpl_abbr) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_abbr column
 * @method     ChildApiGiantBombPlatform findOneByAliases(array $vgagpl_aliases) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_aliases column
 * @method     ChildApiGiantBombPlatform findOneBySummary(string $vgagpl_summary) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_summary column
 * @method     ChildApiGiantBombPlatform findOneByDescription(string $vgagpl_description) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_description column
 * @method     ChildApiGiantBombPlatform findOneByInstallBase(int $vgagpl_install_base) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_install_base column
 * @method     ChildApiGiantBombPlatform findOneByOnlineSupport(boolean $vgagpl_online_support) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_online_support column
 * @method     ChildApiGiantBombPlatform findOneByOriginalPrice(double $vgagpl_original_price) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_original_price column
 * @method     ChildApiGiantBombPlatform findOneByReleasedAt(string $vgagpl_released_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_released_at column
 * @method     ChildApiGiantBombPlatform findOneByApiDetailUrl(string $vgagpl_api_detail_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_api_detail_url column
 * @method     ChildApiGiantBombPlatform findOneBySiteDetailUrl(string $vgagpl_site_detail_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_site_detail_url column
 * @method     ChildApiGiantBombPlatform findOneByImageIconUrl(string $vgagpl_image_icon_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_icon_url column
 * @method     ChildApiGiantBombPlatform findOneByImageMediumUrl(string $vgagpl_image_medium_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_medium_url column
 * @method     ChildApiGiantBombPlatform findOneByImageScreenUrl(string $vgagpl_image_screen_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_screen_url column
 * @method     ChildApiGiantBombPlatform findOneByImageSmallUrl(string $vgagpl_image_small_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_small_url column
 * @method     ChildApiGiantBombPlatform findOneByImageSuperUrl(string $vgagpl_image_super_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_super_url column
 * @method     ChildApiGiantBombPlatform findOneByImageThumbUrl(string $vgagpl_image_thumb_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_thumb_url column
 * @method     ChildApiGiantBombPlatform findOneByImageTinyUrl(string $vgagpl_image_tiny_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_tiny_url column
 * @method     ChildApiGiantBombPlatform findOneByApiGiantBombCompanyId(int $vgagpl_vgagco_id) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_vgagco_id column
 * @method     ChildApiGiantBombPlatform findOneByCreatedAt(string $vgagpl_created_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_created_at column
 * @method     ChildApiGiantBombPlatform findOneByUpdatedAt(string $vgagpl_updated_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_updated_at column *

 * @method     ChildApiGiantBombPlatform requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombPlatform by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombPlatform matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombPlatform requireOneById(int $vgagpl_id) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByName(string $vgagpl_name) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByAbbr(string $vgagpl_abbr) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_abbr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByAliases(array $vgagpl_aliases) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneBySummary(string $vgagpl_summary) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByDescription(string $vgagpl_description) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByInstallBase(int $vgagpl_install_base) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_install_base column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByOnlineSupport(boolean $vgagpl_online_support) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_online_support column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByOriginalPrice(double $vgagpl_original_price) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_original_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByReleasedAt(string $vgagpl_released_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByApiDetailUrl(string $vgagpl_api_detail_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneBySiteDetailUrl(string $vgagpl_site_detail_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageIconUrl(string $vgagpl_image_icon_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageMediumUrl(string $vgagpl_image_medium_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageScreenUrl(string $vgagpl_image_screen_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageSmallUrl(string $vgagpl_image_small_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageSuperUrl(string $vgagpl_image_super_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageThumbUrl(string $vgagpl_image_thumb_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByImageTinyUrl(string $vgagpl_image_tiny_url) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByApiGiantBombCompanyId(int $vgagpl_vgagco_id) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_vgagco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByCreatedAt(string $vgagpl_created_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPlatform requireOneByUpdatedAt(string $vgagpl_updated_at) Return the first ChildApiGiantBombPlatform filtered by the vgagpl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombPlatform objects based on current ModelCriteria
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findById(int $vgagpl_id) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_id column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByName(string $vgagpl_name) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_name column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByAbbr(string $vgagpl_abbr) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_abbr column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByAliases(array $vgagpl_aliases) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_aliases column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findBySummary(string $vgagpl_summary) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_summary column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByDescription(string $vgagpl_description) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_description column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByInstallBase(int $vgagpl_install_base) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_install_base column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByOnlineSupport(boolean $vgagpl_online_support) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_online_support column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByOriginalPrice(double $vgagpl_original_price) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_original_price column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByReleasedAt(string $vgagpl_released_at) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_released_at column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByApiDetailUrl(string $vgagpl_api_detail_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_api_detail_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findBySiteDetailUrl(string $vgagpl_site_detail_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_site_detail_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageIconUrl(string $vgagpl_image_icon_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_icon_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageMediumUrl(string $vgagpl_image_medium_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_medium_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageScreenUrl(string $vgagpl_image_screen_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_screen_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageSmallUrl(string $vgagpl_image_small_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_small_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageSuperUrl(string $vgagpl_image_super_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_super_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageThumbUrl(string $vgagpl_image_thumb_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_thumb_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByImageTinyUrl(string $vgagpl_image_tiny_url) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_image_tiny_url column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByApiGiantBombCompanyId(int $vgagpl_vgagco_id) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_vgagco_id column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByCreatedAt(string $vgagpl_created_at) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_created_at column
 * @method     ChildApiGiantBombPlatform[]|ObjectCollection findByUpdatedAt(string $vgagpl_updated_at) Return ChildApiGiantBombPlatform objects filtered by the vgagpl_updated_at column
 * @method     ChildApiGiantBombPlatform[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombPlatformQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombPlatformQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPlatform', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombPlatformQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombPlatformQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombPlatformQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombPlatformQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombPlatform|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombPlatformTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombPlatform A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagpl_id, vgagpl_name, vgagpl_abbr, vgagpl_aliases, vgagpl_summary, vgagpl_description, vgagpl_install_base, vgagpl_online_support, vgagpl_original_price, vgagpl_released_at, vgagpl_api_detail_url, vgagpl_site_detail_url, vgagpl_image_icon_url, vgagpl_image_medium_url, vgagpl_image_screen_url, vgagpl_image_small_url, vgagpl_image_super_url, vgagpl_image_thumb_url, vgagpl_image_tiny_url, vgagpl_vgagco_id, vgagpl_created_at, vgagpl_updated_at FROM videogames_api_giantbomb_platform_vgagpl WHERE vgagpl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombPlatform $obj */
            $obj = new ChildApiGiantBombPlatform();
            $obj->hydrate($row);
            ApiGiantBombPlatformTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombPlatform|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagpl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagpl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagpl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagpl_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagpl_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagpl_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagpl_abbr column
     *
     * Example usage:
     * <code>
     * $query->filterByAbbr('fooValue');   // WHERE vgagpl_abbr = 'fooValue'
     * $query->filterByAbbr('%fooValue%'); // WHERE vgagpl_abbr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $abbr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByAbbr($abbr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($abbr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ABBR, $abbr, $comparison);
    }

    /**
     * Filter the query on the vgagpl_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagpl_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagpl_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagpl_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagpl_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagpl_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagpl_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagpl_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagpl_install_base column
     *
     * Example usage:
     * <code>
     * $query->filterByInstallBase(1234); // WHERE vgagpl_install_base = 1234
     * $query->filterByInstallBase(array(12, 34)); // WHERE vgagpl_install_base IN (12, 34)
     * $query->filterByInstallBase(array('min' => 12)); // WHERE vgagpl_install_base > 12
     * </code>
     *
     * @param     mixed $installBase The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByInstallBase($installBase = null, $comparison = null)
    {
        if (is_array($installBase)) {
            $useMinMax = false;
            if (isset($installBase['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE, $installBase['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($installBase['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE, $installBase['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_INSTALL_BASE, $installBase, $comparison);
    }

    /**
     * Filter the query on the vgagpl_online_support column
     *
     * Example usage:
     * <code>
     * $query->filterByOnlineSupport(true); // WHERE vgagpl_online_support = true
     * $query->filterByOnlineSupport('yes'); // WHERE vgagpl_online_support = true
     * </code>
     *
     * @param     boolean|string $onlineSupport The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByOnlineSupport($onlineSupport = null, $comparison = null)
    {
        if (is_string($onlineSupport)) {
            $onlineSupport = in_array(strtolower($onlineSupport), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ONLINE_SUPPORT, $onlineSupport, $comparison);
    }

    /**
     * Filter the query on the vgagpl_original_price column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalPrice(1234); // WHERE vgagpl_original_price = 1234
     * $query->filterByOriginalPrice(array(12, 34)); // WHERE vgagpl_original_price IN (12, 34)
     * $query->filterByOriginalPrice(array('min' => 12)); // WHERE vgagpl_original_price > 12
     * </code>
     *
     * @param     mixed $originalPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByOriginalPrice($originalPrice = null, $comparison = null)
    {
        if (is_array($originalPrice)) {
            $useMinMax = false;
            if (isset($originalPrice['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE, $originalPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originalPrice['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE, $originalPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ORIGINAL_PRICE, $originalPrice, $comparison);
    }

    /**
     * Filter the query on the vgagpl_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByReleasedAt('2011-03-14'); // WHERE vgagpl_released_at = '2011-03-14'
     * $query->filterByReleasedAt('now'); // WHERE vgagpl_released_at = '2011-03-14'
     * $query->filterByReleasedAt(array('max' => 'yesterday')); // WHERE vgagpl_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $releasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByReleasedAt($releasedAt = null, $comparison = null)
    {
        if (is_array($releasedAt)) {
            $useMinMax = false;
            if (isset($releasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT, $releasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($releasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT, $releasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_RELEASED_AT, $releasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagpl_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagpl_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagpl_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagpl_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagpl_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagpl_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagpl_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagpl_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagpl_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagpl_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagpl_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagpl_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagpl_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagpl_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagpl_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagpl_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagpl_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagpl_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagpl_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpl_vgagco_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombCompanyId(1234); // WHERE vgagpl_vgagco_id = 1234
     * $query->filterByApiGiantBombCompanyId(array(12, 34)); // WHERE vgagpl_vgagco_id IN (12, 34)
     * $query->filterByApiGiantBombCompanyId(array('min' => 12)); // WHERE vgagpl_vgagco_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombCompany()
     *
     * @param     mixed $apiGiantBombCompanyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCompanyId($apiGiantBombCompanyId = null, $comparison = null)
    {
        if (is_array($apiGiantBombCompanyId)) {
            $useMinMax = false;
            if (isset($apiGiantBombCompanyId['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $apiGiantBombCompanyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombCompanyId['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $apiGiantBombCompanyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $apiGiantBombCompanyId, $comparison);
    }

    /**
     * Filter the query on the vgagpl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagpl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagpl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagpl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagpl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagpl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagpl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagpl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany|ObjectCollection $apiGiantBombCompany The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCompany($apiGiantBombCompany, $comparison = null)
    {
        if ($apiGiantBombCompany instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany) {
            return $this
                ->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $apiGiantBombCompany->getId(), $comparison);
        } elseif ($apiGiantBombCompany instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_VGAGCO_ID, $apiGiantBombCompany->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombCompany() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompany or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombCompany relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function joinApiGiantBombCompany($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombCompany');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombCompany');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombCompany relation ApiGiantBombCompany object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombCompanyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombCompany', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombCompanyQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform|ObjectCollection $apiGiantBombGamePlatform the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGamePlatform($apiGiantBombGamePlatform, $comparison = null)
    {
        if ($apiGiantBombGamePlatform instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform) {
            return $this
                ->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $apiGiantBombGamePlatform->getApiGiantBombPlatformId(), $comparison);
        } elseif ($apiGiantBombGamePlatform instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGamePlatformQuery()
                ->filterByPrimaryKeys($apiGiantBombGamePlatform->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGamePlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGamePlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGamePlatform($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGamePlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGamePlatform');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGamePlatform relation ApiGiantBombGamePlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGamePlatformQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGamePlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGamePlatform', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease|ObjectCollection $apiGiantBombGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRelease($apiGiantBombGameRelease, $comparison = null)
    {
        if ($apiGiantBombGameRelease instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease) {
            return $this
                ->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $apiGiantBombGameRelease->getApiGiantBombPlatformId(), $comparison);
        } elseif ($apiGiantBombGameRelease instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameRelease($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameRelease');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameRelease relation ApiGiantBombGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameRelease', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombPlatform $apiGiantBombPlatform Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombPlatformQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombPlatform = null)
    {
        if ($apiGiantBombPlatform) {
            $this->addUsingAlias(ApiGiantBombPlatformTableMap::COL_VGAGPL_ID, $apiGiantBombPlatform->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_platform_vgagpl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombPlatformTableMap::clearInstancePool();
            ApiGiantBombPlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPlatformTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombPlatformTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombPlatformTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombPlatformTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombPlatformQuery
