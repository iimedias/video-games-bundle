<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameRelease as ChildSiteGameKultGameRelease;
use IiMedias\VideoGamesBundle\Model\SiteGameKultGameReleaseQuery as ChildSiteGameKultGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameKultGameReleaseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_gamekult_release_vgakrl' table.
 *
 *
 *
 * @method     ChildSiteGameKultGameReleaseQuery orderById($order = Criteria::ASC) Order by the vgakrl_id column
 * @method     ChildSiteGameKultGameReleaseQuery orderBySiteGameKultGameId($order = Criteria::ASC) Order by the vgakrl_vgakga_id column
 * @method     ChildSiteGameKultGameReleaseQuery orderBySiteGameKultPlatformId($order = Criteria::ASC) Order by the vgakrl_vgakpl_id column
 * @method     ChildSiteGameKultGameReleaseQuery orderByFrReleasedAt($order = Criteria::ASC) Order by the vgakrl_fr_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery orderByUsReleasedAt($order = Criteria::ASC) Order by the vgakrl_us_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery orderByJpReleasedAt($order = Criteria::ASC) Order by the vgakrl_jp_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgakrl_site_detail_url column
 *
 * @method     ChildSiteGameKultGameReleaseQuery groupById() Group by the vgakrl_id column
 * @method     ChildSiteGameKultGameReleaseQuery groupBySiteGameKultGameId() Group by the vgakrl_vgakga_id column
 * @method     ChildSiteGameKultGameReleaseQuery groupBySiteGameKultPlatformId() Group by the vgakrl_vgakpl_id column
 * @method     ChildSiteGameKultGameReleaseQuery groupByFrReleasedAt() Group by the vgakrl_fr_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery groupByUsReleasedAt() Group by the vgakrl_us_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery groupByJpReleasedAt() Group by the vgakrl_jp_released_at column
 * @method     ChildSiteGameKultGameReleaseQuery groupBySiteDetailUrl() Group by the vgakrl_site_detail_url column
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteGameKultGameReleaseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteGameKultGameReleaseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteGameKultGameReleaseQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteGameKultGameReleaseQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoinSiteGameKultGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameKultGame relation
 * @method     ChildSiteGameKultGameReleaseQuery rightJoinSiteGameKultGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameKultGame relation
 * @method     ChildSiteGameKultGameReleaseQuery innerJoinSiteGameKultGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameKultGame relation
 *
 * @method     ChildSiteGameKultGameReleaseQuery joinWithSiteGameKultGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameKultGame relation
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoinWithSiteGameKultGame() Adds a LEFT JOIN clause and with to the query using the SiteGameKultGame relation
 * @method     ChildSiteGameKultGameReleaseQuery rightJoinWithSiteGameKultGame() Adds a RIGHT JOIN clause and with to the query using the SiteGameKultGame relation
 * @method     ChildSiteGameKultGameReleaseQuery innerJoinWithSiteGameKultGame() Adds a INNER JOIN clause and with to the query using the SiteGameKultGame relation
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoinSiteGameKultPlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameKultPlatform relation
 * @method     ChildSiteGameKultGameReleaseQuery rightJoinSiteGameKultPlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameKultPlatform relation
 * @method     ChildSiteGameKultGameReleaseQuery innerJoinSiteGameKultPlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameKultPlatform relation
 *
 * @method     ChildSiteGameKultGameReleaseQuery joinWithSiteGameKultPlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameKultPlatform relation
 *
 * @method     ChildSiteGameKultGameReleaseQuery leftJoinWithSiteGameKultPlatform() Adds a LEFT JOIN clause and with to the query using the SiteGameKultPlatform relation
 * @method     ChildSiteGameKultGameReleaseQuery rightJoinWithSiteGameKultPlatform() Adds a RIGHT JOIN clause and with to the query using the SiteGameKultPlatform relation
 * @method     ChildSiteGameKultGameReleaseQuery innerJoinWithSiteGameKultPlatform() Adds a INNER JOIN clause and with to the query using the SiteGameKultPlatform relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameKultPlatformQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteGameKultGameRelease findOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultGameRelease matching the query
 * @method     ChildSiteGameKultGameRelease findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteGameKultGameRelease matching the query, or a new ChildSiteGameKultGameRelease object populated from the query conditions when no match is found
 *
 * @method     ChildSiteGameKultGameRelease findOneById(int $vgakrl_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_id column
 * @method     ChildSiteGameKultGameRelease findOneBySiteGameKultGameId(string $vgakrl_vgakga_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_vgakga_id column
 * @method     ChildSiteGameKultGameRelease findOneBySiteGameKultPlatformId(int $vgakrl_vgakpl_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_vgakpl_id column
 * @method     ChildSiteGameKultGameRelease findOneByFrReleasedAt(string $vgakrl_fr_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_fr_released_at column
 * @method     ChildSiteGameKultGameRelease findOneByUsReleasedAt(string $vgakrl_us_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_us_released_at column
 * @method     ChildSiteGameKultGameRelease findOneByJpReleasedAt(string $vgakrl_jp_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_jp_released_at column
 * @method     ChildSiteGameKultGameRelease findOneBySiteDetailUrl(string $vgakrl_site_detail_url) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_site_detail_url column *

 * @method     ChildSiteGameKultGameRelease requirePk($key, ConnectionInterface $con = null) Return the ChildSiteGameKultGameRelease by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOne(ConnectionInterface $con = null) Return the first ChildSiteGameKultGameRelease matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultGameRelease requireOneById(int $vgakrl_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneBySiteGameKultGameId(string $vgakrl_vgakga_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_vgakga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneBySiteGameKultPlatformId(int $vgakrl_vgakpl_id) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_vgakpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneByFrReleasedAt(string $vgakrl_fr_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_fr_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneByUsReleasedAt(string $vgakrl_us_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_us_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneByJpReleasedAt(string $vgakrl_jp_released_at) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_jp_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameKultGameRelease requireOneBySiteDetailUrl(string $vgakrl_site_detail_url) Return the first ChildSiteGameKultGameRelease filtered by the vgakrl_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteGameKultGameRelease objects based on current ModelCriteria
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findById(int $vgakrl_id) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_id column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findBySiteGameKultGameId(string $vgakrl_vgakga_id) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_vgakga_id column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findBySiteGameKultPlatformId(int $vgakrl_vgakpl_id) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_vgakpl_id column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findByFrReleasedAt(string $vgakrl_fr_released_at) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_fr_released_at column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findByUsReleasedAt(string $vgakrl_us_released_at) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_us_released_at column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findByJpReleasedAt(string $vgakrl_jp_released_at) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_jp_released_at column
 * @method     ChildSiteGameKultGameRelease[]|ObjectCollection findBySiteDetailUrl(string $vgakrl_site_detail_url) Return ChildSiteGameKultGameRelease objects filtered by the vgakrl_site_detail_url column
 * @method     ChildSiteGameKultGameRelease[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteGameKultGameReleaseQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteGameKultGameReleaseQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameKultGameRelease', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteGameKultGameReleaseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteGameKultGameReleaseQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteGameKultGameReleaseQuery) {
            return $criteria;
        }
        $query = new ChildSiteGameKultGameReleaseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteGameKultGameRelease|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteGameKultGameReleaseTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultGameRelease A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgakrl_id, vgakrl_vgakga_id, vgakrl_vgakpl_id, vgakrl_fr_released_at, vgakrl_us_released_at, vgakrl_jp_released_at, vgakrl_site_detail_url FROM videogames_site_gamekult_release_vgakrl WHERE vgakrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteGameKultGameRelease $obj */
            $obj = new ChildSiteGameKultGameRelease();
            $obj->hydrate($row);
            SiteGameKultGameReleaseTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteGameKultGameRelease|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgakrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgakrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgakrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgakrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgakrl_vgakga_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteGameKultGameId(1234); // WHERE vgakrl_vgakga_id = 1234
     * $query->filterBySiteGameKultGameId(array(12, 34)); // WHERE vgakrl_vgakga_id IN (12, 34)
     * $query->filterBySiteGameKultGameId(array('min' => 12)); // WHERE vgakrl_vgakga_id > 12
     * </code>
     *
     * @see       filterBySiteGameKultGame()
     *
     * @param     mixed $siteGameKultGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultGameId($siteGameKultGameId = null, $comparison = null)
    {
        if (is_array($siteGameKultGameId)) {
            $useMinMax = false;
            if (isset($siteGameKultGameId['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, $siteGameKultGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteGameKultGameId['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, $siteGameKultGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, $siteGameKultGameId, $comparison);
    }

    /**
     * Filter the query on the vgakrl_vgakpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteGameKultPlatformId(1234); // WHERE vgakrl_vgakpl_id = 1234
     * $query->filterBySiteGameKultPlatformId(array(12, 34)); // WHERE vgakrl_vgakpl_id IN (12, 34)
     * $query->filterBySiteGameKultPlatformId(array('min' => 12)); // WHERE vgakrl_vgakpl_id > 12
     * </code>
     *
     * @see       filterBySiteGameKultPlatform()
     *
     * @param     mixed $siteGameKultPlatformId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultPlatformId($siteGameKultPlatformId = null, $comparison = null)
    {
        if (is_array($siteGameKultPlatformId)) {
            $useMinMax = false;
            if (isset($siteGameKultPlatformId['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, $siteGameKultPlatformId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteGameKultPlatformId['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, $siteGameKultPlatformId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, $siteGameKultPlatformId, $comparison);
    }

    /**
     * Filter the query on the vgakrl_fr_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByFrReleasedAt('2011-03-14'); // WHERE vgakrl_fr_released_at = '2011-03-14'
     * $query->filterByFrReleasedAt('now'); // WHERE vgakrl_fr_released_at = '2011-03-14'
     * $query->filterByFrReleasedAt(array('max' => 'yesterday')); // WHERE vgakrl_fr_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $frReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterByFrReleasedAt($frReleasedAt = null, $comparison = null)
    {
        if (is_array($frReleasedAt)) {
            $useMinMax = false;
            if (isset($frReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT, $frReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($frReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT, $frReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_FR_RELEASED_AT, $frReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakrl_us_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUsReleasedAt('2011-03-14'); // WHERE vgakrl_us_released_at = '2011-03-14'
     * $query->filterByUsReleasedAt('now'); // WHERE vgakrl_us_released_at = '2011-03-14'
     * $query->filterByUsReleasedAt(array('max' => 'yesterday')); // WHERE vgakrl_us_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $usReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterByUsReleasedAt($usReleasedAt = null, $comparison = null)
    {
        if (is_array($usReleasedAt)) {
            $useMinMax = false;
            if (isset($usReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT, $usReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT, $usReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_US_RELEASED_AT, $usReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakrl_jp_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByJpReleasedAt('2011-03-14'); // WHERE vgakrl_jp_released_at = '2011-03-14'
     * $query->filterByJpReleasedAt('now'); // WHERE vgakrl_jp_released_at = '2011-03-14'
     * $query->filterByJpReleasedAt(array('max' => 'yesterday')); // WHERE vgakrl_jp_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $jpReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterByJpReleasedAt($jpReleasedAt = null, $comparison = null)
    {
        if (is_array($jpReleasedAt)) {
            $useMinMax = false;
            if (isset($jpReleasedAt['min'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT, $jpReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jpReleasedAt['max'])) {
                $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT, $jpReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_JP_RELEASED_AT, $jpReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgakrl_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgakrl_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgakrl_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameKultGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameKultGame|ObjectCollection $siteGameKultGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultGame($siteGameKultGame, $comparison = null)
    {
        if ($siteGameKultGame instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultGame) {
            return $this
                ->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, $siteGameKultGame->getId(), $comparison);
        } elseif ($siteGameKultGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKGA_ID, $siteGameKultGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteGameKultGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameKultGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameKultGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteGameKultGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameKultGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameKultGame');
        }

        return $this;
    }

    /**
     * Use the SiteGameKultGame relation SiteGameKultGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameKultGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameKultGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameKultGame', '\IiMedias\VideoGamesBundle\Model\SiteGameKultGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameKultPlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameKultPlatform|ObjectCollection $siteGameKultPlatform The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameKultPlatform($siteGameKultPlatform, $comparison = null)
    {
        if ($siteGameKultPlatform instanceof \IiMedias\VideoGamesBundle\Model\SiteGameKultPlatform) {
            return $this
                ->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, $siteGameKultPlatform->getId(), $comparison);
        } elseif ($siteGameKultPlatform instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_VGAKPL_ID, $siteGameKultPlatform->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteGameKultPlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameKultPlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameKultPlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteGameKultPlatform($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameKultPlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameKultPlatform');
        }

        return $this;
    }

    /**
     * Use the SiteGameKultPlatform relation SiteGameKultPlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameKultPlatformQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameKultPlatformQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameKultPlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameKultPlatform', '\IiMedias\VideoGamesBundle\Model\SiteGameKultPlatformQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteGameKultGameRelease $siteGameKultGameRelease Object to remove from the list of results
     *
     * @return $this|ChildSiteGameKultGameReleaseQuery The current query, for fluid interface
     */
    public function prune($siteGameKultGameRelease = null)
    {
        if ($siteGameKultGameRelease) {
            $this->addUsingAlias(SiteGameKultGameReleaseTableMap::COL_VGAKRL_ID, $siteGameKultGameRelease->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_gamekult_release_vgakrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteGameKultGameReleaseTableMap::clearInstancePool();
            SiteGameKultGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameKultGameReleaseTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteGameKultGameReleaseTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteGameKultGameReleaseTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteGameKultGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteGameKultGameReleaseQuery
