<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombObject as ChildApiGiantBombObject;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery as ChildApiGiantBombObjectQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombObjectTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_object_vgagob' table.
 *
 *
 *
 * @method     ChildApiGiantBombObjectQuery orderById($order = Criteria::ASC) Order by the vgagob_id column
 * @method     ChildApiGiantBombObjectQuery orderByName($order = Criteria::ASC) Order by the vgagob_name column
 * @method     ChildApiGiantBombObjectQuery orderByAliases($order = Criteria::ASC) Order by the vgagob_aliases column
 * @method     ChildApiGiantBombObjectQuery orderBySummary($order = Criteria::ASC) Order by the vgagob_summary column
 * @method     ChildApiGiantBombObjectQuery orderByDescription($order = Criteria::ASC) Order by the vgagob_description column
 * @method     ChildApiGiantBombObjectQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagob_api_detail_url column
 * @method     ChildApiGiantBombObjectQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagob_site_detail_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagob_image_icon_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagob_image_medium_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagob_image_screen_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagob_image_small_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagob_image_super_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagob_image_thumb_url column
 * @method     ChildApiGiantBombObjectQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagob_image_tiny_url column
 * @method     ChildApiGiantBombObjectQuery orderByGiantBombGameId($order = Criteria::ASC) Order by the vgagob_first_vgagga_id column
 * @method     ChildApiGiantBombObjectQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagob_created_at column
 * @method     ChildApiGiantBombObjectQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagob_updated_at column
 *
 * @method     ChildApiGiantBombObjectQuery groupById() Group by the vgagob_id column
 * @method     ChildApiGiantBombObjectQuery groupByName() Group by the vgagob_name column
 * @method     ChildApiGiantBombObjectQuery groupByAliases() Group by the vgagob_aliases column
 * @method     ChildApiGiantBombObjectQuery groupBySummary() Group by the vgagob_summary column
 * @method     ChildApiGiantBombObjectQuery groupByDescription() Group by the vgagob_description column
 * @method     ChildApiGiantBombObjectQuery groupByApiDetailUrl() Group by the vgagob_api_detail_url column
 * @method     ChildApiGiantBombObjectQuery groupBySiteDetailUrl() Group by the vgagob_site_detail_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageIconUrl() Group by the vgagob_image_icon_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageMediumUrl() Group by the vgagob_image_medium_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageScreenUrl() Group by the vgagob_image_screen_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageSmallUrl() Group by the vgagob_image_small_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageSuperUrl() Group by the vgagob_image_super_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageThumbUrl() Group by the vgagob_image_thumb_url column
 * @method     ChildApiGiantBombObjectQuery groupByImageTinyUrl() Group by the vgagob_image_tiny_url column
 * @method     ChildApiGiantBombObjectQuery groupByGiantBombGameId() Group by the vgagob_first_vgagga_id column
 * @method     ChildApiGiantBombObjectQuery groupByCreatedAt() Group by the vgagob_created_at column
 * @method     ChildApiGiantBombObjectQuery groupByUpdatedAt() Group by the vgagob_updated_at column
 *
 * @method     ChildApiGiantBombObjectQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombObjectQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombObjectQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombObjectQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombObjectQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombObjectQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombObjectQuery leftJoinApiGiantBombFirstGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombObjectQuery rightJoinApiGiantBombFirstGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombObjectQuery innerJoinApiGiantBombFirstGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombObjectQuery joinWithApiGiantBombFirstGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombObjectQuery leftJoinWithApiGiantBombFirstGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombObjectQuery rightJoinWithApiGiantBombFirstGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombObjectQuery innerJoinWithApiGiantBombFirstGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombObject findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombObject matching the query
 * @method     ChildApiGiantBombObject findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombObject matching the query, or a new ChildApiGiantBombObject object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombObject findOneById(int $vgagob_id) Return the first ChildApiGiantBombObject filtered by the vgagob_id column
 * @method     ChildApiGiantBombObject findOneByName(string $vgagob_name) Return the first ChildApiGiantBombObject filtered by the vgagob_name column
 * @method     ChildApiGiantBombObject findOneByAliases(array $vgagob_aliases) Return the first ChildApiGiantBombObject filtered by the vgagob_aliases column
 * @method     ChildApiGiantBombObject findOneBySummary(string $vgagob_summary) Return the first ChildApiGiantBombObject filtered by the vgagob_summary column
 * @method     ChildApiGiantBombObject findOneByDescription(string $vgagob_description) Return the first ChildApiGiantBombObject filtered by the vgagob_description column
 * @method     ChildApiGiantBombObject findOneByApiDetailUrl(string $vgagob_api_detail_url) Return the first ChildApiGiantBombObject filtered by the vgagob_api_detail_url column
 * @method     ChildApiGiantBombObject findOneBySiteDetailUrl(string $vgagob_site_detail_url) Return the first ChildApiGiantBombObject filtered by the vgagob_site_detail_url column
 * @method     ChildApiGiantBombObject findOneByImageIconUrl(string $vgagob_image_icon_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_icon_url column
 * @method     ChildApiGiantBombObject findOneByImageMediumUrl(string $vgagob_image_medium_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_medium_url column
 * @method     ChildApiGiantBombObject findOneByImageScreenUrl(string $vgagob_image_screen_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_screen_url column
 * @method     ChildApiGiantBombObject findOneByImageSmallUrl(string $vgagob_image_small_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_small_url column
 * @method     ChildApiGiantBombObject findOneByImageSuperUrl(string $vgagob_image_super_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_super_url column
 * @method     ChildApiGiantBombObject findOneByImageThumbUrl(string $vgagob_image_thumb_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_thumb_url column
 * @method     ChildApiGiantBombObject findOneByImageTinyUrl(string $vgagob_image_tiny_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_tiny_url column
 * @method     ChildApiGiantBombObject findOneByGiantBombGameId(int $vgagob_first_vgagga_id) Return the first ChildApiGiantBombObject filtered by the vgagob_first_vgagga_id column
 * @method     ChildApiGiantBombObject findOneByCreatedAt(string $vgagob_created_at) Return the first ChildApiGiantBombObject filtered by the vgagob_created_at column
 * @method     ChildApiGiantBombObject findOneByUpdatedAt(string $vgagob_updated_at) Return the first ChildApiGiantBombObject filtered by the vgagob_updated_at column *

 * @method     ChildApiGiantBombObject requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombObject by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombObject matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombObject requireOneById(int $vgagob_id) Return the first ChildApiGiantBombObject filtered by the vgagob_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByName(string $vgagob_name) Return the first ChildApiGiantBombObject filtered by the vgagob_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByAliases(array $vgagob_aliases) Return the first ChildApiGiantBombObject filtered by the vgagob_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneBySummary(string $vgagob_summary) Return the first ChildApiGiantBombObject filtered by the vgagob_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByDescription(string $vgagob_description) Return the first ChildApiGiantBombObject filtered by the vgagob_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByApiDetailUrl(string $vgagob_api_detail_url) Return the first ChildApiGiantBombObject filtered by the vgagob_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneBySiteDetailUrl(string $vgagob_site_detail_url) Return the first ChildApiGiantBombObject filtered by the vgagob_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageIconUrl(string $vgagob_image_icon_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageMediumUrl(string $vgagob_image_medium_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageScreenUrl(string $vgagob_image_screen_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageSmallUrl(string $vgagob_image_small_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageSuperUrl(string $vgagob_image_super_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageThumbUrl(string $vgagob_image_thumb_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByImageTinyUrl(string $vgagob_image_tiny_url) Return the first ChildApiGiantBombObject filtered by the vgagob_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByGiantBombGameId(int $vgagob_first_vgagga_id) Return the first ChildApiGiantBombObject filtered by the vgagob_first_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByCreatedAt(string $vgagob_created_at) Return the first ChildApiGiantBombObject filtered by the vgagob_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombObject requireOneByUpdatedAt(string $vgagob_updated_at) Return the first ChildApiGiantBombObject filtered by the vgagob_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombObject[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombObject objects based on current ModelCriteria
 * @method     ChildApiGiantBombObject[]|ObjectCollection findById(int $vgagob_id) Return ChildApiGiantBombObject objects filtered by the vgagob_id column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByName(string $vgagob_name) Return ChildApiGiantBombObject objects filtered by the vgagob_name column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByAliases(array $vgagob_aliases) Return ChildApiGiantBombObject objects filtered by the vgagob_aliases column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findBySummary(string $vgagob_summary) Return ChildApiGiantBombObject objects filtered by the vgagob_summary column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByDescription(string $vgagob_description) Return ChildApiGiantBombObject objects filtered by the vgagob_description column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByApiDetailUrl(string $vgagob_api_detail_url) Return ChildApiGiantBombObject objects filtered by the vgagob_api_detail_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findBySiteDetailUrl(string $vgagob_site_detail_url) Return ChildApiGiantBombObject objects filtered by the vgagob_site_detail_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageIconUrl(string $vgagob_image_icon_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_icon_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageMediumUrl(string $vgagob_image_medium_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_medium_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageScreenUrl(string $vgagob_image_screen_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_screen_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageSmallUrl(string $vgagob_image_small_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_small_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageSuperUrl(string $vgagob_image_super_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_super_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageThumbUrl(string $vgagob_image_thumb_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_thumb_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByImageTinyUrl(string $vgagob_image_tiny_url) Return ChildApiGiantBombObject objects filtered by the vgagob_image_tiny_url column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByGiantBombGameId(int $vgagob_first_vgagga_id) Return ChildApiGiantBombObject objects filtered by the vgagob_first_vgagga_id column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByCreatedAt(string $vgagob_created_at) Return ChildApiGiantBombObject objects filtered by the vgagob_created_at column
 * @method     ChildApiGiantBombObject[]|ObjectCollection findByUpdatedAt(string $vgagob_updated_at) Return ChildApiGiantBombObject objects filtered by the vgagob_updated_at column
 * @method     ChildApiGiantBombObject[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombObjectQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombObjectQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombObject', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombObjectQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombObjectQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombObjectQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombObjectQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombObject|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombObjectTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombObjectTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombObject A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagob_id, vgagob_name, vgagob_aliases, vgagob_summary, vgagob_description, vgagob_api_detail_url, vgagob_site_detail_url, vgagob_image_icon_url, vgagob_image_medium_url, vgagob_image_screen_url, vgagob_image_small_url, vgagob_image_super_url, vgagob_image_thumb_url, vgagob_image_tiny_url, vgagob_first_vgagga_id, vgagob_created_at, vgagob_updated_at FROM videogames_api_giantbomb_object_vgagob WHERE vgagob_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombObject $obj */
            $obj = new ChildApiGiantBombObject();
            $obj->hydrate($row);
            ApiGiantBombObjectTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombObject|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagob_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagob_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagob_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagob_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagob_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagob_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagob_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagob_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombObjectTableMap::COL_VGAGOB_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagob_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombObjectTableMap::COL_VGAGOB_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagob_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagob_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagob_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagob_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagob_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagob_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagob_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagob_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagob_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagob_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagob_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagob_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagob_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagob_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagob_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagob_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagob_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagob_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagob_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagob_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagob_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagob_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagob_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagob_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagob_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagob_first_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombGameId(1234); // WHERE vgagob_first_vgagga_id = 1234
     * $query->filterByGiantBombGameId(array(12, 34)); // WHERE vgagob_first_vgagga_id IN (12, 34)
     * $query->filterByGiantBombGameId(array('min' => 12)); // WHERE vgagob_first_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombFirstGame()
     *
     * @param     mixed $giantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByGiantBombGameId($giantBombGameId = null, $comparison = null)
    {
        if (is_array($giantBombGameId)) {
            $useMinMax = false;
            if (isset($giantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_FIRST_VGAGGA_ID, $giantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_FIRST_VGAGGA_ID, $giantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_FIRST_VGAGGA_ID, $giantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgagob_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagob_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagob_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagob_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagob_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagob_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagob_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagob_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_FIRST_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_FIRST_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombFirstGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombObject $apiGiantBombObject Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombObjectQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombObject = null)
    {
        if ($apiGiantBombObject) {
            $this->addUsingAlias(ApiGiantBombObjectTableMap::COL_VGAGOB_ID, $apiGiantBombObject->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_object_vgagob table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombObjectTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombObjectTableMap::clearInstancePool();
            ApiGiantBombObjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombObjectTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombObjectTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombObjectTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombObjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombObjectQuery
