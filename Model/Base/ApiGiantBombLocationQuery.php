<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation as ChildApiGiantBombLocation;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery as ChildApiGiantBombLocationQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombLocationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_location_vgaglo' table.
 *
 *
 *
 * @method     ChildApiGiantBombLocationQuery orderById($order = Criteria::ASC) Order by the vgaglo_id column
 * @method     ChildApiGiantBombLocationQuery orderByName($order = Criteria::ASC) Order by the vgaglo_name column
 * @method     ChildApiGiantBombLocationQuery orderByAliases($order = Criteria::ASC) Order by the vgaglo_aliases column
 * @method     ChildApiGiantBombLocationQuery orderBySummary($order = Criteria::ASC) Order by the vgaglo_summary column
 * @method     ChildApiGiantBombLocationQuery orderByDescription($order = Criteria::ASC) Order by the vgaglo_description column
 * @method     ChildApiGiantBombLocationQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgaglo_api_detail_url column
 * @method     ChildApiGiantBombLocationQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgaglo_site_detail_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgaglo_image_icon_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgaglo_image_medium_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgaglo_image_screen_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgaglo_image_small_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgaglo_image_super_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgaglo_image_thumb_url column
 * @method     ChildApiGiantBombLocationQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgaglo_image_tiny_url column
 * @method     ChildApiGiantBombLocationQuery orderByGiantBombGameId($order = Criteria::ASC) Order by the vgaglo_first_vgagga_id column
 * @method     ChildApiGiantBombLocationQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgaglo_created_at column
 * @method     ChildApiGiantBombLocationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgaglo_updated_at column
 *
 * @method     ChildApiGiantBombLocationQuery groupById() Group by the vgaglo_id column
 * @method     ChildApiGiantBombLocationQuery groupByName() Group by the vgaglo_name column
 * @method     ChildApiGiantBombLocationQuery groupByAliases() Group by the vgaglo_aliases column
 * @method     ChildApiGiantBombLocationQuery groupBySummary() Group by the vgaglo_summary column
 * @method     ChildApiGiantBombLocationQuery groupByDescription() Group by the vgaglo_description column
 * @method     ChildApiGiantBombLocationQuery groupByApiDetailUrl() Group by the vgaglo_api_detail_url column
 * @method     ChildApiGiantBombLocationQuery groupBySiteDetailUrl() Group by the vgaglo_site_detail_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageIconUrl() Group by the vgaglo_image_icon_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageMediumUrl() Group by the vgaglo_image_medium_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageScreenUrl() Group by the vgaglo_image_screen_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageSmallUrl() Group by the vgaglo_image_small_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageSuperUrl() Group by the vgaglo_image_super_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageThumbUrl() Group by the vgaglo_image_thumb_url column
 * @method     ChildApiGiantBombLocationQuery groupByImageTinyUrl() Group by the vgaglo_image_tiny_url column
 * @method     ChildApiGiantBombLocationQuery groupByGiantBombGameId() Group by the vgaglo_first_vgagga_id column
 * @method     ChildApiGiantBombLocationQuery groupByCreatedAt() Group by the vgaglo_created_at column
 * @method     ChildApiGiantBombLocationQuery groupByUpdatedAt() Group by the vgaglo_updated_at column
 *
 * @method     ChildApiGiantBombLocationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombLocationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombLocationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombLocationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombLocationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombLocationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombLocationQuery leftJoinApiGiantBombFirstGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombLocationQuery rightJoinApiGiantBombFirstGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombLocationQuery innerJoinApiGiantBombFirstGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombLocationQuery joinWithApiGiantBombFirstGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     ChildApiGiantBombLocationQuery leftJoinWithApiGiantBombFirstGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombLocationQuery rightJoinWithApiGiantBombFirstGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 * @method     ChildApiGiantBombLocationQuery innerJoinWithApiGiantBombFirstGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombLocation findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombLocation matching the query
 * @method     ChildApiGiantBombLocation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombLocation matching the query, or a new ChildApiGiantBombLocation object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombLocation findOneById(int $vgaglo_id) Return the first ChildApiGiantBombLocation filtered by the vgaglo_id column
 * @method     ChildApiGiantBombLocation findOneByName(string $vgaglo_name) Return the first ChildApiGiantBombLocation filtered by the vgaglo_name column
 * @method     ChildApiGiantBombLocation findOneByAliases(array $vgaglo_aliases) Return the first ChildApiGiantBombLocation filtered by the vgaglo_aliases column
 * @method     ChildApiGiantBombLocation findOneBySummary(string $vgaglo_summary) Return the first ChildApiGiantBombLocation filtered by the vgaglo_summary column
 * @method     ChildApiGiantBombLocation findOneByDescription(string $vgaglo_description) Return the first ChildApiGiantBombLocation filtered by the vgaglo_description column
 * @method     ChildApiGiantBombLocation findOneByApiDetailUrl(string $vgaglo_api_detail_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_api_detail_url column
 * @method     ChildApiGiantBombLocation findOneBySiteDetailUrl(string $vgaglo_site_detail_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_site_detail_url column
 * @method     ChildApiGiantBombLocation findOneByImageIconUrl(string $vgaglo_image_icon_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_icon_url column
 * @method     ChildApiGiantBombLocation findOneByImageMediumUrl(string $vgaglo_image_medium_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_medium_url column
 * @method     ChildApiGiantBombLocation findOneByImageScreenUrl(string $vgaglo_image_screen_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_screen_url column
 * @method     ChildApiGiantBombLocation findOneByImageSmallUrl(string $vgaglo_image_small_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_small_url column
 * @method     ChildApiGiantBombLocation findOneByImageSuperUrl(string $vgaglo_image_super_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_super_url column
 * @method     ChildApiGiantBombLocation findOneByImageThumbUrl(string $vgaglo_image_thumb_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_thumb_url column
 * @method     ChildApiGiantBombLocation findOneByImageTinyUrl(string $vgaglo_image_tiny_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_tiny_url column
 * @method     ChildApiGiantBombLocation findOneByGiantBombGameId(int $vgaglo_first_vgagga_id) Return the first ChildApiGiantBombLocation filtered by the vgaglo_first_vgagga_id column
 * @method     ChildApiGiantBombLocation findOneByCreatedAt(string $vgaglo_created_at) Return the first ChildApiGiantBombLocation filtered by the vgaglo_created_at column
 * @method     ChildApiGiantBombLocation findOneByUpdatedAt(string $vgaglo_updated_at) Return the first ChildApiGiantBombLocation filtered by the vgaglo_updated_at column *

 * @method     ChildApiGiantBombLocation requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombLocation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombLocation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombLocation requireOneById(int $vgaglo_id) Return the first ChildApiGiantBombLocation filtered by the vgaglo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByName(string $vgaglo_name) Return the first ChildApiGiantBombLocation filtered by the vgaglo_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByAliases(array $vgaglo_aliases) Return the first ChildApiGiantBombLocation filtered by the vgaglo_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneBySummary(string $vgaglo_summary) Return the first ChildApiGiantBombLocation filtered by the vgaglo_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByDescription(string $vgaglo_description) Return the first ChildApiGiantBombLocation filtered by the vgaglo_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByApiDetailUrl(string $vgaglo_api_detail_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneBySiteDetailUrl(string $vgaglo_site_detail_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageIconUrl(string $vgaglo_image_icon_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageMediumUrl(string $vgaglo_image_medium_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageScreenUrl(string $vgaglo_image_screen_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageSmallUrl(string $vgaglo_image_small_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageSuperUrl(string $vgaglo_image_super_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageThumbUrl(string $vgaglo_image_thumb_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByImageTinyUrl(string $vgaglo_image_tiny_url) Return the first ChildApiGiantBombLocation filtered by the vgaglo_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByGiantBombGameId(int $vgaglo_first_vgagga_id) Return the first ChildApiGiantBombLocation filtered by the vgaglo_first_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByCreatedAt(string $vgaglo_created_at) Return the first ChildApiGiantBombLocation filtered by the vgaglo_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombLocation requireOneByUpdatedAt(string $vgaglo_updated_at) Return the first ChildApiGiantBombLocation filtered by the vgaglo_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombLocation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombLocation objects based on current ModelCriteria
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findById(int $vgaglo_id) Return ChildApiGiantBombLocation objects filtered by the vgaglo_id column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByName(string $vgaglo_name) Return ChildApiGiantBombLocation objects filtered by the vgaglo_name column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByAliases(array $vgaglo_aliases) Return ChildApiGiantBombLocation objects filtered by the vgaglo_aliases column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findBySummary(string $vgaglo_summary) Return ChildApiGiantBombLocation objects filtered by the vgaglo_summary column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByDescription(string $vgaglo_description) Return ChildApiGiantBombLocation objects filtered by the vgaglo_description column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByApiDetailUrl(string $vgaglo_api_detail_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_api_detail_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findBySiteDetailUrl(string $vgaglo_site_detail_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_site_detail_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageIconUrl(string $vgaglo_image_icon_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_icon_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageMediumUrl(string $vgaglo_image_medium_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_medium_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageScreenUrl(string $vgaglo_image_screen_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_screen_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageSmallUrl(string $vgaglo_image_small_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_small_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageSuperUrl(string $vgaglo_image_super_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_super_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageThumbUrl(string $vgaglo_image_thumb_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_thumb_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByImageTinyUrl(string $vgaglo_image_tiny_url) Return ChildApiGiantBombLocation objects filtered by the vgaglo_image_tiny_url column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByGiantBombGameId(int $vgaglo_first_vgagga_id) Return ChildApiGiantBombLocation objects filtered by the vgaglo_first_vgagga_id column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByCreatedAt(string $vgaglo_created_at) Return ChildApiGiantBombLocation objects filtered by the vgaglo_created_at column
 * @method     ChildApiGiantBombLocation[]|ObjectCollection findByUpdatedAt(string $vgaglo_updated_at) Return ChildApiGiantBombLocation objects filtered by the vgaglo_updated_at column
 * @method     ChildApiGiantBombLocation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombLocationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombLocationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombLocation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombLocationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombLocationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombLocationQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombLocationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombLocation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombLocationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombLocationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombLocation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgaglo_id, vgaglo_name, vgaglo_aliases, vgaglo_summary, vgaglo_description, vgaglo_api_detail_url, vgaglo_site_detail_url, vgaglo_image_icon_url, vgaglo_image_medium_url, vgaglo_image_screen_url, vgaglo_image_small_url, vgaglo_image_super_url, vgaglo_image_thumb_url, vgaglo_image_tiny_url, vgaglo_first_vgagga_id, vgaglo_created_at, vgaglo_updated_at FROM videogames_api_giantbomb_location_vgaglo WHERE vgaglo_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombLocation $obj */
            $obj = new ChildApiGiantBombLocation();
            $obj->hydrate($row);
            ApiGiantBombLocationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombLocation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgaglo_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgaglo_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgaglo_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgaglo_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgaglo_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaglo_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaglo_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgaglo_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombLocationTableMap::COL_VGAGLO_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgaglo_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombLocationTableMap::COL_VGAGLO_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgaglo_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgaglo_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgaglo_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgaglo_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgaglo_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgaglo_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgaglo_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgaglo_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgaglo_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgaglo_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgaglo_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgaglo_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgaglo_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgaglo_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgaglo_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgaglo_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgaglo_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgaglo_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgaglo_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgaglo_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgaglo_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgaglo_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgaglo_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgaglo_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgaglo_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgaglo_first_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombGameId(1234); // WHERE vgaglo_first_vgagga_id = 1234
     * $query->filterByGiantBombGameId(array(12, 34)); // WHERE vgaglo_first_vgagga_id IN (12, 34)
     * $query->filterByGiantBombGameId(array('min' => 12)); // WHERE vgaglo_first_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombFirstGame()
     *
     * @param     mixed $giantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByGiantBombGameId($giantBombGameId = null, $comparison = null)
    {
        if (is_array($giantBombGameId)) {
            $useMinMax = false;
            if (isset($giantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_FIRST_VGAGGA_ID, $giantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_FIRST_VGAGGA_ID, $giantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_FIRST_VGAGGA_ID, $giantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgaglo_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgaglo_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgaglo_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgaglo_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgaglo_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgaglo_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgaglo_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgaglo_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_FIRST_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_FIRST_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombFirstGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombLocation $apiGiantBombLocation Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombLocationQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombLocation = null)
    {
        if ($apiGiantBombLocation) {
            $this->addUsingAlias(ApiGiantBombLocationTableMap::COL_VGAGLO_ID, $apiGiantBombLocation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_location_vgaglo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombLocationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombLocationTableMap::clearInstancePool();
            ApiGiantBombLocationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombLocationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombLocationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombLocationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombLocationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombLocationQuery
