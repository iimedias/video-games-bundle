<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery as ChildApiGiantBombConceptQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchise as ChildApiGiantBombFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombFranchiseQuery as ChildApiGiantBombFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombConceptTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_concept_vgagcc' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombConcept implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombConceptTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagcc_id field.
     *
     * @var        int
     */
    protected $vgagcc_id;

    /**
     * The value for the vgagcc_name field.
     *
     * @var        string
     */
    protected $vgagcc_name;

    /**
     * The value for the vgagcc_aliases field.
     *
     * @var        array
     */
    protected $vgagcc_aliases;

    /**
     * The unserialized $vgagcc_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagcc_aliases_unserialized;

    /**
     * The value for the vgagcc_summary field.
     *
     * @var        string
     */
    protected $vgagcc_summary;

    /**
     * The value for the vgagcc_description field.
     *
     * @var        string
     */
    protected $vgagcc_description;

    /**
     * The value for the vgagcc_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagcc_api_detail_url;

    /**
     * The value for the vgagcc_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagcc_site_detail_url;

    /**
     * The value for the vgagcc_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_icon_url;

    /**
     * The value for the vgagcc_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_medium_url;

    /**
     * The value for the vgagcc_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_screen_url;

    /**
     * The value for the vgagcc_image_small_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_small_url;

    /**
     * The value for the vgagcc_image_super_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_super_url;

    /**
     * The value for the vgagcc_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_thumb_url;

    /**
     * The value for the vgagcc_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagcc_image_tiny_url;

    /**
     * The value for the vgagcc_first_vgagga_id field.
     *
     * @var        int
     */
    protected $vgagcc_first_vgagga_id;

    /**
     * The value for the vgagcc_first_vgagfr_id field.
     *
     * @var        int
     */
    protected $vgagcc_first_vgagfr_id;

    /**
     * The value for the vgagcc_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagcc_created_at;

    /**
     * The value for the vgagcc_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagcc_updated_at;

    /**
     * @var        ChildApiGiantBombGame
     */
    protected $aApiGiantBombFirstGame;

    /**
     * @var        ChildApiGiantBombFranchise
     */
    protected $aApiGiantBombFirstFranchise;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombConcept object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombConcept</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombConcept</code>, delegates to
     * <code>equals(ApiGiantBombConcept)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombConcept The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagcc_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagcc_id;
    }

    /**
     * Get the [vgagcc_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagcc_name;
    }

    /**
     * Get the [vgagcc_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagcc_aliases_unserialized) {
            $this->vgagcc_aliases_unserialized = array();
        }
        if (!$this->vgagcc_aliases_unserialized && null !== $this->vgagcc_aliases) {
            $vgagcc_aliases_unserialized = substr($this->vgagcc_aliases, 2, -2);
            $this->vgagcc_aliases_unserialized = $vgagcc_aliases_unserialized ? explode(' | ', $vgagcc_aliases_unserialized) : array();
        }

        return $this->vgagcc_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagcc_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagcc_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagcc_summary;
    }

    /**
     * Get the [vgagcc_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagcc_description;
    }

    /**
     * Get the [vgagcc_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagcc_api_detail_url;
    }

    /**
     * Get the [vgagcc_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagcc_site_detail_url;
    }

    /**
     * Get the [vgagcc_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagcc_image_icon_url;
    }

    /**
     * Get the [vgagcc_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagcc_image_medium_url;
    }

    /**
     * Get the [vgagcc_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagcc_image_screen_url;
    }

    /**
     * Get the [vgagcc_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagcc_image_small_url;
    }

    /**
     * Get the [vgagcc_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagcc_image_super_url;
    }

    /**
     * Get the [vgagcc_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagcc_image_thumb_url;
    }

    /**
     * Get the [vgagcc_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagcc_image_tiny_url;
    }

    /**
     * Get the [vgagcc_first_vgagga_id] column value.
     *
     * @return int
     */
    public function getGiantBombFirstGameId()
    {
        return $this->vgagcc_first_vgagga_id;
    }

    /**
     * Get the [vgagcc_first_vgagfr_id] column value.
     *
     * @return int
     */
    public function getGiantBombFirstFranchiseId()
    {
        return $this->vgagcc_first_vgagfr_id;
    }

    /**
     * Get the [optionally formatted] temporal [vgagcc_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagcc_created_at;
        } else {
            return $this->vgagcc_created_at instanceof \DateTimeInterface ? $this->vgagcc_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagcc_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagcc_updated_at;
        } else {
            return $this->vgagcc_updated_at instanceof \DateTimeInterface ? $this->vgagcc_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagcc_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagcc_id !== $v) {
            $this->vgagcc_id = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagcc_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_name !== $v) {
            $this->vgagcc_name = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagcc_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagcc_aliases_unserialized !== $v) {
            $this->vgagcc_aliases_unserialized = $v;
            $this->vgagcc_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagcc_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagcc_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagcc_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_summary !== $v) {
            $this->vgagcc_summary = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagcc_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_description !== $v) {
            $this->vgagcc_description = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagcc_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_api_detail_url !== $v) {
            $this->vgagcc_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagcc_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_site_detail_url !== $v) {
            $this->vgagcc_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagcc_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_icon_url !== $v) {
            $this->vgagcc_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagcc_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_medium_url !== $v) {
            $this->vgagcc_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagcc_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_screen_url !== $v) {
            $this->vgagcc_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagcc_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_small_url !== $v) {
            $this->vgagcc_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagcc_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_super_url !== $v) {
            $this->vgagcc_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagcc_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_thumb_url !== $v) {
            $this->vgagcc_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagcc_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagcc_image_tiny_url !== $v) {
            $this->vgagcc_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagcc_first_vgagga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setGiantBombFirstGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagcc_first_vgagga_id !== $v) {
            $this->vgagcc_first_vgagga_id = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID] = true;
        }

        if ($this->aApiGiantBombFirstGame !== null && $this->aApiGiantBombFirstGame->getId() !== $v) {
            $this->aApiGiantBombFirstGame = null;
        }

        return $this;
    } // setGiantBombFirstGameId()

    /**
     * Set the value of [vgagcc_first_vgagfr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setGiantBombFirstFranchiseId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagcc_first_vgagfr_id !== $v) {
            $this->vgagcc_first_vgagfr_id = $v;
            $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID] = true;
        }

        if ($this->aApiGiantBombFirstFranchise !== null && $this->aApiGiantBombFirstFranchise->getId() !== $v) {
            $this->aApiGiantBombFirstFranchise = null;
        }

        return $this;
    } // setGiantBombFirstFranchiseId()

    /**
     * Sets the value of [vgagcc_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagcc_created_at !== null || $dt !== null) {
            if ($this->vgagcc_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagcc_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagcc_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagcc_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagcc_updated_at !== null || $dt !== null) {
            if ($this->vgagcc_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagcc_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagcc_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_aliases = $col;
            $this->vgagcc_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('GiantBombFirstGameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_first_vgagga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('GiantBombFirstFranchiseId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagcc_first_vgagfr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagcc_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombConceptTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagcc_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = ApiGiantBombConceptTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombConcept'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombFirstGame !== null && $this->vgagcc_first_vgagga_id !== $this->aApiGiantBombFirstGame->getId()) {
            $this->aApiGiantBombFirstGame = null;
        }
        if ($this->aApiGiantBombFirstFranchise !== null && $this->vgagcc_first_vgagfr_id !== $this->aApiGiantBombFirstFranchise->getId()) {
            $this->aApiGiantBombFirstFranchise = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombConceptQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombFirstGame = null;
            $this->aApiGiantBombFirstFranchise = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombConcept::setDeleted()
     * @see ApiGiantBombConcept::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombConceptQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombConceptTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombConceptTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombFirstGame !== null) {
                if ($this->aApiGiantBombFirstGame->isModified() || $this->aApiGiantBombFirstGame->isNew()) {
                    $affectedRows += $this->aApiGiantBombFirstGame->save($con);
                }
                $this->setApiGiantBombFirstGame($this->aApiGiantBombFirstGame);
            }

            if ($this->aApiGiantBombFirstFranchise !== null) {
                if ($this->aApiGiantBombFirstFranchise->isModified() || $this->aApiGiantBombFirstFranchise->isNew()) {
                    $affectedRows += $this->aApiGiantBombFirstFranchise->save($con);
                }
                $this->setApiGiantBombFirstFranchise($this->aApiGiantBombFirstFranchise);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_id';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_name';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_summary';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_description';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_first_vgagga_id';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_first_vgagfr_id';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagcc_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_concept_vgagcc (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagcc_id':
                        $stmt->bindValue($identifier, $this->vgagcc_id, PDO::PARAM_INT);
                        break;
                    case 'vgagcc_name':
                        $stmt->bindValue($identifier, $this->vgagcc_name, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_aliases':
                        $stmt->bindValue($identifier, $this->vgagcc_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_summary':
                        $stmt->bindValue($identifier, $this->vgagcc_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_description':
                        $stmt->bindValue($identifier, $this->vgagcc_description, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagcc_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagcc_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagcc_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_first_vgagga_id':
                        $stmt->bindValue($identifier, $this->vgagcc_first_vgagga_id, PDO::PARAM_INT);
                        break;
                    case 'vgagcc_first_vgagfr_id':
                        $stmt->bindValue($identifier, $this->vgagcc_first_vgagfr_id, PDO::PARAM_INT);
                        break;
                    case 'vgagcc_created_at':
                        $stmt->bindValue($identifier, $this->vgagcc_created_at ? $this->vgagcc_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagcc_updated_at':
                        $stmt->bindValue($identifier, $this->vgagcc_updated_at ? $this->vgagcc_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombConceptTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getAliases();
                break;
            case 3:
                return $this->getSummary();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getApiDetailUrl();
                break;
            case 6:
                return $this->getSiteDetailUrl();
                break;
            case 7:
                return $this->getImageIconUrl();
                break;
            case 8:
                return $this->getImageMediumUrl();
                break;
            case 9:
                return $this->getImageScreenUrl();
                break;
            case 10:
                return $this->getImageSmallUrl();
                break;
            case 11:
                return $this->getImageSuperUrl();
                break;
            case 12:
                return $this->getImageThumbUrl();
                break;
            case 13:
                return $this->getImageTinyUrl();
                break;
            case 14:
                return $this->getGiantBombFirstGameId();
                break;
            case 15:
                return $this->getGiantBombFirstFranchiseId();
                break;
            case 16:
                return $this->getCreatedAt();
                break;
            case 17:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombConcept'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombConcept'][$this->hashCode()] = true;
        $keys = ApiGiantBombConceptTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getAliases(),
            $keys[3] => $this->getSummary(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getApiDetailUrl(),
            $keys[6] => $this->getSiteDetailUrl(),
            $keys[7] => $this->getImageIconUrl(),
            $keys[8] => $this->getImageMediumUrl(),
            $keys[9] => $this->getImageScreenUrl(),
            $keys[10] => $this->getImageSmallUrl(),
            $keys[11] => $this->getImageSuperUrl(),
            $keys[12] => $this->getImageThumbUrl(),
            $keys[13] => $this->getImageTinyUrl(),
            $keys[14] => $this->getGiantBombFirstGameId(),
            $keys[15] => $this->getGiantBombFirstFranchiseId(),
            $keys[16] => $this->getCreatedAt(),
            $keys[17] => $this->getUpdatedAt(),
        );
        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombFirstGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGame';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_vgagga';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstGame';
                }

                $result[$key] = $this->aApiGiantBombFirstGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApiGiantBombFirstFranchise) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombFranchise';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_franchise_vgagfr';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstFranchise';
                }

                $result[$key] = $this->aApiGiantBombFirstFranchise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombConceptTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 3:
                $this->setSummary($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setApiDetailUrl($value);
                break;
            case 6:
                $this->setSiteDetailUrl($value);
                break;
            case 7:
                $this->setImageIconUrl($value);
                break;
            case 8:
                $this->setImageMediumUrl($value);
                break;
            case 9:
                $this->setImageScreenUrl($value);
                break;
            case 10:
                $this->setImageSmallUrl($value);
                break;
            case 11:
                $this->setImageSuperUrl($value);
                break;
            case 12:
                $this->setImageThumbUrl($value);
                break;
            case 13:
                $this->setImageTinyUrl($value);
                break;
            case 14:
                $this->setGiantBombFirstGameId($value);
                break;
            case 15:
                $this->setGiantBombFirstFranchiseId($value);
                break;
            case 16:
                $this->setCreatedAt($value);
                break;
            case 17:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombConceptTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAliases($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSummary($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescription($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setApiDetailUrl($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImageIconUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImageMediumUrl($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImageScreenUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setImageSmallUrl($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setImageSuperUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageThumbUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setImageTinyUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setGiantBombFirstGameId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setGiantBombFirstFranchiseId($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUpdatedAt($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombConceptTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_ID)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $this->vgagcc_id);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_NAME)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_NAME, $this->vgagcc_name);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_ALIASES, $this->vgagcc_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_SUMMARY)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_SUMMARY, $this->vgagcc_summary);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_DESCRIPTION)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_DESCRIPTION, $this->vgagcc_description);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_API_DETAIL_URL, $this->vgagcc_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_SITE_DETAIL_URL, $this->vgagcc_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_ICON_URL, $this->vgagcc_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_MEDIUM_URL, $this->vgagcc_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SCREEN_URL, $this->vgagcc_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SMALL_URL, $this->vgagcc_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_SUPER_URL, $this->vgagcc_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_THUMB_URL, $this->vgagcc_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_IMAGE_TINY_URL, $this->vgagcc_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGGA_ID, $this->vgagcc_first_vgagga_id);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_FIRST_VGAGFR_ID, $this->vgagcc_first_vgagfr_id);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_CREATED_AT, $this->vgagcc_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT)) {
            $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_UPDATED_AT, $this->vgagcc_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombConceptQuery::create();
        $criteria->add(ApiGiantBombConceptTableMap::COL_VGAGCC_ID, $this->vgagcc_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagcc_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setGiantBombFirstGameId($this->getGiantBombFirstGameId());
        $copyObj->setGiantBombFirstFranchiseId($this->getGiantBombFirstFranchiseId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGame object.
     *
     * @param  ChildApiGiantBombGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombFirstGame(ChildApiGiantBombGame $v = null)
    {
        if ($v === null) {
            $this->setGiantBombFirstGameId(NULL);
        } else {
            $this->setGiantBombFirstGameId($v->getId());
        }

        $this->aApiGiantBombFirstGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGame object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombFirstGameConcept($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGame The associated ChildApiGiantBombGame object.
     * @throws PropelException
     */
    public function getApiGiantBombFirstGame(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombFirstGame === null && ($this->vgagcc_first_vgagga_id !== null)) {
            $this->aApiGiantBombFirstGame = ChildApiGiantBombGameQuery::create()->findPk($this->vgagcc_first_vgagga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombFirstGame->addApiGiantBombFirstGameConcepts($this);
             */
        }

        return $this->aApiGiantBombFirstGame;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombFranchise object.
     *
     * @param  ChildApiGiantBombFranchise $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombFirstFranchise(ChildApiGiantBombFranchise $v = null)
    {
        if ($v === null) {
            $this->setGiantBombFirstFranchiseId(NULL);
        } else {
            $this->setGiantBombFirstFranchiseId($v->getId());
        }

        $this->aApiGiantBombFirstFranchise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombFranchise object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombFirstFranchiseConcept($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombFranchise object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombFranchise The associated ChildApiGiantBombFranchise object.
     * @throws PropelException
     */
    public function getApiGiantBombFirstFranchise(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombFirstFranchise === null && ($this->vgagcc_first_vgagfr_id !== null)) {
            $this->aApiGiantBombFirstFranchise = ChildApiGiantBombFranchiseQuery::create()->findPk($this->vgagcc_first_vgagfr_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombFirstFranchise->addApiGiantBombFirstFranchiseConcepts($this);
             */
        }

        return $this->aApiGiantBombFirstFranchise;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombFirstGame) {
            $this->aApiGiantBombFirstGame->removeApiGiantBombFirstGameConcept($this);
        }
        if (null !== $this->aApiGiantBombFirstFranchise) {
            $this->aApiGiantBombFirstFranchise->removeApiGiantBombFirstFranchiseConcept($this);
        }
        $this->vgagcc_id = null;
        $this->vgagcc_name = null;
        $this->vgagcc_aliases = null;
        $this->vgagcc_aliases_unserialized = null;
        $this->vgagcc_summary = null;
        $this->vgagcc_description = null;
        $this->vgagcc_api_detail_url = null;
        $this->vgagcc_site_detail_url = null;
        $this->vgagcc_image_icon_url = null;
        $this->vgagcc_image_medium_url = null;
        $this->vgagcc_image_screen_url = null;
        $this->vgagcc_image_small_url = null;
        $this->vgagcc_image_super_url = null;
        $this->vgagcc_image_thumb_url = null;
        $this->vgagcc_image_tiny_url = null;
        $this->vgagcc_first_vgagga_id = null;
        $this->vgagcc_first_vgagfr_id = null;
        $this->vgagcc_created_at = null;
        $this->vgagcc_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aApiGiantBombFirstGame = null;
        $this->aApiGiantBombFirstFranchise = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombConceptTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
