<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_game_vgagga' table.
 *
 *
 *
 * @method     ChildApiGiantBombGameQuery orderById($order = Criteria::ASC) Order by the vgagga_id column
 * @method     ChildApiGiantBombGameQuery orderByGameId($order = Criteria::ASC) Order by the vgagga_vgagam_id column
 * @method     ChildApiGiantBombGameQuery orderByName($order = Criteria::ASC) Order by the vgagga_name column
 * @method     ChildApiGiantBombGameQuery orderByAliases($order = Criteria::ASC) Order by the vgagga_aliases column
 * @method     ChildApiGiantBombGameQuery orderBySummary($order = Criteria::ASC) Order by the vgagga_summary column
 * @method     ChildApiGiantBombGameQuery orderByDescription($order = Criteria::ASC) Order by the vgagga_description column
 * @method     ChildApiGiantBombGameQuery orderByOriginalReleasedAt($order = Criteria::ASC) Order by the vgagga_original_released_at column
 * @method     ChildApiGiantBombGameQuery orderByExpectedDayReleasedAt($order = Criteria::ASC) Order by the vgagga_expected_day_released_at column
 * @method     ChildApiGiantBombGameQuery orderByExpectedMonthReleasedAt($order = Criteria::ASC) Order by the vgagga_expected_month_released_at column
 * @method     ChildApiGiantBombGameQuery orderByExpectedQuarterReleasedAt($order = Criteria::ASC) Order by the vgagga_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameQuery orderByExpectedYearReleasedAt($order = Criteria::ASC) Order by the vgagga_expected_year_released_at column
 * @method     ChildApiGiantBombGameQuery orderByUserReviewsCount($order = Criteria::ASC) Order by the vgagga_user_reviews_count column
 * @method     ChildApiGiantBombGameQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagga_image_icon_url column
 * @method     ChildApiGiantBombGameQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagga_image_medium_url column
 * @method     ChildApiGiantBombGameQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagga_image_screen_url column
 * @method     ChildApiGiantBombGameQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagga_image_small_url column
 * @method     ChildApiGiantBombGameQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagga_image_super_url column
 * @method     ChildApiGiantBombGameQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagga_image_thumb_url column
 * @method     ChildApiGiantBombGameQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagga_image_tiny_url column
 * @method     ChildApiGiantBombGameQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagga_api_detail_url column
 * @method     ChildApiGiantBombGameQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagga_site_detail_url column
 * @method     ChildApiGiantBombGameQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagga_created_at column
 * @method     ChildApiGiantBombGameQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagga_updated_at column
 * @method     ChildApiGiantBombGameQuery orderByRefreshedAt($order = Criteria::ASC) Order by the vgagga_refreshed_at column
 *
 * @method     ChildApiGiantBombGameQuery groupById() Group by the vgagga_id column
 * @method     ChildApiGiantBombGameQuery groupByGameId() Group by the vgagga_vgagam_id column
 * @method     ChildApiGiantBombGameQuery groupByName() Group by the vgagga_name column
 * @method     ChildApiGiantBombGameQuery groupByAliases() Group by the vgagga_aliases column
 * @method     ChildApiGiantBombGameQuery groupBySummary() Group by the vgagga_summary column
 * @method     ChildApiGiantBombGameQuery groupByDescription() Group by the vgagga_description column
 * @method     ChildApiGiantBombGameQuery groupByOriginalReleasedAt() Group by the vgagga_original_released_at column
 * @method     ChildApiGiantBombGameQuery groupByExpectedDayReleasedAt() Group by the vgagga_expected_day_released_at column
 * @method     ChildApiGiantBombGameQuery groupByExpectedMonthReleasedAt() Group by the vgagga_expected_month_released_at column
 * @method     ChildApiGiantBombGameQuery groupByExpectedQuarterReleasedAt() Group by the vgagga_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameQuery groupByExpectedYearReleasedAt() Group by the vgagga_expected_year_released_at column
 * @method     ChildApiGiantBombGameQuery groupByUserReviewsCount() Group by the vgagga_user_reviews_count column
 * @method     ChildApiGiantBombGameQuery groupByImageIconUrl() Group by the vgagga_image_icon_url column
 * @method     ChildApiGiantBombGameQuery groupByImageMediumUrl() Group by the vgagga_image_medium_url column
 * @method     ChildApiGiantBombGameQuery groupByImageScreenUrl() Group by the vgagga_image_screen_url column
 * @method     ChildApiGiantBombGameQuery groupByImageSmallUrl() Group by the vgagga_image_small_url column
 * @method     ChildApiGiantBombGameQuery groupByImageSuperUrl() Group by the vgagga_image_super_url column
 * @method     ChildApiGiantBombGameQuery groupByImageThumbUrl() Group by the vgagga_image_thumb_url column
 * @method     ChildApiGiantBombGameQuery groupByImageTinyUrl() Group by the vgagga_image_tiny_url column
 * @method     ChildApiGiantBombGameQuery groupByApiDetailUrl() Group by the vgagga_api_detail_url column
 * @method     ChildApiGiantBombGameQuery groupBySiteDetailUrl() Group by the vgagga_site_detail_url column
 * @method     ChildApiGiantBombGameQuery groupByCreatedAt() Group by the vgagga_created_at column
 * @method     ChildApiGiantBombGameQuery groupByUpdatedAt() Group by the vgagga_updated_at column
 * @method     ChildApiGiantBombGameQuery groupByRefreshedAt() Group by the vgagga_refreshed_at column
 *
 * @method     ChildApiGiantBombGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombGameQuery leftJoinGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the Game relation
 * @method     ChildApiGiantBombGameQuery rightJoinGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Game relation
 * @method     ChildApiGiantBombGameQuery innerJoinGame($relationAlias = null) Adds a INNER JOIN clause to the query using the Game relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Game relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithGame() Adds a LEFT JOIN clause and with to the query using the Game relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithGame() Adds a RIGHT JOIN clause and with to the query using the Game relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithGame() Adds a INNER JOIN clause and with to the query using the Game relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombFirstGameCharacter($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGameCharacter relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombFirstGameCharacter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGameCharacter relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombFirstGameCharacter($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGameCharacter relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombFirstGameCharacter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGameCharacter relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombFirstGameCharacter() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGameCharacter relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombFirstGameCharacter() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGameCharacter relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombFirstGameCharacter() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGameCharacter relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombFirstGameConcept($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGameConcept relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombFirstGameConcept($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGameConcept relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombFirstGameConcept($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGameConcept relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombFirstGameConcept($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGameConcept relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombFirstGameConcept() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGameConcept relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombFirstGameConcept() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGameConcept relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombFirstGameConcept() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGameConcept relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGameOriginalRating($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGameOriginalRating($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGameOriginalRating() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGameOriginalRating() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGameOriginalRating() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameOriginalRating relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGamePlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGamePlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGamePlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGamePlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGamePlatform() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGamePlatform() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGamePlatform() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGamePlatform relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGameDeveloper($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGameDeveloper($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGameDeveloper() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGameDeveloper() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGameDeveloper() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameDeveloper relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGamePublisher($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGamePublisher($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGamePublisher($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGamePublisher($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGamePublisher() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGamePublisher() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGamePublisher() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGamePublisher relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGameFranchise($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGameFranchise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGameFranchise($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGameFranchise($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGameFranchise() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGameFranchise() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGameFranchise() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameFranchise relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGameGenre($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameGenre relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGameGenre($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameGenre relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGameGenre($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameGenre relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGameGenre($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameGenre relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGameGenre() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameGenre relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGameGenre() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameGenre relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGameGenre() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameGenre relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombGameRelease() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombGameRelease() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombGameRelease() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameRelease relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombFirstGameLocation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGameLocation relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombFirstGameLocation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGameLocation relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombFirstGameLocation($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGameLocation relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombFirstGameLocation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGameLocation relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombFirstGameLocation() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGameLocation relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombFirstGameLocation() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGameLocation relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombFirstGameLocation() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGameLocation relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombFirstGameObject($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombFirstGameObject relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombFirstGameObject($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombFirstGameObject relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombFirstGameObject($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombFirstGameObject relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombFirstGameObject($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombFirstGameObject relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombFirstGameObject() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombFirstGameObject relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombFirstGameObject() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombFirstGameObject relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombFirstGameObject() Adds a INNER JOIN clause and with to the query using the ApiGiantBombFirstGameObject relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombCreditedGamePerson($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombCreditedGamePerson relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombCreditedGamePerson($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombCreditedGamePerson relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombCreditedGamePerson($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombCreditedGamePerson relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombCreditedGamePerson($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombCreditedGamePerson relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombCreditedGamePerson() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombCreditedGamePerson relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombCreditedGamePerson() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombCreditedGamePerson relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombCreditedGamePerson() Adds a INNER JOIN clause and with to the query using the ApiGiantBombCreditedGamePerson relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombImage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombImage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombImage($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombImage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombImage() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombImage() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombImage() Adds a INNER JOIN clause and with to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinApiGiantBombVideo($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameQuery rightJoinApiGiantBombVideo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameQuery innerJoinApiGiantBombVideo($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombVideo relation
 *
 * @method     ChildApiGiantBombGameQuery joinWithApiGiantBombVideo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombVideo relation
 *
 * @method     ChildApiGiantBombGameQuery leftJoinWithApiGiantBombVideo() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameQuery rightJoinWithApiGiantBombVideo() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameQuery innerJoinWithApiGiantBombVideo() Adds a INNER JOIN clause and with to the query using the ApiGiantBombVideo relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenreQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombGame findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGame matching the query
 * @method     ChildApiGiantBombGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombGame matching the query, or a new ChildApiGiantBombGame object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombGame findOneById(int $vgagga_id) Return the first ChildApiGiantBombGame filtered by the vgagga_id column
 * @method     ChildApiGiantBombGame findOneByGameId(int $vgagga_vgagam_id) Return the first ChildApiGiantBombGame filtered by the vgagga_vgagam_id column
 * @method     ChildApiGiantBombGame findOneByName(string $vgagga_name) Return the first ChildApiGiantBombGame filtered by the vgagga_name column
 * @method     ChildApiGiantBombGame findOneByAliases(array $vgagga_aliases) Return the first ChildApiGiantBombGame filtered by the vgagga_aliases column
 * @method     ChildApiGiantBombGame findOneBySummary(string $vgagga_summary) Return the first ChildApiGiantBombGame filtered by the vgagga_summary column
 * @method     ChildApiGiantBombGame findOneByDescription(string $vgagga_description) Return the first ChildApiGiantBombGame filtered by the vgagga_description column
 * @method     ChildApiGiantBombGame findOneByOriginalReleasedAt(string $vgagga_original_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_original_released_at column
 * @method     ChildApiGiantBombGame findOneByExpectedDayReleasedAt(string $vgagga_expected_day_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_day_released_at column
 * @method     ChildApiGiantBombGame findOneByExpectedMonthReleasedAt(string $vgagga_expected_month_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_month_released_at column
 * @method     ChildApiGiantBombGame findOneByExpectedQuarterReleasedAt(string $vgagga_expected_quarter_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_quarter_released_at column
 * @method     ChildApiGiantBombGame findOneByExpectedYearReleasedAt(string $vgagga_expected_year_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_year_released_at column
 * @method     ChildApiGiantBombGame findOneByUserReviewsCount(int $vgagga_user_reviews_count) Return the first ChildApiGiantBombGame filtered by the vgagga_user_reviews_count column
 * @method     ChildApiGiantBombGame findOneByImageIconUrl(string $vgagga_image_icon_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_icon_url column
 * @method     ChildApiGiantBombGame findOneByImageMediumUrl(string $vgagga_image_medium_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_medium_url column
 * @method     ChildApiGiantBombGame findOneByImageScreenUrl(string $vgagga_image_screen_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_screen_url column
 * @method     ChildApiGiantBombGame findOneByImageSmallUrl(string $vgagga_image_small_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_small_url column
 * @method     ChildApiGiantBombGame findOneByImageSuperUrl(string $vgagga_image_super_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_super_url column
 * @method     ChildApiGiantBombGame findOneByImageThumbUrl(string $vgagga_image_thumb_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_thumb_url column
 * @method     ChildApiGiantBombGame findOneByImageTinyUrl(string $vgagga_image_tiny_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_tiny_url column
 * @method     ChildApiGiantBombGame findOneByApiDetailUrl(string $vgagga_api_detail_url) Return the first ChildApiGiantBombGame filtered by the vgagga_api_detail_url column
 * @method     ChildApiGiantBombGame findOneBySiteDetailUrl(string $vgagga_site_detail_url) Return the first ChildApiGiantBombGame filtered by the vgagga_site_detail_url column
 * @method     ChildApiGiantBombGame findOneByCreatedAt(string $vgagga_created_at) Return the first ChildApiGiantBombGame filtered by the vgagga_created_at column
 * @method     ChildApiGiantBombGame findOneByUpdatedAt(string $vgagga_updated_at) Return the first ChildApiGiantBombGame filtered by the vgagga_updated_at column
 * @method     ChildApiGiantBombGame findOneByRefreshedAt(string $vgagga_refreshed_at) Return the first ChildApiGiantBombGame filtered by the vgagga_refreshed_at column *

 * @method     ChildApiGiantBombGame requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGame requireOneById(int $vgagga_id) Return the first ChildApiGiantBombGame filtered by the vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByGameId(int $vgagga_vgagam_id) Return the first ChildApiGiantBombGame filtered by the vgagga_vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByName(string $vgagga_name) Return the first ChildApiGiantBombGame filtered by the vgagga_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByAliases(array $vgagga_aliases) Return the first ChildApiGiantBombGame filtered by the vgagga_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneBySummary(string $vgagga_summary) Return the first ChildApiGiantBombGame filtered by the vgagga_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByDescription(string $vgagga_description) Return the first ChildApiGiantBombGame filtered by the vgagga_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByOriginalReleasedAt(string $vgagga_original_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_original_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByExpectedDayReleasedAt(string $vgagga_expected_day_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_day_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByExpectedMonthReleasedAt(string $vgagga_expected_month_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_month_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByExpectedQuarterReleasedAt(string $vgagga_expected_quarter_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_quarter_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByExpectedYearReleasedAt(string $vgagga_expected_year_released_at) Return the first ChildApiGiantBombGame filtered by the vgagga_expected_year_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByUserReviewsCount(int $vgagga_user_reviews_count) Return the first ChildApiGiantBombGame filtered by the vgagga_user_reviews_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageIconUrl(string $vgagga_image_icon_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageMediumUrl(string $vgagga_image_medium_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageScreenUrl(string $vgagga_image_screen_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageSmallUrl(string $vgagga_image_small_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageSuperUrl(string $vgagga_image_super_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageThumbUrl(string $vgagga_image_thumb_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByImageTinyUrl(string $vgagga_image_tiny_url) Return the first ChildApiGiantBombGame filtered by the vgagga_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByApiDetailUrl(string $vgagga_api_detail_url) Return the first ChildApiGiantBombGame filtered by the vgagga_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneBySiteDetailUrl(string $vgagga_site_detail_url) Return the first ChildApiGiantBombGame filtered by the vgagga_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByCreatedAt(string $vgagga_created_at) Return the first ChildApiGiantBombGame filtered by the vgagga_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByUpdatedAt(string $vgagga_updated_at) Return the first ChildApiGiantBombGame filtered by the vgagga_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGame requireOneByRefreshedAt(string $vgagga_refreshed_at) Return the first ChildApiGiantBombGame filtered by the vgagga_refreshed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombGame objects based on current ModelCriteria
 * @method     ChildApiGiantBombGame[]|ObjectCollection findById(int $vgagga_id) Return ChildApiGiantBombGame objects filtered by the vgagga_id column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByGameId(int $vgagga_vgagam_id) Return ChildApiGiantBombGame objects filtered by the vgagga_vgagam_id column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByName(string $vgagga_name) Return ChildApiGiantBombGame objects filtered by the vgagga_name column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByAliases(array $vgagga_aliases) Return ChildApiGiantBombGame objects filtered by the vgagga_aliases column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findBySummary(string $vgagga_summary) Return ChildApiGiantBombGame objects filtered by the vgagga_summary column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByDescription(string $vgagga_description) Return ChildApiGiantBombGame objects filtered by the vgagga_description column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByOriginalReleasedAt(string $vgagga_original_released_at) Return ChildApiGiantBombGame objects filtered by the vgagga_original_released_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByExpectedDayReleasedAt(string $vgagga_expected_day_released_at) Return ChildApiGiantBombGame objects filtered by the vgagga_expected_day_released_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByExpectedMonthReleasedAt(string $vgagga_expected_month_released_at) Return ChildApiGiantBombGame objects filtered by the vgagga_expected_month_released_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByExpectedQuarterReleasedAt(string $vgagga_expected_quarter_released_at) Return ChildApiGiantBombGame objects filtered by the vgagga_expected_quarter_released_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByExpectedYearReleasedAt(string $vgagga_expected_year_released_at) Return ChildApiGiantBombGame objects filtered by the vgagga_expected_year_released_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByUserReviewsCount(int $vgagga_user_reviews_count) Return ChildApiGiantBombGame objects filtered by the vgagga_user_reviews_count column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageIconUrl(string $vgagga_image_icon_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_icon_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageMediumUrl(string $vgagga_image_medium_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_medium_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageScreenUrl(string $vgagga_image_screen_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_screen_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageSmallUrl(string $vgagga_image_small_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_small_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageSuperUrl(string $vgagga_image_super_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_super_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageThumbUrl(string $vgagga_image_thumb_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_thumb_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByImageTinyUrl(string $vgagga_image_tiny_url) Return ChildApiGiantBombGame objects filtered by the vgagga_image_tiny_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByApiDetailUrl(string $vgagga_api_detail_url) Return ChildApiGiantBombGame objects filtered by the vgagga_api_detail_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findBySiteDetailUrl(string $vgagga_site_detail_url) Return ChildApiGiantBombGame objects filtered by the vgagga_site_detail_url column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByCreatedAt(string $vgagga_created_at) Return ChildApiGiantBombGame objects filtered by the vgagga_created_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByUpdatedAt(string $vgagga_updated_at) Return ChildApiGiantBombGame objects filtered by the vgagga_updated_at column
 * @method     ChildApiGiantBombGame[]|ObjectCollection findByRefreshedAt(string $vgagga_refreshed_at) Return ChildApiGiantBombGame objects filtered by the vgagga_refreshed_at column
 * @method     ChildApiGiantBombGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombGameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombGameQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombGameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagga_id, vgagga_vgagam_id, vgagga_name, vgagga_aliases, vgagga_summary, vgagga_description, vgagga_original_released_at, vgagga_expected_day_released_at, vgagga_expected_month_released_at, vgagga_expected_quarter_released_at, vgagga_expected_year_released_at, vgagga_user_reviews_count, vgagga_image_icon_url, vgagga_image_medium_url, vgagga_image_screen_url, vgagga_image_small_url, vgagga_image_super_url, vgagga_image_thumb_url, vgagga_image_tiny_url, vgagga_api_detail_url, vgagga_site_detail_url, vgagga_created_at, vgagga_updated_at, vgagga_refreshed_at FROM videogames_api_giantbomb_game_vgagga WHERE vgagga_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombGame $obj */
            $obj = new ChildApiGiantBombGame();
            $obj->hydrate($row);
            ApiGiantBombGameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagga_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagga_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagga_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagga_vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGameId(1234); // WHERE vgagga_vgagam_id = 1234
     * $query->filterByGameId(array(12, 34)); // WHERE vgagga_vgagam_id IN (12, 34)
     * $query->filterByGameId(array('min' => 12)); // WHERE vgagga_vgagam_id > 12
     * </code>
     *
     * @see       filterByGame()
     *
     * @param     mixed $gameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByGameId($gameId = null, $comparison = null)
    {
        if (is_array($gameId)) {
            $useMinMax = false;
            if (isset($gameId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $gameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gameId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $gameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $gameId, $comparison);
    }

    /**
     * Filter the query on the vgagga_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagga_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagga_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagga_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagga_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagga_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagga_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagga_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagga_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagga_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagga_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagga_original_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalReleasedAt('2011-03-14'); // WHERE vgagga_original_released_at = '2011-03-14'
     * $query->filterByOriginalReleasedAt('now'); // WHERE vgagga_original_released_at = '2011-03-14'
     * $query->filterByOriginalReleasedAt(array('max' => 'yesterday')); // WHERE vgagga_original_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $originalReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByOriginalReleasedAt($originalReleasedAt = null, $comparison = null)
    {
        if (is_array($originalReleasedAt)) {
            $useMinMax = false;
            if (isset($originalReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT, $originalReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originalReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT, $originalReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT, $originalReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_expected_day_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedDayReleasedAt('2011-03-14'); // WHERE vgagga_expected_day_released_at = '2011-03-14'
     * $query->filterByExpectedDayReleasedAt('now'); // WHERE vgagga_expected_day_released_at = '2011-03-14'
     * $query->filterByExpectedDayReleasedAt(array('max' => 'yesterday')); // WHERE vgagga_expected_day_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedDayReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByExpectedDayReleasedAt($expectedDayReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedDayReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedDayReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedDayReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_expected_month_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedMonthReleasedAt('2011-03-14'); // WHERE vgagga_expected_month_released_at = '2011-03-14'
     * $query->filterByExpectedMonthReleasedAt('now'); // WHERE vgagga_expected_month_released_at = '2011-03-14'
     * $query->filterByExpectedMonthReleasedAt(array('max' => 'yesterday')); // WHERE vgagga_expected_month_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedMonthReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByExpectedMonthReleasedAt($expectedMonthReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedMonthReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedMonthReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedMonthReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_expected_quarter_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedQuarterReleasedAt('2011-03-14'); // WHERE vgagga_expected_quarter_released_at = '2011-03-14'
     * $query->filterByExpectedQuarterReleasedAt('now'); // WHERE vgagga_expected_quarter_released_at = '2011-03-14'
     * $query->filterByExpectedQuarterReleasedAt(array('max' => 'yesterday')); // WHERE vgagga_expected_quarter_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedQuarterReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByExpectedQuarterReleasedAt($expectedQuarterReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedQuarterReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedQuarterReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedQuarterReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_expected_year_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedYearReleasedAt('2011-03-14'); // WHERE vgagga_expected_year_released_at = '2011-03-14'
     * $query->filterByExpectedYearReleasedAt('now'); // WHERE vgagga_expected_year_released_at = '2011-03-14'
     * $query->filterByExpectedYearReleasedAt(array('max' => 'yesterday')); // WHERE vgagga_expected_year_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedYearReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByExpectedYearReleasedAt($expectedYearReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedYearReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedYearReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedYearReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_user_reviews_count column
     *
     * Example usage:
     * <code>
     * $query->filterByUserReviewsCount(1234); // WHERE vgagga_user_reviews_count = 1234
     * $query->filterByUserReviewsCount(array(12, 34)); // WHERE vgagga_user_reviews_count IN (12, 34)
     * $query->filterByUserReviewsCount(array('min' => 12)); // WHERE vgagga_user_reviews_count > 12
     * </code>
     *
     * @param     mixed $userReviewsCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByUserReviewsCount($userReviewsCount = null, $comparison = null)
    {
        if (is_array($userReviewsCount)) {
            $useMinMax = false;
            if (isset($userReviewsCount['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT, $userReviewsCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userReviewsCount['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT, $userReviewsCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT, $userReviewsCount, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagga_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagga_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagga_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagga_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagga_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagga_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagga_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagga_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagga_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagga_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagga_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagga_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagga_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagga_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagga_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagga_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagga_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagga_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagga_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagga_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagga_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagga_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagga_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagga_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagga_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the vgagga_refreshed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRefreshedAt('2011-03-14'); // WHERE vgagga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt('now'); // WHERE vgagga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt(array('max' => 'yesterday')); // WHERE vgagga_refreshed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $refreshedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByRefreshedAt($refreshedAt = null, $comparison = null)
    {
        if (is_array($refreshedAt)) {
            $useMinMax = false;
            if (isset($refreshedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT, $refreshedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refreshedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT, $refreshedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT, $refreshedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\Game object
     *
     * @param \IiMedias\VideoGamesBundle\Model\Game|ObjectCollection $game The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByGame($game, $comparison = null)
    {
        if ($game instanceof \IiMedias\VideoGamesBundle\Model\Game) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $game->getId(), $comparison);
        } elseif ($game instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $game->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\Game or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Game relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Game');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Game');
        }

        return $this;
    }

    /**
     * Use the Game relation Game object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameQuery A secondary query class using the current class as primary query
     */
    public function useGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Game', '\IiMedias\VideoGamesBundle\Model\GameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter|ObjectCollection $apiGiantBombCharacter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGameCharacter($apiGiantBombCharacter, $comparison = null)
    {
        if ($apiGiantBombCharacter instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombCharacter->getGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombCharacter instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombFirstGameCharacterQuery()
                ->filterByPrimaryKeys($apiGiantBombCharacter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombFirstGameCharacter() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGameCharacter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGameCharacter($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGameCharacter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGameCharacter');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGameCharacter relation ApiGiantBombCharacter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameCharacterQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGameCharacter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGameCharacter', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept|ObjectCollection $apiGiantBombConcept the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGameConcept($apiGiantBombConcept, $comparison = null)
    {
        if ($apiGiantBombConcept instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombConcept->getGiantBombFirstGameId(), $comparison);
        } elseif ($apiGiantBombConcept instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombFirstGameConceptQuery()
                ->filterByPrimaryKeys($apiGiantBombConcept->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombFirstGameConcept() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGameConcept relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGameConcept($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGameConcept');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGameConcept');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGameConcept relation ApiGiantBombConcept object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameConceptQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGameConcept($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGameConcept', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating|ObjectCollection $apiGiantBombGameOriginalRating the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameOriginalRating($apiGiantBombGameOriginalRating, $comparison = null)
    {
        if ($apiGiantBombGameOriginalRating instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGameOriginalRating->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGameOriginalRating instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameOriginalRatingQuery()
                ->filterByPrimaryKeys($apiGiantBombGameOriginalRating->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameOriginalRating() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameOriginalRating relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameOriginalRating($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameOriginalRating');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameOriginalRating');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameOriginalRating relation ApiGiantBombGameOriginalRating object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameOriginalRatingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameOriginalRating($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameOriginalRating', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform|ObjectCollection $apiGiantBombGamePlatform the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGamePlatform($apiGiantBombGamePlatform, $comparison = null)
    {
        if ($apiGiantBombGamePlatform instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGamePlatform->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGamePlatform instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGamePlatformQuery()
                ->filterByPrimaryKeys($apiGiantBombGamePlatform->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGamePlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGamePlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGamePlatform($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGamePlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGamePlatform');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGamePlatform relation ApiGiantBombGamePlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGamePlatformQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGamePlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGamePlatform', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper|ObjectCollection $apiGiantBombGameDeveloper the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameDeveloper($apiGiantBombGameDeveloper, $comparison = null)
    {
        if ($apiGiantBombGameDeveloper instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGameDeveloper->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGameDeveloper instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameDeveloperQuery()
                ->filterByPrimaryKeys($apiGiantBombGameDeveloper->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameDeveloper() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameDeveloper relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameDeveloper($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameDeveloper');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameDeveloper');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameDeveloper relation ApiGiantBombGameDeveloper object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameDeveloperQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameDeveloper($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameDeveloper', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher|ObjectCollection $apiGiantBombGamePublisher the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGamePublisher($apiGiantBombGamePublisher, $comparison = null)
    {
        if ($apiGiantBombGamePublisher instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGamePublisher->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGamePublisher instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGamePublisherQuery()
                ->filterByPrimaryKeys($apiGiantBombGamePublisher->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGamePublisher() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGamePublisher relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGamePublisher($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGamePublisher');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGamePublisher');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGamePublisher relation ApiGiantBombGamePublisher object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGamePublisherQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGamePublisher($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGamePublisher', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise|ObjectCollection $apiGiantBombGameFranchise the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameFranchise($apiGiantBombGameFranchise, $comparison = null)
    {
        if ($apiGiantBombGameFranchise instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGameFranchise->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGameFranchise instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameFranchiseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameFranchise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameFranchise() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameFranchise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameFranchise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameFranchise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameFranchise');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameFranchise relation ApiGiantBombGameFranchise object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameFranchiseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameFranchise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameFranchise', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre|ObjectCollection $apiGiantBombGameGenre the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameGenre($apiGiantBombGameGenre, $comparison = null)
    {
        if ($apiGiantBombGameGenre instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGameGenre->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGameGenre instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameGenreQuery()
                ->filterByPrimaryKeys($apiGiantBombGameGenre->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameGenre() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameGenre relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameGenre($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameGenre');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameGenre');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameGenre relation ApiGiantBombGameGenre object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenreQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameGenreQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameGenre($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameGenre', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenreQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease|ObjectCollection $apiGiantBombGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRelease($apiGiantBombGameRelease, $comparison = null)
    {
        if ($apiGiantBombGameRelease instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGameRelease->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombGameRelease instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseQuery()
                ->filterByPrimaryKeys($apiGiantBombGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameRelease');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameRelease relation ApiGiantBombGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameRelease', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation|ObjectCollection $apiGiantBombLocation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGameLocation($apiGiantBombLocation, $comparison = null)
    {
        if ($apiGiantBombLocation instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombLocation->getGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombLocation instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombFirstGameLocationQuery()
                ->filterByPrimaryKeys($apiGiantBombLocation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombFirstGameLocation() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGameLocation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGameLocation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGameLocation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGameLocation');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGameLocation relation ApiGiantBombLocation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameLocationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGameLocation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGameLocation', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombObject object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombObject|ObjectCollection $apiGiantBombObject the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombFirstGameObject($apiGiantBombObject, $comparison = null)
    {
        if ($apiGiantBombObject instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombObject) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombObject->getGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombObject instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombFirstGameObjectQuery()
                ->filterByPrimaryKeys($apiGiantBombObject->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombFirstGameObject() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombObject or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombFirstGameObject relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombFirstGameObject($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombFirstGameObject');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombFirstGameObject');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombFirstGameObject relation ApiGiantBombObject object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombFirstGameObjectQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombFirstGameObject($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombFirstGameObject', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson|ObjectCollection $apiGiantBombPerson the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCreditedGamePerson($apiGiantBombPerson, $comparison = null)
    {
        if ($apiGiantBombPerson instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombPerson->getGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombPerson instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombCreditedGamePersonQuery()
                ->filterByPrimaryKeys($apiGiantBombPerson->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombCreditedGamePerson() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombCreditedGamePerson relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombCreditedGamePerson($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombCreditedGamePerson');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombCreditedGamePerson');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombCreditedGamePerson relation ApiGiantBombPerson object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombCreditedGamePersonQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombCreditedGamePerson($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombCreditedGamePerson', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage|ObjectCollection $apiGiantBombImage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombImage($apiGiantBombImage, $comparison = null)
    {
        if ($apiGiantBombImage instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombImage->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombImage instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombImageQuery()
                ->filterByPrimaryKeys($apiGiantBombImage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombImage() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombImage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombImage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombImage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombImage');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombImage relation ApiGiantBombImage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombImageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombImage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombImage', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo|ObjectCollection $apiGiantBombVideo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombVideo($apiGiantBombVideo, $comparison = null)
    {
        if ($apiGiantBombVideo instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo) {
            return $this
                ->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombVideo->getApiGiantBombGameId(), $comparison);
        } elseif ($apiGiantBombVideo instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombVideoQuery()
                ->filterByPrimaryKeys($apiGiantBombVideo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombVideo() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombVideo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function joinApiGiantBombVideo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombVideo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombVideo');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombVideo relation ApiGiantBombVideo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombVideoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombVideo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombVideo', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombGame $apiGiantBombGame Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombGameQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombGame = null)
    {
        if ($apiGiantBombGame) {
            $this->addUsingAlias(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $apiGiantBombGame->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_game_vgagga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombGameTableMap::clearInstancePool();
            ApiGiantBombGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombGameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombGameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombGameQuery
