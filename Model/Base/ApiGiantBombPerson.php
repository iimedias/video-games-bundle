<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery as ChildApiGiantBombPersonQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPersonTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_person_vgagpe' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombPerson implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombPersonTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagpe_id field.
     *
     * @var        int
     */
    protected $vgagpe_id;

    /**
     * The value for the vgagpe_name field.
     *
     * @var        string
     */
    protected $vgagpe_name;

    /**
     * The value for the vgagpe_aliases field.
     *
     * @var        array
     */
    protected $vgagpe_aliases;

    /**
     * The unserialized $vgagpe_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagpe_aliases_unserialized;

    /**
     * The value for the vgagpe_summary field.
     *
     * @var        string
     */
    protected $vgagpe_summary;

    /**
     * The value for the vgagpe_description field.
     *
     * @var        string
     */
    protected $vgagpe_description;

    /**
     * The value for the vgagpe_gender field.
     *
     * @var        int
     */
    protected $vgagpe_gender;

    /**
     * The value for the vgagpe_birthdate field.
     *
     * @var        DateTime
     */
    protected $vgagpe_birthdate;

    /**
     * The value for the vgagpe_deathdate field.
     *
     * @var        DateTime
     */
    protected $vgagpe_deathdate;

    /**
     * The value for the vgagpe_hometown field.
     *
     * @var        string
     */
    protected $vgagpe_hometown;

    /**
     * The value for the vgagpe_country field.
     *
     * @var        string
     */
    protected $vgagpe_country;

    /**
     * The value for the vgagpe_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagpe_api_detail_url;

    /**
     * The value for the vgagpe_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagpe_site_detail_url;

    /**
     * The value for the vgagpe_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_icon_url;

    /**
     * The value for the vgagpe_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_medium_url;

    /**
     * The value for the vgagpe_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_screen_url;

    /**
     * The value for the vgagpe_image_small_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_small_url;

    /**
     * The value for the vgagpe_image_super_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_super_url;

    /**
     * The value for the vgagpe_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_thumb_url;

    /**
     * The value for the vgagpe_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagpe_image_tiny_url;

    /**
     * The value for the vgagpe_credited_vgagga_id field.
     *
     * @var        int
     */
    protected $vgagpe_credited_vgagga_id;

    /**
     * The value for the vgagpe_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagpe_created_at;

    /**
     * The value for the vgagpe_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagpe_updated_at;

    /**
     * @var        ChildApiGiantBombGame
     */
    protected $aApiGiantBombCreditedGame;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombPerson object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombPerson</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombPerson</code>, delegates to
     * <code>equals(ApiGiantBombPerson)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombPerson The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagpe_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagpe_id;
    }

    /**
     * Get the [vgagpe_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagpe_name;
    }

    /**
     * Get the [vgagpe_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagpe_aliases_unserialized) {
            $this->vgagpe_aliases_unserialized = array();
        }
        if (!$this->vgagpe_aliases_unserialized && null !== $this->vgagpe_aliases) {
            $vgagpe_aliases_unserialized = substr($this->vgagpe_aliases, 2, -2);
            $this->vgagpe_aliases_unserialized = $vgagpe_aliases_unserialized ? explode(' | ', $vgagpe_aliases_unserialized) : array();
        }

        return $this->vgagpe_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagpe_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagpe_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagpe_summary;
    }

    /**
     * Get the [vgagpe_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagpe_description;
    }

    /**
     * Get the [vgagpe_gender] column value.
     *
     * @return int
     */
    public function getGender()
    {
        return $this->vgagpe_gender;
    }

    /**
     * Get the [optionally formatted] temporal [vgagpe_birthdate] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthDate($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpe_birthdate;
        } else {
            return $this->vgagpe_birthdate instanceof \DateTimeInterface ? $this->vgagpe_birthdate->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagpe_deathdate] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeathDate($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpe_deathdate;
        } else {
            return $this->vgagpe_deathdate instanceof \DateTimeInterface ? $this->vgagpe_deathdate->format($format) : null;
        }
    }

    /**
     * Get the [vgagpe_hometown] column value.
     *
     * @return string
     */
    public function getHometown()
    {
        return $this->vgagpe_hometown;
    }

    /**
     * Get the [vgagpe_country] column value.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->vgagpe_country;
    }

    /**
     * Get the [vgagpe_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagpe_api_detail_url;
    }

    /**
     * Get the [vgagpe_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagpe_site_detail_url;
    }

    /**
     * Get the [vgagpe_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagpe_image_icon_url;
    }

    /**
     * Get the [vgagpe_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagpe_image_medium_url;
    }

    /**
     * Get the [vgagpe_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagpe_image_screen_url;
    }

    /**
     * Get the [vgagpe_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagpe_image_small_url;
    }

    /**
     * Get the [vgagpe_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagpe_image_super_url;
    }

    /**
     * Get the [vgagpe_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagpe_image_thumb_url;
    }

    /**
     * Get the [vgagpe_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagpe_image_tiny_url;
    }

    /**
     * Get the [vgagpe_credited_vgagga_id] column value.
     *
     * @return int
     */
    public function getGiantBombGameId()
    {
        return $this->vgagpe_credited_vgagga_id;
    }

    /**
     * Get the [optionally formatted] temporal [vgagpe_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpe_created_at;
        } else {
            return $this->vgagpe_created_at instanceof \DateTimeInterface ? $this->vgagpe_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagpe_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagpe_updated_at;
        } else {
            return $this->vgagpe_updated_at instanceof \DateTimeInterface ? $this->vgagpe_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagpe_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpe_id !== $v) {
            $this->vgagpe_id = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagpe_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_name !== $v) {
            $this->vgagpe_name = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagpe_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagpe_aliases_unserialized !== $v) {
            $this->vgagpe_aliases_unserialized = $v;
            $this->vgagpe_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagpe_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagpe_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagpe_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_summary !== $v) {
            $this->vgagpe_summary = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagpe_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_description !== $v) {
            $this->vgagpe_description = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [vgagpe_gender] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpe_gender !== $v) {
            $this->vgagpe_gender = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER] = true;
        }

        return $this;
    } // setGender()

    /**
     * Sets the value of [vgagpe_birthdate] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setBirthDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpe_birthdate !== null || $dt !== null) {
            if ($this->vgagpe_birthdate === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpe_birthdate->format("Y-m-d H:i:s.u")) {
                $this->vgagpe_birthdate = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE] = true;
            }
        } // if either are not null

        return $this;
    } // setBirthDate()

    /**
     * Sets the value of [vgagpe_deathdate] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setDeathDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpe_deathdate !== null || $dt !== null) {
            if ($this->vgagpe_deathdate === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpe_deathdate->format("Y-m-d H:i:s.u")) {
                $this->vgagpe_deathdate = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE] = true;
            }
        } // if either are not null

        return $this;
    } // setDeathDate()

    /**
     * Set the value of [vgagpe_hometown] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setHometown($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_hometown !== $v) {
            $this->vgagpe_hometown = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN] = true;
        }

        return $this;
    } // setHometown()

    /**
     * Set the value of [vgagpe_country] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_country !== $v) {
            $this->vgagpe_country = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Set the value of [vgagpe_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_api_detail_url !== $v) {
            $this->vgagpe_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagpe_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_site_detail_url !== $v) {
            $this->vgagpe_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Set the value of [vgagpe_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_icon_url !== $v) {
            $this->vgagpe_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagpe_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_medium_url !== $v) {
            $this->vgagpe_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagpe_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_screen_url !== $v) {
            $this->vgagpe_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagpe_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_small_url !== $v) {
            $this->vgagpe_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagpe_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_super_url !== $v) {
            $this->vgagpe_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagpe_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_thumb_url !== $v) {
            $this->vgagpe_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagpe_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagpe_image_tiny_url !== $v) {
            $this->vgagpe_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagpe_credited_vgagga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setGiantBombGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagpe_credited_vgagga_id !== $v) {
            $this->vgagpe_credited_vgagga_id = $v;
            $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID] = true;
        }

        if ($this->aApiGiantBombCreditedGame !== null && $this->aApiGiantBombCreditedGame->getId() !== $v) {
            $this->aApiGiantBombCreditedGame = null;
        }

        return $this;
    } // setGiantBombGameId()

    /**
     * Sets the value of [vgagpe_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpe_created_at !== null || $dt !== null) {
            if ($this->vgagpe_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpe_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagpe_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagpe_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagpe_updated_at !== null || $dt !== null) {
            if ($this->vgagpe_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagpe_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagpe_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_aliases = $col;
            $this->vgagpe_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_gender = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('BirthDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpe_birthdate = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('DeathDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpe_deathdate = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Hometown', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_hometown = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('GiantBombGameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagpe_credited_vgagga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpe_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApiGiantBombPersonTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagpe_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = ApiGiantBombPersonTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPerson'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApiGiantBombCreditedGame !== null && $this->vgagpe_credited_vgagga_id !== $this->aApiGiantBombCreditedGame->getId()) {
            $this->aApiGiantBombCreditedGame = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombPersonQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aApiGiantBombCreditedGame = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombPerson::setDeleted()
     * @see ApiGiantBombPerson::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombPersonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombPersonTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aApiGiantBombCreditedGame !== null) {
                if ($this->aApiGiantBombCreditedGame->isModified() || $this->aApiGiantBombCreditedGame->isNew()) {
                    $affectedRows += $this->aApiGiantBombCreditedGame->save($con);
                }
                $this->setApiGiantBombCreditedGame($this->aApiGiantBombCreditedGame);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_id';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_name';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_summary';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_description';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_gender';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_birthdate';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_deathdate';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_hometown';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_country';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_credited_vgagga_id';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagpe_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_person_vgagpe (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagpe_id':
                        $stmt->bindValue($identifier, $this->vgagpe_id, PDO::PARAM_INT);
                        break;
                    case 'vgagpe_name':
                        $stmt->bindValue($identifier, $this->vgagpe_name, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_aliases':
                        $stmt->bindValue($identifier, $this->vgagpe_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_summary':
                        $stmt->bindValue($identifier, $this->vgagpe_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_description':
                        $stmt->bindValue($identifier, $this->vgagpe_description, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_gender':
                        $stmt->bindValue($identifier, $this->vgagpe_gender, PDO::PARAM_INT);
                        break;
                    case 'vgagpe_birthdate':
                        $stmt->bindValue($identifier, $this->vgagpe_birthdate ? $this->vgagpe_birthdate->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_deathdate':
                        $stmt->bindValue($identifier, $this->vgagpe_deathdate ? $this->vgagpe_deathdate->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_hometown':
                        $stmt->bindValue($identifier, $this->vgagpe_hometown, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_country':
                        $stmt->bindValue($identifier, $this->vgagpe_country, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagpe_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagpe_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagpe_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_credited_vgagga_id':
                        $stmt->bindValue($identifier, $this->vgagpe_credited_vgagga_id, PDO::PARAM_INT);
                        break;
                    case 'vgagpe_created_at':
                        $stmt->bindValue($identifier, $this->vgagpe_created_at ? $this->vgagpe_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagpe_updated_at':
                        $stmt->bindValue($identifier, $this->vgagpe_updated_at ? $this->vgagpe_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombPersonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getAliases();
                break;
            case 3:
                return $this->getSummary();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getGender();
                break;
            case 6:
                return $this->getBirthDate();
                break;
            case 7:
                return $this->getDeathDate();
                break;
            case 8:
                return $this->getHometown();
                break;
            case 9:
                return $this->getCountry();
                break;
            case 10:
                return $this->getApiDetailUrl();
                break;
            case 11:
                return $this->getSiteDetailUrl();
                break;
            case 12:
                return $this->getImageIconUrl();
                break;
            case 13:
                return $this->getImageMediumUrl();
                break;
            case 14:
                return $this->getImageScreenUrl();
                break;
            case 15:
                return $this->getImageSmallUrl();
                break;
            case 16:
                return $this->getImageSuperUrl();
                break;
            case 17:
                return $this->getImageThumbUrl();
                break;
            case 18:
                return $this->getImageTinyUrl();
                break;
            case 19:
                return $this->getGiantBombGameId();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombPerson'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombPerson'][$this->hashCode()] = true;
        $keys = ApiGiantBombPersonTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getAliases(),
            $keys[3] => $this->getSummary(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getGender(),
            $keys[6] => $this->getBirthDate(),
            $keys[7] => $this->getDeathDate(),
            $keys[8] => $this->getHometown(),
            $keys[9] => $this->getCountry(),
            $keys[10] => $this->getApiDetailUrl(),
            $keys[11] => $this->getSiteDetailUrl(),
            $keys[12] => $this->getImageIconUrl(),
            $keys[13] => $this->getImageMediumUrl(),
            $keys[14] => $this->getImageScreenUrl(),
            $keys[15] => $this->getImageSmallUrl(),
            $keys[16] => $this->getImageSuperUrl(),
            $keys[17] => $this->getImageThumbUrl(),
            $keys[18] => $this->getImageTinyUrl(),
            $keys[19] => $this->getGiantBombGameId(),
            $keys[20] => $this->getCreatedAt(),
            $keys[21] => $this->getUpdatedAt(),
        );
        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTime) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aApiGiantBombCreditedGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGame';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_vgagga';
                        break;
                    default:
                        $key = 'ApiGiantBombCreditedGame';
                }

                $result[$key] = $this->aApiGiantBombCreditedGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombPersonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 3:
                $this->setSummary($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setGender($value);
                break;
            case 6:
                $this->setBirthDate($value);
                break;
            case 7:
                $this->setDeathDate($value);
                break;
            case 8:
                $this->setHometown($value);
                break;
            case 9:
                $this->setCountry($value);
                break;
            case 10:
                $this->setApiDetailUrl($value);
                break;
            case 11:
                $this->setSiteDetailUrl($value);
                break;
            case 12:
                $this->setImageIconUrl($value);
                break;
            case 13:
                $this->setImageMediumUrl($value);
                break;
            case 14:
                $this->setImageScreenUrl($value);
                break;
            case 15:
                $this->setImageSmallUrl($value);
                break;
            case 16:
                $this->setImageSuperUrl($value);
                break;
            case 17:
                $this->setImageThumbUrl($value);
                break;
            case 18:
                $this->setImageTinyUrl($value);
                break;
            case 19:
                $this->setGiantBombGameId($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombPersonTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAliases($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSummary($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescription($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setGender($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBirthDate($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDeathDate($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHometown($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCountry($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setApiDetailUrl($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageIconUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setImageMediumUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setImageScreenUrl($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setImageSmallUrl($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setImageSuperUrl($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setImageThumbUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setImageTinyUrl($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setGiantBombGameId($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setUpdatedAt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombPersonTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_ID)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $this->vgagpe_id);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_NAME)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_NAME, $this->vgagpe_name);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES, $this->vgagpe_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY, $this->vgagpe_summary);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION, $this->vgagpe_description);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER, $this->vgagpe_gender);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE, $this->vgagpe_birthdate);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE, $this->vgagpe_deathdate);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN, $this->vgagpe_hometown);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY, $this->vgagpe_country);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL, $this->vgagpe_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL, $this->vgagpe_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL, $this->vgagpe_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL, $this->vgagpe_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL, $this->vgagpe_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL, $this->vgagpe_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL, $this->vgagpe_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL, $this->vgagpe_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL, $this->vgagpe_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $this->vgagpe_credited_vgagga_id);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT, $this->vgagpe_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT)) {
            $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT, $this->vgagpe_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombPersonQuery::create();
        $criteria->add(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $this->vgagpe_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagpe_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setGender($this->getGender());
        $copyObj->setBirthDate($this->getBirthDate());
        $copyObj->setDeathDate($this->getDeathDate());
        $copyObj->setHometown($this->getHometown());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setGiantBombGameId($this->getGiantBombGameId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildApiGiantBombGame object.
     *
     * @param  ChildApiGiantBombGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApiGiantBombCreditedGame(ChildApiGiantBombGame $v = null)
    {
        if ($v === null) {
            $this->setGiantBombGameId(NULL);
        } else {
            $this->setGiantBombGameId($v->getId());
        }

        $this->aApiGiantBombCreditedGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildApiGiantBombGame object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombCreditedGamePerson($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildApiGiantBombGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildApiGiantBombGame The associated ChildApiGiantBombGame object.
     * @throws PropelException
     */
    public function getApiGiantBombCreditedGame(ConnectionInterface $con = null)
    {
        if ($this->aApiGiantBombCreditedGame === null && ($this->vgagpe_credited_vgagga_id !== null)) {
            $this->aApiGiantBombCreditedGame = ChildApiGiantBombGameQuery::create()->findPk($this->vgagpe_credited_vgagga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApiGiantBombCreditedGame->addApiGiantBombCreditedGamepeople($this);
             */
        }

        return $this->aApiGiantBombCreditedGame;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aApiGiantBombCreditedGame) {
            $this->aApiGiantBombCreditedGame->removeApiGiantBombCreditedGamePerson($this);
        }
        $this->vgagpe_id = null;
        $this->vgagpe_name = null;
        $this->vgagpe_aliases = null;
        $this->vgagpe_aliases_unserialized = null;
        $this->vgagpe_summary = null;
        $this->vgagpe_description = null;
        $this->vgagpe_gender = null;
        $this->vgagpe_birthdate = null;
        $this->vgagpe_deathdate = null;
        $this->vgagpe_hometown = null;
        $this->vgagpe_country = null;
        $this->vgagpe_api_detail_url = null;
        $this->vgagpe_site_detail_url = null;
        $this->vgagpe_image_icon_url = null;
        $this->vgagpe_image_medium_url = null;
        $this->vgagpe_image_screen_url = null;
        $this->vgagpe_image_small_url = null;
        $this->vgagpe_image_super_url = null;
        $this->vgagpe_image_thumb_url = null;
        $this->vgagpe_image_tiny_url = null;
        $this->vgagpe_credited_vgagga_id = null;
        $this->vgagpe_created_at = null;
        $this->vgagpe_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aApiGiantBombCreditedGame = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombPersonTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
