<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_release_vgagrl' table.
 *
 *
 *
 * @method     ChildApiGiantBombGameReleaseQuery orderById($order = Criteria::ASC) Order by the vgagrl_id column
 * @method     ChildApiGiantBombGameReleaseQuery orderByApiGiantBombGameId($order = Criteria::ASC) Order by the vgagrl_vgagga_id column
 * @method     ChildApiGiantBombGameReleaseQuery orderByApiGiantBombPlatformId($order = Criteria::ASC) Order by the vgagrl_vgagpl_id column
 * @method     ChildApiGiantBombGameReleaseQuery orderByApiGiantBombGameRatingId($order = Criteria::ASC) Order by the vgagrl_vgaggr_id column
 * @method     ChildApiGiantBombGameReleaseQuery orderByApiGiantBombRegionId($order = Criteria::ASC) Order by the vgagrl_vgagre_id column
 * @method     ChildApiGiantBombGameReleaseQuery orderByName($order = Criteria::ASC) Order by the vgaggl_name column
 * @method     ChildApiGiantBombGameReleaseQuery orderBySummary($order = Criteria::ASC) Order by the vgagrl_summary column
 * @method     ChildApiGiantBombGameReleaseQuery orderByDescription($order = Criteria::ASC) Order by the vgagrl_description column
 * @method     ChildApiGiantBombGameReleaseQuery orderByMinimumPlayers($order = Criteria::ASC) Order by the vgagrl_minimum_players column
 * @method     ChildApiGiantBombGameReleaseQuery orderByMaximumPlayers($order = Criteria::ASC) Order by the vgagrl_maximum_players column
 * @method     ChildApiGiantBombGameReleaseQuery orderByWidescreenSupport($order = Criteria::ASC) Order by the vgagrl_widescreen_support column
 * @method     ChildApiGiantBombGameReleaseQuery orderByProductCodeValue($order = Criteria::ASC) Order by the vgagrl_product_code_value column
 * @method     ChildApiGiantBombGameReleaseQuery orderByOriginalReleasedAt($order = Criteria::ASC) Order by the vgagrl_original_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByExpectedDayReleasedAt($order = Criteria::ASC) Order by the vgagrl_expected_day_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByExpectedMonthReleasedAt($order = Criteria::ASC) Order by the vgagrl_expected_month_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByExpectedQuarterReleasedAt($order = Criteria::ASC) Order by the vgagrl_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByExpectedYearReleasedAt($order = Criteria::ASC) Order by the vgagrl_expected_year_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagrl_image_icon_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagrl_image_medium_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagrl_image_screen_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagrl_image_small_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagrl_image_super_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagrl_image_thumb_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagrl_image_tiny_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagrl_api_detail_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagrl_site_detail_url column
 * @method     ChildApiGiantBombGameReleaseQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagrl_created_at column
 * @method     ChildApiGiantBombGameReleaseQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagrl_updated_at column
 *
 * @method     ChildApiGiantBombGameReleaseQuery groupById() Group by the vgagrl_id column
 * @method     ChildApiGiantBombGameReleaseQuery groupByApiGiantBombGameId() Group by the vgagrl_vgagga_id column
 * @method     ChildApiGiantBombGameReleaseQuery groupByApiGiantBombPlatformId() Group by the vgagrl_vgagpl_id column
 * @method     ChildApiGiantBombGameReleaseQuery groupByApiGiantBombGameRatingId() Group by the vgagrl_vgaggr_id column
 * @method     ChildApiGiantBombGameReleaseQuery groupByApiGiantBombRegionId() Group by the vgagrl_vgagre_id column
 * @method     ChildApiGiantBombGameReleaseQuery groupByName() Group by the vgaggl_name column
 * @method     ChildApiGiantBombGameReleaseQuery groupBySummary() Group by the vgagrl_summary column
 * @method     ChildApiGiantBombGameReleaseQuery groupByDescription() Group by the vgagrl_description column
 * @method     ChildApiGiantBombGameReleaseQuery groupByMinimumPlayers() Group by the vgagrl_minimum_players column
 * @method     ChildApiGiantBombGameReleaseQuery groupByMaximumPlayers() Group by the vgagrl_maximum_players column
 * @method     ChildApiGiantBombGameReleaseQuery groupByWidescreenSupport() Group by the vgagrl_widescreen_support column
 * @method     ChildApiGiantBombGameReleaseQuery groupByProductCodeValue() Group by the vgagrl_product_code_value column
 * @method     ChildApiGiantBombGameReleaseQuery groupByOriginalReleasedAt() Group by the vgagrl_original_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByExpectedDayReleasedAt() Group by the vgagrl_expected_day_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByExpectedMonthReleasedAt() Group by the vgagrl_expected_month_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByExpectedQuarterReleasedAt() Group by the vgagrl_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByExpectedYearReleasedAt() Group by the vgagrl_expected_year_released_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageIconUrl() Group by the vgagrl_image_icon_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageMediumUrl() Group by the vgagrl_image_medium_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageScreenUrl() Group by the vgagrl_image_screen_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageSmallUrl() Group by the vgagrl_image_small_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageSuperUrl() Group by the vgagrl_image_super_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageThumbUrl() Group by the vgagrl_image_thumb_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByImageTinyUrl() Group by the vgagrl_image_tiny_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByApiDetailUrl() Group by the vgagrl_api_detail_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupBySiteDetailUrl() Group by the vgagrl_site_detail_url column
 * @method     ChildApiGiantBombGameReleaseQuery groupByCreatedAt() Group by the vgagrl_created_at column
 * @method     ChildApiGiantBombGameReleaseQuery groupByUpdatedAt() Group by the vgagrl_updated_at column
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombGameReleaseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombGameReleaseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGame relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGame relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombPlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombPlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombPlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombPlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombPlatform() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombPlatform() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombPlatform relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombPlatform() Adds a INNER JOIN clause and with to the query using the ApiGiantBombPlatform relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombGameRating($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameRating relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombGameRating($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameRating relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombGameRating($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameRating relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombGameRating($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameRating relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombGameRating() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameRating relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombGameRating() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameRating relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombGameRating() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameRating relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombRegion($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombRegion relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombRegion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombRegion relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombRegion($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombRegion relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombRegion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombRegion relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombRegion() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombRegion relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombRegion() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombRegion relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombRegion() Adds a INNER JOIN clause and with to the query using the ApiGiantBombRegion relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombGameReleaseDeveloper($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombGameReleaseDeveloper($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombGameReleaseDeveloper() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombGameReleaseDeveloper() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombGameReleaseDeveloper() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameReleaseDeveloper relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombGameReleasePublisher($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombGameReleasePublisher($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombGameReleasePublisher() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombGameReleasePublisher() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombGameReleasePublisher() Adds a INNER JOIN clause and with to the query using the ApiGiantBombGameReleasePublisher relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombImage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombImage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombImage($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombImage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombImage() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombImage() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombImage relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombImage() Adds a INNER JOIN clause and with to the query using the ApiGiantBombImage relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinApiGiantBombVideo($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinApiGiantBombVideo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinApiGiantBombVideo($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombVideo relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery joinWithApiGiantBombVideo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombVideo relation
 *
 * @method     ChildApiGiantBombGameReleaseQuery leftJoinWithApiGiantBombVideo() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameReleaseQuery rightJoinWithApiGiantBombVideo() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombVideo relation
 * @method     ChildApiGiantBombGameReleaseQuery innerJoinWithApiGiantBombVideo() Adds a INNER JOIN clause and with to the query using the ApiGiantBombVideo relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery|\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombGameRelease findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRelease matching the query
 * @method     ChildApiGiantBombGameRelease findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRelease matching the query, or a new ChildApiGiantBombGameRelease object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombGameRelease findOneById(int $vgagrl_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_id column
 * @method     ChildApiGiantBombGameRelease findOneByApiGiantBombGameId(int $vgagrl_vgagga_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagga_id column
 * @method     ChildApiGiantBombGameRelease findOneByApiGiantBombPlatformId(int $vgagrl_vgagpl_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagpl_id column
 * @method     ChildApiGiantBombGameRelease findOneByApiGiantBombGameRatingId(int $vgagrl_vgaggr_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgaggr_id column
 * @method     ChildApiGiantBombGameRelease findOneByApiGiantBombRegionId(int $vgagrl_vgagre_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagre_id column
 * @method     ChildApiGiantBombGameRelease findOneByName(string $vgaggl_name) Return the first ChildApiGiantBombGameRelease filtered by the vgaggl_name column
 * @method     ChildApiGiantBombGameRelease findOneBySummary(string $vgagrl_summary) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_summary column
 * @method     ChildApiGiantBombGameRelease findOneByDescription(string $vgagrl_description) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_description column
 * @method     ChildApiGiantBombGameRelease findOneByMinimumPlayers(int $vgagrl_minimum_players) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_minimum_players column
 * @method     ChildApiGiantBombGameRelease findOneByMaximumPlayers(int $vgagrl_maximum_players) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_maximum_players column
 * @method     ChildApiGiantBombGameRelease findOneByWidescreenSupport(boolean $vgagrl_widescreen_support) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_widescreen_support column
 * @method     ChildApiGiantBombGameRelease findOneByProductCodeValue(string $vgagrl_product_code_value) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_product_code_value column
 * @method     ChildApiGiantBombGameRelease findOneByOriginalReleasedAt(string $vgagrl_original_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_original_released_at column
 * @method     ChildApiGiantBombGameRelease findOneByExpectedDayReleasedAt(string $vgagrl_expected_day_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_day_released_at column
 * @method     ChildApiGiantBombGameRelease findOneByExpectedMonthReleasedAt(string $vgagrl_expected_month_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_month_released_at column
 * @method     ChildApiGiantBombGameRelease findOneByExpectedQuarterReleasedAt(string $vgagrl_expected_quarter_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameRelease findOneByExpectedYearReleasedAt(string $vgagrl_expected_year_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_year_released_at column
 * @method     ChildApiGiantBombGameRelease findOneByImageIconUrl(string $vgagrl_image_icon_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_icon_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageMediumUrl(string $vgagrl_image_medium_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_medium_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageScreenUrl(string $vgagrl_image_screen_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_screen_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageSmallUrl(string $vgagrl_image_small_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_small_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageSuperUrl(string $vgagrl_image_super_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_super_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageThumbUrl(string $vgagrl_image_thumb_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_thumb_url column
 * @method     ChildApiGiantBombGameRelease findOneByImageTinyUrl(string $vgagrl_image_tiny_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_tiny_url column
 * @method     ChildApiGiantBombGameRelease findOneByApiDetailUrl(string $vgagrl_api_detail_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_api_detail_url column
 * @method     ChildApiGiantBombGameRelease findOneBySiteDetailUrl(string $vgagrl_site_detail_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_site_detail_url column
 * @method     ChildApiGiantBombGameRelease findOneByCreatedAt(string $vgagrl_created_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_created_at column
 * @method     ChildApiGiantBombGameRelease findOneByUpdatedAt(string $vgagrl_updated_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_updated_at column *

 * @method     ChildApiGiantBombGameRelease requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombGameRelease by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombGameRelease matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameRelease requireOneById(int $vgagrl_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByApiGiantBombGameId(int $vgagrl_vgagga_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByApiGiantBombPlatformId(int $vgagrl_vgagpl_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByApiGiantBombGameRatingId(int $vgagrl_vgaggr_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgaggr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByApiGiantBombRegionId(int $vgagrl_vgagre_id) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_vgagre_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByName(string $vgaggl_name) Return the first ChildApiGiantBombGameRelease filtered by the vgaggl_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneBySummary(string $vgagrl_summary) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByDescription(string $vgagrl_description) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByMinimumPlayers(int $vgagrl_minimum_players) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_minimum_players column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByMaximumPlayers(int $vgagrl_maximum_players) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_maximum_players column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByWidescreenSupport(boolean $vgagrl_widescreen_support) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_widescreen_support column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByProductCodeValue(string $vgagrl_product_code_value) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_product_code_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByOriginalReleasedAt(string $vgagrl_original_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_original_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByExpectedDayReleasedAt(string $vgagrl_expected_day_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_day_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByExpectedMonthReleasedAt(string $vgagrl_expected_month_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_month_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByExpectedQuarterReleasedAt(string $vgagrl_expected_quarter_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_quarter_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByExpectedYearReleasedAt(string $vgagrl_expected_year_released_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_expected_year_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageIconUrl(string $vgagrl_image_icon_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageMediumUrl(string $vgagrl_image_medium_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageScreenUrl(string $vgagrl_image_screen_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageSmallUrl(string $vgagrl_image_small_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageSuperUrl(string $vgagrl_image_super_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageThumbUrl(string $vgagrl_image_thumb_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByImageTinyUrl(string $vgagrl_image_tiny_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByApiDetailUrl(string $vgagrl_api_detail_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneBySiteDetailUrl(string $vgagrl_site_detail_url) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByCreatedAt(string $vgagrl_created_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombGameRelease requireOneByUpdatedAt(string $vgagrl_updated_at) Return the first ChildApiGiantBombGameRelease filtered by the vgagrl_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombGameRelease objects based on current ModelCriteria
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findById(int $vgagrl_id) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_id column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByApiGiantBombGameId(int $vgagrl_vgagga_id) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_vgagga_id column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByApiGiantBombPlatformId(int $vgagrl_vgagpl_id) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_vgagpl_id column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByApiGiantBombGameRatingId(int $vgagrl_vgaggr_id) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_vgaggr_id column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByApiGiantBombRegionId(int $vgagrl_vgagre_id) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_vgagre_id column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByName(string $vgaggl_name) Return ChildApiGiantBombGameRelease objects filtered by the vgaggl_name column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findBySummary(string $vgagrl_summary) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_summary column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByDescription(string $vgagrl_description) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_description column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByMinimumPlayers(int $vgagrl_minimum_players) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_minimum_players column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByMaximumPlayers(int $vgagrl_maximum_players) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_maximum_players column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByWidescreenSupport(boolean $vgagrl_widescreen_support) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_widescreen_support column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByProductCodeValue(string $vgagrl_product_code_value) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_product_code_value column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByOriginalReleasedAt(string $vgagrl_original_released_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_original_released_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByExpectedDayReleasedAt(string $vgagrl_expected_day_released_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_expected_day_released_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByExpectedMonthReleasedAt(string $vgagrl_expected_month_released_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_expected_month_released_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByExpectedQuarterReleasedAt(string $vgagrl_expected_quarter_released_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_expected_quarter_released_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByExpectedYearReleasedAt(string $vgagrl_expected_year_released_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_expected_year_released_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageIconUrl(string $vgagrl_image_icon_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_icon_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageMediumUrl(string $vgagrl_image_medium_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_medium_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageScreenUrl(string $vgagrl_image_screen_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_screen_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageSmallUrl(string $vgagrl_image_small_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_small_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageSuperUrl(string $vgagrl_image_super_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_super_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageThumbUrl(string $vgagrl_image_thumb_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_thumb_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByImageTinyUrl(string $vgagrl_image_tiny_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_image_tiny_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByApiDetailUrl(string $vgagrl_api_detail_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_api_detail_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findBySiteDetailUrl(string $vgagrl_site_detail_url) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_site_detail_url column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByCreatedAt(string $vgagrl_created_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_created_at column
 * @method     ChildApiGiantBombGameRelease[]|ObjectCollection findByUpdatedAt(string $vgagrl_updated_at) Return ChildApiGiantBombGameRelease objects filtered by the vgagrl_updated_at column
 * @method     ChildApiGiantBombGameRelease[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombGameReleaseQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGameReleaseQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGameRelease', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombGameReleaseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombGameReleaseQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombGameReleaseQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombGameReleaseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombGameRelease|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombGameReleaseTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameRelease A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagrl_id, vgagrl_vgagga_id, vgagrl_vgagpl_id, vgagrl_vgaggr_id, vgagrl_vgagre_id, vgaggl_name, vgagrl_summary, vgagrl_description, vgagrl_minimum_players, vgagrl_maximum_players, vgagrl_widescreen_support, vgagrl_product_code_value, vgagrl_original_released_at, vgagrl_expected_day_released_at, vgagrl_expected_month_released_at, vgagrl_expected_quarter_released_at, vgagrl_expected_year_released_at, vgagrl_image_icon_url, vgagrl_image_medium_url, vgagrl_image_screen_url, vgagrl_image_small_url, vgagrl_image_super_url, vgagrl_image_thumb_url, vgagrl_image_tiny_url, vgagrl_api_detail_url, vgagrl_site_detail_url, vgagrl_created_at, vgagrl_updated_at FROM videogames_api_giantbomb_release_vgagrl WHERE vgagrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombGameRelease $obj */
            $obj = new ChildApiGiantBombGameRelease();
            $obj->hydrate($row);
            ApiGiantBombGameReleaseTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombGameRelease|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagrl_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombGameId(1234); // WHERE vgagrl_vgagga_id = 1234
     * $query->filterByApiGiantBombGameId(array(12, 34)); // WHERE vgagrl_vgagga_id IN (12, 34)
     * $query->filterByApiGiantBombGameId(array('min' => 12)); // WHERE vgagrl_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombGame()
     *
     * @param     mixed $apiGiantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameId($apiGiantBombGameId = null, $comparison = null)
    {
        if (is_array($apiGiantBombGameId)) {
            $useMinMax = false;
            if (isset($apiGiantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $apiGiantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $apiGiantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $apiGiantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgagrl_vgagpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombPlatformId(1234); // WHERE vgagrl_vgagpl_id = 1234
     * $query->filterByApiGiantBombPlatformId(array(12, 34)); // WHERE vgagrl_vgagpl_id IN (12, 34)
     * $query->filterByApiGiantBombPlatformId(array('min' => 12)); // WHERE vgagrl_vgagpl_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombPlatform()
     *
     * @param     mixed $apiGiantBombPlatformId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombPlatformId($apiGiantBombPlatformId = null, $comparison = null)
    {
        if (is_array($apiGiantBombPlatformId)) {
            $useMinMax = false;
            if (isset($apiGiantBombPlatformId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $apiGiantBombPlatformId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombPlatformId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $apiGiantBombPlatformId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $apiGiantBombPlatformId, $comparison);
    }

    /**
     * Filter the query on the vgagrl_vgaggr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombGameRatingId(1234); // WHERE vgagrl_vgaggr_id = 1234
     * $query->filterByApiGiantBombGameRatingId(array(12, 34)); // WHERE vgagrl_vgaggr_id IN (12, 34)
     * $query->filterByApiGiantBombGameRatingId(array('min' => 12)); // WHERE vgagrl_vgaggr_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombGameRating()
     *
     * @param     mixed $apiGiantBombGameRatingId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRatingId($apiGiantBombGameRatingId = null, $comparison = null)
    {
        if (is_array($apiGiantBombGameRatingId)) {
            $useMinMax = false;
            if (isset($apiGiantBombGameRatingId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $apiGiantBombGameRatingId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombGameRatingId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $apiGiantBombGameRatingId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $apiGiantBombGameRatingId, $comparison);
    }

    /**
     * Filter the query on the vgagrl_vgagre_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApiGiantBombRegionId(1234); // WHERE vgagrl_vgagre_id = 1234
     * $query->filterByApiGiantBombRegionId(array(12, 34)); // WHERE vgagrl_vgagre_id IN (12, 34)
     * $query->filterByApiGiantBombRegionId(array('min' => 12)); // WHERE vgagrl_vgagre_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombRegion()
     *
     * @param     mixed $apiGiantBombRegionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombRegionId($apiGiantBombRegionId = null, $comparison = null)
    {
        if (is_array($apiGiantBombRegionId)) {
            $useMinMax = false;
            if (isset($apiGiantBombRegionId['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $apiGiantBombRegionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($apiGiantBombRegionId['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $apiGiantBombRegionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $apiGiantBombRegionId, $comparison);
    }

    /**
     * Filter the query on the vgaggl_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgaggl_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgaggl_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGGL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagrl_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagrl_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagrl_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagrl_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagrl_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagrl_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagrl_minimum_players column
     *
     * Example usage:
     * <code>
     * $query->filterByMinimumPlayers(1234); // WHERE vgagrl_minimum_players = 1234
     * $query->filterByMinimumPlayers(array(12, 34)); // WHERE vgagrl_minimum_players IN (12, 34)
     * $query->filterByMinimumPlayers(array('min' => 12)); // WHERE vgagrl_minimum_players > 12
     * </code>
     *
     * @param     mixed $minimumPlayers The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByMinimumPlayers($minimumPlayers = null, $comparison = null)
    {
        if (is_array($minimumPlayers)) {
            $useMinMax = false;
            if (isset($minimumPlayers['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS, $minimumPlayers['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minimumPlayers['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS, $minimumPlayers['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MINIMUM_PLAYERS, $minimumPlayers, $comparison);
    }

    /**
     * Filter the query on the vgagrl_maximum_players column
     *
     * Example usage:
     * <code>
     * $query->filterByMaximumPlayers(1234); // WHERE vgagrl_maximum_players = 1234
     * $query->filterByMaximumPlayers(array(12, 34)); // WHERE vgagrl_maximum_players IN (12, 34)
     * $query->filterByMaximumPlayers(array('min' => 12)); // WHERE vgagrl_maximum_players > 12
     * </code>
     *
     * @param     mixed $maximumPlayers The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByMaximumPlayers($maximumPlayers = null, $comparison = null)
    {
        if (is_array($maximumPlayers)) {
            $useMinMax = false;
            if (isset($maximumPlayers['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS, $maximumPlayers['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maximumPlayers['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS, $maximumPlayers['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_MAXIMUM_PLAYERS, $maximumPlayers, $comparison);
    }

    /**
     * Filter the query on the vgagrl_widescreen_support column
     *
     * Example usage:
     * <code>
     * $query->filterByWidescreenSupport(true); // WHERE vgagrl_widescreen_support = true
     * $query->filterByWidescreenSupport('yes'); // WHERE vgagrl_widescreen_support = true
     * </code>
     *
     * @param     boolean|string $widescreenSupport The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByWidescreenSupport($widescreenSupport = null, $comparison = null)
    {
        if (is_string($widescreenSupport)) {
            $widescreenSupport = in_array(strtolower($widescreenSupport), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_WIDESCREEN_SUPPORT, $widescreenSupport, $comparison);
    }

    /**
     * Filter the query on the vgagrl_product_code_value column
     *
     * Example usage:
     * <code>
     * $query->filterByProductCodeValue('fooValue');   // WHERE vgagrl_product_code_value = 'fooValue'
     * $query->filterByProductCodeValue('%fooValue%'); // WHERE vgagrl_product_code_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $productCodeValue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByProductCodeValue($productCodeValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($productCodeValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_PRODUCT_CODE_VALUE, $productCodeValue, $comparison);
    }

    /**
     * Filter the query on the vgagrl_original_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalReleasedAt('2011-03-14'); // WHERE vgagrl_original_released_at = '2011-03-14'
     * $query->filterByOriginalReleasedAt('now'); // WHERE vgagrl_original_released_at = '2011-03-14'
     * $query->filterByOriginalReleasedAt(array('max' => 'yesterday')); // WHERE vgagrl_original_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $originalReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByOriginalReleasedAt($originalReleasedAt = null, $comparison = null)
    {
        if (is_array($originalReleasedAt)) {
            $useMinMax = false;
            if (isset($originalReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT, $originalReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originalReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT, $originalReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ORIGINAL_RELEASED_AT, $originalReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_expected_day_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedDayReleasedAt('2011-03-14'); // WHERE vgagrl_expected_day_released_at = '2011-03-14'
     * $query->filterByExpectedDayReleasedAt('now'); // WHERE vgagrl_expected_day_released_at = '2011-03-14'
     * $query->filterByExpectedDayReleasedAt(array('max' => 'yesterday')); // WHERE vgagrl_expected_day_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedDayReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByExpectedDayReleasedAt($expectedDayReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedDayReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedDayReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedDayReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_DAY_RELEASED_AT, $expectedDayReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_expected_month_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedMonthReleasedAt('2011-03-14'); // WHERE vgagrl_expected_month_released_at = '2011-03-14'
     * $query->filterByExpectedMonthReleasedAt('now'); // WHERE vgagrl_expected_month_released_at = '2011-03-14'
     * $query->filterByExpectedMonthReleasedAt(array('max' => 'yesterday')); // WHERE vgagrl_expected_month_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedMonthReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByExpectedMonthReleasedAt($expectedMonthReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedMonthReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedMonthReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedMonthReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_MONTH_RELEASED_AT, $expectedMonthReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_expected_quarter_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedQuarterReleasedAt('2011-03-14'); // WHERE vgagrl_expected_quarter_released_at = '2011-03-14'
     * $query->filterByExpectedQuarterReleasedAt('now'); // WHERE vgagrl_expected_quarter_released_at = '2011-03-14'
     * $query->filterByExpectedQuarterReleasedAt(array('max' => 'yesterday')); // WHERE vgagrl_expected_quarter_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedQuarterReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByExpectedQuarterReleasedAt($expectedQuarterReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedQuarterReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedQuarterReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedQuarterReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_QUARTER_RELEASED_AT, $expectedQuarterReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_expected_year_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpectedYearReleasedAt('2011-03-14'); // WHERE vgagrl_expected_year_released_at = '2011-03-14'
     * $query->filterByExpectedYearReleasedAt('now'); // WHERE vgagrl_expected_year_released_at = '2011-03-14'
     * $query->filterByExpectedYearReleasedAt(array('max' => 'yesterday')); // WHERE vgagrl_expected_year_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expectedYearReleasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByExpectedYearReleasedAt($expectedYearReleasedAt = null, $comparison = null)
    {
        if (is_array($expectedYearReleasedAt)) {
            $useMinMax = false;
            if (isset($expectedYearReleasedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expectedYearReleasedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_EXPECTED_YEAR_RELEASED_AT, $expectedYearReleasedAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagrl_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagrl_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagrl_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagrl_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagrl_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagrl_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagrl_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagrl_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagrl_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagrl_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagrl_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagrl_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagrl_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagrl_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagrl_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagrl_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagrl_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagrl_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagrl_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagrl_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagrl_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagrl_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagrl_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagrl_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform|ObjectCollection $apiGiantBombPlatform The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombPlatform($apiGiantBombPlatform, $comparison = null)
    {
        if ($apiGiantBombPlatform instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $apiGiantBombPlatform->getId(), $comparison);
        } elseif ($apiGiantBombPlatform instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGPL_ID, $apiGiantBombPlatform->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombPlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombPlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombPlatform($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombPlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombPlatform');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombPlatform relation ApiGiantBombPlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombPlatformQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombPlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombPlatform', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombPlatformQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating|ObjectCollection $apiGiantBombGameRating The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameRating($apiGiantBombGameRating, $comparison = null)
    {
        if ($apiGiantBombGameRating instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $apiGiantBombGameRating->getId(), $comparison);
        } elseif ($apiGiantBombGameRating instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGGR_ID, $apiGiantBombGameRating->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombGameRating() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRating or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameRating relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameRating($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameRating');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameRating');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameRating relation ApiGiantBombGameRating object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameRatingQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombGameRating($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameRating', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRatingQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion|ObjectCollection $apiGiantBombRegion The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombRegion($apiGiantBombRegion, $comparison = null)
    {
        if ($apiGiantBombRegion instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $apiGiantBombRegion->getId(), $comparison);
        } elseif ($apiGiantBombRegion instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_VGAGRE_ID, $apiGiantBombRegion->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombRegion() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombRegion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombRegion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombRegion($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombRegion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombRegion');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombRegion relation ApiGiantBombRegion object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombRegionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombRegion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombRegion', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombRegionQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper|ObjectCollection $apiGiantBombGameReleaseDeveloper the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameReleaseDeveloper($apiGiantBombGameReleaseDeveloper, $comparison = null)
    {
        if ($apiGiantBombGameReleaseDeveloper instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $apiGiantBombGameReleaseDeveloper->getApiGiantBombGameReleaseId(), $comparison);
        } elseif ($apiGiantBombGameReleaseDeveloper instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleaseDeveloperQuery()
                ->filterByPrimaryKeys($apiGiantBombGameReleaseDeveloper->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameReleaseDeveloper() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloper or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameReleaseDeveloper relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameReleaseDeveloper($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameReleaseDeveloper');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameReleaseDeveloper');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameReleaseDeveloper relation ApiGiantBombGameReleaseDeveloper object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleaseDeveloperQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameReleaseDeveloper($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameReleaseDeveloper', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseDeveloperQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher|ObjectCollection $apiGiantBombGameReleasePublisher the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombGameReleasePublisher($apiGiantBombGameReleasePublisher, $comparison = null)
    {
        if ($apiGiantBombGameReleasePublisher instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $apiGiantBombGameReleasePublisher->getApiGiantBombGameReleaseId(), $comparison);
        } elseif ($apiGiantBombGameReleasePublisher instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombGameReleasePublisherQuery()
                ->filterByPrimaryKeys($apiGiantBombGameReleasePublisher->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombGameReleasePublisher() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisher or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombGameReleasePublisher relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombGameReleasePublisher($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombGameReleasePublisher');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombGameReleasePublisher');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombGameReleasePublisher relation ApiGiantBombGameReleasePublisher object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombGameReleasePublisherQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinApiGiantBombGameReleasePublisher($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombGameReleasePublisher', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleasePublisherQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage|ObjectCollection $apiGiantBombImage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombImage($apiGiantBombImage, $comparison = null)
    {
        if ($apiGiantBombImage instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $apiGiantBombImage->getApiGiantBombGameReleaseId(), $comparison);
        } elseif ($apiGiantBombImage instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombImageQuery()
                ->filterByPrimaryKeys($apiGiantBombImage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombImage() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombImage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombImage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombImage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombImage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombImage');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombImage relation ApiGiantBombImage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombImageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombImage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombImage', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo|ObjectCollection $apiGiantBombVideo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombVideo($apiGiantBombVideo, $comparison = null)
    {
        if ($apiGiantBombVideo instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo) {
            return $this
                ->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $apiGiantBombVideo->getApiGiantBombGameReleaseId(), $comparison);
        } elseif ($apiGiantBombVideo instanceof ObjectCollection) {
            return $this
                ->useApiGiantBombVideoQuery()
                ->filterByPrimaryKeys($apiGiantBombVideo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByApiGiantBombVideo() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombVideo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function joinApiGiantBombVideo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombVideo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombVideo');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombVideo relation ApiGiantBombVideo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombVideoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombVideo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombVideo', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombGameRelease $apiGiantBombGameRelease Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombGameReleaseQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombGameRelease = null)
    {
        if ($apiGiantBombGameRelease) {
            $this->addUsingAlias(ApiGiantBombGameReleaseTableMap::COL_VGAGRL_ID, $apiGiantBombGameRelease->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_release_vgagrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombGameReleaseTableMap::clearInstancePool();
            ApiGiantBombGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombGameReleaseTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombGameReleaseTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombGameReleaseQuery
