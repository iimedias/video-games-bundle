<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter as ChildApiGiantBombCharacter;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery as ChildApiGiantBombCharacterQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept as ChildApiGiantBombConcept;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery as ChildApiGiantBombConceptQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGame as ChildApiGiantBombGame;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper as ChildApiGiantBombGameDeveloper;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery as ChildApiGiantBombGameDeveloperQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise as ChildApiGiantBombGameFranchise;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery as ChildApiGiantBombGameFranchiseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre as ChildApiGiantBombGameGenre;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenreQuery as ChildApiGiantBombGameGenreQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating as ChildApiGiantBombGameOriginalRating;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery as ChildApiGiantBombGameOriginalRatingQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform as ChildApiGiantBombGamePlatform;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery as ChildApiGiantBombGamePlatformQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher as ChildApiGiantBombGamePublisher;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery as ChildApiGiantBombGamePublisherQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery as ChildApiGiantBombGameQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease as ChildApiGiantBombGameRelease;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery as ChildApiGiantBombGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImage as ChildApiGiantBombImage;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery as ChildApiGiantBombImageQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation as ChildApiGiantBombLocation;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery as ChildApiGiantBombLocationQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombObject as ChildApiGiantBombObject;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery as ChildApiGiantBombObjectQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson as ChildApiGiantBombPerson;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery as ChildApiGiantBombPersonQuery;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo as ChildApiGiantBombVideo;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery as ChildApiGiantBombVideoQuery;
use IiMedias\VideoGamesBundle\Model\Game as ChildGame;
use IiMedias\VideoGamesBundle\Model\GameQuery as ChildGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombCharacterTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombConceptTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameDeveloperTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameFranchiseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameGenreTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameOriginalRatingTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGamePlatformTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGamePublisherTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameReleaseTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombGameTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombImageTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombLocationTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombObjectTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPersonTableMap;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombVideoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'videogames_api_giantbomb_game_vgagga' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.VideoGamesBundle.Model.Base
 */
abstract class ApiGiantBombGame implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\VideoGamesBundle\\Model\\Map\\ApiGiantBombGameTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the vgagga_id field.
     *
     * @var        int
     */
    protected $vgagga_id;

    /**
     * The value for the vgagga_vgagam_id field.
     *
     * @var        int
     */
    protected $vgagga_vgagam_id;

    /**
     * The value for the vgagga_name field.
     *
     * @var        string
     */
    protected $vgagga_name;

    /**
     * The value for the vgagga_aliases field.
     *
     * @var        array
     */
    protected $vgagga_aliases;

    /**
     * The unserialized $vgagga_aliases value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $vgagga_aliases_unserialized;

    /**
     * The value for the vgagga_summary field.
     *
     * @var        string
     */
    protected $vgagga_summary;

    /**
     * The value for the vgagga_description field.
     *
     * @var        string
     */
    protected $vgagga_description;

    /**
     * The value for the vgagga_original_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_original_released_at;

    /**
     * The value for the vgagga_expected_day_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_expected_day_released_at;

    /**
     * The value for the vgagga_expected_month_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_expected_month_released_at;

    /**
     * The value for the vgagga_expected_quarter_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_expected_quarter_released_at;

    /**
     * The value for the vgagga_expected_year_released_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_expected_year_released_at;

    /**
     * The value for the vgagga_user_reviews_count field.
     *
     * @var        int
     */
    protected $vgagga_user_reviews_count;

    /**
     * The value for the vgagga_image_icon_url field.
     *
     * @var        string
     */
    protected $vgagga_image_icon_url;

    /**
     * The value for the vgagga_image_medium_url field.
     *
     * @var        string
     */
    protected $vgagga_image_medium_url;

    /**
     * The value for the vgagga_image_screen_url field.
     *
     * @var        string
     */
    protected $vgagga_image_screen_url;

    /**
     * The value for the vgagga_image_small_url field.
     *
     * @var        string
     */
    protected $vgagga_image_small_url;

    /**
     * The value for the vgagga_image_super_url field.
     *
     * @var        string
     */
    protected $vgagga_image_super_url;

    /**
     * The value for the vgagga_image_thumb_url field.
     *
     * @var        string
     */
    protected $vgagga_image_thumb_url;

    /**
     * The value for the vgagga_image_tiny_url field.
     *
     * @var        string
     */
    protected $vgagga_image_tiny_url;

    /**
     * The value for the vgagga_api_detail_url field.
     *
     * @var        string
     */
    protected $vgagga_api_detail_url;

    /**
     * The value for the vgagga_site_detail_url field.
     *
     * @var        string
     */
    protected $vgagga_site_detail_url;

    /**
     * The value for the vgagga_created_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_created_at;

    /**
     * The value for the vgagga_updated_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_updated_at;

    /**
     * The value for the vgagga_refreshed_at field.
     *
     * @var        DateTime
     */
    protected $vgagga_refreshed_at;

    /**
     * @var        ChildGame
     */
    protected $aGame;

    /**
     * @var        ObjectCollection|ChildApiGiantBombCharacter[] Collection to store aggregation of ChildApiGiantBombCharacter objects.
     */
    protected $collApiGiantBombFirstGameCharacters;
    protected $collApiGiantBombFirstGameCharactersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombConcept[] Collection to store aggregation of ChildApiGiantBombConcept objects.
     */
    protected $collApiGiantBombFirstGameConcepts;
    protected $collApiGiantBombFirstGameConceptsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameOriginalRating[] Collection to store aggregation of ChildApiGiantBombGameOriginalRating objects.
     */
    protected $collApiGiantBombGameOriginalRatings;
    protected $collApiGiantBombGameOriginalRatingsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGamePlatform[] Collection to store aggregation of ChildApiGiantBombGamePlatform objects.
     */
    protected $collApiGiantBombGamePlatforms;
    protected $collApiGiantBombGamePlatformsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameDeveloper[] Collection to store aggregation of ChildApiGiantBombGameDeveloper objects.
     */
    protected $collApiGiantBombGameDevelopers;
    protected $collApiGiantBombGameDevelopersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGamePublisher[] Collection to store aggregation of ChildApiGiantBombGamePublisher objects.
     */
    protected $collApiGiantBombGamePublishers;
    protected $collApiGiantBombGamePublishersPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameFranchise[] Collection to store aggregation of ChildApiGiantBombGameFranchise objects.
     */
    protected $collApiGiantBombGameFranchises;
    protected $collApiGiantBombGameFranchisesPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameGenre[] Collection to store aggregation of ChildApiGiantBombGameGenre objects.
     */
    protected $collApiGiantBombGameGenres;
    protected $collApiGiantBombGameGenresPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombGameRelease[] Collection to store aggregation of ChildApiGiantBombGameRelease objects.
     */
    protected $collApiGiantBombGameReleases;
    protected $collApiGiantBombGameReleasesPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombLocation[] Collection to store aggregation of ChildApiGiantBombLocation objects.
     */
    protected $collApiGiantBombFirstGameLocations;
    protected $collApiGiantBombFirstGameLocationsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombObject[] Collection to store aggregation of ChildApiGiantBombObject objects.
     */
    protected $collApiGiantBombFirstGameObjects;
    protected $collApiGiantBombFirstGameObjectsPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombPerson[] Collection to store aggregation of ChildApiGiantBombPerson objects.
     */
    protected $collApiGiantBombCreditedGamepeople;
    protected $collApiGiantBombCreditedGamepeoplePartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombImage[] Collection to store aggregation of ChildApiGiantBombImage objects.
     */
    protected $collApiGiantBombImages;
    protected $collApiGiantBombImagesPartial;

    /**
     * @var        ObjectCollection|ChildApiGiantBombVideo[] Collection to store aggregation of ChildApiGiantBombVideo objects.
     */
    protected $collApiGiantBombVideos;
    protected $collApiGiantBombVideosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombCharacter[]
     */
    protected $apiGiantBombFirstGameCharactersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombConcept[]
     */
    protected $apiGiantBombFirstGameConceptsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameOriginalRating[]
     */
    protected $apiGiantBombGameOriginalRatingsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGamePlatform[]
     */
    protected $apiGiantBombGamePlatformsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameDeveloper[]
     */
    protected $apiGiantBombGameDevelopersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGamePublisher[]
     */
    protected $apiGiantBombGamePublishersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameFranchise[]
     */
    protected $apiGiantBombGameFranchisesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameGenre[]
     */
    protected $apiGiantBombGameGenresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombGameRelease[]
     */
    protected $apiGiantBombGameReleasesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombLocation[]
     */
    protected $apiGiantBombFirstGameLocationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombObject[]
     */
    protected $apiGiantBombFirstGameObjectsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombPerson[]
     */
    protected $apiGiantBombCreditedGamepeopleScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombImage[]
     */
    protected $apiGiantBombImagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildApiGiantBombVideo[]
     */
    protected $apiGiantBombVideosScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombGame object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ApiGiantBombGame</code> instance.  If
     * <code>obj</code> is an instance of <code>ApiGiantBombGame</code>, delegates to
     * <code>equals(ApiGiantBombGame)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ApiGiantBombGame The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [vgagga_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->vgagga_id;
    }

    /**
     * Get the [vgagga_vgagam_id] column value.
     *
     * @return int
     */
    public function getGameId()
    {
        return $this->vgagga_vgagam_id;
    }

    /**
     * Get the [vgagga_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->vgagga_name;
    }

    /**
     * Get the [vgagga_aliases] column value.
     *
     * @return array
     */
    public function getAliases()
    {
        if (null === $this->vgagga_aliases_unserialized) {
            $this->vgagga_aliases_unserialized = array();
        }
        if (!$this->vgagga_aliases_unserialized && null !== $this->vgagga_aliases) {
            $vgagga_aliases_unserialized = substr($this->vgagga_aliases, 2, -2);
            $this->vgagga_aliases_unserialized = $vgagga_aliases_unserialized ? explode(' | ', $vgagga_aliases_unserialized) : array();
        }

        return $this->vgagga_aliases_unserialized;
    }

    /**
     * Test the presence of a value in the [vgagga_aliases] array column value.
     * @param      mixed $value
     *
     * @return boolean
     */
    public function hasAliase($value)
    {
        return in_array($value, $this->getAliases());
    } // hasAliase()

    /**
     * Get the [vgagga_summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->vgagga_summary;
    }

    /**
     * Get the [vgagga_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->vgagga_description;
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_original_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getOriginalReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_original_released_at;
        } else {
            return $this->vgagga_original_released_at instanceof \DateTimeInterface ? $this->vgagga_original_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_expected_day_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedDayReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_expected_day_released_at;
        } else {
            return $this->vgagga_expected_day_released_at instanceof \DateTimeInterface ? $this->vgagga_expected_day_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_expected_month_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedMonthReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_expected_month_released_at;
        } else {
            return $this->vgagga_expected_month_released_at instanceof \DateTimeInterface ? $this->vgagga_expected_month_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_expected_quarter_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedQuarterReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_expected_quarter_released_at;
        } else {
            return $this->vgagga_expected_quarter_released_at instanceof \DateTimeInterface ? $this->vgagga_expected_quarter_released_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_expected_year_released_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpectedYearReleasedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_expected_year_released_at;
        } else {
            return $this->vgagga_expected_year_released_at instanceof \DateTimeInterface ? $this->vgagga_expected_year_released_at->format($format) : null;
        }
    }

    /**
     * Get the [vgagga_user_reviews_count] column value.
     *
     * @return int
     */
    public function getUserReviewsCount()
    {
        return $this->vgagga_user_reviews_count;
    }

    /**
     * Get the [vgagga_image_icon_url] column value.
     *
     * @return string
     */
    public function getImageIconUrl()
    {
        return $this->vgagga_image_icon_url;
    }

    /**
     * Get the [vgagga_image_medium_url] column value.
     *
     * @return string
     */
    public function getImageMediumUrl()
    {
        return $this->vgagga_image_medium_url;
    }

    /**
     * Get the [vgagga_image_screen_url] column value.
     *
     * @return string
     */
    public function getImageScreenUrl()
    {
        return $this->vgagga_image_screen_url;
    }

    /**
     * Get the [vgagga_image_small_url] column value.
     *
     * @return string
     */
    public function getImageSmallUrl()
    {
        return $this->vgagga_image_small_url;
    }

    /**
     * Get the [vgagga_image_super_url] column value.
     *
     * @return string
     */
    public function getImageSuperUrl()
    {
        return $this->vgagga_image_super_url;
    }

    /**
     * Get the [vgagga_image_thumb_url] column value.
     *
     * @return string
     */
    public function getImageThumbUrl()
    {
        return $this->vgagga_image_thumb_url;
    }

    /**
     * Get the [vgagga_image_tiny_url] column value.
     *
     * @return string
     */
    public function getImageTinyUrl()
    {
        return $this->vgagga_image_tiny_url;
    }

    /**
     * Get the [vgagga_api_detail_url] column value.
     *
     * @return string
     */
    public function getApiDetailUrl()
    {
        return $this->vgagga_api_detail_url;
    }

    /**
     * Get the [vgagga_site_detail_url] column value.
     *
     * @return string
     */
    public function getSiteDetailUrl()
    {
        return $this->vgagga_site_detail_url;
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_created_at;
        } else {
            return $this->vgagga_created_at instanceof \DateTimeInterface ? $this->vgagga_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_updated_at;
        } else {
            return $this->vgagga_updated_at instanceof \DateTimeInterface ? $this->vgagga_updated_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vgagga_refreshed_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRefreshedAt($format = NULL)
    {
        if ($format === null) {
            return $this->vgagga_refreshed_at;
        } else {
            return $this->vgagga_refreshed_at instanceof \DateTimeInterface ? $this->vgagga_refreshed_at->format($format) : null;
        }
    }

    /**
     * Set the value of [vgagga_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagga_id !== $v) {
            $this->vgagga_id = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [vgagga_vgagam_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setGameId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagga_vgagam_id !== $v) {
            $this->vgagga_vgagam_id = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID] = true;
        }

        if ($this->aGame !== null && $this->aGame->getId() !== $v) {
            $this->aGame = null;
        }

        return $this;
    } // setGameId()

    /**
     * Set the value of [vgagga_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_name !== $v) {
            $this->vgagga_name = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [vgagga_aliases] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setAliases($v)
    {
        if ($this->vgagga_aliases_unserialized !== $v) {
            $this->vgagga_aliases_unserialized = $v;
            $this->vgagga_aliases = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES] = true;
        }

        return $this;
    } // setAliases()

    /**
     * Adds a value to the [vgagga_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addAliase($value)
    {
        $currentArray = $this->getAliases();
        $currentArray []= $value;
        $this->setAliases($currentArray);

        return $this;
    } // addAliase()

    /**
     * Removes a value from the [vgagga_aliases] array column value.
     * @param  mixed $value
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function removeAliase($value)
    {
        $targetArray = array();
        foreach ($this->getAliases() as $element) {
            if ($element != $value) {
                $targetArray []= $element;
            }
        }
        $this->setAliases($targetArray);

        return $this;
    } // removeAliase()

    /**
     * Set the value of [vgagga_summary] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_summary !== $v) {
            $this->vgagga_summary = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [vgagga_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_description !== $v) {
            $this->vgagga_description = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Sets the value of [vgagga_original_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setOriginalReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_original_released_at !== null || $dt !== null) {
            if ($this->vgagga_original_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_original_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_original_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setOriginalReleasedAt()

    /**
     * Sets the value of [vgagga_expected_day_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setExpectedDayReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_expected_day_released_at !== null || $dt !== null) {
            if ($this->vgagga_expected_day_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_expected_day_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_expected_day_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedDayReleasedAt()

    /**
     * Sets the value of [vgagga_expected_month_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setExpectedMonthReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_expected_month_released_at !== null || $dt !== null) {
            if ($this->vgagga_expected_month_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_expected_month_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_expected_month_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedMonthReleasedAt()

    /**
     * Sets the value of [vgagga_expected_quarter_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setExpectedQuarterReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_expected_quarter_released_at !== null || $dt !== null) {
            if ($this->vgagga_expected_quarter_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_expected_quarter_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_expected_quarter_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedQuarterReleasedAt()

    /**
     * Sets the value of [vgagga_expected_year_released_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setExpectedYearReleasedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_expected_year_released_at !== null || $dt !== null) {
            if ($this->vgagga_expected_year_released_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_expected_year_released_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_expected_year_released_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpectedYearReleasedAt()

    /**
     * Set the value of [vgagga_user_reviews_count] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setUserReviewsCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vgagga_user_reviews_count !== $v) {
            $this->vgagga_user_reviews_count = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT] = true;
        }

        return $this;
    } // setUserReviewsCount()

    /**
     * Set the value of [vgagga_image_icon_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageIconUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_icon_url !== $v) {
            $this->vgagga_image_icon_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL] = true;
        }

        return $this;
    } // setImageIconUrl()

    /**
     * Set the value of [vgagga_image_medium_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageMediumUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_medium_url !== $v) {
            $this->vgagga_image_medium_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL] = true;
        }

        return $this;
    } // setImageMediumUrl()

    /**
     * Set the value of [vgagga_image_screen_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageScreenUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_screen_url !== $v) {
            $this->vgagga_image_screen_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL] = true;
        }

        return $this;
    } // setImageScreenUrl()

    /**
     * Set the value of [vgagga_image_small_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageSmallUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_small_url !== $v) {
            $this->vgagga_image_small_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL] = true;
        }

        return $this;
    } // setImageSmallUrl()

    /**
     * Set the value of [vgagga_image_super_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageSuperUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_super_url !== $v) {
            $this->vgagga_image_super_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL] = true;
        }

        return $this;
    } // setImageSuperUrl()

    /**
     * Set the value of [vgagga_image_thumb_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageThumbUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_thumb_url !== $v) {
            $this->vgagga_image_thumb_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL] = true;
        }

        return $this;
    } // setImageThumbUrl()

    /**
     * Set the value of [vgagga_image_tiny_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setImageTinyUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_image_tiny_url !== $v) {
            $this->vgagga_image_tiny_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL] = true;
        }

        return $this;
    } // setImageTinyUrl()

    /**
     * Set the value of [vgagga_api_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_api_detail_url !== $v) {
            $this->vgagga_api_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL] = true;
        }

        return $this;
    } // setApiDetailUrl()

    /**
     * Set the value of [vgagga_site_detail_url] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setSiteDetailUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vgagga_site_detail_url !== $v) {
            $this->vgagga_site_detail_url = $v;
            $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL] = true;
        }

        return $this;
    } // setSiteDetailUrl()

    /**
     * Sets the value of [vgagga_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_created_at !== null || $dt !== null) {
            if ($this->vgagga_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_created_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [vgagga_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_updated_at !== null || $dt !== null) {
            if ($this->vgagga_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_updated_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Sets the value of [vgagga_refreshed_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function setRefreshedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vgagga_refreshed_at !== null || $dt !== null) {
            if ($this->vgagga_refreshed_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->vgagga_refreshed_at->format("Y-m-d H:i:s.u")) {
                $this->vgagga_refreshed_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setRefreshedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApiGiantBombGameTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApiGiantBombGameTableMap::translateFieldName('GameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_vgagam_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApiGiantBombGameTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApiGiantBombGameTableMap::translateFieldName('Aliases', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_aliases = $col;
            $this->vgagga_aliases_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApiGiantBombGameTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApiGiantBombGameTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApiGiantBombGameTableMap::translateFieldName('OriginalReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_original_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ExpectedDayReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_expected_day_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ExpectedMonthReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_expected_month_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ExpectedQuarterReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_expected_quarter_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ExpectedYearReleasedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_expected_year_released_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApiGiantBombGameTableMap::translateFieldName('UserReviewsCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_user_reviews_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageIconUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_icon_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageMediumUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_medium_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageScreenUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_screen_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageSmallUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_small_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageSuperUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_super_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageThumbUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_thumb_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ImageTinyUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_image_tiny_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApiGiantBombGameTableMap::translateFieldName('ApiDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_api_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApiGiantBombGameTableMap::translateFieldName('SiteDetailUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vgagga_site_detail_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApiGiantBombGameTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ApiGiantBombGameTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ApiGiantBombGameTableMap::translateFieldName('RefreshedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->vgagga_refreshed_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 24; // 24 = ApiGiantBombGameTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombGame'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aGame !== null && $this->vgagga_vgagam_id !== $this->aGame->getId()) {
            $this->aGame = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApiGiantBombGameQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aGame = null;
            $this->collApiGiantBombFirstGameCharacters = null;

            $this->collApiGiantBombFirstGameConcepts = null;

            $this->collApiGiantBombGameOriginalRatings = null;

            $this->collApiGiantBombGamePlatforms = null;

            $this->collApiGiantBombGameDevelopers = null;

            $this->collApiGiantBombGamePublishers = null;

            $this->collApiGiantBombGameFranchises = null;

            $this->collApiGiantBombGameGenres = null;

            $this->collApiGiantBombGameReleases = null;

            $this->collApiGiantBombFirstGameLocations = null;

            $this->collApiGiantBombFirstGameObjects = null;

            $this->collApiGiantBombCreditedGamepeople = null;

            $this->collApiGiantBombImages = null;

            $this->collApiGiantBombVideos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ApiGiantBombGame::setDeleted()
     * @see ApiGiantBombGame::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApiGiantBombGameQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombGameTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiGiantBombGameTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aGame !== null) {
                if ($this->aGame->isModified() || $this->aGame->isNew()) {
                    $affectedRows += $this->aGame->save($con);
                }
                $this->setGame($this->aGame);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->apiGiantBombFirstGameCharactersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombFirstGameCharactersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacterQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombFirstGameCharactersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombFirstGameCharactersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombFirstGameCharacters !== null) {
                foreach ($this->collApiGiantBombFirstGameCharacters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombFirstGameConceptsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombFirstGameConceptsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombConceptQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombFirstGameConceptsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombFirstGameConceptsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombFirstGameConcepts !== null) {
                foreach ($this->collApiGiantBombFirstGameConcepts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameOriginalRatingsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameOriginalRatingsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRatingQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameOriginalRatingsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameOriginalRatings !== null) {
                foreach ($this->collApiGiantBombGameOriginalRatings as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGamePlatformsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGamePlatformsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatformQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGamePlatformsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGamePlatformsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGamePlatforms !== null) {
                foreach ($this->collApiGiantBombGamePlatforms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameDevelopersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameDevelopersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloperQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameDevelopersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameDevelopersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameDevelopers !== null) {
                foreach ($this->collApiGiantBombGameDevelopers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGamePublishersScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGamePublishersScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisherQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGamePublishersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGamePublishersScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGamePublishers !== null) {
                foreach ($this->collApiGiantBombGamePublishers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameFranchisesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameFranchisesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchiseQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameFranchisesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameFranchisesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameFranchises !== null) {
                foreach ($this->collApiGiantBombGameFranchises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameGenresScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameGenresScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenreQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameGenresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameGenresScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameGenres !== null) {
                foreach ($this->collApiGiantBombGameGenres as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombGameReleasesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombGameReleasesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameReleaseQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombGameReleasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombGameReleasesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombGameReleases !== null) {
                foreach ($this->collApiGiantBombGameReleases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombFirstGameLocationsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombFirstGameLocationsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombLocationQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombFirstGameLocationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombFirstGameLocationsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombFirstGameLocations !== null) {
                foreach ($this->collApiGiantBombFirstGameLocations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombFirstGameObjectsScheduledForDeletion !== null) {
                if (!$this->apiGiantBombFirstGameObjectsScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombObjectQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombFirstGameObjectsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombFirstGameObjectsScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombFirstGameObjects !== null) {
                foreach ($this->collApiGiantBombFirstGameObjects as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombCreditedGamepeopleScheduledForDeletion !== null) {
                if (!$this->apiGiantBombCreditedGamepeopleScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombCreditedGamepeopleScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombCreditedGamepeopleScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombCreditedGamepeople !== null) {
                foreach ($this->collApiGiantBombCreditedGamepeople as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombImagesScheduledForDeletion !== null) {
                if (!$this->apiGiantBombImagesScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombImageQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombImagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombImagesScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombImages !== null) {
                foreach ($this->collApiGiantBombImages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->apiGiantBombVideosScheduledForDeletion !== null) {
                if (!$this->apiGiantBombVideosScheduledForDeletion->isEmpty()) {
                    \IiMedias\VideoGamesBundle\Model\ApiGiantBombVideoQuery::create()
                        ->filterByPrimaryKeys($this->apiGiantBombVideosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->apiGiantBombVideosScheduledForDeletion = null;
                }
            }

            if ($this->collApiGiantBombVideos !== null) {
                foreach ($this->collApiGiantBombVideos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_vgagam_id';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_name';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_aliases';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_summary';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_description';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_original_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_expected_day_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_expected_month_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_expected_quarter_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_expected_year_released_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_user_reviews_count';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_icon_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_medium_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_screen_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_small_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_super_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_thumb_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_image_tiny_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_api_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_site_detail_url';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_created_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_updated_at';
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'vgagga_refreshed_at';
        }

        $sql = sprintf(
            'INSERT INTO videogames_api_giantbomb_game_vgagga (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'vgagga_id':
                        $stmt->bindValue($identifier, $this->vgagga_id, PDO::PARAM_INT);
                        break;
                    case 'vgagga_vgagam_id':
                        $stmt->bindValue($identifier, $this->vgagga_vgagam_id, PDO::PARAM_INT);
                        break;
                    case 'vgagga_name':
                        $stmt->bindValue($identifier, $this->vgagga_name, PDO::PARAM_STR);
                        break;
                    case 'vgagga_aliases':
                        $stmt->bindValue($identifier, $this->vgagga_aliases, PDO::PARAM_STR);
                        break;
                    case 'vgagga_summary':
                        $stmt->bindValue($identifier, $this->vgagga_summary, PDO::PARAM_STR);
                        break;
                    case 'vgagga_description':
                        $stmt->bindValue($identifier, $this->vgagga_description, PDO::PARAM_STR);
                        break;
                    case 'vgagga_original_released_at':
                        $stmt->bindValue($identifier, $this->vgagga_original_released_at ? $this->vgagga_original_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_expected_day_released_at':
                        $stmt->bindValue($identifier, $this->vgagga_expected_day_released_at ? $this->vgagga_expected_day_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_expected_month_released_at':
                        $stmt->bindValue($identifier, $this->vgagga_expected_month_released_at ? $this->vgagga_expected_month_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_expected_quarter_released_at':
                        $stmt->bindValue($identifier, $this->vgagga_expected_quarter_released_at ? $this->vgagga_expected_quarter_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_expected_year_released_at':
                        $stmt->bindValue($identifier, $this->vgagga_expected_year_released_at ? $this->vgagga_expected_year_released_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_user_reviews_count':
                        $stmt->bindValue($identifier, $this->vgagga_user_reviews_count, PDO::PARAM_INT);
                        break;
                    case 'vgagga_image_icon_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_icon_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_medium_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_medium_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_screen_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_screen_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_small_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_small_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_super_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_super_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_thumb_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_thumb_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_image_tiny_url':
                        $stmt->bindValue($identifier, $this->vgagga_image_tiny_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_api_detail_url':
                        $stmt->bindValue($identifier, $this->vgagga_api_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_site_detail_url':
                        $stmt->bindValue($identifier, $this->vgagga_site_detail_url, PDO::PARAM_STR);
                        break;
                    case 'vgagga_created_at':
                        $stmt->bindValue($identifier, $this->vgagga_created_at ? $this->vgagga_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_updated_at':
                        $stmt->bindValue($identifier, $this->vgagga_updated_at ? $this->vgagga_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vgagga_refreshed_at':
                        $stmt->bindValue($identifier, $this->vgagga_refreshed_at ? $this->vgagga_refreshed_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getGameId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getAliases();
                break;
            case 4:
                return $this->getSummary();
                break;
            case 5:
                return $this->getDescription();
                break;
            case 6:
                return $this->getOriginalReleasedAt();
                break;
            case 7:
                return $this->getExpectedDayReleasedAt();
                break;
            case 8:
                return $this->getExpectedMonthReleasedAt();
                break;
            case 9:
                return $this->getExpectedQuarterReleasedAt();
                break;
            case 10:
                return $this->getExpectedYearReleasedAt();
                break;
            case 11:
                return $this->getUserReviewsCount();
                break;
            case 12:
                return $this->getImageIconUrl();
                break;
            case 13:
                return $this->getImageMediumUrl();
                break;
            case 14:
                return $this->getImageScreenUrl();
                break;
            case 15:
                return $this->getImageSmallUrl();
                break;
            case 16:
                return $this->getImageSuperUrl();
                break;
            case 17:
                return $this->getImageThumbUrl();
                break;
            case 18:
                return $this->getImageTinyUrl();
                break;
            case 19:
                return $this->getApiDetailUrl();
                break;
            case 20:
                return $this->getSiteDetailUrl();
                break;
            case 21:
                return $this->getCreatedAt();
                break;
            case 22:
                return $this->getUpdatedAt();
                break;
            case 23:
                return $this->getRefreshedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ApiGiantBombGame'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiGiantBombGame'][$this->hashCode()] = true;
        $keys = ApiGiantBombGameTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getGameId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getAliases(),
            $keys[4] => $this->getSummary(),
            $keys[5] => $this->getDescription(),
            $keys[6] => $this->getOriginalReleasedAt(),
            $keys[7] => $this->getExpectedDayReleasedAt(),
            $keys[8] => $this->getExpectedMonthReleasedAt(),
            $keys[9] => $this->getExpectedQuarterReleasedAt(),
            $keys[10] => $this->getExpectedYearReleasedAt(),
            $keys[11] => $this->getUserReviewsCount(),
            $keys[12] => $this->getImageIconUrl(),
            $keys[13] => $this->getImageMediumUrl(),
            $keys[14] => $this->getImageScreenUrl(),
            $keys[15] => $this->getImageSmallUrl(),
            $keys[16] => $this->getImageSuperUrl(),
            $keys[17] => $this->getImageThumbUrl(),
            $keys[18] => $this->getImageTinyUrl(),
            $keys[19] => $this->getApiDetailUrl(),
            $keys[20] => $this->getSiteDetailUrl(),
            $keys[21] => $this->getCreatedAt(),
            $keys[22] => $this->getUpdatedAt(),
            $keys[23] => $this->getRefreshedAt(),
        );
        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTime) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        if ($result[$keys[22]] instanceof \DateTime) {
            $result[$keys[22]] = $result[$keys[22]]->format('c');
        }

        if ($result[$keys[23]] instanceof \DateTime) {
            $result[$keys[23]] = $result[$keys[23]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aGame) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'game';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_game_vgagam';
                        break;
                    default:
                        $key = 'Game';
                }

                $result[$key] = $this->aGame->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collApiGiantBombFirstGameCharacters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombCharacters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_character_vgagches';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstGameCharacters';
                }

                $result[$key] = $this->collApiGiantBombFirstGameCharacters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombFirstGameConcepts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombConcepts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_concept_vgagccs';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstGameConcepts';
                }

                $result[$key] = $this->collApiGiantBombFirstGameConcepts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameOriginalRatings) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameOriginalRatings';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_original_rating_vgaggos';
                        break;
                    default:
                        $key = 'ApiGiantBombGameOriginalRatings';
                }

                $result[$key] = $this->collApiGiantBombGameOriginalRatings->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGamePlatforms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGamePlatforms';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_platform_vgaggps';
                        break;
                    default:
                        $key = 'ApiGiantBombGamePlatforms';
                }

                $result[$key] = $this->collApiGiantBombGamePlatforms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameDevelopers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameDevelopers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_developer_vgaggds';
                        break;
                    default:
                        $key = 'ApiGiantBombGameDevelopers';
                }

                $result[$key] = $this->collApiGiantBombGameDevelopers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGamePublishers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGamePublishers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_publisher_vgaggus';
                        break;
                    default:
                        $key = 'ApiGiantBombGamePublishers';
                }

                $result[$key] = $this->collApiGiantBombGamePublishers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameFranchises) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameFranchises';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_franchise_vgaggfs';
                        break;
                    default:
                        $key = 'ApiGiantBombGameFranchises';
                }

                $result[$key] = $this->collApiGiantBombGameFranchises->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameGenres) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameGenres';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_game_genre_vgagggs';
                        break;
                    default:
                        $key = 'ApiGiantBombGameGenres';
                }

                $result[$key] = $this->collApiGiantBombGameGenres->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombGameReleases) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombGameReleases';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_release_vgagrls';
                        break;
                    default:
                        $key = 'ApiGiantBombGameReleases';
                }

                $result[$key] = $this->collApiGiantBombGameReleases->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombFirstGameLocations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombLocations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_location_vgaglos';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstGameLocations';
                }

                $result[$key] = $this->collApiGiantBombFirstGameLocations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombFirstGameObjects) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombObjects';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_object_vgagobs';
                        break;
                    default:
                        $key = 'ApiGiantBombFirstGameObjects';
                }

                $result[$key] = $this->collApiGiantBombFirstGameObjects->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombCreditedGamepeople) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombpeople';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_person_vgagpes';
                        break;
                    default:
                        $key = 'ApiGiantBombCreditedGamepeople';
                }

                $result[$key] = $this->collApiGiantBombCreditedGamepeople->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombImages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombImages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_image_vgagims';
                        break;
                    default:
                        $key = 'ApiGiantBombImages';
                }

                $result[$key] = $this->collApiGiantBombImages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collApiGiantBombVideos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'apiGiantBombVideos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'videogames_api_giantbomb_video_vgagvis';
                        break;
                    default:
                        $key = 'ApiGiantBombVideos';
                }

                $result[$key] = $this->collApiGiantBombVideos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApiGiantBombGameTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setGameId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setAliases($value);
                break;
            case 4:
                $this->setSummary($value);
                break;
            case 5:
                $this->setDescription($value);
                break;
            case 6:
                $this->setOriginalReleasedAt($value);
                break;
            case 7:
                $this->setExpectedDayReleasedAt($value);
                break;
            case 8:
                $this->setExpectedMonthReleasedAt($value);
                break;
            case 9:
                $this->setExpectedQuarterReleasedAt($value);
                break;
            case 10:
                $this->setExpectedYearReleasedAt($value);
                break;
            case 11:
                $this->setUserReviewsCount($value);
                break;
            case 12:
                $this->setImageIconUrl($value);
                break;
            case 13:
                $this->setImageMediumUrl($value);
                break;
            case 14:
                $this->setImageScreenUrl($value);
                break;
            case 15:
                $this->setImageSmallUrl($value);
                break;
            case 16:
                $this->setImageSuperUrl($value);
                break;
            case 17:
                $this->setImageThumbUrl($value);
                break;
            case 18:
                $this->setImageTinyUrl($value);
                break;
            case 19:
                $this->setApiDetailUrl($value);
                break;
            case 20:
                $this->setSiteDetailUrl($value);
                break;
            case 21:
                $this->setCreatedAt($value);
                break;
            case 22:
                $this->setUpdatedAt($value);
                break;
            case 23:
                $this->setRefreshedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApiGiantBombGameTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setGameId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAliases($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSummary($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDescription($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setOriginalReleasedAt($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setExpectedDayReleasedAt($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setExpectedMonthReleasedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setExpectedQuarterReleasedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setExpectedYearReleasedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUserReviewsCount($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImageIconUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setImageMediumUrl($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setImageScreenUrl($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setImageSmallUrl($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setImageSuperUrl($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setImageThumbUrl($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setImageTinyUrl($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setApiDetailUrl($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setSiteDetailUrl($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setCreatedAt($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setUpdatedAt($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setRefreshedAt($arr[$keys[23]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiGiantBombGameTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ID)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $this->vgagga_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_VGAGAM_ID, $this->vgagga_vgagam_id);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_NAME)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_NAME, $this->vgagga_name);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_ALIASES, $this->vgagga_aliases);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_SUMMARY, $this->vgagga_summary);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_DESCRIPTION, $this->vgagga_description);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_ORIGINAL_RELEASED_AT, $this->vgagga_original_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_DAY_RELEASED_AT, $this->vgagga_expected_day_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_MONTH_RELEASED_AT, $this->vgagga_expected_month_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_QUARTER_RELEASED_AT, $this->vgagga_expected_quarter_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_EXPECTED_YEAR_RELEASED_AT, $this->vgagga_expected_year_released_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_USER_REVIEWS_COUNT, $this->vgagga_user_reviews_count);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_ICON_URL, $this->vgagga_image_icon_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_MEDIUM_URL, $this->vgagga_image_medium_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SCREEN_URL, $this->vgagga_image_screen_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SMALL_URL, $this->vgagga_image_small_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_SUPER_URL, $this->vgagga_image_super_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_THUMB_URL, $this->vgagga_image_thumb_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_IMAGE_TINY_URL, $this->vgagga_image_tiny_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_API_DETAIL_URL, $this->vgagga_api_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_SITE_DETAIL_URL, $this->vgagga_site_detail_url);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_CREATED_AT, $this->vgagga_created_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_UPDATED_AT, $this->vgagga_updated_at);
        }
        if ($this->isColumnModified(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT)) {
            $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_REFRESHED_AT, $this->vgagga_refreshed_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApiGiantBombGameQuery::create();
        $criteria->add(ApiGiantBombGameTableMap::COL_VGAGGA_ID, $this->vgagga_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (vgagga_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setGameId($this->getGameId());
        $copyObj->setName($this->getName());
        $copyObj->setAliases($this->getAliases());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setOriginalReleasedAt($this->getOriginalReleasedAt());
        $copyObj->setExpectedDayReleasedAt($this->getExpectedDayReleasedAt());
        $copyObj->setExpectedMonthReleasedAt($this->getExpectedMonthReleasedAt());
        $copyObj->setExpectedQuarterReleasedAt($this->getExpectedQuarterReleasedAt());
        $copyObj->setExpectedYearReleasedAt($this->getExpectedYearReleasedAt());
        $copyObj->setUserReviewsCount($this->getUserReviewsCount());
        $copyObj->setImageIconUrl($this->getImageIconUrl());
        $copyObj->setImageMediumUrl($this->getImageMediumUrl());
        $copyObj->setImageScreenUrl($this->getImageScreenUrl());
        $copyObj->setImageSmallUrl($this->getImageSmallUrl());
        $copyObj->setImageSuperUrl($this->getImageSuperUrl());
        $copyObj->setImageThumbUrl($this->getImageThumbUrl());
        $copyObj->setImageTinyUrl($this->getImageTinyUrl());
        $copyObj->setApiDetailUrl($this->getApiDetailUrl());
        $copyObj->setSiteDetailUrl($this->getSiteDetailUrl());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setRefreshedAt($this->getRefreshedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getApiGiantBombFirstGameCharacters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombFirstGameCharacter($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombFirstGameConcepts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombFirstGameConcept($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameOriginalRatings() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameOriginalRating($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGamePlatforms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGamePlatform($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameDevelopers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameDeveloper($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGamePublishers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGamePublisher($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameFranchises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameFranchise($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameGenres() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameGenre($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombGameReleases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombGameRelease($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombFirstGameLocations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombFirstGameLocation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombFirstGameObjects() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombFirstGameObject($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombCreditedGamepeople() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombCreditedGamePerson($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombImages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombImage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getApiGiantBombVideos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addApiGiantBombVideo($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildGame object.
     *
     * @param  ChildGame $v
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     * @throws PropelException
     */
    public function setGame(ChildGame $v = null)
    {
        if ($v === null) {
            $this->setGameId(NULL);
        } else {
            $this->setGameId($v->getId());
        }

        $this->aGame = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildGame object, it will not be re-added.
        if ($v !== null) {
            $v->addApiGiantBombGame($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildGame object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildGame The associated ChildGame object.
     * @throws PropelException
     */
    public function getGame(ConnectionInterface $con = null)
    {
        if ($this->aGame === null && ($this->vgagga_vgagam_id !== null)) {
            $this->aGame = ChildGameQuery::create()->findPk($this->vgagga_vgagam_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aGame->addApiGiantBombGames($this);
             */
        }

        return $this->aGame;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ApiGiantBombFirstGameCharacter' == $relationName) {
            return $this->initApiGiantBombFirstGameCharacters();
        }
        if ('ApiGiantBombFirstGameConcept' == $relationName) {
            return $this->initApiGiantBombFirstGameConcepts();
        }
        if ('ApiGiantBombGameOriginalRating' == $relationName) {
            return $this->initApiGiantBombGameOriginalRatings();
        }
        if ('ApiGiantBombGamePlatform' == $relationName) {
            return $this->initApiGiantBombGamePlatforms();
        }
        if ('ApiGiantBombGameDeveloper' == $relationName) {
            return $this->initApiGiantBombGameDevelopers();
        }
        if ('ApiGiantBombGamePublisher' == $relationName) {
            return $this->initApiGiantBombGamePublishers();
        }
        if ('ApiGiantBombGameFranchise' == $relationName) {
            return $this->initApiGiantBombGameFranchises();
        }
        if ('ApiGiantBombGameGenre' == $relationName) {
            return $this->initApiGiantBombGameGenres();
        }
        if ('ApiGiantBombGameRelease' == $relationName) {
            return $this->initApiGiantBombGameReleases();
        }
        if ('ApiGiantBombFirstGameLocation' == $relationName) {
            return $this->initApiGiantBombFirstGameLocations();
        }
        if ('ApiGiantBombFirstGameObject' == $relationName) {
            return $this->initApiGiantBombFirstGameObjects();
        }
        if ('ApiGiantBombCreditedGamePerson' == $relationName) {
            return $this->initApiGiantBombCreditedGamepeople();
        }
        if ('ApiGiantBombImage' == $relationName) {
            return $this->initApiGiantBombImages();
        }
        if ('ApiGiantBombVideo' == $relationName) {
            return $this->initApiGiantBombVideos();
        }
    }

    /**
     * Clears out the collApiGiantBombFirstGameCharacters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombFirstGameCharacters()
     */
    public function clearApiGiantBombFirstGameCharacters()
    {
        $this->collApiGiantBombFirstGameCharacters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombFirstGameCharacters collection loaded partially.
     */
    public function resetPartialApiGiantBombFirstGameCharacters($v = true)
    {
        $this->collApiGiantBombFirstGameCharactersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombFirstGameCharacters collection.
     *
     * By default this just sets the collApiGiantBombFirstGameCharacters collection to an empty array (like clearcollApiGiantBombFirstGameCharacters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombFirstGameCharacters($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombFirstGameCharacters && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombCharacterTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombFirstGameCharacters = new $collectionClassName;
        $this->collApiGiantBombFirstGameCharacters->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombCharacter');
    }

    /**
     * Gets an array of ChildApiGiantBombCharacter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombCharacter[] List of ChildApiGiantBombCharacter objects
     * @throws PropelException
     */
    public function getApiGiantBombFirstGameCharacters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameCharactersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameCharacters || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameCharacters) {
                // return empty collection
                $this->initApiGiantBombFirstGameCharacters();
            } else {
                $collApiGiantBombFirstGameCharacters = ChildApiGiantBombCharacterQuery::create(null, $criteria)
                    ->filterByApiGiantBombFirstGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombFirstGameCharactersPartial && count($collApiGiantBombFirstGameCharacters)) {
                        $this->initApiGiantBombFirstGameCharacters(false);

                        foreach ($collApiGiantBombFirstGameCharacters as $obj) {
                            if (false == $this->collApiGiantBombFirstGameCharacters->contains($obj)) {
                                $this->collApiGiantBombFirstGameCharacters->append($obj);
                            }
                        }

                        $this->collApiGiantBombFirstGameCharactersPartial = true;
                    }

                    return $collApiGiantBombFirstGameCharacters;
                }

                if ($partial && $this->collApiGiantBombFirstGameCharacters) {
                    foreach ($this->collApiGiantBombFirstGameCharacters as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombFirstGameCharacters[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombFirstGameCharacters = $collApiGiantBombFirstGameCharacters;
                $this->collApiGiantBombFirstGameCharactersPartial = false;
            }
        }

        return $this->collApiGiantBombFirstGameCharacters;
    }

    /**
     * Sets a collection of ChildApiGiantBombCharacter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombFirstGameCharacters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombFirstGameCharacters(Collection $apiGiantBombFirstGameCharacters, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombCharacter[] $apiGiantBombFirstGameCharactersToDelete */
        $apiGiantBombFirstGameCharactersToDelete = $this->getApiGiantBombFirstGameCharacters(new Criteria(), $con)->diff($apiGiantBombFirstGameCharacters);


        $this->apiGiantBombFirstGameCharactersScheduledForDeletion = $apiGiantBombFirstGameCharactersToDelete;

        foreach ($apiGiantBombFirstGameCharactersToDelete as $apiGiantBombFirstGameCharacterRemoved) {
            $apiGiantBombFirstGameCharacterRemoved->setApiGiantBombFirstGame(null);
        }

        $this->collApiGiantBombFirstGameCharacters = null;
        foreach ($apiGiantBombFirstGameCharacters as $apiGiantBombFirstGameCharacter) {
            $this->addApiGiantBombFirstGameCharacter($apiGiantBombFirstGameCharacter);
        }

        $this->collApiGiantBombFirstGameCharacters = $apiGiantBombFirstGameCharacters;
        $this->collApiGiantBombFirstGameCharactersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombCharacter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombCharacter objects.
     * @throws PropelException
     */
    public function countApiGiantBombFirstGameCharacters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameCharactersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameCharacters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameCharacters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombFirstGameCharacters());
            }

            $query = ChildApiGiantBombCharacterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFirstGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombFirstGameCharacters);
    }

    /**
     * Method called to associate a ChildApiGiantBombCharacter object to this object
     * through the ChildApiGiantBombCharacter foreign key attribute.
     *
     * @param  ChildApiGiantBombCharacter $l ChildApiGiantBombCharacter
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombFirstGameCharacter(ChildApiGiantBombCharacter $l)
    {
        if ($this->collApiGiantBombFirstGameCharacters === null) {
            $this->initApiGiantBombFirstGameCharacters();
            $this->collApiGiantBombFirstGameCharactersPartial = true;
        }

        if (!$this->collApiGiantBombFirstGameCharacters->contains($l)) {
            $this->doAddApiGiantBombFirstGameCharacter($l);

            if ($this->apiGiantBombFirstGameCharactersScheduledForDeletion and $this->apiGiantBombFirstGameCharactersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombFirstGameCharactersScheduledForDeletion->remove($this->apiGiantBombFirstGameCharactersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombCharacter $apiGiantBombFirstGameCharacter The ChildApiGiantBombCharacter object to add.
     */
    protected function doAddApiGiantBombFirstGameCharacter(ChildApiGiantBombCharacter $apiGiantBombFirstGameCharacter)
    {
        $this->collApiGiantBombFirstGameCharacters[]= $apiGiantBombFirstGameCharacter;
        $apiGiantBombFirstGameCharacter->setApiGiantBombFirstGame($this);
    }

    /**
     * @param  ChildApiGiantBombCharacter $apiGiantBombFirstGameCharacter The ChildApiGiantBombCharacter object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombFirstGameCharacter(ChildApiGiantBombCharacter $apiGiantBombFirstGameCharacter)
    {
        if ($this->getApiGiantBombFirstGameCharacters()->contains($apiGiantBombFirstGameCharacter)) {
            $pos = $this->collApiGiantBombFirstGameCharacters->search($apiGiantBombFirstGameCharacter);
            $this->collApiGiantBombFirstGameCharacters->remove($pos);
            if (null === $this->apiGiantBombFirstGameCharactersScheduledForDeletion) {
                $this->apiGiantBombFirstGameCharactersScheduledForDeletion = clone $this->collApiGiantBombFirstGameCharacters;
                $this->apiGiantBombFirstGameCharactersScheduledForDeletion->clear();
            }
            $this->apiGiantBombFirstGameCharactersScheduledForDeletion[]= $apiGiantBombFirstGameCharacter;
            $apiGiantBombFirstGameCharacter->setApiGiantBombFirstGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombFirstGameConcepts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombFirstGameConcepts()
     */
    public function clearApiGiantBombFirstGameConcepts()
    {
        $this->collApiGiantBombFirstGameConcepts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombFirstGameConcepts collection loaded partially.
     */
    public function resetPartialApiGiantBombFirstGameConcepts($v = true)
    {
        $this->collApiGiantBombFirstGameConceptsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombFirstGameConcepts collection.
     *
     * By default this just sets the collApiGiantBombFirstGameConcepts collection to an empty array (like clearcollApiGiantBombFirstGameConcepts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombFirstGameConcepts($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombFirstGameConcepts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombConceptTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombFirstGameConcepts = new $collectionClassName;
        $this->collApiGiantBombFirstGameConcepts->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombConcept');
    }

    /**
     * Gets an array of ChildApiGiantBombConcept objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombConcept[] List of ChildApiGiantBombConcept objects
     * @throws PropelException
     */
    public function getApiGiantBombFirstGameConcepts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameConceptsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameConcepts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameConcepts) {
                // return empty collection
                $this->initApiGiantBombFirstGameConcepts();
            } else {
                $collApiGiantBombFirstGameConcepts = ChildApiGiantBombConceptQuery::create(null, $criteria)
                    ->filterByApiGiantBombFirstGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombFirstGameConceptsPartial && count($collApiGiantBombFirstGameConcepts)) {
                        $this->initApiGiantBombFirstGameConcepts(false);

                        foreach ($collApiGiantBombFirstGameConcepts as $obj) {
                            if (false == $this->collApiGiantBombFirstGameConcepts->contains($obj)) {
                                $this->collApiGiantBombFirstGameConcepts->append($obj);
                            }
                        }

                        $this->collApiGiantBombFirstGameConceptsPartial = true;
                    }

                    return $collApiGiantBombFirstGameConcepts;
                }

                if ($partial && $this->collApiGiantBombFirstGameConcepts) {
                    foreach ($this->collApiGiantBombFirstGameConcepts as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombFirstGameConcepts[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombFirstGameConcepts = $collApiGiantBombFirstGameConcepts;
                $this->collApiGiantBombFirstGameConceptsPartial = false;
            }
        }

        return $this->collApiGiantBombFirstGameConcepts;
    }

    /**
     * Sets a collection of ChildApiGiantBombConcept objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombFirstGameConcepts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombFirstGameConcepts(Collection $apiGiantBombFirstGameConcepts, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombConcept[] $apiGiantBombFirstGameConceptsToDelete */
        $apiGiantBombFirstGameConceptsToDelete = $this->getApiGiantBombFirstGameConcepts(new Criteria(), $con)->diff($apiGiantBombFirstGameConcepts);


        $this->apiGiantBombFirstGameConceptsScheduledForDeletion = $apiGiantBombFirstGameConceptsToDelete;

        foreach ($apiGiantBombFirstGameConceptsToDelete as $apiGiantBombFirstGameConceptRemoved) {
            $apiGiantBombFirstGameConceptRemoved->setApiGiantBombFirstGame(null);
        }

        $this->collApiGiantBombFirstGameConcepts = null;
        foreach ($apiGiantBombFirstGameConcepts as $apiGiantBombFirstGameConcept) {
            $this->addApiGiantBombFirstGameConcept($apiGiantBombFirstGameConcept);
        }

        $this->collApiGiantBombFirstGameConcepts = $apiGiantBombFirstGameConcepts;
        $this->collApiGiantBombFirstGameConceptsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombConcept objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombConcept objects.
     * @throws PropelException
     */
    public function countApiGiantBombFirstGameConcepts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameConceptsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameConcepts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameConcepts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombFirstGameConcepts());
            }

            $query = ChildApiGiantBombConceptQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFirstGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombFirstGameConcepts);
    }

    /**
     * Method called to associate a ChildApiGiantBombConcept object to this object
     * through the ChildApiGiantBombConcept foreign key attribute.
     *
     * @param  ChildApiGiantBombConcept $l ChildApiGiantBombConcept
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombFirstGameConcept(ChildApiGiantBombConcept $l)
    {
        if ($this->collApiGiantBombFirstGameConcepts === null) {
            $this->initApiGiantBombFirstGameConcepts();
            $this->collApiGiantBombFirstGameConceptsPartial = true;
        }

        if (!$this->collApiGiantBombFirstGameConcepts->contains($l)) {
            $this->doAddApiGiantBombFirstGameConcept($l);

            if ($this->apiGiantBombFirstGameConceptsScheduledForDeletion and $this->apiGiantBombFirstGameConceptsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombFirstGameConceptsScheduledForDeletion->remove($this->apiGiantBombFirstGameConceptsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombConcept $apiGiantBombFirstGameConcept The ChildApiGiantBombConcept object to add.
     */
    protected function doAddApiGiantBombFirstGameConcept(ChildApiGiantBombConcept $apiGiantBombFirstGameConcept)
    {
        $this->collApiGiantBombFirstGameConcepts[]= $apiGiantBombFirstGameConcept;
        $apiGiantBombFirstGameConcept->setApiGiantBombFirstGame($this);
    }

    /**
     * @param  ChildApiGiantBombConcept $apiGiantBombFirstGameConcept The ChildApiGiantBombConcept object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombFirstGameConcept(ChildApiGiantBombConcept $apiGiantBombFirstGameConcept)
    {
        if ($this->getApiGiantBombFirstGameConcepts()->contains($apiGiantBombFirstGameConcept)) {
            $pos = $this->collApiGiantBombFirstGameConcepts->search($apiGiantBombFirstGameConcept);
            $this->collApiGiantBombFirstGameConcepts->remove($pos);
            if (null === $this->apiGiantBombFirstGameConceptsScheduledForDeletion) {
                $this->apiGiantBombFirstGameConceptsScheduledForDeletion = clone $this->collApiGiantBombFirstGameConcepts;
                $this->apiGiantBombFirstGameConceptsScheduledForDeletion->clear();
            }
            $this->apiGiantBombFirstGameConceptsScheduledForDeletion[]= $apiGiantBombFirstGameConcept;
            $apiGiantBombFirstGameConcept->setApiGiantBombFirstGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombFirstGameConcepts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombConcept[] List of ChildApiGiantBombConcept objects
     */
    public function getApiGiantBombFirstGameConceptsJoinApiGiantBombFirstFranchise(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombConceptQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombFirstFranchise', $joinBehavior);

        return $this->getApiGiantBombFirstGameConcepts($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameOriginalRatings collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameOriginalRatings()
     */
    public function clearApiGiantBombGameOriginalRatings()
    {
        $this->collApiGiantBombGameOriginalRatings = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameOriginalRatings collection loaded partially.
     */
    public function resetPartialApiGiantBombGameOriginalRatings($v = true)
    {
        $this->collApiGiantBombGameOriginalRatingsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameOriginalRatings collection.
     *
     * By default this just sets the collApiGiantBombGameOriginalRatings collection to an empty array (like clearcollApiGiantBombGameOriginalRatings());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameOriginalRatings($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameOriginalRatings && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameOriginalRatingTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameOriginalRatings = new $collectionClassName;
        $this->collApiGiantBombGameOriginalRatings->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameOriginalRating');
    }

    /**
     * Gets an array of ChildApiGiantBombGameOriginalRating objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameOriginalRating[] List of ChildApiGiantBombGameOriginalRating objects
     * @throws PropelException
     */
    public function getApiGiantBombGameOriginalRatings(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameOriginalRatingsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameOriginalRatings || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameOriginalRatings) {
                // return empty collection
                $this->initApiGiantBombGameOriginalRatings();
            } else {
                $collApiGiantBombGameOriginalRatings = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameOriginalRatingsPartial && count($collApiGiantBombGameOriginalRatings)) {
                        $this->initApiGiantBombGameOriginalRatings(false);

                        foreach ($collApiGiantBombGameOriginalRatings as $obj) {
                            if (false == $this->collApiGiantBombGameOriginalRatings->contains($obj)) {
                                $this->collApiGiantBombGameOriginalRatings->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameOriginalRatingsPartial = true;
                    }

                    return $collApiGiantBombGameOriginalRatings;
                }

                if ($partial && $this->collApiGiantBombGameOriginalRatings) {
                    foreach ($this->collApiGiantBombGameOriginalRatings as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameOriginalRatings[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameOriginalRatings = $collApiGiantBombGameOriginalRatings;
                $this->collApiGiantBombGameOriginalRatingsPartial = false;
            }
        }

        return $this->collApiGiantBombGameOriginalRatings;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameOriginalRating objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameOriginalRatings A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGameOriginalRatings(Collection $apiGiantBombGameOriginalRatings, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameOriginalRating[] $apiGiantBombGameOriginalRatingsToDelete */
        $apiGiantBombGameOriginalRatingsToDelete = $this->getApiGiantBombGameOriginalRatings(new Criteria(), $con)->diff($apiGiantBombGameOriginalRatings);


        $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = $apiGiantBombGameOriginalRatingsToDelete;

        foreach ($apiGiantBombGameOriginalRatingsToDelete as $apiGiantBombGameOriginalRatingRemoved) {
            $apiGiantBombGameOriginalRatingRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGameOriginalRatings = null;
        foreach ($apiGiantBombGameOriginalRatings as $apiGiantBombGameOriginalRating) {
            $this->addApiGiantBombGameOriginalRating($apiGiantBombGameOriginalRating);
        }

        $this->collApiGiantBombGameOriginalRatings = $apiGiantBombGameOriginalRatings;
        $this->collApiGiantBombGameOriginalRatingsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameOriginalRating objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameOriginalRating objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameOriginalRatings(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameOriginalRatingsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameOriginalRatings || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameOriginalRatings) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameOriginalRatings());
            }

            $query = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameOriginalRatings);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameOriginalRating object to this object
     * through the ChildApiGiantBombGameOriginalRating foreign key attribute.
     *
     * @param  ChildApiGiantBombGameOriginalRating $l ChildApiGiantBombGameOriginalRating
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $l)
    {
        if ($this->collApiGiantBombGameOriginalRatings === null) {
            $this->initApiGiantBombGameOriginalRatings();
            $this->collApiGiantBombGameOriginalRatingsPartial = true;
        }

        if (!$this->collApiGiantBombGameOriginalRatings->contains($l)) {
            $this->doAddApiGiantBombGameOriginalRating($l);

            if ($this->apiGiantBombGameOriginalRatingsScheduledForDeletion and $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->remove($this->apiGiantBombGameOriginalRatingsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating The ChildApiGiantBombGameOriginalRating object to add.
     */
    protected function doAddApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating)
    {
        $this->collApiGiantBombGameOriginalRatings[]= $apiGiantBombGameOriginalRating;
        $apiGiantBombGameOriginalRating->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating The ChildApiGiantBombGameOriginalRating object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGameOriginalRating(ChildApiGiantBombGameOriginalRating $apiGiantBombGameOriginalRating)
    {
        if ($this->getApiGiantBombGameOriginalRatings()->contains($apiGiantBombGameOriginalRating)) {
            $pos = $this->collApiGiantBombGameOriginalRatings->search($apiGiantBombGameOriginalRating);
            $this->collApiGiantBombGameOriginalRatings->remove($pos);
            if (null === $this->apiGiantBombGameOriginalRatingsScheduledForDeletion) {
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion = clone $this->collApiGiantBombGameOriginalRatings;
                $this->apiGiantBombGameOriginalRatingsScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameOriginalRatingsScheduledForDeletion[]= clone $apiGiantBombGameOriginalRating;
            $apiGiantBombGameOriginalRating->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameOriginalRatings from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameOriginalRating[] List of ChildApiGiantBombGameOriginalRating objects
     */
    public function getApiGiantBombGameOriginalRatingsJoinApiGiantBombGameRating(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameOriginalRatingQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRating', $joinBehavior);

        return $this->getApiGiantBombGameOriginalRatings($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGamePlatforms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGamePlatforms()
     */
    public function clearApiGiantBombGamePlatforms()
    {
        $this->collApiGiantBombGamePlatforms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGamePlatforms collection loaded partially.
     */
    public function resetPartialApiGiantBombGamePlatforms($v = true)
    {
        $this->collApiGiantBombGamePlatformsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGamePlatforms collection.
     *
     * By default this just sets the collApiGiantBombGamePlatforms collection to an empty array (like clearcollApiGiantBombGamePlatforms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGamePlatforms($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGamePlatforms && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGamePlatformTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGamePlatforms = new $collectionClassName;
        $this->collApiGiantBombGamePlatforms->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePlatform');
    }

    /**
     * Gets an array of ChildApiGiantBombGamePlatform objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGamePlatform[] List of ChildApiGiantBombGamePlatform objects
     * @throws PropelException
     */
    public function getApiGiantBombGamePlatforms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePlatforms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePlatforms) {
                // return empty collection
                $this->initApiGiantBombGamePlatforms();
            } else {
                $collApiGiantBombGamePlatforms = ChildApiGiantBombGamePlatformQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGamePlatformsPartial && count($collApiGiantBombGamePlatforms)) {
                        $this->initApiGiantBombGamePlatforms(false);

                        foreach ($collApiGiantBombGamePlatforms as $obj) {
                            if (false == $this->collApiGiantBombGamePlatforms->contains($obj)) {
                                $this->collApiGiantBombGamePlatforms->append($obj);
                            }
                        }

                        $this->collApiGiantBombGamePlatformsPartial = true;
                    }

                    return $collApiGiantBombGamePlatforms;
                }

                if ($partial && $this->collApiGiantBombGamePlatforms) {
                    foreach ($this->collApiGiantBombGamePlatforms as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGamePlatforms[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGamePlatforms = $collApiGiantBombGamePlatforms;
                $this->collApiGiantBombGamePlatformsPartial = false;
            }
        }

        return $this->collApiGiantBombGamePlatforms;
    }

    /**
     * Sets a collection of ChildApiGiantBombGamePlatform objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGamePlatforms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGamePlatforms(Collection $apiGiantBombGamePlatforms, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGamePlatform[] $apiGiantBombGamePlatformsToDelete */
        $apiGiantBombGamePlatformsToDelete = $this->getApiGiantBombGamePlatforms(new Criteria(), $con)->diff($apiGiantBombGamePlatforms);


        $this->apiGiantBombGamePlatformsScheduledForDeletion = $apiGiantBombGamePlatformsToDelete;

        foreach ($apiGiantBombGamePlatformsToDelete as $apiGiantBombGamePlatformRemoved) {
            $apiGiantBombGamePlatformRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGamePlatforms = null;
        foreach ($apiGiantBombGamePlatforms as $apiGiantBombGamePlatform) {
            $this->addApiGiantBombGamePlatform($apiGiantBombGamePlatform);
        }

        $this->collApiGiantBombGamePlatforms = $apiGiantBombGamePlatforms;
        $this->collApiGiantBombGamePlatformsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGamePlatform objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGamePlatform objects.
     * @throws PropelException
     */
    public function countApiGiantBombGamePlatforms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePlatformsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePlatforms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePlatforms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGamePlatforms());
            }

            $query = ChildApiGiantBombGamePlatformQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGamePlatforms);
    }

    /**
     * Method called to associate a ChildApiGiantBombGamePlatform object to this object
     * through the ChildApiGiantBombGamePlatform foreign key attribute.
     *
     * @param  ChildApiGiantBombGamePlatform $l ChildApiGiantBombGamePlatform
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $l)
    {
        if ($this->collApiGiantBombGamePlatforms === null) {
            $this->initApiGiantBombGamePlatforms();
            $this->collApiGiantBombGamePlatformsPartial = true;
        }

        if (!$this->collApiGiantBombGamePlatforms->contains($l)) {
            $this->doAddApiGiantBombGamePlatform($l);

            if ($this->apiGiantBombGamePlatformsScheduledForDeletion and $this->apiGiantBombGamePlatformsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGamePlatformsScheduledForDeletion->remove($this->apiGiantBombGamePlatformsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform The ChildApiGiantBombGamePlatform object to add.
     */
    protected function doAddApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform)
    {
        $this->collApiGiantBombGamePlatforms[]= $apiGiantBombGamePlatform;
        $apiGiantBombGamePlatform->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform The ChildApiGiantBombGamePlatform object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGamePlatform(ChildApiGiantBombGamePlatform $apiGiantBombGamePlatform)
    {
        if ($this->getApiGiantBombGamePlatforms()->contains($apiGiantBombGamePlatform)) {
            $pos = $this->collApiGiantBombGamePlatforms->search($apiGiantBombGamePlatform);
            $this->collApiGiantBombGamePlatforms->remove($pos);
            if (null === $this->apiGiantBombGamePlatformsScheduledForDeletion) {
                $this->apiGiantBombGamePlatformsScheduledForDeletion = clone $this->collApiGiantBombGamePlatforms;
                $this->apiGiantBombGamePlatformsScheduledForDeletion->clear();
            }
            $this->apiGiantBombGamePlatformsScheduledForDeletion[]= clone $apiGiantBombGamePlatform;
            $apiGiantBombGamePlatform->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGamePlatforms from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGamePlatform[] List of ChildApiGiantBombGamePlatform objects
     */
    public function getApiGiantBombGamePlatformsJoinApiGiantBombPlatform(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGamePlatformQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombPlatform', $joinBehavior);

        return $this->getApiGiantBombGamePlatforms($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameDevelopers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameDevelopers()
     */
    public function clearApiGiantBombGameDevelopers()
    {
        $this->collApiGiantBombGameDevelopers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameDevelopers collection loaded partially.
     */
    public function resetPartialApiGiantBombGameDevelopers($v = true)
    {
        $this->collApiGiantBombGameDevelopersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameDevelopers collection.
     *
     * By default this just sets the collApiGiantBombGameDevelopers collection to an empty array (like clearcollApiGiantBombGameDevelopers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameDevelopers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameDevelopers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameDeveloperTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameDevelopers = new $collectionClassName;
        $this->collApiGiantBombGameDevelopers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameDeveloper');
    }

    /**
     * Gets an array of ChildApiGiantBombGameDeveloper objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameDeveloper[] List of ChildApiGiantBombGameDeveloper objects
     * @throws PropelException
     */
    public function getApiGiantBombGameDevelopers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameDevelopers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameDevelopers) {
                // return empty collection
                $this->initApiGiantBombGameDevelopers();
            } else {
                $collApiGiantBombGameDevelopers = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameDevelopersPartial && count($collApiGiantBombGameDevelopers)) {
                        $this->initApiGiantBombGameDevelopers(false);

                        foreach ($collApiGiantBombGameDevelopers as $obj) {
                            if (false == $this->collApiGiantBombGameDevelopers->contains($obj)) {
                                $this->collApiGiantBombGameDevelopers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameDevelopersPartial = true;
                    }

                    return $collApiGiantBombGameDevelopers;
                }

                if ($partial && $this->collApiGiantBombGameDevelopers) {
                    foreach ($this->collApiGiantBombGameDevelopers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameDevelopers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameDevelopers = $collApiGiantBombGameDevelopers;
                $this->collApiGiantBombGameDevelopersPartial = false;
            }
        }

        return $this->collApiGiantBombGameDevelopers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameDeveloper objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameDevelopers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGameDevelopers(Collection $apiGiantBombGameDevelopers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameDeveloper[] $apiGiantBombGameDevelopersToDelete */
        $apiGiantBombGameDevelopersToDelete = $this->getApiGiantBombGameDevelopers(new Criteria(), $con)->diff($apiGiantBombGameDevelopers);


        $this->apiGiantBombGameDevelopersScheduledForDeletion = $apiGiantBombGameDevelopersToDelete;

        foreach ($apiGiantBombGameDevelopersToDelete as $apiGiantBombGameDeveloperRemoved) {
            $apiGiantBombGameDeveloperRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGameDevelopers = null;
        foreach ($apiGiantBombGameDevelopers as $apiGiantBombGameDeveloper) {
            $this->addApiGiantBombGameDeveloper($apiGiantBombGameDeveloper);
        }

        $this->collApiGiantBombGameDevelopers = $apiGiantBombGameDevelopers;
        $this->collApiGiantBombGameDevelopersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameDeveloper objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameDeveloper objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameDevelopers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameDevelopersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameDevelopers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameDevelopers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameDevelopers());
            }

            $query = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameDevelopers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameDeveloper object to this object
     * through the ChildApiGiantBombGameDeveloper foreign key attribute.
     *
     * @param  ChildApiGiantBombGameDeveloper $l ChildApiGiantBombGameDeveloper
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $l)
    {
        if ($this->collApiGiantBombGameDevelopers === null) {
            $this->initApiGiantBombGameDevelopers();
            $this->collApiGiantBombGameDevelopersPartial = true;
        }

        if (!$this->collApiGiantBombGameDevelopers->contains($l)) {
            $this->doAddApiGiantBombGameDeveloper($l);

            if ($this->apiGiantBombGameDevelopersScheduledForDeletion and $this->apiGiantBombGameDevelopersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameDevelopersScheduledForDeletion->remove($this->apiGiantBombGameDevelopersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper The ChildApiGiantBombGameDeveloper object to add.
     */
    protected function doAddApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper)
    {
        $this->collApiGiantBombGameDevelopers[]= $apiGiantBombGameDeveloper;
        $apiGiantBombGameDeveloper->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper The ChildApiGiantBombGameDeveloper object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGameDeveloper(ChildApiGiantBombGameDeveloper $apiGiantBombGameDeveloper)
    {
        if ($this->getApiGiantBombGameDevelopers()->contains($apiGiantBombGameDeveloper)) {
            $pos = $this->collApiGiantBombGameDevelopers->search($apiGiantBombGameDeveloper);
            $this->collApiGiantBombGameDevelopers->remove($pos);
            if (null === $this->apiGiantBombGameDevelopersScheduledForDeletion) {
                $this->apiGiantBombGameDevelopersScheduledForDeletion = clone $this->collApiGiantBombGameDevelopers;
                $this->apiGiantBombGameDevelopersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameDevelopersScheduledForDeletion[]= clone $apiGiantBombGameDeveloper;
            $apiGiantBombGameDeveloper->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameDevelopers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameDeveloper[] List of ChildApiGiantBombGameDeveloper objects
     */
    public function getApiGiantBombGameDevelopersJoinApiGiantBombCompany(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameDeveloperQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombCompany', $joinBehavior);

        return $this->getApiGiantBombGameDevelopers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGamePublishers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGamePublishers()
     */
    public function clearApiGiantBombGamePublishers()
    {
        $this->collApiGiantBombGamePublishers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGamePublishers collection loaded partially.
     */
    public function resetPartialApiGiantBombGamePublishers($v = true)
    {
        $this->collApiGiantBombGamePublishersPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGamePublishers collection.
     *
     * By default this just sets the collApiGiantBombGamePublishers collection to an empty array (like clearcollApiGiantBombGamePublishers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGamePublishers($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGamePublishers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGamePublisherTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGamePublishers = new $collectionClassName;
        $this->collApiGiantBombGamePublishers->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGamePublisher');
    }

    /**
     * Gets an array of ChildApiGiantBombGamePublisher objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGamePublisher[] List of ChildApiGiantBombGamePublisher objects
     * @throws PropelException
     */
    public function getApiGiantBombGamePublishers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePublishers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePublishers) {
                // return empty collection
                $this->initApiGiantBombGamePublishers();
            } else {
                $collApiGiantBombGamePublishers = ChildApiGiantBombGamePublisherQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGamePublishersPartial && count($collApiGiantBombGamePublishers)) {
                        $this->initApiGiantBombGamePublishers(false);

                        foreach ($collApiGiantBombGamePublishers as $obj) {
                            if (false == $this->collApiGiantBombGamePublishers->contains($obj)) {
                                $this->collApiGiantBombGamePublishers->append($obj);
                            }
                        }

                        $this->collApiGiantBombGamePublishersPartial = true;
                    }

                    return $collApiGiantBombGamePublishers;
                }

                if ($partial && $this->collApiGiantBombGamePublishers) {
                    foreach ($this->collApiGiantBombGamePublishers as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGamePublishers[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGamePublishers = $collApiGiantBombGamePublishers;
                $this->collApiGiantBombGamePublishersPartial = false;
            }
        }

        return $this->collApiGiantBombGamePublishers;
    }

    /**
     * Sets a collection of ChildApiGiantBombGamePublisher objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGamePublishers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGamePublishers(Collection $apiGiantBombGamePublishers, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGamePublisher[] $apiGiantBombGamePublishersToDelete */
        $apiGiantBombGamePublishersToDelete = $this->getApiGiantBombGamePublishers(new Criteria(), $con)->diff($apiGiantBombGamePublishers);


        $this->apiGiantBombGamePublishersScheduledForDeletion = $apiGiantBombGamePublishersToDelete;

        foreach ($apiGiantBombGamePublishersToDelete as $apiGiantBombGamePublisherRemoved) {
            $apiGiantBombGamePublisherRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGamePublishers = null;
        foreach ($apiGiantBombGamePublishers as $apiGiantBombGamePublisher) {
            $this->addApiGiantBombGamePublisher($apiGiantBombGamePublisher);
        }

        $this->collApiGiantBombGamePublishers = $apiGiantBombGamePublishers;
        $this->collApiGiantBombGamePublishersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGamePublisher objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGamePublisher objects.
     * @throws PropelException
     */
    public function countApiGiantBombGamePublishers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGamePublishersPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGamePublishers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGamePublishers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGamePublishers());
            }

            $query = ChildApiGiantBombGamePublisherQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGamePublishers);
    }

    /**
     * Method called to associate a ChildApiGiantBombGamePublisher object to this object
     * through the ChildApiGiantBombGamePublisher foreign key attribute.
     *
     * @param  ChildApiGiantBombGamePublisher $l ChildApiGiantBombGamePublisher
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $l)
    {
        if ($this->collApiGiantBombGamePublishers === null) {
            $this->initApiGiantBombGamePublishers();
            $this->collApiGiantBombGamePublishersPartial = true;
        }

        if (!$this->collApiGiantBombGamePublishers->contains($l)) {
            $this->doAddApiGiantBombGamePublisher($l);

            if ($this->apiGiantBombGamePublishersScheduledForDeletion and $this->apiGiantBombGamePublishersScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGamePublishersScheduledForDeletion->remove($this->apiGiantBombGamePublishersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher The ChildApiGiantBombGamePublisher object to add.
     */
    protected function doAddApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher)
    {
        $this->collApiGiantBombGamePublishers[]= $apiGiantBombGamePublisher;
        $apiGiantBombGamePublisher->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher The ChildApiGiantBombGamePublisher object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGamePublisher(ChildApiGiantBombGamePublisher $apiGiantBombGamePublisher)
    {
        if ($this->getApiGiantBombGamePublishers()->contains($apiGiantBombGamePublisher)) {
            $pos = $this->collApiGiantBombGamePublishers->search($apiGiantBombGamePublisher);
            $this->collApiGiantBombGamePublishers->remove($pos);
            if (null === $this->apiGiantBombGamePublishersScheduledForDeletion) {
                $this->apiGiantBombGamePublishersScheduledForDeletion = clone $this->collApiGiantBombGamePublishers;
                $this->apiGiantBombGamePublishersScheduledForDeletion->clear();
            }
            $this->apiGiantBombGamePublishersScheduledForDeletion[]= clone $apiGiantBombGamePublisher;
            $apiGiantBombGamePublisher->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGamePublishers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGamePublisher[] List of ChildApiGiantBombGamePublisher objects
     */
    public function getApiGiantBombGamePublishersJoinApiGiantBombCompany(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGamePublisherQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombCompany', $joinBehavior);

        return $this->getApiGiantBombGamePublishers($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameFranchises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameFranchises()
     */
    public function clearApiGiantBombGameFranchises()
    {
        $this->collApiGiantBombGameFranchises = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameFranchises collection loaded partially.
     */
    public function resetPartialApiGiantBombGameFranchises($v = true)
    {
        $this->collApiGiantBombGameFranchisesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameFranchises collection.
     *
     * By default this just sets the collApiGiantBombGameFranchises collection to an empty array (like clearcollApiGiantBombGameFranchises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameFranchises($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameFranchises && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameFranchiseTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameFranchises = new $collectionClassName;
        $this->collApiGiantBombGameFranchises->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameFranchise');
    }

    /**
     * Gets an array of ChildApiGiantBombGameFranchise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameFranchise[] List of ChildApiGiantBombGameFranchise objects
     * @throws PropelException
     */
    public function getApiGiantBombGameFranchises(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameFranchisesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameFranchises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameFranchises) {
                // return empty collection
                $this->initApiGiantBombGameFranchises();
            } else {
                $collApiGiantBombGameFranchises = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameFranchisesPartial && count($collApiGiantBombGameFranchises)) {
                        $this->initApiGiantBombGameFranchises(false);

                        foreach ($collApiGiantBombGameFranchises as $obj) {
                            if (false == $this->collApiGiantBombGameFranchises->contains($obj)) {
                                $this->collApiGiantBombGameFranchises->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameFranchisesPartial = true;
                    }

                    return $collApiGiantBombGameFranchises;
                }

                if ($partial && $this->collApiGiantBombGameFranchises) {
                    foreach ($this->collApiGiantBombGameFranchises as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameFranchises[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameFranchises = $collApiGiantBombGameFranchises;
                $this->collApiGiantBombGameFranchisesPartial = false;
            }
        }

        return $this->collApiGiantBombGameFranchises;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameFranchise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameFranchises A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGameFranchises(Collection $apiGiantBombGameFranchises, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameFranchise[] $apiGiantBombGameFranchisesToDelete */
        $apiGiantBombGameFranchisesToDelete = $this->getApiGiantBombGameFranchises(new Criteria(), $con)->diff($apiGiantBombGameFranchises);


        $this->apiGiantBombGameFranchisesScheduledForDeletion = $apiGiantBombGameFranchisesToDelete;

        foreach ($apiGiantBombGameFranchisesToDelete as $apiGiantBombGameFranchiseRemoved) {
            $apiGiantBombGameFranchiseRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGameFranchises = null;
        foreach ($apiGiantBombGameFranchises as $apiGiantBombGameFranchise) {
            $this->addApiGiantBombGameFranchise($apiGiantBombGameFranchise);
        }

        $this->collApiGiantBombGameFranchises = $apiGiantBombGameFranchises;
        $this->collApiGiantBombGameFranchisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameFranchise objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameFranchise objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameFranchises(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameFranchisesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameFranchises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameFranchises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameFranchises());
            }

            $query = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameFranchises);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameFranchise object to this object
     * through the ChildApiGiantBombGameFranchise foreign key attribute.
     *
     * @param  ChildApiGiantBombGameFranchise $l ChildApiGiantBombGameFranchise
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $l)
    {
        if ($this->collApiGiantBombGameFranchises === null) {
            $this->initApiGiantBombGameFranchises();
            $this->collApiGiantBombGameFranchisesPartial = true;
        }

        if (!$this->collApiGiantBombGameFranchises->contains($l)) {
            $this->doAddApiGiantBombGameFranchise($l);

            if ($this->apiGiantBombGameFranchisesScheduledForDeletion and $this->apiGiantBombGameFranchisesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameFranchisesScheduledForDeletion->remove($this->apiGiantBombGameFranchisesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise The ChildApiGiantBombGameFranchise object to add.
     */
    protected function doAddApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise)
    {
        $this->collApiGiantBombGameFranchises[]= $apiGiantBombGameFranchise;
        $apiGiantBombGameFranchise->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise The ChildApiGiantBombGameFranchise object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGameFranchise(ChildApiGiantBombGameFranchise $apiGiantBombGameFranchise)
    {
        if ($this->getApiGiantBombGameFranchises()->contains($apiGiantBombGameFranchise)) {
            $pos = $this->collApiGiantBombGameFranchises->search($apiGiantBombGameFranchise);
            $this->collApiGiantBombGameFranchises->remove($pos);
            if (null === $this->apiGiantBombGameFranchisesScheduledForDeletion) {
                $this->apiGiantBombGameFranchisesScheduledForDeletion = clone $this->collApiGiantBombGameFranchises;
                $this->apiGiantBombGameFranchisesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameFranchisesScheduledForDeletion[]= clone $apiGiantBombGameFranchise;
            $apiGiantBombGameFranchise->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameFranchises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameFranchise[] List of ChildApiGiantBombGameFranchise objects
     */
    public function getApiGiantBombGameFranchisesJoinApiGiantBombFranchise(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameFranchiseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombFranchise', $joinBehavior);

        return $this->getApiGiantBombGameFranchises($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameGenres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameGenres()
     */
    public function clearApiGiantBombGameGenres()
    {
        $this->collApiGiantBombGameGenres = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameGenres collection loaded partially.
     */
    public function resetPartialApiGiantBombGameGenres($v = true)
    {
        $this->collApiGiantBombGameGenresPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameGenres collection.
     *
     * By default this just sets the collApiGiantBombGameGenres collection to an empty array (like clearcollApiGiantBombGameGenres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameGenres($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameGenres && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameGenreTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameGenres = new $collectionClassName;
        $this->collApiGiantBombGameGenres->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameGenre');
    }

    /**
     * Gets an array of ChildApiGiantBombGameGenre objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameGenre[] List of ChildApiGiantBombGameGenre objects
     * @throws PropelException
     */
    public function getApiGiantBombGameGenres(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameGenresPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameGenres || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameGenres) {
                // return empty collection
                $this->initApiGiantBombGameGenres();
            } else {
                $collApiGiantBombGameGenres = ChildApiGiantBombGameGenreQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameGenresPartial && count($collApiGiantBombGameGenres)) {
                        $this->initApiGiantBombGameGenres(false);

                        foreach ($collApiGiantBombGameGenres as $obj) {
                            if (false == $this->collApiGiantBombGameGenres->contains($obj)) {
                                $this->collApiGiantBombGameGenres->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameGenresPartial = true;
                    }

                    return $collApiGiantBombGameGenres;
                }

                if ($partial && $this->collApiGiantBombGameGenres) {
                    foreach ($this->collApiGiantBombGameGenres as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameGenres[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameGenres = $collApiGiantBombGameGenres;
                $this->collApiGiantBombGameGenresPartial = false;
            }
        }

        return $this->collApiGiantBombGameGenres;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameGenre objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameGenres A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGameGenres(Collection $apiGiantBombGameGenres, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameGenre[] $apiGiantBombGameGenresToDelete */
        $apiGiantBombGameGenresToDelete = $this->getApiGiantBombGameGenres(new Criteria(), $con)->diff($apiGiantBombGameGenres);


        $this->apiGiantBombGameGenresScheduledForDeletion = $apiGiantBombGameGenresToDelete;

        foreach ($apiGiantBombGameGenresToDelete as $apiGiantBombGameGenreRemoved) {
            $apiGiantBombGameGenreRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGameGenres = null;
        foreach ($apiGiantBombGameGenres as $apiGiantBombGameGenre) {
            $this->addApiGiantBombGameGenre($apiGiantBombGameGenre);
        }

        $this->collApiGiantBombGameGenres = $apiGiantBombGameGenres;
        $this->collApiGiantBombGameGenresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameGenre objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameGenre objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameGenres(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameGenresPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameGenres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameGenres) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameGenres());
            }

            $query = ChildApiGiantBombGameGenreQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameGenres);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameGenre object to this object
     * through the ChildApiGiantBombGameGenre foreign key attribute.
     *
     * @param  ChildApiGiantBombGameGenre $l ChildApiGiantBombGameGenre
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGameGenre(ChildApiGiantBombGameGenre $l)
    {
        if ($this->collApiGiantBombGameGenres === null) {
            $this->initApiGiantBombGameGenres();
            $this->collApiGiantBombGameGenresPartial = true;
        }

        if (!$this->collApiGiantBombGameGenres->contains($l)) {
            $this->doAddApiGiantBombGameGenre($l);

            if ($this->apiGiantBombGameGenresScheduledForDeletion and $this->apiGiantBombGameGenresScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameGenresScheduledForDeletion->remove($this->apiGiantBombGameGenresScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameGenre $apiGiantBombGameGenre The ChildApiGiantBombGameGenre object to add.
     */
    protected function doAddApiGiantBombGameGenre(ChildApiGiantBombGameGenre $apiGiantBombGameGenre)
    {
        $this->collApiGiantBombGameGenres[]= $apiGiantBombGameGenre;
        $apiGiantBombGameGenre->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGameGenre $apiGiantBombGameGenre The ChildApiGiantBombGameGenre object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGameGenre(ChildApiGiantBombGameGenre $apiGiantBombGameGenre)
    {
        if ($this->getApiGiantBombGameGenres()->contains($apiGiantBombGameGenre)) {
            $pos = $this->collApiGiantBombGameGenres->search($apiGiantBombGameGenre);
            $this->collApiGiantBombGameGenres->remove($pos);
            if (null === $this->apiGiantBombGameGenresScheduledForDeletion) {
                $this->apiGiantBombGameGenresScheduledForDeletion = clone $this->collApiGiantBombGameGenres;
                $this->apiGiantBombGameGenresScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameGenresScheduledForDeletion[]= clone $apiGiantBombGameGenre;
            $apiGiantBombGameGenre->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameGenres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameGenre[] List of ChildApiGiantBombGameGenre objects
     */
    public function getApiGiantBombGameGenresJoinApiGiantBombGenre(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameGenreQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGenre', $joinBehavior);

        return $this->getApiGiantBombGameGenres($query, $con);
    }

    /**
     * Clears out the collApiGiantBombGameReleases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombGameReleases()
     */
    public function clearApiGiantBombGameReleases()
    {
        $this->collApiGiantBombGameReleases = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombGameReleases collection loaded partially.
     */
    public function resetPartialApiGiantBombGameReleases($v = true)
    {
        $this->collApiGiantBombGameReleasesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombGameReleases collection.
     *
     * By default this just sets the collApiGiantBombGameReleases collection to an empty array (like clearcollApiGiantBombGameReleases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombGameReleases($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombGameReleases && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombGameReleaseTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombGameReleases = new $collectionClassName;
        $this->collApiGiantBombGameReleases->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameRelease');
    }

    /**
     * Gets an array of ChildApiGiantBombGameRelease objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     * @throws PropelException
     */
    public function getApiGiantBombGameReleases(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                // return empty collection
                $this->initApiGiantBombGameReleases();
            } else {
                $collApiGiantBombGameReleases = ChildApiGiantBombGameReleaseQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombGameReleasesPartial && count($collApiGiantBombGameReleases)) {
                        $this->initApiGiantBombGameReleases(false);

                        foreach ($collApiGiantBombGameReleases as $obj) {
                            if (false == $this->collApiGiantBombGameReleases->contains($obj)) {
                                $this->collApiGiantBombGameReleases->append($obj);
                            }
                        }

                        $this->collApiGiantBombGameReleasesPartial = true;
                    }

                    return $collApiGiantBombGameReleases;
                }

                if ($partial && $this->collApiGiantBombGameReleases) {
                    foreach ($this->collApiGiantBombGameReleases as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombGameReleases[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombGameReleases = $collApiGiantBombGameReleases;
                $this->collApiGiantBombGameReleasesPartial = false;
            }
        }

        return $this->collApiGiantBombGameReleases;
    }

    /**
     * Sets a collection of ChildApiGiantBombGameRelease objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombGameReleases A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombGameReleases(Collection $apiGiantBombGameReleases, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombGameRelease[] $apiGiantBombGameReleasesToDelete */
        $apiGiantBombGameReleasesToDelete = $this->getApiGiantBombGameReleases(new Criteria(), $con)->diff($apiGiantBombGameReleases);


        $this->apiGiantBombGameReleasesScheduledForDeletion = $apiGiantBombGameReleasesToDelete;

        foreach ($apiGiantBombGameReleasesToDelete as $apiGiantBombGameReleaseRemoved) {
            $apiGiantBombGameReleaseRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombGameReleases = null;
        foreach ($apiGiantBombGameReleases as $apiGiantBombGameRelease) {
            $this->addApiGiantBombGameRelease($apiGiantBombGameRelease);
        }

        $this->collApiGiantBombGameReleases = $apiGiantBombGameReleases;
        $this->collApiGiantBombGameReleasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombGameRelease objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombGameRelease objects.
     * @throws PropelException
     */
    public function countApiGiantBombGameReleases(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombGameReleasesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombGameReleases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombGameReleases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombGameReleases());
            }

            $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombGameReleases);
    }

    /**
     * Method called to associate a ChildApiGiantBombGameRelease object to this object
     * through the ChildApiGiantBombGameRelease foreign key attribute.
     *
     * @param  ChildApiGiantBombGameRelease $l ChildApiGiantBombGameRelease
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombGameRelease(ChildApiGiantBombGameRelease $l)
    {
        if ($this->collApiGiantBombGameReleases === null) {
            $this->initApiGiantBombGameReleases();
            $this->collApiGiantBombGameReleasesPartial = true;
        }

        if (!$this->collApiGiantBombGameReleases->contains($l)) {
            $this->doAddApiGiantBombGameRelease($l);

            if ($this->apiGiantBombGameReleasesScheduledForDeletion and $this->apiGiantBombGameReleasesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombGameReleasesScheduledForDeletion->remove($this->apiGiantBombGameReleasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to add.
     */
    protected function doAddApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        $this->collApiGiantBombGameReleases[]= $apiGiantBombGameRelease;
        $apiGiantBombGameRelease->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombGameRelease $apiGiantBombGameRelease The ChildApiGiantBombGameRelease object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombGameRelease(ChildApiGiantBombGameRelease $apiGiantBombGameRelease)
    {
        if ($this->getApiGiantBombGameReleases()->contains($apiGiantBombGameRelease)) {
            $pos = $this->collApiGiantBombGameReleases->search($apiGiantBombGameRelease);
            $this->collApiGiantBombGameReleases->remove($pos);
            if (null === $this->apiGiantBombGameReleasesScheduledForDeletion) {
                $this->apiGiantBombGameReleasesScheduledForDeletion = clone $this->collApiGiantBombGameReleases;
                $this->apiGiantBombGameReleasesScheduledForDeletion->clear();
            }
            $this->apiGiantBombGameReleasesScheduledForDeletion[]= clone $apiGiantBombGameRelease;
            $apiGiantBombGameRelease->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombPlatform(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombPlatform', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombGameRating(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRating', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombGameReleases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombGameRelease[] List of ChildApiGiantBombGameRelease objects
     */
    public function getApiGiantBombGameReleasesJoinApiGiantBombRegion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombGameReleaseQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombRegion', $joinBehavior);

        return $this->getApiGiantBombGameReleases($query, $con);
    }

    /**
     * Clears out the collApiGiantBombFirstGameLocations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombFirstGameLocations()
     */
    public function clearApiGiantBombFirstGameLocations()
    {
        $this->collApiGiantBombFirstGameLocations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombFirstGameLocations collection loaded partially.
     */
    public function resetPartialApiGiantBombFirstGameLocations($v = true)
    {
        $this->collApiGiantBombFirstGameLocationsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombFirstGameLocations collection.
     *
     * By default this just sets the collApiGiantBombFirstGameLocations collection to an empty array (like clearcollApiGiantBombFirstGameLocations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombFirstGameLocations($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombFirstGameLocations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombLocationTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombFirstGameLocations = new $collectionClassName;
        $this->collApiGiantBombFirstGameLocations->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombLocation');
    }

    /**
     * Gets an array of ChildApiGiantBombLocation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombLocation[] List of ChildApiGiantBombLocation objects
     * @throws PropelException
     */
    public function getApiGiantBombFirstGameLocations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameLocationsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameLocations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameLocations) {
                // return empty collection
                $this->initApiGiantBombFirstGameLocations();
            } else {
                $collApiGiantBombFirstGameLocations = ChildApiGiantBombLocationQuery::create(null, $criteria)
                    ->filterByApiGiantBombFirstGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombFirstGameLocationsPartial && count($collApiGiantBombFirstGameLocations)) {
                        $this->initApiGiantBombFirstGameLocations(false);

                        foreach ($collApiGiantBombFirstGameLocations as $obj) {
                            if (false == $this->collApiGiantBombFirstGameLocations->contains($obj)) {
                                $this->collApiGiantBombFirstGameLocations->append($obj);
                            }
                        }

                        $this->collApiGiantBombFirstGameLocationsPartial = true;
                    }

                    return $collApiGiantBombFirstGameLocations;
                }

                if ($partial && $this->collApiGiantBombFirstGameLocations) {
                    foreach ($this->collApiGiantBombFirstGameLocations as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombFirstGameLocations[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombFirstGameLocations = $collApiGiantBombFirstGameLocations;
                $this->collApiGiantBombFirstGameLocationsPartial = false;
            }
        }

        return $this->collApiGiantBombFirstGameLocations;
    }

    /**
     * Sets a collection of ChildApiGiantBombLocation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombFirstGameLocations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombFirstGameLocations(Collection $apiGiantBombFirstGameLocations, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombLocation[] $apiGiantBombFirstGameLocationsToDelete */
        $apiGiantBombFirstGameLocationsToDelete = $this->getApiGiantBombFirstGameLocations(new Criteria(), $con)->diff($apiGiantBombFirstGameLocations);


        $this->apiGiantBombFirstGameLocationsScheduledForDeletion = $apiGiantBombFirstGameLocationsToDelete;

        foreach ($apiGiantBombFirstGameLocationsToDelete as $apiGiantBombFirstGameLocationRemoved) {
            $apiGiantBombFirstGameLocationRemoved->setApiGiantBombFirstGame(null);
        }

        $this->collApiGiantBombFirstGameLocations = null;
        foreach ($apiGiantBombFirstGameLocations as $apiGiantBombFirstGameLocation) {
            $this->addApiGiantBombFirstGameLocation($apiGiantBombFirstGameLocation);
        }

        $this->collApiGiantBombFirstGameLocations = $apiGiantBombFirstGameLocations;
        $this->collApiGiantBombFirstGameLocationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombLocation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombLocation objects.
     * @throws PropelException
     */
    public function countApiGiantBombFirstGameLocations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameLocationsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameLocations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameLocations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombFirstGameLocations());
            }

            $query = ChildApiGiantBombLocationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFirstGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombFirstGameLocations);
    }

    /**
     * Method called to associate a ChildApiGiantBombLocation object to this object
     * through the ChildApiGiantBombLocation foreign key attribute.
     *
     * @param  ChildApiGiantBombLocation $l ChildApiGiantBombLocation
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombFirstGameLocation(ChildApiGiantBombLocation $l)
    {
        if ($this->collApiGiantBombFirstGameLocations === null) {
            $this->initApiGiantBombFirstGameLocations();
            $this->collApiGiantBombFirstGameLocationsPartial = true;
        }

        if (!$this->collApiGiantBombFirstGameLocations->contains($l)) {
            $this->doAddApiGiantBombFirstGameLocation($l);

            if ($this->apiGiantBombFirstGameLocationsScheduledForDeletion and $this->apiGiantBombFirstGameLocationsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombFirstGameLocationsScheduledForDeletion->remove($this->apiGiantBombFirstGameLocationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombLocation $apiGiantBombFirstGameLocation The ChildApiGiantBombLocation object to add.
     */
    protected function doAddApiGiantBombFirstGameLocation(ChildApiGiantBombLocation $apiGiantBombFirstGameLocation)
    {
        $this->collApiGiantBombFirstGameLocations[]= $apiGiantBombFirstGameLocation;
        $apiGiantBombFirstGameLocation->setApiGiantBombFirstGame($this);
    }

    /**
     * @param  ChildApiGiantBombLocation $apiGiantBombFirstGameLocation The ChildApiGiantBombLocation object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombFirstGameLocation(ChildApiGiantBombLocation $apiGiantBombFirstGameLocation)
    {
        if ($this->getApiGiantBombFirstGameLocations()->contains($apiGiantBombFirstGameLocation)) {
            $pos = $this->collApiGiantBombFirstGameLocations->search($apiGiantBombFirstGameLocation);
            $this->collApiGiantBombFirstGameLocations->remove($pos);
            if (null === $this->apiGiantBombFirstGameLocationsScheduledForDeletion) {
                $this->apiGiantBombFirstGameLocationsScheduledForDeletion = clone $this->collApiGiantBombFirstGameLocations;
                $this->apiGiantBombFirstGameLocationsScheduledForDeletion->clear();
            }
            $this->apiGiantBombFirstGameLocationsScheduledForDeletion[]= $apiGiantBombFirstGameLocation;
            $apiGiantBombFirstGameLocation->setApiGiantBombFirstGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombFirstGameObjects collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombFirstGameObjects()
     */
    public function clearApiGiantBombFirstGameObjects()
    {
        $this->collApiGiantBombFirstGameObjects = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombFirstGameObjects collection loaded partially.
     */
    public function resetPartialApiGiantBombFirstGameObjects($v = true)
    {
        $this->collApiGiantBombFirstGameObjectsPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombFirstGameObjects collection.
     *
     * By default this just sets the collApiGiantBombFirstGameObjects collection to an empty array (like clearcollApiGiantBombFirstGameObjects());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombFirstGameObjects($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombFirstGameObjects && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombObjectTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombFirstGameObjects = new $collectionClassName;
        $this->collApiGiantBombFirstGameObjects->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombObject');
    }

    /**
     * Gets an array of ChildApiGiantBombObject objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombObject[] List of ChildApiGiantBombObject objects
     * @throws PropelException
     */
    public function getApiGiantBombFirstGameObjects(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameObjectsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameObjects || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameObjects) {
                // return empty collection
                $this->initApiGiantBombFirstGameObjects();
            } else {
                $collApiGiantBombFirstGameObjects = ChildApiGiantBombObjectQuery::create(null, $criteria)
                    ->filterByApiGiantBombFirstGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombFirstGameObjectsPartial && count($collApiGiantBombFirstGameObjects)) {
                        $this->initApiGiantBombFirstGameObjects(false);

                        foreach ($collApiGiantBombFirstGameObjects as $obj) {
                            if (false == $this->collApiGiantBombFirstGameObjects->contains($obj)) {
                                $this->collApiGiantBombFirstGameObjects->append($obj);
                            }
                        }

                        $this->collApiGiantBombFirstGameObjectsPartial = true;
                    }

                    return $collApiGiantBombFirstGameObjects;
                }

                if ($partial && $this->collApiGiantBombFirstGameObjects) {
                    foreach ($this->collApiGiantBombFirstGameObjects as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombFirstGameObjects[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombFirstGameObjects = $collApiGiantBombFirstGameObjects;
                $this->collApiGiantBombFirstGameObjectsPartial = false;
            }
        }

        return $this->collApiGiantBombFirstGameObjects;
    }

    /**
     * Sets a collection of ChildApiGiantBombObject objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombFirstGameObjects A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombFirstGameObjects(Collection $apiGiantBombFirstGameObjects, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombObject[] $apiGiantBombFirstGameObjectsToDelete */
        $apiGiantBombFirstGameObjectsToDelete = $this->getApiGiantBombFirstGameObjects(new Criteria(), $con)->diff($apiGiantBombFirstGameObjects);


        $this->apiGiantBombFirstGameObjectsScheduledForDeletion = $apiGiantBombFirstGameObjectsToDelete;

        foreach ($apiGiantBombFirstGameObjectsToDelete as $apiGiantBombFirstGameObjectRemoved) {
            $apiGiantBombFirstGameObjectRemoved->setApiGiantBombFirstGame(null);
        }

        $this->collApiGiantBombFirstGameObjects = null;
        foreach ($apiGiantBombFirstGameObjects as $apiGiantBombFirstGameObject) {
            $this->addApiGiantBombFirstGameObject($apiGiantBombFirstGameObject);
        }

        $this->collApiGiantBombFirstGameObjects = $apiGiantBombFirstGameObjects;
        $this->collApiGiantBombFirstGameObjectsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombObject objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombObject objects.
     * @throws PropelException
     */
    public function countApiGiantBombFirstGameObjects(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombFirstGameObjectsPartial && !$this->isNew();
        if (null === $this->collApiGiantBombFirstGameObjects || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombFirstGameObjects) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombFirstGameObjects());
            }

            $query = ChildApiGiantBombObjectQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombFirstGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombFirstGameObjects);
    }

    /**
     * Method called to associate a ChildApiGiantBombObject object to this object
     * through the ChildApiGiantBombObject foreign key attribute.
     *
     * @param  ChildApiGiantBombObject $l ChildApiGiantBombObject
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombFirstGameObject(ChildApiGiantBombObject $l)
    {
        if ($this->collApiGiantBombFirstGameObjects === null) {
            $this->initApiGiantBombFirstGameObjects();
            $this->collApiGiantBombFirstGameObjectsPartial = true;
        }

        if (!$this->collApiGiantBombFirstGameObjects->contains($l)) {
            $this->doAddApiGiantBombFirstGameObject($l);

            if ($this->apiGiantBombFirstGameObjectsScheduledForDeletion and $this->apiGiantBombFirstGameObjectsScheduledForDeletion->contains($l)) {
                $this->apiGiantBombFirstGameObjectsScheduledForDeletion->remove($this->apiGiantBombFirstGameObjectsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombObject $apiGiantBombFirstGameObject The ChildApiGiantBombObject object to add.
     */
    protected function doAddApiGiantBombFirstGameObject(ChildApiGiantBombObject $apiGiantBombFirstGameObject)
    {
        $this->collApiGiantBombFirstGameObjects[]= $apiGiantBombFirstGameObject;
        $apiGiantBombFirstGameObject->setApiGiantBombFirstGame($this);
    }

    /**
     * @param  ChildApiGiantBombObject $apiGiantBombFirstGameObject The ChildApiGiantBombObject object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombFirstGameObject(ChildApiGiantBombObject $apiGiantBombFirstGameObject)
    {
        if ($this->getApiGiantBombFirstGameObjects()->contains($apiGiantBombFirstGameObject)) {
            $pos = $this->collApiGiantBombFirstGameObjects->search($apiGiantBombFirstGameObject);
            $this->collApiGiantBombFirstGameObjects->remove($pos);
            if (null === $this->apiGiantBombFirstGameObjectsScheduledForDeletion) {
                $this->apiGiantBombFirstGameObjectsScheduledForDeletion = clone $this->collApiGiantBombFirstGameObjects;
                $this->apiGiantBombFirstGameObjectsScheduledForDeletion->clear();
            }
            $this->apiGiantBombFirstGameObjectsScheduledForDeletion[]= $apiGiantBombFirstGameObject;
            $apiGiantBombFirstGameObject->setApiGiantBombFirstGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombCreditedGamepeople collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombCreditedGamepeople()
     */
    public function clearApiGiantBombCreditedGamepeople()
    {
        $this->collApiGiantBombCreditedGamepeople = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombCreditedGamepeople collection loaded partially.
     */
    public function resetPartialApiGiantBombCreditedGamepeople($v = true)
    {
        $this->collApiGiantBombCreditedGamepeoplePartial = $v;
    }

    /**
     * Initializes the collApiGiantBombCreditedGamepeople collection.
     *
     * By default this just sets the collApiGiantBombCreditedGamepeople collection to an empty array (like clearcollApiGiantBombCreditedGamepeople());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombCreditedGamepeople($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombCreditedGamepeople && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombPersonTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombCreditedGamepeople = new $collectionClassName;
        $this->collApiGiantBombCreditedGamepeople->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson');
    }

    /**
     * Gets an array of ChildApiGiantBombPerson objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombPerson[] List of ChildApiGiantBombPerson objects
     * @throws PropelException
     */
    public function getApiGiantBombCreditedGamepeople(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombCreditedGamepeoplePartial && !$this->isNew();
        if (null === $this->collApiGiantBombCreditedGamepeople || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombCreditedGamepeople) {
                // return empty collection
                $this->initApiGiantBombCreditedGamepeople();
            } else {
                $collApiGiantBombCreditedGamepeople = ChildApiGiantBombPersonQuery::create(null, $criteria)
                    ->filterByApiGiantBombCreditedGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombCreditedGamepeoplePartial && count($collApiGiantBombCreditedGamepeople)) {
                        $this->initApiGiantBombCreditedGamepeople(false);

                        foreach ($collApiGiantBombCreditedGamepeople as $obj) {
                            if (false == $this->collApiGiantBombCreditedGamepeople->contains($obj)) {
                                $this->collApiGiantBombCreditedGamepeople->append($obj);
                            }
                        }

                        $this->collApiGiantBombCreditedGamepeoplePartial = true;
                    }

                    return $collApiGiantBombCreditedGamepeople;
                }

                if ($partial && $this->collApiGiantBombCreditedGamepeople) {
                    foreach ($this->collApiGiantBombCreditedGamepeople as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombCreditedGamepeople[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombCreditedGamepeople = $collApiGiantBombCreditedGamepeople;
                $this->collApiGiantBombCreditedGamepeoplePartial = false;
            }
        }

        return $this->collApiGiantBombCreditedGamepeople;
    }

    /**
     * Sets a collection of ChildApiGiantBombPerson objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombCreditedGamepeople A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombCreditedGamepeople(Collection $apiGiantBombCreditedGamepeople, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombPerson[] $apiGiantBombCreditedGamepeopleToDelete */
        $apiGiantBombCreditedGamepeopleToDelete = $this->getApiGiantBombCreditedGamepeople(new Criteria(), $con)->diff($apiGiantBombCreditedGamepeople);


        $this->apiGiantBombCreditedGamepeopleScheduledForDeletion = $apiGiantBombCreditedGamepeopleToDelete;

        foreach ($apiGiantBombCreditedGamepeopleToDelete as $apiGiantBombCreditedGamePersonRemoved) {
            $apiGiantBombCreditedGamePersonRemoved->setApiGiantBombCreditedGame(null);
        }

        $this->collApiGiantBombCreditedGamepeople = null;
        foreach ($apiGiantBombCreditedGamepeople as $apiGiantBombCreditedGamePerson) {
            $this->addApiGiantBombCreditedGamePerson($apiGiantBombCreditedGamePerson);
        }

        $this->collApiGiantBombCreditedGamepeople = $apiGiantBombCreditedGamepeople;
        $this->collApiGiantBombCreditedGamepeoplePartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombPerson objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombPerson objects.
     * @throws PropelException
     */
    public function countApiGiantBombCreditedGamepeople(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombCreditedGamepeoplePartial && !$this->isNew();
        if (null === $this->collApiGiantBombCreditedGamepeople || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombCreditedGamepeople) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombCreditedGamepeople());
            }

            $query = ChildApiGiantBombPersonQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombCreditedGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombCreditedGamepeople);
    }

    /**
     * Method called to associate a ChildApiGiantBombPerson object to this object
     * through the ChildApiGiantBombPerson foreign key attribute.
     *
     * @param  ChildApiGiantBombPerson $l ChildApiGiantBombPerson
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombCreditedGamePerson(ChildApiGiantBombPerson $l)
    {
        if ($this->collApiGiantBombCreditedGamepeople === null) {
            $this->initApiGiantBombCreditedGamepeople();
            $this->collApiGiantBombCreditedGamepeoplePartial = true;
        }

        if (!$this->collApiGiantBombCreditedGamepeople->contains($l)) {
            $this->doAddApiGiantBombCreditedGamePerson($l);

            if ($this->apiGiantBombCreditedGamepeopleScheduledForDeletion and $this->apiGiantBombCreditedGamepeopleScheduledForDeletion->contains($l)) {
                $this->apiGiantBombCreditedGamepeopleScheduledForDeletion->remove($this->apiGiantBombCreditedGamepeopleScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombPerson $apiGiantBombCreditedGamePerson The ChildApiGiantBombPerson object to add.
     */
    protected function doAddApiGiantBombCreditedGamePerson(ChildApiGiantBombPerson $apiGiantBombCreditedGamePerson)
    {
        $this->collApiGiantBombCreditedGamepeople[]= $apiGiantBombCreditedGamePerson;
        $apiGiantBombCreditedGamePerson->setApiGiantBombCreditedGame($this);
    }

    /**
     * @param  ChildApiGiantBombPerson $apiGiantBombCreditedGamePerson The ChildApiGiantBombPerson object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombCreditedGamePerson(ChildApiGiantBombPerson $apiGiantBombCreditedGamePerson)
    {
        if ($this->getApiGiantBombCreditedGamepeople()->contains($apiGiantBombCreditedGamePerson)) {
            $pos = $this->collApiGiantBombCreditedGamepeople->search($apiGiantBombCreditedGamePerson);
            $this->collApiGiantBombCreditedGamepeople->remove($pos);
            if (null === $this->apiGiantBombCreditedGamepeopleScheduledForDeletion) {
                $this->apiGiantBombCreditedGamepeopleScheduledForDeletion = clone $this->collApiGiantBombCreditedGamepeople;
                $this->apiGiantBombCreditedGamepeopleScheduledForDeletion->clear();
            }
            $this->apiGiantBombCreditedGamepeopleScheduledForDeletion[]= $apiGiantBombCreditedGamePerson;
            $apiGiantBombCreditedGamePerson->setApiGiantBombCreditedGame(null);
        }

        return $this;
    }

    /**
     * Clears out the collApiGiantBombImages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombImages()
     */
    public function clearApiGiantBombImages()
    {
        $this->collApiGiantBombImages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombImages collection loaded partially.
     */
    public function resetPartialApiGiantBombImages($v = true)
    {
        $this->collApiGiantBombImagesPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombImages collection.
     *
     * By default this just sets the collApiGiantBombImages collection to an empty array (like clearcollApiGiantBombImages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombImages($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombImages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombImageTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombImages = new $collectionClassName;
        $this->collApiGiantBombImages->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombImage');
    }

    /**
     * Gets an array of ChildApiGiantBombImage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombImage[] List of ChildApiGiantBombImage objects
     * @throws PropelException
     */
    public function getApiGiantBombImages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombImagesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombImages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombImages) {
                // return empty collection
                $this->initApiGiantBombImages();
            } else {
                $collApiGiantBombImages = ChildApiGiantBombImageQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombImagesPartial && count($collApiGiantBombImages)) {
                        $this->initApiGiantBombImages(false);

                        foreach ($collApiGiantBombImages as $obj) {
                            if (false == $this->collApiGiantBombImages->contains($obj)) {
                                $this->collApiGiantBombImages->append($obj);
                            }
                        }

                        $this->collApiGiantBombImagesPartial = true;
                    }

                    return $collApiGiantBombImages;
                }

                if ($partial && $this->collApiGiantBombImages) {
                    foreach ($this->collApiGiantBombImages as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombImages[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombImages = $collApiGiantBombImages;
                $this->collApiGiantBombImagesPartial = false;
            }
        }

        return $this->collApiGiantBombImages;
    }

    /**
     * Sets a collection of ChildApiGiantBombImage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombImages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombImages(Collection $apiGiantBombImages, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombImage[] $apiGiantBombImagesToDelete */
        $apiGiantBombImagesToDelete = $this->getApiGiantBombImages(new Criteria(), $con)->diff($apiGiantBombImages);


        $this->apiGiantBombImagesScheduledForDeletion = $apiGiantBombImagesToDelete;

        foreach ($apiGiantBombImagesToDelete as $apiGiantBombImageRemoved) {
            $apiGiantBombImageRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombImages = null;
        foreach ($apiGiantBombImages as $apiGiantBombImage) {
            $this->addApiGiantBombImage($apiGiantBombImage);
        }

        $this->collApiGiantBombImages = $apiGiantBombImages;
        $this->collApiGiantBombImagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombImage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombImage objects.
     * @throws PropelException
     */
    public function countApiGiantBombImages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombImagesPartial && !$this->isNew();
        if (null === $this->collApiGiantBombImages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombImages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombImages());
            }

            $query = ChildApiGiantBombImageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombImages);
    }

    /**
     * Method called to associate a ChildApiGiantBombImage object to this object
     * through the ChildApiGiantBombImage foreign key attribute.
     *
     * @param  ChildApiGiantBombImage $l ChildApiGiantBombImage
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombImage(ChildApiGiantBombImage $l)
    {
        if ($this->collApiGiantBombImages === null) {
            $this->initApiGiantBombImages();
            $this->collApiGiantBombImagesPartial = true;
        }

        if (!$this->collApiGiantBombImages->contains($l)) {
            $this->doAddApiGiantBombImage($l);

            if ($this->apiGiantBombImagesScheduledForDeletion and $this->apiGiantBombImagesScheduledForDeletion->contains($l)) {
                $this->apiGiantBombImagesScheduledForDeletion->remove($this->apiGiantBombImagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombImage $apiGiantBombImage The ChildApiGiantBombImage object to add.
     */
    protected function doAddApiGiantBombImage(ChildApiGiantBombImage $apiGiantBombImage)
    {
        $this->collApiGiantBombImages[]= $apiGiantBombImage;
        $apiGiantBombImage->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombImage $apiGiantBombImage The ChildApiGiantBombImage object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombImage(ChildApiGiantBombImage $apiGiantBombImage)
    {
        if ($this->getApiGiantBombImages()->contains($apiGiantBombImage)) {
            $pos = $this->collApiGiantBombImages->search($apiGiantBombImage);
            $this->collApiGiantBombImages->remove($pos);
            if (null === $this->apiGiantBombImagesScheduledForDeletion) {
                $this->apiGiantBombImagesScheduledForDeletion = clone $this->collApiGiantBombImages;
                $this->apiGiantBombImagesScheduledForDeletion->clear();
            }
            $this->apiGiantBombImagesScheduledForDeletion[]= $apiGiantBombImage;
            $apiGiantBombImage->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombImages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombImage[] List of ChildApiGiantBombImage objects
     */
    public function getApiGiantBombImagesJoinApiGiantBombGameRelease(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombImageQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRelease', $joinBehavior);

        return $this->getApiGiantBombImages($query, $con);
    }

    /**
     * Clears out the collApiGiantBombVideos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addApiGiantBombVideos()
     */
    public function clearApiGiantBombVideos()
    {
        $this->collApiGiantBombVideos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collApiGiantBombVideos collection loaded partially.
     */
    public function resetPartialApiGiantBombVideos($v = true)
    {
        $this->collApiGiantBombVideosPartial = $v;
    }

    /**
     * Initializes the collApiGiantBombVideos collection.
     *
     * By default this just sets the collApiGiantBombVideos collection to an empty array (like clearcollApiGiantBombVideos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initApiGiantBombVideos($overrideExisting = true)
    {
        if (null !== $this->collApiGiantBombVideos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ApiGiantBombVideoTableMap::getTableMap()->getCollectionClassName();

        $this->collApiGiantBombVideos = new $collectionClassName;
        $this->collApiGiantBombVideos->setModel('\IiMedias\VideoGamesBundle\Model\ApiGiantBombVideo');
    }

    /**
     * Gets an array of ChildApiGiantBombVideo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApiGiantBombGame is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildApiGiantBombVideo[] List of ChildApiGiantBombVideo objects
     * @throws PropelException
     */
    public function getApiGiantBombVideos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombVideosPartial && !$this->isNew();
        if (null === $this->collApiGiantBombVideos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombVideos) {
                // return empty collection
                $this->initApiGiantBombVideos();
            } else {
                $collApiGiantBombVideos = ChildApiGiantBombVideoQuery::create(null, $criteria)
                    ->filterByApiGiantBombGame($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collApiGiantBombVideosPartial && count($collApiGiantBombVideos)) {
                        $this->initApiGiantBombVideos(false);

                        foreach ($collApiGiantBombVideos as $obj) {
                            if (false == $this->collApiGiantBombVideos->contains($obj)) {
                                $this->collApiGiantBombVideos->append($obj);
                            }
                        }

                        $this->collApiGiantBombVideosPartial = true;
                    }

                    return $collApiGiantBombVideos;
                }

                if ($partial && $this->collApiGiantBombVideos) {
                    foreach ($this->collApiGiantBombVideos as $obj) {
                        if ($obj->isNew()) {
                            $collApiGiantBombVideos[] = $obj;
                        }
                    }
                }

                $this->collApiGiantBombVideos = $collApiGiantBombVideos;
                $this->collApiGiantBombVideosPartial = false;
            }
        }

        return $this->collApiGiantBombVideos;
    }

    /**
     * Sets a collection of ChildApiGiantBombVideo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $apiGiantBombVideos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function setApiGiantBombVideos(Collection $apiGiantBombVideos, ConnectionInterface $con = null)
    {
        /** @var ChildApiGiantBombVideo[] $apiGiantBombVideosToDelete */
        $apiGiantBombVideosToDelete = $this->getApiGiantBombVideos(new Criteria(), $con)->diff($apiGiantBombVideos);


        $this->apiGiantBombVideosScheduledForDeletion = $apiGiantBombVideosToDelete;

        foreach ($apiGiantBombVideosToDelete as $apiGiantBombVideoRemoved) {
            $apiGiantBombVideoRemoved->setApiGiantBombGame(null);
        }

        $this->collApiGiantBombVideos = null;
        foreach ($apiGiantBombVideos as $apiGiantBombVideo) {
            $this->addApiGiantBombVideo($apiGiantBombVideo);
        }

        $this->collApiGiantBombVideos = $apiGiantBombVideos;
        $this->collApiGiantBombVideosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ApiGiantBombVideo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ApiGiantBombVideo objects.
     * @throws PropelException
     */
    public function countApiGiantBombVideos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collApiGiantBombVideosPartial && !$this->isNew();
        if (null === $this->collApiGiantBombVideos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collApiGiantBombVideos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getApiGiantBombVideos());
            }

            $query = ChildApiGiantBombVideoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApiGiantBombGame($this)
                ->count($con);
        }

        return count($this->collApiGiantBombVideos);
    }

    /**
     * Method called to associate a ChildApiGiantBombVideo object to this object
     * through the ChildApiGiantBombVideo foreign key attribute.
     *
     * @param  ChildApiGiantBombVideo $l ChildApiGiantBombVideo
     * @return $this|\IiMedias\VideoGamesBundle\Model\ApiGiantBombGame The current object (for fluent API support)
     */
    public function addApiGiantBombVideo(ChildApiGiantBombVideo $l)
    {
        if ($this->collApiGiantBombVideos === null) {
            $this->initApiGiantBombVideos();
            $this->collApiGiantBombVideosPartial = true;
        }

        if (!$this->collApiGiantBombVideos->contains($l)) {
            $this->doAddApiGiantBombVideo($l);

            if ($this->apiGiantBombVideosScheduledForDeletion and $this->apiGiantBombVideosScheduledForDeletion->contains($l)) {
                $this->apiGiantBombVideosScheduledForDeletion->remove($this->apiGiantBombVideosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildApiGiantBombVideo $apiGiantBombVideo The ChildApiGiantBombVideo object to add.
     */
    protected function doAddApiGiantBombVideo(ChildApiGiantBombVideo $apiGiantBombVideo)
    {
        $this->collApiGiantBombVideos[]= $apiGiantBombVideo;
        $apiGiantBombVideo->setApiGiantBombGame($this);
    }

    /**
     * @param  ChildApiGiantBombVideo $apiGiantBombVideo The ChildApiGiantBombVideo object to remove.
     * @return $this|ChildApiGiantBombGame The current object (for fluent API support)
     */
    public function removeApiGiantBombVideo(ChildApiGiantBombVideo $apiGiantBombVideo)
    {
        if ($this->getApiGiantBombVideos()->contains($apiGiantBombVideo)) {
            $pos = $this->collApiGiantBombVideos->search($apiGiantBombVideo);
            $this->collApiGiantBombVideos->remove($pos);
            if (null === $this->apiGiantBombVideosScheduledForDeletion) {
                $this->apiGiantBombVideosScheduledForDeletion = clone $this->collApiGiantBombVideos;
                $this->apiGiantBombVideosScheduledForDeletion->clear();
            }
            $this->apiGiantBombVideosScheduledForDeletion[]= $apiGiantBombVideo;
            $apiGiantBombVideo->setApiGiantBombGame(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ApiGiantBombGame is new, it will return
     * an empty collection; or if this ApiGiantBombGame has previously
     * been saved, it will retrieve related ApiGiantBombVideos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ApiGiantBombGame.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildApiGiantBombVideo[] List of ChildApiGiantBombVideo objects
     */
    public function getApiGiantBombVideosJoinApiGiantBombGameRelease(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildApiGiantBombVideoQuery::create(null, $criteria);
        $query->joinWith('ApiGiantBombGameRelease', $joinBehavior);

        return $this->getApiGiantBombVideos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aGame) {
            $this->aGame->removeApiGiantBombGame($this);
        }
        $this->vgagga_id = null;
        $this->vgagga_vgagam_id = null;
        $this->vgagga_name = null;
        $this->vgagga_aliases = null;
        $this->vgagga_aliases_unserialized = null;
        $this->vgagga_summary = null;
        $this->vgagga_description = null;
        $this->vgagga_original_released_at = null;
        $this->vgagga_expected_day_released_at = null;
        $this->vgagga_expected_month_released_at = null;
        $this->vgagga_expected_quarter_released_at = null;
        $this->vgagga_expected_year_released_at = null;
        $this->vgagga_user_reviews_count = null;
        $this->vgagga_image_icon_url = null;
        $this->vgagga_image_medium_url = null;
        $this->vgagga_image_screen_url = null;
        $this->vgagga_image_small_url = null;
        $this->vgagga_image_super_url = null;
        $this->vgagga_image_thumb_url = null;
        $this->vgagga_image_tiny_url = null;
        $this->vgagga_api_detail_url = null;
        $this->vgagga_site_detail_url = null;
        $this->vgagga_created_at = null;
        $this->vgagga_updated_at = null;
        $this->vgagga_refreshed_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collApiGiantBombFirstGameCharacters) {
                foreach ($this->collApiGiantBombFirstGameCharacters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombFirstGameConcepts) {
                foreach ($this->collApiGiantBombFirstGameConcepts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameOriginalRatings) {
                foreach ($this->collApiGiantBombGameOriginalRatings as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGamePlatforms) {
                foreach ($this->collApiGiantBombGamePlatforms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameDevelopers) {
                foreach ($this->collApiGiantBombGameDevelopers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGamePublishers) {
                foreach ($this->collApiGiantBombGamePublishers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameFranchises) {
                foreach ($this->collApiGiantBombGameFranchises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameGenres) {
                foreach ($this->collApiGiantBombGameGenres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombGameReleases) {
                foreach ($this->collApiGiantBombGameReleases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombFirstGameLocations) {
                foreach ($this->collApiGiantBombFirstGameLocations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombFirstGameObjects) {
                foreach ($this->collApiGiantBombFirstGameObjects as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombCreditedGamepeople) {
                foreach ($this->collApiGiantBombCreditedGamepeople as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombImages) {
                foreach ($this->collApiGiantBombImages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collApiGiantBombVideos) {
                foreach ($this->collApiGiantBombVideos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collApiGiantBombFirstGameCharacters = null;
        $this->collApiGiantBombFirstGameConcepts = null;
        $this->collApiGiantBombGameOriginalRatings = null;
        $this->collApiGiantBombGamePlatforms = null;
        $this->collApiGiantBombGameDevelopers = null;
        $this->collApiGiantBombGamePublishers = null;
        $this->collApiGiantBombGameFranchises = null;
        $this->collApiGiantBombGameGenres = null;
        $this->collApiGiantBombGameReleases = null;
        $this->collApiGiantBombFirstGameLocations = null;
        $this->collApiGiantBombFirstGameObjects = null;
        $this->collApiGiantBombCreditedGamepeople = null;
        $this->collApiGiantBombImages = null;
        $this->collApiGiantBombVideos = null;
        $this->aGame = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiGiantBombGameTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
