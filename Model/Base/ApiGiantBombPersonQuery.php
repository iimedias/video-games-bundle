<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPerson as ChildApiGiantBombPerson;
use IiMedias\VideoGamesBundle\Model\ApiGiantBombPersonQuery as ChildApiGiantBombPersonQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiGiantBombPersonTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_giantbomb_person_vgagpe' table.
 *
 *
 *
 * @method     ChildApiGiantBombPersonQuery orderById($order = Criteria::ASC) Order by the vgagpe_id column
 * @method     ChildApiGiantBombPersonQuery orderByName($order = Criteria::ASC) Order by the vgagpe_name column
 * @method     ChildApiGiantBombPersonQuery orderByAliases($order = Criteria::ASC) Order by the vgagpe_aliases column
 * @method     ChildApiGiantBombPersonQuery orderBySummary($order = Criteria::ASC) Order by the vgagpe_summary column
 * @method     ChildApiGiantBombPersonQuery orderByDescription($order = Criteria::ASC) Order by the vgagpe_description column
 * @method     ChildApiGiantBombPersonQuery orderByGender($order = Criteria::ASC) Order by the vgagpe_gender column
 * @method     ChildApiGiantBombPersonQuery orderByBirthDate($order = Criteria::ASC) Order by the vgagpe_birthdate column
 * @method     ChildApiGiantBombPersonQuery orderByDeathDate($order = Criteria::ASC) Order by the vgagpe_deathdate column
 * @method     ChildApiGiantBombPersonQuery orderByHometown($order = Criteria::ASC) Order by the vgagpe_hometown column
 * @method     ChildApiGiantBombPersonQuery orderByCountry($order = Criteria::ASC) Order by the vgagpe_country column
 * @method     ChildApiGiantBombPersonQuery orderByApiDetailUrl($order = Criteria::ASC) Order by the vgagpe_api_detail_url column
 * @method     ChildApiGiantBombPersonQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgagpe_site_detail_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageIconUrl($order = Criteria::ASC) Order by the vgagpe_image_icon_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageMediumUrl($order = Criteria::ASC) Order by the vgagpe_image_medium_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageScreenUrl($order = Criteria::ASC) Order by the vgagpe_image_screen_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageSmallUrl($order = Criteria::ASC) Order by the vgagpe_image_small_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageSuperUrl($order = Criteria::ASC) Order by the vgagpe_image_super_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageThumbUrl($order = Criteria::ASC) Order by the vgagpe_image_thumb_url column
 * @method     ChildApiGiantBombPersonQuery orderByImageTinyUrl($order = Criteria::ASC) Order by the vgagpe_image_tiny_url column
 * @method     ChildApiGiantBombPersonQuery orderByGiantBombGameId($order = Criteria::ASC) Order by the vgagpe_credited_vgagga_id column
 * @method     ChildApiGiantBombPersonQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgagpe_created_at column
 * @method     ChildApiGiantBombPersonQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgagpe_updated_at column
 *
 * @method     ChildApiGiantBombPersonQuery groupById() Group by the vgagpe_id column
 * @method     ChildApiGiantBombPersonQuery groupByName() Group by the vgagpe_name column
 * @method     ChildApiGiantBombPersonQuery groupByAliases() Group by the vgagpe_aliases column
 * @method     ChildApiGiantBombPersonQuery groupBySummary() Group by the vgagpe_summary column
 * @method     ChildApiGiantBombPersonQuery groupByDescription() Group by the vgagpe_description column
 * @method     ChildApiGiantBombPersonQuery groupByGender() Group by the vgagpe_gender column
 * @method     ChildApiGiantBombPersonQuery groupByBirthDate() Group by the vgagpe_birthdate column
 * @method     ChildApiGiantBombPersonQuery groupByDeathDate() Group by the vgagpe_deathdate column
 * @method     ChildApiGiantBombPersonQuery groupByHometown() Group by the vgagpe_hometown column
 * @method     ChildApiGiantBombPersonQuery groupByCountry() Group by the vgagpe_country column
 * @method     ChildApiGiantBombPersonQuery groupByApiDetailUrl() Group by the vgagpe_api_detail_url column
 * @method     ChildApiGiantBombPersonQuery groupBySiteDetailUrl() Group by the vgagpe_site_detail_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageIconUrl() Group by the vgagpe_image_icon_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageMediumUrl() Group by the vgagpe_image_medium_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageScreenUrl() Group by the vgagpe_image_screen_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageSmallUrl() Group by the vgagpe_image_small_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageSuperUrl() Group by the vgagpe_image_super_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageThumbUrl() Group by the vgagpe_image_thumb_url column
 * @method     ChildApiGiantBombPersonQuery groupByImageTinyUrl() Group by the vgagpe_image_tiny_url column
 * @method     ChildApiGiantBombPersonQuery groupByGiantBombGameId() Group by the vgagpe_credited_vgagga_id column
 * @method     ChildApiGiantBombPersonQuery groupByCreatedAt() Group by the vgagpe_created_at column
 * @method     ChildApiGiantBombPersonQuery groupByUpdatedAt() Group by the vgagpe_updated_at column
 *
 * @method     ChildApiGiantBombPersonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiGiantBombPersonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiGiantBombPersonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiGiantBombPersonQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiGiantBombPersonQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiGiantBombPersonQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiGiantBombPersonQuery leftJoinApiGiantBombCreditedGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the ApiGiantBombCreditedGame relation
 * @method     ChildApiGiantBombPersonQuery rightJoinApiGiantBombCreditedGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ApiGiantBombCreditedGame relation
 * @method     ChildApiGiantBombPersonQuery innerJoinApiGiantBombCreditedGame($relationAlias = null) Adds a INNER JOIN clause to the query using the ApiGiantBombCreditedGame relation
 *
 * @method     ChildApiGiantBombPersonQuery joinWithApiGiantBombCreditedGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ApiGiantBombCreditedGame relation
 *
 * @method     ChildApiGiantBombPersonQuery leftJoinWithApiGiantBombCreditedGame() Adds a LEFT JOIN clause and with to the query using the ApiGiantBombCreditedGame relation
 * @method     ChildApiGiantBombPersonQuery rightJoinWithApiGiantBombCreditedGame() Adds a RIGHT JOIN clause and with to the query using the ApiGiantBombCreditedGame relation
 * @method     ChildApiGiantBombPersonQuery innerJoinWithApiGiantBombCreditedGame() Adds a INNER JOIN clause and with to the query using the ApiGiantBombCreditedGame relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiGiantBombPerson findOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombPerson matching the query
 * @method     ChildApiGiantBombPerson findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiGiantBombPerson matching the query, or a new ChildApiGiantBombPerson object populated from the query conditions when no match is found
 *
 * @method     ChildApiGiantBombPerson findOneById(int $vgagpe_id) Return the first ChildApiGiantBombPerson filtered by the vgagpe_id column
 * @method     ChildApiGiantBombPerson findOneByName(string $vgagpe_name) Return the first ChildApiGiantBombPerson filtered by the vgagpe_name column
 * @method     ChildApiGiantBombPerson findOneByAliases(array $vgagpe_aliases) Return the first ChildApiGiantBombPerson filtered by the vgagpe_aliases column
 * @method     ChildApiGiantBombPerson findOneBySummary(string $vgagpe_summary) Return the first ChildApiGiantBombPerson filtered by the vgagpe_summary column
 * @method     ChildApiGiantBombPerson findOneByDescription(string $vgagpe_description) Return the first ChildApiGiantBombPerson filtered by the vgagpe_description column
 * @method     ChildApiGiantBombPerson findOneByGender(int $vgagpe_gender) Return the first ChildApiGiantBombPerson filtered by the vgagpe_gender column
 * @method     ChildApiGiantBombPerson findOneByBirthDate(string $vgagpe_birthdate) Return the first ChildApiGiantBombPerson filtered by the vgagpe_birthdate column
 * @method     ChildApiGiantBombPerson findOneByDeathDate(string $vgagpe_deathdate) Return the first ChildApiGiantBombPerson filtered by the vgagpe_deathdate column
 * @method     ChildApiGiantBombPerson findOneByHometown(string $vgagpe_hometown) Return the first ChildApiGiantBombPerson filtered by the vgagpe_hometown column
 * @method     ChildApiGiantBombPerson findOneByCountry(string $vgagpe_country) Return the first ChildApiGiantBombPerson filtered by the vgagpe_country column
 * @method     ChildApiGiantBombPerson findOneByApiDetailUrl(string $vgagpe_api_detail_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_api_detail_url column
 * @method     ChildApiGiantBombPerson findOneBySiteDetailUrl(string $vgagpe_site_detail_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_site_detail_url column
 * @method     ChildApiGiantBombPerson findOneByImageIconUrl(string $vgagpe_image_icon_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_icon_url column
 * @method     ChildApiGiantBombPerson findOneByImageMediumUrl(string $vgagpe_image_medium_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_medium_url column
 * @method     ChildApiGiantBombPerson findOneByImageScreenUrl(string $vgagpe_image_screen_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_screen_url column
 * @method     ChildApiGiantBombPerson findOneByImageSmallUrl(string $vgagpe_image_small_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_small_url column
 * @method     ChildApiGiantBombPerson findOneByImageSuperUrl(string $vgagpe_image_super_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_super_url column
 * @method     ChildApiGiantBombPerson findOneByImageThumbUrl(string $vgagpe_image_thumb_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_thumb_url column
 * @method     ChildApiGiantBombPerson findOneByImageTinyUrl(string $vgagpe_image_tiny_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_tiny_url column
 * @method     ChildApiGiantBombPerson findOneByGiantBombGameId(int $vgagpe_credited_vgagga_id) Return the first ChildApiGiantBombPerson filtered by the vgagpe_credited_vgagga_id column
 * @method     ChildApiGiantBombPerson findOneByCreatedAt(string $vgagpe_created_at) Return the first ChildApiGiantBombPerson filtered by the vgagpe_created_at column
 * @method     ChildApiGiantBombPerson findOneByUpdatedAt(string $vgagpe_updated_at) Return the first ChildApiGiantBombPerson filtered by the vgagpe_updated_at column *

 * @method     ChildApiGiantBombPerson requirePk($key, ConnectionInterface $con = null) Return the ChildApiGiantBombPerson by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOne(ConnectionInterface $con = null) Return the first ChildApiGiantBombPerson matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombPerson requireOneById(int $vgagpe_id) Return the first ChildApiGiantBombPerson filtered by the vgagpe_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByName(string $vgagpe_name) Return the first ChildApiGiantBombPerson filtered by the vgagpe_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByAliases(array $vgagpe_aliases) Return the first ChildApiGiantBombPerson filtered by the vgagpe_aliases column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneBySummary(string $vgagpe_summary) Return the first ChildApiGiantBombPerson filtered by the vgagpe_summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByDescription(string $vgagpe_description) Return the first ChildApiGiantBombPerson filtered by the vgagpe_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByGender(int $vgagpe_gender) Return the first ChildApiGiantBombPerson filtered by the vgagpe_gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByBirthDate(string $vgagpe_birthdate) Return the first ChildApiGiantBombPerson filtered by the vgagpe_birthdate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByDeathDate(string $vgagpe_deathdate) Return the first ChildApiGiantBombPerson filtered by the vgagpe_deathdate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByHometown(string $vgagpe_hometown) Return the first ChildApiGiantBombPerson filtered by the vgagpe_hometown column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByCountry(string $vgagpe_country) Return the first ChildApiGiantBombPerson filtered by the vgagpe_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByApiDetailUrl(string $vgagpe_api_detail_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_api_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneBySiteDetailUrl(string $vgagpe_site_detail_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageIconUrl(string $vgagpe_image_icon_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_icon_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageMediumUrl(string $vgagpe_image_medium_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_medium_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageScreenUrl(string $vgagpe_image_screen_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_screen_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageSmallUrl(string $vgagpe_image_small_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_small_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageSuperUrl(string $vgagpe_image_super_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_super_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageThumbUrl(string $vgagpe_image_thumb_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_thumb_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByImageTinyUrl(string $vgagpe_image_tiny_url) Return the first ChildApiGiantBombPerson filtered by the vgagpe_image_tiny_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByGiantBombGameId(int $vgagpe_credited_vgagga_id) Return the first ChildApiGiantBombPerson filtered by the vgagpe_credited_vgagga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByCreatedAt(string $vgagpe_created_at) Return the first ChildApiGiantBombPerson filtered by the vgagpe_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiGiantBombPerson requireOneByUpdatedAt(string $vgagpe_updated_at) Return the first ChildApiGiantBombPerson filtered by the vgagpe_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiGiantBombPerson[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiGiantBombPerson objects based on current ModelCriteria
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findById(int $vgagpe_id) Return ChildApiGiantBombPerson objects filtered by the vgagpe_id column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByName(string $vgagpe_name) Return ChildApiGiantBombPerson objects filtered by the vgagpe_name column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByAliases(array $vgagpe_aliases) Return ChildApiGiantBombPerson objects filtered by the vgagpe_aliases column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findBySummary(string $vgagpe_summary) Return ChildApiGiantBombPerson objects filtered by the vgagpe_summary column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByDescription(string $vgagpe_description) Return ChildApiGiantBombPerson objects filtered by the vgagpe_description column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByGender(int $vgagpe_gender) Return ChildApiGiantBombPerson objects filtered by the vgagpe_gender column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByBirthDate(string $vgagpe_birthdate) Return ChildApiGiantBombPerson objects filtered by the vgagpe_birthdate column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByDeathDate(string $vgagpe_deathdate) Return ChildApiGiantBombPerson objects filtered by the vgagpe_deathdate column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByHometown(string $vgagpe_hometown) Return ChildApiGiantBombPerson objects filtered by the vgagpe_hometown column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByCountry(string $vgagpe_country) Return ChildApiGiantBombPerson objects filtered by the vgagpe_country column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByApiDetailUrl(string $vgagpe_api_detail_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_api_detail_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findBySiteDetailUrl(string $vgagpe_site_detail_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_site_detail_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageIconUrl(string $vgagpe_image_icon_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_icon_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageMediumUrl(string $vgagpe_image_medium_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_medium_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageScreenUrl(string $vgagpe_image_screen_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_screen_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageSmallUrl(string $vgagpe_image_small_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_small_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageSuperUrl(string $vgagpe_image_super_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_super_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageThumbUrl(string $vgagpe_image_thumb_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_thumb_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByImageTinyUrl(string $vgagpe_image_tiny_url) Return ChildApiGiantBombPerson objects filtered by the vgagpe_image_tiny_url column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByGiantBombGameId(int $vgagpe_credited_vgagga_id) Return ChildApiGiantBombPerson objects filtered by the vgagpe_credited_vgagga_id column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByCreatedAt(string $vgagpe_created_at) Return ChildApiGiantBombPerson objects filtered by the vgagpe_created_at column
 * @method     ChildApiGiantBombPerson[]|ObjectCollection findByUpdatedAt(string $vgagpe_updated_at) Return ChildApiGiantBombPerson objects filtered by the vgagpe_updated_at column
 * @method     ChildApiGiantBombPerson[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiGiantBombPersonQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiGiantBombPersonQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiGiantBombPerson', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiGiantBombPersonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiGiantBombPersonQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiGiantBombPersonQuery) {
            return $criteria;
        }
        $query = new ChildApiGiantBombPersonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiGiantBombPerson|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiGiantBombPersonTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombPerson A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgagpe_id, vgagpe_name, vgagpe_aliases, vgagpe_summary, vgagpe_description, vgagpe_gender, vgagpe_birthdate, vgagpe_deathdate, vgagpe_hometown, vgagpe_country, vgagpe_api_detail_url, vgagpe_site_detail_url, vgagpe_image_icon_url, vgagpe_image_medium_url, vgagpe_image_screen_url, vgagpe_image_small_url, vgagpe_image_super_url, vgagpe_image_thumb_url, vgagpe_image_tiny_url, vgagpe_credited_vgagga_id, vgagpe_created_at, vgagpe_updated_at FROM videogames_api_giantbomb_person_vgagpe WHERE vgagpe_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiGiantBombPerson $obj */
            $obj = new ChildApiGiantBombPerson();
            $obj->hydrate($row);
            ApiGiantBombPersonTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiGiantBombPerson|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgagpe_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgagpe_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgagpe_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgagpe_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgagpe_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgagpe_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgagpe_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgagpe_aliases column
     *
     * @param     array $aliases The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByAliases($aliases = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($aliases as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagpe_aliases column
     * @param     mixed $aliases The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::CONTAINS_ALL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByAliase($aliases = null, $comparison = null)
    {
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            if (is_scalar($aliases)) {
                $aliases = '%| ' . $aliases . ' |%';
                $comparison = Criteria::LIKE;
            }
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            $aliases = '%| ' . $aliases . ' |%';
            $comparison = Criteria::NOT_LIKE;
            $key = $this->getAliasedColName(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES);
            if ($this->containsKey($key)) {
                $this->addAnd($key, $aliases, $comparison);
            } else {
                $this->addAnd($key, $aliases, $comparison);
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ALIASES, $aliases, $comparison);
    }

    /**
     * Filter the query on the vgagpe_summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE vgagpe_summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE vgagpe_summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the vgagpe_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE vgagpe_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE vgagpe_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the vgagpe_gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender(1234); // WHERE vgagpe_gender = 1234
     * $query->filterByGender(array(12, 34)); // WHERE vgagpe_gender IN (12, 34)
     * $query->filterByGender(array('min' => 12)); // WHERE vgagpe_gender > 12
     * </code>
     *
     * @param     mixed $gender The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (is_array($gender)) {
            $useMinMax = false;
            if (isset($gender['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER, $gender['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gender['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER, $gender['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the vgagpe_birthdate column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthDate('2011-03-14'); // WHERE vgagpe_birthdate = '2011-03-14'
     * $query->filterByBirthDate('now'); // WHERE vgagpe_birthdate = '2011-03-14'
     * $query->filterByBirthDate(array('max' => 'yesterday')); // WHERE vgagpe_birthdate > '2011-03-13'
     * </code>
     *
     * @param     mixed $birthDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByBirthDate($birthDate = null, $comparison = null)
    {
        if (is_array($birthDate)) {
            $useMinMax = false;
            if (isset($birthDate['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE, $birthDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthDate['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE, $birthDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_BIRTHDATE, $birthDate, $comparison);
    }

    /**
     * Filter the query on the vgagpe_deathdate column
     *
     * Example usage:
     * <code>
     * $query->filterByDeathDate('2011-03-14'); // WHERE vgagpe_deathdate = '2011-03-14'
     * $query->filterByDeathDate('now'); // WHERE vgagpe_deathdate = '2011-03-14'
     * $query->filterByDeathDate(array('max' => 'yesterday')); // WHERE vgagpe_deathdate > '2011-03-13'
     * </code>
     *
     * @param     mixed $deathDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByDeathDate($deathDate = null, $comparison = null)
    {
        if (is_array($deathDate)) {
            $useMinMax = false;
            if (isset($deathDate['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE, $deathDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deathDate['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE, $deathDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_DEATHDATE, $deathDate, $comparison);
    }

    /**
     * Filter the query on the vgagpe_hometown column
     *
     * Example usage:
     * <code>
     * $query->filterByHometown('fooValue');   // WHERE vgagpe_hometown = 'fooValue'
     * $query->filterByHometown('%fooValue%'); // WHERE vgagpe_hometown LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hometown The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByHometown($hometown = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hometown)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_HOMETOWN, $hometown, $comparison);
    }

    /**
     * Filter the query on the vgagpe_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE vgagpe_country = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE vgagpe_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the vgagpe_api_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDetailUrl('fooValue');   // WHERE vgagpe_api_detail_url = 'fooValue'
     * $query->filterByApiDetailUrl('%fooValue%'); // WHERE vgagpe_api_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByApiDetailUrl($apiDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_API_DETAIL_URL, $apiDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgagpe_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgagpe_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_icon_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageIconUrl('fooValue');   // WHERE vgagpe_image_icon_url = 'fooValue'
     * $query->filterByImageIconUrl('%fooValue%'); // WHERE vgagpe_image_icon_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageIconUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageIconUrl($imageIconUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageIconUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_ICON_URL, $imageIconUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_medium_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageMediumUrl('fooValue');   // WHERE vgagpe_image_medium_url = 'fooValue'
     * $query->filterByImageMediumUrl('%fooValue%'); // WHERE vgagpe_image_medium_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageMediumUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageMediumUrl($imageMediumUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageMediumUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_MEDIUM_URL, $imageMediumUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_screen_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageScreenUrl('fooValue');   // WHERE vgagpe_image_screen_url = 'fooValue'
     * $query->filterByImageScreenUrl('%fooValue%'); // WHERE vgagpe_image_screen_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageScreenUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageScreenUrl($imageScreenUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageScreenUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SCREEN_URL, $imageScreenUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_small_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSmallUrl('fooValue');   // WHERE vgagpe_image_small_url = 'fooValue'
     * $query->filterByImageSmallUrl('%fooValue%'); // WHERE vgagpe_image_small_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSmallUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageSmallUrl($imageSmallUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSmallUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SMALL_URL, $imageSmallUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_super_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageSuperUrl('fooValue');   // WHERE vgagpe_image_super_url = 'fooValue'
     * $query->filterByImageSuperUrl('%fooValue%'); // WHERE vgagpe_image_super_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageSuperUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageSuperUrl($imageSuperUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageSuperUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_SUPER_URL, $imageSuperUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_thumb_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageThumbUrl('fooValue');   // WHERE vgagpe_image_thumb_url = 'fooValue'
     * $query->filterByImageThumbUrl('%fooValue%'); // WHERE vgagpe_image_thumb_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageThumbUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageThumbUrl($imageThumbUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageThumbUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_THUMB_URL, $imageThumbUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_image_tiny_url column
     *
     * Example usage:
     * <code>
     * $query->filterByImageTinyUrl('fooValue');   // WHERE vgagpe_image_tiny_url = 'fooValue'
     * $query->filterByImageTinyUrl('%fooValue%'); // WHERE vgagpe_image_tiny_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageTinyUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByImageTinyUrl($imageTinyUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageTinyUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_IMAGE_TINY_URL, $imageTinyUrl, $comparison);
    }

    /**
     * Filter the query on the vgagpe_credited_vgagga_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGiantBombGameId(1234); // WHERE vgagpe_credited_vgagga_id = 1234
     * $query->filterByGiantBombGameId(array(12, 34)); // WHERE vgagpe_credited_vgagga_id IN (12, 34)
     * $query->filterByGiantBombGameId(array('min' => 12)); // WHERE vgagpe_credited_vgagga_id > 12
     * </code>
     *
     * @see       filterByApiGiantBombCreditedGame()
     *
     * @param     mixed $giantBombGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByGiantBombGameId($giantBombGameId = null, $comparison = null)
    {
        if (is_array($giantBombGameId)) {
            $useMinMax = false;
            if (isset($giantBombGameId['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $giantBombGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($giantBombGameId['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $giantBombGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $giantBombGameId, $comparison);
    }

    /**
     * Filter the query on the vgagpe_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgagpe_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgagpe_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgagpe_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgagpe_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgagpe_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgagpe_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgagpe_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame|ObjectCollection $apiGiantBombGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function filterByApiGiantBombCreditedGame($apiGiantBombGame, $comparison = null)
    {
        if ($apiGiantBombGame instanceof \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame) {
            return $this
                ->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $apiGiantBombGame->getId(), $comparison);
        } elseif ($apiGiantBombGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_CREDITED_VGAGGA_ID, $apiGiantBombGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApiGiantBombCreditedGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\ApiGiantBombGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ApiGiantBombCreditedGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function joinApiGiantBombCreditedGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ApiGiantBombCreditedGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ApiGiantBombCreditedGame');
        }

        return $this;
    }

    /**
     * Use the ApiGiantBombCreditedGame relation ApiGiantBombGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery A secondary query class using the current class as primary query
     */
    public function useApiGiantBombCreditedGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApiGiantBombCreditedGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ApiGiantBombCreditedGame', '\IiMedias\VideoGamesBundle\Model\ApiGiantBombGameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiGiantBombPerson $apiGiantBombPerson Object to remove from the list of results
     *
     * @return $this|ChildApiGiantBombPersonQuery The current query, for fluid interface
     */
    public function prune($apiGiantBombPerson = null)
    {
        if ($apiGiantBombPerson) {
            $this->addUsingAlias(ApiGiantBombPersonTableMap::COL_VGAGPE_ID, $apiGiantBombPerson->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_giantbomb_person_vgagpe table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiGiantBombPersonTableMap::clearInstancePool();
            ApiGiantBombPersonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiGiantBombPersonTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiGiantBombPersonTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiGiantBombPersonTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiGiantBombPersonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApiGiantBombPersonQuery
