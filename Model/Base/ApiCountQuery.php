<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\VideoGamesBundle\Model\ApiCount as ChildApiCount;
use IiMedias\VideoGamesBundle\Model\ApiCountQuery as ChildApiCountQuery;
use IiMedias\VideoGamesBundle\Model\Map\ApiCountTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_api_count_vgapic' table.
 *
 *
 *
 * @method     ChildApiCountQuery orderById($order = Criteria::ASC) Order by the vgapic_id column
 * @method     ChildApiCountQuery orderByKey($order = Criteria::ASC) Order by the vgapic_key column
 * @method     ChildApiCountQuery orderByValue($order = Criteria::ASC) Order by the vgapic_value column
 * @method     ChildApiCountQuery orderByRemainingQueryNumber($order = Criteria::ASC) Order by the vgapic_remaining_query_number column
 * @method     ChildApiCountQuery orderByMaxQueryNumber($order = Criteria::ASC) Order by the vgapic_max_query_number column
 * @method     ChildApiCountQuery orderByMaxQueryInterval($order = Criteria::ASC) Order by the vgapic_max_query_interval column
 * @method     ChildApiCountQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the vgapic_created_by_user_id column
 * @method     ChildApiCountQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the vgapic_updated_by_user_id column
 * @method     ChildApiCountQuery orderByCreatedAt($order = Criteria::ASC) Order by the vgapic_created_at column
 * @method     ChildApiCountQuery orderByUpdatedAt($order = Criteria::ASC) Order by the vgapic_updated_at column
 *
 * @method     ChildApiCountQuery groupById() Group by the vgapic_id column
 * @method     ChildApiCountQuery groupByKey() Group by the vgapic_key column
 * @method     ChildApiCountQuery groupByValue() Group by the vgapic_value column
 * @method     ChildApiCountQuery groupByRemainingQueryNumber() Group by the vgapic_remaining_query_number column
 * @method     ChildApiCountQuery groupByMaxQueryNumber() Group by the vgapic_max_query_number column
 * @method     ChildApiCountQuery groupByMaxQueryInterval() Group by the vgapic_max_query_interval column
 * @method     ChildApiCountQuery groupByCreatedByUserId() Group by the vgapic_created_by_user_id column
 * @method     ChildApiCountQuery groupByUpdatedByUserId() Group by the vgapic_updated_by_user_id column
 * @method     ChildApiCountQuery groupByCreatedAt() Group by the vgapic_created_at column
 * @method     ChildApiCountQuery groupByUpdatedAt() Group by the vgapic_updated_at column
 *
 * @method     ChildApiCountQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApiCountQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApiCountQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApiCountQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApiCountQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApiCountQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApiCountQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildApiCountQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildApiCountQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildApiCountQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildApiCountQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildApiCountQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildApiCountQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildApiCountQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildApiCountQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildApiCountQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildApiCountQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildApiCountQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildApiCountQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildApiCountQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     \IiMedias\AdminBundle\Model\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApiCount findOne(ConnectionInterface $con = null) Return the first ChildApiCount matching the query
 * @method     ChildApiCount findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApiCount matching the query, or a new ChildApiCount object populated from the query conditions when no match is found
 *
 * @method     ChildApiCount findOneById(int $vgapic_id) Return the first ChildApiCount filtered by the vgapic_id column
 * @method     ChildApiCount findOneByKey(string $vgapic_key) Return the first ChildApiCount filtered by the vgapic_key column
 * @method     ChildApiCount findOneByValue(string $vgapic_value) Return the first ChildApiCount filtered by the vgapic_value column
 * @method     ChildApiCount findOneByRemainingQueryNumber(int $vgapic_remaining_query_number) Return the first ChildApiCount filtered by the vgapic_remaining_query_number column
 * @method     ChildApiCount findOneByMaxQueryNumber(int $vgapic_max_query_number) Return the first ChildApiCount filtered by the vgapic_max_query_number column
 * @method     ChildApiCount findOneByMaxQueryInterval(int $vgapic_max_query_interval) Return the first ChildApiCount filtered by the vgapic_max_query_interval column
 * @method     ChildApiCount findOneByCreatedByUserId(int $vgapic_created_by_user_id) Return the first ChildApiCount filtered by the vgapic_created_by_user_id column
 * @method     ChildApiCount findOneByUpdatedByUserId(int $vgapic_updated_by_user_id) Return the first ChildApiCount filtered by the vgapic_updated_by_user_id column
 * @method     ChildApiCount findOneByCreatedAt(string $vgapic_created_at) Return the first ChildApiCount filtered by the vgapic_created_at column
 * @method     ChildApiCount findOneByUpdatedAt(string $vgapic_updated_at) Return the first ChildApiCount filtered by the vgapic_updated_at column *

 * @method     ChildApiCount requirePk($key, ConnectionInterface $con = null) Return the ChildApiCount by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOne(ConnectionInterface $con = null) Return the first ChildApiCount matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiCount requireOneById(int $vgapic_id) Return the first ChildApiCount filtered by the vgapic_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByKey(string $vgapic_key) Return the first ChildApiCount filtered by the vgapic_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByValue(string $vgapic_value) Return the first ChildApiCount filtered by the vgapic_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByRemainingQueryNumber(int $vgapic_remaining_query_number) Return the first ChildApiCount filtered by the vgapic_remaining_query_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByMaxQueryNumber(int $vgapic_max_query_number) Return the first ChildApiCount filtered by the vgapic_max_query_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByMaxQueryInterval(int $vgapic_max_query_interval) Return the first ChildApiCount filtered by the vgapic_max_query_interval column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByCreatedByUserId(int $vgapic_created_by_user_id) Return the first ChildApiCount filtered by the vgapic_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByUpdatedByUserId(int $vgapic_updated_by_user_id) Return the first ChildApiCount filtered by the vgapic_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByCreatedAt(string $vgapic_created_at) Return the first ChildApiCount filtered by the vgapic_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApiCount requireOneByUpdatedAt(string $vgapic_updated_at) Return the first ChildApiCount filtered by the vgapic_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApiCount[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApiCount objects based on current ModelCriteria
 * @method     ChildApiCount[]|ObjectCollection findById(int $vgapic_id) Return ChildApiCount objects filtered by the vgapic_id column
 * @method     ChildApiCount[]|ObjectCollection findByKey(string $vgapic_key) Return ChildApiCount objects filtered by the vgapic_key column
 * @method     ChildApiCount[]|ObjectCollection findByValue(string $vgapic_value) Return ChildApiCount objects filtered by the vgapic_value column
 * @method     ChildApiCount[]|ObjectCollection findByRemainingQueryNumber(int $vgapic_remaining_query_number) Return ChildApiCount objects filtered by the vgapic_remaining_query_number column
 * @method     ChildApiCount[]|ObjectCollection findByMaxQueryNumber(int $vgapic_max_query_number) Return ChildApiCount objects filtered by the vgapic_max_query_number column
 * @method     ChildApiCount[]|ObjectCollection findByMaxQueryInterval(int $vgapic_max_query_interval) Return ChildApiCount objects filtered by the vgapic_max_query_interval column
 * @method     ChildApiCount[]|ObjectCollection findByCreatedByUserId(int $vgapic_created_by_user_id) Return ChildApiCount objects filtered by the vgapic_created_by_user_id column
 * @method     ChildApiCount[]|ObjectCollection findByUpdatedByUserId(int $vgapic_updated_by_user_id) Return ChildApiCount objects filtered by the vgapic_updated_by_user_id column
 * @method     ChildApiCount[]|ObjectCollection findByCreatedAt(string $vgapic_created_at) Return ChildApiCount objects filtered by the vgapic_created_at column
 * @method     ChildApiCount[]|ObjectCollection findByUpdatedAt(string $vgapic_updated_at) Return ChildApiCount objects filtered by the vgapic_updated_at column
 * @method     ChildApiCount[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApiCountQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\ApiCountQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\ApiCount', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApiCountQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApiCountQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApiCountQuery) {
            return $criteria;
        }
        $query = new ChildApiCountQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApiCount|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApiCountTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApiCountTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiCount A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgapic_id, vgapic_key, vgapic_value, vgapic_remaining_query_number, vgapic_max_query_number, vgapic_max_query_interval, vgapic_created_by_user_id, vgapic_updated_by_user_id, vgapic_created_at, vgapic_updated_at FROM videogames_api_count_vgapic WHERE vgapic_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApiCount $obj */
            $obj = new ChildApiCount();
            $obj->hydrate($row);
            ApiCountTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApiCount|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgapic_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgapic_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgapic_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgapic_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgapic_key column
     *
     * Example usage:
     * <code>
     * $query->filterByKey('fooValue');   // WHERE vgapic_key = 'fooValue'
     * $query->filterByKey('%fooValue%'); // WHERE vgapic_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $key The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByKey($key = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($key)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_KEY, $key, $comparison);
    }

    /**
     * Filter the query on the vgapic_value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue('fooValue');   // WHERE vgapic_value = 'fooValue'
     * $query->filterByValue('%fooValue%'); // WHERE vgapic_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $value The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($value)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the vgapic_remaining_query_number column
     *
     * Example usage:
     * <code>
     * $query->filterByRemainingQueryNumber(1234); // WHERE vgapic_remaining_query_number = 1234
     * $query->filterByRemainingQueryNumber(array(12, 34)); // WHERE vgapic_remaining_query_number IN (12, 34)
     * $query->filterByRemainingQueryNumber(array('min' => 12)); // WHERE vgapic_remaining_query_number > 12
     * </code>
     *
     * @param     mixed $remainingQueryNumber The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByRemainingQueryNumber($remainingQueryNumber = null, $comparison = null)
    {
        if (is_array($remainingQueryNumber)) {
            $useMinMax = false;
            if (isset($remainingQueryNumber['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_REMAINING_QUERY_NUMBER, $remainingQueryNumber['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remainingQueryNumber['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_REMAINING_QUERY_NUMBER, $remainingQueryNumber['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_REMAINING_QUERY_NUMBER, $remainingQueryNumber, $comparison);
    }

    /**
     * Filter the query on the vgapic_max_query_number column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxQueryNumber(1234); // WHERE vgapic_max_query_number = 1234
     * $query->filterByMaxQueryNumber(array(12, 34)); // WHERE vgapic_max_query_number IN (12, 34)
     * $query->filterByMaxQueryNumber(array('min' => 12)); // WHERE vgapic_max_query_number > 12
     * </code>
     *
     * @param     mixed $maxQueryNumber The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByMaxQueryNumber($maxQueryNumber = null, $comparison = null)
    {
        if (is_array($maxQueryNumber)) {
            $useMinMax = false;
            if (isset($maxQueryNumber['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_NUMBER, $maxQueryNumber['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxQueryNumber['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_NUMBER, $maxQueryNumber['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_NUMBER, $maxQueryNumber, $comparison);
    }

    /**
     * Filter the query on the vgapic_max_query_interval column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxQueryInterval(1234); // WHERE vgapic_max_query_interval = 1234
     * $query->filterByMaxQueryInterval(array(12, 34)); // WHERE vgapic_max_query_interval IN (12, 34)
     * $query->filterByMaxQueryInterval(array('min' => 12)); // WHERE vgapic_max_query_interval > 12
     * </code>
     *
     * @param     mixed $maxQueryInterval The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByMaxQueryInterval($maxQueryInterval = null, $comparison = null)
    {
        if (is_array($maxQueryInterval)) {
            $useMinMax = false;
            if (isset($maxQueryInterval['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_INTERVAL, $maxQueryInterval['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxQueryInterval['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_INTERVAL, $maxQueryInterval['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_MAX_QUERY_INTERVAL, $maxQueryInterval, $comparison);
    }

    /**
     * Filter the query on the vgapic_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE vgapic_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE vgapic_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE vgapic_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the vgapic_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE vgapic_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE vgapic_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE vgapic_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the vgapic_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE vgapic_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE vgapic_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE vgapic_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the vgapic_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE vgapic_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE vgapic_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE vgapic_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApiCountQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApiCount $apiCount Object to remove from the list of results
     *
     * @return $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function prune($apiCount = null)
    {
        if ($apiCount) {
            $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_ID, $apiCount->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_api_count_vgapic table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiCountTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApiCountTableMap::clearInstancePool();
            ApiCountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApiCountTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApiCountTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApiCountTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApiCountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ApiCountTableMap::COL_VGAPIC_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ApiCountTableMap::COL_VGAPIC_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ApiCountTableMap::COL_VGAPIC_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ApiCountTableMap::COL_VGAPIC_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildApiCountQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ApiCountTableMap::COL_VGAPIC_CREATED_AT);
    }

} // ApiCountQuery
