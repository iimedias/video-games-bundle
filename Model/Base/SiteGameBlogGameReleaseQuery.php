<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameRelease as ChildSiteGameBlogGameRelease;
use IiMedias\VideoGamesBundle\Model\SiteGameBlogGameReleaseQuery as ChildSiteGameBlogGameReleaseQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteGameBlogGameReleaseTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_gameblog_release_vgabrl' table.
 *
 *
 *
 * @method     ChildSiteGameBlogGameReleaseQuery orderById($order = Criteria::ASC) Order by the vgabrl_id column
 * @method     ChildSiteGameBlogGameReleaseQuery orderBySiteGameBlogGameId($order = Criteria::ASC) Order by the vgabrl_vgabga_id column
 * @method     ChildSiteGameBlogGameReleaseQuery orderBySiteGameBlogPlatformId($order = Criteria::ASC) Order by the vgabrl_vgabpl_id column
 * @method     ChildSiteGameBlogGameReleaseQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgabrl_site_detail_url column
 *
 * @method     ChildSiteGameBlogGameReleaseQuery groupById() Group by the vgabrl_id column
 * @method     ChildSiteGameBlogGameReleaseQuery groupBySiteGameBlogGameId() Group by the vgabrl_vgabga_id column
 * @method     ChildSiteGameBlogGameReleaseQuery groupBySiteGameBlogPlatformId() Group by the vgabrl_vgabpl_id column
 * @method     ChildSiteGameBlogGameReleaseQuery groupBySiteDetailUrl() Group by the vgabrl_site_detail_url column
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoinSiteGameBlogGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameBlogGame relation
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoinSiteGameBlogGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameBlogGame relation
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoinSiteGameBlogGame($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameBlogGame relation
 *
 * @method     ChildSiteGameBlogGameReleaseQuery joinWithSiteGameBlogGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameBlogGame relation
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoinWithSiteGameBlogGame() Adds a LEFT JOIN clause and with to the query using the SiteGameBlogGame relation
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoinWithSiteGameBlogGame() Adds a RIGHT JOIN clause and with to the query using the SiteGameBlogGame relation
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoinWithSiteGameBlogGame() Adds a INNER JOIN clause and with to the query using the SiteGameBlogGame relation
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoinSiteGameBlogPlatform($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteGameBlogPlatform relation
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoinSiteGameBlogPlatform($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteGameBlogPlatform relation
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoinSiteGameBlogPlatform($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteGameBlogPlatform relation
 *
 * @method     ChildSiteGameBlogGameReleaseQuery joinWithSiteGameBlogPlatform($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteGameBlogPlatform relation
 *
 * @method     ChildSiteGameBlogGameReleaseQuery leftJoinWithSiteGameBlogPlatform() Adds a LEFT JOIN clause and with to the query using the SiteGameBlogPlatform relation
 * @method     ChildSiteGameBlogGameReleaseQuery rightJoinWithSiteGameBlogPlatform() Adds a RIGHT JOIN clause and with to the query using the SiteGameBlogPlatform relation
 * @method     ChildSiteGameBlogGameReleaseQuery innerJoinWithSiteGameBlogPlatform() Adds a INNER JOIN clause and with to the query using the SiteGameBlogPlatform relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery|\IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatformQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteGameBlogGameRelease findOne(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGameRelease matching the query
 * @method     ChildSiteGameBlogGameRelease findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGameRelease matching the query, or a new ChildSiteGameBlogGameRelease object populated from the query conditions when no match is found
 *
 * @method     ChildSiteGameBlogGameRelease findOneById(int $vgabrl_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_id column
 * @method     ChildSiteGameBlogGameRelease findOneBySiteGameBlogGameId(string $vgabrl_vgabga_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_vgabga_id column
 * @method     ChildSiteGameBlogGameRelease findOneBySiteGameBlogPlatformId(int $vgabrl_vgabpl_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_vgabpl_id column
 * @method     ChildSiteGameBlogGameRelease findOneBySiteDetailUrl(string $vgabrl_site_detail_url) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_site_detail_url column *

 * @method     ChildSiteGameBlogGameRelease requirePk($key, ConnectionInterface $con = null) Return the ChildSiteGameBlogGameRelease by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGameRelease requireOne(ConnectionInterface $con = null) Return the first ChildSiteGameBlogGameRelease matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameBlogGameRelease requireOneById(int $vgabrl_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGameRelease requireOneBySiteGameBlogGameId(string $vgabrl_vgabga_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_vgabga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGameRelease requireOneBySiteGameBlogPlatformId(int $vgabrl_vgabpl_id) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_vgabpl_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteGameBlogGameRelease requireOneBySiteDetailUrl(string $vgabrl_site_detail_url) Return the first ChildSiteGameBlogGameRelease filtered by the vgabrl_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteGameBlogGameRelease[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteGameBlogGameRelease objects based on current ModelCriteria
 * @method     ChildSiteGameBlogGameRelease[]|ObjectCollection findById(int $vgabrl_id) Return ChildSiteGameBlogGameRelease objects filtered by the vgabrl_id column
 * @method     ChildSiteGameBlogGameRelease[]|ObjectCollection findBySiteGameBlogGameId(string $vgabrl_vgabga_id) Return ChildSiteGameBlogGameRelease objects filtered by the vgabrl_vgabga_id column
 * @method     ChildSiteGameBlogGameRelease[]|ObjectCollection findBySiteGameBlogPlatformId(int $vgabrl_vgabpl_id) Return ChildSiteGameBlogGameRelease objects filtered by the vgabrl_vgabpl_id column
 * @method     ChildSiteGameBlogGameRelease[]|ObjectCollection findBySiteDetailUrl(string $vgabrl_site_detail_url) Return ChildSiteGameBlogGameRelease objects filtered by the vgabrl_site_detail_url column
 * @method     ChildSiteGameBlogGameRelease[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteGameBlogGameReleaseQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteGameBlogGameReleaseQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteGameBlogGameRelease', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteGameBlogGameReleaseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteGameBlogGameReleaseQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteGameBlogGameReleaseQuery) {
            return $criteria;
        }
        $query = new ChildSiteGameBlogGameReleaseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteGameBlogGameRelease|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteGameBlogGameReleaseTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteGameBlogGameReleaseTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameBlogGameRelease A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgabrl_id, vgabrl_vgabga_id, vgabrl_vgabpl_id, vgabrl_site_detail_url FROM videogames_site_gameblog_release_vgabrl WHERE vgabrl_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteGameBlogGameRelease $obj */
            $obj = new ChildSiteGameBlogGameRelease();
            $obj->hydrate($row);
            SiteGameBlogGameReleaseTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteGameBlogGameRelease|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgabrl_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgabrl_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgabrl_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgabrl_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgabrl_vgabga_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteGameBlogGameId(1234); // WHERE vgabrl_vgabga_id = 1234
     * $query->filterBySiteGameBlogGameId(array(12, 34)); // WHERE vgabrl_vgabga_id IN (12, 34)
     * $query->filterBySiteGameBlogGameId(array('min' => 12)); // WHERE vgabrl_vgabga_id > 12
     * </code>
     *
     * @see       filterBySiteGameBlogGame()
     *
     * @param     mixed $siteGameBlogGameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogGameId($siteGameBlogGameId = null, $comparison = null)
    {
        if (is_array($siteGameBlogGameId)) {
            $useMinMax = false;
            if (isset($siteGameBlogGameId['min'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABGA_ID, $siteGameBlogGameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteGameBlogGameId['max'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABGA_ID, $siteGameBlogGameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABGA_ID, $siteGameBlogGameId, $comparison);
    }

    /**
     * Filter the query on the vgabrl_vgabpl_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteGameBlogPlatformId(1234); // WHERE vgabrl_vgabpl_id = 1234
     * $query->filterBySiteGameBlogPlatformId(array(12, 34)); // WHERE vgabrl_vgabpl_id IN (12, 34)
     * $query->filterBySiteGameBlogPlatformId(array('min' => 12)); // WHERE vgabrl_vgabpl_id > 12
     * </code>
     *
     * @see       filterBySiteGameBlogPlatform()
     *
     * @param     mixed $siteGameBlogPlatformId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogPlatformId($siteGameBlogPlatformId = null, $comparison = null)
    {
        if (is_array($siteGameBlogPlatformId)) {
            $useMinMax = false;
            if (isset($siteGameBlogPlatformId['min'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABPL_ID, $siteGameBlogPlatformId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteGameBlogPlatformId['max'])) {
                $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABPL_ID, $siteGameBlogPlatformId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABPL_ID, $siteGameBlogPlatformId, $comparison);
    }

    /**
     * Filter the query on the vgabrl_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgabrl_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgabrl_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame|ObjectCollection $siteGameBlogGame The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogGame($siteGameBlogGame, $comparison = null)
    {
        if ($siteGameBlogGame instanceof \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame) {
            return $this
                ->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABGA_ID, $siteGameBlogGame->getId(), $comparison);
        } elseif ($siteGameBlogGame instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABGA_ID, $siteGameBlogGame->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteGameBlogGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameBlogGame or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameBlogGame relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteGameBlogGame($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameBlogGame');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameBlogGame');
        }

        return $this;
    }

    /**
     * Use the SiteGameBlogGame relation SiteGameBlogGame object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameBlogGameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameBlogGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameBlogGame', '\IiMedias\VideoGamesBundle\Model\SiteGameBlogGameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatform object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatform|ObjectCollection $siteGameBlogPlatform The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function filterBySiteGameBlogPlatform($siteGameBlogPlatform, $comparison = null)
    {
        if ($siteGameBlogPlatform instanceof \IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatform) {
            return $this
                ->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABPL_ID, $siteGameBlogPlatform->getId(), $comparison);
        } elseif ($siteGameBlogPlatform instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_VGABPL_ID, $siteGameBlogPlatform->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteGameBlogPlatform() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatform or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteGameBlogPlatform relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function joinSiteGameBlogPlatform($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteGameBlogPlatform');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteGameBlogPlatform');
        }

        return $this;
    }

    /**
     * Use the SiteGameBlogPlatform relation SiteGameBlogPlatform object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatformQuery A secondary query class using the current class as primary query
     */
    public function useSiteGameBlogPlatformQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteGameBlogPlatform($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteGameBlogPlatform', '\IiMedias\VideoGamesBundle\Model\SiteGameBlogPlatformQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteGameBlogGameRelease $siteGameBlogGameRelease Object to remove from the list of results
     *
     * @return $this|ChildSiteGameBlogGameReleaseQuery The current query, for fluid interface
     */
    public function prune($siteGameBlogGameRelease = null)
    {
        if ($siteGameBlogGameRelease) {
            $this->addUsingAlias(SiteGameBlogGameReleaseTableMap::COL_VGABRL_ID, $siteGameBlogGameRelease->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_gameblog_release_vgabrl table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameBlogGameReleaseTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteGameBlogGameReleaseTableMap::clearInstancePool();
            SiteGameBlogGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteGameBlogGameReleaseTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteGameBlogGameReleaseTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteGameBlogGameReleaseTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteGameBlogGameReleaseTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteGameBlogGameReleaseQuery
