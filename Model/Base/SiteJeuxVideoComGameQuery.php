<?php

namespace IiMedias\VideoGamesBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGame as ChildSiteJeuxVideoComGame;
use IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameQuery as ChildSiteJeuxVideoComGameQuery;
use IiMedias\VideoGamesBundle\Model\Map\SiteJeuxVideoComGameTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videogames_site_jeuxvideocom_game_vgajga' table.
 *
 *
 *
 * @method     ChildSiteJeuxVideoComGameQuery orderById($order = Criteria::ASC) Order by the vgajga_id column
 * @method     ChildSiteJeuxVideoComGameQuery orderByGameId($order = Criteria::ASC) Order by the vgajga_vgagam_id column
 * @method     ChildSiteJeuxVideoComGameQuery orderByName($order = Criteria::ASC) Order by the vgajga_name column
 * @method     ChildSiteJeuxVideoComGameQuery orderByReleasedAt($order = Criteria::ASC) Order by the vgajga_released_at column
 * @method     ChildSiteJeuxVideoComGameQuery orderBySiteDetailUrl($order = Criteria::ASC) Order by the vgajga_site_detail_url column
 * @method     ChildSiteJeuxVideoComGameQuery orderByRefreshedAt($order = Criteria::ASC) Order by the vgajga_refreshed_at column
 *
 * @method     ChildSiteJeuxVideoComGameQuery groupById() Group by the vgajga_id column
 * @method     ChildSiteJeuxVideoComGameQuery groupByGameId() Group by the vgajga_vgagam_id column
 * @method     ChildSiteJeuxVideoComGameQuery groupByName() Group by the vgajga_name column
 * @method     ChildSiteJeuxVideoComGameQuery groupByReleasedAt() Group by the vgajga_released_at column
 * @method     ChildSiteJeuxVideoComGameQuery groupBySiteDetailUrl() Group by the vgajga_site_detail_url column
 * @method     ChildSiteJeuxVideoComGameQuery groupByRefreshedAt() Group by the vgajga_refreshed_at column
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteJeuxVideoComGameQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteJeuxVideoComGameQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteJeuxVideoComGameQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteJeuxVideoComGameQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoinGame($relationAlias = null) Adds a LEFT JOIN clause to the query using the Game relation
 * @method     ChildSiteJeuxVideoComGameQuery rightJoinGame($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Game relation
 * @method     ChildSiteJeuxVideoComGameQuery innerJoinGame($relationAlias = null) Adds a INNER JOIN clause to the query using the Game relation
 *
 * @method     ChildSiteJeuxVideoComGameQuery joinWithGame($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Game relation
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoinWithGame() Adds a LEFT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteJeuxVideoComGameQuery rightJoinWithGame() Adds a RIGHT JOIN clause and with to the query using the Game relation
 * @method     ChildSiteJeuxVideoComGameQuery innerJoinWithGame() Adds a INNER JOIN clause and with to the query using the Game relation
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoinSiteJeuxVideoComGameRelease($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteJeuxVideoComGameRelease relation
 * @method     ChildSiteJeuxVideoComGameQuery rightJoinSiteJeuxVideoComGameRelease($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteJeuxVideoComGameRelease relation
 * @method     ChildSiteJeuxVideoComGameQuery innerJoinSiteJeuxVideoComGameRelease($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteJeuxVideoComGameRelease relation
 *
 * @method     ChildSiteJeuxVideoComGameQuery joinWithSiteJeuxVideoComGameRelease($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteJeuxVideoComGameRelease relation
 *
 * @method     ChildSiteJeuxVideoComGameQuery leftJoinWithSiteJeuxVideoComGameRelease() Adds a LEFT JOIN clause and with to the query using the SiteJeuxVideoComGameRelease relation
 * @method     ChildSiteJeuxVideoComGameQuery rightJoinWithSiteJeuxVideoComGameRelease() Adds a RIGHT JOIN clause and with to the query using the SiteJeuxVideoComGameRelease relation
 * @method     ChildSiteJeuxVideoComGameQuery innerJoinWithSiteJeuxVideoComGameRelease() Adds a INNER JOIN clause and with to the query using the SiteJeuxVideoComGameRelease relation
 *
 * @method     \IiMedias\VideoGamesBundle\Model\GameQuery|\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameReleaseQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteJeuxVideoComGame findOne(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGame matching the query
 * @method     ChildSiteJeuxVideoComGame findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGame matching the query, or a new ChildSiteJeuxVideoComGame object populated from the query conditions when no match is found
 *
 * @method     ChildSiteJeuxVideoComGame findOneById(string $vgajga_id) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_id column
 * @method     ChildSiteJeuxVideoComGame findOneByGameId(int $vgajga_vgagam_id) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_vgagam_id column
 * @method     ChildSiteJeuxVideoComGame findOneByName(string $vgajga_name) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_name column
 * @method     ChildSiteJeuxVideoComGame findOneByReleasedAt(string $vgajga_released_at) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_released_at column
 * @method     ChildSiteJeuxVideoComGame findOneBySiteDetailUrl(string $vgajga_site_detail_url) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_site_detail_url column
 * @method     ChildSiteJeuxVideoComGame findOneByRefreshedAt(string $vgajga_refreshed_at) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_refreshed_at column *

 * @method     ChildSiteJeuxVideoComGame requirePk($key, ConnectionInterface $con = null) Return the ChildSiteJeuxVideoComGame by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOne(ConnectionInterface $con = null) Return the first ChildSiteJeuxVideoComGame matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteJeuxVideoComGame requireOneById(string $vgajga_id) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOneByGameId(int $vgajga_vgagam_id) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_vgagam_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOneByName(string $vgajga_name) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOneByReleasedAt(string $vgajga_released_at) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_released_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOneBySiteDetailUrl(string $vgajga_site_detail_url) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_site_detail_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteJeuxVideoComGame requireOneByRefreshedAt(string $vgajga_refreshed_at) Return the first ChildSiteJeuxVideoComGame filtered by the vgajga_refreshed_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteJeuxVideoComGame objects based on current ModelCriteria
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findById(string $vgajga_id) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_id column
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findByGameId(int $vgajga_vgagam_id) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_vgagam_id column
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findByName(string $vgajga_name) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_name column
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findByReleasedAt(string $vgajga_released_at) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_released_at column
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findBySiteDetailUrl(string $vgajga_site_detail_url) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_site_detail_url column
 * @method     ChildSiteJeuxVideoComGame[]|ObjectCollection findByRefreshedAt(string $vgajga_refreshed_at) Return ChildSiteJeuxVideoComGame objects filtered by the vgajga_refreshed_at column
 * @method     ChildSiteJeuxVideoComGame[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteJeuxVideoComGameQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\VideoGamesBundle\Model\Base\SiteJeuxVideoComGameQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\VideoGamesBundle\\Model\\SiteJeuxVideoComGame', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteJeuxVideoComGameQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteJeuxVideoComGameQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteJeuxVideoComGameQuery) {
            return $criteria;
        }
        $query = new ChildSiteJeuxVideoComGameQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteJeuxVideoComGame|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteJeuxVideoComGameTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteJeuxVideoComGame A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT vgajga_id, vgajga_vgagam_id, vgajga_name, vgajga_released_at, vgajga_site_detail_url, vgajga_refreshed_at FROM videogames_site_jeuxvideocom_game_vgajga WHERE vgajga_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteJeuxVideoComGame $obj */
            $obj = new ChildSiteJeuxVideoComGame();
            $obj->hydrate($row);
            SiteJeuxVideoComGameTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteJeuxVideoComGame|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the vgajga_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE vgajga_id = 1234
     * $query->filterById(array(12, 34)); // WHERE vgajga_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE vgajga_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vgajga_vgagam_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGameId(1234); // WHERE vgajga_vgagam_id = 1234
     * $query->filterByGameId(array(12, 34)); // WHERE vgajga_vgagam_id IN (12, 34)
     * $query->filterByGameId(array('min' => 12)); // WHERE vgajga_vgagam_id > 12
     * </code>
     *
     * @see       filterByGame()
     *
     * @param     mixed $gameId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByGameId($gameId = null, $comparison = null)
    {
        if (is_array($gameId)) {
            $useMinMax = false;
            if (isset($gameId['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, $gameId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gameId['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, $gameId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, $gameId, $comparison);
    }

    /**
     * Filter the query on the vgajga_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE vgajga_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE vgajga_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the vgajga_released_at column
     *
     * Example usage:
     * <code>
     * $query->filterByReleasedAt('2011-03-14'); // WHERE vgajga_released_at = '2011-03-14'
     * $query->filterByReleasedAt('now'); // WHERE vgajga_released_at = '2011-03-14'
     * $query->filterByReleasedAt(array('max' => 'yesterday')); // WHERE vgajga_released_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $releasedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByReleasedAt($releasedAt = null, $comparison = null)
    {
        if (is_array($releasedAt)) {
            $useMinMax = false;
            if (isset($releasedAt['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT, $releasedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($releasedAt['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT, $releasedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_RELEASED_AT, $releasedAt, $comparison);
    }

    /**
     * Filter the query on the vgajga_site_detail_url column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteDetailUrl('fooValue');   // WHERE vgajga_site_detail_url = 'fooValue'
     * $query->filterBySiteDetailUrl('%fooValue%'); // WHERE vgajga_site_detail_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteDetailUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterBySiteDetailUrl($siteDetailUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteDetailUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_SITE_DETAIL_URL, $siteDetailUrl, $comparison);
    }

    /**
     * Filter the query on the vgajga_refreshed_at column
     *
     * Example usage:
     * <code>
     * $query->filterByRefreshedAt('2011-03-14'); // WHERE vgajga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt('now'); // WHERE vgajga_refreshed_at = '2011-03-14'
     * $query->filterByRefreshedAt(array('max' => 'yesterday')); // WHERE vgajga_refreshed_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $refreshedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByRefreshedAt($refreshedAt = null, $comparison = null)
    {
        if (is_array($refreshedAt)) {
            $useMinMax = false;
            if (isset($refreshedAt['min'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT, $refreshedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refreshedAt['max'])) {
                $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT, $refreshedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_REFRESHED_AT, $refreshedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\Game object
     *
     * @param \IiMedias\VideoGamesBundle\Model\Game|ObjectCollection $game The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterByGame($game, $comparison = null)
    {
        if ($game instanceof \IiMedias\VideoGamesBundle\Model\Game) {
            return $this
                ->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, $game->getId(), $comparison);
        } elseif ($game instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_VGAGAM_ID, $game->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGame() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\Game or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Game relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function joinGame($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Game');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Game');
        }

        return $this;
    }

    /**
     * Use the Game relation Game object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\GameQuery A secondary query class using the current class as primary query
     */
    public function useGameQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGame($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Game', '\IiMedias\VideoGamesBundle\Model\GameQuery');
    }

    /**
     * Filter the query by a related \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameRelease object
     *
     * @param \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameRelease|ObjectCollection $siteJeuxVideoComGameRelease the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function filterBySiteJeuxVideoComGameRelease($siteJeuxVideoComGameRelease, $comparison = null)
    {
        if ($siteJeuxVideoComGameRelease instanceof \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameRelease) {
            return $this
                ->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $siteJeuxVideoComGameRelease->getSiteJeuxVideoComGameId(), $comparison);
        } elseif ($siteJeuxVideoComGameRelease instanceof ObjectCollection) {
            return $this
                ->useSiteJeuxVideoComGameReleaseQuery()
                ->filterByPrimaryKeys($siteJeuxVideoComGameRelease->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteJeuxVideoComGameRelease() only accepts arguments of type \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameRelease or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteJeuxVideoComGameRelease relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function joinSiteJeuxVideoComGameRelease($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteJeuxVideoComGameRelease');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteJeuxVideoComGameRelease');
        }

        return $this;
    }

    /**
     * Use the SiteJeuxVideoComGameRelease relation SiteJeuxVideoComGameRelease object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameReleaseQuery A secondary query class using the current class as primary query
     */
    public function useSiteJeuxVideoComGameReleaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteJeuxVideoComGameRelease($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteJeuxVideoComGameRelease', '\IiMedias\VideoGamesBundle\Model\SiteJeuxVideoComGameReleaseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteJeuxVideoComGame $siteJeuxVideoComGame Object to remove from the list of results
     *
     * @return $this|ChildSiteJeuxVideoComGameQuery The current query, for fluid interface
     */
    public function prune($siteJeuxVideoComGame = null)
    {
        if ($siteJeuxVideoComGame) {
            $this->addUsingAlias(SiteJeuxVideoComGameTableMap::COL_VGAJGA_ID, $siteJeuxVideoComGame->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videogames_site_jeuxvideocom_game_vgajga table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteJeuxVideoComGameTableMap::clearInstancePool();
            SiteJeuxVideoComGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteJeuxVideoComGameTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteJeuxVideoComGameTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteJeuxVideoComGameTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteJeuxVideoComGameTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteJeuxVideoComGameQuery
