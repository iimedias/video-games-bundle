<?php

namespace IiMedias\VideoGamesBundle\Form\Type;

use IiMedias\VideoGamesBundle\Model\Game;
use IiMedias\VideoGamesBundle\Model\GameQuery;
use IiMedias\VideoGamesBundle\Model\GameRelease;
use IiMedias\VideoGamesBundle\Model\PlatformQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;


class GameReleaseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $platformChoices = [];
        $platforms       = PlatformQuery::getAll();
        foreach ($platforms as $platform) {
            $platformChoices[$platform->getName()] = $platform->getId();
        }

        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('gameId', HiddenType::class)
            ->add('platformId', ChoiceType::class, ['required' => true, 'choices' => $platformChoices])
            ->add('boxart', FileType::class, ['required' => false])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GameRelease::class,
            'name'       => 'Game',
        ));
    }
}
