<?php

namespace IiMedias\VideoGamesBundle\Form\Type;

use IiMedias\VideoGamesBundle\Model\Platform;
use IiMedias\VideoGamesBundle\Model\Game;
use IiMedias\VideoGamesBundle\Model\GameQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;


class PlatformType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['required' => true])
//            ->add('gameblog_link_ids', HiddenType::class, array('required' => false))
//            ->add('gameblog_link_search', TextType::class, array('required' => false))
//            ->add('gamekult_link_ids', HiddenType::class, array('required' => false))
//            ->add('gamekult_link_search', TextType::class, array('required' => false))
//            ->add('giantbomb_link_ids', HiddenType::class)
//            ->add('igdb_link_ids', HiddenType::class)
//            ->add('jeuxvideocom_link_ids', HiddenType::class, array('required' => false))
//            ->add('jeuxvideocom_link_search', TextType::class, array('required' => false))
//            ->add('thegamesdb_link_ids', HiddenType::class)
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Platform::class,
            'name'       => 'Game',
        ));
    }
}
