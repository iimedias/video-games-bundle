<?php

namespace IiMedias\VideoGamesBundle\Form\Type;

use IiMedias\VideoGamesBundle\Model\GameQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;


class GameLinkGiantBombType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = array();
        $games   = GameQuery::create()
            ->orderByName()
            ->find();
        foreach ($games as $game) {
            $choices[$game->getName()] = $game->getId();
        }

        $builder
            ->add('game_id', ChoiceType::class, array(
                'required' => false,
                'choices'  => $choices,
            ))
            ->add('gamelink', TextType::class, array(
                'required' => false,
            ))
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IiMedias\VideoGamesBundle\Model\ApiGiantBombGame',
            'name'       => 'ApiGiantBombGame',
        ));
    }
}